<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<portlet:defineObjects />

<%
	boolean isAdmin = false;
	if(renderRequest.getAttribute("isAdmin") != null
			&& renderRequest.getAttribute("isAdmin").toString().equalsIgnoreCase("true")) isAdmin = true;
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();

	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
	//
	if(isAdmin){
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="uploadHtml5FileURL"  name="uploadHtml5File"/>

<script  type="text/javascript">	
	function sendingIndustry() {
		Liferay.fire('fileUploadOk', "true");
		//
		AUI().DialogManager.hideAll();
	}
</script>

<aui:form action="<%=uploadHtml5FileURL%>"  enctype="multipart/form-data" method="post" >
	<aui:layout>
		<aui:column cssClass="td">上傳本課程的HTML5壓縮檔(*.zip)：</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName0" label=""/>
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="上傳" cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>
<%} %>

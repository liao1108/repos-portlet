package com.itez.feb;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletContext;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import net.lingala.zip4j.model.FileHeader;

import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/*
 * 本portlet不適用於 Liferay 6.1.0 版本
 * Bitbucket 交付通知 測試 (Tina 2021/1/13) 
 */
public class Html5UploaderPortlet extends MVCPortlet {
	long topicID = 0;
	long itemID = 0;
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			try{
	    		topicID = Long.parseLong(LiferaySessionUtil.getGlobalSessionAttribute("currTopicID", renderRequest).toString());
	    	}catch(Exception _ex){
	    	}
	    	//
	    	try{
	    		itemID = Long.parseLong(LiferaySessionUtil.getGlobalSessionAttribute("currItemID", renderRequest).toString());
	    	}catch(Exception _ex){
	    	}
	    	PermissionChecker pc =  PermissionCheckerFactoryUtil.create(themeDisplay.getUser());
			if(pc.isOmniadmin()){
				renderRequest.setAttribute("isAdmin", "true");
			}else{
				renderRequest.setAttribute("isAdmin", "false");
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}
		//
		super.doView(renderRequest, renderResponse);
	}

	//瑼��
	@SuppressWarnings("unchecked")
	public void uploadHtml5File(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		try{
	        ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        PortletContext portletContext = actionRequest.getPortletSession().getPortletContext();
	        //
	        UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        String sourceFileName = uploadRequest.getFileName("fileName0");
			if(!sourceFileName.toLowerCase().endsWith("zip")) throw new Exception("銝��瑼��zip��");
	        File fileUploaded = uploadRequest.getFile("fileName0");
	        if(fileUploaded == null) throw new Exception(sourceFileName + " 銝憭望���");
	        File file = new File(fileUploaded.getParentFile().getCanonicalPath() + "\\" + "itezupload_" + System.currentTimeMillis() + ".zip");
	        fileUploaded.renameTo(file);
			//InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
			//
	        Folder folderTopic = DLAppServiceUtil.getFolder(topicID);
	        //皜祈岫�
	        //Folder folderTopic = null;
	        //List<Folder> listRootFolder = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
	        //for(Folder folder: listRootFolder){
	        //	if(folder.getName().contains("隤脩��")){
	        //		folderTopic = folder;
	        //		break;
	        //	}
	        //}
	        //
	    	Folder folderItem = null;
	    	if(itemID == 0){
	    		Hashtable<String, Long> ht = new Hashtable<String, Long>();
	    		Vector<String> vec = new Vector<String>();
	    		//
	    		List<Folder> list = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), folderTopic.getFolderId());
	    		for(Folder fd: list){
					vec.add(fd.getName());
					ht.put(fd.getName(), fd.getFolderId());
				}
				Collections.sort(vec);
				if(vec.size() > 0){
					itemID = ht.get(vec.get(0));
				}
	    	}
	    	//
	    	if(itemID != 0){
	    		folderItem = DLAppServiceUtil.getFolder(itemID);
	    	}else{
	    		throw new Exception("Item not exist !");
	    	}
	    	//
	    	Folder folderHtml5 = null;
	    	List<Folder> listFolder = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), folderItem.getFolderId());
    		for(Folder fd: listFolder){
				if(fd.getName().equalsIgnoreCase("html5")){
					folderHtml5 = fd;
					break;
				}
			}
    		if(folderHtml5 == null) folderHtml5 = DLAppServiceUtil.addFolder(themeDisplay.getScopeGroupId(),
    																		folderItem.getFolderId(),
    																		"html5",
    																		"",
    																		new com.liferay.portal.service.ServiceContext());
    		Folder folderData = null;
    		listFolder = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), folderHtml5.getFolderId());
    		for(Folder fd: listFolder){
				if(fd.getName().equalsIgnoreCase("data")){
					folderData = fd;
					break;
				}
			}
    		if(folderData == null) folderData = DLAppServiceUtil.addFolder(themeDisplay.getScopeGroupId(),
    																		folderHtml5.getFolderId(),
    																		"data",
    																		"",
    																		new com.liferay.portal.service.ServiceContext());
	    	//皜征 html5 瑼�冗
    		for(FileEntry fe: DLAppServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), folderHtml5.getFolderId())){
    			DLAppServiceUtil.deleteFileEntry(fe.getFileEntryId());
    		}
    		//皜征 data 瑼�冗
    		for(FileEntry fe: DLAppServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), folderData.getFolderId())){
    			DLAppServiceUtil.deleteFileEntry(fe.getFileEntryId());
    		}
    		//
    		//蝣箏��� Servlet 瑼�冗�
    		File fd = new File(portletContext.getRealPath("/"));
    		while(!fd.getName().toLowerCase().contains("tomcat")){
    			fd = fd.getParentFile();
    			if(fd == null) break;
    		}
    		if(fd == null) throw new Exception("�瘜�tomcat�����");
    		//
    		File fdRootHtml5 = new File(fd.getCanonicalPath() + "/webapps/Html5Servlet");
    		if(!fdRootHtml5.exists()) throw new Exception("HTML5 Servlet �摰�� !");
    		//
    		File fdHtml5 = null;
    		File fdData = null;
    		//
    		HashMap<String, net.lingala.zip4j.io.ZipInputStream> mapZis = new HashMap<String, net.lingala.zip4j.io.ZipInputStream>();
    		//
    		net.lingala.zip4j.core.ZipFile zipFile = new net.lingala.zip4j.core.ZipFile(file);
    		if(zipFile == null) throw new Exception(file.getCanonicalPath() + " 銝�嚗�");
    		//
    		List<FileHeader> listHeader = zipFile.getFileHeaders();
			for (FileHeader header : listHeader) {
				if (header.getFileName().toLowerCase().contains("index.htm")){
					mapZis.put("index.html", zipFile.getInputStream(header));
				}else if (header.getFileName().toLowerCase().contains("data/")){
					int idx = header.getFileName().lastIndexOf("/");
					if(idx < 0) continue;
					String fileName = header.getFileName().substring(idx + 1).trim();
					if(fileName.trim().isEmpty()) continue;
					//
					mapZis.put(fileName, zipFile.getInputStream(header));
				}
			}
			if(!mapZis.containsKey("index.html")) throw new Exception("HTML5 zip瑼�銝 index.html��");
			//	
			fdHtml5 = new File(fdRootHtml5.getCanonicalPath() + "\\" + folderItem.getFolderId());
			if(!fdHtml5.exists()) fdHtml5.mkdir();

			fdData = new File(fdHtml5.getCanonicalPath() + "\\" + "data");
			if(!fdData.exists()) fdData.mkdir();
			//
			for(String fileName: mapZis.keySet()){
				net.lingala.zip4j.io.ZipInputStream zis = mapZis.get(fileName);
				//ByteArrayOutputStream baos = new ByteArrayOutputStream();
				//int b = 0;
				//while ((b = zis.read()) != -1) baos.write(b);
	        	//
				long parentId = -1;
				File fdDest = null;
				if(fileName.equalsIgnoreCase("index.html")){
					parentId = folderHtml5.getFolderId();
					fdDest = fdHtml5;
	        	}else{
	        		parentId = folderData.getFolderId();
	        		fdDest = fdData;
	        	}
				System.out.println("撖急��: " + fileName + " ...");
				
	        	//DLAppServiceUtil.addFileEntry(themeDisplay.getScopeGroupId(),
	        	//								parentId,
				//								fileName,
				//								MimeTypesUtil.getContentType(fileName),
				//								fileName,
				//								"",
				//								"",
				//								zis,
				//								baos.size(),
				//								new com.liferay.portal.service.ServiceContext());
	        	//
	        	File fileDest = new File(fdDest.getCanonicalPath() + "\\" + fileName);
	        	if(fileDest.exists()) fileDest.delete();
	        	//
	        	FileUtils.copyInputStreamToFile(zis, fileDest);
			}
	        //
			file.delete();
			//
		 	String msg = "撌脫���瑼��: " + sourceFileName + "��";
			actionRequest.setAttribute("successMsg", msg);
			SessionMessages.add(actionRequest, "success");
	     }catch(Exception ex){
	    	ex.printStackTrace();
	    	//
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionRresponse.setRenderParameter("jspPage", "/html/uploadhtml5zip/view.jsp");
	     }	
	}
	
	private String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		//if(showErrorCode == 1){
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			ex.printStackTrace(printWriter);
			//
			BufferedReader reader = new BufferedReader(
					   						new StringReader(stringWriter.toString()));
			try{
				String str = "";
				while((str = reader.readLine()) != null){
					if(str.contains("com.itez") && str.contains("(") && str.contains(")") ){
						int idx1 = str.indexOf("(");
						int idx2 = str.lastIndexOf(")");
						String s = str.substring(idx1 + 1, idx2).replace(".java", "");
						ret += ("[" + s + "]");
						break;
					}
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		//}
		return ret;
	}
	
	private String cleanXSS(String value) {
        //You'll need to remove the spaces from the html entities below
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("script", "");
		value = value.replaceAll("alert", "");
		return value;
	}
}

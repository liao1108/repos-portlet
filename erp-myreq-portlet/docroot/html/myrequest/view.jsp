<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.ErpUtil"%>
<%@ page import= "com.itez.Req"%>

<portlet:defineObjects />
 
<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	Vector<Req> vecReqAwait = new Vector<Req>();
	if(renderRequest.getPortletSession(true).getAttribute("vecReqAwait") != null){
		vecReqAwait = (Vector<Req>)renderRequest.getPortletSession(true).getAttribute("vecReqAwait");
	}

	Vector<Req> vecReqDone = new Vector<Req>();
	if(renderRequest.getPortletSession(true).getAttribute("vecReqDone") != null){
		vecReqDone = (Vector<Req>)renderRequest.getPortletSession(true).getAttribute("vecReqDone");
	}

	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="newAReqURL">
	<portlet:param name="jspPage" value="/html/myrequest/editReq.jsp"/>
	<portlet:param name="reqId" value="0"/>
</portlet:renderURL>		

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/req32_2.png">&nbsp;您(<%=ErpUtil.getCurrUserFullName(themeDisplay) %>)的調閱申請(未回覆)
	</aui:column>
	<aui:column cssClass="header">&nbsp;</aui:column>
	
	<aui:column cssClass="td">
		<br>
		<a href="<%=newAReqURL.toString()%>" >填寫調閱申請單</a>
	</aui:column>
</aui:layout>
<hr/>
<liferay-ui:search-container emptyResultsMessage="您沒有任何待回覆的調閱申請" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecReqAwait, searchContainer.getStart(), searchContainer.getEnd());
			total = vecReqAwait.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Req" keyProperty="reqId" modelVar="req">
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/req24_2.png"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="申請日期" property="reqDate" />
		<liferay-ui:search-container-column-text name="報告編號" property="reportNo" />
		<liferay-ui:search-container-column-text name="文件檔名" property="fileName" />
		<liferay-ui:search-container-column-text name="調閱說明" property="reqBrief"/>
		<liferay-ui:search-container-column-text name="審核人" property="approveBy"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminAwait.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<%if(vecReqDone.size() > 0){ %>
<br>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/req32_3.png">&nbsp;您(<%=ErpUtil.getCurrUserFullName(themeDisplay) %>)的調閱申請(已回覆)
	</aui:column>
</aui:layout>
<hr/>
<liferay-ui:search-container emptyResultsMessage="您沒有任何已回覆的調閱申請" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecReqDone, searchContainer.getStart(), searchContainer.getEnd());
			total = vecReqDone.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Req" keyProperty="reqId" modelVar="reqDone">
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/req24_3.png"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="申請日期" property="reqDate" />
		<liferay-ui:search-container-column-text name="報告編號" property="reportNo" />
		<liferay-ui:search-container-column-text name="文件檔名" property="fileName" />
		<liferay-ui:search-container-column-text name="調閱說明" property="reqBrief"/>
		<liferay-ui:search-container-column-text name="審核人" property="approveBy"/>
		<liferay-ui:search-container-column-text name="審核狀況" property="approveTypeName"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminDone.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
<%} %>


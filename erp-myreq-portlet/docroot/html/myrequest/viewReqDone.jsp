<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.Req"%>
<%@ page import= "com.itez.ResFile"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

  <%
  			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            	//
            	int reqId = Integer.parseInt(renderRequest.getParameter("reqId"));
            	//
            	Req q = new Req();
            	if(renderRequest.getPortletSession(true).getAttribute("vecReqDone") != null){
            		Vector<Req> vecReq = (Vector<Req>)renderRequest.getPortletSession(true).getAttribute("vecReqDone");
            		for(Req _q: vecReq){
            			if(_q.reqId == reqId){
            				q = _q;
            				break;
            			}
            		}
            	}
            	//
            	Vector<ResFile> vecRes = q.vecRes;
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doResponseReqURL"  name="doResponseReq" >
</portlet:actionURL>		

<portlet:actionURL var="doRejectReqURL"  name="doRejectReq" >
	<portlet:param name="reqId" value="<%=String.valueOf(q.reqId) %>"/>
</portlet:actionURL>		

<portlet:renderURL var="goBackURL">
  	<portlet:param name="jspPage"  value="/html/myrequest/view.jsp"/>
</portlet:renderURL>

<script type="text/javascript">
</script>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/req32_3.png">&nbsp;調閱審核結果
	</aui:column>
</aui:layout>
<hr/>

<aui:layout>
	<aui:column>	
		<aui:input name="reportNo" label="報告編號" value="<%=q.reportNo %>"  cssClass="td" readonly="readonly" />
	</aui:column>
	<aui:column>	
		<aui:input name="approveBy" label="審核人" value="<%=q.approveBy %>"  cssClass="td" readonly="readonly"/>
	</aui:column>
</aui:layout>
<aui:layout>	
	<aui:column>	
		<aui:input name="fileName" label="文件檔名" value="<%=q.fileName %>"  cssClass="td"  size="60" readonly="readonly"/>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column  cssClass="td"  columnWidth="90">
		<aui:input type="textarea" name="reqBrief" label="調閱文件說明"  value="<%=q.reqBrief %>" style="width: 100%;"  rows="4" readonly="readonly" />	
	</aui:column>
</aui:layout>

<br>

<aui:fieldset label="可調閱的文件">
<liferay-ui:search-container>
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecRes, searchContainer.getStart(), searchContainer.getEnd());
			total = vecRes.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.ResFile" keyProperty="fileId" modelVar="res">
		
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=res.fileId %>"/>
  			<portlet:param name="fileName"  value="<%=res.fileName %>"/>
  			<portlet:param name="actionType"  value="download"/>
  			<portlet:param name="currentTime"  value="<%=ErpUtil.getCurrDateTime() %>"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/req24_3.png"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="檔名" property="fileName" href="<%=downloadFileURL.toString() %>" />
		<liferay-ui:search-container-column-text name="可讀" property="canRead" />
		<liferay-ui:search-container-column-text name="可印" property="canPrint" />
		<liferay-ui:search-container-column-text name="可改" property="canEdit" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
</aui:fieldset>



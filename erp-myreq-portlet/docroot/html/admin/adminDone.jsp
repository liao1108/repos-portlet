<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Req" %>
<%@ page import="com.itez.ErpUtil" %>

<portlet:defineObjects />

<%
	boolean isLeader = false;
	if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
		isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	}
	
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Req q = (Req)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="goViewReqDoneURL" >
		<portlet:param name="reqId"  value="<%=String.valueOf(q.reqId)%>"/>
		<portlet:param name="jspPage"  value="/html/myrequest/viewReqDone.jsp"/>
	</portlet:renderURL>

	<%if(q.approveType == 1){ %>
		<liferay-ui:icon image="view" message="檢視" url="<%=goViewReqDoneURL.toString() %>"/>
	<%} %>	
</liferay-ui:icon-menu>
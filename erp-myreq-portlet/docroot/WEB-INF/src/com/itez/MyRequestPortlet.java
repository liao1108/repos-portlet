package com.itez;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.http.HttpHeaders;

import com.aspose.words.HeaderFooter;
import com.aspose.words.HeaderFooterType;
import com.aspose.words.HorizontalAlignment;
import com.aspose.words.License;
import com.aspose.words.Paragraph;
import com.aspose.words.RelativeHorizontalPosition;
import com.aspose.words.RelativeVerticalPosition;
import com.aspose.words.SaveFormat;
import com.aspose.words.Section;
import com.aspose.words.Shape;
import com.aspose.words.ShapeType;
import com.aspose.words.VerticalAlignment;
import com.aspose.words.WrapType;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyRequestPortlet extends MVCPortlet {
	DataSource ds = null;
	
	//
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		//載入 Aspose.Words License
	   		License license = new License();
	   		license.setLicense("Aspose.Words.lic");
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			//
			Vector<Req> vecReqAwait = this.getMyReqAwait(userFullName, conn);
			Vector<Req> vecReqDone = this.getMyReqDone(userFullName, conn);
			//
			renderRequest.getPortletSession(true).setAttribute("vecReqAwait", vecReqAwait);
			renderRequest.getPortletSession(true).setAttribute("vecReqDone", vecReqDone);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
			//
			
			
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//我的調閱記錄(已回覆)
	private Vector<Req> getMyReqDone(String userFullName, Connection conn) throws Exception{
		Vector<Req> vecReq = new Vector<Req>();
		//
		String sql = "SELECT * FROM doc_req WHERE req_by = ? AND done = 1 ORDER BY req_date DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Req q = new Req();
			q.reqId = rs.getInt("req_id");
			q.reqBy = userFullName;
			if(rs.getString("report_no") != null){
				q.reportNo = rs.getString("report_no");
			}
			if(rs.getString("file_name") != null){
				q.fileName = rs.getString("file_name");
			}
			q.reqBrief = rs.getString("req_brief");
			if(rs.getString("approve_by") != null){
				q.approveBy = rs.getString("approve_by");
			}
			q.reqDate = rs.getString("req_date");
			if(rs.getString("passwd") != null){
				q.passwd = rs.getString("passwd");
			}
			//
			q.approveType = rs.getInt("approve_type");
			if(q.approveType == 1){
				q.approveTypeName="核可調閱";
				//核可的檔案
				Vector<ResFile> vecRes = new Vector<ResFile>(); 
				sql = "SELECT * FROM req_files WHERE req_id = ?";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, q.reqId);
				ResultSet rs2 = ps.executeQuery();
				while(rs2.next()){
					ResFile r = new ResFile();
					r.fileId = rs2.getString("file_id");
					r.fileName = rs2.getString("file_name");
					if(rs2.getInt("can_read") == 1){
						r.canRead = "是";
					}
					if(rs2.getInt("can_print") == 1){
						r.canPrint = "是";
					}
					if(rs2.getInt("can_edit") == 1){
						r.canEdit = "是";
					}
					vecRes.add(r);
				}
				q.vecRes = vecRes;
			}else if(q.approveType == 2){
				q.approveTypeName="無法提供";
			}
			vecReq.add(q);
		}
		//
		return vecReq;
	}
	
	//我的調閱記錄(未回覆)
	private Vector<Req> getMyReqAwait(String userFullName, Connection conn) throws Exception{
		Vector<Req> vecReq = new Vector<Req>();
		//
		String sql = "SELECT * FROM doc_req WHERE req_by = ? AND (done IS NULL OR done=0) ORDER BY req_date DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Req q = new Req();
			q.reqId = rs.getInt("req_id");
			q.reqBy = userFullName;
			if(rs.getString("report_no") != null){
				q.reportNo = rs.getString("report_no");
			}
			if(rs.getString("file_name") != null){
				q.fileName = rs.getString("file_name");
			}
			q.reqBrief = rs.getString("req_brief");
			if(rs.getString("approve_by") != null){
				q.approveBy = rs.getString("approve_by");
			}
			q.reqDate = rs.getString("req_date");
			//
			vecReq.add(q);
		}
		//
		return vecReq;
	}

	//
	public void updateRequest(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//取得登入人密碼
			//String password = (String)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute(WebKeys.USER_PASSWORD);
			//
			if(!ErpUtil.isExamUser(userFullName, themeDisplay)) throw new Exception("您目前不在檢查人員名單內，無法調閱文件。");
			//
			conn = ds.getConnection();
			//
			Req q = new Req();
			q.reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			q.reportNo = actionRequest.getParameter("reportNo").trim();
			q.fileName = actionRequest.getParameter("fileName").trim();
			q.reqBrief = actionRequest.getParameter("reqBrief").trim();
			q.approveBy = actionRequest.getParameter("approveBy").trim();
			
			if(q.reportNo == null || q.reportNo.equals("")) throw new Exception("請指定文件所在的報告編號。");
			if(q.approveBy == null || q.approveBy.equals("")) throw new Exception("請指定審核人。");
			if(q.reqBrief == null || q.reqBrief.equals("")) throw new Exception("請說明申調的文件或描述。");
			//
			if(q.reqId == 0){
				ErpUtil.logUserAction("建立調閱單", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				
				//查詢報告編號所在的組別
				String secNameReport = "";
				String sql = "SELECT sec_name FROM reports WHERE report_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, q.reportNo);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					secNameReport = rs.getString(1);
				}
				rs.close();
				if(secNameReport.equals("")) throw new Exception("報告編號 " + q.reportNo + " 不存在。");
				//去掉組別英文字母代碼
				if(secNameReport.contains(".")) secNameReport = secNameReport.substring(secNameReport.indexOf(".") + 1);
				//
				User userApprove = null;
				List<User> listUser = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
				for(User _u: listUser){
					String _name = _u.getLastName().trim() + _u.getFirstName().trim();
					if(_name.equalsIgnoreCase(q.approveBy)){
						userApprove = _u;
						break;
					}
				}
				if(userApprove == null) throw new Exception("檢查人員資料庫內查無審核人 " + q.approveBy + "。");
				//
				String jobTitle = userApprove.getJobTitle();
				if(!jobTitle.contains("科長") && 
						!jobTitle.contains("專委") && 
						!jobTitle.contains("專門委員") && 
						!jobTitle.contains("副組長") && 
						!jobTitle.contains("組長")) throw new Exception("審核人「" + q.approveBy + "」之職稱為「" + jobTitle + "」，非科長、專委、副組長或組長等職位。");
				//查看看審核人是否位於該組
				boolean rightApprover = false;
				
				List<UserGroup> listGroup = userApprove.getUserGroups();
				for(UserGroup _ug: listGroup){
					if(_ug.getName().contains(secNameReport)){
						rightApprover = true;
						break;
					}
				}
				if(!rightApprover) throw new Exception("審核人" + q.approveBy + "目前不屬於" + secNameReport + "。");
				//確定建立
				q.reqDate = ErpUtil.getCurrDateTime();
				//
				sql = "INSERT INTO doc_req (req_date, " +			//1
											" report_no," +			//2
											" file_name," +			//3
											" req_brief," +			//4
											" approve_by," +		//5
											" req_by )" +			//6
										" VALUES (?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setString(1, q.reqDate);
				ps.setString(2, q.reportNo);
				ps.setString(3, q.fileName);
				ps.setString(4, q.reqBrief);
				ps.setString(5, q.approveBy);
				ps.setString(6, userFullName);
				ps.execute();
				//通知審核人
				String subject = "文件調閱審核通知";
				String msgText = q.approveBy +  jobTitle + " 您好;" + "\n";
				msgText += "\n";
				msgText += "檢查人員 " + userFullName + " 申請以下文件需請您批示：" + "\n";
				msgText += "\n";
				msgText += "申調日期：" + q.reqDate + "\n";
				msgText += "報告編號：" + q.reportNo + "\n";
				msgText += "檔案名稱：" + q.fileName + "\n";
				msgText += "申調原因：" + q.reqBrief + "\n";
				msgText += "\n";
				msgText += "\n";
				msgText += "檢查報告處理系統 敬上";
				//
				//發送電子郵件
				String toAddress = ErpUtil.getUserMailAddress(themeDisplay, q.approveBy);
				if(!toAddress.equals("")){
					ErpUtil.sendMail(toAddress, subject, msgText);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("req", q);
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/myrequest/editReq.jsp");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//取消調閱
	public void doCancelRequest(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			//
			conn = ds.getConnection();
			//
			ErpUtil.logUserAction("取消調閱單", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "DELETE FROM doc_req WHERE  (done IS NULL OR done =0)  AND req_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reqId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//提供文件
	public void doResponseReq(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			Req q = new Req();
			@SuppressWarnings("unchecked")
			Vector<Req> vecReqApprove = (Vector<Req>)actionRequest.getPortletSession(true).getAttribute("vecReqApprove");
			for(Req _q: vecReqApprove){
				if(_q.reqId == reqId){
					q = _q;
					break;
				}
			}
			//
			HashMap<String, String> htFile = (HashMap<String, String>)actionRequest.getPortletSession(true).getAttribute("htFile");
			Vector<String> vecFile = (Vector<String>)actionRequest.getPortletSession(true).getAttribute("vecFile");
			//準備入選的檔案ID
			Vector<String> vecNode = new Vector<String>();
			//
			for(int i=0; i < vecFile.size(); i++){
				String key = "INDEX_" + i;
				if(actionRequest.getParameter(key) != null && actionRequest.getParameter(key).equalsIgnoreCase("true")){
					vecNode.add(htFile.get(vecFile.get(i)));
				}
			}
			if(vecNode.size() == 0) throw new Exception("請指定可供調閱的文件。");
			//Liferay 申調區
			Folder fdReq = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, "申調區");
			//浮水印文字
			String watermarkText = q.reqBy + ErpUtil.getCurrDateTime();
			//
			conn = ds.getConnection();
			String passwd = this.genPasswd(6);		//隨機密碼
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			for(String fid: vecNode){
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fid);
				//複製至調閱區
				String pureName = alfDoc.getName().substring(0, alfDoc.getName().lastIndexOf("."));
				
				DateFormat formatter= new SimpleDateFormat("yyyyMMddHHmmss");
				formatter.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
				String destName = pureName + "_" + formatter.format(new Date()) + ".pdf";
				//加上文字浮水印
				com.aspose.words.Document aDoc = new com.aspose.words.Document(alfDoc.getContentStream().getStream());
				//
				Shape watermark = new Shape(aDoc, ShapeType.TEXT_PLAIN_TEXT);
			        // Set up the text of the watermark.
			        watermark.getTextPath().setText(watermarkText);
			        watermark.getTextPath().setFontFamily("新細明體");
			        watermark.setWidth(500);
			        watermark.setHeight(100);
			        // Text will be directed from the bottom-left to the top-right corner.
			        watermark.setRotation(-40);
			        // Remove the following two lines if you need a solid black text.
			        watermark.getFill().setColor(Color.GRAY); // Try LightGray to get more Word-style watermark
			        watermark.setStrokeColor(Color.GRAY); // Try LightGray to get more Word-style watermark
			        // Place the watermark in the page center.
			        watermark.setRelativeHorizontalPosition(RelativeHorizontalPosition.PAGE);
			        watermark.setRelativeVerticalPosition(RelativeVerticalPosition.PAGE);
			        watermark.setWrapType(WrapType.NONE);
			        watermark.setVerticalAlignment(VerticalAlignment.CENTER);
			        watermark.setHorizontalAlignment(HorizontalAlignment.CENTER);
			        // Create a new paragraph and append the watermark to this paragraph.
			        Paragraph watermarkPara = new Paragraph(aDoc);
			        watermarkPara.appendChild(watermark);
			        // Insert the watermark into all headers of each document section.
			        for (Section sect : aDoc.getSections()){
			            // There could be up to three different headers in each section, since we want the watermark to appear on all pages, insert into all headers.
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_PRIMARY);
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_FIRST);
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_EVEN);
			        }
				//
				OutputStream os = new ByteArrayOutputStream();
			        aDoc.save(os, SaveFormat.PDF);
			        
				if(os == null) throw new Exception("無法轉換成 Pdf 的輸出串流。");
				//
				InputStream is=new ByteArrayInputStream(((ByteArrayOutputStream)os).toByteArray());
				//防拷,防印
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				PdfReader reader = new PdfReader(is, null);
				PdfStamper stamper = new PdfStamper(reader, baos);
				stamper.setEncryption(passwd.getBytes(),
									passwd.getBytes(),
									PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING,
									PdfWriter.STANDARD_ENCRYPTION_128);
				stamper.close();
				reader.close();
				//存入 Liferay
				FileEntry fe = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(),
														themeDisplay.getScopeGroupId(),
														fdReq.getFolderId(),
														destName,
														"application/pdf",
														destName,
														"", 
														null,
														baos.toByteArray(), 
														new ServiceContext());
				String sql = "INSERT INTO req_files (req_id, file_id, file_name) VALUES (?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, reqId);
				ps.setLong(2, fe.getFileEntryId());
				ps.setString(3, pureName + ".pdf");
				ps.execute();
			}
			//
			String sql = "UPDATE doc_req SET done=1, approve_type=1, passwd = ? WHERE req_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, passwd);
			ps.setInt(2, reqId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private static void insertWatermarkIntoHeader(Paragraph watermarkPara, Section sect, int headerType) throws Exception{
	        HeaderFooter header = sect.getHeadersFooters().getByHeaderFooterType(headerType);
	        if (header == null){
	            // There is no header of the specified type in the current section, create it.
	            header = new HeaderFooter(sect.getDocument(), headerType);
	            sect.getHeadersFooters().add(header);
	        }
	        // Insert a clone of the watermark into the header.
	        header.appendChild(watermarkPara.deepClone(true));
	}	
	
	//產出文數字密碼
	private String genPasswd(int size){
		    String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    String ret = "";
		    int length = chars.length();
		    for (int i = 0; i < size; i ++){
		        ret += chars.split("")[ (int) (Math.random() * (length - 1)) ];
		    }
		    return ret;
	}
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = resRequest.getParameter("fileId");
			String fileName = resRequest.getParameter("fileName");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			resResponse.setContentType(alfDoc.getContentStreamMimeType());
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			//
			//System.out.println("pdf file name: " + fe.getTitle());
			
			InputStream is = alfDoc.getContentStream().getStream();
			byte[] buffer = new byte[4096];
			int n;
			while ((n = is.read(buffer)) > 0) {
			    out.write(buffer, 0, n);
			}
			//
			out.flush();
			out.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}
	}
	
	
}

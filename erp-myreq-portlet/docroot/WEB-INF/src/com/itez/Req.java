package com.itez;

import java.util.Vector;

public class Req {
	public int reqId = 0;
	public String reqBy = "";		//申請人
	public String reqDate = "";	//申請送出日期時間
	public String reqBrief = "";	//調閱資料描述
	public String reportNo = "";	//報告編號
	public String fileName = "";	
	public int done = 0;			//已回覆
	public String approveBy = "";
	public int approveType = 0;		//0.未審核 1.已核可 2.已核否
	public String approveTypeName = "";
	
	public Vector<ResFile> vecRes = new Vector<ResFile>();		//回覆的檔案
	public String passwd = "";		//開檔密碼
}

package com.itez;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.http.HttpHeaders;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyTasksPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			//
			//int showTaskDone = 1;
			//String sql = "SELECT show_task_done FROM user_prefer WHERE screen_name=?";
			//PreparedStatement ps = conn.prepareStatement(sql);
			//ps.setString(1, themeDisplay.getUser().getScreenName());
			//ResultSet rs = ps.executeQuery();
			//if(rs.next()){
			//	showTaskDone = rs.getInt(1);
			//}
			//rs.close();
			//
			Vector<String> vecReport = new Vector<String>();
			if(renderRequest.getPortletSession(true).getAttribute("vecReport") != null){
				vecReport = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecReport");
			}
			String reportNoBank = "";
			if(renderRequest.getPortletSession(true).getAttribute("currReportNoBank") != null){
				reportNoBank = renderRequest.getPortletSession(true).getAttribute("currReportNoBank").toString();
			}			
			//
			Vector<Task> vecMyTask = this.getMyInitialTasks(userFullName, conn, reportNoBank);
			for(Task t: vecMyTask){
				String key = t.reportNo + "_" + t.bankName;
				if(!vecReport.contains(key)){
					vecReport.add(key);
				}
			}
			renderRequest.getPortletSession(true).setAttribute("vecMyTask", vecMyTask);
			//
			Vector<Task> vecMyTaskDone = this.getMyDoneTasks(userFullName, conn, reportNoBank);
			for(Task t: vecMyTaskDone){
				String key = t.reportNo + "_" + t.bankName;
				if(!vecReport.contains(key)){
					vecReport.add(key);
				}
			}
			renderRequest.getPortletSession(true).setAttribute("vecMyTaskDone", vecMyTaskDone);
			//
			Collections.sort(vecReport);
			renderRequest.getPortletSession(true).setAttribute("vecReport", vecReport);
			//}
			//
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			//
			conn.close();
			//
			//renderRequest.getPortletSession(true).setAttribute("showTaskDone", showTaskDone);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//我發起或回應的任務(未完成)
	private Vector<Task> getMyInitialTasks(String userFullName, Connection conn, String reportNoBank) throws Exception{
		Vector<Task> vecMyTask = new Vector<Task>();
		//
		String sql = "SELECT * FROM todo_list WHERE issuer = ? AND (done IS NULL OR done = 0) AND canceled <> 1 ";
		if(reportNoBank != null && !reportNoBank.equals("") && !reportNoBank.equals("*")){
			String[] ss = reportNoBank.split("_");
			sql += " AND report_no='" + ss[0] + "'  AND bank_name = '" + ss[1] + "' ";
		}
		sql += " ORDER BY dead_line, file_name";
		
		//System.out.println(sql);
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("assignee") != null) t.assignee = rs.getString("assignee");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			t.received = rs.getInt("received");	//是否簽收
			//
			vecMyTask.add(t);
		}
		//
		return vecMyTask;
	}
	
	//我發起或回應的任務(已完成)
	private Vector<Task> getMyDoneTasks(String userFullName, Connection conn, String reportNoBank) throws Exception{
		Vector<Task> vecMyTaskDone = new Vector<Task>();
		//
		String sql = "SELECT * FROM todo_list WHERE assignee = ? AND done = 1 " ;
		if(reportNoBank != null && !reportNoBank.equals("") && !reportNoBank.equals("*")){
			String[] ss = reportNoBank.split("_");
			sql += " AND report_no='" + ss[0] + "'  AND bank_name = '" + ss[1] + "' ";
		}		
		sql += " ORDER BY dead_line, file_name";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("assignee") != null) t.assignee = rs.getString("assignee");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			//
			vecMyTaskDone.add(t);
		}
		//
		return vecMyTaskDone;
	}
	
	//以報告編號篩選
	public void doFilterByReport(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
	        	//
			String reportNoBank = actionRequest.getParameter("reportNoBank");
			if(reportNoBank.equals("") || reportNoBank.equals("*")){
				actionRequest.getPortletSession(true).removeAttribute("currReportNoBank");
			}else{
				actionRequest.getPortletSession(true).setAttribute("currReportNoBank", reportNoBank);	
			}
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	        }catch(Exception ex){
	        	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	
	//撤銷任務
	public void doCancelTask(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	conn = ds.getConnection();
	        	this.cancelTaskImpl(taskId, conn, themeDisplay, userFullName, actionRequest);
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}	        	
		}catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	private void cancelTaskImpl(int taskId, Connection conn, ThemeDisplay themeDisplay, String userFullName, ActionRequest actionRequest) throws Exception{
        	Task t = ErpUtil.getTask(taskId, conn);
        	//取得文件
        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
        	AlfrescoDocument alfDoc = null;
        	try{
        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
        	}catch(Exception _ex){
        	}
        	if(alfDoc == null){
        		try{
        			System.out.print("Closing task id: " + taskId + " Since file can not be found .....");
        			
        			ErpUtil.closeTask(taskId, userFullName, conn);
        			
        			System.out.println("Done");
        		}catch(Exception _ex){
        			_ex.printStackTrace();
        		}
        		//
        		return;
        	}
        	//if(alfDoc.getVersionLabel().compareTo(t.versionNo) > 0) throw new Exception("檔案：" + t.fileName + " 已被更新，請取得最新版本後再執行。");
        	if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + t.fileName + " 已被領出，無法變更。");
        	//取得最近版本
        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
        	try{
        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + alfDoc.getName());
        	}catch(Exception _ex){
        	}
        	//將文件狀態改成撤回
        	String updateMemo = "撤回重分";
        	if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
        		updateMemo = "撤回重填";
        	}
        	//Log
        	ErpUtil.logUserAction(updateMemo, ErpUtil.getFileFullName(t.fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
        	//
        	Map<String, String> props = new HashMap<String, String>();
		props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
		props.put("cm:author", userFullName);
		props.put("it:docStatus", ErpUtil.STATUS_CANCELED);
		
		ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), alfDoc.getContentStream().getStream());
		
        	ObjectId oid = alfDoc.checkOut();
		Document pwc = (Document)alfSessionAdmin.getObject(oid);
		oid = pwc.checkIn(false, props, cs, updateMemo);
		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(oid);
        	//發電子郵件通知受任人
		ErpUtil.cancelTask(t, 1, conn, themeDisplay);
		
		//撤回重填時, 將待辦事項恢復
		if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
			String fileIdPure = t.fileId;
			int _idx = fileIdPure.indexOf(";");
			if(_idx > 0){
				fileIdPure = fileIdPure.substring(0, _idx);
			}
			//
			String sql = "SELECT task_id FROM todo_list WHERE assignee=? " +
												" AND file_name=? " +
												//" AND version_no < ? " +
												" AND file_id LIKE '" + fileIdPure + "%' " +
												" AND completed_time IS NOT NULL " +
												" AND completed_time <> '' " +
											" ORDER BY version_no DESC";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userFullName);
			ps.setString(2, t.fileName);
			//ps.setString(3, versionNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				int taskIdOrig = rs.getInt(1);
				//
				sql = "UPDATE todo_list SET file_id = ?, " +				//1
									 " stage_folder_id = ?, " +			//2
									 " stage_name = ?, " +			//3
									 " version_no = ?, " +			//4
									 " done = 0, " +
									 " done_by='', " +
									 " completed_time = '' " +
							" WHERE task_id = ?";					//5
				ps = conn.prepareStatement(sql);
				ps.setString(1, alfDoc.getId());
				ps.setString(2, alfDoc.getParents().get(0).getId());
				ps.setString(3, alfDoc.getParents().get(0).getName());
				ps.setString(4, alfDoc.getVersionLabel());
				ps.setInt(5, taskIdOrig);
				ps.execute();
			}else{
				throw new Exception("找不到原任務(請填寫或請修改)：" + t.fileName + "。" );
			}
			rs.close();
		}
	}
		
	//批次撤回
	public void doBatchCancel(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
		try{		
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Enumeration<String> en = actionRequest.getParameterNames();
			if(en == null) return;
			//
			Vector<Task> vecBatch = new Vector<Task>();
			//
			conn = ds.getConnection();
			while(en.hasMoreElements()){
				String p = en.nextElement();
				if(p.startsWith("cb_")){
					try{
						String[] ss = p.split("_");
						int taskId = Integer.parseInt(ss[1]);
						Task t = ErpUtil.getTask(taskId, conn);
						if((t.taskType.equals(ErpUtil.TASK_FILL_REPORT) || t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT) || t.taskType.equals(ErpUtil.TASK_REVIEW)) 
								&& t.received == 0){
							vecBatch.add(t);
						}
					}catch(Exception _ex){
					}
				}
			}
			//
			if(vecBatch.size() == 0){
				throw new Exception("請勾選可撤回的任務。");
			}
			for(Task t: vecBatch){
				this.cancelTaskImpl(t.taskId, conn, themeDisplay, userFullName, actionRequest);
			}
			//
		        conn.close();
		}catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	

	//修改任務
	public void doAlterTask(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	conn = ds.getConnection();
	        	//
	        	Task t = ErpUtil.getTask(taskId, conn); 
	        	String dateDead = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearDead"), actionRequest.getParameter("monDead"), actionRequest.getParameter("dayDead"));
	        	//Log
	        	ErpUtil.logUserAction("修改任務", t.getFileFullName(), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//
	        	String sql = "UPDATE todo_list SET dead_line = ? WHERE task_id=?";
		        PreparedStatement ps = conn.prepareStatement(sql);
		        ps.setString(1, dateDead);
		        ps.setInt(2, taskId);
		        ps.execute();
	        	//
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}	        	
		}catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//取消請求審核之任務
	/*
	public void doCancelComplete(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	conn = ds.getConnection();
	        	Task t = ErpUtil.getTask(taskId, conn);
	        	//取得文件
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
	        	if(alfDoc == null) throw new Exception("找不到檔名：" + t.fileName + " 之實體檔案。");
	        	if(alfDoc.getVersionLabel().compareTo(t.versionNo) > 0) throw new Exception("檔案：" + t.fileName + " 已被更新，請取得最新版本後再執行。");
	        	if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + t.fileName + " 已被領出，無法變更。");
	        	//
	        	if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
	        		String sql = "UPDATE todo_list SET done=0 WHERE stage_folder_id=? AND file_id = ? AND assignee = ?";
	        		PreparedStatement ps = conn.prepareStatement(sql);
	        		ps.setString(1, t.stageFolderId);
	        		
	        		
	        		
	        		
	        		
	        		String updateMemo = "取消傳送";
		        	
		        	Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
				props.put("cm:author", userFullName);
				props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				
				ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), alfDoc.getContentStream().getStream());
				
		        	ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(pwc.checkIn(false, props, cs, updateMemo));
	        		//
		        	String sql = "UPDATE todo_list SET done = 1, canceled=1 WHERE task_id=?";
		        	PreparedStatement ps = conn.prepareStatement(sql);
		        	ps.setInt(1, taskId);
		        	ps.execute();
	        	}
	        	//將文件狀態改成撤回
	        	String updateMemo = "撤回";
	        	
	        	Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			props.put("it:docStatus", ErpUtil.STATUS_CANCELED);
			
			ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), alfDoc.getContentStream().getStream());
			
	        	ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			pwc.checkIn(false, props, cs, updateMemo);
	        	//
	        	conn.close();
		}catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	 */
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//String stageFolderId = resRequest.getParameter("stageFolderId");
			String fileId = resRequest.getParameter("fileId");
			//String stageFolderId = resRequest.getParameter("stageFolderId");
			//
			conn = ds.getConnection();
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//Log
			ErpUtil.logUserAction("下載任務檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
			//
			//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			CmisObject co = alfSessionAdmin.getObject(fileId);
			Document doc = (Document)co;
			//String fileName = doc.getName();
			//AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			//System.out.println("Mine type: " + doc.getContentStreamMimeType());
			resResponse.setContentType(doc.getContentStreamMimeType());
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (doc.getName(), "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			java.io.InputStream is =  doc.getContentStream().getStream();
			//
			if(is != null){
				byte[] buffer = new byte[4096];
				int n;
				while ((n = is.read(buffer)) > 0) {
				    out.write(buffer, 0, n);
				}
				//
				out.flush();
				out.close();
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//領出任務檔案
	/*
	public void doCheckOut(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			//
			Task t = null;
			Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
			for(Task _t: vecTask){
				if(_t.taskId == taskId){
					t = _t;
					break;
				}
			}
			//
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			if(fdOut == null) throw new Exception("領出暫存區目錄尚未設定。");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoDocument alfDoc2Out = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			ChkOutObj coo = ErpUtil.doCheckOut(alfSessionAdmin, fdExam, fdStage, alfDoc2Out, fdOut, themeDisplay);
			//
			conn = ds.getConnection();
			if(!coo.fileId.equals("")){
				String sql = "UPDATE todo_list SET file_id=?, " +		//1	
										"file_name=?, " +		//2
										"file_name_out=?, " +	//3
										"version_no=?, " +		//4
										"web_dav_url=? " +		//5
									" WHERE task_id=?";			//6
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, coo.fileId);
				ps.setString(2, coo.fileName);
				ps.setString(3, coo.fileNameOut);
				ps.setString(4, coo.versionNo);
				ps.setString(5, coo.webDavURL);
				ps.setInt(6, t.taskId);
				ps.execute();
			}
			conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

	//繳回
	public void doCheckIn(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			Task t = null;
			Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
			for(Task _t: vecTask){
				if(_t.taskId == taskId){
					t = _t;
					break;
				}
			}
			//	
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
			//領出暫存區
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			//
			alfDoc = ErpUtil.doCheckIn(alfSessionAdmin, fdStage, alfDoc, fdOut, themeDisplay);
			//
			conn = ds.getConnection();
			String sql = "UPDATE todo_list SET file_id=?, " +		//1	
										"file_name=?, " +		//2
										"file_name_out=?, " +	//3
										"version_no=?, " +		//4
										"web_dav_url=? " +		//5
									" WHERE task_id=?";			//6
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, alfDoc.getId());
			ps.setString(2, alfDoc.getName());
			ps.setString(3, "");
			ps.setString(4, alfDoc.getVersionLabel());
			ps.setString(5, "");
			ps.setInt(6, t.taskId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}	
	*/
	
	//檢查任務檔案是否還在 db 與 file Sync
	/*
	public void doSyncData(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			//boolean updated = false;
			@SuppressWarnings("unchecked")
			Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
			for(Task t: vecTask){
				AlfrescoDocument alfDoc = null;
				try{
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
				}catch(Exception _ex){
				}
				if(alfDoc == null){	//再找看看是否有新版本
					boolean found = false;
					//
					AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
					Iterator<CmisObject> it = fdStage.getChildren().iterator();
					while(it.hasNext()){
						AlfrescoDocument _alfDoc = (AlfrescoDocument)it.next();
						if(_alfDoc.getName().equals(t.fileName) && _alfDoc.getVersionLabel().compareTo(t.versionNo) > 0){
							//更新
							String sql = "UPDATE todo_list SET file_id=?, version_no=? WHERE task_id=?";
							PreparedStatement ps = conn.prepareStatement(sql);
							ps.setString(1, _alfDoc.getId());
							ps.setString(2, _alfDoc.getVersionLabel());
							ps.setInt(3, t.taskId);
							ps.execute();
							//
							found = true;
							break;
						}
					}
					if(!found && t.fileName.contains("Working Copy")){
						String fileNameOrig = t.fileName.replace(" (Working Copy)", "");
						it = fdStage.getChildren().iterator();
						while(it.hasNext()){
							AlfrescoDocument _alfDoc = (AlfrescoDocument)it.next();
							if(_alfDoc.getName().equals(fileNameOrig)){
								//更新
								String sql = "UPDATE todo_list SET file_id=?, version_no=? WHERE task_id=?";
								PreparedStatement ps = conn.prepareStatement(sql);
								ps.setString(1, _alfDoc.getId());
								ps.setString(2, _alfDoc.getVersionLabel());
								ps.setInt(3, t.taskId);
								ps.execute();
								//
								found = true;
								break;
							}
						}
					}
					//最後還是找不到
					if(!found){
						String sql = "DELETE FROM todo_list WHERE task_id=?";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.setInt(1, t.taskId);
						ps.execute();
					}
				}else if(alfDoc.getParents().size() == 0){		//已是舊版本找不到 Parent Folder
					//throw new Exception("檔案：" + alfDoc.getName() + " 找不到所在的作業階段。");
					String sql = "DELETE FROM todo_list WHERE task_id=?";
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.setInt(1, t.taskId);
					ps.execute();
				}
			}
	        	conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	*/
}

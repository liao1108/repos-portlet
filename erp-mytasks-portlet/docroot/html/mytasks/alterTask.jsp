<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 <%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            User user = themeDisplay.getUser();
            //
            int taskId = Integer.parseInt(renderRequest.getParameter("taskId"));
            //
            Vector<Task> vecMyTask = new Vector<Task>();
            if(renderRequest.getPortletSession(true).getAttribute("vecMyTask") != null){
            	vecMyTask = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecMyTask");
            }
            Task t = new Task();
            for(Task _t: vecMyTask){
            	if(_t.taskId == taskId ){
            		t = _t;
            		break;
            	}
            }
            //	
            String userFullName = "";
            if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
            	userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
            }

            //
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            String successMsg = "";	
            if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doAlterTaskURL" name="doAlterTask" >
</portlet:actionURL>

<portlet:renderURL var="goBackURL" >
	<portlet:param name="jspPage" value="/html/mytasks/view.jsp"/>
</portlet:renderURL>

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/alterTask.png">&nbsp;您可重新指定到期日期
	</aui:column>
</aui:layout>
<hr/>

<aui:fieldset >
<aui:form action="<%=doAlterTaskURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
			<aui:input type="hidden" name="taskId"  value="<%=taskId %>"/>
			<aui:input name="assignee" label="受任人" value="<%=t.assignee %>"  cssClass="td"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">									
			<h4>繳回期限</h4>
			<liferay-ui:input-date dayParam="dayDead"  monthParam="monDead"  yearParam="yearDead"
										 yearValue="<%=ErpUtil.getYear(t.deadLine) %>"  monthValue="<%=ErpUtil.getMonth(t.deadLine) - 1%>"  dayValue="<%=ErpUtil.getDay(t.deadLine) %>"
											yearRangeStart="<%=ErpUtil.getYear(t.deadLine)  - 1 %>" yearRangeEnd="<%=ErpUtil.getYear(t.deadLine) + 1%>"/>	
		</aui:column>	
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:input name="fileName" label="檔案名稱" value="<%=t.fileName %>"  cssClass="td" readonly="readonly" size="80"/>
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column>
			<aui:input name="taskType" label="任務類型" value="<%=t.taskType %>"  cssClass="td" readonly="readonly"/>
		</aui:column>
		<aui:column>
			<aui:input name="taskComment" label="任務摘要" value="<%=t.taskComment %>"  cssClass="td" readonly="readonly" size="55"/>
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column>
			<aui:input name="bankName" label="受檢單位" value="<%=t.bankName %>"  cssClass="td" readonly="readonly"/>
		</aui:column>
		<aui:column>
			<aui:input name="stageName" label="作業階段" value="<%=t.stageName %>"  cssClass="td" readonly="readonly"/>
		</aui:column>
	</aui:layout>

	<aui:button-row>
		<aui:button  onClick="<%=goBackURL.toString() %>"   value="返回上頁" />
		<aui:button type="submit"  value="確定修改" />
	</aui:button-row>
</aui:form>
</aui:fieldset>
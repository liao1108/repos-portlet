<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 
<portlet:defineObjects />
 
  <%
   		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
           	User user = themeDisplay.getUser();
           	//
           	 if(renderRequest.getAttribute("javax.servlet.forward.query_string") != null){
        	    	String currParam = renderRequest.getAttribute("javax.servlet.forward.query_string").toString();
        	    	renderRequest.getPortletSession(true).setAttribute("currParam", currParam);
            }else{
        	    	renderRequest.getPortletSession(true).removeAttribute("currParam");
            }
           	//
           	Vector<Task> vecMyTask = new Vector<Task>();
           	if(renderRequest.getPortletSession(true).getAttribute("vecMyTask") != null){
           		vecMyTask = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecMyTask");
           	}
		//int showTaskDone = 0; 
           	//if(renderRequest.getPortletSession(true).getAttribute("showTaskDone") != null){
           	//	showTaskDone = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("showTaskDone").toString());
           	//}
           	Vector<Task> vecMyTaskDone = new Vector<Task>();
           	if(renderRequest.getPortletSession(true).getAttribute("vecMyTaskDone") != null){
           		vecMyTaskDone = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecMyTaskDone");
           	}
		//包含的報告編號
            Vector<String> vecReport = new Vector<String>();
            if(renderRequest.getPortletSession(true).getAttribute("vecReport") != null){
              	vecReport = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecReport");
            }
          	//目前篩選的 report no
          	String currReportNoBank = "";
          	if(renderRequest.getPortletSession(true).getAttribute("currReportNoBank") != null){
          		currReportNoBank = renderRequest.getPortletSession(true).getAttribute("currReportNoBank").toString();
          	}
           	
           	int rowsPrefer = 20;
		if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
			rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
		}
            //
           	String userFullName = "";
           	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
           		userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
           	}

           	//
           	String errMsg = "";	
           	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
           	
           	String successMsg = "";	
           	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doSyncDataURL" name="doSyncData" >
</portlet:actionURL>

<portlet:actionURL var="doBatchCancelURL" name="doBatchCancel" >
</portlet:actionURL>

<portlet:actionURL var="doFilterByReportURL" name="doFilterByReport" >
</portlet:actionURL>

<script type="text/javascript">
	
	function doCheck(){
		var withCkecked = false;
		var chk = document.getElementsByTagName('input');
	    	var len = chk.length;
	      for (var i = 0; i < len; i++) {
	      	if (chk[i].type == 'checkbox') {
	            	if(chk[i].checked){
	            		withCkecked = true;
	            		break;
	            	} 
	        	}
	    	}      
	      //
		if(withCkecked){
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = false;
			        	}
		    	}
		}else{
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = true;
			        	}
		    	}
		}
	}
	
</script>

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/wip.png">&nbsp;您(<%=userFullName%>)發起的任務(尚未完成)
	</aui:column>
	<aui:column cssClass="td">以報告編號篩選：</aui:column>
	<aui:form action="<%=doFilterByReportURL.toString()%>"  method="post" >
		<aui:column cssClass="td">
			<aui:select name="reportNoBank" label="">
				<aui:option>*</aui:option>
				<%for(String reportNoBank: vecReport){ %> 
					<aui:option value="<%=reportNoBank%>"  selected="<%=reportNoBank.equals(currReportNoBank) %>"><%=reportNoBank %></aui:option>
				<%} %>
			</aui:select>
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="篩選" cssClass="button"/>
		</aui:column>
	</aui:form>
</aui:layout>
<hr/>

<aui:form action="<%=doBatchCancelURL%>" method="post" name="<portlet:namespace />fm">
<aui:layout>
	<aui:column>
		<a href="javascript:void();"  onClick="javascript:doCheck();">
			<img src='<%=renderRequest.getContextPath() + "/images/checked.png" %>'/>
		</a>
	</aui:column>

	<aui:column cssClass="td">&nbsp;&nbsp;</aui:column>

	<aui:column>
		<aui:button type="submit"  value="批次撤回"  cssClass="button"/>
	</aui:column>
	<aui:column cssClass="td">
		(僅處理尚未簽收的任務)
	</aui:column>	
</aui:layout>

<br>

<liferay-ui:search-container curParam="curTask"  emptyResultsMessage="沒有資料"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMyTask, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMyTask.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="myTask">
		<portlet:resourceURL var="downloadFile2URL" >
			<portlet:param name="stageFolderId"  value="<%=myTask.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=myTask.fileId %>"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=myTask.taskId%>"/>   
            </liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=myTask.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName" />
		<liferay-ui:search-container-column-text name="版本" property="versionNo"/>
		<liferay-ui:search-container-column-text name="受任人" property="assignee"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
</aui:form>
<hr>

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/done.png">&nbsp;您(<%=userFullName%>)發起的任務(已完成)
	</aui:column>
</aui:layout>
<hr/>
<br>
<liferay-ui:search-container curParam="curTaskDone" emptyResultsMessage="您沒有已完成的任務"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMyTaskDone, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMyTaskDone.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="myTaskDone">
		<portlet:resourceURL var="downloadFile2URL" >
			<portlet:param name="stageFolderId"  value="<%=myTaskDone.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=myTaskDone.fileId %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=myTaskDone.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName" />
		<liferay-ui:search-container-column-text name="版本" property="versionNo"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<% //liferay-ui:search-container-column-jsp path="/html/admin/adminDone.jsp" align="center" %>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

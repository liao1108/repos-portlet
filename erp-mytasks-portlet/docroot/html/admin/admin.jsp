<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Task" %>
<%@ page import="com.itez.ErpUtil" %>

<portlet:defineObjects />

<%
	boolean isLeader = false;
	if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
		isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	}
	
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Task t = (Task)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:resourceURL var="downloadFileURL" >
  		<portlet:param name="stageFolderId"  value="<%=t.stageFolderId %>"/>
  		<portlet:param name="fileId"  value="<%=t.fileId %>"/>
  		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:resourceURL>
	
	<portlet:actionURL var="doCancelTaskURL" name="doCancelTask" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:actionURL>

	<portlet:actionURL var="doCancelCompleteURL" name="doCancelComplete" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:actionURL>

	<portlet:renderURL var="goAlterTaskURL" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
		<portlet:param name="jspPage"  value="/html/mytasks/alterTask.jsp"/>
	</portlet:renderURL>

	<liferay-ui:icon  message="下載" url="<%=downloadFileURL.toString() %>"/>
	<%if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT) || t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){ %>
		<%if(t.received == 0){ %>
			<liferay-ui:icon message="撤回" url="<%=doCancelTaskURL.toString() %>"/>
		<%} %>	
		<liferay-ui:icon message="修改" url="<%=goAlterTaskURL.toString() %>"/>
	<%}else if(t.taskType.equals(ErpUtil.TASK_REVIEW) && t.received != 1){ %>
		<liferay-ui:icon message="撤回" url="<%=doCancelTaskURL.toString() %>"/>
	<%} %>
</liferay-ui:icon-menu>
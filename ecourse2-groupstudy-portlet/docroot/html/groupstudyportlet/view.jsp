<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="strHTML" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="datePicker1" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="datePicker2" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="strOrgOption" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="strCourseOption" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="strQuizPassOnly" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="strUnReadIncluded" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="selectedOrgId" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="selectedCourseId" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Navigator</title>
	
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery.treeview.css"/>
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery-ui-1.8.19.custom.css" />
	
	<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/jquery-ui-1.8.19.custom.min.js"></script>

	<script src="<%=renderRequest.getContextPath()%>/js/jquery.treeview.js" type="text/javascript"></script>
	
	<script>
		$(function() {
			$( "#datePicker1" ).datepicker();
			$( "#datePicker1" ).datepicker( "option", "dateFormat", "yy-mm-dd");

			$( "#datePicker2" ).datepicker();
			$( "#datePicker2" ).datepicker( "option", "dateFormat", "yy-mm-dd");
			
		});
	</script>

	<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/demo.js"></script>

</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<form id="myNaviForm" method="POST" action="<portlet:actionURL/>">
	<fieldset width = '100%'>
		<legend align='left'>篩選條件</legend>
		<table width="100%" cellpadding = "5" cellspacing = "5">
			<tr valign="middle">
				<td>組織單位：
					<select name="orgId">
						<%=strOrgOption%>
					</select>
				</td>
				<td>課程主題或單元：
					<select name="courseId">
						<%=strCourseOption%>
					</select>
				</td>
				<td>通過檢測日期：
					<input type="text" name="datePicker1" id="datePicker1" size="9" style="height:9px" value="<%=datePicker1%>"/>&nbsp;到&nbsp;
					<input type="text"  name="datePicker2" id="datePicker2" size="9" style="height:9px" value="<%=datePicker2%>"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="CHECKBOX" name="quizPassOnly" <%=strQuizPassOnly%>>只列通過檢測者
				</td>	

				<td>
					<input type="CHECKBOX" name="unReadIncluded" <%=strUnReadIncluded%>>含未閱讀者
				</td>	
				
				<td>
					<input type="submit" value="開始查詢">
				</td>
			</tr>
		</table>
	</fieldset>
	<p/>
	<%if(!strHTML.equals("")){%>
		<p align="right">
		<a href = '<portlet:resourceURL><portlet:param name="datePicker1" value="<%=datePicker1%>"/><portlet:param name="datePicker2" value="<%=datePicker2%>"/><portlet:param name="orgId" value="<%=selectedOrgId%>"/><portlet:param name="courseId" value="<%=selectedCourseId%>"/><portlet:param name="quizPassOnly" value="<%=strQuizPassOnly%>"/></portlet:resourceURL>'">匯出Excel</a>
		</p>
	<%}%>
		
	<%=strHTML%>		
	</form>
</body>
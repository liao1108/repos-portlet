package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.OrganizationServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.tomcat.jdbc.pool.DataSource;

/**
 * Portlet implementation class GroupStudyPortlet
 */
public class GroupStudyPortlet extends GenericPortlet {
	String eCourseRootFoler = "";	//課程根目錄
	ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	long groupId = -1;
	
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	    	//
	    	eCourseRootFoler = getInitParameter("ecourse-root-folder");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
   		//
   		String datePicker1 = "", datePicker2 = "";
   		String strOrgOption = "";
   		String strCourseOption = "";
   		String strHTML = "";
   		long selectedOrgId = -1;
   		long selectedCourseId = -1;
   		String strQuizPassOnly = "";
   		String strUnReadIncluded = "";
    	try{
	    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		//long companyId = themeDisplay.getCompanyId();
	   		groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//
	   		//取得登入者的機構名稱
	   		List<Organization> listOrgs = OrganizationServiceUtil.getUserOrganizations(userId);
	   		//Organization myOrg = this.getDefaultOrg(userId);
			if(listOrgs.size() == 0){
				throw new Exception("您尚未歸屬於任何組織 !");
			//}else if(myOrg.getSuborganizationsSize() == 0){
			//	throw new Exception("您沒有管理任何組織 !");
			}
			//
			if(renderRequest.getAttribute("datePicker1") != null){
				datePicker1 = renderRequest.getAttribute("datePicker1").toString();
			}
			if(renderRequest.getAttribute("datePicker2") != null){
				datePicker2 = renderRequest.getAttribute("datePicker2").toString();
			}
			
			if(renderRequest.getAttribute("orgId") != null){
				try{
					selectedOrgId = Long.parseLong(renderRequest.getAttribute("orgId").toString());
				}catch(Exception ignore){
					
				}
			}
			
			if(renderRequest.getAttribute("courseId") != null){
				try{
					selectedCourseId = Long.parseLong(renderRequest.getAttribute("courseId").toString());
				}catch(Exception ignore){
					
				}
			}
			
			if(renderRequest.getAttribute("quizPassOnly") != null){
				strQuizPassOnly = renderRequest.getAttribute("quizPassOnly").toString();
			}
			
			if(renderRequest.getAttribute("unReadIncluded") != null){
				strUnReadIncluded = renderRequest.getAttribute("unReadIncluded").toString();
			}
			
			//組織選項
			strOrgOption = makeUpOrgOption(renderRequest, listOrgs, 0, selectedOrgId);
			//
			List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			if(fd.getName().equals(eCourseRootFoler)){
	   				long allOptionId = fd.getFolderId() * 1111111;
	   				String strSelected = "";
	   				if(allOptionId == selectedCourseId){
	   					strSelected = "SELECTED";
	   				}
	   				strCourseOption = "<OPTION value='" + allOptionId  + "'" + " " + strSelected + ">" + "全部課程" + "</OPTION>";
	   				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
	   				for(Folder fd2: list2){			//主題
	   					long subjectOptionId = fd2.getFolderId() * 1000000;
	   					strSelected = "";
	   					if(subjectOptionId == selectedCourseId){
	   						strSelected = "SELECTED";
	   					}
	   					strCourseOption += "<OPTION value='" + subjectOptionId + "'" + " " + strSelected + ">" + fd2.getName() + "</OPTION>";
	   					//單元
	   					List<Folder> list3 = DLAppServiceUtil.getFolders(groupId, fd2.getFolderId());
	   					for(Folder fd3: list3){		//單元
	   						strSelected = "";
	   						if(selectedCourseId == fd3.getFolderId()){
	   							strSelected = "SELECTED";
	   						}
	   						strCourseOption += "<OPTION value='" + fd3.getFolderId() + "'" + " " + strSelected + ">" + "&nbsp;&nbsp;&nbsp;&nbsp;" + fd3.getName() + "</OPTION>";
	   					}
	   				}
	   			}
	   		}
	   		
	   		if(renderRequest.getAttribute("strHTML") != null){
				strHTML = renderRequest.getAttribute("strHTML").toString();
			}

	   		if(renderRequest.getAttribute("errMsg") != null){
	   			errMsg = renderRequest.getAttribute("errMsg").toString();
			}
    	}catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }
        //System.out.println(topicTree);
        renderRequest.setAttribute("datePicker1", datePicker1);
        renderRequest.setAttribute("datePicker2", datePicker2);
        //
        /*
        if(!datePicker1.equals("") || !datePicker2.equals("") || orgIdFilter >= 0){
        	boolean withDate = false;
        	String strFilterTerm = "以下資料之篩選條件為: ";
        	if(!datePicker1.equals("") && !datePicker2.equals("")){
        		strFilterTerm += "自 " + datePicker1 + " 起至 " + datePicker2 + " 止完成檢測者;";
        		withDate = true;
        	}else if(!datePicker1.equals("")){
        		strFilterTerm += "自 " + datePicker1 + " 起完成檢測者;";
        		withDate = true;
        	}else if(!datePicker2.equals("")){
        		strFilterTerm += "截至 " + datePicker2 + " 完成檢測者;";
        		withDate = true;
        	}
        	if(orgIdFilter >= 0 && !orgNameFilter.equals("")){
        		if(withDate) strFilterTerm += "且";
        		strFilterTerm += "組織單位為" + orgNameFilter + "。"; 
        	}
        	renderRequest.setAttribute("filterTerm", strFilterTerm);
        }
        */
        
		//renderRequest.setAttribute("tree", tree);
        renderRequest.setAttribute("strOrgOption", strOrgOption);
        renderRequest.setAttribute("strCourseOption", strCourseOption);
        renderRequest.setAttribute("strQuizPassOnly", strQuizPassOnly);
        renderRequest.setAttribute("strUnReadIncluded", strUnReadIncluded);
        renderRequest.setAttribute("strHTML", strHTML);
   		renderRequest.setAttribute("errMsg", errMsg);
   		renderRequest.setAttribute("selectedOrgId", String.valueOf(selectedOrgId));
   		renderRequest.setAttribute("selectedCourseId", String.valueOf(selectedCourseId));
    	//
        include(viewJSP, renderRequest, renderResponse);
    }

    private String makeUpOrgOption(RenderRequest renderRequest,
									List<Organization> listOrgs,
									int level,
									long selectedOrgId) throws Exception{
    	String ret = "";
    	//
    	for(Organization org: listOrgs){
	    	String strSelected = "";
	    	//if(level == 0){
	    		if(org.getOrganizationId() == selectedOrgId){
	    			strSelected = "SELECTED";
	    		}
	    		//ret += "<OPTION value='" + org.getOrganizationId() + "'" + " " + strSelected + ">" + org.getName() + "</OPTION>";
	    	//}
	    	//
			String lead = "";
			for(int i=0; i < level; i++){
				lead += "&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			strSelected = "";
			if(org.getOrganizationId() == selectedOrgId){
				strSelected = "SELECTED";
			}
			ret += "<OPTION value='" + org.getOrganizationId() + "'" + " " + strSelected + ">" + (lead + org.getName()) + "</OPTION>";
			//如果有子組織, 則往下繼續展開
	    	if(org.hasSuborganizations()){
	    		ret += makeUpOrgOption(renderRequest, org.getSuborganizations(), level+1, selectedOrgId);
	    	}
	    	/*
	    	List<Organization> subOrgs = org.getSuborganizations();
			for(Organization o: subOrgs){
				String lead = "";
				for(int i=0; i < level; i++){
					lead += "&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				strSelected = "";
				if(o.getOrganizationId() == selectedOrgId){
					strSelected = "SELECTED";
				}
				ret += "<OPTION value='" + o.getOrganizationId() + "'" + " " + strSelected + ">" + (lead + o.getName()) + "</OPTION>";
				//
				if(o.getSuborganizationsSize() > 0){
					ret += makeUpOrgOption(renderRequest, o, level+1, selectedOrgId);
				}
			}
			*/
    	}
    	return ret;
    }
    
    private Hashtable<String, Vector<User>> getOrgUsers(Long orgId, String parentOrgName) throws Exception{
    	Hashtable<String, Vector<User>> htRet = new Hashtable<String, Vector<User>>();
    	//if(orgId != null && OrganizationServiceUtil.getOrganization(orgId) != null){
	    	Organization currOrg = OrganizationLocalServiceUtil.getOrganization(orgId);
	    	//
	    	List<Organization> listSubOrgs = currOrg.getSuborganizations();
	    	if(listSubOrgs.size() > 0){
	    		for(Organization o: listSubOrgs){
	    			Hashtable<String, Vector<User>> htRet2 = getOrgUsers(o.getOrganizationId(), currOrg.getName());
	    			Enumeration<String> en = htRet2.keys();
	    			while(en.hasMoreElements()){
	    				String key = en.nextElement();
	    				htRet.put(key, htRet2.get(key));
	    			}
	    		}
	    	}else{
	    		String key = currOrg.getName();
	    		if(!parentOrgName.equals("")) key = (parentOrgName + "/" + key);
	    		//
	    		Vector<User> vecUser = new Vector<User>();
	    		if(htRet.containsKey(key)){
	    			vecUser = htRet.get(key);
	    		}
	    		//
	    		List<User> listUsers = UserLocalServiceUtil.getOrganizationUsers(orgId);
	    		for(User u: listUsers){
	    			if(u.isActive()){
	    				vecUser.add(u);
	    			}
	    		}
	    		//
	    		if(vecUser.size() > 0){
	    			htRet.put(key, vecUser);
	    		}
	    	}
    	//}
    	return htRet;
    }
    
    //private Organization getDefaultOrg(long userId) throws Exception{
    //	Organization org = null;
    //	List<Organization> orgs = OrganizationServiceUtil.getUserOrganizations(userId);
    //	if(orgs.size() > 0){
    //		org = orgs.get(0);
    //	}
    //	return org;
    //}
    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
		String datePicker1 = "", datePicker2 = "", errMsg="";
		long orgId = 0, courseId = 0;
		boolean quizPassOnly = false;
		boolean unReadIncluded = false;
		String strHTML = "";
		//
		try{
	    	if(actionRequest.getParameter("datePicker1") != null){
	    		datePicker1 = actionRequest.getParameter("datePicker1");
	    	}
			
			if(actionRequest.getParameter("datePicker2") != null){
				datePicker2 = actionRequest.getParameter("datePicker2");
	    	}
			
			if(actionRequest.getParameter("orgId") != null){
				orgId = Long.parseLong(actionRequest.getParameter("orgId"));
	    	}
			
			if(actionRequest.getParameter("courseId") != null){
				courseId = Long.parseLong(actionRequest.getParameter("courseId"));
	    	}
			
			if(actionRequest.getParameter("quizPassOnly") != null){
				quizPassOnly = true;
	    	}
			
			if(actionRequest.getParameter("unReadIncluded") != null){
				unReadIncluded = true;
	    	}
			//
			if(orgId > 0 && courseId > 0){
				strHTML = this.genReport(orgId, courseId, datePicker1, datePicker2, quizPassOnly, unReadIncluded, null).toString(); 
			}else{
				throw new Exception("未指定單位代碼或課程代碼 !");
			}
    	}catch (Exception ex){
    		ex.printStackTrace();
    		
    		if(errMsg.equals("")) errMsg += "\n";
    		errMsg +=  utils.getItezErrorCode(ex);
    	}
    	//
    	actionRequest.setAttribute("errMsg", errMsg);
    	actionRequest.setAttribute("datePicker1", datePicker1);
		actionRequest.setAttribute("datePicker2", datePicker2);
		actionRequest.setAttribute("orgId", String.valueOf(orgId));
		actionRequest.setAttribute("courseId", String.valueOf(courseId));
		if(quizPassOnly){
			actionRequest.setAttribute("quizPassOnly", "CHECKED");
		}
		if(unReadIncluded){
			actionRequest.setAttribute("unReadIncluded", "CHECKED");
		}
		actionRequest.setAttribute("strHTML", strHTML);
		//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}
    
    
    private Object genReport(long orgId, long courseId, String datePicker1, String datePicker2, boolean quizPassOnly, boolean unReadIncluded, WritableSheet ws) throws Exception{
    	String strHTML = "";
    	//
    	Connection conn = ds.getConnection();
		//搜尋入列的課程
		Vector<Long> vecTopic = new Vector<Long>();
		if(courseId%1111111 == 0){				//選項為全部主題
			List<Folder> list = DLAppServiceUtil.getFolders(groupId, courseId/1111111);
			for(Folder fd: list){
				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
				for(Folder fd2: list2){
					vecTopic.add(fd2.getFolderId());
				}
			}
		}else if(courseId%1000000 == 0){		//選項為主題
			List<Folder> list = DLAppServiceUtil.getFolders(groupId, courseId/1000000);
			for(Folder fd: list){
				vecTopic.add(fd.getFolderId());
			}
		}else{
			vecTopic.add(courseId);
		}
		//搜尋入列的用戶
		Hashtable<String, Vector<User>> htOrgUser = this.getOrgUsers(orgId, "");
		//
		if(ws == null){
			strHTML = "<table width='100%' border='1'>\n";
			strHTML += "<tr>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "組織單位" + "</td>\n"; 	
			strHTML += "<td align='center' style='background-color:lightblue'>" + "成員" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "課程單元" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "開始閱讀時間" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "檢測次數" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "答對比例" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "檢測通過時間" + "</td>\n";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "換算學習時數" + "</td>\n";
			strHTML += "</tr>\n";
		}else{
			Label label = new Label(0, 0, "組織單位");
			ws.addCell(label);
			label = new Label(1, 0, "成員");
			ws.addCell(label);
			label = new Label(2, 0, "課程單元");
			ws.addCell(label);
			label = new Label(3, 0, "開始閱讀時間");
			ws.addCell(label);
			label = new Label(4, 0, "檢測次數");
			ws.addCell(label);
			label = new Label(5, 0, "答對比例");
			ws.addCell(label);
			label = new Label(6, 0, "檢測通過時間");
			ws.addCell(label);
			label = new Label(7, 0, "換算學習時數");
			ws.addCell(label);
		}
		int rowIndex = 0;
		//
		Enumeration<String> enOrg = htOrgUser.keys();
		while(enOrg.hasMoreElements()){
			String keyOrg = enOrg.nextElement();
			Vector<User> vecUser = htOrgUser.get(keyOrg);
			for(User u: vecUser){
				Vector<Rec> vecRec = new Vector<Rec>();
	    		for(Long topicId: vecTopic){
    				Rec rec = new Rec();
    				rec.topic = DLAppServiceUtil.getFolder(topicId).getParentFolder().getName() + "/" + DLAppServiceUtil.getFolder(topicId).getName();
    				
	    			//查詢開始閱讀時間
    				String sql = "SELECT start_time FROM trace_reading WHERE u_id=? AND topic_uuid=? ORDER BY start_time";
    				PreparedStatement ps = conn.prepareStatement(sql);
    				ps.setLong(1, u.getUserId());
        			ps.setLong(2, topicId);
        			ResultSet rs = ps.executeQuery();
        			if(rs.next()){
        				rec.startReadTime = rs.getString(1);
        			}
        			//
        			if(unReadIncluded || (!unReadIncluded && !rec.startReadTime.equals(""))){
	        			boolean enRoll = false;
	        			int completed = 0;
	    				//檢查本成員本單元是否檢測完成
	    				sql = "SELECT MAX(completed), MAX(quiz_end_time), COUNT(quiz_end_time), MAX(tot_score), SUM(got_score) FROM trace_quiz WHERE u_id=? AND topic_uuid=? ";
		    			if(!datePicker1.equals("")){
		    				sql += " AND quiz_end_time >= '" + (datePicker1 + " " + "00:00:00") + "' ";
		    			}
		    			if(!datePicker2.equals("")){
		    				sql += " AND quiz_end_time <= '" + (datePicker2 + " " + "23:59:59") + "' ";
		    			}
		    			//if(quizPassOnly){
		    			//	sql += " AND completed = 1";
		    			//}
	    				ps = conn.prepareStatement(sql);
	    				ps.setLong(1, u.getUserId());
		    			ps.setLong(2, topicId);
		    			rs = ps.executeQuery();
		    			if(rs.next()){	
		    				completed = rs.getInt(1);
		    				if(rs.getInt(1) == 1 && rs.getString(2) != null && !rs.getString(2).equals("")){
		    					rec.passQuizTime = rs.getString(2);
		    				}
		    				rec.quizTimes = rs.getInt(3);
		    				rec.totScore = rs.getInt(4);
		    				rec.gotScore = rs.getInt(5);
		    				//
		    				if(!rec.passQuizTime.equals("")){	//查詢本單元訂定的累計學習時數	
			        			double gotStudyHour = 0;
			        			sql = "SELECT study_hours_get FROM topic_setting WHERE topic_id = ?";
			        			ps = conn.prepareStatement(sql);
			        			ps.setLong(1, topicId);
			        			rs = ps.executeQuery();
			        			if(rs.next()){
			        				gotStudyHour = rs.getDouble(1);
			        			}
			        			rec.studyHourGet = gotStudyHour;
			    			}
		    				enRoll = true;
		    			}
		    			//
		    			if(enRoll){
		    				if(quizPassOnly){
		    					if(completed == 1){
		    						vecRec.add(rec);
		    					}
		    				}else{
		    					vecRec.add(rec);
		    				}
		    			}
        			}
	    		}
	    		//畫HTML
	    		int c = 0;
	    		for(Rec rec: vecRec){
	    			if(ws == null){
		    			strHTML += "<tr>\n";
		    			if(c==0){
		    				strHTML += "<td align='left' rowspan='" + vecRec.size() + "'>" + keyOrg + "</td>\n";
		    				strHTML += "<td align='center' rowspan='" + vecRec.size() + "'>" + u.getFullName() + "</td>\n";
		    			}
		    			strHTML += "<td align='left'>" + rec.topic + "</td>\n";
		    			strHTML += "<td align='center'>" + rec.startReadTime + "</td>\n";
		    			strHTML += "<td align='center'>" + rec.quizTimes + "</td>\n";
		    			strHTML += "<td align='center'>" + (rec.gotScore/10) + "/" + (rec.totScore/10) + "</td>\n";
		    			strHTML += "<td align='center'>" + rec.passQuizTime + "</td>\n";
		    			strHTML += "<td align='center'>" + rec.studyHourGet + "</td>\n";
		    			strHTML += "</tr>\n";
	    			}else{
	    				rowIndex++;
	    				if(c==0){
	    					ws.mergeCells(0,rowIndex,0,rowIndex + vecRec.size() - 1);
	    					Label label = new Label(0, rowIndex, keyOrg);
	    					ws.addCell(label);

	    					ws.mergeCells(1,rowIndex,1,rowIndex + vecRec.size() - 1);
	    					label = new Label(1, rowIndex, u.getFullName());
	    					ws.addCell(label);
	    				}
	    				
	    				Label label = new Label(2, rowIndex, rec.topic);
	    				ws.addCell(label);
	    				label = new Label(3, rowIndex, rec.startReadTime);
	    				ws.addCell(label);
	    				label = new Label(4, rowIndex, String.valueOf(rec.quizTimes));
	    				ws.addCell(label);
	    				label = new Label(5, rowIndex, (rec.gotScore/10) + "/" + (rec.totScore/10));
	    				ws.addCell(label);
	    				label = new Label(6, rowIndex, rec.passQuizTime);
	    				ws.addCell(label);
	    				label = new Label(7, rowIndex, String.valueOf(rec.studyHourGet));
	    				ws.addCell(label);
	    			}
	    			c++;
	    		}
			}
    	}
		//
		strHTML += "</table>\n";
		//
		if(ws == null){
			return strHTML;
		}else{
			return ws;
		}
    }
    
    public void serveResource(ResourceRequest req, ResourceResponse res) throws PortletException, IOException {
		String datePicker1 = ParamUtil.getString(req, "datePicker1");
		String datePicker2 = ParamUtil.getString(req, "datePicker2");
		long orgId = Long.parseLong(ParamUtil.getString(req, "orgId"));
		long courseId = Long.parseLong(ParamUtil.getString(req, "courseId"));
		boolean quizPassOnly = false;
		boolean unReadIncluded = false;
		if(ParamUtil.getString(req, "quizPassOnly").equalsIgnoreCase("CHECKED")){
			quizPassOnly = true;
		}
		if(ParamUtil.getString(req, "unReadIncluded").equalsIgnoreCase("CHECKED")){
			unReadIncluded = true;
		}
		//
		try{
			res.setContentType("application/vnd.ms-excel");
		    res.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			OutputStream out = res.getPortletOutputStream();
			//
			WritableWorkbook wwb = Workbook.createWorkbook(out);				
			WritableSheet ws = wwb.createSheet("群組學習成果", 0);
			//
			ws = (WritableSheet)this.genReport(orgId, courseId, datePicker1, datePicker2, quizPassOnly, unReadIncluded, ws);
			//
			wwb.write();
			wwb.close();
			//
			out.flush();
			out.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(GroupStudyPortlet.class);

}

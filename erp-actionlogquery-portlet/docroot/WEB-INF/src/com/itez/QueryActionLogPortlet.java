package com.itez;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.apache.http.HttpHeaders;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class QueryActionLogPortlet
 */
public class QueryActionLogPortlet extends MVCPortlet {
	DataSource ds = null;
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void doQuery(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			Params p = new Params();
			//
			p.userNo = actionRequest.getParameter("userNo");
			if(actionRequest.getParameter("date1") != null){
				p.date1 = actionRequest.getParameter("date1");
			}
			if(actionRequest.getParameter("date2") != null){
				p.date2 = actionRequest.getParameter("date2");
			}
			if(actionRequest.getParameter("funcName") != null){
				p.funcName = actionRequest.getParameter("funcName");
			}
			//先存入 Session
			actionRequest.getPortletSession(true).setAttribute("params", p);
			//驗證
			if(p.date1.equals("") || p.date2.equals("")){
				throw new Exception("請輸入查詢日期區間。");
			}
			//utils.validateCDate(p.date1);
			//utils.validateCDate(p.date2);
			//
			String date1 = p.date1 + " " + "00:00:00:000";
			String date2 = p.date2 + " " + "23:59:59:999";
			//
			Vector<Row> vecRow = new Vector<Row>();
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM action_log WHERE action_time >= ? AND action_time <= ? ";
			if(!p.userNo.equals("")){
				sql += " AND screen_name='" + p.userNo + "'";
			}
			if(!p.funcName.equals("")){
				sql += " AND action_name LIKE '%" + p.funcName + "%'";
			}
			sql += " ORDER BY action_time DESC";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, date1);
			ps.setString(2, date2);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Row r = new Row();
				r.userNo = rs.getString("screen_name");
				try{
					r.userName = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), r.userNo).getFirstName();
				}catch(Exception _ex){
				}
				r.actionTime = rs.getString("action_time");
				r.actionName = rs.getString("action_name");
				r.fromIp = rs.getString("from_ip");
				if(rs.getString("target_file_name") != null){
					r.targetFileName = rs.getString("target_file_name");
				}
				vecRow.add(r);
			}
			rs.close();
			conn.close();
			//
			actionRequest.getPortletSession(true).setAttribute("vecRow", vecRow); 
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//@SuppressWarnings("deprecation")
	//@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			User user = themeDisplay.getUser();
			//
			Params p = (Params)resRequest.getPortletSession(true).getAttribute("params");
			Vector<Row> vecRow = (Vector<Row>)resRequest.getPortletSession(true).getAttribute("vecRow"); 
			//
			String strTerm = "查詢條件：使用日期 " + p.date1 + " ~ " + p.date2;
			if(!p.funcName.equals("")){
				strTerm += " " + "功能名稱：" + p.funcName;
			}
			//
			resResponse.setContentType("application/vnd.ms-excel");
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode ("使用記錄.xls", "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			//
			java.io.InputStream is = resRequest.getPortletSession().getPortletContext().getResourceAsStream("使用記錄.xls");
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0);
			//列印人
			HSSFRow row = sheet.getRow(1);
			//row.createCell(2);
			HSSFCell cell = row.getCell(2);
			cell.setCellValue(userFullName);
			//查詢條件
			cell = row.getCell(3);
			cell.setCellValue(strTerm);
			//
			int idx = 1;
			int rowInsertAt = 3;
			for(Row r: vecRow){
				sheet.shiftRows(rowInsertAt, sheet.getLastRowNum(), 1, true, false);
				sheet.createRow(rowInsertAt);
				row = sheet.getRow(rowInsertAt);
				row.setHeight((short)-1);			//自動放高
				//
				HSSFRow origRow = sheet.getRow(rowInsertAt+1);
				for(int i=0; i < origRow.getLastCellNum(); i++){
					row.createCell(i);
					row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
				}
				//0
				cell = row.getCell(0);
				cell.setCellValue(idx);
				//1
				cell = row.getCell(1);
				cell.setCellValue(r.userNo);
				//2
				cell = row.getCell(2);
				cell.setCellValue(r.userName);
				//3
				cell = row.getCell(3);
				cell.setCellValue(r.actionTime);
				//4
				cell = row.getCell(4);
				cell.setCellValue(r.actionName);
				//5
				cell = row.getCell(5);
				cell.setCellValue(r.targetFileName);
				//6
				cell = row.getCell(6);
				cell.setCellValue(r.fromIp);
				//
				rowInsertAt++;
				idx++;
			}
			wb.write(out);
			//
			out.flush();
			out.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}
	}
}

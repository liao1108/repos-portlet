<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Row" %>
<%@ page import="com.itez.Params" %>

<portlet:defineObjects />

<%
	Params p = new Params();
	if(renderRequest.getPortletSession(true).getAttribute("params") != null){
		p = (Params)renderRequest.getPortletSession(true).getAttribute("params");
	}
	
	Vector<Row> vecRow = new Vector<Row>();
	if(renderRequest.getPortletSession(true).getAttribute("vecRow") != null){
		vecRow = (Vector<Row>)renderRequest.getPortletSession(true).getAttribute("vecRow");
	}
	
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doQueryURL" name="doQuery">
</portlet:actionURL>

<portlet:resourceURL var="doExportURL" >
</portlet:resourceURL>

<aui:form action="<%=doQueryURL%>" method="post" name="<portlet:namespace />fm">
<aui:layout>
 	<aui:column>
          <aui:input type="text"  name="userNo"  label="使用者代碼"  value="<%=p.userNo%>" />
       </aui:column>

 	<aui:column>
          <aui:input type="text"  name="date1" label="起始日(yyyy-MM-dd)"  value="<%=p.date1%>" />
       </aui:column>
 
       <aui:column>
          <aui:input type="text"  name="date2" label="截止日(yyyy-MM-dd)"  value="<%=p.date2%>" />
       </aui:column>
       
       <aui:column>
          <aui:input type="text"  name="funcName" label="功能摘要"  value="<%=p.funcName%>" />
       </aui:column>  
</aui:layout>
 <aui:button-row>      
		<aui:button type="SUBMIT"  value="開始查詢" cssClass="button"/>
</aui:button-row>
</aui:form>
<hr>

<aui:form action="<%=doExportURL%>" method="post" name="<portlet:namespace />fm">
<%if(vecRow.size() > 0){ %>
	<aui:layout>
		<aui:column>
			<aui:button  type="submit"   value="匯出" cssClass="button"/>
		</aui:column>
	</aui:layout>
<%} %>
<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecRow, searchContainer.getStart(), searchContainer.getEnd());
			total = vecRow.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.Row" keyProperty="path" modelVar="Row">
		<liferay-ui:search-container-column-text name="登入帳號" property="userNo"/>
		<liferay-ui:search-container-column-text name="使用者名稱" property="userName"/>
		<liferay-ui:search-container-column-text name="作業時間" property="actionTime"/>
		<liferay-ui:search-container-column-text name="功能摘要" property="actionName"/>
		<liferay-ui:search-container-column-text name="標的目錄或檔案" property="targetFileName"/>
		<liferay-ui:search-container-column-text name="執行位址" property="fromIp"/>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
</aui:form>
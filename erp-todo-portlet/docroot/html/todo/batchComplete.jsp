<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 
<portlet:defineObjects />
 
  <%
         	String errMsg = "";	
         	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
         	
         	String successMsg = "";	
         	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/todo/view.jsp"/>
</portlet:renderURL>	


<portlet:actionURL var="batchCompleteFileURL"  name="batchCompleteFile" >
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="th">
	 	<img src="<%=renderRequest.getContextPath()%>/images/batchUpload.png">&nbsp;批次交付審核之檔案(只處理與任務同名之檔案)
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:form action="<%=batchCompleteFileURL%>"  enctype="multipart/form-data"  method="post" >
		<aui:layout>
			<aui:column cssClass="td">
				<aui:input type="text" name="completeMemo" label="附註說明" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName0"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName1"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName2"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName3"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName4"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName5"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName6"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName7"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName8"  label="" size="100"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName9"  label="" size="100"/>
			</aui:column>
		</aui:layout>
	
		<aui:button-row>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" cssClass="button"/>
			<aui:button type="submit" value="確定交付" cssClass="button"/>
		</aui:button-row>
</aui:form>	

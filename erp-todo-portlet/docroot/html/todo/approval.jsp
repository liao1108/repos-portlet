<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   		String taskId = "", fileNameOrig = "", taskComment = "", fileNameUpload="", approveType="", replyDueDate="";
   		if(renderRequest.getParameter("taskId") != null){
   			taskId = renderRequest.getParameter("taskId");
   		}
   		if(renderRequest.getParameter("fileNameOrig") != null){
   			fileNameOrig = renderRequest.getParameter("fileNameOrig");
   		}
   		if(renderRequest.getParameter("taskComment") != null){
   			taskComment = renderRequest.getParameter("taskComment");
   		}
   		if(renderRequest.getParameter("fileNameUpload") != null){
   			fileNameUpload = renderRequest.getParameter("fileNameUpload");
   		}
   		if(renderRequest.getParameter("approveType") != null){
   			approveType = renderRequest.getParameter("approveType");
   		}
   		if(renderRequest.getParameter("replyDueDate") != null){
   			replyDueDate = renderRequest.getParameter("replyDueDate");
   		}
   		//
		if(taskId.equals("")){
			if(renderRequest.getPortletSession(true).getAttribute("currTask") != null){
				Task t = (Task)renderRequest.getPortletSession(true).getAttribute("currTask");
				taskId = String.valueOf(t.taskId);
				fileNameOrig = t.fileName;
				taskComment = t.taskComment;
			}
		}
   		//
   		if(replyDueDate.equals("")){
   			replyDueDate = ErpUtil.getCurrDateTime();
   		}
            //
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            
            String successMsg = "";	
            if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/todo/view.jsp"/>
</portlet:renderURL>	


<portlet:actionURL var="doApprovingFileURL"  name="doApprovingFile" >
</portlet:actionURL>	

<portlet:actionURL var="doRejectFileURL"  name="doRejectFile" >
</portlet:actionURL>	

<portlet:actionURL var="doForwardApproveFileURL"  name="doForwardApproveFile" >
</portlet:actionURL>	

<script type="text/javascript">
	function doChange(val) {
		alert(val);
		
		if(val.equals("2")){
			<portlet:namespace />rejectReason.enabled = true;
			<portlet:namespace />fileName.enabled = true;
		}else{
			<portlet:namespace />rejectReason.enabled = false;
			<portlet:namespace />fileName.enabled = false;
		}
	}
</script>	
	
<aui:layout>
	<aui:column cssClass="header">
	 	<img src="<%=renderRequest.getContextPath()%>/images/review.png">&nbsp;文件審核：<%=fileNameOrig %>
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:form action="<%=doApprovingFileURL%>" method="post"  enctype="multipart/form-data"  name="<portlet:namespace />fm">
	<aui:input type="hidden" name="taskId"  value="<%=taskId %>"/>
	
	<aui:layout>
	  	<aui:column cssClass="header">
	  		<aui:input name="approveType" type="radio"  value="approve" label="接受"  checked='<%=approveType.equals("approve")%>'/>
	  	</aui:column>		
	</aui:layout>
	<aui:layout>
	  	<aui:column>&nbsp; 	&nbsp; </aui:column>
	  	<aui:column cssClass="td">
				※ 執行後本檔案狀態即變更為「完成」。
	  	</aui:column>		
	</aui:layout>
	  
	 <br/><br/> 
	  
	<aui:layout>
	  	<aui:column cssClass="header">	
	  		<aui:input name="approveType" type="radio"  value="reject" label="退回"  checked='<%=approveType.equals("reject")%>' />
	  	</aui:column>	
	</aui:layout>	
	<aui:layout>
	  	<aui:column>&nbsp; &nbsp; </aui:column>
	  	<aui:column>
  			<aui:layout>
            		<aui:column cssClass="td">
					<aui:input type="text" name="rejectReason"  label="退回原因"  size="100"  checked='<%=approveType.equals("rejectReason")%>' />
				</aui:column>
			</aui:layout>	
  			<aui:layout>
            		<aui:column cssClass="td">
					<h4>繳回期限</h4>
					<liferay-ui:input-date dayParam="dayDue"  monthParam="monDue"  yearParam="yearDue"
											 yearValue="<%=ErpUtil.getYear(replyDueDate) %>"  monthValue="<%=ErpUtil.getMonth(replyDueDate) - 1%>"  dayValue="<%=ErpUtil.getDay(replyDueDate) %>"
												yearRangeStart="<%=ErpUtil.getYear(replyDueDate) %>" yearRangeEnd="<%=ErpUtil.getYear(replyDueDate) + 1%>"/>            		
            		
				</aui:column>
			</aui:layout>	
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input type="file" name="fileName" label="附加的已修改檔案" size="100"  value="<%=fileNameUpload %>"/>
				</aui:column>
			</aui:layout>
	  	</aui:column>		
	  </aui:layout>
	  	
	  <br/><br/>
	  	
	  <aui:layout>
	  	<aui:column cssClass="header">
	  		<aui:input name="approveType" type="radio"  value="modifySelf" label="逕行修改"  />
	  	</aui:column>		
	  </aui:layout>
	  <aui:layout>
		<aui:column cssClass="td">
			<aui:input type="file" name="fileName2" label="附加的逕行修改檔案" size="100"  value="<%=fileNameUpload %>"/>
		</aui:column>
	 </aui:layout>
	  <aui:layout>
	  		<aui:column>&nbsp;&nbsp; </aui:column>
	  		<aui:column cssClass="td">
	  				※ 執行後本檔案狀態即變更為「逕行修改」。
	  		</aui:column>		
	  </aui:layout>
	  
	  <br/><br/>
	  
	  <aui:button-row>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" cssClass="button"/>
			<aui:button type="submit" value="確定執行" cssClass="button"/>
		</aui:button-row>
</aui:form>
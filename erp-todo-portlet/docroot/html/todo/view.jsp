<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.RowChecker"%>

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 
<portlet:defineObjects />
 
  <%
   		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            User user = themeDisplay.getUser();
            //
            if(renderRequest.getAttribute("javax.servlet.forward.query_string") != null){
        	    	String currParam = renderRequest.getAttribute("javax.servlet.forward.query_string").toString();
        	    	renderRequest.getPortletSession(true).setAttribute("currParam", currParam);
            }else{
        	    	renderRequest.getPortletSession(true).removeAttribute("currParam");
            }
            //
            Vector<Task> vecTask = new Vector<Task>();
            if(renderRequest.getPortletSession(true).getAttribute("vecTask") != null){
            	vecTask = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecTask");
            }
		//所屬助檢的待辦
            Vector<Task> vecTaskStaff = new Vector<Task>();
            if(renderRequest.getPortletSession(true).getAttribute("vecTaskStaff") != null){
        	    vecTaskStaff = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecTaskStaff");
            }
		//包含的報告編號
            Vector<String> vecReport = new Vector<String>();
            if(renderRequest.getPortletSession(true).getAttribute("vecReport") != null){
            	vecReport = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecReport");
            }
		
      	//目前篩選的 report no
      	String currReportNoBank = "";
      	if(renderRequest.getPortletSession(true).getAttribute("currReportNoBank") != null){
      		currReportNoBank = renderRequest.getPortletSession(true).getAttribute("currReportNoBank").toString();
      	}
      	
		//      	
	
            String userFullName = "";
            if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
            	userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
            }
            //
            int rowsPrefer = 20;
		if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
			rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
		}
            //
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            String successMsg = "";	
            if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="prepareBatchJobURL" name="prepareBatchJob" >
</portlet:actionURL>

<portlet:actionURL var="prepareBatchApproveStaffURL" name="prepareBatchApproveStaff" >
</portlet:actionURL>

<portlet:actionURL var="doFilterByReportURL" name="doFilterByReport" >
</portlet:actionURL>

<script type="text/javascript">
	Liferay.on('receiveTaskId', 
		function(event, p_data){
			var dd = p_data.split("#");
			//
			var ids = document.getElementById("<portlet:namespace/>batchIds").value;
			var names = document.getElementById("<portlet:namespace/>batchNames").value;
			//
			if(ids.indexOf(dd[0]) == -1){
				if(ids != ""){
					ids += "#";
				}
				ids += dd[0];
				//
				if(names != ""){
					names += "#";
				}
				names += dd[1];
				//			
				document.getElementById("<portlet:namespace/>batchIds").value = ids;
				document.getElementById("<portlet:namespace/>batchNames").value = names;
			}
		}
	);
	
	function doClearBatchIds(){
		document.getElementById("<portlet:namespace/>batchIds").value = "";
		document.getElementById("<portlet:namespace/>batchNames").value = "";
	}
	
	function doCheck(){
		var withCkecked = false;
		var chk = document.getElementsByTagName('input');
	    	var len = chk.length;
	      for (var i = 0; i < len; i++) {
	      	if (chk[i].type == 'checkbox') {
	            	if(chk[i].checked){
	            		withCkecked = true;
	            		break;
	            	} 
	        	}
	    	}      
	      //
		if(withCkecked){
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = false;
			        	}
		    	}
		}else{
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = true;
			        	}
		    	}
		}
	}
	
</script>

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/todo.png">&nbsp;您(<%=userFullName%>)的待辦事項
	</aui:column>
	<aui:column cssClass="td">&nbsp;&nbsp;</aui:column>
	
	<aui:column cssClass="td">以報告編號篩選：</aui:column>
	<aui:form action="<%=doFilterByReportURL.toString()%>"  method="post" >
		<aui:column cssClass="td">
			<aui:select name="reportNoBank" label="">
				<aui:option>*</aui:option>
				<%for(String reportNoBank: vecReport){	%>	
				<aui:option value="<%=reportNoBank%>"  selected="<%=reportNoBank.equals(currReportNoBank) %>"><%=reportNoBank %></aui:option>
				<%} %>
			</aui:select>
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="篩選" cssClass="button"/>
		</aui:column>
	</aui:form>
</aui:layout>
<hr/>

<aui:form action="<%=prepareBatchJobURL%>" method="post" name="<portlet:namespace />fm">
<aui:layout>
	<aui:column>
		<a href="javascript:void();"  onClick="javascript:doCheck();">
			<img src='<%=renderRequest.getContextPath() + "/images/checked.png" %>'/>
		</a>
	</aui:column>

	<aui:column>&nbsp;
	</aui:column>
	
	<aui:column cssClass="td">※ 批次作業選項：</aui:column>
	<aui:column cssClass="td">
	  	<aui:input name="batchType" type="radio"  value="approval" label="批次審核(請先勾選待審項目)" />
	</aui:column>
	<aui:column cssClass="td">
	  	<aui:input name="batchType" type="radio"  value="complete" label="多檔交付審核" />
	</aui:column>
	<aui:column cssClass="td">
	  	<aui:input name="batchType" type="radio"  value="download" label="批次下載(請先勾選)" />
	</aui:column>
			
	<aui:column>
		<aui:button type="submit" value="執行" cssClass="button"/>
	</aui:column>
</aui:layout>

<br>

<liferay-ui:search-container curParam="curTask"  emptyResultsMessage="您目前沒有任務。"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecTask, searchContainer.getStart(), searchContainer.getEnd());
			total = vecTask.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="task">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="stageFolderId"  value="<%=task.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=task.fileId %>"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=task.taskId%>_<%=task.taskType%>"/>   
            </liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=task.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="簽收人" property="receivedBy" />
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName"  href="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<br>

<%if(vecTaskStaff.size() > 0){ %>
<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/proxy.png">&nbsp;您所屬助檢未完成的任務
	</aui:column>
</aui:layout>
<hr/>

<liferay-ui:search-container curParam="curTaskStaff" emptyResultsMessage=""  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecTaskStaff, searchContainer.getStart(), searchContainer.getEnd());
			total = vecTaskStaff.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="taskStaff">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="stageFolderId"  value="<%=taskStaff.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=taskStaff.fileId %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=taskStaff.taskId%>_<%=taskStaff.taskType%>"/>   
            </liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=taskStaff.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="受任人" property="assignee" />
		<liferay-ui:search-container-column-text name="簽收人" property="receivedBy" />
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName"  href="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<%} %>

</aui:form>

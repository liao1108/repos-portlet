<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
		  Vector<Task> vecBatchTask = new Vector<Task>();
		  if(renderRequest.getPortletSession(true).getAttribute("vecBatchTaskStaff") != null){
			  vecBatchTask = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecBatchTaskStaff");
		  }
  		//
   		String replyDueDate = ErpUtil.getCurrDateTime();
            //
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            
            String successMsg = "";	
            if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/todo/view.jsp"/>
</portlet:renderURL>	


<portlet:actionURL var="doBatchApprovingStaffURL"  name="doBatchApprovingStaff" >
</portlet:actionURL>	

<script type="text/javascript">
	function doChange(val) {
		alert(val);
		
		if(val.equals("2")){
			<portlet:namespace />rejectReason.enabled = true;
			<portlet:namespace />fileNameUpload.enabled = true;
		}else{
			<portlet:namespace />rejectReason.enabled = false;
			<portlet:namespace />fileNameUpload.enabled = false;
		}
	}
</script>	
	
<aui:form action="<%=doBatchApprovingStaffURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
	  	<aui:column cssClass="header">
	  		<aui:input name="approveType" type="radio"  value="approve" label="接受"  />
	  	</aui:column>		
	</aui:layout>
	<aui:layout>
	  	<aui:column>&nbsp; 	&nbsp; </aui:column>
	  	<aui:column cssClass="td">
				※ 執行後本檔案狀態即變更為「完成」。
	  	</aui:column>		
	</aui:layout>
	 <br/>
	<aui:layout>
	  	<aui:column cssClass="header">	
	  		<aui:input name="approveType" type="radio"  value="reject" label="退回"   />
	  	</aui:column>	
	</aui:layout>	
	<aui:layout>
	  	<aui:column>&nbsp; &nbsp; </aui:column>
	  	<aui:column>
  			<aui:layout>
            		<aui:column cssClass="td">
					<aui:input type="text" name="rejectReason"  label="退回原因"  size="100"  />
				</aui:column>
			</aui:layout>	
  			<aui:layout>
            		<aui:column cssClass="td">
					<h4>繳回期限</h4>
					<liferay-ui:input-date dayParam="dayDue"  monthParam="monDue"  yearParam="yearDue"
											 yearValue="<%=ErpUtil.getYear(replyDueDate) %>"  monthValue="<%=ErpUtil.getMonth(replyDueDate) - 1%>"  dayValue="<%=ErpUtil.getDay(replyDueDate) %>"
												yearRangeStart="<%=ErpUtil.getYear(replyDueDate) %>" yearRangeEnd="<%=ErpUtil.getYear(replyDueDate) + 1%>"/>            		
            		
				</aui:column>
			</aui:layout>	
	  	</aui:column>		
	  </aui:layout>
	 <br/>
	  <aui:layout>
	  	<aui:column cssClass="header">
	  		<aui:input name="approveType" type="radio"  value="modifySelf" label="逕行修改"  />
	  	</aui:column>		
	  </aui:layout>
	  <aui:layout>
	  		<aui:column>&nbsp;&nbsp; </aui:column>
	  		<aui:column cssClass="td">
	  				※ 執行後本檔案狀態即變更為「逕行修改」。
	  		</aui:column>		
	  </aui:layout>
	  <br/>
	  <aui:button-row>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" cssClass="button"/>
			<aui:button type="submit" value="確定執行" cssClass="button"/>
	  </aui:button-row>
</aui:form>
<hr>
<aui:layout>
	<aui:column cssClass="td">批次審核項目列表：	</aui:column>
</aui:layout>

<liferay-ui:search-container>
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecBatchTask, searchContainer.getStart(), searchContainer.getEnd());
			total = vecBatchTask.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="taskStaff">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="stageFolderId"  value="<%=taskStaff.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=taskStaff.fileId %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=taskStaff.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="受任人" property="assignee" />
		<liferay-ui:search-container-column-text name="簽收人" property="receivedBy" />
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName"  href="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>

	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>



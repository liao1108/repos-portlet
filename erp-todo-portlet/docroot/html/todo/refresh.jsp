<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   		String fileName = renderRequest.getParameter("fileName");
   		String taskId = renderRequest.getParameter("taskId");
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/todo/view.jsp"/>
</portlet:renderURL>	


<portlet:actionURL var="doRefreshFileURL"  name="doRefreshFile" >
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="header">
		<img src="<%=renderRequest.getContextPath()%>/images/refresh.png">&nbsp;文件最新版本載入
	</aui:column>
</aui:layout>		
<hr/>
<br>

<aui:fieldset >
	<aui:form action="<%=doRefreshFileURL%>" method="post" name="<portlet:namespace />fm">
		<aui:input type="hidden" name="taskId"  value="<%=taskId %>"/>
		<aui:input type="hidden" name="fileName"  value="<%=fileName %>"/>

		<aui:layout>
			<aui:column cssClass="th">
				確定載入檔案：<%=fileName %>之最新版本？
			</aui:column>
		</aui:layout>	
		
		<aui:button-row>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"/>
			<aui:button type="submit" value="確定載入"/>
		</aui:button-row>
		
	</aui:form>
</aui:fieldset>

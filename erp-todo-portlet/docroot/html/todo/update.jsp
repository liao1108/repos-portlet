<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 
<portlet:defineObjects />
 
  <%
  		String stageFolderId = "";
		if(renderRequest.getParameter("stageFolderId") != null){
			stageFolderId = renderRequest.getParameter("stageFolderId");
		}
  
		String fileName = "";
		if(renderRequest.getParameter("fileName") != null){
			fileName = renderRequest.getParameter("fileName");
		}

         	String userFullName = "";
         	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
         		userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
         	}
         	//
         	String errMsg = "";	
         	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
         	
         	String successMsg = "";	
         	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/todo/view.jsp"/>
</portlet:renderURL>	


<portlet:actionURL var="updateFileURL"  name="updateFile" >
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="th">
	 	<img src="<%=renderRequest.getContextPath()%>/images/job32.png">&nbsp;更新待辦事項文件：<%=fileName %>
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:form action="<%=updateFileURL%>"  enctype="multipart/form-data" method="post" >
<aui:fieldset>
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="stageFolderId"  value="<%=stageFolderId %>"/>
			<aui:input type="hidden" name="fileName"  value="<%=fileName %>"/>
			<aui:input type="file" name="fileName" label="請指定所要上傳的檔案" size="75"/>
		</aui:column>
	</aui:layout>

	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" cssClass="button"/>
		<aui:button type="submit" value="開始上傳" cssClass="button"/>
	</aui:button-row>
</aui:fieldset>	
</aui:form>

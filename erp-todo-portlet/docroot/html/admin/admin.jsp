<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Task" %>
<%@ page import="com.itez.ErpUtil" %>

<portlet:defineObjects />

<%
	boolean isLeader = false;
	if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
		isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	}
	
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Task t = (Task)row.getObject();
	//加入批次
	String addToBatchList = "javascript: addToBatchList('" +t.taskId +"#" + t.fileName + "');";
	//線上編輯
	String editOnlineURL = "javascript: openWebDAVFile('" +t.webDavURL + "');";
	String copyToClipBoardURL = "javascript: copyToClipBoard('" + t.webDavURL + "');";
	String showWebDavURL = "javascript: showWebDavURL('" + t.webDavURL + "');";
%>

<script type="text/javascript">
	function addToBatchList(val){
		//document.getElementByName("<portlet:namespace />addCommIds").value = val;
		Liferay.fire('receiveTaskId', val);
	}
	
	function openWebDAVFile(url) {
		//alert(url);
		new ActiveXObject("SharePoint.OpenDocuments.1").EditDocument(url);
		//url.execCommand("Copy")

	};
	
	function copyToClipBoard(url){
			try{
				var result = window.clipboardData.setData('Text', url);
				if(result){
					alert("已將檔案編輯網址複製到剪貼簿。");
				}else{
					alert("檔案編輯網址無法複製到剪貼簿！");
				}
			}catch(ex){
				alert("檔案編輯網址無法複製到剪貼簿！");
			}
	}
	
	function showWebDavURL(url){
		prompt("檔案編輯網址： ", url);
	}
	
</script>
<liferay-ui:icon-menu>

	<portlet:resourceURL var="downloadFileURL" >
  		<portlet:param name="stageFolderId"  value="<%=t.stageFolderId %>"/>
  		<portlet:param name="fileId"  value="<%=t.fileId %>"/>
  		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:resourceURL>
	
	<portlet:actionURL var="doRefreshFileURL"  name="doRefreshFile" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:actionURL>

	<portlet:renderURL var="goUpdateFileURL" >
		<portlet:param name="jspPage"  value="/html/todo/update.jsp"/>
		<portlet:param name="stageFolderId"  value="<%=t.stageFolderId %>"/>
  		<portlet:param name="fileName"  value="<%=t.fileName %>"/>
  		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:renderURL>

	<portlet:renderURL var="goCompleteFileURL" >
		<portlet:param name="jspPage"  value="/html/todo/complete.jsp"/>
  		<portlet:param name="fileName"  value="<%=t.fileName %>"/>
  		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="goApproveURL" >
		<portlet:param name="jspPage"  value="/html/todo/approval.jsp"/>
		<portlet:param name="stageFolderId"  value="<%=t.stageFolderId %>"/>
  		<portlet:param name="fileNameOrig"  value="<%=t.fileName %>"/>
  		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
  		<portlet:param name="taskComment"  value="<%=t.taskComment%>"/>
	</portlet:renderURL>
	
	<portlet:actionURL var="doCheckOutURL" name="doCheckOut" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:actionURL>
	
	<portlet:actionURL var="doCheckInURL" name="doCheckIn" >
		<portlet:param name="taskId"  value="<%=String.valueOf(t.taskId)%>"/>
	</portlet:actionURL>


	<% //liferay-ui:icon image="edit" message="領出" url="<%=doCheckOutURL.toString()  %>
	<% //liferay-ui:icon image="edit" message="取得最新版本" url="<%=doRefreshFileURL.toString()  %>

	<%if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT) || t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){ %>
			<liferay-ui:icon  message="交付審核" url="<%=goCompleteFileURL.toString() %>"/>
	<%}else if(t.taskType.equals(ErpUtil.TASK_REVIEW) || t.taskType.equals(ErpUtil.TASK_DELEGATED_REVIEW)){ %>
			<liferay-ui:icon  message="進入審核" url="<%=goApproveURL.toString() %>"/>
	<%} %>

</liferay-ui:icon-menu>
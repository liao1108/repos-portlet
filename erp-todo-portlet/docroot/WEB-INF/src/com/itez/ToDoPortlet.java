package com.itez;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;

import com.aspose.words.SaveFormat;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ToDoPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			//
			Vector<String> vecReport = new Vector<String>();	//包含的報告案
			if(renderRequest.getPortletSession(true).getAttribute("vecReport") != null){
				vecReport = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecReport");
			}
			String reportNoBank = "";
			if(renderRequest.getPortletSession(true).getAttribute("currReportNoBank") != null){
				reportNoBank = renderRequest.getPortletSession(true).getAttribute("currReportNoBank").toString();
			}
			//
			Vector<Task> vecTask = this.getTasks(userFullName, conn, reportNoBank);
			for(Task t: vecTask){
				String key = t.reportNo + "_" + t.bankName;
				if(!vecReport.contains(key)){
					vecReport.add(key);
				}
			}				
			renderRequest.getPortletSession(true).setAttribute("vecTask", vecTask);
			//
			Vector<Task> vecTaskStaff = this.getTasksStaff(userFullName, conn, reportNoBank);
			for(Task t: vecTaskStaff){
				String key = t.reportNo + "_" + t.bankName;
				if(!vecReport.contains(key)){
					vecReport.add(key);
				}
			}					
			renderRequest.getPortletSession(true).setAttribute("vecTaskStaff", vecTaskStaff);
			//
			Collections.sort(vecReport);
			renderRequest.getPortletSession(true).setAttribute("vecReport", vecReport);
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			//
			conn.close();
			//
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//查詢我的待辦事項
	private Vector<Task> getTasks(String userFullName, Connection conn, String reportNoBank) throws Exception{
		Vector<Task> vecTask = new Vector<Task>();
		//
		String sql = "SELECT * FROM todo_list WHERE assignee = ? AND (done IS NULL OR done = 0) AND received = 1 ";
		if(!reportNoBank.equals("") && !reportNoBank.equals("*")){
			String[] ss = reportNoBank.split("_");
			sql += " AND report_no = '" + ss[0] + "' AND bank_name='" + ss[1] + "' ";
		}
		sql += " ORDER BY dead_line, file_name";
		
		//System.out.println(sql);
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.assignee = rs.getString("assignee");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("issuer") != null) t.issuer = rs.getString("issuer");
			if(rs.getString("reissuer") != null) t.reIssuer = rs.getString("reissuer");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			if(rs.getString("received_by") != null && !rs.getString("received_by").equals("")){
				t.receivedBy = rs.getString("received_by").trim();
			}else{
				t.receivedBy = t.assignee;
			}
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			t.received = rs.getInt("received");
			//
			vecTask.add(t);
		}
		//
		return vecTask;
	}
	
	//查詢我助檢的待辦事項
	private Vector<Task> getTasksStaff(String userFullName, Connection conn, String reportNoBank) throws Exception{
		Vector<Task> vecTaskStaff = new Vector<Task>();
		//
		//是否有可代辦的任務
		String sql = "SELECT A.* FROM todo_list A WHERE A.assignee <> ? " +
						" AND (A.done IS NULL OR A.done = 0) " +
						" AND A.received = 1 " +
						" AND A.exam_no IN (SELECT B.exam_no FROM exams B WHERE B.leader_name = ? OR B.created_by = ?) ";
		if(!reportNoBank.equals("") && !reportNoBank.equals("*")){
			String[] ss = reportNoBank.split("_");
			sql += " AND A.report_no = '" + ss[0] + "' AND bank_name='" + ss[1] + "' ";
		}		
		sql += " ORDER BY A.dead_line, A.file_name";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ps.setString(2, userFullName);
		ps.setString(3, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.assignee = rs.getString("assignee");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("issuer") != null) t.issuer = rs.getString("issuer");
			if(rs.getString("reissuer") != null) t.reIssuer = rs.getString("reissuer");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			if(rs.getString("received_by") != null && !rs.getString("received_by").equals("")){
				t.receivedBy = rs.getString("received_by").trim();
			}else{
				t.receivedBy = t.assignee;
			}
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			t.received = rs.getInt("received");
			//
			vecTaskStaff.add(t);
		}
		//
		return vecTaskStaff;
	}

	
	//更新內容
	/*
	public void updateFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        try{
	        	//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	String stageFolderId = ParamUtil.getString(uploadRequest,"stageFolderId");
	        	String fileName = ParamUtil.getString(uploadRequest,"fileName");
	        	//
	        	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要上傳的檔案。");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
	        	AlfrescoDocument alfDoc = null;
	        	Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().equals(fileName)){
	        			alfDoc = (AlfrescoDocument)co;
	        			break;
	        		}
	        	}
	        	//
		        //String sourceFileName = uploadRequest.getFileName("fileName");
		        //File file = uploadRequest.getFile("fileName");
		        InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
			ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
			//
			alfDoc.setContentStream(cs, true);
			//
			is.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/html/todo/update.jsp");
	        }
	}
	*/
	
	//助檢文件交付
	public void completeFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			int taskId  = Integer.parseInt(ParamUtil.getString(uploadRequest,"taskId"));
	        	//String stageFolderId = ParamUtil.getString(uploadRequest,"stageFolderId");
			String completeMemo = ParamUtil.getString(uploadRequest,"completeMemo");
	        	String fileName = ParamUtil.getString(uploadRequest,"fileName");
	        	
	        	String fileNameUpload = uploadRequest.getFileName("fileNameUpload");
	        	//
	        	//取得 task
	        	conn = ds.getConnection();
	        	//
	        	Task t = ErpUtil.getTask(taskId, conn);
	        	actionRequest.getPortletSession(true).setAttribute("currTask", t);
	        	//
	        	ErpUtil.logUserAction("交回檔案", t.getFileFullName(), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//
	        	//if (uploadRequest.getSize("fileNameUpload")==0)  throw new Exception("請指定完成後所要上傳的檔案。");
	        	
	        	String origPureName = t.fileName.substring(0, t.fileName.lastIndexOf("."));
	        	String origExtName = t.fileName.substring(t.fileName.lastIndexOf(".") + 1);
	        	String sourceExtName = fileNameUpload.substring(fileNameUpload.lastIndexOf(".") + 1);
	        	
		        if(!fileNameUpload.equals("") && 
		        		(!origExtName.equalsIgnoreCase(sourceExtName) || !fileNameUpload.startsWith(origPureName))){ 
		        	throw new Exception("您上傳的檔名(" + fileNameUpload + ")與所要更新的檔名不符。");
		        }
	        	if(!fileNameUpload.equals("") && !fileNameUpload.equalsIgnoreCase(fileName)) throw new Exception("上傳的檔案：" + fileNameUpload + " 與原檔名：" + fileName + " 不符。");
			//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	
	        	AlfrescoDocument alfDoc = null;
	        	try{
	        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + fileName);
	        	}catch(Exception _ex){
	        	}
	        	//
	        	if(alfDoc == null){
	        		ErpUtil.closeTask(taskId, userFullName, conn);
	        		//
	        		throw new Exception("找不到檔名為「" + fileName + "」之檔案，本項任務將標註為完成。");
	        	}
	        	//if(alfDoc.getVersionLabel().compareTo(t.versionNo) > 0) throw new Exception("檔案：" + fileName + " 已被其他人更新，請先更新版本後再處理。");

	        	//如過被領出, 先取消領出
	        	if(alfDoc.isVersionSeriesCheckedOut()){
	        		alfDoc.cancelCheckOut();
	        	}
	        	//如果有上傳檔案
			ContentStream cs = alfDoc.getContentStream();
	        	if(!fileNameUpload.equals("")){
				//進行內容更新
		        	InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileNameUpload"));
		        	cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
			}
	        	//
	        	if(!this.taskCompleteImpl(t, cs, alfDoc, fdStage, fdExam, alfSessionAdmin, themeDisplay, userFullName, completeMemo, conn)){
	        		throw new Exception("檔案無法交付。");
	        	}
		        //
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/complete.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	//任務完成實作
	private boolean taskCompleteImpl(Task t,
								ContentStream cs,
								AlfrescoDocument alfDoc,
								AlfrescoFolder fdStage,
								AlfrescoFolder fdExam,
								Session alfSessionAdmin,
								ThemeDisplay themeDisplay,
								String userFullName,
								String completeMemo,
								Connection conn) throws Exception{
		//內容更新摘要
        	String updateMemo = "交付審核";
        	Map<String, String> props = new HashMap<String, String>();
        	props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
		props.put("it:updateMemo", updateMemo);
		//找出最新版本
        	if(!alfDoc.isLatestVersion()){
        		try{
        			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + alfDoc.getName());
        			if(alfDoc != null){
        				fdStage = (AlfrescoFolder)alfDoc.getParents().get(0);
        			}
        		}catch(Exception _ex){
        			_ex.printStackTrace();
        			throw new Exception("無法取得 " + alfDoc.getName() + " 之最新版本。");
        		}
        	}
		//
		AlfrescoFolder fdNext = null;
        	if(fdStage.getName().contains("行前")){
        		//找出檢查結束目錄
        		AlfrescoFolder fdEnd = null;
        		Iterator<CmisObject> it = fdExam.getChildren().iterator();
        		while(it.hasNext()){
        			CmisObject co = it.next();
        			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("結束")){
        				fdEnd = (AlfrescoFolder)co;
        			}
        		}
        		//if(fdEnd == null) throw new Exception("找不到檢查結束階段。");
        		if(fdEnd == null) return false;
        		
        		//將檔案搬移至檢查結束階段
        		AlfrescoDocument alfDocDest = null;
        		try{
        			alfDocDest = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdEnd.getPath() + "/" + alfDoc.getName());
        		}catch(Exception _ex){
        		}
        		//
        		if(alfDocDest == null){
        			ObjectId objectId = alfDoc.move(fdStage, fdEnd);
        			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
        		}else{
        			alfDoc.delete();
        			alfDoc = alfDocDest;
        		}
        		//
        		fdNext = fdEnd;
        	}else{
        		fdNext = fdStage;
        	}
        	//啟動 MS Word 追蹤修訂
        	if(alfDoc.getName().toLowerCase().endsWith("doc")){
	        	com.aspose.words.Document aDoc = new com.aspose.words.Document(cs.getStream());
	        	
	        	aDoc.setTrackRevisions(true);
	        	
	        	OutputStream os = new ByteArrayOutputStream();
	        	aDoc.save(os, SaveFormat.DOC);
	        	
	        	InputStream is=new ByteArrayInputStream(((ByteArrayOutputStream)os).toByteArray());
	        	cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
        	}
        	//
        	updateMemo = userFullName + "交付文件";
        	if(!t.issuer.equals(t.assignee)){		//發起人與受任人不同
			props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
			props.put("it:docStatus", ErpUtil.STATUS_APPROVING);
			//
			
			//CmisObject co = alfDoc.updateProperties(props);
			//alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(co.getId());
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
        		//新增發起人之審核任務
			ExamFact ei = ErpUtil.getExamInfo(fdNext, alfSessionAdmin, conn);
		        Task nt = new Task();
		        		
		        nt.taskType = ErpUtil.TASK_REVIEW;
		        nt.reportNo = ei.reportNo;
		        nt.examNo = ei.examNo;
		        nt.bankName = ei.bankName;
		        nt.stageFolderId = fdNext.getId();						//階段 Folder ID
		        nt.stageName = fdNext.getName();
		        nt.fileId = alfDoc.getId();
		        nt.versionNo = alfDoc.getVersionLabel();
		        nt.fileName = alfDoc.getName();
		        nt.assignee = ei.leaderName;							//受任人
		        nt.deadLine = "";									//領隊沒有審核期限
		        nt.issuer = ErpUtil.getCurrUserFullName(themeDisplay);		//助檢當發起人
		        nt.createdTime = ErpUtil.getCurrDateTime();
		        nt.taskComment = completeMemo;
		        //
	        	ErpUtil.addTask(nt, 1, conn, themeDisplay);
        	}else{	//直接將狀態改成完成
			props.put("it:docStatus", ErpUtil.STATUS_APPROVED);
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			pwc.checkIn(false, props, cs, updateMemo);
        	}
        	 //將本任務結束
	        ErpUtil.closeTask(t.taskId, userFullName, conn);
	        return true;
	}
	
	//審核處理
	public void doApprovingFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
	    Task t = null;
	    int taskId = 0;
	    String fileNameOrig = "";
	    String rejectReason = "";
	    String fileNameUpload = "";
	    String fileNameUpload2 = "";
	    String approveType = "";
	    String replyDueDate = "";
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
	        	//
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			taskId  = Integer.parseInt(ParamUtil.getString(uploadRequest, "taskId"));
			//記住執行審核動作的任務
			t = ErpUtil.getTask(taskId, conn);
	        	fileNameOrig = t.fileName;
	        	//
	        	ErpUtil.logUserAction("審核檔案", t.getFileFullName(), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//
	        	approveType = ParamUtil.getString(uploadRequest, "approveType");
	        	rejectReason = ParamUtil.getString(uploadRequest, "rejectReason");
	        	replyDueDate = ErpUtil.genDateFromAUI(ParamUtil.getString(uploadRequest, "yearDue"),
	        											ParamUtil.getString(uploadRequest, "monDue"),
	        											ParamUtil.getString(uploadRequest, "dayDue"));
	        	if(uploadRequest.getFileName("fileName") != null){
	        		fileNameUpload = uploadRequest.getFileName("fileName");
	        	}
	        	if(uploadRequest.getFileName("fileName2") != null){
	        		fileNameUpload2 = uploadRequest.getFileName("fileName2");
	        	}
	        	//
	        	if(approveType.equalsIgnoreCase("")){
	        		throw new Exception("請指定接受、退回或逕行修改。");
	        	}else if(!approveType.equalsIgnoreCase("reject")){
	        		if(!rejectReason.equals("") || !fileNameUpload.equals("")){
	        			throw new Exception("如非退件，請勿輸入退件原因或檔案。");
	        		}
	        	}else{
	        		if(rejectReason.equals("")){
	        			throw new Exception("請輸入退件原因。");
	        		}
	        	}
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	
	        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + fileNameOrig);
	        	//Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	//while(it.hasNext()){
	        	//	CmisObject co = it.next();
	        	//	if(co.getName().equals(t.fileName)){
	        	//		alfDoc = (AlfrescoDocument)co;
	        	//		break;
	        	//	}
	        	//}
	        	//
	        	if(alfDoc == null){
	        		ErpUtil.closeTask(taskId, userFullName, conn);
	        		//
	        		throw new Exception("找不到檔名為「" + t.fileName + "」之檔案，本項任務將標註為完成。");	        		
	        	}
	        	//如果已領出者先取消領出
	        	if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + fileNameOrig + "已被鎖定，請回到該檔案所在階段取消領出後再處理。");
	        	if(alfDoc.getVersionLabel().compareTo(t.versionNo) <= 0){	// throw new Exception("檔案：" + fileNameOrig + "已被修改，請重新載入後再執行。");
	        	    if(approveType.equals("approve") || approveType.equals("modifySelf")){
	        	    		//更新
	        	    		String versionNote = userFullName + "接受本文件";
	        	    		Map<String, String> props = new HashMap<String, String>();
	        	    		props.put("it:updateMemo", versionNote);
	        	    		props.put("it:docStatus", ErpUtil.STATUS_APPROVED);
	        	    		//
	        	    		ContentStream cs = alfDoc.getContentStream();
	        	    		//如果是逕行修改
	        	    		if(approveType.equals("modifySelf")){
	        	    			versionNote = userFullName + "逕行修改本文件";
	        	    			props = new HashMap<String, String>();
	        	    			props.put("it:updateMemo", versionNote);
	        	    			props.put("it:docStatus", ErpUtil.STATUS_MODIFY_SELF);
	        	    			//
	        	    			if(!fileNameUpload2.equals("")){
	        	    				String pureNameUpload = fileNameUpload2.substring(fileNameUpload.lastIndexOf("//") + 1) .substring(0, fileNameUpload2.lastIndexOf("."));
	        	    				String pureName = fileNameOrig.substring(0, fileNameOrig.lastIndexOf("."));
	        	    				if(!pureNameUpload.startsWith(pureName)) throw new Exception("準備更新的檔案：" + fileNameUpload2 + " 與原檔名：" + t.fileName + " 不符。");
	        	    				//
	        	    				InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName2"));
	        	    				cs = new ContentStreamImpl(fileNameOrig, null, alfDoc.getContentStreamMimeType(), is);
	        	    			}
	        	    		}
	        	    		//
	        	    		ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
	        	    		Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
	        	    		ObjectId objectId = pwc.checkIn(false, props, cs, versionNote);
	        	    		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
	        	    		//關閉本任務
	        	    		ErpUtil.closeTask(taskId, userFullName, conn);
		        }else if(approveType.equals("reject")){
		        		//更新檔案
	        			String versionNote = userFullName + "退回";
	        			Map<String, String> props = new HashMap<String, String>();
	        			props.put("it:updateMemo", versionNote);
	        			props.put("it:docStatus", ErpUtil.STATUS_REJECTED);
	        			//
	        			ContentStream cs = null;
	        			if(!fileNameUpload.equals("")){
	        				String pureNameUpload = fileNameUpload.substring(fileNameUpload.lastIndexOf("//") + 1) .substring(0, fileNameUpload.lastIndexOf("."));
	        				String pureName = fileNameOrig.substring(0, fileNameOrig.lastIndexOf("."));
	        				if(!pureNameUpload.startsWith(pureName)) throw new Exception("準備更新的檔案：" + fileNameUpload + " 與原檔名：" + t.fileName + " 不符。");
	        				//
	        				InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
	        				cs = new ContentStreamImpl(fileNameOrig, null, alfDoc.getContentStreamMimeType(), is);
	        			}else{
	        				cs = alfDoc.getContentStream();
	        			}
	        			//
	        			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
	        			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
	        			ObjectId objectId = pwc.checkIn(false, props, cs, versionNote);
	        			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
	        			//
	        			Task ct = ErpUtil.getTask(taskId, conn);
	        			//
	        			Task nt = new Task();
	        			nt.taskType = ErpUtil.TASK_MODIFY_REPORT;
	        			nt.assignee = ct.issuer;
	        			nt.reportNo = ct.reportNo;
	        			nt.examNo = ct.examNo;
	        			nt.bankName = ct.bankName;
	        			nt.fileId = alfDoc.getId();
	        			nt.fileName = ct.fileName;
	        			nt.versionNo = alfDoc.getVersionLabel();
	        			nt.issuer = userFullName;
	        			nt.stageFolderId = ct.stageFolderId;
	        			nt.stageName = ct.stageName;
	        			nt.taskComment = rejectReason;
	        			nt.deadLine = replyDueDate;
	        			//
	        			ErpUtil.addTask(nt, 0, conn, themeDisplay);
		        }
	        	}
	        //關閉本任務
	        ErpUtil.closeTask(taskId, userFullName, conn);
		    //
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	    }catch(Exception ex){
	        	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRequest.getPortletSession(true).setAttribute("currTask", t);
			
			actionResponse.setRenderParameter("taskId", String.valueOf(taskId));
			actionResponse.setRenderParameter("fileNameOrig", fileNameOrig);
			actionResponse.setRenderParameter("taskComment", rejectReason);
			actionResponse.setRenderParameter("fileNameUpload", fileNameUpload);
			actionResponse.setRenderParameter("approveType", approveType);
			actionResponse.setRenderParameter("replyDueDate", replyDueDate);
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/approval.jsp");
	    }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	    }
	}
	
	//以報告編號篩選
	public void doFilterByReport(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
	        	//
			String reportNoBank = actionRequest.getParameter("reportNoBank");
			if(reportNoBank.equals("") || reportNoBank.equals("*")){
				actionRequest.getPortletSession(true).removeAttribute("currReportNoBank");
			}else{
				actionRequest.getPortletSession(true).setAttribute("currReportNoBank", reportNoBank);	
			}
	        	conn.close();
	        	//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	        }catch(Exception ex){
	        	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}

	//準備批次審核
	public void prepareBatchJob(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
			try{		
				ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
				//
				conn = ds.getConnection();
				String batchType = "";
				if(actionRequest.getParameter("batchType") != null){
					batchType = actionRequest.getParameter("batchType");
				}
				if(batchType.equals("")) throw new Exception("請指定批次作業項目。");
				//
				if(batchType.equalsIgnoreCase("approval") || batchType.equalsIgnoreCase("download")){
					Enumeration<String> en = actionRequest.getParameterNames();
					Vector<Task> vecBatch = new Vector<Task>();
					//
					while(en.hasMoreElements()){
						String p = en.nextElement();
						if(p.startsWith("cb_")){
							try{
								String[] ss = p.split("_");
								int taskId = Integer.parseInt(ss[1]);
								Task t = ErpUtil.getTask(taskId, conn);
								//
								String taskType = ss[2];		//任務類型
								if(batchType.equalsIgnoreCase("download") || taskType.equals(ErpUtil.TASK_REVIEW)){
									vecBatch.add(t);
								}
							}catch(Exception _ex){
							}
						}
					}
					//
					actionRequest.getPortletSession(true).removeAttribute("vecBatchTask");
					if(vecBatch.size() == 0){
						if(batchType.equalsIgnoreCase("approval")){
							throw new Exception("請勾選待審核的任務。");
						}else{
							throw new Exception("請勾選所要下載的任務檔案。");
						}
					}
					actionRequest.getPortletSession(true).setAttribute("vecBatchTask", vecBatch);
					//顯示成功訊息
					String msg = "本批次即將審核 " + vecBatch.size() + " 項任務。";
					String forwardPage = "/html/todo/batchApproval.jsp";
					if(batchType.equalsIgnoreCase("download")){
						msg = "本批次即將下載 " + vecBatch.size() + " 項任務檔案。";
						forwardPage = "/html/todo/batchDownload.jsp";
					}
					actionRequest.setAttribute("successMsg", msg);
					SessionMessages.add(actionRequest, "success");
					//轉向到批次審核頁面
					
					actionResponse.setRenderParameter("jspPage", forwardPage);
				}else if(batchType.equalsIgnoreCase("complete")){			//批次交付檔案
					String msg = "請指定所要交付的檔案。";
					String forwardPage = "/html/todo/batchComplete.jsp";
					actionRequest.setAttribute("successMsg", msg);
					SessionMessages.add(actionRequest, "success");
					//轉向到批次交付頁面
					actionResponse.setRenderParameter("jspPage", forwardPage);
				}
				//
				conn.close();
			}catch(Exception ex){
		        	//ex.printStackTrace();
				String errMsg = ErpUtil.getItezErrorCode(ex);
				if(errMsg.equals("")){
					errMsg = ex.getMessage();
				}
				actionRequest.setAttribute("errMsg", errMsg);
				SessionErrors.add(actionRequest, "error");
				//
				//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
		        }finally{
		        	try{
		        		if(conn != null) conn.close();
		        	}catch(Exception _ex){
		        	}
		        }
	}
	
	//準備批次代審
	public void prepareBatchApproveStaff(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
			try{		
				ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
				//
				Enumeration<String> en = actionRequest.getParameterNames();
				if(en == null) return;
				//
				Vector<Task> vecBatch = new Vector<Task>();
				//
				conn = ds.getConnection();
				while(en.hasMoreElements()){
					String p = en.nextElement();
					if(p.startsWith("cb_")){
						try{
							String[] ss = p.split("_");
							int taskId = Integer.parseInt(ss[1]);
							String taskType = ss[2];
							if(taskType.equals(ErpUtil.TASK_REVIEW)){
								Task t = ErpUtil.getTask(taskId, conn);
								vecBatch.add(t);
							}
						}catch(Exception _ex){
						}
					}
				}
				conn.close();
				//
				if(vecBatch.size() == 0){
					actionRequest.getPortletSession(true).removeAttribute("vecBatchTaskStaff");
					throw new Exception("請勾選待審核的任務。");
				}
				actionRequest.getPortletSession(true).setAttribute("vecBatchTaskStaff", vecBatch);
				//顯示成功訊息
				actionRequest.setAttribute("successMsg", "本批次即將代審 " + vecBatch.size() + " 項任務。");
				SessionMessages.add(actionRequest, "success");
				//轉向到批次審核頁面
				actionResponse.setRenderParameter("jspPage", "/html/todo/batchApprovalStaff.jsp");
			}catch(Exception ex){
		        	ex.printStackTrace();
				String errMsg = ErpUtil.getItezErrorCode(ex);
				if(errMsg.equals("")){
					errMsg = ex.getMessage();
				}
				actionRequest.setAttribute("errMsg", errMsg);
				SessionErrors.add(actionRequest, "error");
				//
				//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
		        }finally{
		        	try{
		        		if(conn != null) conn.close();
		        	}catch(Exception _ex){
		        	}
		        }
	}

	//批次審核處理
	@SuppressWarnings("resource")
	public void doBatchApproving(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
			//
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	String approveType = ParamUtil.getString(uploadRequest, "approveType");
	        	String rejectReason = ParamUtil.getString(uploadRequest, "rejectReason");
	        	String replyDueDate = ErpUtil.genDateFromAUI(ParamUtil.getString(uploadRequest, "yearDue"),
	        											ParamUtil.getString(uploadRequest, "monDue"),
	        											ParamUtil.getString(uploadRequest, "dayDue"));
	        	//
	        	if(approveType.equalsIgnoreCase("")){
	        		throw new Exception("請指定接受、退回或逕行修改。");
	        	}else if(!approveType.equalsIgnoreCase("reject")){
	        		if(!rejectReason.equals("")){
	        			throw new Exception("如非退件，請勿輸入退件原因或檔案。");
	        		}
	        	}else{
	        		if(rejectReason.equals("")){
	        			throw new Exception("請輸入退件原因。");
	        		}
	        	}
	        	@SuppressWarnings("unchecked")
			Vector<Task> vecBatchTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecBatchTask");
	        	if(vecBatchTask.size() == 0) throw new Exception("尚未指定批次審核的項目。");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	for(Task t: vecBatchTask){
		        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
		        	AlfrescoDocument alfDoc = null;
		        	try{
		        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + t.fileName);
		        	}catch(Exception _ex){
		        	}
		        	//
		        	if(alfDoc != null){
			        	//如果已領出者先取消領出
			        	if(alfDoc.isVersionSeriesCheckedOut()) alfDoc.cancelCheckOut();
			        	//
				        if(approveType.equals("approve") || approveType.equals("modifySelf")){
				        	//更新
			        		String versionNote = userFullName + "接受本文件";
			        		Map<String, String> props = new HashMap<String, String>();
						props.put("it:updateMemo", versionNote);
						props.put("it:docStatus", ErpUtil.STATUS_APPROVED);
						//如果是逕行修改
						if(approveType.equals("modifySelf")){
				        		versionNote = userFullName + "逕行修改本文件";
				        		props = new HashMap<String, String>();
							props.put("it:updateMemo", versionNote);
							props.put("it:docStatus", ErpUtil.STATUS_MODIFY_SELF);
						}
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, alfDoc.getContentStream(), versionNote);
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
						//關閉本任務
						ErpUtil.closeTask(t.taskId, userFullName, conn);
				        }else if(approveType.equals("reject")){
				        	ContentStream cs = alfDoc.getContentStream();
			        		String versionNote = userFullName + "退回";
			        		Map<String, String> props = new HashMap<String, String>();
			        		props.put("it:updateMemo", versionNote);
						props.put("it:docStatus", ErpUtil.STATUS_REJECTED);
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, versionNote);
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
						//
						Task ct = ErpUtil.getTask(t.taskId, conn);
						//
						Task nt = new Task();
						nt.taskType = ErpUtil.TASK_MODIFY_REPORT;
						nt.assignee = ct.issuer;
						nt.reportNo = ct.reportNo;
						nt.examNo = ct.examNo;
						nt.bankName = ct.bankName;
						nt.fileId = alfDoc.getId();
						nt.fileName = ct.fileName;
						nt.versionNo = alfDoc.getVersionLabel();
						nt.issuer = userFullName;
						nt.stageFolderId = ct.stageFolderId;
						nt.stageName = ct.stageName;
						nt.taskComment = rejectReason;
						nt.deadLine = replyDueDate;
						//
						ErpUtil.addTask(nt, 0, conn, themeDisplay);
						//關閉本任務
						ErpUtil.closeTask(t.taskId, userFullName, conn);
				        }
		        	}
	        	}
	        	//清空批次待審事項
	        	actionRequest.getPortletSession(true).removeAttribute("vecBatchTask");
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/batchApproval.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}

	//批次代審處理
	@SuppressWarnings("resource")
	public void doBatchApprovingStaff(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
			//
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	String approveType = ParamUtil.getString(uploadRequest, "approveType");
	        	String rejectReason = ParamUtil.getString(uploadRequest, "rejectReason");
	        	String replyDueDate = ErpUtil.genDateFromAUI(ParamUtil.getString(uploadRequest, "yearDue"),
	        											ParamUtil.getString(uploadRequest, "monDue"),
	        											ParamUtil.getString(uploadRequest, "dayDue"));
	        	//
	        	if(approveType.equalsIgnoreCase("")){
	        		throw new Exception("請指定接受、退回或逕行修改。");
	        	}else if(!approveType.equalsIgnoreCase("reject")){
	        		if(!rejectReason.equals("")){
	        			throw new Exception("如非退件，請勿輸入退件原因或檔案。");
	        		}
	        	}else{
	        		if(rejectReason.equals("")){
	        			throw new Exception("請輸入退件原因。");
	        		}
	        	}
	        	@SuppressWarnings("unchecked")
			Vector<Task> vecBatchTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecBatchTaskStaff");
	        	if(vecBatchTask.size() == 0) throw new Exception("尚未指定批次審核的項目。");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	for(Task t: vecBatchTask){
		        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
		        	AlfrescoDocument alfDoc = null;
		        	try{
		        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + t.fileName);
		        	}catch(Exception _ex){
		        	}
		        	//
		        	if(alfDoc == null) throw new Exception("檔案 " + t.fileName + " 找不到。");
		        	//如果已領出者先取消領出
		        	if(alfDoc.isVersionSeriesCheckedOut()) alfDoc.cancelCheckOut();
		        	//
			        if(approveType.equals("approve") || approveType.equals("modifySelf")){
			        	//更新
		        		String versionNote = userFullName + "接受本文件";
		        		Map<String, String> props = new HashMap<String, String>();
					props.put("it:updateMemo", versionNote);
					props.put("it:docStatus", ErpUtil.STATUS_APPROVED);
					//如果是逕行修改
					if(approveType.equals("modifySelf")){
			        		versionNote = userFullName + "逕行修改本文件";
			        		props = new HashMap<String, String>();
						props.put("it:updateMemo", versionNote);
						props.put("it:docStatus", ErpUtil.STATUS_MODIFY_SELF);
					}
					//
					ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, alfDoc.getContentStream(), versionNote);
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					//關閉本任務
					ErpUtil.closeTask(t.taskId, userFullName, conn);
			        }else if(approveType.equals("reject")){
			        	ContentStream cs = alfDoc.getContentStream();
		        		String versionNote = userFullName + "退回";
		        		Map<String, String> props = new HashMap<String, String>();
		        		props.put("it:updateMemo", versionNote);
					props.put("it:docStatus", ErpUtil.STATUS_REJECTED);
					//
					ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, versionNote);
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					//
					Task ct = ErpUtil.getTask(t.taskId, conn);
					//
					Task nt = new Task();
					nt.taskType = ErpUtil.TASK_MODIFY_REPORT;
					nt.assignee = ct.issuer;
					nt.reportNo = ct.reportNo;
					nt.examNo = ct.examNo;
					nt.bankName = ct.bankName;
					nt.fileId = alfDoc.getId();
					nt.fileName = ct.fileName;
					nt.versionNo = alfDoc.getVersionLabel();
					nt.issuer = userFullName;
					nt.stageFolderId = ct.stageFolderId;
					nt.stageName = ct.stageName;
					nt.taskComment = rejectReason;
					nt.deadLine = replyDueDate;
					//
					ErpUtil.addTask(nt, 0, conn, themeDisplay);
					//關閉本任務
					ErpUtil.closeTask(t.taskId, userFullName, conn);
			        }
	        	}
	        	//清空批次待審事項
	        	actionRequest.getPortletSession(true).removeAttribute("vecBatchTaskStaff");
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/batchApprovalStaff.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}	
	
	
	//批次檔案交付
	public void batchCompleteFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	String completeMemo = ParamUtil.getString(uploadRequest,"completeMemo");
	        	HashMap<String, String> htPaName = new HashMap<String, String>();
	        	for(int i=0; i < 10; i++){
	        		String paName = "fileName" + i;
	        		if(uploadRequest.getSize(paName) > 0 && !htPaName.containsKey(uploadRequest.getFileName(paName))){
	        			htPaName.put(uploadRequest.getFileName(paName), paName);		//key: 上傳之檔名, value: 參數名
	        		}
	        	}
	        	if (htPaName.size() ==0)  throw new Exception("請指定所要交付審核(上傳)的檔案。");
	        	//
	        	Vector<Task> vecTask = new Vector<Task>();
	                if(actionRequest.getPortletSession(true).getAttribute("vecTask") != null){
	                	vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
	                }
	    		//所屬助檢的待辦
	                Vector<Task> vecTaskStaff = new Vector<Task>();
	                if(actionRequest.getPortletSession(true).getAttribute("vecTaskStaff") != null){
	            	    vecTaskStaff = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTaskStaff");
	                }	        	
	        	conn = ds.getConnection();
	        	//找出相對的任務
	        	HashMap<String, Task> htTask = new HashMap<String, Task>();
	        	//
	        	Iterator<String> it = htPaName.keySet().iterator();
	        	while(it.hasNext()){
	        		String fileName = it.next();
	        		//
	        		for(Task t: vecTask){
	        			if(fileName.equalsIgnoreCase(t.fileName) && 
	        					(t.taskType.equalsIgnoreCase(ErpUtil.TASK_FILL_REPORT) || t.taskType.equalsIgnoreCase(ErpUtil.TASK_MODIFY_REPORT))){
	        				htTask.put(fileName, t);
	        				break;
	        			}
	        		}
	        		for(Task t: vecTaskStaff){
	        			if(fileName.equalsIgnoreCase(t.fileName) &&
	        					(t.taskType.equalsIgnoreCase(ErpUtil.TASK_FILL_REPORT) || t.taskType.equalsIgnoreCase(ErpUtil.TASK_MODIFY_REPORT))){
	        				htTask.put(fileName, t);
	        				break;
	        			}
	        		}
	        	}
	        	//
	        	if(htTask.size() == 0) throw new Exception("上傳的檔案找不到相對待填寫(或修改)的任務。");
		        //開始更新檔案
	        	int ccComplete = 0;
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	it = htTask.keySet().iterator();
	        	while(it.hasNext()){
	        		String fileName = it.next();
	        		Task t = htTask.get(fileName);
	        		//
	        		AlfrescoDocument alfDoc = null;
				try{
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
				}catch(Exception _ex){
					throw new Exception("找不到檔名為 " + fileName + " 之實體檔案。");
				}
				if(alfDoc != null){
					AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
					if(!alfDoc.isLatestVersion()){
			        		try{
			        			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + alfDoc.getName());
			        			if(alfDoc != null){
			        				fdStage = (AlfrescoFolder)alfDoc.getParents().get(0);
			        			}
			        		}catch(Exception _ex){
			        			_ex.printStackTrace();
			        			throw new Exception("無法取得 " + alfDoc.getName() + " 之最新版本。");
			        		}
			        	}
					AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
					//
					String paName = htPaName.get(fileName);
					File file = uploadRequest.getFile(paName);
					InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
					//FileInputStream fis = new FileInputStream(file);
					MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	
					Map<String, String> props = new HashMap<String, String>();
					props.put("it:updateMemo", "交付檔案");
					props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
					//
					ContentStream cs = new ContentStreamImpl(fileName, null, mimeTypesMap.getContentType(file), is);
					//
					if(this.taskCompleteImpl(t, cs, alfDoc, fdStage, fdExam, alfSessionAdmin, themeDisplay, userFullName, completeMemo, conn)){
						ccComplete++;
					}
					//
					is.close();		
				}
			}
			//
			String msg = "已成功交付 " + ccComplete + " 個新檔案。";
			actionRequest.setAttribute("successMsg", msg);
			SessionMessages.add(actionRequest, "success");
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//核可文件
	/*
	public void doApproveFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			//
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	Task t = ErpUtil.getTask(taskId, conn);
	        	//
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	
	        	AlfrescoDocument alfDoc = null;
	        	Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().equals(t.fileName)){
	        			alfDoc = (AlfrescoDocument)co;
	        			break;
	        		}
	        	}
	        	//
	        	if(alfDoc == null) throw new Exception("檔案 " + t.fileName + " 找不到。");
	        	//
		        //更新
        		String versionNote = userFullName + "核可本文件";
        		Map<String, String> props = new HashMap<String, String>();
			props.put("it:updateMemo", versionNote);
			props.put("it:docStatus", ErpUtil.STATUS_APPROVED);
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, alfDoc.getContentStream(), versionNote);
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			//關閉本任務
			ErpUtil.closeTask(taskId, conn);
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/approval.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}

	//核退文件
	public void doRejectFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			conn = ds.getConnection();
			//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			int taskId  = Integer.parseInt(ParamUtil.getString(uploadRequest,"taskId"));

			Task t = ErpUtil.getTask(taskId, conn);
	        	actionRequest.getPortletSession(true).setAttribute("currTask", t);
	        	//String stageFolderId = ParamUtil.getString(uploadRequest,"stageFolderId");
	        	String rejectReason = ParamUtil.getString(uploadRequest,"rejectReason");
	        	if(rejectReason.equals("")) throw new Exception("請說明退回原因。");
	        	String fileNameUpload = uploadRequest.getFileName("fileNameUpload");
	        	//
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	if(!fileNameUpload.equals("") && !fileNameUpload.equalsIgnoreCase(t.fileName)) throw new Exception("準備更新的檔案：" + fileNameUpload + " 與原檔名：" + t.fileName + " 不符。");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	AlfrescoDocument alfDoc = null;
	        	Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().equals(t.fileName)){
	        			alfDoc = (AlfrescoDocument)co;
	        			break;
	        		}
	        	}
	        	//
	        	if(alfDoc == null) throw new Exception("檔案 " + t.fileName + " 找不到。");
	        	if(alfDoc.getVersionLabel().compareTo(t.versionNo) > 0) throw new Exception("檔案：" + t.fileName + " 已被其他人更新，請先更新版本後再處理。");	        	
	        	//
        		//更新檔案
	        	ContentStream cs = alfDoc.getContentStream();
	        	if(!fileNameUpload.equals("")){
	        		InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileNameUpload"));
	        		cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
	        	}
        		String versionNote = userFullName + "退回本文件";
        		Map<String, String> props = new HashMap<String, String>();
        		props.put("it:updateMemo", versionNote);
			props.put("it:docStatus", ErpUtil.STATUS_REJECTED);
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, cs, versionNote);
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			//關閉本任務
			ErpUtil.closeTask(taskId, conn);
			//
			Task ct = ErpUtil.getTask(taskId, conn);
			//
			Task nt = new Task();
			nt.taskType = ErpUtil.TASK_MODIFY_REPORT;
			nt.assignee = ct.issuer;
			nt.reportNo = ct.reportNo;
			nt.examNo = ct.examNo;
			nt.bankName = ct.bankName;
			nt.fileId = alfDoc.getId();
			nt.fileName = ct.fileName;
			nt.versionNo = alfDoc.getVersionLabel();
			nt.issuer = userFullName;
			nt.stageFolderId = ct.stageFolderId;
			nt.stageName = ct.stageName;
			nt.taskComment = rejectReason;
			//
			ErpUtil.addTask(nt, conn, themeDisplay);
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/html/todo/approval.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	//轉請審核
	public void doForwardApproveFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			//
	        	String toApprover = actionRequest.getParameter("toApprover");
	        	String myComment = actionRequest.getParameter("myComment");
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	Task t = ErpUtil.getTask(taskId, conn);
	        	//
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	
	        	AlfrescoDocument alfDoc = null;
	        	Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().equals(t.fileName)){
	        			alfDoc = (AlfrescoDocument)co;
	        			break;
	        		}
	        	}
	        	//
	        	if(alfDoc == null) throw new Exception("檔案 " + t.fileName + " 找不到。");
	        	//
        		if(myComment.equals("")) throw new Exception("請簡述您的審核意見。");
        		//確定審核人存在
        		if(!ErpUtil.isExamUser(toApprover, themeDisplay)) throw new Exception("審核人：" + toApprover + " 不在檢查人員資料庫內。" );
			//
        		String versionNote = userFullName + "的意見：" + myComment +"；並轉請 " + toApprover + " 審核";
        		Map<String, String> props = new HashMap<String, String>();
        		props.put("it:updateMemo", versionNote);
        		props.put("it:docStatus", ErpUtil.STATUS_APPROVING);
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, alfDoc.getContentStream(), versionNote);
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			//
			//關閉本任務
			ErpUtil.closeTask(taskId, conn);
			//
			Task ct = ErpUtil.getTask(taskId, conn);
			//
			Task nt = new Task();
			nt.taskType = ErpUtil.TASK_DELEGATED_REVIEW;
			nt.assignee = toApprover;
			nt.reportNo = ct.reportNo;
			nt.examNo = ct.examNo;
			nt.bankName = ct.bankName;
			nt.fileId = alfDoc.getId();
			nt.fileName = ct.fileName;
			nt.versionNo = alfDoc.getVersionLabel();
			nt.issuer = ct.issuer;
			nt.reIssuer = userFullName;
			nt.stageFolderId = ct.stageFolderId;
			nt.stageName = ct.stageName;
			nt.taskComment = myComment;
			//
			ErpUtil.addTask(nt, conn, themeDisplay);
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/approval.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	*/
	
	//
	public void doRefreshFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
	        	int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
	        	//
	        	conn = ds.getConnection();
	        	Task t = ErpUtil.getTask(taskId, conn);
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
	        	AlfrescoDocument alfDoc = null;
	        	try{
	        		alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(fdStage.getPath() + "/" + t.fileName);
	        	}catch(Exception _ex){
	        	}
	        	if(alfDoc == null) throw new Exception("檔案：" + t.fileName + " 在" + fdStage.getName() + "階段找不到。" );
	        	//
	        	if(alfDoc.getVersionLabel().compareTo(t.versionNo) > 0){
	        		String sql = "UPDATE todo_list SET file_id=?, version_no=? WHERE task_id=?";
	        		PreparedStatement ps = conn.prepareStatement(sql);
	        		ps.setString(1, alfDoc.getId());
	        		ps.setString(2, alfDoc.getVersionLabel());
	        		ps.setInt(3, taskId);
	        		ps.execute();
	        		//
	        		int cc = 0;
	        		@SuppressWarnings("unchecked")
				Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
	        		for(Task _t: vecTask){
	        			if(_t.taskId == taskId){
	        				_t.fileId = alfDoc.getId();
	        				_t.versionNo = alfDoc.getVersionLabel();
	        				//
	        				vecTask.remove(cc);
	        				vecTask.add(cc, _t);
	        				break;
	        			}
	        			cc++;
	        		}
	        		actionRequest.getPortletSession(true).setAttribute("vecTask", vecTask);
	        	}else if(alfDoc.isVersionSeriesCheckedOut()){		//找到 Working Copy 版本
	        		String pureName = alfDoc.getName().substring(0, alfDoc.getName().lastIndexOf("."));
	        		//
	        		AlfrescoDocument alfDoc2 = null;
	        		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		        	while(it.hasNext()){
		        		CmisObject co = it.next();
		        		if(co.getName().startsWith(pureName) && co.getName().contains("Working Copy")){
		        			alfDoc2 = (AlfrescoDocument)co;
		        			break;
		        		}
		        	}
		        	if(alfDoc2 == null) throw new Exception("檔案：" + alfDoc.getName() + " 已被領出，但找不到領出檔。" );
		        	//
		        	
	        	}else{
				actionRequest.setAttribute("successMsg", "本檔案已是最新版本，無需重新載入。");
				SessionMessages.add(actionRequest, "success");
	        	}
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
		
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			OutputStream out = resResponse.getPortletOutputStream();
			if(resRequest.getParameter("actionType") != null && resRequest.getParameter("actionType").equalsIgnoreCase("batchDownload") ){			//批次下載
				String zipName = resRequest.getParameter("zipName");
				ErpUtil.logUserAction("批次下載待辦檔案", zipName, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//
				ZipArchiveOutputStream zos = new ZipArchiveOutputStream(new BufferedOutputStream(out));
				//zos.setEncoding("UTF-8");
				zos.setEncoding("MS950");
				zos.setCreateUnicodeExtraFields(ZipArchiveOutputStream.UnicodeExtraFieldPolicy.ALWAYS);				
				//
				resResponse.setContentType("application/zip");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (zipName, "utf-8") + "\"");	
				//
				@SuppressWarnings("unchecked")
				Vector<Task> vecTask = (Vector<Task>)resRequest.getPortletSession(true).getAttribute("vecBatchTask");
				for(Task t: vecTask){
					AlfrescoDocument doc = null;
					try{
						doc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
						//
						ZipArchiveEntry entry = new ZipArchiveEntry(doc.getName());
					        entry.setSize(doc.getContentStreamLength());
					        zos.putArchiveEntry(entry);
					        zos.write(IOUtils.toByteArray(doc.getContentStream().getStream()));
					        zos.closeArchiveEntry();						
					}catch(Exception _ex){
					}
				}
				zos.close();
				//
				out.flush();
				out.close();				
			}else{		//單檔下載
				//String stageFolderId = resRequest.getParameter("stageFolderId");
				String fileId = resRequest.getParameter("fileId");
				String stageFolderId = resRequest.getParameter("stageFolderId");
				//
				ErpUtil.logUserAction("下載待辦檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//
				AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
				AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
				//
				CmisObject co = alfSessionAdmin.getObject(fileId);
				Document doc = (Document)co;
				String fileName = doc.getName();
				//AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				//
				//System.out.println("Mine type: " + doc.getContentStreamMimeType());
				resResponse.setContentType(doc.getContentStreamMimeType());
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (doc.getName(), "utf-8") + "\"");
				
				java.io.InputStream is = doc.getContentStream().getStream();
				//
				if(is != null){
					byte[] buffer = new byte[4096];
					int n;
					while ((n = is.read(buffer)) > 0) {
					    out.write(buffer, 0, n);
					}
					//
					out.flush();
					out.close();
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//簽收 doReceiveTask
	/*
	public void doReceiveTask(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			//
			conn = ds.getConnection();
			ErpUtil.logUserAction("簽收檔案", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "UPDATE todo_list SET received=1, received_time=? WHERE task_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, ErpUtil.getCurrDateTime());
			ps.setInt(2, taskId);
			ps.execute();
			//
			conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	*/
	
	
	//領出任務檔案
	/*
	public void doCheckOut(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			//
			Task t = null;
			@SuppressWarnings("unchecked")
			Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
			for(Task _t: vecTask){
				if(_t.taskId == taskId){
					t = _t;
					break;
				}
			}
			//
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			if(fdOut == null) throw new Exception("領出暫存區目錄尚未設定。");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoDocument alfDoc2Out = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			ChkOutObj coo = ErpUtil.doCheckOut(alfSessionAdmin, fdExam, fdStage, alfDoc2Out, fdOut, themeDisplay);
			//
			conn = ds.getConnection();
			if(!coo.fileId.equals("")){
				String sql = "UPDATE todo_list SET file_id=?, " +		//1	
										"file_name=?, " +		//2
										"file_name_out=?, " +	//3
										"version_no=?, " +		//4
										"web_dav_url=? " +		//5
									" WHERE task_id=?";			//6
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, coo.fileId);
				ps.setString(2, coo.fileName);
				ps.setString(3, coo.fileNameOut);
				ps.setString(4, coo.versionNo);
				ps.setString(5, coo.webDavURL);
				ps.setInt(6, t.taskId);
				ps.execute();
			}
			conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

	//繳回
	public void doCheckIn(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			Task t = null;
			@SuppressWarnings("unchecked")
			Vector<Task> vecTask = (Vector<Task>)actionRequest.getPortletSession(true).getAttribute("vecTask");
			for(Task _t: vecTask){
				if(_t.taskId == taskId){
					t = _t;
					break;
				}
			}
			//	
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
			//領出暫存區
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			//
			alfDoc = ErpUtil.doCheckIn(alfSessionAdmin, fdStage, alfDoc, fdOut, themeDisplay);
			//
			conn = ds.getConnection();
			String sql = "UPDATE todo_list SET file_id=?, " +		//1	
										"file_name=?, " +		//2
										"file_name_out=?, " +	//3
										"version_no=?, " +		//4
										"web_dav_url=? " +		//5
									" WHERE task_id=?";			//6
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, alfDoc.getId());
			ps.setString(2, alfDoc.getName());
			ps.setString(3, "");
			ps.setString(4, alfDoc.getVersionLabel());
			ps.setString(5, "");
			ps.setInt(6, t.taskId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}	
	*/
	
	//檢查任務檔案是否還在 db 與 file Sync
	/*
	public void doSyncData(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			//boolean updated = false;
			Collec collec = this.getTasks(userFullName, conn);
			for(Task t: collec.vecTask){
				AlfrescoDocument alfDoc = null;
				try{
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
				}catch(Exception _ex){
				}
				if(alfDoc == null || !alfDoc.isLatestVersion()){	//再找看看是否有新版本
					boolean found = false;
					//
					AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
					Iterator<CmisObject> it = fdStage.getChildren().iterator();
					while(it.hasNext()){
						AlfrescoDocument _alfDoc = (AlfrescoDocument)it.next();
						if(_alfDoc.getName().equals(t.fileName) && _alfDoc.getVersionLabel().compareTo(t.versionNo) >= 0){
							//更新
							String sql = "UPDATE todo_list SET done=0, file_id=?, version_no=? WHERE task_id=?";
							PreparedStatement ps = conn.prepareStatement(sql);
							ps.setString(1, _alfDoc.getId());
							ps.setString(2, _alfDoc.getVersionLabel());
							ps.setInt(3, t.taskId);
							ps.execute();
							//
							found = true;
							break;
						}
					}
					//最後還是找不到
					if(!found){
						String sql = "UPDATE todo_list SET done=1, update_memo='系統找不到檔案' WHERE task_id=?";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.setInt(1, t.taskId);
						ps.execute();
					}
				}
			}
			//未簽收
			for(Task t: collec.vecTaskQue){
				AlfrescoDocument alfDoc = null;
				try{
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(t.fileId);
				}catch(Exception _ex){
				}
				if(alfDoc == null || !alfDoc.isLatestVersion()){	//再找看看是否有新版本
					boolean found = false;
					//
					AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(t.stageFolderId);
					Iterator<CmisObject> it = fdStage.getChildren().iterator();
					while(it.hasNext()){
						AlfrescoDocument _alfDoc = (AlfrescoDocument)it.next();
						if(_alfDoc.getName().equals(t.fileName) && _alfDoc.getVersionLabel().compareTo(t.versionNo) >= 0){
							//更新
							String sql = "UPDATE todo_list SET done=0, file_id=?, version_no=? WHERE task_id=?";
							PreparedStatement ps = conn.prepareStatement(sql);
							ps.setString(1, _alfDoc.getId());
							ps.setString(2, _alfDoc.getVersionLabel());
							ps.setInt(3, t.taskId);
							ps.execute();
							//
							found = true;
							break;
						}
					}
					//最後還是找不到
					if(!found){
						String sql = "UPDATE todo_list SET done=1, update_memo='系統找不到檔案' WHERE task_id=?";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.setInt(1, t.taskId);
						ps.execute();
					}
				}
			}
			conn.close();
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	*/
	
	//執行批次審核
	/*
	public void doBatchApprove(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String batchIds = actionRequest.getParameter("batchIds");
			if(batchIds.equals("")) throw new Exception("請先指定批次審核的事項。");
			//
			conn = ds.getConnection();
			//
			Vector<Task> vecBatchTask = new Vector<Task>();
			String[] ids = batchIds.split("#");
			for(String id: ids){
				Task t = ErpUtil.getTask(Integer.parseInt(id), conn);
				vecBatchTask.add(t);
			}
			if(vecBatchTask.size() == 0) throw new Exception("找不到批次審核的的事項。");
			//
			 actionRequest.setAttribute("successMsg", "您已進入批次審核畫面。");
			 SessionMessages.add(actionRequest, "success");
			 //
			 actionRequest.getPortletSession(true).setAttribute("vecBatchTask", vecBatchTask);
	        	conn.close();
	        	//
	        	actionResponse.setRenderParameter("jspPage", "/html/todo/batchApproval.jsp");
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	*/
	
}

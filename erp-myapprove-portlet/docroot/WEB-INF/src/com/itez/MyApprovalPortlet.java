package com.itez;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;

import com.aspose.words.HeaderFooter;
import com.aspose.words.HeaderFooterType;
import com.aspose.words.HorizontalAlignment;
import com.aspose.words.License;
import com.aspose.words.Paragraph;
import com.aspose.words.RelativeHorizontalPosition;
import com.aspose.words.RelativeVerticalPosition;
import com.aspose.words.SaveFormat;
import com.aspose.words.Section;
import com.aspose.words.Shape;
import com.aspose.words.ShapeType;
import com.aspose.words.VerticalAlignment;
import com.aspose.words.WrapType;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyApprovalPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		//載入 Aspose.Words License
	   		License license = new License();
	   		license.setLicense("Aspose.Words.lic");
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			//
			Vector<Req> vecReqApprove = this.getMyApprovalAwait(userFullName, conn);
			conn.close();
			//
			renderRequest.getPortletSession(true).setAttribute("vecReqApprove", vecReqApprove);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//待我審核的記錄
	private Vector<Req> getMyApprovalAwait(String userFullName, Connection conn) throws Exception{
		Vector<Req> vecReq = new Vector<Req>();
		//
		String sql = "SELECT * FROM doc_req WHERE approve_by = ? AND (done IS NULL OR done =0) ORDER BY req_date DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Req q = new Req();
			q.reqId = rs.getInt("req_id");
			q.reqBy = rs.getString("req_by");
			if(rs.getString("report_no") != null){
				q.reportNo = rs.getString("report_no");
			}
			if(rs.getString("file_name") != null){
				q.fileName = rs.getString("file_name");
			}
			q.reqBrief = rs.getString("req_brief");
			if(rs.getString("approve_by") != null){
				q.approveBy = rs.getString("approve_by");
			}
			q.reqDate = rs.getString("req_date");
			//
			vecReq.add(q);
		}
		//
		return vecReq;
	}	
	
	//進入申調審核
	public void doReqApproval(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			//
			Req q = new Req();
			@SuppressWarnings("unchecked")
			Vector<Req> vecReqApprove = (Vector<Req>)actionRequest.getPortletSession(true).getAttribute("vecReqApprove");
			for(Req _q: vecReqApprove){
				if(_q.reqId == reqId){
					q = _q;
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("req", q);
			//取得報告可調閱的加密檔案
			HashMap<String, String> htFile = new HashMap<String, String>();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			String sql = "SELECT B.folder_id, B.exam_no FROM reports A, exams B WHERE A.report_id = B.report_id AND A.report_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, q.reportNo);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String folderIdExam = rs.getString(1);
				String examNo = rs.getString(2);
				AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(folderIdExam);
				Iterator<CmisObject> it = fdExam.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("歸檔")){
						AlfrescoFolder fdStage = (AlfrescoFolder)co;
						Iterator<CmisObject> it2 = fdStage.getChildren().iterator();
						while(it2.hasNext()){
							CmisObject co2 = it2.next();
							if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
									(co2.getName().toLowerCase().endsWith("doc") || co2.getName().toLowerCase().endsWith("xls"))){
								AlfrescoDocument alfDoc = (AlfrescoDocument)co2;
								if(alfDoc.getPropertyValue("cm:author") != null && alfDoc.getPropertyValue("cm:author").toString().contains("加密")){
									String path = examNo + "/" + fdStage.getName() + "/" + co2.getName();
									htFile.put(path, co2.getId());
								}
							}
						}
						break;
					}
				}
			}
			//
			Vector<String> vecFile = new Vector<String>();
			Iterator<String> it = htFile.keySet().iterator();
			while(it.hasNext()){
				String path = it.next();
				vecFile.add(path);
			}
			Collections.sort(vecFile);
			//System.out.println("Available files: " + vecFile.size());
			//
			actionRequest.getPortletSession(true).setAttribute("req", q);
			actionRequest.getPortletSession(true).setAttribute("htFile", htFile);
			actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
			//
			conn.close();
			actionResponse.setRenderParameter("jspPage", "/html/myapproval/approval.jsp");
			//通知申調者
			String subject = "文件調閱審核通知";
			String msgText = q.reqBy + " 您好;" + "\n";
			msgText += "\n";
			msgText += "您所申調的文件(詳如下)已審核完成：" + "\n";
			msgText += "\n";
			msgText += "申調日期：" + q.reqDate + "\n";
			msgText += "報告編號：" + q.reportNo + "\n";
			msgText += "檔案名稱：" + q.fileName + "\n";
			msgText += "申調原因：" + q.reqBrief + "\n";
			msgText += "\n";
			msgText += "\n";
			msgText += "檢查報告處理系統 敬上";
			//
			//發送電子郵件
			String toAddress = ErpUtil.getUserMailAddress(themeDisplay, q.reqBy);
			if(!toAddress.equals("")){
				ErpUtil.sendMail(toAddress, subject, msgText);
			}
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//拒絕提供
	public void doRejectReq(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			//
			conn = ds.getConnection();
			String sql = "UPDATE doc_req SET done=1, approve_type=2 WHERE req_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reqId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//提供加密後文件
	@SuppressWarnings({ "unchecked", "unused" })
	public void doResponseReq(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		Connection connMS = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			Req q = new Req();
			Vector<Req> vecReqApprove = (Vector<Req>)actionRequest.getPortletSession(true).getAttribute("vecReqApprove");
			for(Req _q: vecReqApprove){
				if(_q.reqId == reqId){
					q = _q;
					break;
				}
			}
			//
			HashMap<String, String> htFile = (HashMap<String, String>)actionRequest.getPortletSession(true).getAttribute("htFile");
			Vector<String> vecFile = (Vector<String>)actionRequest.getPortletSession(true).getAttribute("vecFile");
			//準備入選的檔案ID
			HashMap<String, String> htNode = new HashMap<String, String>();	//值為印+修
			//
			for(int i=0; i < vecFile.size(); i++){
				String key = "INDEX_" + i;
				String prt = "PRINT_" + i;
				String edt = "EDIT_" + i;
				if(actionRequest.getParameter(key) != null && actionRequest.getParameter(key).equalsIgnoreCase("true")){
					String val = "";	//內定不能印與修
					if(actionRequest.getParameter(prt) != null && actionRequest.getParameter(prt).equalsIgnoreCase("true")){
						val += "1";
					}else{
						val += "0";
					}
					if(actionRequest.getParameter(edt) != null && actionRequest.getParameter(edt).equalsIgnoreCase("true")){
						val += "1";
					}else{
						val += "0";
					}
					htNode.put(htFile.get(vecFile.get(i)), val);
				}
			}
			if(htNode.size() == 0) throw new Exception("請指定可供調閱的文件。");
			//
			conn = ds.getConnection();
			connMS = this.getMsConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			Iterator<String> it = htNode.keySet().iterator();
			while(it.hasNext()){
				String fid = it.next();
				String privi = htNode.get(fid);
				//
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fid);
				//
				int seqNo5A = 0;
				String sql = "SELECT seqno FROM exd05a WHERE reportno=? AND filename=?";
				PreparedStatement ps = connMS.prepareStatement(sql);
				ps.setString(1, q.reportNo);
				ps.setString(2, alfDoc.getName());
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					seqNo5A = rs.getInt(1);
				}
				rs.close();
				//
				int canRead = 1;
				int canPrint = 0;
				int canEdit = 0;
				//
				sql = "INSERT INTO exd12a (seqno5a, reportno, userid, filename, orgtype, p_read ";
				if(privi.equals("11")){
					sql += ", p_print, p_edit) VALUES (?,?,?,?,?,?,?,?)";
					canPrint = 1;
					canEdit = 1;
				}else if(privi.equals("10")){
					sql += ", p_print) VALUES (?,?,?,?,?,?,?)";
					canPrint = 1;
				}else if(privi.equals("01")){
					sql += ", p_edit) VALUES (?,?,?,?,?,?,?)";
					canEdit = 1;
				}else if(privi.equals("00")){
					sql += ") VALUES (?,?,?,?,?,?)";
				}
				//
				ps = connMS.prepareStatement(sql);
				ps.setInt(1, seqNo5A);
				ps.setString(2, q.reportNo);
				ps.setString(3, ErpUtil.getUserScreenName(themeDisplay, q.reqBy));
				ps.setString(4, alfDoc.getName());
				ps.setString(5, "FEB");			//檢查局人員之申請
				ps.setInt(6, 0);
				if(privi.equals("11")){
					ps.setInt(7, 0);
					ps.setInt(8, 0);
				}else if(privi.equals("10")){
					ps.setInt(7, 0);
				}else if(privi.equals("01")){
					ps.setInt(7, 0);
				}
				ps.execute();
				//
				sql = "INSERT INTO req_files (req_id, file_id, file_name, can_read, can_print, can_edit) VALUES (?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, reqId);
				ps.setString(2, alfDoc.getId());
				ps.setString(3, alfDoc.getName());
				ps.setInt(4, canRead);
				ps.setInt(5, canPrint);
				ps.setInt(6, canEdit);
				ps.execute();
			}
			//
			String sql = "UPDATE doc_req SET done=1, approve_type=1, approve_time=?  WHERE req_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, ErpUtil.getCurrDateTime());
			ps.setInt(2, reqId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	
	//提供文件
	@SuppressWarnings({ "unchecked", "unused" })
	public void doResponseReq4PDF(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reqId = Integer.parseInt(actionRequest.getParameter("reqId"));
			Req q = new Req();
			Vector<Req> vecReqApprove = (Vector<Req>)actionRequest.getPortletSession(true).getAttribute("vecReqApprove");
			for(Req _q: vecReqApprove){
				if(_q.reqId == reqId){
					q = _q;
					break;
				}
			}
			//
			HashMap<String, String> htFile = (HashMap<String, String>)actionRequest.getPortletSession(true).getAttribute("htFile");
			Vector<String> vecFile = (Vector<String>)actionRequest.getPortletSession(true).getAttribute("vecFile");
			//準備入選的檔案ID
			Vector<String> vecNode = new Vector<String>();
			//
			for(int i=0; i < vecFile.size(); i++){
				String key = "INDEX_" + i;
				if(actionRequest.getParameter(key) != null && actionRequest.getParameter(key).equalsIgnoreCase("true")){
					vecNode.add(htFile.get(vecFile.get(i)));
				}
			}
			if(vecNode.size() == 0) throw new Exception("請指定可供調閱的文件。");
			//Liferay 申調區
			Folder fdReq = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, "申調區");
			//浮水印文字
			String watermarkText = q.reqBy + ErpUtil.getCurrDateTime();
			//
			conn = ds.getConnection();
			String passwd = this.genPasswd(6);		//隨機密碼
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			for(String fid: vecNode){
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fid);
				//複製至調閱區
				String pureName = alfDoc.getName().substring(0, alfDoc.getName().lastIndexOf("."));
				
				DateFormat formatter= new SimpleDateFormat("yyyyMMddHHmmss");
				formatter.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
				String destName = pureName + "_" + formatter.format(new Date()) + ".pdf";
				//加上文字浮水印
				com.aspose.words.Document aDoc = new com.aspose.words.Document(alfDoc.getContentStream().getStream());
				//
				Shape watermark = new Shape(aDoc, ShapeType.TEXT_PLAIN_TEXT);
			        // Set up the text of the watermark.
			        watermark.getTextPath().setText(watermarkText);
			        watermark.getTextPath().setFontFamily("新細明體");
			        watermark.setWidth(500);
			        watermark.setHeight(100);
			        // Text will be directed from the bottom-left to the top-right corner.
			        watermark.setRotation(-40);
			        // Remove the following two lines if you need a solid black text.
			        watermark.getFill().setColor(Color.LIGHT_GRAY); // Try LightGray to get more Word-style watermark
			        watermark.setStrokeColor(Color.LIGHT_GRAY); // Try LightGray to get more Word-style watermark
			        // Place the watermark in the page center.
			        watermark.setRelativeHorizontalPosition(RelativeHorizontalPosition.PAGE);
			        watermark.setRelativeVerticalPosition(RelativeVerticalPosition.PAGE);
			        watermark.setWrapType(WrapType.NONE);
			        watermark.setVerticalAlignment(VerticalAlignment.CENTER);
			        watermark.setHorizontalAlignment(HorizontalAlignment.CENTER);
			        // Create a new paragraph and append the watermark to this paragraph.
			        Paragraph watermarkPara = new Paragraph(aDoc);
			        watermarkPara.appendChild(watermark);
			        // Insert the watermark into all headers of each document section.
			        for (Section sect : aDoc.getSections()){
			            // There could be up to three different headers in each section, since we want the watermark to appear on all pages, insert into all headers.
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_PRIMARY);
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_FIRST);
			            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_EVEN);
			        }
				//
				OutputStream os = new ByteArrayOutputStream();
			        aDoc.save(os, SaveFormat.PDF);
			        
				if(os == null) throw new Exception("無法轉換成 Pdf 的輸出串流。");
				//
				InputStream is=new ByteArrayInputStream(((ByteArrayOutputStream)os).toByteArray());
				//防拷,防印
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				PdfReader reader = new PdfReader(is, null);
				PdfStamper stamper = new PdfStamper(reader, baos);
				stamper.setEncryption(passwd.getBytes(),
									passwd.getBytes(),
									PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING,
									PdfWriter.STANDARD_ENCRYPTION_128);
				stamper.close();
				reader.close();
				//存入 Liferay
				FileEntry fe = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(),
														themeDisplay.getScopeGroupId(),
														fdReq.getFolderId(),
														destName,
														"application/pdf",
														destName,
														"", 
														null,
														baos.toByteArray(), 
														new ServiceContext());
				String sql = "INSERT INTO req_files (req_id, file_id, file_name) VALUES (?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, reqId);
				ps.setLong(2, fe.getFileEntryId());
				ps.setString(3, pureName + ".pdf");
				ps.execute();
			}
			//
			String sql = "UPDATE doc_req SET done=1, approve_type=1, passwd = ?, approve_time=?  WHERE req_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, passwd);
			ps.setString(2, ErpUtil.getCurrDateTime());
			ps.setInt(3, reqId);
			ps.execute();
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private static void insertWatermarkIntoHeader(Paragraph watermarkPara, Section sect, int headerType) throws Exception{
	        HeaderFooter header = sect.getHeadersFooters().getByHeaderFooterType(headerType);
	        if (header == null){
	            // There is no header of the specified type in the current section, create it.
	            header = new HeaderFooter(sect.getDocument(), headerType);
	            sect.getHeadersFooters().add(header);
	        }
	        // Insert a clone of the watermark into the header.
	        header.appendChild(watermarkPara.deepClone(true));
	}	
	
	//產出文數字密碼
	private String genPasswd(int size){
		    String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    String ret = "";
		    int length = chars.length();
		    for (int i = 0; i < size; i ++){
		        ret += chars.split("")[ (int) (Math.random() * (length - 1)) ];
		    }
		    return ret;
	}	

	//取得 MS SQL 連線
	private Connection getMsConnection() throws Exception{
		String msDriver = PropsUtil.get("sqlserver.driver"); 		//"net.sourceforge.jtds.jdbc.Driver";
		String msURL = PropsUtil.get("sqlserver.url");			//"jdbc:jtds:sqlserver://localhost:1433/traffic";
		String msUser = PropsUtil.get("sqlserver.user");			//"sa";
                String msPasswd= PropsUtil.get("sqlserver.password");
                //
		Class.forName(msDriver);
                return DriverManager.getConnection(msURL, msUser, msPasswd);
	}

}

<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.Req"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

  <%
  			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            	//
            	Req q = new Req();
            	if(renderRequest.getPortletSession(true).getAttribute("req") != null){
            		q = (Req)renderRequest.getPortletSession(true).getAttribute("req");
            	}
            	//
            	Vector<String> vecFile = new Vector<String>();
            	if(renderRequest.getPortletSession(true).getAttribute("vecFile") != null){
            		vecFile = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecFile");
            	}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doResponseReqURL"  name="doResponseReq" >
</portlet:actionURL>		

<portlet:actionURL var="doRejectReqURL"  name="doRejectReq" >
	<portlet:param name="reqId" value="<%=String.valueOf(q.reqId) %>"/>
</portlet:actionURL>		

<portlet:renderURL var="goBackURL">
  	<portlet:param name="jspPage"  value="/html/myapproval/view.jsp"/>
</portlet:renderURL>

<script type="text/javascript">
</script>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/req32.png">&nbsp;調閱審核(申調資料如下)
	</aui:column>
</aui:layout>
<hr/>

<aui:layout>
	<aui:column>	
		<aui:input name="reportNo" label="報告編號" value="<%=q.reportNo %>"  cssClass="td" readonly="readonly" />
	</aui:column>
	<aui:column>	
		<aui:input name="reqBy" label="申請人" value="<%=q.reqBy %>"  cssClass="td" readonly="readonly"/>
	</aui:column>
</aui:layout>
<aui:layout>	
	<aui:column>	
		<aui:input name="fileName" label="文件檔名" value="<%=q.fileName %>"  cssClass="td"  size="60" readonly="readonly"/>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column  cssClass="td"  columnWidth="90">
		<aui:input type="textarea" name="reqBrief" label="調閱文件說明"  value="<%=q.reqBrief %>" style="width: 100%;"  rows="4" readonly="readonly" />	
	</aui:column>
</aui:layout>
<br>

<aui:fieldset label="本案報告歸檔區可供調閱的文件列表(可多選)">
<aui:form action="<%=doResponseReqURL%>" method="post" name="<portlet:namespace />fm">
	<aui:input name="reqId"  type="hidden"  value="<%=q.reqId %>"/>
	<%for(String path: vecFile){
			String fldName = "INDEX_" + vecFile.indexOf(path);
			String printName = "PRINT_" + vecFile.indexOf(path);
			String editName = "EDIT_" + vecFile.indexOf(path);
	%>
		<aui:layout>
			<aui:column>
				<aui:input type="checkbox"  name="<%=fldName%>"  label="<%=path %>" />
			</aui:column>
			<aui:column>
				<aui:input type="checkbox"  name="<%=printName%>"  label="可列印" />
			</aui:column>
			<aui:column>
				<aui:input type="checkbox"  name="<%=editName%>"  label="可修改" />
			</aui:column>
		</aui:layout>
	<%} %>
	<aui:button-row>
		<aui:button  onClick="<%=goBackURL.toString() %>"   value="返回上頁" />
		<aui:button onClick="<%=doRejectReqURL.toString() %>"  value="無文件可供調閱" />
		<%if(vecFile.size() > 0){ %>
			<aui:button type="submit"  value="核可勾選文件供調閱" />
		<%} %>	
	</aui:button-row>
</aui:form>
</aui:fieldset>



<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.ErpUtil"%>
<%@ page import= "com.itez.Req"%>

<portlet:defineObjects />
 
<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	Vector<Req> vecReqApprove = new Vector<Req>();
	if(renderRequest.getPortletSession(true).getAttribute("vecReqApprove") != null){
		vecReqApprove = (Vector<Req>)renderRequest.getPortletSession(true).getAttribute("vecReqApprove");
	}
	
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/req32.png">&nbsp;待您(<%=ErpUtil.getCurrUserFullName(themeDisplay) %>)審核的調閱申請
	</aui:column>
</aui:layout>
<hr/>
<liferay-ui:search-container emptyResultsMessage="您沒有任何待審核的調閱申請" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecReqApprove, searchContainer.getStart(), searchContainer.getEnd());
			total = vecReqApprove.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Req" keyProperty="reqId" modelVar="reqApprove">
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/req24.png"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="申請日期" property="reqDate" />
		<liferay-ui:search-container-column-text name="報告編號" property="reportNo" />
		<liferay-ui:search-container-column-text name="文件檔名" property="fileName" />
		<liferay-ui:search-container-column-text name="調閱說明" property="reqBrief"/>
		<liferay-ui:search-container-column-text name="申請人" property="reqBy"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminApprove.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
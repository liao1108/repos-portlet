<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "com.itez.Param"%>

<portlet:defineObjects />

<%
	//String bankNo="", bankName="", signUpId="", signUpName="", signUpEmail="", showMsg="";
	//String signUpPasswd="", signUpPasswd2="";
	Param p = new Param();
	if(renderRequest.getAttribute("param") != null) p = (Param)renderRequest.getAttribute("param"); 
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doChangeSignUpURL" name="doChangeSignUp">
</portlet:actionURL>

 <style type="text/css">
  	.th {
    			font-family:微軟正黑體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    		height: 40px
    		}
  	td {
    			font-family:微軟正黑體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:red;
    		}
    	r{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkred;
    		}		
  </style>
  
<portlet:resourceURL var="checkAccountURL">
</portlet:resourceURL>

 <%
	String rootPaht = renderRequest.getContextPath();
%>
<script src="<%=rootPaht%>/js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript">
		function checkAccount() {
			var signUpId =$("#<portlet:namespace />signUpId" ).val();
			var param = [{ name: "signUpId", value: signUpId}];
			$.ajax({
					type : "POST",
					url : '<portlet:resourceURL/>',
					data : param,
					dataType : "text",
					success : function(data) {
						$('#result').text(data);
					},
					error : function() {
					}
			});
		}
</script>
  
 
<aui:form action="<%=doChangeSignUpURL%>" method="post"  id="<portlet:namespace />fm"  name="<portlet:namespace />fm">
<table width = '100%'>
	<tr>
		<td><img src='<%=renderRequest.getContextPath()%>/images/SignUp.png'></td>
		<td class='th' colspan=3>請輸入 您變更後的資料。</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>服務之金融機構代號：</td>
		<td align='left'>
			<aui:input type="text"  name="bankNo"   label = "" value="<%=p.bankNo%>" readonly="readonly" />
		</td>
		<td>(不可變更)</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>服務之金融機構名稱：</td>
		<td align='left'>
			<aui:input type="text"  name="bankName"   label = ""  value="<%=p.bankName%>" />
		</td>
		<td>(如: 國泰世華銀行)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td nowrap>使用者帳號(代名)：</td>
		<td align= LEFT>
			<aui:input type="text"  id = "signUpId"  name="signUpId"   label = ""  value="<%=p.signUpId%>" readonly="readonly" />
		</td>
		<td width="90%" class="r">(不可變更)</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>姓名：</td>
		<td align= LEFT>
			<%if(p.isBank){ %>
				<aui:input type="text"  name="signUpName"  label = ""   value="<%=p.signUpName%>"  readonly="readonly"/>
			<%}else{ %>
				<aui:input type="text"  name="signUpName"  label = ""   value="<%=p.signUpName%>"/>
			<%} %>	
		</td>
		<td class="r">
			<%if(p.isBank){ %>
				(不可變更)
			<%}else{ %>
				&nbsp;
			<%} %>
		</td>
	</tr>
	
	<%if(!p.isBank){ %>
		<tr>
			<td>&nbsp;</td>
			<td nowrap>職稱：</td>
			<td align= LEFT>
				<aui:input type="text"  name="jobTitle"  label = ""   value="<%=p.jobTitle%>"/>
			</td>
			<td class="r">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td nowrap>電話：</td>
			<td align= LEFT>
				<aui:input type="text"  name="phoneNo"  label = ""   value="<%=p.phoneNo%>"/>
			</td>
			<td class="r">&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td nowrap>對外ip：</td>
			<td align= LEFT>
				<aui:input type="text"  name="ip"  label = ""   value="<%=p.ip%>" readonly="readonly"/>
			</td>
			<td class="r">(不可變更)</td>
		</tr>
	<%} %>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>密碼設定：</td>
		<td align=LEFT>
			<aui:input type="PASSWORD"  name="signUpPasswd"  label = ""   value="<%=p.signUpPasswd%>" />
		<td>(最少8碼)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td nowrap>密碼確認：</td>
		<td align=LEFT>
			<aui:input type="PASSWORD"  name="signUpPasswd2"  label = ""   value="<%=p.signUpPasswd2%>" />
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>eMail：</td>
		<td align=LEFT>
			<aui:input type="text"  name="signUpEmail"  label = ""   value="<%=p.signUpEmail%>" />
		</td>
		<td>
			<aui:input type="hidden"  name="isBank"  label = ""   value="<%=p.isBank%>" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>	
		<td colspan=2>
			<aui:layout>
				<aui:column>
					<aui:input type='submit'  name="SUBMIT"  label=""   value='確定變更'/>
				</aui:column>
			</aui:layout>
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
</aui:form>

package com.itez;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.PasswordTrackerLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


public class ChangeSignupPortlet extends MVCPortlet {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
    
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    	requestPath = getInitParameter("requestPath");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String userNo = themeDisplay.getUser().getScreenName();
			if(userNo == null || userNo.equals("")) return;
			//
			conn = ds.getConnection();
			//this.doPoke(conn, "auditors");
			
			String sql = "SELECT * FROM auditors WHERE user_no = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				Param p = new Param();
				p.isBank = true;
				if(rs.getString("bank_no") != null){
					p.bankNo = rs.getString("bank_no").trim();
				}
				if(rs.getString("bank_name") != null){
					p.bankName = rs.getString("bank_name").trim();
				}
				
				p.signUpId = userNo;
				
				if(rs.getString("user_name") != null){
					p.signUpName = rs.getString("user_name").trim();
				}
				if(rs.getString("email") != null){
					p.signUpEmail = rs.getString("email").trim();
				}
				//
				renderRequest.setAttribute("param", p);
			}else{
				sql = "SELECT * FROM fin_staff WHERE login_id = ?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, userNo);
				rs = ps.executeQuery();
				if(rs.next()){
					Param p = new Param();
					p.isBank = false;
					p.bankNo = rs.getString("fin_id");
					p.bankName = rs.getString("fin_name");
					p.signUpId = userNo;
					p.signUpName = rs.getString("staff_name");
					p.signUpEmail = rs.getString("staff_email");
					
					p.jobTitle = rs.getString("staff_title");
					p.phoneNo = rs.getString("staff_phone");
					p.ip = rs.getString("static_ip");
					//
					renderRequest.setAttribute("param", p);
				}else{
					throw new Exception("使用者代號 " + userNo + " 不存在！");
				}
			}
			rs.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	public void doChangeSignUp(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//long companyId = themeDisplay.getCompanyId();
	   		//com.liferay.portal.service.ServiceContext serviceContext = new com.liferay.portal.service.ServiceContext();
	   		//
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			conn = ds.getConnection();
			
			Param p = new Param();
			//
			if(actionRequest.getParameter("bankNo") != null){
				p.bankNo = actionRequest.getParameter("bankNo");
			}
			if(actionRequest.getParameter("bankName") != null){
				p.bankName = actionRequest.getParameter("bankName") ;
			}
			if(actionRequest.getParameter("signUpName") != null){
				p.signUpName = actionRequest.getParameter("signUpName");
			}
			if(actionRequest.getParameter("signUpId") != null){
				p.signUpId = actionRequest.getParameter("signUpId"); 
			}
			if(actionRequest.getParameter("signUpPasswd") != null){
				p.signUpPasswd = actionRequest.getParameter("signUpPasswd");
			}
			if(actionRequest.getParameter("signUpPasswd2") != null){
				p.signUpPasswd2 =  actionRequest.getParameter("signUpPasswd2");
			}
			if(actionRequest.getParameter("signUpEmail") != null){
				p.signUpEmail = actionRequest.getParameter("signUpEmail");
			}
			if(actionRequest.getParameter("isBank") != null){
				if(actionRequest.getParameter("isBank").equalsIgnoreCase("true")){
					p.isBank = true;
				}else{
					p.isBank = false;
				}
			}
			//-------------------------
			if(actionRequest.getParameter("jobTitle") != null){
				p.jobTitle = actionRequest.getParameter("jobTitle");
			}
			if(actionRequest.getParameter("phoneNo") != null){
				p.phoneNo = actionRequest.getParameter("phoneNo");
			}
			//
			actionRequest.setAttribute("param", p);
			//
			if(p.bankNo.equals("")) throw new Exception("請輸入銀行代號。");
			if(p.bankName.equals("")) throw new Exception("請輸入銀行名稱。");
			if(p.signUpName.equals("")) throw new Exception("請輸入您的姓名。");
			if(p.signUpId.equals("")) throw new Exception("請輸入您要使用的代名。");
			if(p.signUpPasswd.equals("") || p.signUpPasswd2.equals("")) throw new Exception("請輸入您要使用密碼。");
			if(p.signUpEmail.equals("")) throw new Exception("請輸入您聯絡的電子郵件信箱。");
			//
			if(!p.signUpPasswd.equals(p.signUpPasswd2)){
				throw new Exception("密碼不一致，請重新鍵入。");
			}
			if(p.signUpPasswd.length() < 8){
				throw new Exception("密碼長度最小為 8 碼。");
			}
			if(p.isBank){
				//確定User是否在DB2上
				DefaultHttpClient httpClient = new DefaultHttpClient();
				URIBuilder builder = new URIBuilder();
				builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
											.setParameter("type", "Auth")
											.setParameter("bankNo", p.bankNo)
											.setParameter("userName", URLEncoder.encode(p.signUpName, "UTF-8"));
				URI uri = builder.build();
				HttpPost httpPost = new HttpPost(uri);
				HttpResponse response = httpClient.execute(httpPost);
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = br.readLine();
				//
				if(response.getEntity() != null ) {
					httpPost.abort();
				}
				if(line == null || !line.equalsIgnoreCase("T")) throw new Exception("您登錄的資料不存於單一申報稽核人員資料庫內，請再確認資料是否正確。");
			}
			//變更Liferay
			UserData u = new UserData();
			u.screenName = p.signUpId;
			u.firstName = p.signUpName;
			u.passwd = p.signUpPasswd;
			u.email = p.signUpEmail;
			//
			this.updateUser(actionRequest, u);
			//
			if(p.isBank){
				String sql = "UPDATE auditors SET bank_no=?," +		//1
											" bank_name=?," +		//2
											" user_name=?," +		//3
											//" passwd=?," +		//4
											" email=? " +			//4
										" WHERE user_no = ?";		//5
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, p.bankNo);
				ps.setString(2, p.bankName);
				ps.setString(3, p.signUpName);
				//ps.setString(4, p.signUpPasswd);
				ps.setString(4, p.signUpEmail);
				ps.setString(5, p.signUpId);
				ps.execute();
			}else{
				String sql = "UPDATE fin_staff SET fin_id = ?," +	//1
										" fin_name = ?," +			//2
										" staff_name = ?," +		//3
										//" passwd=?," +			//4
										" staff_email = ?, " +		//4
										" staff_title = ?, " +		//5
										" staff_phone = ? " +		//6
									" WHERE login_id = ?";			//7
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, p.bankNo);
				ps.setString(2, p.bankName);
				ps.setString(3, p.signUpName);
				//ps.setString(4, p.signUpPasswd);
				ps.setString(4, p.signUpEmail);
				ps.setString(5, p.jobTitle);
				ps.setString(6, p.phoneNo);
				ps.setString(7, p.signUpId);
				ps.execute();
			}
			//
			String strBody = "您好：\n" +
			   					"您於本局網站留言板的註冊資訊已更新完成。\n" +
			   					"\n金管會檢查局";
			//
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URIBuilder builder = new URIBuilder();
			builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
									.setParameter("type", "SendMail")
									.setParameter("mailNo", p.signUpEmail)
									.setParameter("mailText", URLEncoder.encode(strBody, "UTF-8"));
			URI uri = builder.build();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse response = httpClient.execute(httpPost);
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = br.readLine();
			//
			if(response.getEntity() != null ) {
				httpPost.abort();
			}
			//
			SessionMessages.add(actionRequest, "success");
			actionRequest.setAttribute("successMsg", "您的帳號已變更完成。");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private void updateUser(ActionRequest request, UserData userData) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long creatorUserId = themeDisplay.getUserId(); 	// default liferay user
		long companyId = themeDisplay.getCompanyId(); 	// default company
		boolean autoPassword = false;
		String password1 = userData.passwd;
		String password2 = userData.passwd;
		boolean autoScreenName = false;
		String screenName = userData.screenName;
		String emailAddress = userData.email;
		long facebookId = 0;
		String openId = "";
		//Locale locale = themeDisplay.getLocale();
		String firstName = userData.firstName;
		String middleName = "";
		String lastName = ".";
		int prefixId = 0;
		int suffixId = 0;
		boolean male = true;    
		int birthdayMonth = 1;
		int birthdayDay = 1;
		int birthdayYear = 1970;
		String jobTitle = "";

		long[] groupIds = {};
		long[] organizationIds = {};
		long[] roleIds = {};
		long[] userGroupIds = {};

		boolean sendEmail = false;

		//ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
		com.liferay.portal.service.ServiceContext serviceContext = new com.liferay.portal.service.ServiceContext();
		User user = null;
		try{
			user = UserLocalServiceUtil.getUserByEmailAddress(companyId, emailAddress);
		}catch(Exception _ex){
			user = null;
		}
		if(user == null || user.getScreenName().equals(userData.screenName)){
			//user = UserLocalServiceUtil.getUserByScreenName(companyId, userData.screenName);
			User u = themeDisplay.getUser();
			//UserLocalServiceUtil.updateEmailAddress(u.getUserId(), u.getPassword(), userData.email, userData.email);
			if(!PasswordTrackerLocalServiceUtil.isSameAsCurrentPassword(u.getUserId(), userData.passwd)){
				try{
					u = UserLocalServiceUtil.updatePassword(u.getUserId(), userData.passwd, userData.passwd, false);
				}catch(Exception ex){
					throw new Exception("密碼變更失敗。");
				}
			}
			//
			u.setFirstName(firstName);
			u.setEmailAddress(userData.email);
			//
			UserLocalServiceUtil.updateUser(u, false);
		}else{
			throw new Exception("電子郵件 " + emailAddress + " 已有人使用！");
		}
		//return user;
	}
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
				if(str.contains("com.itez")){
					ret += ("\n" + str);
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		return ret;
	}

	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}

}

<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<portlet:defineObjects />

<%
	String signUpId = ""; 

	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="checkSignIdURL" name="checkSignId">
</portlet:actionURL>

<portlet:actionURL var="backToViewURL">
	<portlet:param name="jspPage" value="/html/signup/view.jsp" />
</portlet:actionURL>

 <style type="text/css">
  	.th {
    			font-family:微軟正黑體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    		height: 40px
    		}
  	td {
    			font-family:微軟正黑體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:red;
    		}		
  </style>
<aui:form action="<%=checkSignIdURL%>" method="post" name="<portlet:namespace />fm">
<table width = '100%' cellspacing="5" cellpadding="5">
	<tr>
		<td><img src='<%=renderRequest.getContextPath()%>/images/SignUp.png'></td>
		<td colspan="3"  class='th' >請輸入您要申請的代名：</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
		<td colspan="3" align= LEFT>
			<aui:input type="text"  name="signUpId"   label = ""  value="<%=signUpId%>" />
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>	
		<td>
				<aui:input type='submit'  name="doCheck"  label=""   value='執行檢查'/>
		</td>
		<td>&nbsp;</td>
		<td width="95%" align="left">		
				<aui:button onClick="<%=backToViewURL %>"  value="回上頁" />
		</td>
	</tr>
</table>
</aui:form>

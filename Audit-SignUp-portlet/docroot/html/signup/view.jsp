<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "com.itez.Param"%>

<portlet:defineObjects />

<%
	//String bankNo="", bankName="", signUpId="", signUpName="", signUpEmail="", showMsg="";
	//String signUpPasswd="", signUpPasswd2="";
	Param p = new Param();
	if(renderRequest.getAttribute("param") != null) p = (Param)renderRequest.getAttribute("param"); 
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doSignUpURL" name="doSignUp">
</portlet:actionURL>

<portlet:renderURL var="checkSignIdURL">
	<portlet:param name="jspPage" value="/html/signup/checkId.jsp" />
</portlet:renderURL>

 <style type="text/css">
  	.th {
    			font-family:微軟正黑體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    		height: 40px
    		}
  	td {
    			font-family:微軟正黑體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:red;
    		}
    	r{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkred;
    		}
  .button{
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}	
    				
  </style>
  
<portlet:resourceURL var="checkAccountURL">
</portlet:resourceURL>

 <%
	String rootPaht = renderRequest.getContextPath();
%>
<script src="<%=rootPaht%>/js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript">
		function checkAccount() {
			var signUpId =$("#<portlet:namespace />signUpId" ).val();
			var param = [{ name: "signUpId", value: signUpId}];
			$.ajax({
					type : "POST",
					url : '<portlet:resourceURL/>',
					data : param,
					dataType : "text",
					success : function(data) {
						//$('#result').text(data);
						alert(data);
					},
					error : function() {
					}
			});
		}
</script>
 
 <aui:form action="<%=doSignUpURL%>" method="post"  id="<portlet:namespace />fm"  name="<portlet:namespace />fm">
<table width = '100%'>
	<tr>
		<td><img src='<%=renderRequest.getContextPath()%>/images/SignUp.png'></td>
		<td class='th' colspan=3>您是金融機構已向本局單一申報窗口申報之稽核人員嗎？如果是，您可以再於下列欄位輸入您的資料註冊，日後相關之提問或建議，本局會以最快速方式作業，
如涉及個案問題，本網站會妥善處理。</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td nowrap>服務之金融機構代號：</td>
		<td align='left'>
			<aui:input type="text"  name="bankNo"   label = "" value="<%=p.bankNo%>" />
		</td>
		<td>(係指本局單一申報窗口「非財報申報系統」之金融機構代碼，如國泰世華銀行013)</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
		<td nowrap>服務之金融機構名稱：</td>
		<td align='left'>
			<aui:input type="text"  name="bankName"   label = ""  value="<%=p.bankName%>" />
		</td>
		<td>(如: 國泰世華銀行)</td>
	</tr>
	
	<tr>
	<td>&nbsp;</td>
		<td nowrap>姓名：</td>
		<td align= LEFT>
			<aui:input type="text"  name="signUpName"  label = ""   value="<%=p.signUpName%>" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
			<td nowrap>使用者名稱(代名)：</td>
			<td align= LEFT>
				<aui:input type="text"  id = "signUpId"  name="signUpId"   label = ""  value="<%=p.signUpId%>" />
			</td>
			<td width="90%" align="left">
				<aui:button onClick="checkAccount();" value="檢查代名是否可用" cssClass="button"/>
				<label  id="result"  name="result"></label>
			</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td nowrap>密碼設定：</td>
		<td align=LEFT>
			<aui:input type="PASSWORD"  name="signUpPasswd"  label = ""   value="<%=p.signUpPasswd%>" />
		<td>(最少8碼)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td nowrap>密碼確認：</td>
		<td align=LEFT>
			<aui:input type="PASSWORD"  name="signUpPasswd2"  label = ""   value="<%=p.signUpPasswd2%>" />
		<td>&nbsp;</td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
		<td nowrap>eMail：</td>
		<td align=LEFT>
			<aui:input type="text"  name="signUpEmail"  label = ""   value="<%=p.signUpEmail%>" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>	
		<td colspan=2>
			<aui:layout>
				<aui:column>
					<aui:input type='submit'  name="applyAccount"  label=""   value='申請' cssClass="button"/>
				</aui:column>
			</aui:layout>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><img src="<%=renderRequest.getContextPath() %>/images/tip.png"></td>
		<td class='tb' colspan=3>
			<ol>
				<li>
					金融機構稽核人員輸入「服務之金融機構代號」、「服務之金融機構名稱」、「姓名」等必填資料後；如無法成功申請，請電洽(02)89680031或89680522。
				</li>
				<li>
					非金融機構內部稽核人員如有內部稽核作業相關問題，請逕以電子郵件將您的寶貴意見透過<a href='http://fscmail.fsc.gov.tw/fsc-sps/SPSB/SPSB01002.aspx'>民意信箱</a>寄送。
				</li>
			</ol>
		</td>
		
	</tr>		
</table>
</aui:form>

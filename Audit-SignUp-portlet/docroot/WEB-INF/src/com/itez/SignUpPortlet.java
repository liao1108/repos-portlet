package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.webcache.WebCachePoolUtil;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SignUpPortlet
 */
public class SignUpPortlet extends MVCPortlet {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
    
	//Utils utils = new Utils();
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    		requestPath = getInitParameter("requestPath");
	    		//
	    		Connection conn = ds.getConnection();
	    		String sql = "CREATE TABLE IF NOT EXISTS auditors (" +
					    						  "bank_no varchar(30) NOT NULL," +
					    						  "bank_name varchar(50) NOT NULL," +
					    						  "user_name varchar(50) NOT NULL," +
					    						  "user_no varchar(50) NOT NULL," +
					    						  "passwd varchar(50) NOT NULL," +
					    						  "email varchar(50) NOT NULL," +
					    						  "verified_ok int(1) DEFAULT NULL," +
					    					" PRIMARY KEY (bank_no, user_no))";
	    		PreparedStatement ps = conn.prepareStatement(sql);
	    		ps.execute();
	    		//
	    		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void checkSignId(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			if(actionRequest.getParameter("signUpId") != null && !actionRequest.getParameter("signUpId").equals("")){
				String signUpId = actionRequest.getParameter("signUpId");
				Connection conn = ds.getConnection();
				this.doPoke(conn,"auditors");
				//
				String sql = "SELECT user_no FROM auditors WHERE user_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, signUpId);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					actionRequest.setAttribute("successMsg", "代名 " + signUpId + " 已有人使用。");
				}else{
					actionRequest.setAttribute("successMsg", "代名 " + signUpId + " 尚未被使用。");
				}
				conn.close();
			}
			//
			SessionMessages.add(actionRequest, "success");
			actionResponse.setRenderParameter("jspPage", "/html/signup/checkId.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doSignUp(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		long companyId = themeDisplay.getCompanyId();
	   		//com.liferay.portal.service.ServiceContext serviceContext = new com.liferay.portal.service.ServiceContext();
	   		//
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			conn = ds.getConnection();
			this.doPoke(conn, "auditors");
			//
			Param p = new Param();
			//
			if(actionRequest.getParameter("bankNo") != null){
				p.bankNo = actionRequest.getParameter("bankNo");
			}
			if(actionRequest.getParameter("bankName") != null){
				p.bankName = actionRequest.getParameter("bankName") ;
			}
			if(actionRequest.getParameter("signUpName") != null){
				p.signUpName = actionRequest.getParameter("signUpName");
			}
			if(actionRequest.getParameter("signUpId") != null){
				p.signUpId = actionRequest.getParameter("signUpId"); 
			}
			if(actionRequest.getParameter("signUpPasswd") != null){
				p.signUpPasswd = actionRequest.getParameter("signUpPasswd");
			}
			if(actionRequest.getParameter("signUpPasswd2") != null){
				p.signUpPasswd2 =  actionRequest.getParameter("signUpPasswd2");
			}
			if(actionRequest.getParameter("signUpEmail") != null){
				p.signUpEmail = actionRequest.getParameter("signUpEmail");
			}
			//
			actionRequest.setAttribute("param", p);
			//
			if(p.bankNo.equals("")) throw new Exception("請輸入銀行代號。");
			if(p.bankName.equals("")) throw new Exception("請輸入銀行名稱。");
			if(p.signUpName.equals("")) throw new Exception("請輸入您的姓名。");
			if(p.signUpId.equals("")) throw new Exception("請輸入您要使用的代名。");
			if(p.signUpPasswd.equals("") || p.signUpPasswd2.equals("")) throw new Exception("請輸入您要使用密碼。");
			if(p.signUpEmail.equals("")) throw new Exception("請輸入您聯絡的電子郵件信箱。");
			//檢查使用代名邏輯
			this.checkScreenName(p.signUpId);
			//
			String sql = "SELECT user_no FROM auditors WHERE user_no = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, p.signUpId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("代名 " + p.signUpId + " 已有人使用。");
			}
			sql = "SELECT * FROM fin_staff WHERE login_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.signUpId);
			rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("代名 " + p.signUpId + " 已有人使用。");
			}
			//檢查是否重複申請
			sql = "SELECT 1 FROM auditors WHERE bank_no=? AND user_name = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, p.bankNo);
			ps.setString(2, p.signUpName);
			rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("您已經申請過帳號，無法再申請其他帳號。");
			}
			//
			if(!p.signUpPasswd.equals(p.signUpPasswd2)){
				throw new Exception("密碼不一致，請重新鍵入。");
			}
			//
			if(p.signUpPasswd.length() < 8){
				throw new Exception("密碼最短長度為八位文數字。");
			}
			//清除 Cache
			CacheRegistryUtil.clear();
			MultiVMPoolUtil.clear();
			WebCachePoolUtil.clear();
			//檢查電子郵件是否已用
			boolean emailExisted = false;
			try{
				UserLocalServiceUtil.getUserByEmailAddress(companyId, p.signUpEmail);
				emailExisted = true;
			}catch(Exception _ex){
			}
			if(emailExisted)	 throw new Exception("電子郵件 " + p.signUpEmail + " 已有人使用了。");
			//確定User是否在DB2上
			//CloseableHttpClient httpClient = HttpClients.createDefault();
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URIBuilder builder = new URIBuilder();
			builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
										.setParameter("type", "Auth")
										.setParameter("bankNo", p.bankNo)
										.setParameter("userName", URLEncoder.encode(p.signUpName, "UTF-8"));
			URI uri = builder.build();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse response = httpClient.execute(httpPost);
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = br.readLine();
			//
			if(response.getEntity() != null ) {
				httpPost.abort();
			}
			if(line != null && line.equalsIgnoreCase("T")){
				//將User加入Liferay
				UserData u = new UserData();
				u.screenName = p.signUpId;
				u.firstName = p.signUpName;
				u.passwd = p.signUpPasswd;
				u.email = p.signUpEmail;
				//
				if(!this.addUser(actionRequest, u)){
					throw new Exception("稽核留言板系統無法建立本帳號，請先確定代名是否重複。");
				}
				//
				sql = "INSERT INTO auditors (bank_no, bank_name, user_no, user_name, email) VALUES (?,?,?,?,?) ";
				ps = conn.prepareStatement(sql);
				ps.setString(1, p.bankNo);
				ps.setString(2, p.bankName);
				ps.setString(3, p.signUpId);
				ps.setString(4, p.signUpName);
				//ps.setString(5, p.signUpPasswd);
				ps.setString(5, p.signUpEmail);
				ps.execute();
				//
				String strBody = "您好：\n" +
		   					"您已成功註冊至本局網站留言板。\n" +
		   					"\n金管會檢查局";
				//
				httpClient = new DefaultHttpClient();
				builder = new URIBuilder();
				builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
								.setParameter("type", "SendMail")
								.setParameter("mailNo", p.signUpEmail)
								.setParameter("mailText", URLEncoder.encode(strBody, "UTF-8"));
				uri = builder.build();
				httpPost = new HttpPost(uri);
				response = httpClient.execute(httpPost);
				br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				line = br.readLine();
				//
				if(response.getEntity() != null ) {
					httpPost.abort();
				}
				//
				SessionMessages.add(actionRequest, "success");
				//actionRequest.setAttribute("successMsg", "您已成功註冊稽核人員留言板，請點選登入按鈕登入本系統。");
				actionRequest.setAttribute("successMsg", "");
				//
				actionResponse.setRenderParameter("jspPage", "/html/signup/signUpOk.jsp");
		   }else{
		       	throw new Exception("您登錄的資料不存於單一申報稽核人員資料庫內，請再確認資料是否正確。");
		   }
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//檢查使用者代名字串
	private void checkScreenName(String screenName) throws Exception{
		char[] charSet = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
		//
		boolean found = false;
		char c  = screenName.toLowerCase().charAt(0);
		for(char _c: charSet){
			if(c == _c){
				found = true;
				break;
			}
		}
		if(!found) throw new Exception("使用代名請以英文字母開頭。");
		//
		charSet = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
		char[] ss = screenName.toLowerCase().toCharArray();
		for(char s: ss){
			found = false;
			for(char _c: charSet){
				if(s == _c){
					found = true;
					break;
				}
			}
			if(!found) throw new Exception("使用代名請以英文字母與阿拉伯數字命名。");
		}
	}
	
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId(); 	// default company
		//
		resourceResponse.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resourceResponse.getWriter();
		Connection conn = null;
		try{
			if(resourceRequest.getParameter("signUpId") !=null && !resourceRequest.getParameter("signUpId").equals("")){
				String signUpId = resourceRequest.getParameter("signUpId");
				//
				String result = null;
				conn = ds.getConnection();
				this.doPoke(conn, "auditors");
				//
				String sql = "select * from auditors where user_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, signUpId);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					result = "此代名已被使用";
				}else{
					User user = null;
					try{
						user = UserLocalServiceUtil.getUserByScreenName(companyId, signUpId);
					}catch(Exception _ex){
					}
					if(user != null){
						result = "此代名已被使用";
					}else{
						result = "此代名可用";
					}
				}
				out.println(result);
				out.flush();
				out.close();
			}else{
				throw new Exception("請輸入使用者代名 !");
			}
		}catch(Exception ex){
			out.println(ex.getMessage());
			out.flush();
			out.close();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
 	}
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
				if(str.contains("com.itez")){
					ret += ("\n" + str);
				}
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}

    	private boolean addUser(ActionRequest request, UserData userData) throws Exception {
    		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
    		long creatorUserId = themeDisplay.getUserId(); 	// default liferay user
    		long companyId = themeDisplay.getCompanyId(); 	// default company
    		Role role = RoleLocalServiceUtil.getRole(companyId, "Auditor");
    		
    		boolean autoPassword = false;
    		String password1 = userData.passwd;
    		String password2 = userData.passwd;
    		boolean autoScreenName = false;
    		String screenName = userData.screenName;
    		String emailAddress = userData.email;
    		long facebookId = 0;
    		String openId = "";
    		//Locale locale = themeDisplay.getLocale();
    		String firstName = userData.firstName;
    		String middleName = "";
    		String lastName = ".";
    		int prefixId = 0;
    		int suffixId = 0;
    		boolean male = true;    
    		int birthdayMonth = 1;
    		int birthdayDay = 1;
    		int birthdayYear = 1970;
    		String jobTitle = "";

    		long[] groupIds = {};
    		long[] organizationIds = {};
    		long[] roleIds = {role.getRoleId()};
    		long[] userGroupIds = {};

    		boolean sendEmail = false;
    		
    		//ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
    		com.liferay.portal.service.ServiceContext serviceContext = new com.liferay.portal.service.ServiceContext();
    		try{
    			UserLocalServiceUtil.addUser(creatorUserId,		//10196,
										 	companyId,		//10154,
										 	autoPassword,
										 	password1,
										 	password2,
										 	autoScreenName,
										 	screenName,
										 	emailAddress,
										 	facebookId,
										 	openId,
										 	LocaleUtil.getDefault(),
										 	firstName,
										 	middleName,
										 	lastName,
										 	prefixId,
										 	suffixId,
										 	male,
										 	birthdayMonth,
										 	birthdayDay,
										 	birthdayYear,
										 	jobTitle,
										 	groupIds,
										 	organizationIds,
										 	roleIds,
										 	userGroupIds,
										 	sendEmail,
										 	serviceContext);
    			return true;
    		}catch(Exception ex){
    			return false;
    		}
    	}

    	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}
}

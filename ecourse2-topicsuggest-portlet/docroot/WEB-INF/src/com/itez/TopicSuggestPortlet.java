package com.itez;

import com.liferay.portal.kernel.log.Log;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jdbc.pool.DataSource;

/**
 * Portlet implementation class TopicSuggestPortlet
 */
public class TopicSuggestPortlet extends GenericPortlet {
	ECourseUtils utils = new ECourseUtils();
	DataSource ds = null;
	String tableSchema = "";
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	editJSP = getInitParameter("edit-jsp");
	    	
	    	tableSchema = getInitParameter("table_schema");
	    	//
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	renderResponse.setContentType("text/html");
    	//
    	if(renderRequest.getAttribute("submitted") != null){
    		this.include(viewJSP, renderRequest, renderResponse);
    		return;
    	}

    	//
    	String errMsg = "";
    	Connection conn = null;
    	//
    	String ratingArchitecture = "";
    	try{
			//Check id rating table exist
    		conn = ds.getConnection();
   			//System.out.println("Generating table topic_rating ...");
   			String sql = "CREATE TABLE IF NOT EXISTS overall_rating (rating_time VARCHAR(50) NOT NULL, " +
   										"architecture_score int," +
   										"layout_score int, " +
   										"usability_score int," +
   										"layout_comment VARCHAR(200)," +
   										"else_comment VARCHAR(200)," +
   										"overall_score int, PRIMARY KEY (rating_time))";
   			PreparedStatement ps = conn.prepareStatement(sql);
   			ps.execute();
   			//
   			if(renderRequest.getParameter("rating_architecture") != null){
   				ratingArchitecture = renderRequest.getParameter("rating_architecture");
   			}
    	}catch (Exception ex){
    		if(errMsg.equals("")) errMsg += "\n";
    		String itezErr = utils.getItezErrorCode(ex);
        	errMsg +=  itezErr;
        	System.out.println(itezErr);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
        }
    	renderRequest.setAttribute("errMsg", errMsg);
    	//
   		include(editJSP, renderRequest, renderResponse);
    }

    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
		String errMsg = "";
		Connection conn = null;
		try{
			//System.out.println("Receiving submit ...");
			int ratingArchitecture = 0;
	    	if(actionRequest.getParameter("rating_architecture") != null){
	    		try{
	    			ratingArchitecture = Integer.parseInt(actionRequest.getParameter("rating_architecture").trim());
	    		}catch(Exception _ex){
	    			
	    		}
	    	}
			int ratingLayout = 0;
	    	if(actionRequest.getParameter("rating_layout") != null){
	    		try{
	    			ratingLayout = Integer.parseInt(actionRequest.getParameter("rating_layout").trim());
	    		}catch(Exception _ex){
	    			
	    		}
	    	}
			int ratingUsability = 0;
	    	if(actionRequest.getParameter("rating_usability") != null){
	    		try{
	    			ratingUsability = Integer.parseInt(actionRequest.getParameter("rating_usability").trim());
	    		}catch(Exception _ex){
	    			
	    		}
	    	}
	    	String commentLayout = "";
	    	if(actionRequest.getParameter("comment_layout") != null){
	    		commentLayout = actionRequest.getParameter("comment_layout");
	    	}
	    	String commentElse = "";
	    	if(actionRequest.getParameter("comment_else") != null){
	    		commentElse = actionRequest.getParameter("comment_else");
	    	}
			int ratingOverall = 0;
	    	if(actionRequest.getParameter("rating_overall") != null){
	    		try{
	    			ratingOverall = Integer.parseInt(actionRequest.getParameter("rating_overall").trim());
	    		}catch(Exception _ex){
	    			
	    		}
	    	}
			//
	    	if(ratingArchitecture > 0){
				String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
			    Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
				String currDateTime = sdf.format(cal.getTime());
				//
	    		conn = ds.getConnection();
	    		String sql = "INSERT INTO overall_rating (rating_time, architecture_score, layout_score, usability_score, layout_comment, else_comment, overall_score) VALUES (?,?,?,?,?,?,?)";
	    		PreparedStatement ps = conn.prepareStatement(sql);
	    		ps.setString(1, currDateTime);
	    		ps.setInt(2, ratingArchitecture);
	    		ps.setInt(3, ratingLayout);
	    		ps.setInt(4, ratingUsability);
	    		ps.setString(5, commentLayout);
	    		ps.setString(6, commentElse);
	    		ps.setInt(7, ratingOverall);
	    		ps.execute();
	    	}	    		
		}catch(Exception ex){
			ex.printStackTrace();
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
        actionRequest.setAttribute("errMsg", errMsg);
        actionRequest.setAttribute("submitted", "true");
    	//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}
    
    protected void include(String path, RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;
    protected String editJSP;

    private static Log _log = LogFactoryUtil.getLog(TopicSuggestPortlet.class);

}

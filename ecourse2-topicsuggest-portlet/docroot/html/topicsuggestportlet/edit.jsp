<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
<%}%>

<form id="myComment" method="POST" action="<portlet:actionURL/>">
<table width="100%" border="0">
	<tr>
		<td align="center"><img src="<%=renderRequest.getContextPath()%>/images/suggest_title.png"/></td>
	</tr>
	<tr>
		<td align="left"><img src="<%=renderRequest.getContextPath()%>/images/layout.png"/></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="1">
				<tr style="height:35px;">
					<td align="center">項目</td>
					<td align="center" width="17%"><img src="<%=renderRequest.getContextPath()%>/images/star_5.png"/></td>
					<td align="center" width="17%"><img src="<%=renderRequest.getContextPath()%>/images/star_4.png"/></td>
					<td align="center" width="17%"><img src="<%=renderRequest.getContextPath()%>/images/star_3.png"/></td>
					<td align="center" width="17%"><img src="<%=renderRequest.getContextPath()%>/images/star_2.png"/></td>
					<td align="center" width="17%"><img src="<%=renderRequest.getContextPath()%>/images/star_1.png"/></td>
				</tr>
				<tr style="height:35px;">
					<td valign="middle"><img src="<%=renderRequest.getContextPath()%>/images/point_blue.png"/>平台架構</td>
					<td align="center"><input type="radio" name="rating_architecture" value="5" checked/></td>
					<td align="center"><input type="radio" name="rating_architecture" value="4"/></td>
					<td align="center"><input type="radio" name="rating_architecture" value="3"/></td>
					<td align="center"><input type="radio" name="rating_architecture" value="2"/></td>
					<td align="center"><input type="radio" name="rating_architecture" value="1"/></td>
				</tr>
				<tr style="height:35px;">	
					<td valign="middle"><img src="<%=renderRequest.getContextPath()%>/images/point_blue.png"/>版面設計</td>
					<td align="center"><input type="radio" name="rating_layout" value="5" checked/></td>
					<td align="center"><input type="radio" name="rating_layout" value="4"/></td>
					<td align="center"><input type="radio" name="rating_layout" value="3"/></td>
					<td align="center"><input type="radio" name="rating_layout" value="2"/></td>
					<td align="center"><input type="radio" name="rating_layout" value="1"/></td>
				</tr>
				<tr style="height:35px;">	
					<td valign="middle"><img src="<%=renderRequest.getContextPath()%>/images/point_blue.png"/>使用便利性</td>
					<td align="center"><input type="radio" name="rating_usability" value="5" checked/></td>
					<td align="center"><input type="radio" name="rating_usability" value="4"/></td>
					<td align="center"><input type="radio" name="rating_usability" value="3"/></td>
					<td align="center"><input type="radio" name="rating_usability" value="2"/></td>
					<td align="center"><input type="radio" name="rating_usability" value="1"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td><img src="<%=renderRequest.getContextPath()%>/images/message.png"/></td>
					<td align="left" valign="middle" width="90%">
						<span style="color: DarkBlue; font-family:微軟正黑體; font-size: 16pt;">建議</span>
					</td>		
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<textarea name='comment_layout' cols='100%' rows='4' wrap='SOFT'>&nbsp;</textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td><img src="<%=renderRequest.getContextPath()%>/images/content.png"/></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><a href="/viewresult"><img src="<%=renderRequest.getContextPath()%>/images/view_rating_result.png"/></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td valign="middle"><img src="<%=renderRequest.getContextPath()%>/images/point_green.png"/></td>
					<td align="left"><span style="color: Blue; font-family:微軟正黑體; font-size: 14pt;">
						單元評價：請至「課程目錄」進入主題單元及閱覽教材內容後給予評價。</span></td>
					<td align="left"><a href="/courselist"><img src="<%=renderRequest.getContextPath()%>/images/goto_course_list.png"/></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td><img src="<%=renderRequest.getContextPath()%>/images/message.png"/></td>
					<td align="left" valign="middle">
						<span style="color: DarkBlue; font-family:微軟正黑體; font-size: 16pt;">其他建議：（請敍明主題、單元或項目等）</span>
					</td>		
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<textarea name='comment_else' cols='100%' rows='4' wrap='SOFT'>&nbsp;</textarea>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td><img src="<%=renderRequest.getContextPath()%>/images/rating_overall.png"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr>
					<td>
						<input type="radio" name="rating_overall" value="5" checked>
						<img src="<%=renderRequest.getContextPath()%>/images/face_5.png"/>
						很滿意
					</td>
					<td>
						<input type="radio" name="rating_overall" value="4">
						<img src="<%=renderRequest.getContextPath()%>/images/face_4.png"/>
						滿意
					</td>
					<td>
						<input type="radio" name="rating_overall" value="3">
						<img src="<%=renderRequest.getContextPath()%>/images/face_3.png"/>
						普通
					</td>
					<td>
						<input type="radio" name="rating_overall" value="2">
						<img src="<%=renderRequest.getContextPath()%>/images/face_2.png"/>
						不滿意
					</td>
					<td>
						<input type="radio" name="rating_overall" value="1">
						<img src="<%=renderRequest.getContextPath()%>/images/face_1.png"/>
						很不滿意
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<INPUT TYPE="image" src="<%=renderRequest.getContextPath()%>/images/suggest_submit.png" HEIGHT="58" WIDTH="182" BORDER="0" ALT="確認送出">
		</td>
	</tr>
</table>
</form>
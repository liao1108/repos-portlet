package com.itez;

import java.io.File;
//import java.security.cert.CertificateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
//import javax.net.ssl.HttpsURLConnection;
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
import javax.portlet.PortletException;
import javax.sql.DataSource;

//import org.apache.chemistry.opencmis.client.api.Session;
//import org.apache.chemistry.opencmis.client.api.SessionFactory;
//import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
//import org.apache.chemistry.opencmis.commons.SessionParameter;
//import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class AuthSchedulerPortlet extends MVCPortlet implements MessageListener {
	DataSource ds = null;
	String rootReports = "檢查報告處理區";
	 
	private static Log _log = LogFactory.getLog(AuthSchedulerPortlet.class);
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		//mailSession = (javax.mail.Session) envContext.lookup("mail/Session");
	   		//
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootReports = rs.getString("folder_reports");
	   		}
	   		rs.close();
	   		conn.close();
		}catch(Exception ex){
			_log.error(ex.getMessage());
			//
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	 public void receive(Message arg0) {
		//Connection connAuthenticaMS = null;
		Connection connExamAdminMS = null;
		Connection conn = null;
		try{
			if(ds == null){
				Context initContext = new InitialContext();
		   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		   		ds = (DataSource)envContext.lookup("jdbc/exam");
			}
			conn = ds.getConnection();
			//
			_log.warn("Scheduler run at " + this.getCurrDateTime());
			
			String sql = "INSERT INTO action_log (screen_name, action_time, action_name, from_ip, target_file_name) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, "Liferay");
			ps.setString(2, this.getTimeStamp());
			ps.setString(3, "自動排程作業");
			ps.setString(4, "");
			ps.setString(5, "");
			ps.execute();
			//
			//讀取路徑設定
			String pathSrc = PropsUtil.get("authentica.src.path");
			//String pathDest = PropsUtil.get("authentica.dest.path");
			//
			/*
			HashMap<String, File> htFile = new HashMap<String, File>();
			//
			File folderAuth = new File(pathSrc);
			File[] listReport = folderAuth.listFiles();
			for(File folderReport: listReport){
				if(folderReport.isDirectory()){
					File[] listExam = folderReport.listFiles();
					for(File folderExam: listExam){
						if(folderExam.isDirectory()){
							File[] listFile = folderExam.listFiles();
							for(File f: listFile){
								if(f.isFile()) htFile.put(folderReport.getName(), f);
							}
						}
					}
				}
			}
			*/
			//
			//Session alfSessionAdmin = this.getAlfrescoSessionAdmin();
			
			/*
			//查詢檔案是否加密完成
			connAuthenticaMS = this.getAuthenticaSQLServerConnection();
			Iterator<String> itt = htFile.keySet().iterator();
			while(itt.hasNext()){
				String reportNo = itt.next();
				File f = htFile.get(reportNo);
				//
				sql = "SELECT 1 FROM exd05a WHERE reportno=? " +
												" AND filename=? " +
												" AND (encdate IS NOT NULL AND encdate <> '') " +
												" AND (error IS NULL OR error = '')";
				ps = connAuthenticaMS.prepareStatement(sql);
				ps.setString(1, reportNo);
				ps.setString(2, f.getName());
				ResultSet rs = ps.executeQuery();
				if(rs.next()){			//加密完成
					String examNo = f.getParentFile().getName();
					//
					AlfrescoFolder fdArchive = null;
					AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObjectByPath("/" + rootReports + "/" + reportNo + "/" + examNo);
					Iterator<CmisObject> it = fdExam.getChildren().iterator();
					while(it.hasNext()){
						CmisObject co = it.next();
						if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("歸檔")){
							fdArchive = (AlfrescoFolder)co;
							break;
						}
					}
					if(fdArchive != null){
						AlfrescoDocument alfDoc = null;
						try{
							String nodeFullPath = "/" + rootReports + "/" + reportNo + "/" + examNo + "/" + fdArchive.getName() + "/" + f.getName();
							alfDoc = (AlfrescoDocument)alfSessionAdmin.getObjectByPath(nodeFullPath);
						}catch(Exception _ex){
						}
						if(alfDoc == null){
							Map<String, String> props = new HashMap<String, String>();
							props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
							props.put(PropertyIds.NAME, f.getName());
							props.put("cm:author", "已加密");
	
							String mimeType = "application/msword";
							if(f.getName().toLowerCase().endsWith("xls")){
								mimeType = "application/msexcel";
							}
							//
							InputStream is = new FileInputStream(f);
							ContentStream cs = new ContentStreamImpl(f.getName(), null, mimeType, is);
							// create a major version
							fdArchive.createDocument(props, cs, VersioningState.MAJOR);
							//
							is.close();
						}
						//刪除檔案
						f.delete();
					}
				}
				rs.close();
			}
			*/
			
			//檢查是否需要關閉的報告案
			Calendar cal = Calendar.getInstance();
			String dateCurr = cal.get(Calendar.YEAR) + "-" + String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
			//
			sql = "UPDATE reports SET date_closed=?, closed_by=? WHERE date_to_close = ? AND (date_closed IS NULL OR date_closed='') ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, this.getCurrDateTime());
			ps.setString(2, "auto");
			ps.setString(3, dateCurr);
			ps.execute();
			
			//檢查是否有工作分配表須更新至檢查行政系統
			connExamAdminMS = this.getExamAdminSQLServerConnection();
			//
			/*
			sql = "SELECT A.*, B.report_no FROM exams A, reports B WHERE A.report_id = B.report_id "
							+ " AND B.date_created >= '2017-01-01 00:00:00' "
							+ " AND (B.date_closed IS NULL OR B.date_closed = '') ";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String folderId = rs.getString("folder_id");
				String reportNo = rs.getString("report_no");
				String examNo = rs.getString("exam_no");
				String bankNameShort = rs.getString("bank_name_short");
				String leaderName = rs.getString("leader_name");
				//
				this.syncExamAdminByExamFolder(alfSessionAdmin,
												folderId,
												conn,
												connExamAdminMS,
												reportNo,
												examNo,
												bankNameShort,
												leaderName);
			}
			rs.close();
			*/
			//檢查欄位
			//checkSyncCols(conn);
			//
			List<String> listNode = new ArrayList<>();
			//
			sql = "SELECT * FROM alloc_sync WHERE node_id IS NOT NULL "
												+ " AND (report_no IS NOT NULL AND report_no <> '') "
												+ " AND (exam_no IS NOT NULL AND exam_no <> '') "
												+ " AND (bank_name_short IS NOT NULL AND bank_name_short <> '') "
												+ " AND (screen_name_leader IS NOT NULL AND screen_name_leader <> '') "
												+ " AND (sync_ok IS NULL OR sync_ok <> 1) "	//還未同步者
										+ " ORDER BY exam_update_time DESC, node_id DESC";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String nodeIdFull = rs.getString("node_id");
				String nodeId = trimAlfDocId(nodeIdFull);
				if(listNode.contains(nodeId)) continue;
				//最後的工作分配表版本已同步者不再同步
				//if(rs.getInt("sync_ok") == 1) {
				//	listNode.add(nodeId);
				//	continue;
				//}
				//
				DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
				String syncTime = sdf.format(new java.util.Date());
				try{
					syncAllocWithExamAdminSystem(nodeId,
												connExamAdminMS,
												rs.getString("report_no"),
												rs.getString("exam_no"),
												rs.getString("bank_name_short"),
												rs.getString("screen_name_leader"));
					sql = "UPDATE alloc_sync SET sync_ok = 1, sync_time = ? WHERE node_id = ?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, syncTime);
					ps.setString(2, nodeIdFull);
					ps.execute();
					//
					listNode.add(nodeId);
				}catch(Exception ex){
					sql = "UPDATE alloc_sync SET sync_ok = 2, sync_time = ? WHERE node_id = ?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, syncTime);
					ps.setString(2, nodeIdFull);
					ps.execute();
				}
			}
			conn.close();
		}catch(Exception ex){
			_log.error(ex.getMessage());
			//
			ex.printStackTrace();
		}finally{
			try{
				//if(connAuthenticaMS != null) connAuthenticaMS.close();
			}catch(Exception _ex){
			}
			try{
				if(connExamAdminMS != null) connExamAdminMS.close();
			}catch(Exception _ex){
			}
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}			
		}
	}
	
	private String trimAlfDocId(String alfDocId){
		if(alfDocId.contains(";")){
			return alfDocId.substring(0, alfDocId.lastIndexOf(";"));
		}else{
			return alfDocId;
		}
	}
	
	private void checkSyncCols(Connection conn) throws Exception{
		boolean existReportNo = false;			//report_no
		boolean existExamNo = false;			//exam_no
		boolean existBankShort = false;			//bank_name_short
		boolean existScreenNameLeader = false;	//screen_name_leader
		boolean existSyncTime = false;
		boolean existExamUpdateTime = false;
		//
		String sql = "SELECT * FROM alloc_sync";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			ResultSetMetaData rsMeta = rs.getMetaData();
			int cc = rsMeta.getColumnCount();
			for (int i = 1; i <= cc; i++) {
				String fldName = rsMeta.getColumnName(i);
				if(fldName.equalsIgnoreCase("report_no")) {
					existReportNo = true;
				}else if(fldName.equalsIgnoreCase("exam_no")) {
					existExamNo = true;
				}else if(fldName.equalsIgnoreCase("bank_name_short")) {
					existBankShort = true;
				}else if(fldName.equalsIgnoreCase("screen_name_leader")) {
					existScreenNameLeader = true;
				}else if(fldName.equalsIgnoreCase("sync_time")) {
					existSyncTime = true;					
				}else if(fldName.equalsIgnoreCase("exam_update_time")) {
					existExamUpdateTime = true;
				}
			}
		}
		rs.close();
		ps.close();
		//
		if(!existReportNo) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN report_no VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existExamNo) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN exam_no VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existBankShort) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN bank_name_short VARCHAR(100)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existScreenNameLeader) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN screen_name_leader VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existSyncTime) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN sync_time VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existExamUpdateTime) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN exam_update_time VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}	
		ps.close();		
	}
	
	private void syncAllocWithExamAdminSystem(String nodeId,
												Connection connExamAdminMS,
												String reportNo,
												String examNo,
												String bankNameShort,
												String screenNameLeader) throws Exception{
		String s = "EXEC [spITEZ01] " + reportNo + " " + examNo + " " + bankNameShort + " " + screenNameLeader + " " + nodeId;
		
		_log.warn("Calling Procedure with command: " + s + " ....") ;
		
		CallableStatement cstmt = connExamAdminMS.prepareCall("EXEC [spITEZ01] ?,?,?,?,?");
		cstmt.setString (1, reportNo);
		cstmt.setString (2, examNo);
		cstmt.setString(3, bankNameShort);
		cstmt.setString(4, screenNameLeader);
		cstmt.setString(5, nodeId);
		//
		cstmt.execute();
		_log.warn("Success");
	}
	
	//取得人員之ScreenName
	public static String getUserScreenName(String userFullName) throws Exception{
		String ret = "";
		List<User> listUser = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
		for(User u: listUser){
			String _fullName = u.getLastName().trim() + u.getFirstName().trim();
			if(userFullName.trim().equals(_fullName)){
				ret = u.getScreenName();
				break;
			}
		}
		return ret;
	}
	
	//取得 Authentica SQL Server 連線
	//private Connection getAuthenticaSQLServerConnection() throws Exception{
	//	String msDriver = PropsUtil.get("sqlserver.driver"); 		//"net.sourceforge.jtds.jdbc.Driver";
	//	String msURL = PropsUtil.get("sqlserver.url");				//"jdbc:jtds:sqlserver://localhost:1433/traffic";
	//	String msUser = PropsUtil.get("sqlserver.user");			//"sa";
    //  String msPasswd= PropsUtil.get("sqlserver.password");
    //  //
	//	Class.forName(msDriver);
    //  return DriverManager.getConnection(msURL, msUser, msPasswd);
	//}
	
	//取得檢查行政系統 SQL Server 連線
	private Connection getExamAdminSQLServerConnection() throws Exception{
		
		Class.forName(PropsUtil.get("db2.driver"));
		// establish a connection to DB2
		String _url = PropsUtil.get("db2.url");
		String _user = PropsUtil.get("db2.user");
		String _pwd = PropsUtil.get("db2.password");
		_log.warn("Connection to SQL Server with url: " + _url + " and User: " + _user + " and Password: " + _pwd + " ....");
		Connection _conn = null;
		try{
			_conn = DriverManager.getConnection(_url,
												_user,
												_pwd);
			System.out.println("Success");
			_log.warn("Success");
		}catch(Exception _ex){
			System.out.println("Fail");
			_log.error("Fail");
		}
		return _conn;
	}
	
	private String getCurrDateTime() throws Exception{
		DateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
		return formatter.format(new Date());
	}
	
	//取得時間戳記
	private String getTimeStamp() throws Exception{
		String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS" ;
		final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(new java.util.Date());
	}
	
	//以 Admin 取得 CMIS Session
	/*
	private Session getAlfrescoSessionAdmin() throws Exception{
		acceptSelfSignedCertificates();
		//
		Map<String, String> parameter = new HashMap<String, String>();

		// Set the user credentials
		parameter.put(SessionParameter.USER, PropsUtil.get("alfresco.admin.user"));
		parameter.put(SessionParameter.PASSWORD, PropsUtil.get("alfresco.admin.password"));

		// Specify the connection settings
		//parameter.put(SessionParameter.ATOMPUB_URL, PropsUtil.get("alfresco.host") + "/alfresco/service/cmis");
		parameter.put(SessionParameter.ATOMPUB_URL, PropsUtil.get("alfresco.host") + "/alfresco/cmisatom");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

		// Set the alfresco object factory
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

		// Create a session
		SessionFactory factory = SessionFactoryImpl.newInstance();
		return factory.getRepositories(parameter).get(0).createSession();
	}
	
	private void acceptSelfSignedCertificates() {
		TrustManager[] trustAllCerts = new TrustManager[] { 
			new	X509TrustManager() {
				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
				}
				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
						throws CertificateException {
				}
				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
		    }
		};
		try{
			SSLContext sc = SSLContext.getInstance("SSL");
		    sc.init(null, trustAllCerts, new java.security.SecureRandom());
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		}catch (Exception ex) {
		}
	}
	*/
	
}

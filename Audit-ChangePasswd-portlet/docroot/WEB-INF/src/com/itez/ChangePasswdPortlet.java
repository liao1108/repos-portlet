package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
/**
 * Portlet implementation class ChangePasswdPortlet
 */
public class ChangePasswdPortlet extends MVCPortlet {
	DataSource ds = null;
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	//
	public void doChangePasswd(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			String oldPasswd = "", newPasswd = "", newPasswd2 = "";
			if(actionRequest.getParameter("oldPasswd") != null){
				oldPasswd = actionRequest.getParameter("oldPasswd").trim();
			}
			if(actionRequest.getParameter("newPasswd") != null){
				newPasswd = actionRequest.getParameter("newPasswd").trim();
			}
			if(actionRequest.getParameter("newPasswd2") != null){
				newPasswd2 = actionRequest.getParameter("newPasswd2").trim();
			}
			if(oldPasswd.equals("") || newPasswd.equals("") || newPasswd2.equals("")){
				throw new Exception("請完整輸入新舊密碼。");
			}
			if(!newPasswd.equals(newPasswd2)){
				throw new Exception("新密碼輸入不一致。");
			}
			//
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			User user = UserLocalServiceUtil.getUser(themeDisplay.getUserId());
			//
			Connection conn = ds.getConnection();
			this.doPoke(conn, "auditors");
			
			String sql = "SELECT * FROM auditors WHERE user_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getScreenName());
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				if(!rs.getString("passwd").equals(oldPasswd)){
					throw new Exception("您輸入的舊密碼有誤。");
				}
				UserLocalServiceUtil.updatePassword(user.getUserId(), newPasswd, newPasswd2, false, true);
				//
				//sql = "UPDATE auditors SET passwd = ? WHERE user_no=?";
				//ps = conn.prepareStatement(sql);
				//ps.setString(1, newPasswd);
				//ps.setString(2, user.getScreenName());
				//ps.execute();
			}else{
				throw new Exception("使用者代名 " + user.getScreenName() + " 不存於稽核人員名單內。");
			}
			//
			conn.close();
			//
			SessionMessages.add(actionRequest, "success");
			actionRequest.setAttribute("successMsg", "您的密碼已更新完成。");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	 
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}

	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}

}

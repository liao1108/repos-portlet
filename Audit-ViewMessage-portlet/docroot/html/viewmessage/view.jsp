<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>


<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:16pt;
			color:darkred;
    		}
    	tr  {
    			height: 30x
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}
	
.button{
  	display: block ;
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}
		
	.portlet-section-header results-header th {	
		background-color: #FF6600;
	}
		
	.portlet-section-alternate.results-row.alt.cwfi-row-index-1  td{
     		background-color: #FFFFCC;
 	}
</style>
  
<%
  	String msgNo = "";
  	if(renderRequest.getPortletSession(true).getAttribute("msgNo") != null){
  		msgNo = renderRequest.getPortletSession(true).getAttribute("msgNo").toString();
  	}
  	
  	String userNo = "";
  	if(renderRequest.getPortletSession(true).getAttribute("userNo") != null){
  		userNo = renderRequest.getPortletSession(true).getAttribute("userNo").toString();
  	}

  	Vector<Message> vecMessage = new Vector<Message>();
  	if(renderRequest.getPortletSession(true).getAttribute("vecMessage") != null){
  		vecMessage = (Vector<Message>)renderRequest.getPortletSession(true).getAttribute("vecMessage");
  	}
  	//
  	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
  	User user = themeDisplay.getUser();
  	
  	//
  	String errMsg = "";	
  	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
  	
  	String successMsg = "";	
  	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  	
  	int rowIndex = 0;
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doViewByMsgTypeURL" name="doViewByMsgType">
</portlet:actionURL>

<portlet:actionURL var="doViewByLogTimeURL" name="doViewByLogTime">
</portlet:actionURL>

<portlet:actionURL var="doViewByCountViewURL" name="doViewByCountView">
</portlet:actionURL>

<portlet:actionURL var="doQueryByMsgNoURL" name="doQueryByMsgNo">
</portlet:actionURL>

<portlet:actionURL var="doQueryByMyMsgURL" name="doQueryByMyMsg">
</portlet:actionURL>

<portlet:actionURL var="doQueryByAllMsgURL" name="doQueryByAllMsg">
</portlet:actionURL>

<portlet:actionURL var="doQueryByUserNoURL" name="doQueryByUserNo">
</portlet:actionURL>

<table width="100%">
<tr><td>

<aui:layout>
		<aui:column>
			<img src="<%=renderRequest.getContextPath()%>/images/auditor.png"/>
		</aui:column>	
		<aui:column>
			使用者名稱：<%=user.getScreenName()%>
		</aui:column>
</aui:layout>		

<aui:layout>
			<aui:column>
				<img src="<%=renderRequest.getContextPath()%>/images/search.png"/>
			</aui:column>
			
			<aui:column>
				<aui:form action="<%=doQueryByMyMsgURL%>" method="post" name="<portlet:namespace />fm" >
					<aui:input type="hidden" name="userNo" value="<%=user.getScreenName()%>"/>
					<%
						if(!user.getScreenName().equalsIgnoreCase("feb")){
					%>
						<aui:button type="submit" name="submit" value="查詢我的留言" cssClass="button"/>
					<%
						}
					%>
				</aui:form>
			</aui:column>
			
			<aui:column>
				<img src="<%=renderRequest.getContextPath()%>/images/dot.png"/>
			</aui:column>
			
			<aui:column>
				<aui:form action="<%=doQueryByAllMsgURL%>" method="post" name="<portlet:namespace />fm">
					<aui:button type="submit" name="submit" value="查詢全部留言"  cssClass="button"/>
				</aui:form>
			</aui:column>

			<aui:column>		
				<img src="<%=renderRequest.getContextPath()%>/images/dot.png"/>
			</aui:column>
			
			
			<aui:column>	
				[
			</aui:column>
			
			<aui:form action="<%=doQueryByMsgNoURL%>" method="post" name="<portlet:namespace />fm">
				<aui:column>
					<aui:button type="submit" name="submit" value="依編號查詢" cssClass="button"/>
				</aui:column>
				<aui:column>
					<aui:input type="text" name="msgNo"  label="請輸入編號："  inlineLabel="left"  value="<%=msgNo%>" />
				</aui:column>
			</aui:form>
			
			<aui:column>	
				]
			</aui:column>

			<%
				if(user.getScreenName().equalsIgnoreCase("feb")){
			%>
			<aui:column>
				<img src="<%=renderRequest.getContextPath()%>/images/dot.png"/>
			</aui:column>
			
			<aui:column>	
				[
			</aui:column>
			
			<aui:form action="<%=doQueryByUserNoURL%>" method="post" name="<portlet:namespace />fm">
				<aui:column>
					<aui:button type="submit" name="submit" value="依使用者名稱查詢" cssClass="button"/>
				</aui:column>

				<aui:column>
					<aui:input type="text" name="userNo"  label="請輸入使用者名稱："  inlineLabel="left"  value="<%=userNo%>" />
				</aui:column>
			</aui:form>
		
			<aui:column>	
				]
			</aui:column>
		<%
			}
		%>
</aui:layout>

<aui:layout>
	<aui:column>
		<img src="<%=renderRequest.getContextPath()%>/images/sort.png"/>
	</aui:column>
	<aui:column>
		查詢結果之排序方式：
	</aui:column>

	<aui:column>
		<a href="<%=doViewByMsgTypeURL%>">分類</a>
	</aui:column>
	<aui:column>
		<a href="<%=doViewByLogTimeURL%>">日期</a> 
	</aui:column>
	<aui:column>
		<a href="<%=doViewByCountViewURL%>">瀏覽人數</a>
	</aui:column>
</aui:layout>

<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMessage, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMessage.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row  className="com.itez.Message" keyProperty="msgId" modelVar="msg">
		<%
			if(rowIndex % 2 == 1){
				row.setClassName("cwfi-row-index-1");
			}
			//
			rowIndex++;
		%>
		
		<liferay-ui:search-container-column-text name="編號" property="msgNo" />
		<liferay-ui:search-container-column-text name="分類" property="msgTypeShort" />
		<liferay-ui:search-container-column-text name="留言摘要" property="logTitle" />
		<%if(user.getScreenName().equalsIgnoreCase("feb")){ %>
			<liferay-ui:search-container-column-text name="留言時間" property="logTime" align="center" />
			<liferay-ui:search-container-column-text name="回覆時間" property="responseTime" align="center" />
		<%}else{ %>
			<liferay-ui:search-container-column-text name="日期" property="responseTime" align="center" />
		<%} %>
		<liferay-ui:search-container-column-text name="瀏覽人數" property="countView" align="center" />
		<liferay-ui:search-container-column-jsp path="/html/admin/admin_actions.jsp" align="center" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

</td></tr>
</table>
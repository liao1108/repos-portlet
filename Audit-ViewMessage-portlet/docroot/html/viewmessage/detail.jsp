<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>


<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    			height: 40x
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}	
  </style>
  
<%
	Message msg = (Message)renderRequest.getPortletSession(true).getAttribute("currMessage");
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
	
	boolean isFeb = false;
	if(renderRequest.getPortletSession(true).getAttribute("isFeb") != null) isFeb = true;
%>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/viewmessage/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="doViewRelatedMsgURL"  name="doViewRelatedMsg">
	<portlet:param name="msgId"  value="<%=String.valueOf(msg.msgId) %>"/>
	<portlet:param name="msgNo"  value="<%=msg.msgNo %>"/>
</portlet:actionURL>

<portlet:actionURL var="doViewOneMsgURL"  name="doViewOneMsg">
</portlet:actionURL>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:resourceURL var="exportToWordURL" >
	<portlet:param name="msgNo"  value="<%=msg.msgNo %>"/>
</portlet:resourceURL>

<table width="100%">
<tr>
<td>
<aui:layout>
	<aui:column columnWidth="2">
		<img src="<%=renderRequest.getContextPath()%>/images/auditor.png"/>
	</aui:column>
	<aui:column>
		<aui:input type="text" name="userNo" label="留言者："  value="<%=msg.userNo %>" readonly="readonly"/>
	</aui:column>
	<aui:column>
		<aui:input type="text" name="msgNo" label="編號："  value="<%=msg.msgNo %>" readonly="readonly"/>
	</aui:column>
	<aui:column>
		<aui:input type="text" name="msgType" label="分類："  value="<%=msg.msgType %>" readonly="readonly"/>
	</aui:column>
	<aui:column>
		<%if(msg.withinMail == 1){ %>
			<br/>(特殊案件)
		<%} %>
	</aui:column>

</aui:layout>
<aui:layout>
	<aui:column columnWidth="2">
		<img src="<%=renderRequest.getContextPath()%>/images/message.png"/>
	</aui:column>
	<aui:column columnWidth="90">
		<aui:input type="textarea" name="logMsg" label="留言內容："  value="<%=msg.logMsg %>"  style="width: 100%;"  rows="6"  readonly="readonly"/>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="2">
		<img src="<%=renderRequest.getContextPath()%>/images/related.png"/>
	</aui:column>	
	<aui:column columnWidth="10">
		相關留言：
	</aui:column>	
	<%
		if(!msg.relatedMsgNo.equals("")){
			String[] rr =  msg.relatedMsgNo.split(",");
			for(String _msgNo: rr){
					String btnTitle = "檢視" + _msgNo;
	%>
		<aui:column>
			<aui:form action="<%=doViewOneMsgURL%>" method="post" name="<portlet:namespace />fm">
				<aui:input type="hidden" name="msgNo" value="<%=_msgNo %>"/>
				<aui:button type="submit" name="submit"  value="<%=btnTitle %>" cssClass="button"/>
			</aui:form>
		</aui:column>	
	<%
			}
		}
	%>	
</aui:layout>	
<aui:layout>
	<aui:column columnWidth="2">
		<img src="<%=renderRequest.getContextPath()%>/images/reply.png"/>
	</aui:column>	
	<aui:column columnWidth="90">
		<aui:input type="textarea" name="ourResponse" label="本局答覆：" value="<%=msg.ourResponse %>"  style="width: 100%;"  rows="6"  readonly="readonly"/>
	</aui:column>
</aui:layout>
<aui:layout>
<aui:column columnWidth="2">
		&nbsp;
</aui:column>	
<aui:column>	
	<a href="<%=backToViewURL%>">返回上頁</a>
	<%if(isFeb){ %>
	<a href="<%=exportToWordURL%>">下載留言單</a>
	<%} %>
</aui:column>
</aui:layout>		  
</td></tr>
</table>

package com.itez;

public class Message {
	public int msgId = 0;
	public String msgNo = "";
	public String userNo = "";
	public String userEmail = "";
	public String logTime = "";
	public String logTitle = "";
	public String logMsg = "";
	public String logMsgShort = "";
	//public String logMsgDisplay = "";		//顯示在網頁的樣式
	public String msgType="";
	public String msgTypeShort="";
	public String ourResponse = "";
	public String responseTime = "";
	
	public int countView = 0;
	public String relatedMsgNo = "";
	
	public int withinMail = 0;		//特殊案件
	
	public int rowIndex = 0;
}

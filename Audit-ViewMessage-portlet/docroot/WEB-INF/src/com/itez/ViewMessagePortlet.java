package com.itez;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import com.aspose.words.Bookmark;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ViewMessagePortlet
 */
public class ViewMessagePortlet extends MVCPortlet {
	DataSource ds = null;
	//Utils utils = new Utils();
	License license = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		license = new License();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//this.createTables();
			if(renderRequest.getPortletSession(true).getAttribute("vecMessage") == null){
				Vector<Message> vecMessage = this.getMessages("", "", themeDisplay);
				renderRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	
	private Vector<Message> getMessages(String queryColName, String param, ThemeDisplay themeDisplay) throws Exception{
		boolean isFeb = false;
		List<Role> listRole = RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
		for(Role r: listRole){
			if(r.getName().startsWith("FEB")){
				isFeb = true;
				break;
			}
		}
		//
		Vector<Message> vecMessage = new Vector<Message>();
		Connection conn = ds.getConnection();
		//
		int rowIndex = 0;
		//
		String sql = "SELECT B.* FROM auditor_message A, message_with_type B " +
					" WHERE A.msg_id = B.msg_id AND B.responsed = 1 AND A.published_status >= 1 " +
						" AND B.msg_no IS NOT NULL AND B.msg_no <> '' ";
		if(!isFeb){
			sql += " AND (B.within_mail IS NULL OR B.within_mail = 0 OR B.user_no='" + themeDisplay.getUser().getScreenName() + "') ";
		}
		if(queryColName.equalsIgnoreCase("myMsg") || queryColName.equalsIgnoreCase("userNo")){
			sql += " AND B.user_no='" + param + "'";
		}else if(queryColName.equalsIgnoreCase("msgNo")){
			sql += " AND B.msg_no LIKE '" + param + "%'";
		}
		sql += " ORDER BY B.log_time DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Message msg = new Message();
			msg.msgId = rs.getInt("msg_id");
			msg.msgNo = rs.getString("msg_no");
			msg.userNo = rs.getString("user_no");
			msg.msgType = rs.getString("msg_type");
			msg.msgTypeShort = msg.msgType.substring(2);
			msg.logTime = rs.getString("log_time");
			if(rs.getString("log_title") != null){
				msg.logTitle = rs.getString("log_title");
			}
			msg.logMsg = rs.getString("log_msg");
			//103.03.06修改
			if(!msg.logTitle.equals("")){
				msg.logMsg = msg.logTitle + "<\n>" + msg.logMsg;
			}
			//msg.logMsgShort = msg.logMsg;
			//if(!msg.logTitle.equals("")){			//如果有留言標題者
			//	msg.logMsgShort = msg.logTitle;
			//}
			//if(msg.logMsgShort.length() > 30){
			//	msg.logMsgShort = msg.logMsgShort.substring(0, 30) + " ...";
			//}
			//msg.logMsgDisplay = msg.logMsgShort + "<br>" + msg.logMsg;		//顯示在網頁的樣式
			
			msg.ourResponse = rs.getString("our_response");
			msg.responseTime = rs.getString("response_time");
			if(isFeb){
				if(!msg.responseTime.equals("") && msg.responseTime.contains(" ")){
					msg.responseTime = msg.responseTime.substring(0, 4) + "-" + msg.responseTime.substring(4, 6) + "-" + msg.responseTime.substring(6, 8) + " " +
							msg.responseTime.substring(9, 11) + ":" + msg.responseTime.substring(11, 13) + ":" + msg.responseTime.substring(13, 15); 
				}
			}else{
				//前台用戶的留言時間其實是回覆時間
				if(!msg.responseTime.equals("") && msg.responseTime.contains(" ")){
					msg.responseTime = msg.responseTime.split(" ")[0];
				}
				if(msg.responseTime.length() == 8){
					msg.responseTime = msg.responseTime.substring(0, 4) + "-" +
										msg.responseTime.substring(4, 6) + "-" +
										msg.responseTime.substring(6);
				}
			}			
			msg.countView = rs.getInt("count_view");
			msg.withinMail = rs.getInt("within_mail");
			//
			String relatedNo = "";
			sql = "SELECT * FROM message_with_type WHERE msg_id=? AND msg_no <> ? " ;		//AND (within_mail IS NULL OR within_mail=0)";
			if(!isFeb){
				sql += " AND (within_mail IS NULL OR within_mail = 0) ";
			}
			ps = conn.prepareStatement(sql);
			ps.setInt(1, msg.msgId);
			ps.setString(2, msg.msgNo);
			ResultSet rs2 = ps.executeQuery();
			while(rs2.next()){
				if(!relatedNo.equals("")) relatedNo += ",";
				relatedNo += rs2.getString("msg_no");
			}
			rs2.close();
			msg.relatedMsgNo = relatedNo;
			//
			msg.rowIndex = rowIndex;
			rowIndex++;
			//
			vecMessage.add(msg);
		}
		conn.close();
		//
		return vecMessage;
	}
	
	public void doQueryByMsgNo(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//
			String msgNo = actionRequest.getParameter("msgNo");
			Vector<Message> vecMessage = this.getMessages("msgNo", msgNo, themeDisplay);
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			//
			actionRequest.getPortletSession(true).setAttribute("msgNo", msgNo);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doQueryByUserNo(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String userNo = actionRequest.getParameter("userNo");
			Vector<Message> vecMessage = this.getMessages("userNo", userNo, themeDisplay);
			
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			//
			actionRequest.getPortletSession(true).setAttribute("userNo", userNo);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}

	public void doQueryByMyMsg(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String userNo = actionRequest.getParameter("userNo");
			Vector<Message> vecMessage = this.getMessages("myMsg", userNo, themeDisplay);
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			//
			actionRequest.getPortletSession(true).removeAttribute("msgNo");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doQueryByAllMsg(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			Vector<Message> vecMessage = this.getMessages("", "", themeDisplay);
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			//
			actionRequest.getPortletSession(true).removeAttribute("msgNo");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doViewByMsgType(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			Hashtable<String, Message> ht = new Hashtable<String, Message>();
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			if(vecMessage != null){
				for(Message m: vecMessage){
					ht.put(m.msgNo, m);
				}
				Vector<String> vec = new Vector<String>();
				Enumeration<String> en = ht.keys();
				while(en.hasMoreElements()){
					vec.add(en.nextElement());
				}
				Collections.sort(vec);
				//
				Vector<Message> _vecMessage = new Vector<Message>();
				for(String s: vec){
					_vecMessage.add(ht.get(s));
				}
				actionRequest.getPortletSession(true).setAttribute("vecMessage", _vecMessage);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doViewByLogTime(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			Hashtable<String, Message> ht = new Hashtable<String, Message>();
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			if(vecMessage != null){
				int z = 0;
				for(Message m: vecMessage){
					ht.put(m.responseTime + "#" + m.msgId + String.format("%06d", z), m);
					z++;
				}
				Vector<String> vec = new Vector<String>();
				Enumeration<String> en = ht.keys();
				while(en.hasMoreElements()){
					String key = en.nextElement();
					vec.add(key);
				}
				Collections.sort(vec);
				Vector<String> _vec = new Vector<String>();
				for(int i=vec.size()-1; i >=0; i-- ){
					_vec.add(vec.get(i));
				}
				//
				Vector<Message> _vecMessage = new Vector<Message>();
				for(String s: _vec){
					_vecMessage.add(ht.get(s));
				}
				actionRequest.getPortletSession(true).setAttribute("vecMessage", _vecMessage);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void doViewByCountView(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			Hashtable<String, Message> ht = new Hashtable<String, Message>();
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			if(vecMessage != null){
				for(Message m: vecMessage){
					ht.put(String.format("%06d", m.countView) + "-" + m.msgNo, m);
				}
				Vector<String> vec = new Vector<String>();
				Enumeration<String> en = ht.keys();
				while(en.hasMoreElements()){
					vec.add(en.nextElement());
				}
				Collections.sort(vec);
				Vector<String> _vec = new Vector<String>();
				for(int i=vec.size()-1; i >=0; i-- ){
					_vec.add(vec.get(i));
				}
				//
				Vector<Message> _vecMessage = new Vector<Message>();
				for(String s: _vec){
					_vecMessage.add(ht.get(s));
				}
				actionRequest.getPortletSession(true).setAttribute("vecMessage", _vecMessage);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	/*
	public void doViewRelatedMsg(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			String msgNo = actionRequest.getParameter("msgNo");
			//
			Vector<Message> vecRelated = new Vector<Message>();
			Connection conn = ds.getConnection();
			String sql = "SELECT * FROM message_with_type WHERE msg_id=? AND msg_no <> ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.setString(2, msgNo);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Message msg = new Message();
				msg.msgId = rs.getInt("msg_id");
				msg.msgNo = rs.getString("msg_no");
				msg.userNo = rs.getString("user_no");
				msg.msgType = rs.getString("msg_type");
				msg.logTime = rs.getString("log_time");
				msg.logMsg = rs.getString("log_msg");
				msg.ourResponse = rs.getString("our_response");
				msg.countView = rs.getInt("count_view");
				//
				vecRelated.add(msg);
			}
			actionRequest.getPortletSession(true).setAttribute("vecRelated", vecRelated);
			//Forward to 
			
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = utils.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	*/
	
	public void doViewOneMsg(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			boolean isFeb = false;
			List<Role> listRole = RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
			for(Role r: listRole){
				if(r.getName().startsWith("FEB")){
					isFeb = true;
					break;
				}
			}
			//
			String msgNo = actionRequest.getParameter("msgNo");
			//
			 conn = ds.getConnection();
			//
			Message msg = new Message();
			if(actionRequest.getPortletSession(true).getAttribute("currMessage") != null){
				msg = (Message)actionRequest.getPortletSession(true).getAttribute("currMessage");
			}
			//
			String sql = "SELECT B.* FROM auditor_message A, message_with_type B " +
						" WHERE A.msg_id = B.msg_id AND B.msg_no =?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, msgNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				msg = new Message();
				msg.msgId = rs.getInt("msg_id");
				msg.msgNo = rs.getString("msg_no");
				msg.userNo = rs.getString("user_no");
				msg.msgType = rs.getString("msg_type");
				msg.msgTypeShort = msg.msgType.substring(2);
				msg.logTime = rs.getString("log_time");
				if(rs.getString("log_title") != null){
					msg.logTitle = rs.getString("log_title");
				}
				msg.logMsg = rs.getString("log_msg");
				//103.03.06修改
				if(!msg.logTitle.equals("")){
					msg.logMsg = msg.logTitle + "\n" + msg.logMsg;
				}
				
				//msg.logMsgShort = msg.logMsg;
				//if(!msg.logTitle.equals("")){
				//	msg.logMsgShort = msg.logTitle;
				//}
				//if(msg.logMsgShort.length() > 30){
				//	msg.logMsgShort = msg.logMsgShort.substring(0, 30) + " ...";
				//}
				//msg.logMsgDisplay = msg.logMsgShort + "<br>" + msg.logMsg;		//顯示再網頁的樣式
				
				msg.ourResponse = rs.getString("our_response");
				msg.responseTime = rs.getString("response_time");
				if(isFeb){
					if(msg.responseTime.contains(" ") && msg.responseTime.length() >= 15){
						msg.responseTime = msg.responseTime.substring(0, 4) + "-" + msg.responseTime.substring(4, 6) + "-" + msg.responseTime.substring(6, 8) + " " +
								msg.responseTime.substring(9, 11) + ":" + msg.responseTime.substring(11, 13) + ":" + msg.responseTime.substring(13, 15); 
					}
				}else{
					//前台用戶的留言時間其實是回覆時間
					if(!msg.responseTime.equals("") && msg.responseTime.contains(" ")){
						msg.responseTime = msg.responseTime.split(" ")[0];
					}
					if(msg.responseTime.length() == 8){
						msg.responseTime = msg.responseTime.substring(0, 4) + "-" +
											msg.responseTime.substring(4, 6) + "-" +
											msg.responseTime.substring(6) ;
					}
				}	
				msg.countView = rs.getInt("count_view");
				msg.withinMail = rs.getInt("within_mail");
				//
				String relatedNo = "";
				sql = "SELECT * FROM message_with_type WHERE msg_id=? AND msg_no <> ? "; 	//AND (within_mail IS NULL OR within_mail=0) ";
				if(!isFeb){
					sql += " AND (within_mail IS NULL OR within_mail = 0 OR user_no='" + themeDisplay.getUser().getScreenName() + "') ";
					//sql += " AND (within_mail IS NULL OR within_mail = 0) ";
				}
				ps = conn.prepareStatement(sql);
				ps.setInt(1, msg.msgId);
				ps.setString(2, msg.msgNo);
				ResultSet rs2 = ps.executeQuery();
				while(rs2.next()){
					if(!relatedNo.equals("")) relatedNo += ",";
					relatedNo += rs2.getString("msg_no");
				}
				rs2.close();
				msg.relatedMsgNo = relatedNo;
			}
			
			//if(!msg.msgNo.equals(msgNo)){
				int idx = -1;
				Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
				for(Message m: vecMessage){
					idx++;
					if(m.msgNo.equals(msgNo)){
						vecMessage.remove(idx);
						vecMessage.add(idx, msg);
						//msg = m;
						break;
					}
				}
				/*
				if(idx >= 0 && idx < vecMessage.size()){
					vecMessage.remove(idx);
					vecMessage.add(idx, msg);
				}else{
					String sql = "SELECT * FROM message_with_type WHERE msg_no =?";
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.setString(1, msgNo);
					ResultSet rs = ps.executeQuery();
					if(rs.next()){
						msg = new Message();
						msg.msgId = rs.getInt("msg_id");
						msg.msgNo = rs.getString("msg_no");
						msg.userNo = rs.getString("user_no");
						msg.msgType = rs.getString("msg_type");
						msg.logTime = rs.getString("log_time");
						msg.logMsg = rs.getString("log_msg");
						msg.ourResponse = rs.getString("our_response");
						msg.countView = rs.getInt("count_view");
						//
						String relatedNo = "";
						sql = "SELECT * FROM message_with_type WHERE msg_id=? AND msg_no <> ?";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, msg.msgId);
						ps.setString(2, msg.msgNo);
						ResultSet rs2 = ps.executeQuery();
						while(rs2.next()){
							if(!relatedNo.equals("")) relatedNo += ",";
							relatedNo += rs2.getString("msg_no");
						}
						rs2.close();
						msg.relatedMsgNo = relatedNo;
					}
				}
				 */
				msg.countView += 1;
				//
				sql = "UPDATE message_with_type SET count_view = ? WHERE msg_no=? ";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, msg.countView);
				ps.setString(2, msg.msgNo);
				ps.execute();
				//
				actionRequest.getPortletSession(true).setAttribute("currMessage", msg);
				actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
				
				if(isFeb){
					actionRequest.getPortletSession(true).setAttribute("isFeb", "是");
				}
			//}
			//Forward to 
			actionResponse.setRenderParameter("jspPage", "/html/viewmessage/detail.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private void createTables() throws Exception{
		Connection conn = ds.getConnection();
    		//建立簿冊分類跳號
		String sql = "CREATE TABLE IF NOT EXISTS message_no_gen (" +
								    		"year INT NOT NULL, " +
								    		"msg_type VARCHAR(30) NOT NULL," +
								    		"next_no INT NOT NULL DEFAULT 1, " +
					"PRIMARY KEY (year, msg_type))";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.execute();
		//建立分類後留言列表表格
		//sql = "DROP table message_with_type";
		//ps = conn.prepareStatement(sql);
		//ps.execute();
		
		sql = "CREATE TABLE IF NOT EXISTS message_with_type (" +
									    "msg_no VARCHAR(10) NOT NULL , " +
									    "msg_id INT(10) NOT NULL , " +
									    "user_no VARCHAR(30) NOT NULL," +
									    "email VARCHAR(30) NOT NULL," +
									    "log_time  VARCHAR(20) NOT NULL," +
									    "log_msg VARCHAR(1000) NOT NULL, " +
									    "msg_type VARCHAR(20)," +
									    "our_response VARCHAR(1000)," +
									    "response_time VARCHAR(20)," +
									    "count_view INT, " +
									 "PRIMARY KEY (msg_no))";   
		ps = conn.prepareStatement(sql);
		ps.execute();
    		//
    		conn.close();
	}

	
	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		try{
			Message msg = (Message)resRequest.getPortletSession(true).getAttribute("currMessage");
			//
			resResponse.setContentType("application/msword");
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			String fileName = URLEncoder.encode("留言單_" + msg.msgNo + ".docx", "UTF-8");
			resResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"");
			
			OutputStream out = resResponse.getPortletOutputStream();
			//
			license.setLicense(resRequest.getPortletSession().getPortletContext().getResourceAsStream("Aspose.Words.lic"));
			//
			com.aspose.words.Document docWord = new com.aspose.words.Document(resRequest.getPortletSession().getPortletContext().getResourceAsStream("稽核人員留言板.docx"));
			for(Bookmark bk: docWord.getRange().getBookmarks()){
				if(bk.getName().equalsIgnoreCase("留言者")){
					bk.setText(msg.userNo);
				}else if(bk.getName().equalsIgnoreCase("編號")){
					bk.setText(msg.msgNo);
				}else if(bk.getName().equalsIgnoreCase("分類")){
					bk.setText(msg.msgType);
				}else if(bk.getName().equalsIgnoreCase("案別")){
					bk.setText("一般案件");
					if(msg.withinMail == 1){
						bk.setText("特殊案件");
					}
				}else if(bk.getName().equalsIgnoreCase("留言內容")){
					bk.setText(msg.logMsg);
				}else if(bk.getName().equalsIgnoreCase("相關留言")){
					bk.setText(msg.relatedMsgNo);
				}else if(bk.getName().equalsIgnoreCase("本局答覆")){
					bk.setText(msg.ourResponse);
				}
			}
			docWord.getRange().getBookmarks().clear();
			//
			docWord.save(out, SaveFormat.DOCX);
			//
			out.flush();
			out.close();
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}
	}
	
	private String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}	
}

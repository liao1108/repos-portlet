<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}

	//String stageName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("stageName") != null){
	//	stageName = renderRequest.getPortletSession(true).getAttribute("stageName").toString();
	//}
	//
	//Vector<StageFile> vecFiles = new Vector<StageFile>();
	//if(renderRequest.getPortletSession(true).getAttribute("vecFiles") != null){
	//	vecFiles = (Vector<StageFile>)renderRequest.getPortletSession(true).getAttribute("vecFiles");
	//}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="uploadFileURL"  name="uploadFile" >
</portlet:actionURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	


<script  type="text/javascript">	
	function sendingIndustry() {
		Liferay.fire('fileUploadOk', "true");
		//
		AUI().DialogManager.hideAll();
	}
</script>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;檔案列表：<%=collec.stageName %>
	</aui:column>
</aui:layout>

<hr/>

<aui:form action="<%=uploadFileURL%>"  enctype="multipart/form-data" method="post" >
	<aui:layout>
		<aui:column cssClass="td">請指定所要上傳的檔案：</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName0" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName1" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName2" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName3" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName4" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName5" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName6" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName7" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName8" label="" size="75"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName9" label="" size="75"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:column cssClass="td">	
	  			<aui:input name="dupOption" type="radio"  value="update"  label="檔名重複者直接更新"  checked="true"/>
	  		</aui:column>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<br/>
			<aui:button type="submit" value="開始上傳" cssClass="button"/>
		</aui:column>
		<aui:column>
			<br/>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>


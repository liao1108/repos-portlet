<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.alloc.ExamCommentProduceType" %>
<%@ page import= "javax.portlet.PortletURL" %>

<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	//String dateCurr = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
	//
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	int rowsPrefer = 20;
	if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
		rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<script type="text/javascript">
	
	//批次下載
	function doBatchDownloadSubmit() {
		document.getElementById("<portlet:namespace/>batchActionType").value = "download";
		document.<portlet:namespace />fmBatchDownload.submit();
	}
	
	//批次歸檔
	function doBatchArchiveSubmit() {
		document.getElementById("<portlet:namespace/>batchActionType").value = "archive";
		document.<portlet:namespace />fmBatchDownload.submit();
	}
	
	//傳至檢查行政
	function doBatchPostSubmit() {
		if (confirm("上傳至檢查行政系統前請檢查溝通會日期是否正確，確定上傳嗎?")) {
			document.getElementById("<portlet:namespace/>batchActionType").value = "post";
			document.<portlet:namespace />fmBatchDownload.submit();
		}
	}
	
	//全選
	function doCheck(){
		var withCkecked = false;
		var chk = document.getElementsByTagName('input');
	    	var len = chk.length;
	      for (var i = 0; i < len; i++) {
	      	if (chk[i].type == 'checkbox') {
	            	if(chk[i].checked){
	            		withCkecked = true;
	            		break;
	            	} 
	        	}
	    	}      
	      //
		if(withCkecked){
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = false;
			        	}
		    	}
		}else{
		      for (var i = 0; i < len; i++) {
			      	if (chk[i].type == 'checkbox') {
			            	chk[i].checked = true;
			        	}
		    	}
		}
	}
</script>

<portlet:renderURL var="uploadFileURL" >
  	<portlet:param name="jspPage"  value="/jsp/stagefiles/uploadFile.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="uploadCommentFileURL" >
  	<portlet:param name="jspPage"  value="/jsp/stagefiles/uploadCommentFile.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="prepareBatchActionURL"  name="prepareBatchAction">
</portlet:actionURL>

<portlet:actionURL var="genPointLeaderURL" name="genPointLeader" >
</portlet:actionURL>

<portlet:actionURL var="genEssenLeaderURL" name="genEssenLeader" >
</portlet:actionURL>

<portlet:actionURL var="genLeaderSummaryTrimColURL" name="genLeaderSummaryTrimCol" >
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInterURL" name="genSpotDefectReportInter" >
	<portlet:param name="infoExam"  value="false"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInter4LocalBankHQNormalURL" name="genSpotDefectReportInter" >
	<portlet:param name="localBankHQNormal"  value="true"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInter4InfoExamURL" name="genSpotDefectReportInter" >
	<portlet:param name="infoExam"  value="true"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportOuterURL" name="genSpotDefectReportOuter" >
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportOuter4LocalBankHQNormalURL" name="genSpotDefectReportOuter" >
	<portlet:param name="localBankHQNormal"  value="true"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportAfterURL" name="genSpotDefectReportAfter" >
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportAfterTrimColURL" name="genSpotDefectReportAfterTrimCol" >
</portlet:actionURL>

<portlet:actionURL var="copyDefectReport2EndMeetURL" name="copyDefectReport2EndMeet" >
</portlet:actionURL>

<portlet:actionURL var="genExamOpinion210NURL" name="genExamOpinion210N" >
</portlet:actionURL>

<portlet:actionURL var="genLocalBankExamOpinionURL" name="genLocalBankExamOpinion" >
</portlet:actionURL>

<portlet:actionURL var="genExamFace910NURL" name="genExamFace910N" >
</portlet:actionURL>

<portlet:actionURL var="moveOpinionFiles2PostURL" name="moveOpinionFiles2Post" >
</portlet:actionURL>

<portlet:actionURL var="moveDefectReport2EvalURL" name="moveDefectReport2Eval" >
</portlet:actionURL>

<portlet:actionURL var="genExamOpinionStatisticURL" name="genExamOpinionStatistic" >
</portlet:actionURL>

<portlet:actionURL var="genMyEmptyEvalFormStaffByItemURL" name="genMyEmptyEvalFormStaffByItem" >
</portlet:actionURL>

<portlet:actionURL var="checkPointFormCompletedURL" name="checkFormCompleted" >
	<portlet:param name="formType"  value="檢查重點"/>
</portlet:actionURL>

<portlet:actionURL var="checkEssenFormCompletedURL" name="checkFormCompleted" >
	<portlet:param name="formType"  value="必查項目"/>
</portlet:actionURL>

<portlet:actionURL var="genEvalMeasureFormStaffURL" name="genEvalMeasureFormStaff" >
</portlet:actionURL>

<portlet:actionURL var="genEvalMeasureFormLeaderURL" name="genEvalMeasureFormLeader" >
</portlet:actionURL>

<portlet:actionURL var="genExamReportIndexAndCoverURL" name="genExamReportIndexAndCover" >
</portlet:actionURL>

<portlet:actionURL var="genProcessFlowAndBisItemURL" name="genProcessFlowAndBisItem" >
</portlet:actionURL>

<portlet:actionURL var="doStageEvalActionURL" name="doStageEvalAction" >
</portlet:actionURL>

<portlet:actionURL var="doStageEndActionURL" name="doStageEndAction" >
</portlet:actionURL>

<portlet:actionURL var="mergeInterState2PostURL" name="mergeInterState2Post" >
</portlet:actionURL>

<portlet:actionURL var="genInfoExamStateLeaderURL" name="genInfoExamStateLeader" >
</portlet:actionURL>

<portlet:actionURL var="genInfoExamAssessFormURL" name="genInfoExamAssessForm" >
</portlet:actionURL>

<portlet:actionURL var="genInfoExamAssessScoreURL" name="genInfoExamAssessScore" >
</portlet:actionURL>

<portlet:resourceURL var="genEmptyExamCommentURL" >
	<portlet:param name="actionType" value="genEmptyExamComment"/>
</portlet:resourceURL>	


<portlet:renderURL var="goAuthenticaRunURL" >
  	<portlet:param name="jspPage"  value="/jsp/stagefiles/authentica.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="goDraftAreaURL"  name="goDraftArea" >
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;檔案列表：檢查證號(<%=collec.examNo %>) / 階段(<%=collec.stageName %>)
	</aui:column>
</aui:layout>

<hr/>

<aui:layout >
	<%if(!collec.reportClosed){ %>	
		<aui:column>
			<a href="javascript:void();"  onClick="javascript:doCheck();">
				<img src='<%=renderRequest.getContextPath() + "/images/checked.png" %>'/>
			</a>
		</aui:column>
		
		<%if(collec.stageName.contains("結束")){ %>
			<aui:column>
				<aui:button onClick="<%=uploadFileURL.toString()%>"  value="上傳重要查核項目報告表"  cssClass="button"/>
			</aui:column>
		<%}else{ %>
			<aui:column>
				<aui:button onClick="<%=uploadFileURL.toString()%>"  value="上傳檔案"  cssClass="button"/>
			</aui:column>
		<%} %>
		
		<aui:column>
			<aui:button onClick="doBatchDownloadSubmit();"  value="下載所選"  cssClass="button"/>
		</aui:column>
	
		<%if(collec.stageName.contains("結束")){ %>
			<aui:column>
				<%if(collec.examNature.contains("評等")){ %>
					<aui:button onClick="<%=genEmptyExamCommentURL.toString()%>"  value="產出空白檢查評述(評等用)"  cssClass="button"/>
				<%}else{ %>
					<aui:button onClick="<%=genEmptyExamCommentURL.toString()%>"  value="產出空白檢查評述"  cssClass="button"/>
				<%}%>
			</aui:column>
			<aui:column>
				<%if(collec.examNature.contains("評等")){ %>
					<aui:button onClick="<%=uploadCommentFileURL.toString()%>"  value="上傳檢查評述(評等用)"  cssClass="button"/>
				<%}else{ %>
					<aui:button onClick="<%=uploadCommentFileURL.toString()%>"  value="上傳檢查評述"  cssClass="button"/>
				<%} %>	
			</aui:column>
		<%} %>
		
		<%//if((collec.stageName.contains("結束")  || collec.stageName.contains("評等") || collec.stageName.contains("陳核")) && collec.isLeader){
			if((collec.stageName.contains("評等") || collec.stageName.contains("陳核")) && collec.isLeader){%>
			<aui:column>
				<aui:button onClick="doBatchArchiveSubmit();"  value="歸檔所選"  cssClass="button"/>
			</aui:column>
		<%} %>		
	<%} %>
	
	<%if(collec.stageName.contains("結束") && !collec.reportClosed && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
			<% if(collec.isRatingAssess && !collec.isComputerAudit){ %>
				<aui:form action="<%=doStageEndActionURL.toString()%>"  method="post" >
					<aui:column cssClass="td">產出：</aui:column>
				
					<aui:column cssClass="td">
						<aui:select name="actionType" label="">
							<aui:option value="我的檢查項目別查核內容摘要表">我的檢查項目別查核內容摘要表</aui:option>
							<%if(collec.isLeader){ %>
								<%for(String _staff: collec.vecStaff){ %>
									<%String _optName = "助檢之檢查項目別查核內容摘要表-" + _staff;%>
									<aui:option value="<%=_optName %>"><%=_optName %></aui:option>
								<%} %>
							<%} %>
						</aui:select>
					</aui:column>
					
					<aui:column>
						<aui:button type="submit" value="執行" cssClass="button"/>
					</aui:column>
					
				</aui:form>		
			<%} %>

			<% if(!collec.isComputerAudit){ %>
				<aui:column>
					<aui:button onClick="<%=checkPointFormCompletedURL.toString()%>"  value="驗證檢查重點報告表" cssClass="button"/>
				</aui:column>	
				<% if(!collec.isSepecialExam){ %>
					<aui:column>
						<aui:button onClick="<%=checkEssenFormCompletedURL.toString()%>"  value="驗證必查項目報告表" cssClass="button"/>
					</aui:column>
				<%} %>
			<%} %>
			<% if(collec.vecFiles.size() > 0 && collec.isLeader){ %>
				<aui:column>
					<aui:button onClick="<%=goDraftAreaURL.toString()%>"  value="草稿區" cssClass="button"/>
				</aui:column>				
			<%} %>
	<%}else if(collec.stageName.contains("會前") && collec.isLeader && !collec.reportClosed && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
			<aui:column>產出：</aui:column>
			<%if(collec.isComputerAudit){ %>
				<aui:column>
					<aui:button onClick="<%=genInfoExamStateLeaderURL.toString() %>"  value="重要查核項目彙整表" cssClass="button"/>
				</aui:column>
			<%}else{ %>
				<aui:column>
					<aui:button onClick="<%=genPointLeaderURL.toString() %>"  value="檢查重點彙整表" cssClass="button"/>
				</aui:column>
				<%if(collec.isEssenNeed){ %>
					<aui:column>
						<aui:button onClick="<%=genEssenLeaderURL.toString() %>"  value="必查項目彙整表" cssClass="button"/>
					</aui:column>
				<%} %>
				<%if(collec.isRatingAssess){ %>
					<aui:column>
						<aui:button onClick="<%=genLeaderSummaryTrimColURL.toString() %>"  value="彙整表(呈核用)" cssClass="button"/>
					</aui:column>
				<%} %>	
			<%} %>
			<aui:column>
				<%if(collec.isComputerAudit){ %>
					<aui:button onClick="<%=genSpotDefectReportInter4InfoExamURL.toString() %>"  value="內部版缺失情形表" cssClass="button"/>
				<%}else if(collec.isRatingAssess && !collec.isComputerAudit){ %>
					<aui:button onClick="<%=genSpotDefectReportInter4LocalBankHQNormalURL.toString() %>"  value="內部版缺失情形表" cssClass="button"/>
				<%}else{ %>
					<aui:button onClick="<%=genSpotDefectReportInterURL.toString() %>"  value="內部版缺失情形表" cssClass="button"/>
				<%} %>	
			</aui:column>
			
			<aui:column>
				<%if(collec.isRatingAssess && !collec.isComputerAudit){ %>
					<aui:button onClick="<%=genSpotDefectReportOuter4LocalBankHQNormalURL.toString() %>"  value="外部版缺失情形表" cssClass="button"/>
				<%}else{ %>
					<aui:button onClick="<%=genSpotDefectReportOuterURL.toString() %>"  value="外部版缺失情形表" cssClass="button"/>
				<%} %>	
			</aui:column>
	<%}else if(collec.stageName.contains("會後") && collec.isLeader && !collec.reportClosed && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
		<aui:column>
			<aui:button onClick="<%=genSpotDefectReportAfterURL.toString() %>"  value="產出會後版缺失情形表" cssClass="button"/>
			<aui:button onClick="<%=genSpotDefectReportAfterTrimColURL.toString() %>"  value="產出呈核用缺失情形表" cssClass="button"/>
		</aui:column>
	<%}else if(collec.stageName.contains("評等") && !collec.reportClosed && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>	
		<%if(collec.isRatingAssess && !collec.isComputerAudit){ %>
			<aui:form action="<%=doStageEvalActionURL.toString()%>"  method="post" >
				<aui:column cssClass="td">檔案產出選項：</aui:column>
				<aui:column cssClass="td">
					<aui:select name="actionType" label="">
						<aui:option>&nbsp;</aui:option>
						<%if(collec.isLeader){ %>
							<%for(String _ms: collec.vecMainAssesser){ %>
								<%//if(collec.isEvalSumNeededWoSpotDefect){ %>
								       <%String optName1 =  "評等項目別查核內容摘要表(無需實地查核缺失報告)-" + _ms;%>
									<aui:option value="<%=optName1 %>"><%=optName1 %></aui:option>
								<%//} %>
								<%//if(collec.isEvalSumNeededWSpotDefect){ %>
									<%String optName2 =  "評等項目別查核內容摘要表(需實地查核缺失報告)-" + _ms;%>
									<aui:option value="<%=optName2 %>"><%=optName2 %></aui:option>
								<%//} %>
							<%} %>
						<%}else if(collec.isMainAssesser){ %>
							<%//if(collec.isEvalSumNeededWoSpotDefect){ %>
								<aui:option value="評等項目別查核內容摘要表(無需實地查核缺失報告)">評等項目別查核內容摘要表(無需實地查核缺失報告)</aui:option>
							<%//} %>
							<%//if(collec.isEvalSumNeededWSpotDefect){ %>
								<aui:option value="評等項目別查核內容摘要表(需實地查核缺失報告)">評等項目別查核內容摘要表(需實地查核缺失報告)</aui:option>
							<%//} %>
						<%} %>	
						<%if(collec.isLeader){ %>
							<aui:option value="領隊版評鑑細項評量表">領隊版評鑑細項評量表</aui:option>
							<aui:option value="評鑑細項得分一覽表">評鑑細項得分一覽表</aui:option>
							<aui:option value="本會內部參考事項">本會內部參考事項</aui:option>
						<%} %>						
					</aui:select>
				</aui:column>
				<aui:column>
					<aui:button type="submit" value="執行" cssClass="button"/>
				</aui:column>
			</aui:form>
		<%}else if(collec.isComputerAudit && collec.isLeader){ %>
			<aui:column>
				<aui:button onClick="<%=genInfoExamAssessFormURL.toString() %>"  value="產出專案檢查評量表"  cssClass="button"/>
			</aui:column>
			<aui:column>
				<aui:button onClick="<%=genInfoExamAssessScoreURL.toString()%>"  value="產出專案檢查評級一覽表" cssClass="button"/>
			</aui:column>
		<%} %>
	<%}else if(collec.stageName.contains("陳核") && collec.isLeader && !collec.reportClosed && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
		<aui:column cssClass="td">產出：</aui:column>
		<%if(collec.examCommentProduceType != ExamCommentProduceType.EXAM_COMMENT_PRODUCE_NONE.ordinal()){ 			//保外組或證票 %>
			<aui:column>
				<aui:button onClick="<%=mergeInterState2PostURL.toString() %>"  value="內參彙整表" cssClass="button"/>
			</aui:column>
		<%} %>	
		<aui:column>
			<%if(collec.isRatingAssess && !collec.isComputerAudit){ %>
				<aui:button onClick="<%=genLocalBankExamOpinionURL.toString() %>"  value="檢查意見" cssClass="button"/>
			<%}else{ %>
				<aui:button onClick="<%=genExamOpinion210NURL.toString() %>"  value="檢查意見" cssClass="button"/>
			<%} %>
		</aui:column>
		<aui:column>	
			<aui:button onClick="<%=genExamFace910NURL.toString() %>"  value="面請意見" cssClass="button"/>
		</aui:column>	
	
		<%if(collec.vecFiles.size() > 0){ %>
			<aui:column>
				<aui:button onClick="<%=genExamReportIndexAndCoverURL.toString() %>"  value="報告目錄/封面" cssClass="button"/>
			</aui:column>
			<aui:column>
				<aui:button onClick="<%= genProcessFlowAndBisItemURL.toString() %>"  value="處理流程/業務項目表" cssClass="button"/>
			</aui:column>
		<%} %>
	<%}else if(collec.stageName.contains("歸檔") && collec.isLeader){ %>
		<%if(collec.vecFiles.size() > 0){ %>
			<aui:column>	
				<aui:button onClick="<%=goAuthenticaRunURL.toString() %>"  value="執行檔案加密" cssClass="button"/>
			</aui:column>
			
			<aui:column>	
				<aui:button onClick="doBatchPostSubmit();"  value="上傳至檢查行政系統"  cssClass="button"/>
			</aui:column>			
		<%} %>	
	<%} %>
</aui:layout>

<br/>

<aui:form action="<%=prepareBatchActionURL%>"  method="post" name="fmBatchDownload"  id="fmBatchDownload">

<aui:input type="hidden" name="batchActionType"/>	<%//下載或歸檔 %>

<aui:fieldset label="已上傳之重要查核項目報告表檔案">

<liferay-ui:search-container emptyResultsMessage=""  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecFiles, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecFiles.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.StageFile" keyProperty="fileId" modelVar="stageFile">
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
  			<portlet:param name="actionType"  value="download"/>
		</portlet:resourceURL>
		
		<portlet:actionURL var="viewVersionURL" name="viewVersion" >
			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
			<portlet:param name="fileName"  value="<%=stageFile.fileName %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=stageFile.fileId%>"/>   
        </liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="#" property="indexNo" align="center"/>
		
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() + "/images/" + stageFile.iconUrl%>"/>
		</liferay-ui:search-container-column-text>
		<%if(collec.isLeader || stageFile.author.equals(collec.userFullName) || stageFile.fileName.contains(collec.userFullName)){ %>
			<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left"  href="<%=downloadFileURL.toString() %>"/>
		<%}else{ %>
			<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left" />
		<%}%>	
		<liferay-ui:search-container-column-text name="建立時間" property="createDate" align="center"/>
		<liferay-ui:search-container-column-text name="建立人" property="author" align="center"/>
		<liferay-ui:search-container-column-text name="修改時間" property="updateDate" align="center"/>
		<liferay-ui:search-container-column-text name="修改人" property="modifier" align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo" align="center" href="<%=viewVersionURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="狀態" property="docStatus" align="center"/>
		<liferay-ui:search-container-column-text name="KB" property="fileSize" align="center"/>
		
		<%if(!collec.reportClosed){ %>
			<liferay-ui:search-container-column-jsp path="/jsp/admin/admin.jsp" align="center" />
		<%} %>	
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

</aui:fieldset>
</aui:form>

<%if(collec.vecComment != null){ %>
	<aui:fieldset label="已上傳之檢查評述檔案">
	
	<liferay-ui:search-container emptyResultsMessage=""  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecComment, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecComment.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.StageFile" keyProperty="fileId" modelVar="stageFile">
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
  			<portlet:param name="actionType"  value="download"/>
		</portlet:resourceURL>
		
		<portlet:actionURL var="viewVersionURL" name="viewVersion" >
			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
			<portlet:param name="fileName"  value="<%=stageFile.fileName %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() + "/images/" + stageFile.iconUrl%>"/>
		</liferay-ui:search-container-column-text>
		<%if(collec.isLeader || stageFile.author.equals(collec.userFullName) || stageFile.fileName.contains(collec.userFullName)){ %>
			<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left"  href="<%=downloadFileURL.toString() %>"/>
		<%}else{ %>
			<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left" />
		<%}%>	
		<liferay-ui:search-container-column-text name="建立時間" property="createDate" align="center"/>
		<liferay-ui:search-container-column-text name="建立人" property="author" align="center"/>
		<liferay-ui:search-container-column-text name="修改時間" property="updateDate" align="center"/>
		<liferay-ui:search-container-column-text name="修改人" property="modifier" align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo" align="center" href="<%=viewVersionURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="狀態" property="docStatus" align="center"/>
		<liferay-ui:search-container-column-text name="KB" property="fileSize" align="center"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
</aui:fieldset>
<%} %>
<br/>

<aui:fieldset label="作廢的檔案">
<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecDeletedFiles, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecDeletedFiles.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.StageFile" keyProperty="fileId" modelVar="stageFile">
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
  			<portlet:param name="actionType"  value="download"/>
		</portlet:resourceURL>
		
		<portlet:actionURL var="viewVersionURL" name="viewVersion" >
			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
			<portlet:param name="fileName"  value="<%=stageFile.fileName %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() + "/images/" + stageFile.iconUrl%>"/>
		</liferay-ui:search-container-column-text>	
		
		<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left" href="<%=viewVersionURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="刪除時間" property="updateDate" align="center"/>
		<liferay-ui:search-container-column-text name="刪除人" property="deletedBy" align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo" align="center"/>
		
		<%if(!collec.reportClosed){ %>
			<liferay-ui:search-container-column-jsp path="/jsp/admin/adminDeactive.jsp" align="center" />
		<%} %>	
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
</aui:fieldset>
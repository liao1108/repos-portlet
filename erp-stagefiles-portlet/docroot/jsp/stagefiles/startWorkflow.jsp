<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

<%
	String fileId = renderRequest.getParameter("fileId");
	String fileName = renderRequest.getParameter("fileName");
	String stageFolderId = renderRequest.getParameter("stageFolderId");
	//
	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Taipei"));
	cal.add(Calendar.DATE, 15);
	String deadLine = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DAY_OF_MONTH);
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="startWorkflowURL"  name="startWorkflow" >
</portlet:actionURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/workflow.png">&nbsp;啟動文件流程
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset>
<aui:form action="<%=startWorkflowURL%>"   method="post"  name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="fileId"   value="<%=fileId %>" />	
			<aui:input type="hidden" name="fileName"   value="<%=fileName %>" />
			<aui:input type="hidden" name="stageFolderId"   value="<%=stageFolderId %>" />
			檔名：<%=fileName %>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">
			<aui:select name="workflowType" label="流程種類">
				<aui:option>&nbsp;</aui:option>
				<aui:option value="<%=ErpUtil.TASK_FILL_REPORT %>"><%=ErpUtil.TASK_FILL_REPORT %></aui:option>
				<aui:option value="<%=ErpUtil.TASK_MODIFY_REPORT %>"><%=ErpUtil.TASK_MODIFY_REPORT %></aui:option>
				<aui:option value="<%=ErpUtil.TASK_REVIEW %>"><%=ErpUtil.TASK_REVIEW %></aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="text" name="taskComment"   label="任務摘要"  size="50"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="text" name="assignee"   label="受任人" />
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column cssClass="td">
				<h5>處理期限</h5>
				<liferay-ui:input-date dayParam="dayDead"  monthParam="monDead"  yearParam="yearDead"
											 yearValue="<%=ErpUtil.getYear(deadLine) %>"  monthValue="<%=ErpUtil.getMonth(deadLine)-1%>"  dayValue="<%=ErpUtil.getDay(deadLine) %>"
												yearRangeStart="<%=ErpUtil.getYear(deadLine)  - 1 %>" yearRangeEnd="<%=ErpUtil.getYear(deadLine) + 1%>"/>
		</aui:column>
	</aui:layout>												
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>	
		<aui:button type="submit" value="確定啟動"  cssClass="button"/>
	</aui:button-row>
</aui:form>
</aui:fieldset>




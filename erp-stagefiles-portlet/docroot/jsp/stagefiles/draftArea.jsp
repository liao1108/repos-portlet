<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>


<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	Vector<StageFile> vecDraft = new Vector<StageFile>();
	if(renderRequest.getPortletSession(true).getAttribute("vecDraft") != null){
		vecDraft = (Vector<StageFile>)renderRequest.getPortletSession(true).getAttribute("vecDraft");
	}
	String folderId = "";
	if(renderRequest.getParameter("folderId") != null){
		folderId = renderRequest.getParameter("folderId");
	}
	
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/stagefiles/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="genPointLeaderDraftURL" name="genPointLeaderDraft" >
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>

<portlet:actionURL var="genEssenLeaderDraftURL" name="genEssenLeaderDraft" >
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>

<portlet:actionURL var="genInfoExamStateLeaderDraftURL" name="genInfoExamStateLeaderDraft" >
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInterDraftURL" name="genSpotDefectReportInterDraft" >
	<portlet:param name="infoExam"  value="false"/>
	<portlet:param name="draft"  value="true"/>
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInter4LocalBankHQNormalDraftURL" name="genSpotDefectReportInterDraft" >
	<portlet:param name="localBankHQNormal"  value="true"/>
	<portlet:param name="draft"  value="true"/>
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>

<portlet:actionURL var="genSpotDefectReportInter4InfoExamDraftURL" name="genSpotDefectReportInterDraft" >
	<portlet:param name="infoExam"  value="true"/>
	<portlet:param name="draft"  value="true"/>
	<portlet:param name="folderId" value="<%=folderId %>"/>
</portlet:actionURL>


<aui:layout>
	<aui:column cssClass="header">
		<img src="<%=renderRequest.getContextPath()%>/images/draft.png">&nbsp;彙整表草稿檔案：檢查證號(<%=collec.examNo %>) / 階段(<%=collec.stageName %>)
	</aui:column>
</aui:layout>

<hr/>

<aui:layout>
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>
		<%if(collec.stageName.contains("結束")){ %>
			<%if(collec.isComputerAudit){ %>
				<aui:button onClick="<%=genInfoExamStateLeaderDraftURL.toString() %>"  value="產出重要查核項目彙整表草稿" cssClass="button"/>
			<%}else{ %>
				<aui:button onClick="<%=genPointLeaderDraftURL.toString() %>"  value="產出檢查重點彙整表草稿" cssClass="button"/>
				<%if(collec.isEssenNeed){ %>
					<aui:button onClick="<%=genEssenLeaderDraftURL.toString() %>"  value="產出必查項目彙整表草稿" cssClass="button"/>
				<%} %>		
			<%}%>
			<%if(collec.isComputerAudit){ %>
				<aui:button onClick="<%=genSpotDefectReportInter4InfoExamDraftURL.toString() %>"  value="產出內部版缺失情形表草稿" cssClass="button"/>
			<%}else if(collec.isRatingAssess && !collec.isComputerAudit){ %>
				<aui:button onClick="<%=genSpotDefectReportInter4LocalBankHQNormalDraftURL.toString() %>"  value="產出內部版缺失情形表草稿" cssClass="button"/>
			<%}else{ %>
				<aui:button onClick="<%=genSpotDefectReportInterDraftURL.toString() %>"  value="產出內部版缺失情形表草稿" cssClass="button"/>
			<%} %>	
		<%} %>			
	</aui:button-row>			
</aui:layout>

<br/>

<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecDraft, searchContainer.getStart(), searchContainer.getEnd());
			total = vecDraft.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	
	<liferay-ui:search-container-row className="com.itez.StageFile" keyProperty="fileId" modelVar="stageFile">
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=stageFile.fileId %>"/>
  			<portlet:param name="actionType"  value="download"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() + "/images/" + stageFile.iconUrl%>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left"  href="<%=downloadFileURL.toString() %>" />
		<liferay-ui:search-container-column-text name="建立時間" property="createDate" align="center"/>
		<liferay-ui:search-container-column-text name="建立人" property="author" align="center"/>
		<liferay-ui:search-container-column-text name="修改時間" property="updateDate" align="center"/>
		<liferay-ui:search-container-column-text name="修改人" property="modifier" align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo" align="center" />
		<liferay-ui:search-container-column-text name="KB" property="fileSize" align="center"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>



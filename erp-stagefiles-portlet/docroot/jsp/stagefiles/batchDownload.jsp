<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	HashMap<String, String> htBatchDoc = new HashMap<String, String>();
	if(renderRequest.getPortletSession(true).getAttribute("htBatchDoc") != null){
		htBatchDoc = (HashMap<String, String>)renderRequest.getPortletSession(true).getAttribute("htBatchDoc");
	}
	Iterator<String> it = htBatchDoc.keySet().iterator();
	//
	String stageName = collec.stageName;
	int idx = stageName.indexOf(".");
	if(idx > 0){
		stageName = stageName.substring(idx + 1);
	}
	String zipName = collec.bankName + "_" + stageName + ".zip";
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:resourceURL var="batchDownloadURL" >
	<portlet:param name="actionType" value="batchDownload"/>
	<portlet:param name="zipName" value="<%=zipName %>"/>
	<portlet:param name="jspPage" value="/jsp/stagefiles/view.jsp" />;	
</portlet:resourceURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	


<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/zipDownload.png">&nbsp;本批次即將下載的檔案(檔名為<%=zipName %>)：
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset>
<%while(it.hasNext()){
	String key = it.next();
	String fileName = htBatchDoc.get(key);
%>

	<aui:layout>
		<aui:column cssClass="td"><%=fileName %></aui:column>
	</aui:layout>
	
<%} %>	
<aui:layout>
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>	
		<aui:button onClick="<%=batchDownloadURL.toString() %>" value="開始下載"  cssClass="button"/>
	</aui:button-row>
</aui:layout>

</aui:fieldset>




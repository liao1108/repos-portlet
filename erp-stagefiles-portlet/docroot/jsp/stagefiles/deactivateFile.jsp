<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}

	//String stageName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("stageName") != null){
	//	stageName = renderRequest.getPortletSession(true).getAttribute("stageName").toString();
	//}

	String fileId = "";
	if(renderRequest.getParameter("fileId") != null){
		fileId = renderRequest.getParameter("fileId");
	}
	String fileName = "";
	if(renderRequest.getParameter("fileName") != null){
		fileName = renderRequest.getParameter("fileName");
	}
	//
	//Vector<StageFile> vecFiles = new Vector<StageFile>();
	//if(renderRequest.getPortletSession(true).getAttribute("vecFiles") != null){
	//	vecFiles = (Vector<StageFile>)renderRequest.getPortletSession(true).getAttribute("vecFiles");
	//}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="deactivateStageFileURL"  name="deactivateStageFile" >
	<portlet:param name="fileId" value="<%=fileId %>"/>
</portlet:actionURL>

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;工作階段：<%=collec.stageName %>
	</aui:column>
</aui:layout>

<hr/>

<br/>

<aui:form action="<%=deactivateStageFileURL.toString()%>"  method="post" >
	<aui:layout>
		<aui:column cssClass="th">
			&nbsp;※ 確定作廢檔案：&nbsp;<%=fileName %>&nbsp;？&nbsp;
		</aui:column>
		<aui:column>
			<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消" cssClass="button" />
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="確定"  cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>

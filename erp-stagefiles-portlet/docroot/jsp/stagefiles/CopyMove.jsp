<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	
	//String stageName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("stageName") != null){
	//	stageName = renderRequest.getPortletSession(true).getAttribute("stageName").toString();
	//}
	//
	//Vector<StageFile> vecFiles = new Vector<StageFile>();
	//if(renderRequest.getPortletSession(true).getAttribute("vecFiles") != null){
	//	vecFiles = (Vector<StageFile>)renderRequest.getPortletSession(true).getAttribute("vecFiles");
	//}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="uploadFileURL"  name="uploadFile" >
</portlet:actionURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	


<script  type="text/javascript">	
	function sendingIndustry() {
		Liferay.fire('fileUploadOk', "true");
		//
		AUI().DialogManager.hideAll();
	}
</script>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;檔案列表：<%=collec.stageName %>
	</aui:column>
</aui:layout>

<hr/>

<aui:form action="<%=uploadFileURL%>"  enctype="multipart/form-data" method="post" >
	<aui:layout>
		<aui:column>
			<aui:input type="file" name="fileName" label="請指定所要上傳的檔案" size="75"/>
		</aui:column>
		<aui:column>
			<br/>
			<aui:button type="submit" value="開始上傳" cssClass="button"/>
		</aui:column>
		<aui:column>
			<br/>
			<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>
		</aui:column>

	</aui:layout>
</aui:form>

<br/>

<liferay-ui:search-container emptyResultsMessage = "" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecFiles, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecFiles.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.StageFile" keyProperty="fileId" modelVar="stageFile">
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() + "/images/" + stageFile.iconUrl%>"/>
		</liferay-ui:search-container-column-text>	
		<liferay-ui:search-container-column-text name="檔名" property="fileName"   align="left" />
		<liferay-ui:search-container-column-text name="建立時間" property="createDate" align="center"/>
		<liferay-ui:search-container-column-text name="更新時間" property="updateDate" align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo" align="center"/>
		<liferay-ui:search-container-column-text name="狀態" property="docStatus" align="center"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>


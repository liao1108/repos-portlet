<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.LossItem"%>
<%@ page import= "com.itez.ErpUtil"%>


<portlet:defineObjects />

<%
	Vector<LossItem> vecLoss = new Vector<LossItem>();
	if(renderRequest.getPortletSession(true).getAttribute("vecLoss") != null){
		vecLoss = (Vector<LossItem>)renderRequest.getPortletSession(true).getAttribute("vecLoss");
	}
	//
	int rowsPrefer = 20;
	if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
		rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
	//
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("jspPage", "/jsp/stagefiles/lostItem.jsp");
	
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/lost.png">&nbsp;未填寫的查核項目列表
	</aui:column>
</aui:layout>

<hr/>

<br/>

<liferay-ui:search-container emptyResultsMessage=""  delta="<%=rowsPrefer %>"  iteratorURL="<%=portletURL%>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecLoss, searchContainer.getStart(), searchContainer.getEnd());
			total = vecLoss.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.LossItem" keyProperty="nodeId" modelVar="lossItem">
		<liferay-ui:search-container-column-text name="助檢人員" property="staffName"/>
		<liferay-ui:search-container-column-text name="檢查業務" property="bisCat"/>
		<liferay-ui:search-container-column-text name="查核項目" property="itemName"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<aui:layout>
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>
	</aui:button-row>			
</aui:layout>


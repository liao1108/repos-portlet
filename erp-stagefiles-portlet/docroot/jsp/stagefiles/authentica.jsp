<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	//Collec collec = new Collec();
	//if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
	//	collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	//}

	//String stageName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("stageName") != null){
	//	stageName = renderRequest.getPortletSession(true).getAttribute("stageName").toString();
	//}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doAuthenticaRunURL"  name="doAuthenticaRun" >
</portlet:actionURL>

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;執行檔案加密保護
	</aui:column>
</aui:layout>

<hr/>

<br/>

<aui:fieldset>
<aui:form action="<%=doAuthenticaRunURL.toString()%>"  method="post" >
	<aui:layout>
		<aui:column cssClass="th">
			※ 檔案一經加密保護後即無法還原，必須透過申調程序才可讀取內容。
		</aui:column>
	</aui:layout>
		
	<aui:layout>
		<aui:column>
			&nbsp;
		</aui:column>
		
		<aui:column cssClass="th">
			&nbsp;&nbsp;確定進行加密保護？
		</aui:column>
	</aui:layout>

	<aui:layout>	
		<aui:column>
			&nbsp;
		</aui:column>
		<aui:column>
			&nbsp;
		</aui:column>

		<aui:column>
			<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消作業" />
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="確定執行" />
		</aui:column>
	</aui:layout>
</aui:form>
</aui:fieldset>
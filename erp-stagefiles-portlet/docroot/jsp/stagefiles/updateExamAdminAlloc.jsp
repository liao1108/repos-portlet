<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

<%
	String fileId = renderRequest.getParameter("fileId");
	String fileName = renderRequest.getParameter("fileName");
	String stageFolderId = renderRequest.getParameter("stageFolderId");
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateExamAdminAllocURL"  name="updateExamAdminAlloc" >
</portlet:actionURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/updateBack.png">&nbsp;更新檢查行政系統之工作分配資料
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset>
<aui:form action="<%=updateExamAdminAllocURL%>"   method="post"  name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="fileId"   value="<%=fileId %>" />	
			<aui:input type="hidden" name="fileName"   value="<%=fileName %>" />
			<aui:input type="hidden" name="stageFolderId"   value="<%=stageFolderId %>" />
			檔名：<%=fileName %>
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁"  cssClass="button"/>	
		<aui:button type="submit" value="確定更新"  cssClass="button"/>
	</aui:button-row>
</aui:form>
</aui:fieldset>




<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.StageFile"%>
<%@ page import= "com.itez.Collec"%>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	String fileId = "";
	if(renderRequest.getParameter("fileId") != null){
		fileId = renderRequest.getParameter("fileId");
	}
	String origFileName = "";
	if(renderRequest.getParameter("origFileName") != null){
		origFileName = renderRequest.getParameter("origFileName");
	}
	String stageFolderId = "";
	if(renderRequest.getParameter("stageFolderId") != null){
		stageFolderId= renderRequest.getParameter("stageFolderId");
	}
	//
	String stageName = collec.stageName;
	//
	if(fileId.equals("") && renderRequest.getPortletSession(true).getAttribute("currFile") != null){
		StageFile sf = (StageFile)renderRequest.getPortletSession(true).getAttribute("currFile");
		fileId = sf.fileId;
		origFileName = sf.fileName;
		stageFolderId = sf.stageFolderId;
		stageName = sf.stageName;
	}
	//
	//Vector<StageFile> vecFiles = new Vector<StageFile>();
	//if(renderRequest.getPortletSession(true).getAttribute("vecFiles") != null){
	//	vecFiles = (Vector<StageFile>)renderRequest.getPortletSession(true).getAttribute("vecFiles");
	//}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateFileURL"  name="updateFile" >
</portlet:actionURL>	

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/jsp/stagefiles/view.jsp"/>
</portlet:actionURL>	


<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;檔案內容更新：
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset>
<aui:form action="<%=updateFileURL%>"  enctype="multipart/form-data" method="post" >
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="origFileName"   value="<%=origFileName %>" />
			原檔名：<%=origFileName %>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="fileId"  value="<%=fileId %>" />
			<aui:input type="hidden" name="stageFolderId"  value="<%=stageFolderId %>" />
			
			<aui:input type="file" name="fileName" label="請指定所要上傳的檔案" size="100"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="text" name="updateMemo"  label="更新摘要" size="100"/>
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" cssClass="button"/>	
		<aui:button type="submit" value="開始上傳"  cssClass="button"/>
	</aui:button-row>
</aui:form>
</aui:fieldset>




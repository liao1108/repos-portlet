<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>

<%@ page import="com.itez.StageFile" %>
<%@ page import="com.itez.ErpUtil" %>
<%@ page import="com.itez.Collec" %>
<%@ page import="com.itez.alloc.ExamCommentProduceType" %>

<portlet:defineObjects />

<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	//
	//String dateCurr = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
	//
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	StageFile sf = (StageFile)row.getObject();
	//
	String editOnlineURL = "javascript: openWebDAVFile('" + sf.webDavURL + "');";
	String copyToClipBoardURL = "javascript: copyToClipBoard('" + sf.webDavURL + "');";
	String showWebDavURL = "javascript: showWebDavURL('" + sf.webDavURL + "');";
%>

<script type="text/javascript">
	function openWebDAVFile(url) {
		//alert(url);
		new ActiveXObject("SharePoint.OpenDocuments.1").EditDocument(url);
		//url.execCommand("Copy")

	};
	
	function copyToClipBoard(url){
			try{
				var result = window.clipboardData.setData('Text', url);
				if(result){
					alert("已將檔案編輯網址複製到剪貼簿。");
				}else{
					alert("檔案編輯網址無法複製到剪貼簿！");
				}
			}catch(ex){
				alert("檔案編輯網址無法複製到剪貼簿！");
			}
	}
	
	function showWebDavURL(url){
		prompt("檔案編輯網址： ", url);
	}
</script>


<liferay-ui:icon-menu>
	<portlet:actionURL var="genDetailAllocationURL"  name="genDetailAllocation">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>
	
	<portlet:actionURL var="genEmptyFormPointStaffURL"  name="genEmptyFormStaff">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="檢查重點"/>
	</portlet:actionURL>

	<portlet:renderURL var="goRemoveEmptyFormPointStaffURL">
		<portlet:param name="jspPage" value="/jsp/stagefiles/removeGenedFiles.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="檢查重點"/>
	</portlet:renderURL>

	<portlet:renderURL var="goRemoveEmptyFormEssenStaffURL">
		<portlet:param name="jspPage" value="/jsp/stagefiles/removeGenedFiles.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="必查項目"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="goRemoveEmptyStateFormStaff4IfURL">
		<portlet:param name="jspPage" value="/jsp/stagefiles/removeGenedFiles.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="保外組檢查評述"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="goRemoveEmptyStateFormStaff4SecURL">
		<portlet:param name="jspPage" value="/jsp/stagefiles/removeGenedFiles.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="證票組檢查評述"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="goRemoveEmptyInfoExamStateStaffURL">
		<portlet:param name="jspPage" value="/jsp/stagefiles/removeGenedFiles.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="資訊檢查重要查核"/>
	</portlet:renderURL>
	
	<portlet:actionURL var="genEmptyFormEssenStaffURL"  name="genEmptyFormStaff">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="formName" value="必查項目"/>
	</portlet:actionURL>

	<portlet:actionURL var="genEmptyEvalFormStaffURL"  name="genEmptyEvalFormStaff">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:actionURL var="genEmptyStateFormStaffURL"  name="genEmptyStateFormStaff">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:renderURL var="goDeactivateStageFileURL_BAK" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/deactivateFile.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
	</portlet:renderURL>

	<portlet:actionURL var="deactivateStageFileURL"  name="deactivateStageFile" >
		<portlet:param name="fileId"  value="<%=sf .fileId %>"/>
	</portlet:actionURL>

	<portlet:actionURL var="doCheckOutURL"  name="doCheckOut">
  			<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
	</portlet:actionURL>
	
	<portlet:actionURL var="doCheckInURL"  name="doCheckIn">
  			<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:resourceURL var="downloadFileURL" >
  		<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
  		<portlet:param name="actionType"  value="download"/>
  		<portlet:param name="currentTime"  value="<%=ErpUtil.getCurrDateTime() %>"/>
	</portlet:resourceURL>

	<portlet:actionURL var="moveToPreMeetURL"  name="moveToPreMeet">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:actionURL var="genPdfFileURL"  name="genPdfFile">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:renderURL var="updateFileURL" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/update.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="origFileName" value="<%=sf.fileName %>"/>
	</portlet:renderURL>

	<portlet:renderURL var="goAsk4ApproveURL" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/ask4approve.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
	</portlet:renderURL>

	<portlet:renderURL var="goStartWorkflowURL" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/startWorkflow.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
		<portlet:param name="stageFolderId" value="<%=sf.stageFolderId %>"/>
	</portlet:renderURL>
	
	<portlet:actionURL var="doCancelCheckOutURL" name="doCancelCheckOut" >
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:actionURL var="genExamBriefSummaryURL" name="genExamBriefSummary" >
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>
	
	<portlet:actionURL var="genEmptyInfoExamStateStaffURL" name="genEmptyInfoExamStateStaff" >
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>
	
	<portlet:actionURL var="startAllocateEditURL" name="startAllocateEdit" >
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
	</portlet:actionURL>

	<portlet:resourceURL var="editAllocateURL" >
  		<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
  		<portlet:param name="actionType"  value="edit"/>
	</portlet:resourceURL>

	<portlet:resourceURL var="exportAllocateURL" >
  		<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
  		<portlet:param name="actionType"  value="exportAllocate"/>
	</portlet:resourceURL>

	<portlet:renderURL var="goUpdateExamAdmintAllocURL" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/updateExamAdminAlloc.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
		<portlet:param name="stageFolderId" value="<%=sf.stageFolderId %>"/>
	</portlet:renderURL>
	
<%if(!sf.isLocked){ %>	
	<%if(sf.isWorkingCopy){ %>
		<liferay-ui:icon image="view" message="下載" url="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:icon image="edit" message="更新" url="<%=updateFileURL.toString() %>"/>
		<liferay-ui:icon image="edit" message="取消領出" url="<%=doCancelCheckOutURL.toString() %>"/>
	<%}else{ %>
		<%if(sf.stageName.contains("行前")){ %>
			<%if(sf.fileName.toLowerCase().endsWith("json") && collec.isLeader && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
				<%if(collec.isComputerAudit){ 				//資訊檢查%>
					<liferay-ui:icon image="edit" message="產出助檢版重要查核項目報告表" url="<%=genEmptyInfoExamStateStaffURL.toString() %>"/>
					<liferay-ui:icon image="delete" message="刪除已產出之重要查核項目報告表" url="<%=goRemoveEmptyInfoExamStateStaffURL.toString() %>"/>
				<%}else{ %>	
					<liferay-ui:icon image="edit" message="產出檢查重點報告表" url="<%=genEmptyFormPointStaffURL.toString() %>"/>
					<liferay-ui:icon image="delete" message="刪除已產出之檢查重點報告表" url="<%=goRemoveEmptyFormPointStaffURL.toString() %>"/>
					<%if(!collec.isSepecialExam){ %>
						<liferay-ui:icon image="edit" message="產出必查項目報告表" url="<%=genEmptyFormEssenStaffURL.toString() %>"/>
						<liferay-ui:icon image="delete" message="刪除已產出之必查項目報告表" url="<%=goRemoveEmptyFormEssenStaffURL.toString() %>"/>
					<%} %>	
					<%if(collec.examCommentProduceType==ExamCommentProduceType.EXAM_COMMENT_PRODUCE_GENERAL.ordinal()){ 			//保外組%>
						<liferay-ui:icon image="edit" message="產出助檢版檢查評述檔" url="<%=genEmptyStateFormStaffURL.toString() %>"/>
						<liferay-ui:icon image="delete" message="刪除已產出之助檢版檢查評述檔" url="<%=goRemoveEmptyStateFormStaff4IfURL.toString() %>"/>
					<%} %>
					<%if(collec.examCommentProduceType==ExamCommentProduceType.EXAM_COMMENT_PRODUCE_BLANK.ordinal()){ 			//證票組%>
						<liferay-ui:icon image="edit" message="產出助檢版檢查評述檔" url="<%=genEmptyStateFormStaffURL.toString() %>"/>
						<liferay-ui:icon image="delete" message="刪除已產出之助檢版檢查評述檔" url="<%=goRemoveEmptyStateFormStaff4SecURL.toString() %>"/>
					<%} %>
				<%} %>
				<liferay-ui:icon image="edit" message="編輯工作分配表" url="<%=editAllocateURL.toString() %>"/>
				<liferay-ui:icon image="edit" message="匯出工作分配表" url="<%=exportAllocateURL.toString() %>"/>	
			<%} %>	
		<%}else if(sf.stageName.contains("結束")){ %>
	
		<%}else if(sf.stageName.contains("會前")){ %>
			<%if(collec.isLeader && sf.docStatus.equals(ErpUtil.STATUS_DRAFT) && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
				<liferay-ui:icon image="edit" message="請求審核" url="<%=goAsk4ApproveURL.toString() %>"/>
			<%} %>	
		<%} %>
		
		<%if(collec.isLeader || sf.author.equals(collec.userFullName) || sf.fileName.contains(collec.userFullName) && collec.dateStart.compareTo(collec.dateWebLife) <= 0){ %>
			<liferay-ui:icon image="view" message="下載" url="<%=downloadFileURL.toString() %>"/>
			
			<%if(!sf.docStatus.equals(ErpUtil.STATUS_DRAFT) && !sf.docStatus.equals(ErpUtil.STATUS_APPROVING) && !sf.docStatus.equals("未簽收")){ %>
				<liferay-ui:icon image="edit" message="上傳更新" url="<%=updateFileURL.toString() %>"/>
			<%} %>
			
			<%if(sf.fileName.toLowerCase().endsWith("doc")){ %>
				<liferay-ui:icon image="edit" message="轉存PDF" url="<%=genPdfFileURL.toString() %>"/>
			<%} %>	
			<liferay-ui:icon image="delete" message="作廢" url="<%=deactivateStageFileURL.toString() %>"/>
			
			<%if(sf.fileName.contains("檢查提要") && sf.fileName.contains("初稿")){ %>
				<liferay-ui:icon image="edit" message="產出檢查提要檔" url="<%=genExamBriefSummaryURL.toString() %>"/>
			<%} %>
	
			<liferay-ui:icon image="edit" message="啟動流程" url="<%=goStartWorkflowURL.toString() %>"/>
		<%} %>
	<%} %>
<%} %>	
</liferay-ui:icon-menu>
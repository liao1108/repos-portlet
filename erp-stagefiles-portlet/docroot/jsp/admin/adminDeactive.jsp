<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.StageFile" %>
<%@ page import="com.itez.Collec" %>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}	
	
	//boolean isLeader = false;
	//if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
	//	isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	//}
	
	//String userFullName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
	//	userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	//}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	StageFile sf = (StageFile)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="goDeleteStageFileURL" >
		<portlet:param name="jspPage" value="/jsp/stagefiles/deleteFile.jsp"/>
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
		<portlet:param name="fileName" value="<%=sf.fileName %>"/>
	</portlet:renderURL>
	
	<portlet:actionURL var="activateStageFileURL"  name="activateStageFile">
		<portlet:param name="fileId" value="<%=sf.fileId %>"/>
	</portlet:actionURL>

	<portlet:resourceURL var="downloadFileURL" >
		<portlet:param name="fileId"  value="<%=sf.fileId %>"/>
		<portlet:param name="actionType"  value="download"/>
	</portlet:resourceURL>

	<%if(collec.isLeader || sf.author.equals(collec.userFullName)){ %>
		<liferay-ui:icon image="view" message="下載" url="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:icon image="edit" message="恢復" url="<%=activateStageFileURL.toString() %>"/>
		<liferay-ui:icon image="delete" message="刪除" url="<%=goDeleteStageFileURL.toString() %>"/>
	<%} %>		
</liferay-ui:icon-menu>
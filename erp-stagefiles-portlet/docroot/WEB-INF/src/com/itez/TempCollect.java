package com.itez;

import java.io.InputStream;
import java.util.HashMap;

import org.alfresco.cmis.client.AlfrescoDocument;
import com.liferay.portal.kernel.repository.model.FileEntry;

public class TempCollect {
	public AlfrescoDocument alfAllocateJson = null;
	public AlfrescoDocument fPointDocAssistant = null;
	public AlfrescoDocument fEssenDocAssistant = null;
	public AlfrescoDocument fPointDocLeader = null;
	public AlfrescoDocument fEssenDocLeader = null;
	//檢討會前內部版實地檢查缺失情形表
	public HashMap<String, InputStream> htSpotDefectInter = new HashMap<String, InputStream>();
	//檢討會前外部版實地檢查缺失情形表
	public HashMap<String, InputStream> htSpotDefectOuter = new HashMap<String, InputStream>();
	//檢討會後內部版實地檢查缺失情形表
	public HashMap<String, InputStream> htSpotDefectAfter = new HashMap<String, InputStream>();
	
	//檢查意見.210N
	public InputStream examOpinion210 = null;
	//面請改善缺失.910N
	public InputStream examFace910 = null;
	//面請改善缺失.910N2 (轉銀行局央行及存保)
	public InputStream examFace910N2 = null;
	//檢查意見分級統計表
	public InputStream examOpinionStatistic = null;
	//檢查報告目錄
	public InputStream examReportIndex = null;
	
	//------------------評等審議----------------------
	//1.本會內部參考事項
	public InputStream evalInterRefItem = null;
	//2.評等項目別查核內容摘要表
	public InputStream evalSummFormByEvaltem = null;
	//3.評鑑細項得分一覽表
	public InputStream evalItemScoreList = null;
	//4.評鑑細項評量表
	public InputStream evalItemEvalForm = null;
	//5.編表連動底稿
	//public InputStream evalGridForm = null;
	//6.檢查項目別查核內容摘要表
	public InputStream evalSummFormByExamItem = null;
	
	//-----------------保外內參--------------------------
	public HashMap<String, InputStream> htInsForeState = new HashMap<String, InputStream>();
	
	//-----------------資訊作業-------------------------
	public AlfrescoDocument fInfoExamImpChkStaffForm = null;		//助檢版重要查核項目報告表
	public AlfrescoDocument fInfoExamImpChkLeaderForm = null;	//領隊版重要查核項目報告表
	public AlfrescoDocument fInfoExamAssessForm = null;			//專案檢查評量表
	public AlfrescoDocument fInfoExamAssessScore = null;			//專案檢查評級一覽表
	public AlfrescoDocument fInfoExamItemTableHeader = null;		//查核項目表頭
	
	//----------------證票組檢查評述---------------
	//public FileEntry fSecuritiesNotesExamState = null;		
	public AlfrescoDocument fExamComment = null;
	public AlfrescoDocument fExamComment2 = null;		//檢查評述(評等用)
	
	//---------------檢查報告封面 ---------------
	public AlfrescoDocument fExamCover = null;
	
	//--------------檢查報告處理流程表--------
	public AlfrescoDocument fProcessFlow = null;
	
	//--------------派差作業各業別檢查項目表---
	public AlfrescoDocument fAllocateExamItemPerBis = null;
	
}

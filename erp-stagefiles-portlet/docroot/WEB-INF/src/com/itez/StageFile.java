package com.itez;

public class StageFile {
	public int indexNo = 0;
	public String stageFolderId = "";
	public String stageName = "";
	public String fileId = "";
	public String fileName = "";
	public String createDate = "";
	public String updateDate = "";
	public String iconUrl = "";
	public String versionNo = "";
	public int countDownload = 0;
	public String docStatus = "";			//狀態
	public String deletedBy = "";
	public String author = "";			//建立者
	public String modifier = "";		//最近修改者
	public String fileSize = "";
	public String leaderName = "";
	public boolean isClosed = false;		//報告案是否已關閉
	public String webDavURL = "";			//線上編輯的 URL
	public boolean isLocked = false;
	public boolean isWorkingCopy = false;
}

package com.itez;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
//import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.activation.MimetypesFileTypeMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.bindings.spi.atompub.AbstractAtomPubService;
import org.apache.chemistry.opencmis.client.bindings.spi.atompub.AtomPubParser;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
//import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.http.HttpHeaders;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.aspose.words.SaveFormat;
import com.itez.alloc.Alloc;
import com.itez.alloc.AssStaffName_TreeMap_AssCatSum_TreeSet;
import com.itez.alloc.CompositeNo;
import com.itez.alloc.ExamCommentProduceType;

public class StageFilesPortlet extends MVCPortlet {
	DataSource ds = null;
	
	String rootReports = "";
	//String rootJobTemp = "";
	String rootDocTemp = "";
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		//mailSession = (javax.mail.Session) envContext.lookup("mail/Session");
	   		//
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootReports = rs.getString("folder_reports");
	   			rootDocTemp = rs.getString("folder_doc_temp");
	   		}
	   		rs.close();
	   		//
	   		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int examId = 0 ;
			//
			conn = ds.getConnection();
			if(LiferaySessionUtil.getGlobalSessionAttribute("currExamId", renderRequest) != null){
				examId = (int)LiferaySessionUtil.getGlobalSessionAttribute("currExamId", renderRequest);
			}else{
				String sql = "SELECT exam_id FROM curr_stage WHERE row_timestamp IS NOT NULL AND user_no=? ORDER BY row_timestamp DESC";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, themeDisplay.getUser().getScreenName());
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					examId = rs.getInt(1);
				}
				rs.close();
			}
			//
			if(examId == 0){
				conn.close();
				return;
			}
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdExam = null;
			//
			String sql = "SELECT folder_id FROM exams WHERE exam_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(rs.getString(1));
			}
			rs.close();
			if(fdExam == null) throw new Exception("找不到檢查證之存檔目錄。");
			//		
			String stageFolderId = "";
			//
			sql = "SELECT stage_folder_id FROM curr_stage WHERE exam_id=? AND user_no=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ps.setString(2, themeDisplay.getUser().getScreenName());
			rs = ps.executeQuery();
			if(rs.next()){
				stageFolderId = rs.getString(1);
			}
			rs.close();
			//
			AlfrescoFolder fdPrepare = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
					if(stageFolderId.equals("")){		//如果未指定階段, 以行前為標的階段
						stageFolderId = co.getId();
					}
					fdPrepare = (AlfrescoFolder)co;
					break;
				}
			}
			if(fdPrepare == null) throw new Exception("找不到檢查行前階段。");
			//
			//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			//
			Collec collec = this.getStageFiles(stageFolderId, themeDisplay, conn, userFullName, alfSessionAdmin, fdPrepare);
			boolean reportClosed = ErpUtil.isReportClosed(examId, conn);
			collec.reportClosed = reportClosed;
				
			renderRequest.getPortletSession(true).setAttribute("collec", collec);
			//
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}

	@javax.portlet.ProcessEvent(qname = "{http://itez.com}sendStageFolderId")
	 public void processLoadStageEvent(javax.portlet.EventRequest eventRequest, 
			    						javax.portlet.EventResponse eventResponse)  throws javax.portlet.PortletException, java.io.IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)eventRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			javax.portlet.Event event = eventRequest.getEvent();
			String stageFolderId = (String) event.getValue();
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//找出檢查行前階段
			AlfrescoFolder fdPrepare = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
					fdPrepare = (AlfrescoFolder)co;
					break;
				}
			}
			//
			//eventRequest.getPortletSession(true).setAttribute("stageFolderId", stageFolderId);
			//
			Collec collec = this.getStageFiles(stageFolderId, themeDisplay, conn, userFullName, alfSessionAdmin, fdPrepare);
			boolean reportClosed = ErpUtil.isReportClosed(collec.reportNo, collec.examNo, conn);
			collec.reportClosed = reportClosed;
			//
			eventRequest.getPortletSession(true).setAttribute("collec", collec);
			//
			conn.close();
			eventResponse.setRenderParameter("jspPage", "/jsp/stagefiles/view.jsp");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			eventRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(eventRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private Collec getStageFiles(String stageFolderId,
							ThemeDisplay themeDisplay,
							Connection conn,
							String userFullName,
							Session alfSessionAdmin,
							AlfrescoFolder fdPrepare) throws Exception{
		Collec collec = new Collec();
		//
		Vector<StageFile> vecFiles = new Vector<StageFile>();
		Vector<StageFile> vecDeletedFiles = new Vector<StageFile>();
		//
		//Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
		AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
		collec.userFullName = userFullName;
		collec.stageFolderId = stageFolderId;
		collec.stageName = fdStage.getName();
		collec.examNo =  ((AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId())).getName();
		//
		ExamFact ef = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		collec.reportNo = ef.reportNo;
		collec.dateStart = ef.dateStart;
		collec.bankName = ef.bankName;
		collec.bankNameShort = ef.bankNameShort;
		collec.isComputerAudit = ef.isCompuerAudit;
		collec.isEssenNeed = ef.isEssenNeed;
		collec.isInsuForeBank = ef.isInsuForeBank;
		collec.isInsuForeBankNormal = ef.isInsuForeBankNormal;
		collec.isRatingAssess = ef.isRatingAssess;
		collec.isSecuritiesNotes = ef.isSecuritiesNotes;
		collec.isSepecialExam = ef.isSpecialExam;
		collec.examCommentProduceType = ef.examCommentProduceType;
		collec.examNature = ef.examNature;
		//this.evalAllocateTiming = ef.evalAllocateTiming;
		//報告案是否已關閉
		boolean reportClosed = ErpUtil.isReportClosed(collec.reportNo, collec.examNo, conn);
		//判斷登入者是否為領隊
		if(ef.leaderName.contains(userFullName) || ef.createdBy.contains(userFullName) || ef.pmName.contains(userFullName)) collec.isLeader = true;
		if(ef.staffs.contains(userFullName)) collec.isStaff = true;
		//判斷是否為主評人員
		if(collec.stageName.contains("評等") || collec.stageName.contains("結束")){
			AlfrescoDocument alfJson = null;
			Iterator<CmisObject> it = fdPrepare.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().toLowerCase().endsWith("json") &&
						!co.getName().toLowerCase().startsWith("_del")){
					alfJson = (AlfrescoDocument)co;
					break;
				}
			}
			if(alfJson == null) throw new Exception("找不到檢查行前階段的工作分配表。" );
			//
			Alloc alloc  = this.getAllocFromFile(alfJson);
			//
			AssStaffName_TreeMap_AssCatSum_TreeSet atat = alloc.vecAssCatSum.collect_AssStaffName_TreeMap_AssCatSum_TreeSet();
			//收集主評人員姓名
			Vector<String> vecMainAssesser = new Vector<String>();
			Iterator<String> _it = atat.getAssStaffNameSet().iterator();
			while(_it.hasNext()){
				String _ms = _it.next();
				vecMainAssesser.add(_ms);
			}
			collec.vecMainAssesser = vecMainAssesser;
			//收集助檢人員
			collec.vecStaff = alloc.vecStaff;
			//
			if(atat.AssStaffName_isAssStaff(userFullName)) collec.isMainAssesser = true;	//為主評人員
			//有沒有「評等別摘要表（可以沒有『實地檢查缺失情形表(內部)(檢討會後)』）」需要被產生
			if(atat.AssStaffName_hasAssCatSum(userFullName, false)) collec.isEvalSumNeededWoSpotDefect = true;
			//有沒有「評等別摘要表（必須要有『實地檢查缺失情形表(內部)(檢討會後)』）」需要被產生
			if(atat.AssStaffName_hasAssCatSum(userFullName, true)) collec.isEvalSumNeededWSpotDefect = true;
		}
		//
		HashMap<String, StageFile> htFile = new HashMap<String, StageFile>();
		//檢查評述檔案
		Vector<StageFile> vecComment = null;
		//
		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
				AlfrescoDocument doc = (AlfrescoDocument)co;
				StageFile sf = new StageFile();
				//
				sf.fileName = doc.getName();
				if(doc.getPropertyValue("cm:author") != null){
					sf.author = doc.getPropertyValue("cm:author");
				}
				if(doc.getPropertyValue("cm:description") != null
						&& doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
					sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
				}else if(doc.getPropertyValue("it:modifier") != null){
					sf.modifier = doc.getPropertyValue("it:modifier");
				}
				
				if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
				if(!sf.author.isEmpty() && !ef.leaderName.contains(sf.author) && !ef.staffs.contains(sf.author)) {
					if(!sf.modifier.isEmpty()) {
						sf.author = sf.modifier;
					}else {
						sf.author = ef.leaderName;
					}
				}
				if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
				
				if(collec.isLeader || sf.fileName.contains(userFullName)
						||	sf.author.equals(userFullName) || sf.modifier.equals(userFullName)){
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdStage.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					sf.leaderName = ef.leaderName;
					//
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
							
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					sf.isClosed = reportClosed;
					if(sf.fileName.contains("Working Copy")){
						sf.isWorkingCopy = true;
					}else if(doc.isVersionSeriesCheckedOut()){
						sf.isLocked = true;
						sf.docStatus = "領出";
					}
					//
					if(sf.fileName.startsWith("_del#")){
						sf.fileName = sf.fileName.substring(5);
						//
						vecDeletedFiles.add(sf);
					}else{
						//vecFiles.add(sf);
						htFile.put(sf.fileName, sf);
					}
				}
			}else if(co instanceof org.apache.chemistry.opencmis.client.api.Folder
								&& co.getName().equals("檢查評述")) {
				vecComment = new Vector<StageFile>();
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()) {
					if(!(co2 instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
					//
					AlfrescoDocument doc = (AlfrescoDocument)co2;
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("cm:description") != null
							&& doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
						sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
					}else if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}
					
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && !ef.leaderName.contains(sf.author) && !ef.staffs.contains(sf.author)) {
						if(!sf.modifier.isEmpty()) {
							sf.author = sf.modifier;
						}else {
							sf.author = ef.leaderName;
						}
					}
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					
					if(collec.isLeader || sf.fileName.contains(userFullName)
							||	sf.author.equals(userFullName) || sf.modifier.equals(userFullName)){
						sf.fileId = doc.getId();
						sf.stageName = collec.stageName;
						sf.stageFolderId = fdStage.getId();
						//
						sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
						sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
						sf.versionNo = doc.getVersionLabel();
						sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
						if(doc.getPropertyValue("it:docStatus") != null){
							sf.docStatus = doc.getPropertyValue("it:docStatus");
						}
						sf.leaderName = ef.leaderName;
						//
						int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
						sf.fileSize = String.valueOf(dd);
								
						String iconName = "file.png";
						if(sf.fileName.toLowerCase().endsWith("xls")){
							iconName = "xls.png";
						}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
							iconName = "doc.png";
						}else if(sf.fileName.toLowerCase().endsWith("ppt")){
							iconName = "ppt.png";
						}else if(sf.fileName.toLowerCase().endsWith("pdf")){
							iconName = "pdf.png";
						}else if(sf.fileName.toLowerCase().endsWith("json")){
							iconName = "json24.png";
						}
						sf.iconUrl = iconName;
						sf.isClosed = reportClosed;
						if(sf.fileName.contains("Working Copy")){
							sf.isWorkingCopy = true;
						}else if(doc.isVersionSeriesCheckedOut()){
							sf.isLocked = true;
							sf.docStatus = "領出";
						}
						//
						vecComment.add(sf);
					}
				}
			}
		}
		collec.vecComment = vecComment;
		//排序處理
		Vector<String> vecSort = new Vector<String>();
		Iterator<String> it2 = htFile.keySet().iterator();
		while(it2.hasNext()){
			String s = it2.next();
			vecSort.add(s);
		}
		//
		int idx = 0;
		Locale theLocale = Locale.TRADITIONAL_CHINESE;
		Collator theCollator = Collator.getInstance(theLocale);
		Collections.sort(vecSort, theCollator);
		for(String fileName: vecSort){
			idx++;
			StageFile sf = htFile.get(fileName);
			sf.indexNo = idx;
			vecFiles.add(sf);
		}
		//
		collec.vecFiles = vecFiles;
		collec.vecDeletedFiles = vecDeletedFiles;
		//
		return collec;
	}
	
	//讀取工作分配表並轉成 Alloc 物件
	private Alloc getAllocFromFile(AlfrescoDocument alfDoc) throws Exception{
		InputStreamReader isr = new InputStreamReader(alfDoc.getContentStream().getStream(), "UTF-8");
		BufferedReader br = new BufferedReader(isr);
	    StringBuilder sb = new StringBuilder();
		String line = br.readLine();
	    while (line != null) {
	        sb.append(line);
	        sb.append("\n");
	        line = br.readLine();
	    }
	    String json = sb.toString();
	    br.close();
		//
		return Alloc.fromJson(json);		
	}
	
	private StageFile getStageFile(String fileId, Vector<StageFile> _vecFiles) throws Exception{
		StageFile stageFile = null;
		for(StageFile sf: _vecFiles){
			if(sf.fileId.equals(fileId)){
				stageFile = sf;
				break;
			}
		}
		return stageFile;
	}
	
	//檔案更新
	public void updateFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
	    StageFile currFile = new StageFile();
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
        	//
        	String fileId = ParamUtil.getString(uploadRequest,"fileId");
        	
        	if(fileId == null) throw new Exception("檔案 Id 為空值。");
        	
        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
        	Vector<StageFile> _vecFiles = collec.vecFiles;
        	//
        	currFile = this.getStageFile(fileId, _vecFiles);
        	if(currFile == null) throw new Exception("檔案 Id: " + fileId + " 找不到。");
        		
        	actionRequest.getPortletSession(true).setAttribute("currFile", currFile);
        	//
        	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要更新的檔案。");
	        	//
		    String sourceFileName = uploadRequest.getFileName("fileName");
	        String origFileName = ParamUtil.getString(uploadRequest,"origFileName");
        	//IE 下載時會將空白以加號取代
        	sourceFileName= sourceFileName.replace("+(Working+Copy)", " (Working Copy)");
        	if(sourceFileName.contains("(") && sourceFileName.contains(")")){
        		boolean isSaveVersion = false;
        		int idx1 = sourceFileName.lastIndexOf("(");
        		int idx2 = sourceFileName.lastIndexOf(")");
        		try{
        			Integer.parseInt(sourceFileName.substring(idx1 +1, idx2));
        			isSaveVersion = true;
        		}catch(Exception _ex){
        		}
        		if(isSaveVersion){
        			String vv = " " + sourceFileName.substring(idx1, idx2 + 1);
        			sourceFileName = sourceFileName.replace(vv, "");
        		}
        	}
        	//
        	String origPureName = origFileName.substring(0, origFileName.lastIndexOf("."));
        	String sourcePureName = sourceFileName.substring(sourceFileName.lastIndexOf("//") + 1).substring(0, sourceFileName.lastIndexOf("."));
        	String origExtName = origFileName.substring(origFileName.lastIndexOf(".") + 1);
        	String sourceExtName = sourceFileName.substring(sourceFileName.lastIndexOf(".") + 1);
	        	
		    if(!origExtName.equalsIgnoreCase(sourceExtName) || !sourcePureName.startsWith(origPureName)){ 
		        	throw new Exception("您上傳的檔名(" + sourceFileName + ")與所要更新的檔名不符。");
		    }
		    //檢查是否在流程中
		    conn = ds.getConnection();
		    //
		    String examNo = collec.examNo;
		    //String stageName = collec.stageName;
		        
		    if(underToDoListIgnoreStage(examNo, origFileName, conn)) throw new Exception(origFileName + " 已在流程處理中，無法變更。");
		    //
		    String updateMemo = ParamUtil.getString(uploadRequest,"updateMemo");
		    if(updateMemo.equals("")) updateMemo = "更新";
		    //
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId); 
			//Log
			ErpUtil.logUserAction("更新階段檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
			ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
		        //
			Map<String, String> props = new HashMap<String, String>();
			props.put("it:updateMemo", updateMemo);
			props.put("it:modifier", userFullName);
			//
		    if(!alfDoc.isVersionSeriesCheckedOut()){		//內容更新
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
		    }
			is.close();
			//是否通知領隊
			if(!collec.isLeader){
				//找出本檢查證專屬的流程設定
				String sql = "SELECT * FROM stage_rules WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, collec.examNo);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					//int formGenedMail2Assistant = rs.getInt("form_gened_mail2_assistant");		//檢查行前產出工作分配表時是否email給助檢人員
					//int inExamMail2Leader = rs.getInt("in_exam_mail2_leader");				//檢查期間有助檢文件匯入時是否通知領隊
					int examEndMail2Leader = rs.getInt("exam_end_mail2_leader");			//檢查結束助檢文件匯入時是否email給領隊
					//int examEndEnableWorkFlow = rs.getInt("exam_end_enable_workflow");		//檢查結束助檢文件匯入時是否啟動流程
					//int examEndWorkFlowType = rs.getInt("exam_end_workflow_type");			//檢查結束助檢文件匯入時之流程種類
					int preMeetEnableWorkFlow = rs.getInt("pre_meet_enable_workflow");		//檢討會前文件匯入時是否啟動流程
					int endMeetEnableWorkFlow = rs.getInt("end_meet_enable_workflow");		//檢討會後文件匯入時是否啟動流程
					//int evalMail2Leader = rs.getInt("eval_mail2_leader");					//評等審議助檢文件匯入時是否email給領隊
					//int evalKeyUserMail2Leader = rs.getInt("eval_keyuser_mail2_leader");	//評等審議主評文件匯入時是否email給領隊
					int evalEnableWorkFlow = rs.getInt("eval_enable_workflow");				//評等審議助檢文件匯入時是否啟動流程
					//int evalWorkFlowType = rs.getInt("eval_workflow_type");					//評等審議助檢文件匯入時之流程種類
					//int postEnableWorkFlow = rs.getInt("post_enable_workflow");
					//
					if((collec.stageName.contains("結束") && examEndMail2Leader == 1) ||
							(collec.stageName.contains("會前") && preMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("會後") && endMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("評等") && evalEnableWorkFlow == 1)){
						sql = "SELECT leader_name FROM exams WHERE exam_no = ?";
						ps = conn.prepareStatement(sql);
						ps.setString(1, collec.examNo);
						rs = ps.executeQuery();
						if(rs.next()){
							String toAddress = ErpUtil.getUserMailAddress(themeDisplay, rs.getString(1));
							if(!toAddress.equals("")){
								String msg = "報告編號：" + collec.reportNo + "、檢查證號：" + examNo + "之" +  collec.stageName + "階段，助檢已更新以下檔案：" + "\n";
								msg += alfDoc.getName() ;
								ErpUtil.sendMail(toAddress, "檔案更新通知", msg);
							}
						}
						rs.close();
					}
				}
				rs.close();
			}
	    }catch(Exception ex){
	        //PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        //ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRequest.getPortletSession(true).setAttribute("currFile", currFile);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/update.jsp");
	    }finally{
	       	try{
	       		if(conn != null) conn.close();
	       	}catch(Exception _ex){
	       	}
	    }
	}

	
	//檔案上傳
	@SuppressWarnings("resource")
	public void uploadFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
	        ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        //
	        Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        String stageFolderId = collec.stageFolderId;
	        //
	        UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        Vector<String> vecPaName = new Vector<String>();
	        for(int i=0; i < 10; i++){
	        	String nn = "fileName" + i;
	        	if(uploadRequest.getSize(nn) > 0 && !vecPaName.contains(nn)){
	        		vecPaName.add(nn);
	        	}
	        }
	        if (vecPaName.size() ==0)  throw new Exception("請指定所要上傳的檔案。");
	        //
	        String dupOption = "skip";			//檔名重複者
	        if(ParamUtil.getString(uploadRequest, "dupOption") != null){
	        	dupOption = ParamUtil.getString(uploadRequest, "dupOption");
	        }
		    //
		    conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdDest = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId); 
			//
			Vector<String> vecNew = new Vector<String>();
			HashMap<String, CmisObject> hUpdate = new HashMap<String, CmisObject>();
			int ccNew = 0;
			int ccUpdate = 0;
			//
			for(String paName: vecPaName){
				String sourceFileName = uploadRequest.getFileName(paName);
				//
				//if(!collec.isLeader && !sourceFileName.contains(ErpUtil.getCurrUserFullName(themeDisplay)))
				//	throw new Exception("上傳檔案:" + sourceFileName + " 未包含您的名字(" + ErpUtil.getCurrUserFullName(themeDisplay) + ")。");
				//
				if(sourceFileName.contains("檢查評述")) throw new Exception("上傳重要查核項目報告表失敗，所選檔案內含檢查評述檔，請將評述檔上傳至檢查評述專區。");
				//
				CmisObject co = null;
				try{
					co = alfSessionAdmin.getObjectByPath(fdDest.getPath() + "/" + sourceFileName );
				}catch(Exception _ex){
				}
				if(co == null){
					vecNew.add(paName);
				}else{
					//if(this.underToDoListIgnoreStage(collec.examNo, sourceFileName, conn)){
					//	throw new Exception("檔案：" + sourceFileName + " 尚在流程處理中，無法更新。");
					//}else{
						hUpdate.put(paName, co);
					//}
				}
				//throw new Exception("檔案：" + sourceFileName + " 已存在，請以「更新」動作異動文件。");
			}
			//記錄上傳成功的檔名
			Vector<String> vecUploaded = new Vector<String>(); 
		    //上傳新檔
			for(String paName: vecNew){
				String sourceFileName = uploadRequest.getFileName(paName);
				File file = uploadRequest.getFile(paName);
				InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
				//FileInputStream fis = new FileInputStream(file);
				MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		        
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put(PropertyIds.NAME, sourceFileName);
				props.put("cm:author", ErpUtil.getCurrUserFullName(themeDisplay));
				props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
				
				ContentStream cs = new ContentStreamImpl(sourceFileName, null, mimeTypesMap.getContentType(file), is);
				// create a major version
				Document doc = fdDest.createDocument(props, cs, VersioningState.MAJOR);
				//
				is.close();
				//Log
				ErpUtil.logUserAction("上傳新檔", ErpUtil.getFileFullName(doc.getId(), alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				ccNew++;
				//
				vecUploaded.add(sourceFileName);
			}
			//更新檔案
			if(!dupOption.equalsIgnoreCase("skip")){
				Iterator<String> it = hUpdate.keySet().iterator();
				while(it.hasNext()){
					String paName = it.next();
					String sourceFileName = uploadRequest.getFileName(paName);
					File file = uploadRequest.getFile(paName);
					InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
					//FileInputStream fis = new FileInputStream(file);
					MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	
					Map<String, String> props = new HashMap<String, String>();
					props.put("it:updateMemo", "上傳更新");
					props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
					if (!file.getName().toLowerCase().endsWith("json"))
						props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
					//
					ContentStream cs = new ContentStreamImpl(sourceFileName, null, mimeTypesMap.getContentType(file), is);
					AlfrescoDocument alfDoc = (AlfrescoDocument)hUpdate.get(paName);
				    if(!alfDoc.isVersionSeriesCheckedOut()){		//內容更新
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, "上傳更新");
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
						//Log
						ErpUtil.logUserAction("更新檔案", ErpUtil.getFileFullName(alfDoc.getId(), alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
						ccUpdate++;
				    }
					is.close();	
					//
					vecUploaded.add(sourceFileName);
				}
			}
			//
			String msg = "已成功上傳 " + ccNew + " 個新檔案";
			if(ccUpdate > 0){
				msg += "；以及更新 " + ccUpdate + " 個檔案";
			}
			msg += "。";
			actionRequest.setAttribute("successMsg", msg);
			SessionMessages.add(actionRequest, "success");
			//是否通知領隊
			if(!collec.isLeader && vecUploaded.size() > 0){
				//找出本檢查證專屬的流程設定
				String sql = "SELECT * FROM stage_rules WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, collec.examNo);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					//int formGenedMail2Assistant = rs.getInt("form_gened_mail2_assistant");		//檢查行前產出工作分配表時是否email給助檢人員
					//int inExamMail2Leader = rs.getInt("in_exam_mail2_leader");				//檢查期間有助檢文件匯入時是否通知領隊
					int examEndMail2Leader = rs.getInt("exam_end_mail2_leader");			//檢查結束助檢文件匯入時是否email給領隊
					//int examEndEnableWorkFlow = rs.getInt("exam_end_enable_workflow");		//檢查結束助檢文件匯入時是否啟動流程
					//int examEndWorkFlowType = rs.getInt("exam_end_workflow_type");			//檢查結束助檢文件匯入時之流程種類
					int preMeetEnableWorkFlow = rs.getInt("pre_meet_enable_workflow");		//檢討會前文件匯入時是否啟動流程
					int endMeetEnableWorkFlow = rs.getInt("end_meet_enable_workflow");		//檢討會後文件匯入時是否啟動流程
					//int evalMail2Leader = rs.getInt("eval_mail2_leader");					//評等審議助檢文件匯入時是否email給領隊
					//int evalKeyUserMail2Leader = rs.getInt("eval_keyuser_mail2_leader");		//評等審議主評文件匯入時是否email給領隊
					int evalEnableWorkFlow = rs.getInt("eval_enable_workflow");				//評等審議助檢文件匯入時是否啟動流程
					//int evalWorkFlowType = rs.getInt("eval_workflow_type");					//評等審議助檢文件匯入時之流程種類
					//int postEnableWorkFlow = rs.getInt("post_enable_workflow");
					//
					if((collec.stageName.contains("結束") && examEndMail2Leader == 1) ||
							(collec.stageName.contains("會前") && preMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("會後") && endMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("評等") && evalEnableWorkFlow == 1)){
						sql = "SELECT leader_name FROM exams WHERE exam_no = ?";
						ps = conn.prepareStatement(sql);
						ps.setString(1, collec.examNo);
						rs = ps.executeQuery();
						if(rs.next()){
							String toAddress = ErpUtil.getUserMailAddress(themeDisplay, rs.getString(1));
							if(!toAddress.equals("")){
								msg = "報告編號：" + collec.reportNo + "、檢查證號：" + collec.examNo + "之" + collec.stageName + "階段，助檢已上傳以下檔案：" + "\n";
								for(String fileName: vecUploaded){
									msg += fileName + "\n";
								}
								//
								ErpUtil.sendMail(toAddress, "檔案上傳通知", msg);
							}
						}
						rs.close();
					}
				}
				rs.close();
			}
			//
			conn.close();
	     }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/jsp/stagefiles/uploadFile.jsp");
	     }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	     }
	}
	
	//檔案上傳
	@SuppressWarnings("resource")
	public void uploadCommentFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
	        ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        //
	        Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        String stageFolderId = collec.stageFolderId;
	        //
	        UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        Vector<String> vecPaName = new Vector<String>();
	        for(int i=0; i < 10; i++){
	        	String nn = "fileName" + i;
	        	if(uploadRequest.getSize(nn) > 0 && !vecPaName.contains(nn)){
	        		vecPaName.add(nn);
	        	}
	        }
	        if (vecPaName.size() ==0)  throw new Exception("請指定所要上傳的檔案。");
	        //
	        String dupOption = "skip";			//檔名重複者
	        if(ParamUtil.getString(uploadRequest, "dupOption") != null){
	        	dupOption = ParamUtil.getString(uploadRequest, "dupOption");
	        }
		    //
		    conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdDestPa = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId); 
			AlfrescoFolder fdDestComment = null;
			for(CmisObject co: fdDestPa.getChildren()) {
				if(co instanceof org.apache.chemistry.opencmis.client.api.Folder) {
					if(co.getName().equals("檢查評述")) {
						fdDestComment = (AlfrescoFolder)co;
						break;
					}
				}
			}
			if(fdDestComment == null) {			//建立檔案夾
				Map<String, String>  props = new HashMap<String,String>();
				props.put(PropertyIds.NAME, "檢查評述");
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
				ObjectId oid = alfSessionAdmin.createFolder(props, fdDestPa);
				fdDestComment = (AlfrescoFolder)alfSessionAdmin.getObject(oid);
			}
			//
			Vector<String> vecNew = new Vector<String>();
			HashMap<String, CmisObject> hUpdate = new HashMap<String, CmisObject>();
			int ccNew = 0;
			int ccUpdate = 0;
			//
			String keyHeader = collec.reportNo + "_檢查評述";
			if(collec.examNature.contains("評等")){
				keyHeader = collec.reportNo + "_檢查評述_評等用";
			}
			for(String paName: vecPaName){
				String sourceFileName = uploadRequest.getFileName(paName);
				if(!sourceFileName.startsWith(keyHeader)) throw new Exception("請以「" + keyHeader + "」開頭之檔案上傳。");
				//if(!collec.isLeader && !sourceFileName.contains(ErpUtil.getCurrUserFullName(themeDisplay)))
				if(!sourceFileName.contains(ErpUtil.getCurrUserFullName(themeDisplay)))
					throw new Exception("上傳檔案:" + sourceFileName + " 未包含您的名字(" + ErpUtil.getCurrUserFullName(themeDisplay) + ")。");
				//
				//if(!sourceFileName.contains(collec.userFullName)) throw new Exception("檢查評述檔案請加上您的姓名「" + "」。");
				//
				CmisObject co = null;
				try{
					co = alfSessionAdmin.getObjectByPath(fdDestComment.getPath() + "/" + sourceFileName );
				}catch(Exception _ex){
				}
				if(co == null){
					vecNew.add(paName);
				}else{
					hUpdate.put(paName, co);
				}
			}
			//記錄上傳成功的檔名
			Vector<String> vecUploaded = new Vector<String>(); 
		    //上傳新檔
			for(String paName: vecNew){
				String sourceFileName = uploadRequest.getFileName(paName);
				File file = uploadRequest.getFile(paName);
				InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
				//FileInputStream fis = new FileInputStream(file);
				MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		        
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put(PropertyIds.NAME, sourceFileName);
				props.put("cm:author", ErpUtil.getCurrUserFullName(themeDisplay));
				props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
				
				ContentStream cs = new ContentStreamImpl(sourceFileName, null, mimeTypesMap.getContentType(file), is);
				// create a major version
				Document doc = fdDestComment.createDocument(props, cs, VersioningState.MAJOR);
				//
				is.close();
				//Log
				ErpUtil.logUserAction("上傳新檔", ErpUtil.getFileFullName(doc.getId(), alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				ccNew++;
				//
				vecUploaded.add(sourceFileName);
			}
			//更新檔案
			if(!dupOption.equalsIgnoreCase("skip")){
				Iterator<String> it = hUpdate.keySet().iterator();
				while(it.hasNext()){
					String paName = it.next();
					String sourceFileName = uploadRequest.getFileName(paName);
					File file = uploadRequest.getFile(paName);
					InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream(paName));
					//FileInputStream fis = new FileInputStream(file);
					MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	
					Map<String, String> props = new HashMap<String, String>();
					props.put("it:updateMemo", "上傳更新");
					props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
					props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
					//
					ContentStream cs = new ContentStreamImpl(sourceFileName, null, mimeTypesMap.getContentType(file), is);
					AlfrescoDocument alfDoc = (AlfrescoDocument)hUpdate.get(paName);
				    if(!alfDoc.isVersionSeriesCheckedOut()){		//內容更新
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, "上傳更新");
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
						//Log
						ErpUtil.logUserAction("更新檔案", ErpUtil.getFileFullName(alfDoc.getId(), alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
						ccUpdate++;
				    }
					is.close();	
					//
					vecUploaded.add(sourceFileName);
				}
			}
			//
			String msg = "已成功上傳 " + ccNew + " 個新檔案";
			if(ccUpdate > 0){
				msg += "；以及更新 " + ccUpdate + " 個檔案";
			}
			msg += "。";
			actionRequest.setAttribute("successMsg", msg);
			SessionMessages.add(actionRequest, "success");
			//是否通知領隊
			if(!collec.isLeader && vecUploaded.size() > 0){
				//找出本檢查證專屬的流程設定
				String sql = "SELECT * FROM stage_rules WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, collec.examNo);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					//int formGenedMail2Assistant = rs.getInt("form_gened_mail2_assistant");		//檢查行前產出工作分配表時是否email給助檢人員
					//int inExamMail2Leader = rs.getInt("in_exam_mail2_leader");				//檢查期間有助檢文件匯入時是否通知領隊
					int examEndMail2Leader = rs.getInt("exam_end_mail2_leader");			//檢查結束助檢文件匯入時是否email給領隊
					//int examEndEnableWorkFlow = rs.getInt("exam_end_enable_workflow");		//檢查結束助檢文件匯入時是否啟動流程
					//int examEndWorkFlowType = rs.getInt("exam_end_workflow_type");			//檢查結束助檢文件匯入時之流程種類
					int preMeetEnableWorkFlow = rs.getInt("pre_meet_enable_workflow");		//檢討會前文件匯入時是否啟動流程
					int endMeetEnableWorkFlow = rs.getInt("end_meet_enable_workflow");		//檢討會後文件匯入時是否啟動流程
					//int evalMail2Leader = rs.getInt("eval_mail2_leader");					//評等審議助檢文件匯入時是否email給領隊
					//int evalKeyUserMail2Leader = rs.getInt("eval_keyuser_mail2_leader");		//評等審議主評文件匯入時是否email給領隊
					int evalEnableWorkFlow = rs.getInt("eval_enable_workflow");				//評等審議助檢文件匯入時是否啟動流程
					//int evalWorkFlowType = rs.getInt("eval_workflow_type");					//評等審議助檢文件匯入時之流程種類
					//int postEnableWorkFlow = rs.getInt("post_enable_workflow");
					//
					if((collec.stageName.contains("結束") && examEndMail2Leader == 1) ||
							(collec.stageName.contains("會前") && preMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("會後") && endMeetEnableWorkFlow == 1) ||
							(collec.stageName.contains("評等") && evalEnableWorkFlow == 1)){
						sql = "SELECT leader_name FROM exams WHERE exam_no = ?";
						ps = conn.prepareStatement(sql);
						ps.setString(1, collec.examNo);
						rs = ps.executeQuery();
						if(rs.next()){
							String toAddress = ErpUtil.getUserMailAddress(themeDisplay, rs.getString(1));
							if(!toAddress.equals("")){
								msg = "報告編號：" + collec.reportNo + "、檢查證號：" + collec.examNo + "之" + collec.stageName + "階段，助檢已上傳以下檔案：" + "\n";
								for(String fileName: vecUploaded){
									msg += fileName + "\n";
								}
								//
								ErpUtil.sendMail(toAddress, "檔案上傳通知", msg);
							}
						}
						rs.close();
					}
				}
				rs.close();
			}
			//
			conn.close();
	     }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/jsp/stagefiles/uploadCommentFile.jsp");
	     }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	     }
	}

	//啟動流程
	public void startWorkflow(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	//
        	String fileId = actionRequest.getParameter("fileId");
        	String fileName = actionRequest.getParameter("fileName");
        	String stageFolderId = actionRequest.getParameter("stageFolderId");
        	String workflowType = actionRequest.getParameter("workflowType");
        	String taskComment = actionRequest.getParameter("taskComment");
        	String assignee = actionRequest.getParameter("assignee");
        	//
        	if(workflowType.trim().equals("")) throw new Exception("請指定流程種類。");
        	if(assignee.trim().equals("")) throw new Exception("請指定任務受任人。");
        	//
        	conn = ds.getConnection();
        	//
        	String sql = "SELECT 1 FROM todo_list WHERE file_id=? AND (done IS NULL OR done = 0)";
        	PreparedStatement ps = conn.prepareStatement(sql);
        	ps.setString(1, fileId);
        	ResultSet rs = ps.executeQuery();
        	if(rs.next()){
        		throw new Exception("檔案：" + fileName + " 已在流程處理中。");
        	}
        	//
        	String deadLine = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearDead"), actionRequest.getParameter("monDead"), actionRequest.getParameter("dayDead"));
        	//檢查受任人是否存在
        	if(!ErpUtil.isExamUser(assignee, themeDisplay)) throw new Exception("受任人不在檢查人員清單中。");
	        //
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//Log
			ErpUtil.logUserAction("啟動階段檔案流程", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
			//如果啟動的是審核流程
			if(workflowType.equals(ErpUtil.TASK_REVIEW) || workflowType.equals(ErpUtil.TASK_FILL_REPORT) || workflowType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				String updateMemo = "人工啟動";
				Map<String, String> props = new HashMap<String, String>();
				props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				props.put("it:updateMemo", updateMemo);
				//
				InputStream is = alfDoc.getContentStream().getStream();
				ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}
			//
			ExamFact ef = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//
			Task t = new Task();
			t.taskType = workflowType;
			t.taskComment = taskComment;
			t.assignee = assignee;
			t.deadLine = deadLine;
			t.issuer = ErpUtil.getCurrUserFullName(themeDisplay);
			t.fileId = alfDoc.getId();
			t.fileName = alfDoc.getName();
			t.versionNo = alfDoc.getVersionLabel();
			t.reportNo = ef.reportNo;
			t.examNo = ef.examNo;
			t.stageFolderId = fdStage.getId();
			t.stageName = fdStage.getName();
			t.bankName = ef.bankName;
			ErpUtil.addTask(t, ef.formGenedMail2Assistant, conn, themeDisplay);
			//
			conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//
	        	String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/startWorkflow.jsp");
	    }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	    }		 
	}
	
	//更新檢查行政工作分配資料
	@SuppressWarnings("unused")
	private void updateExamAdminAlloc(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	//
        	String fileId = actionRequest.getParameter("fileId");
        	String fileName = actionRequest.getParameter("fileName");
        	String stageFolderId = actionRequest.getParameter("stageFolderId");
        	//
        	conn = ds.getConnection();
	        //
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//Log
			ErpUtil.logUserAction("更新檢查行政工作分配資料", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			ExamFact ef = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//
			String screenNameLeader = ErpUtil.getUserScreenName(themeDisplay, ef.leaderName);
			if(screenNameLeader == null || screenNameLeader.equals("")){
				throw new Exception("查無領隊「" + ef.leaderName + "」之基本資料！");
			}
			//呼叫 SQL Server
			Class.forName(PropsUtil.get("db2.driver"));	//內容是 SQL Server
			Connection sqlConn  = DriverManager.getConnection(PropsUtil.get("db2.url"),
	        											PropsUtil.get("db2.user"),
	        											PropsUtil.get("db2.password"));
			CallableStatement cstmt = sqlConn.prepareCall("EXEC [spITEZ01] ?,?,?,?");
			cstmt.setString (1, ef.reportNo);
			cstmt.setString(2, ef.bankNameShort);
			cstmt.setString(3,  screenNameLeader.toLowerCase());
			cstmt.setString(4, fileId);
			//
			boolean b = cstmt.execute();
			sqlConn.close();
			//
			conn.close();
	    }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//
	        	String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/updateExamAdminAlloc.jsp");
	    }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	    }		 
	}
	
	//檔案作廢
	public void deactivateStageFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	//
        	String fileId = actionRequest.getParameter("fileId");
	        //
        	conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			String newFileName = "_del#" + alfDoc.getName();
			//
			AlfrescoDocument alfDocDeactive = null; 
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			String stageFolderId = collec.stageFolderId;
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			AlfrescoFolder fdExam =  (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			ErpUtil.logUserAction("作廢階段檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
	        	if(ErpUtil.underToDoList(fdExam.getName(), fdStage.getName(), alfDoc.getName(), conn)) throw new Exception(alfDoc.getName() + " 已在流程處理中，無法變更。");
	        	//
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getName().equals(newFileName)){
					alfDocDeactive = (AlfrescoDocument)co;
					break;
				}
			}		
			//更名成刪除
			if(alfDocDeactive == null){
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author,P:it:deletedBy");
				props.put(PropertyIds.NAME, newFileName);
				props.put("it:deletedBy", userFullName);
				props.put("it:modifier", userFullName);
				props.put("it:docStatus", "作廢");
				alfDoc.updateProperties(props);
			}else{
				alfDocDeactive.setContentStream(alfDoc.getContentStream(), true);
				alfDoc.delete();
			}
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        		
	        	}
	        }
	}

	//恢復刪除
	public void activateStageFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	String fileId = actionRequest.getParameter("fileId");
	        //
        	conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoDocument alfDocDeactive = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			//恢復的檔名
			String fileName = alfDocDeactive.getName().substring(5);	//拿掉 _del#
			//
			ErpUtil.logUserAction("恢復階段檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoDocument alfDocActive = null; 
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			String stageFolderId = collec.stageFolderId;
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getName().equals(fileName)){
					alfDocActive = (AlfrescoDocument)co;
					break;
				}
			}
			//
			if(alfDocActive == null){
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author,P:it:deletedBy");
				props.put(PropertyIds.NAME, fileName);		
				props.put("it:deletedBy", "");
				props.put("it:modifier", userFullName);
				props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
				props.put("it:docStatus", "");
				alfDocDeactive.updateProperties(props);
			}else{
				alfDocActive.setContentStream(alfDocDeactive.getContentStream(), true);
				alfDocDeactive.delete();
			}
	    }catch(Exception ex){
        	//ex.printStackTrace();
        	String errMsg = ErpUtil.getItezErrorCode(ex);
        	if(errMsg.equals("")){
        		errMsg = ex.getMessage();
        	}
        	actionRequest.setAttribute("errMsg", errMsg);
        	SessionErrors.add(actionRequest, "error");
        	//
        	actionRresponse.setRenderParameter("jspPage", "/jsp/stagefiles/uploadFile.jsp");
        }finally{
        	try{
        		if(conn != null) conn.close();
        	}catch(Exception _ex){
        	}
        }
	}
	
	//檔案作廢
	public void deleteStageFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        String fileId = actionRequest.getParameter("fileId");
	        //
	        conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//Log
			ErpUtil.logUserAction("刪除階段檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			alfDoc.delete();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/jsp/stagefiles/uploadFile.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	//產出領隊版檢查重點彙整表
	public void genPointLeader(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
        try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	conn = ds.getConnection();
        	//
        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
        	//
        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
        	//Log
        	String _path = fdExam.getName() + "/" + fdStage.getName();
        	ErpUtil.logUserAction("產出領隊版檢查重點彙整表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
        	//找出範本檔
        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
        	//找出檢查結束階段助檢版報告表
			AlfrescoFolder fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
        	while(it.hasNext()){
        		CmisObject co = it.next();
        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
        			if(co.getName().contains("結束")){
        				fdEndExam = (AlfrescoFolder)co;
        				break;
        			}
        		}
        	}
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
	        	
	        //找出要合併的檔
			int countUnApproved = 0;
			String unApprovedNames = "";
			HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("檢查重點_")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				!co.getName().contains("彙整表") &&
	        				!co.getName().startsWith("_del#") && 
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
	        			if(doc.getPropertyValue("it:docStatus") != null){
	        				if(doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_DRAFT) || doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_APPROVING)){
	        					countUnApproved++;
	        					if(!unApprovedNames.equals("")) unApprovedNames += ",";
	        					unApprovedNames += doc.getName();
	        				}else{
	        					hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        				}
	        			}else{
	        				hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        			}
	        		}
	        	}
	        	if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + unApprovedNames + " 等 " + countUnApproved + " 個報告檔案待審核。");
	        	
	        	if(hmIS.size() == 0) throw new Exception("檢查結束階段找不到以「檢查重點_」開頭的檔案。");
	        		        	
	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
	        	String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查重點彙整表.doc";
	        	//
	        	FileNameData fnd = new FileNameData();
	        	fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
	        	
			ParseDoc pd = new ParseDoc();
			ByteArrayOutputStream baos = pd.Merge("檢查重點",
											  tempCollect.fPointDocLeader.getContentStream().getStream(),
											  hmIS,
											  ei.bankName,
											  ei.reportNo,
											  tempCollect.alfAllocateJson.getContentStream().getStream(),
											  fnd); 
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			InputStream is = new ByteArrayInputStream(baos.toByteArray());
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocLeader.getName()), is);
			
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put("cm:author", userFullName);
			props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
			if(htNames.containsKey(fileName)){
				String updateMemo = "重新產出";
				props.put("it:updateMemo", updateMemo);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				
				AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
				//alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//產出領隊版檢查重點彙整表草稿
	public void genPointLeaderDraft(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//草稿區
	        	String folderIdDraft = actionRequest.getParameter("folderId");
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	AlfrescoFolder fdDraft = (AlfrescoFolder)alfSessionAdmin.getObject(folderIdDraft);
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出領隊版檢查重點彙整表草稿", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	//找出檢查結束階段助檢版報告表
			AlfrescoFolder fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        				break;
	        			}
	        		}
	        	}
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
	        	
	        	//找出要合併的檔
			//int countUnApproved = 0;
	        	HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("檢查重點_")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				!co.getName().contains("彙整表") &&
	        				!co.getName().startsWith("_del#") && 
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
        				hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        		}
	        	}
	        	//if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + countUnApproved + " 個報告檔案待審核。");
	        	
	        	if(hmIS.size() == 0) throw new Exception("檢查結束階段找不到以「檢查重點_」開頭的檔案。");
	        		        	
	        	//本階段所有檔名(草稿區)
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdDraft);
	        	//
	        	String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查重點彙整表草稿.doc";
	        	//
	        	FileNameData fnd = new FileNameData();
	        	fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
	        	
			ParseDoc pd = new ParseDoc();
			ByteArrayOutputStream baos = pd.Merge("檢查重點",
											  tempCollect.fPointDocLeader.getContentStream().getStream(),
											  hmIS,
											  ei.bankName,
											  ei.reportNo,
											  tempCollect.alfAllocateJson.getContentStream().getStream(),
											  fnd); 
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			InputStream is = new ByteArrayInputStream(baos.toByteArray());
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocLeader.getName()), is);
			
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put("cm:author", userFullName);
			props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
			if(htNames.containsKey(fileName)){
				String updateMemo = "重新產出";
				props.put("it:updateMemo", updateMemo);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				
				AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
				//alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdDraft.createDocument(props, cs, VersioningState.MAJOR);
			}
			//草稿區檔案存入Session
			Vector<StageFile> vecDraft = new Vector<StageFile>();
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					AlfrescoDocument doc = (AlfrescoDocument)co;
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}else if(doc.getPropertyValue("cm:description") != null) {
						if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
							sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
						}
					}
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					//if(!ef.leaderName.contains(sf.author) && ef.staffs.contains(sf.author)) sf.author = ef.leaderName;
					
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdDraft.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
									
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					//
					vecDraft.add(sf);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecDraft", vecDraft);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/draftArea.jsp");
			actionResponse.setRenderParameter("folderId", fdDraft.getId());			
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}	

	//產出領隊版必查項目彙整表
	public void genEssenLeader(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出領隊版必查項目彙整表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	//找出檢查結束階段助檢版報告表
			AlfrescoFolder fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        				break;
	        			}
	        		}
	        	}
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");

	        	//找出要合併的檔
			int countUnApproved = 0;
			String unApprovedNames = "";
	        	HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("必查項目_")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				!co.getName().contains("彙整表") &&
	        				!co.getName().startsWith("_del#") && 
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
	        			if(doc.getPropertyValue("it:docStatus") != null){
	        				if(doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_DRAFT) || doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_APPROVING)){
	        					countUnApproved++;
	        					if(!unApprovedNames.equals("")) unApprovedNames += ",";
	        					unApprovedNames += doc.getName();
	        				}else{
	        					hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        				}
	        			}else{
	        				hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        			}
	        		}
	        	}
	        	if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + unApprovedNames + " 等 " + countUnApproved + " 個必查項目報告表待審核。");
	        	if(hmIS.size() == 0) throw new Exception("找不到以「必查項目_」開頭的檔案。");

	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
	        	String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "必查項目彙整表.doc";
	        	
	        	FileNameData fnd = new FileNameData();
	        	fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			  
			ParseDoc pd = new ParseDoc();
			ByteArrayOutputStream baos = pd.Merge("必查項目", 
					 						tempCollect.fEssenDocLeader.getContentStream().getStream(),
					 						hmIS,
					 						ei.bankName,
					 						ei.reportNo,
					 						tempCollect.alfAllocateJson.getContentStream().getStream(),
					 						fnd);
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fEssenDocLeader.getName()), new ByteArrayInputStream(baos.toByteArray()));
			
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put("cm:author", userFullName);
			props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
			if(htNames.containsKey(fileName)){
				String updateMemo = "重新產出";
				props.put("it:updateMemo", updateMemo);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				
				AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	//產出領隊版必查項目彙整表(草稿)
	public void genEssenLeaderDraft(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	String folderIdDraft = actionRequest.getParameter("folderId");
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	AlfrescoFolder fdDraft = (AlfrescoFolder)alfSessionAdmin.getObject(folderIdDraft);
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出領隊版必查項目彙整表草稿", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	//找出檢查結束階段助檢版報告表
			AlfrescoFolder fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        				break;
	        			}
	        		}
	        	}
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");

	        	//找出要合併的檔
			//int countUnApproved = 0;
	        HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("必查項目_")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				!co.getName().contains("彙整表") &&
	        				!co.getName().startsWith("_del#") && 
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
        				hmIS.put(doc.getName(), doc.getContentStream().getStream());
	        		}
	        	}
	        	//if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + countUnApproved + " 個必查項目報告表待審核。");
	        	if(hmIS.size() == 0) throw new Exception("找不到以「必查項目_」開頭的檔案。");

	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdDraft);
	        	//
	        	String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "必查項目彙整表草稿.doc";
	        	
	        	FileNameData fnd = new FileNameData();
	        	fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			  
			ParseDoc pd = new ParseDoc();
			ByteArrayOutputStream baos = pd.Merge("必查項目", 
					 						tempCollect.fEssenDocLeader.getContentStream().getStream(),
					 						hmIS,
					 						ei.bankName,
					 						ei.reportNo,
					 						tempCollect.alfAllocateJson.getContentStream().getStream(),
					 						fnd);
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fEssenDocLeader.getName()), new ByteArrayInputStream(baos.toByteArray()));
			
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put("cm:author", userFullName);
			props.put("cm:description", "by " + ErpUtil.getCurrUserFullName(themeDisplay));
			if(htNames.containsKey(fileName)){
				String updateMemo = "重新產出";
				props.put("it:updateMemo", updateMemo);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				
				AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdDraft.createDocument(props, cs, VersioningState.MAJOR);
			}
			//草稿區檔案存入Session
			Vector<StageFile> vecDraft = new Vector<StageFile>();
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					AlfrescoDocument doc = (AlfrescoDocument)co;
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}else if(doc.getPropertyValue("cm:description") != null) {
						if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
							sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
						}
					}
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					//if(!ef.leaderName.contains(sf.author) && ef.staffs.contains(sf.author)) sf.author = ef.leaderName;
					
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdDraft.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
									
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					//
					vecDraft.add(sf);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecDraft", vecDraft);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/draftArea.jsp");
			actionResponse.setRenderParameter("folderId", fdDraft.getId());			
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}	
	
	//產出重要查核項目彙整表(無關連欄位, 輸出或呈核用)
	public void genLeaderSummaryTrimCol(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	//Log
	        	String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出呈核用查核項目彙整表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出已產出的彙整表
	        	HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>();
	        	Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if((co.getName().contains("必查項目彙整表") ||  co.getName().contains("檢查重點彙整表")) &&
	        				!co.getName().contains("列印用") && 
	        				!co.getName().startsWith("_del#") &&
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
        				hmIS.put(co.getName(), doc.getContentStream().getStream());
	        		}
	        	}
	        	if(hmIS.size() == 0) throw new Exception("找不到以重要查核項目彙整表。");

	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
	        	HashMap<String, ByteArrayOutputStream> hmBOS = new HashMap<String, ByteArrayOutputStream>();
			ParseDoc pd = new ParseDoc();
			hmBOS = pd.TransformWordToPrint(hmIS, "1");
			
			Iterator<String> it2 = hmBOS.keySet().iterator();
			while(it2.hasNext()){
				String fileName = it2.next();
				//
				InputStream is = new ByteArrayInputStream(hmBOS.get(fileName).toByteArray());
				ContentStream cs = new ContentStreamImpl(fileName,
													null,
													"application/msword",
													is);
				//
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put("cm:author", userFullName);
				if(htNames.containsKey(fileName)){
					String updateMemo = "重新產出";
					props.put("it:updateMemo", updateMemo);
					
					AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					pwc.checkIn(false, props, cs, updateMemo);
				}else{
					props.put(PropertyIds.NAME, fileName);
					fdStage.createDocument(props, cs, VersioningState.MAJOR);
				}
			}
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String actionType = resRequest.getParameter("actionType");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//
			String fileId = "";
			AlfrescoDocument doc = null;
			String fileName = "";
			if(resRequest.getParameter("fileId") != null){
				fileId = resRequest.getParameter("fileId");
				doc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				fileName = doc.getName();
			}
			//
			OutputStream out = resResponse.getPortletOutputStream();
			//下載時
			if(actionType.equalsIgnoreCase("download")){
				//Log
				ErpUtil.logUserAction("下載階段檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
				//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
				//
				//if(fileName.startsWith("_del#")) fileName = fileName.substring(5);
				//AlfrescoDocument alfDoc = (AlfrescoDocument)co;
				//
				//System.out.println("Mine type: " + doc.getContentStreamMimeType());
				//作廢檔案下載時先濾掉 _del#
				fileName = fileName.replace("_del#", "");
				//	
				resResponse.setContentType(doc.getContentStreamMimeType());
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
				//
				java.io.InputStream is = doc.getContentStream().getStream();
				if(is != null){
					byte[] buffer = new byte[4096];
					int n;
					while ((n = is.read(buffer)) > 0) {
					    out.write(buffer, 0, n);
					}
					out.flush();
					out.close();
					is.close();
				}
			}else if(actionType.equalsIgnoreCase("edit")){
				//Log
				ErpUtil.logUserAction("編輯工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				 //Cipher cipher = Cipher.getInstance("DES");
				 //cipher.init(Cipher.ENCRYPT_MODE, "#A29429820");
				//
				String alfTicket = ErpUtil.getAlfrecoTicket(PropsUtil.get("alfresco.admin.user"), PropsUtil.get("alfresco.admin.password"));
				String url = this.getDocumentURL(doc, alfSessionAdmin);
				int idx = url.lastIndexOf("?");
				url = PropsUtil.get("alfresco.host.lan") + "/alfresco/api/-default-/public/cmis/versions/1.0/atom/content/" + URLEncoder.encode(doc.getName(), "UTF-8") + url.substring(idx);
				//
				String home = resRequest.getScheme() + "://" + resRequest.getServerName();
				String uri = home + "/" + resRequest.getContextPath();
				String jarName = "/webstart/Allocater.jar";
				String className = "com.itez.AllocateMain";
				//
				resResponse.setContentType("application/x-java-jnlp-file");
				//
				String s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
				s +="<jnlp spec=\"1.0+\" codebase=\"" + uri + "\" href=\"\">";
				s += "<information>";
				s += "<title>檢查報告處理系統工作分配編輯程式</title>";
				s += "<vendor>商頁網股份有限公司</vendor>";
				s += "<homepage href=\"" + home + "\" />";
				s += "<description></description>";
				s += "</information>";
				s += "<security>";
				s += "<all-permissions/>";
				s += "</security>";
				s += "<resources>";
				s += "<j2se version=\"1.7+\" />";
				s += "<jar href=\"" + jarName + "\" />";
				s += "</resources>";
				s += "<application-desc main-class=\"" + className + "\" >";
				s += "<argument>" +  PropsUtil.get("alfresco.host.lan") + "</argument>";
				s += "<argument>" +  PropsUtil.get("alfresco.admin.user") + "</argument>";
				s += "<argument>" +  PropsUtil.get("alfresco.admin.password") + "</argument>";
				s += "<argument>" +  fileId + "</argument>";
				s += "<argument>" + (url + "&alf_ticket=" + alfTicket) + "</argument>";
				s += "<argument>" + userFullName + "</argument>";
				s += "<argument>" + "E" + "</argument>";		//T:範本區 E:檢查報告處理區 
				s += "</application-desc>";
				s += "</jnlp>";
				//
				out.write(s.getBytes());
				out.flush();
				out.close();
			}else if(actionType.equalsIgnoreCase("batchDownload")){
				String zipName = resRequest.getParameter("zipName");
				//
				ZipArchiveOutputStream zos = new ZipArchiveOutputStream(new BufferedOutputStream(out));
				//zos.setEncoding("UTF-8");
				zos.setEncoding("MS950");
				zos.setCreateUnicodeExtraFields(ZipArchiveOutputStream.UnicodeExtraFieldPolicy.ALWAYS);				
				//
				//ZipOutputStream zos = new ZipOutputStream(out);
				resResponse.setContentType("application/zip");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (zipName, "utf-8") + "\"");				
				//
				HashMap<String, String> htBatchDoc = (HashMap<String, String>)resRequest.getPortletSession(true).getAttribute("htBatchDoc");
				Iterator<String> it = htBatchDoc.keySet().iterator();
				while(it.hasNext()){
					fileId = it.next();
					fileName = htBatchDoc.get(fileId);
					doc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
					//
					ZipArchiveEntry entry = new ZipArchiveEntry(fileName);
				        entry.setSize(doc.getContentStreamLength());
				        zos.putArchiveEntry(entry);
				        zos.write(IOUtils.toByteArray(doc.getContentStream().getStream()));
				        zos.closeArchiveEntry();
				}
				zos.close();
				//
				out.flush();
				out.close();
			}else if(actionType.equalsIgnoreCase("exportAllocate")){
				//Log
				ErpUtil.logUserAction("匯出工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
				//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
				//
				//if(fileName.startsWith("_del#")) fileName = fileName.substring(5);
				//AlfrescoDocument alfDoc = (AlfrescoDocument)co;
				//
				//System.out.println("Mine type: " + doc.getContentStreamMimeType());
				//作廢檔案下載時先濾掉 _del#
				fileName = fileName.replace("json", "xls");
				//	
				resResponse.setContentType("application/vnd.ms-excel");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
				//
				ParseDoc pd = new ParseDoc();
				
				ByteArrayOutputStream baos = pd.allocateConvertExcel(doc.getContentStream().getStream());
				ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
				//POIFSFileSystem fs = new POIFSFileSystem(bais); 
				if(bais != null){
					byte[] buffer = new byte[4096];
					int n;
					while ((n = bais.read(buffer)) > 0) {
					    out.write(buffer, 0, n);
					}
					out.flush();
					out.close();
				}
				bais.close();
			}else if(actionType.equalsIgnoreCase("genEmptyExamComment")) {
				Collec collec = (Collec)resRequest.getPortletSession(true).getAttribute("collec");
				AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
				//取得檢查案結構資訊
				ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
				//查詢範本
				TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
				if(ei.examNature.contains("評等") && tempCollect.fExamComment2 == null) throw new Exception("文件樣板區找不到「檢查評述_評等用」樣板檔案。");
				if(!ei.examNature.contains("評等") && tempCollect.fExamComment == null) throw new Exception("文件樣板區找不到「檢查評述」樣板檔案。");
				//
				fileName = collec.reportNo + "_檢查評述_" + collec.userFullName + ".doc";
				if(ei.examNature.contains("評等")){
					fileName = collec.reportNo + "_檢查評述_評等用_" + collec.userFullName + ".doc";
				}
				MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
				resResponse.setContentType(fileTypeMap.getContentType(tempCollect.fExamComment.getName()));
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
				//
				java.io.InputStream is = null;
				if(ei.examNature.contains("評等")){
					is = tempCollect.fExamComment2.getContentStream().getStream();
				}else{
					is = tempCollect.fExamComment.getContentStream().getStream();
				}
				if(is != null){
					byte[] buffer = new byte[4096];
					int n;
					while ((n = is.read(buffer)) > 0) {
					    out.write(buffer, 0, n);
					}
					out.flush();
					out.close();
					is.close();
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//add entries to archive file...
	/*
	private void addFilesToCompression( ArchiveOutputStream taos, File file, String dir) throws IOException {
		// Create an entry for the file
		taos.putArchiveEntry(new ZipArchiveEntry(file, dir+"/"+file.getName()));
		if (file.isFile()) {
			// Add the file to the archive
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
			IOUtils.copy(bis, taos);
			taos.closeArchiveEntry();
			bis.close();
		} else if (file.isDirectory()) {
			// close the archive entry
			taos.closeArchiveEntry();
			// go through all the files in the directory and using recursion, add them to the archive
			for (File childFile : file.listFiles()) {
				addFilesToCompression(taos, childFile, file.getName());
			}
		}
	}
	*/
	
	/*
	private String encode(Cipher cipher, String text) throws Exception{
		
		return text;
	}
	*/
	
	//產出助檢版報告表
	public void genEmptyFormStaff(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = actionRequest.getParameter("fileId");
			String formName = actionRequest.getParameter("formName");
			
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//Log
			ErpUtil.logUserAction("產出助檢版報告表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			//if(fPointExcel == null) throw new Exception("檢查重點工作分配表找不到。");
			InputStream tempIS = null;
			if(formName.contains("檢查重點")){
				if(tempCollect.fPointDocAssistant == null) throw new Exception("檢查重點報告表樣板檔找不到。");
				//
				tempIS = tempCollect.fPointDocAssistant.getContentStream().getStream();
			}else if(formName.contains("必查項目")){
				if(tempCollect.fEssenDocAssistant == null) throw new Exception("必查項目報告表樣板檔找不到。");
				//
				tempIS = tempCollect.fEssenDocAssistant.getContentStream().getStream();
			}
			if(tempIS == null) throw new Exception(formName + " 之報告表樣板無法開啟。");
			//
			//AlfrescoDocument alfDocAllocate = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			AlfrescoDocument alfDocAllocate = this.getLateastVersion(alfSessionAdmin, fdStage, fileId);		//取得最新版本
			//email 附件
			Vector<InputStream> vecIS = new Vector<InputStream>(); 
			//助檢與分配到的業務項目對照表
			//HashMap<String, Vector<String>> hStaffBis = new HashMap<String, Vector<String>>();
			//Vector<String> vecStaff = new Vector<String>();
			//
			ParseDoc pd = new ParseDoc();
			//HashMap Key: 助檢姓名_業務名稱, value: Word 報表
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			HashMap<String, ByteArrayOutputStream> hMap = pd.ImportantCheckGeneralSplit(formName,
																			  alfDocAllocate.getContentStream().getStream(), 
																			  tempIS,
																			  ei.bankName,
																			  ei.reportNo,
																			  ei.examNo,
																			  fnd);
			//檢查所要產出的檔案是否存於待辦事項中
			Iterator<String> it = hMap.keySet().iterator();
			while(it.hasNext()){
				String fileName = it.next();
				//檢查是否在流程中
				String sql = "SELECT * FROM todo_list WHERE exam_no = ? " +
														" AND (done IS NULL OR done =0) " +
														" AND file_name =? ";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, ei.examNo);
				ps.setString(2, fileName);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					throw new Exception("本案檔名 " + fileName + " 已於待辦事項流程中，無法重新產出。");
				}
				rs.close();	
			}
			//開始寫檔
			it = hMap.keySet().iterator();
			while(it.hasNext()){
				String key = it.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				vecIS.add(is);
				//寫檔
				String fileName = key;
				String pureName = fileName.substring(0, fileName.lastIndexOf("."));
				String[] ss = pureName.split("_");
				String staffName = ss[ss.length - 1];
				//
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
				props.put("cm:author", userFullName);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
				ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
				//
				String updateMemo = "重新產出";
				AlfrescoDocument alfDoc2 = null;
				boolean isNewNode = false;
				if(!htNames.containsKey(fileName)){
					updateMemo = "";
					props.put(PropertyIds.NAME, fileName);
					alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					//
					isNewNode = true;
				}else{
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));	
				}
				//加入狀態
				props.put("it:updateMemo", updateMemo);
				props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
					
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				if(isNewNode){
					alfDoc2 = (AlfrescoDocument)alfDoc2.updateProperties(props);
				}else{
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}
				//---------------------------- 加入助檢待辦事項 -----------------------------------------------
				Task t = new Task();
				t.taskType = ErpUtil.TASK_FILL_REPORT;
				t.reportNo = ei.reportNo;
				t.examNo = ei.examNo;
				t.bankName = ei.bankName;
				t.stageFolderId = fdStage.getId();
				t.stageName = fdStage.getName();
				t.fileId = alfDoc2.getId();
				t.fileName = alfDoc2.getName();
				t.versionNo = alfDoc2.getVersionLabel();
				t.assignee = staffName;
				t.deadLine = ei.deadLineStaff;
				t.issuer = userFullName;
				//
				ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
			}
			//如果是本銀總行, 則找出主評人員存入資料庫
			if(ei.isRatingAssess && !ei.isCompuerAudit){
				String mainAssessers = "";
				TreeSet<String> tsMainAssesser = pd.GetMainList(alfDocAllocate.getContentStream().getStream());
				for(String s: tsMainAssesser){
					if(!mainAssessers.contains(s)){
						if(!mainAssessers.equals("")) mainAssessers += ";";
						mainAssessers += s;
					}
				}
				String sql = "UPDATE exams SET main_assessers = ? WHERE exam_no=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, mainAssessers);
				ps.setString(2, ei.examNo);
				ps.execute();
			}
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//取的工作分配表最新版本
	private AlfrescoDocument getLateastVersion(Session alfSessionAdmin, AlfrescoFolder fdStage, String fileId) throws Exception{
		AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getName().equals(alfDoc.getName())){
				alfDoc = (AlfrescoDocument)co;
				break;
			}
		}
		return alfDoc;
	}
	
	//刪除已產出助檢版報告表
	public void removeEmptyForm(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = actionRequest.getParameter("fileId");
			String formName = actionRequest.getParameter("formName");
			
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			ErpUtil.logUserAction("刪除已產出之助檢版報告表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			//
			AlfrescoDocument alfDocAllocate = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			ParseDoc pd = new ParseDoc();
			HashMap<String, ByteArrayOutputStream> hMap = null;
			//
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			//
			InputStream tempIS = null;
			if(formName.contains("檢查重點")){
				if(tempCollect.fPointDocAssistant == null) throw new Exception("檢查重點報告表樣板檔找不到。");
				//
				tempIS = tempCollect.fPointDocAssistant.getContentStream().getStream();
				hMap = pd.ImportantCheckGeneralSplit(formName,
												  alfDocAllocate.getContentStream().getStream(), 
												  tempIS,
												  ei.bankName,
												  ei.reportNo,
												  ei.examNo,
												  fnd);
			}else if(formName.contains("必查項目")){
				if(tempCollect.fEssenDocAssistant == null) throw new Exception("必查項目報告表樣板檔找不到。");
				//
				tempIS = tempCollect.fEssenDocAssistant.getContentStream().getStream();
				hMap = pd.ImportantCheckGeneralSplit(formName,
												  alfDocAllocate.getContentStream().getStream(), 
												  tempIS,
												  ei.bankName,
												  ei.reportNo,
												  ei.examNo,
												  fnd);
			}else if(formName.contains("保外組檢查評述")){
				if(tempCollect.htInsForeState.size() == 0) throw new Exception("找不到內部參考事項範本檔。");
				hMap = pd.AssingWord(tempCollect.htInsForeState,
									ei.bankName,
									tempCollect.alfAllocateJson.getContentStream().getStream(),
									fnd);
			}else if(formName.contains("證票組檢查評述")){
				if(tempCollect.fExamComment == null) throw new Exception("找不到檢查評述空白樣板檔。");
				//
				String pureName = tempCollect.fExamComment.getName().substring(0, tempCollect.fExamComment.getName().lastIndexOf("."));
				String[] arrStaff = ei.staffs.split(";");
				//存成 hash map
				hMap = new HashMap<String, ByteArrayOutputStream> ();
				for(String staffName: arrStaff){
					String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + pureName + "_" + staffName + ".doc";
					//
					hMap.put(fileName, null);
				}			
			}else if(formName.contains("資訊檢查重要查核")){
				if(tempCollect.fInfoExamImpChkStaffForm == null) throw new Exception("資訊作業重要查核項目事項表(助檢版)範本檔找不到。");
				//
				InformationColumnValue icv = new InformationColumnValue();
				icv.reportId = ei.reportNo;
				icv.checkType = ei.examType;
				icv.organization = ei.bankName;
				icv.leader = ei.leaderName;
				icv.base_date = ei.dateBase;
				icv.startDate = ei.dateStart;
				icv.endDate = ei.dateEnd;
				icv.reviewDate = ei.dateMeeting;
				icv.durationDays = String.valueOf(ei.examDays);
				//
				hMap = pd.InformationExam(tempCollect.fInfoExamImpChkStaffForm.getContentStream().getStream(),
										icv,
										alfDocAllocate.getContentStream().getStream(),
										fnd);
			}
			if(hMap == null) return;
			//
			Iterator<String> it = hMap.keySet().iterator();
			while(it.hasNext()){
				String fileName = it.next();
				//
				String _fileId = "";
				Iterator<CmisObject> it2 = fdStage.getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co = it2.next();
					if(co.getName().equals(fileName)){
						_fileId = co.getId();
						break;
					}
				}
				if(!_fileId.equals("")){
					int idx = _fileId.indexOf(";");
					String fileIdHead = _fileId.substring(0, idx);
					//
					System.out.println("Checking if file received with exam_no='" + ei.examNo + "' and file_id LIKE '" + fileId + "%' ");

					//檢查是否簽收
					String sql = "SELECT * FROM todo_list WHERE exam_no = ? " +
															" AND received=1 " +
															" AND file_id LIKE '" + fileIdHead + "%' ";
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.setString(1, ei.examNo);
					//ps.setString(2, _fileId);
					ResultSet rs = ps.executeQuery();
					if(rs.next()) throw new Exception("本案檔名 " + fileName + " 已被簽收，無法刪除。");
					//
					rs.close();
				}
			}
			//開始刪除-待辦事項
			Vector<String> vecDeleName = new Vector<String>();
			
			 it = hMap.keySet().iterator();
			 while(it.hasNext()){
				String fileName = it.next();
				vecDeleName.add(fileName);
				//刪除
				String sql = "DELETE FROM todo_list WHERE exam_no = ? " +
													" AND file_name =? ";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, ei.examNo);
				ps.setString(2, fileName);
				ps.execute();
			 }
			 //刪除檔案
			 Iterator<CmisObject> it2 = fdStage.getChildren().iterator();
			 while(it2.hasNext()){
				 CmisObject co = it2.next();
				 if(vecDeleName.contains(co.getName())) co.delete();
			 }
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//產出保外組助檢版檢查評述檔/證票組空白檢查評述
	@SuppressWarnings("resource")
	public void genEmptyStateFormStaff(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			//String fileId = actionRequest.getParameter("fileId");
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			//
			if(ei.examCommentProduceType==ExamCommentProduceType.EXAM_COMMENT_PRODUCE_BLANK.ordinal()){				//證票組
				//Log
				String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
				ErpUtil.logUserAction("產出檢查評述檔案", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//
				if(tempCollect.fExamComment == null) throw new Exception("找不到檢查評述空白樣板檔。");
				//
				String pureName = tempCollect.fExamComment.getName().substring(0, tempCollect.fExamComment.getName().lastIndexOf("."));
				String[] arrStaff = ei.staffs.split(";");
				//檢查所要產出的檔案是否存於待辦事項中
				for(String staffName: arrStaff){
					String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + pureName + "_" + staffName + ".doc";
					//
					if(underToDoListIgnoreStage(ei.examNo, fileName, conn)) throw new Exception(fileName + " 已在待辦事項內，無法重新產出。");
				}				
				//開始寫檔
				for(String staffName: arrStaff){
					String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + pureName + "_" + staffName + ".doc";
					//
					ParseDoc pd = new ParseDoc();
					ByteArrayOutputStream baos = pd.insertExamCommentHeader(tempCollect.fExamComment.getContentStream().getStream(),
																		ei.bankName,
																		ei.reportNo);
					
					InputStream is = new ByteArrayInputStream(baos.toByteArray());
					//寫檔
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
					//
					String updateMemo = "重新產出";
					AlfrescoDocument alfDoc2 = null;
					boolean isNewNode = false;
					if(!htNames.containsKey(fileName)){
						updateMemo = "";
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
						//
						isNewNode = true;
					}else{
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));	
					}
					//加入狀態
					props.put("it:updateMemo", updateMemo);
					props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
					
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					if(isNewNode){
						alfDoc2 = (AlfrescoDocument)alfDoc2.updateProperties(props);
					}else{
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}
					//---------------------------- 加入助檢待辦事項 -----------------------------------------------
					if(!staffName.equals("")){
						Task t = new Task();
						t.taskType = ErpUtil.TASK_FILL_REPORT;
						t.reportNo = ei.reportNo;
						t.examNo = ei.examNo;
						t.bankName = ei.bankName;
						t.stageFolderId = fdStage.getId();
						t.stageName = fdStage.getName();
						t.fileId = alfDoc2.getId();
						t.fileName = alfDoc2.getName();
						t.versionNo = alfDoc2.getVersionLabel();
						t.assignee = staffName;
						t.deadLine = ei.deadLineStaff;
						t.issuer = userFullName;
						//
						ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
					}
				}
			}else if(ei.examCommentProduceType==ExamCommentProduceType.EXAM_COMMENT_PRODUCE_GENERAL.ordinal()){			//保外組
				//Log
				String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
				ErpUtil.logUserAction("產出內部參考事項表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//
				if(tempCollect.htInsForeState.size() == 0) throw new Exception("找不到內部參考事項範本檔。");
				//
				//AlfrescoDocument alfDocAllocate = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				//email 附件
				Vector<InputStream> vecIS = new Vector<InputStream>(); 
				//
				ParseDoc pd = new ParseDoc();
				//HashMap Key: 助檢姓名_業務名稱, value: Word 報表
				FileNameData fnd = new FileNameData();
				fnd.certNumber = ei.examNo;
				fnd.CheckBankName = ei.bankNameShort;
				fnd.CheckNumber = ei.reportNo;
				
				HashMap<String, ByteArrayOutputStream> hMap = pd.AssingWord(tempCollect.htInsForeState,
																		ei.bankName,
																		tempCollect.alfAllocateJson.getContentStream().getStream(),
																		fnd);
				//檢查所要產出的檔案是否已存於待辦事項中
				Iterator<String> it = hMap.keySet().iterator();
				while(it.hasNext()){
					String fileName = it.next();
					//
					if(underToDoListIgnoreStage(ei.examNo, fileName, conn)) throw new Exception(fileName + " 已在待辦事項內，無法重新產出。");
				}
				//
				String[] arrStaff = ei.staffs.split(";");
				//
				it = hMap.keySet().iterator();
				while(it.hasNext()){
					String key = it.next();
					//
					ByteArrayOutputStream bo = hMap.get(key);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					vecIS.add(is);
					//寫檔
					String fileName = key;
					if(!fileName.contains("*")){
						String pureName = fileName.substring(0, fileName.lastIndexOf("."));
						//
						String staffName = "";
						for(String s: arrStaff){
							if(pureName.endsWith(s)){
								staffName = s;
								break;
							}
						}
						//
						Map<String, String> props = new HashMap<String, String>();
						props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
						props.put("cm:author", userFullName);
		
						MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
						ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
						//
						String updateMemo = "重新產出";
						AlfrescoDocument alfDoc2 = null;
						boolean isNewNode = false;
						if(!htNames.containsKey(fileName)){
							updateMemo = "";
							props.put(PropertyIds.NAME, fileName);
							alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
							//
							isNewNode = true;
						}else{
							alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));	
						}
						//加入狀態
						props.put("it:updateMemo", updateMemo);
						props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
						
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						if(isNewNode){
							alfDoc2 = (AlfrescoDocument)alfDoc2.updateProperties(props);
						}else{
							ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
							Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
							ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
							alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
						}
						//---------------------------- 加入助檢待辦事項 -----------------------------------------------
						if(!staffName.equals("")){
							Task t = new Task();
							t.taskType = ErpUtil.TASK_FILL_REPORT;
							t.reportNo = ei.reportNo;
							t.examNo = ei.examNo;
							t.bankName = ei.bankName;
							t.stageFolderId = fdStage.getId();
							t.stageName = fdStage.getName();
							t.fileId = alfDoc2.getId();
							t.fileName = alfDoc2.getName();
							t.versionNo = alfDoc2.getVersionLabel();
							t.assignee = staffName;
							t.deadLine = ei.deadLineStaff;
							t.issuer = userFullName;
							//
							ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
						}
					}
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	
	//評等審議階段動作
	public void doStageEvalAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			conn = ds.getConnection();
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//
			String actionType = actionRequest.getParameter("actionType");
			if(!actionType.equals("")){
				String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
				ErpUtil.logUserAction(actionType, _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			}
			//
			if(actionType.startsWith("評等項目別查核內容摘要表(無需實地查核缺失報告)")){
				String _userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
				if(actionType.contains("-")){
					_userFullName = actionType.split("-")[1].trim();
				}
				this.genEmptyEvalFormStaffByEval(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage, false, _userFullName);
			}else if(actionType.startsWith("評等項目別查核內容摘要表(需實地查核缺失報告)")){
				String _userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
				if(actionType.contains("-")){
					_userFullName = actionType.split("-")[1].trim();
				}
				this.genEmptyEvalFormStaffByEval(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage, true, _userFullName);
			//}else if(actionType.equals("主評版評鑑細項評量表")){
			//	this.genEvalMeasureFormMainStaff(actionRequest, actionResponse, conn);
			}else if(actionType.equals("領隊版評鑑細項評量表")){
				this.genEvalMeasureFormMainStaff(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage);
				this.genEvalMeasureFormLeader(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage);
			}else if(actionType.equals("評鑑細項得分一覽表")){
				this.genEvalItemScoreList(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage);
			}else if(actionType.equals("本會內部參考事項")){
				this.genEvalInternalReference(actionRequest, actionResponse, conn, alfSessionAdmin, fdStage);
			}else if(actionType.equals("")){
				throw new Exception("請指定所要執行的選項。");
			}else{
				throw new Exception("指定的選項無法辨識。");
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//檢查結束階段動作
	public void doStageEndAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			conn = ds.getConnection();
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//
			String actionType = actionRequest.getParameter("actionType");
			if(!actionType.equals("")){
				String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
				ErpUtil.logUserAction(actionType, _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			}
			//
			if(actionType.startsWith("我的檢查項目別查核內容摘要表")){
				this.genEmptyEvalFormStaffByItem(actionRequest, actionResponse, userFullName);
			}else if(actionType.startsWith("助檢之檢查項目別查核內容摘要表")){
				String _userFullName = actionType.split("-")[1].trim();
				this.genEmptyEvalFormStaffByItem(actionRequest, actionResponse, _userFullName);
			}else if(actionType.equals("")){
				throw new Exception("請指定所要執行的選項。");
			}else{
				throw new Exception("指定的選項無法辨識。");
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
		
	//產出我的檢查項目別查核內容摘要表
	@SuppressWarnings("resource")
	public void genEmptyEvalFormStaffByItem(ActionRequest actionRequest, ActionResponse actionResponse, String userFullName) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//
			AlfrescoDocument alfDoc = null;
			String fileId = "";
			if(actionRequest.getParameter("fileId") != null){							//由工作分配表檔案啟動
				fileId = actionRequest.getParameter("fileId");
				alfDoc = this.getLateastVersion(alfSessionAdmin, fdStage, fileId);		//取得最新版本
			}
			//準備行前與結束階段內目前使用者的報告表
			Hashtable<String, String> htNameId = new Hashtable<String, String>();		//檔名與ID對照
			//準備 檢查結束階段
			AlfrescoFolder fdEndExam = null;
			//
			//if(alfDoc == null){			//由檢查結束階段啟動
				AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
				Iterator<CmisObject> it = fdExam.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
						Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
						while(it2.hasNext()){
							CmisObject co2 = it2.next();
							if(alfDoc == null && 
									co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
									co2.getName().toLowerCase().endsWith("json") &&
									!co2.getName().startsWith("_del#")){
								alfDoc = (AlfrescoDocument)co2;
							}else if((co2.getName().contains("檢查重點") || co2.getName().contains("必查項目")) &&
									co2.getName().contains(userFullName) &&
									!co2.getName().startsWith("_del#")){
								htNameId.put(co2.getName(), co2.getId());
							}
						}
					}else if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("結束")){
						fdEndExam = (AlfrescoFolder)co;
					}
				}
			//}
			//
			if(alfDoc == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
			//
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出我的查核內容摘要表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//檢查重產時是否仍有未完成的檢查項目別查核內容摘要表(待辦事項)
			String sql = "SELECT COUNT(*) FROM todo_list WHERE exam_no = ? " +
									" AND (done IS NULL OR done =0) " +
									" AND file_name LIKE '%檢查項目別查核內容摘要表%' " +
									" AND file_name LIKE '%" + userFullName + "%'";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, ei.examNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				int cc = rs.getInt(1);
				if(cc > 0) throw new Exception("您尚有 " + cc + " 項檢查項目別查核內容摘要表於待辦事項流程中，無法重新產出。");
			}
			rs.close();
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.evalSummFormByExamItem == null) throw new Exception("檢查項目別查核內容重點摘要表樣板檔找不到。");
			//準備重要查核項目報告表
			HashMap<String, InputStream> hmDocStaff = new HashMap<String, InputStream>();
			if(fdEndExam != null){
				it = fdEndExam.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
							co.getName().toLowerCase().endsWith("doc") && 
							!co.getName().contains("彙整表") &&
							!co.getName().startsWith("列印用") &&
							!co.getName().startsWith("_del#") &&
							(co.getName().contains("檢查重點") || co.getName().contains("必查項目")) &&
							co.getName().contains(userFullName)){		//目前登入人員所填寫的檔案
						//arrDocStaff.add(((AlfrescoDocument)co).getContentStream().getStream());
						htNameId.put(co.getName(), co.getId());
					}
				}
			}
			if(htNameId.size() == 0) throw new Exception("檢查行前與結束階段查無助檢版重要查核項目報告表。");
			//
			Iterator<String> its = htNameId.keySet().iterator();
			while(its.hasNext()){
				String fname = its.next();
				String fid = htNameId.get(fname);
				AlfrescoDocument doc = (AlfrescoDocument)alfSessionAdmin.getObject(fid);
				
				hmDocStaff.put(doc.getName(), doc.getContentStream().getStream());
			}
			//
			ParseDoc pd = new ParseDoc();
			//產出: 檢查項目別查核內容摘要表
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			//
			HashMap<String, ByteArrayOutputStream> hMap = pd.ExamContentSummary(tempCollect.evalSummFormByExamItem, 
																			alfDoc.getContentStream().getStream(),
																			hmDocStaff,
																			userFullName,
																			ei.bankName,
																			ei.reportNo,
																			fnd);
			//System.out.println("Hash Map has count: " + hMap.size());
			Iterator<String> it3 = hMap.keySet().iterator();
			while(it3.hasNext()){
				if(conn.isClosed()) conn = ds.getConnection();
				//
				String key = it3.next();
				if(key.contains(userFullName)){		//目前登入人員的檔案才寫
					ByteArrayOutputStream bo = hMap.get(key);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					//寫檔
					String fileName = key;
					
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
					//
					String updateMemo = "重新產出";
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
					props.put("cm:author", userFullName);
					
					AlfrescoDocument alfDoc2 = null;
					boolean isNewNode = false;
					if(!htNames.containsKey(fileName)){
						updateMemo = "";
						props.put(PropertyIds.NAME, fileName);
						
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
						//
						isNewNode = true;
					}else{
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));	
					}
					//加入狀態
					props.put("it:updateMemo", updateMemo);
					//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
					//
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					if(isNewNode){
						alfDoc2 = (AlfrescoDocument)alfDoc2.updateProperties(props);
					}else{
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}
					//增加待辦事項: 自己產的檔案是否需要加入待辦事項 ??????
					/*
					Task t = new Task();
					
					t.taskType = ErpUtil.TASK_FILL_REPORT;
					t.reportNo = ei.reportNo;
					t.examNo = ei.examNo;
					t.bankName = ei.bankName;
					t.stageFolderId = fdStage.getId();
					t.stageName = fdStage.getName();
					t.fileId = alfDoc2.getId();
					t.fileName = alfDoc2.getName();
					t.versionNo = alfDoc2.getVersionLabel();
					t.assignee = userFullName;
					t.deadLine = ei.deadLineStaff;
					t.issuer = userFullName;
					//
					ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
					*/
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	
	//產出評等項目別查核內容摘要表
	public void genEmptyEvalFormStaffByEval(ActionRequest actionRequest, ActionResponse actionResponse, Connection conn, Session alfSessionAdmin, AlfrescoFolder fdStage, boolean isSpotDefectNeed, String mainAssesserName) throws Exception {
		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//找到工作分配表
		AlfrescoDocument alfDoc = null;
		//
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
							co2.getName().toLowerCase().endsWith("json") &&
							!co2.getName().startsWith("_del#")){
						alfDoc = (AlfrescoDocument)co2;
						break;
					}
				}
			}
		}
		//
		if(alfDoc == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
		//找出檢查結束與檢討會後階段
		AlfrescoFolder fdEndExam = null;
		AlfrescoFolder fdEndMeet = null;
		it = fdExam.getChildren().iterator();
        	while(it.hasNext()){
        		CmisObject co = it.next();
        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
        			if(co.getName().contains("結束")){
        				fdEndExam = (AlfrescoFolder)co;
        			}else if(co.getName().contains("會後")){
        				fdEndMeet = (AlfrescoFolder)co;
        			}
        		}
        	}
        	if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
        	if(fdEndMeet == null) throw new Exception("找不到檢討會後階段。");

        	//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.evalSummFormByEvaltem == null) throw new Exception("評等項目別查核內容重點摘要表範本檔找不到。");
		//準備檢討會後之一、二、三級缺失情形表
		TreeMap<String, InputStream> mapDocDefect = new TreeMap<String, InputStream>(CompositeNo.ms_SortString_Comparator_ByCollator);
		it = fdEndMeet.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("doc") && 
					!co.getName().startsWith("列印用") &&
					!co.getName().startsWith("_del#") &&
					co.getName().contains("缺失情形表") &&
					!co.getName().contains("面請")){
				mapDocDefect.put(co.getName(), ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		// 按照檔名一級、二級、三級順序排列的InputStream。
		ArrayList<InputStream> arrDocDefect = new ArrayList<InputStream>();
		arrDocDefect.addAll(mapDocDefect.values());
		//準備檢查項目別查核內容摘要表
		HashMap<String, InputStream> hmDocStaff = new HashMap<String, InputStream>();
		it = fdEndExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("doc") && 
					!co.getName().startsWith("列印用") &&
					!co.getName().startsWith("_del#") &&
					co.getName().contains("檢查項目別查核內容摘要表")){
				String pureName = co.getName().toLowerCase().replace(".doc", "");
				hmDocStaff.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(hmDocStaff.size() == 0) throw new Exception("尚未產出檢查項目別查核內容摘要表。");
		//檢查主評人員之評等項目別查核內容摘要表是否仍在待辦事項中
		String sql = "SELECT COUNT(*) FROM todo_list WHERE exam_no = ? " +
													" AND (done IS NULL OR done =0) " +
													" AND file_name LIKE '%評等項目別查核內容摘要表%' " +
													" AND file_name LIKE '%" + mainAssesserName + "%'";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, ei.examNo);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			int cc = rs.getInt(1);
			if(cc > 0) throw new Exception("您(" + mainAssesserName + ")尚有 " + cc + " 項評等項目別查核內容摘要表於待辦事項流程中，無法重新產出。");
		}
		rs.close();
		
		//讀取準備貼到 Word 的 Excel 表格(準備編表連動底稿)
		HashMap<String, InputStream> hmExcels = new HashMap<String, InputStream>();
		it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("xls") &&
					!co.getName().startsWith("_del")){
					String pureName = co.getName().toLowerCase().replace(".xls", "");
				hmExcels.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(hmExcels.size() == 0) throw new Exception("尚未載入編表連動底稿之Excel檔案。");
		//
		ParseDoc pd = new ParseDoc();
		//產出: 評等項目別查核內容摘要表
		FileNameData fnd = new FileNameData();
		fnd.certNumber = ei.examNo;
		fnd.CheckBankName = ei.bankNameShort;
		fnd.CheckNumber = ei.reportNo;
		
		HashMap<String, ByteArrayOutputStream> hMap = pd.MainReview(tempCollect.evalSummFormByEvaltem, 
																hmDocStaff,
																mainAssesserName,
																arrDocDefect,
																ei.bankName,
																ei.reportNo,
																alfDoc.getContentStream().getStream(),
																fnd,
																hmExcels,
																isSpotDefectNeed);
		if(hMap.size() ==0 ) throw new Exception("評等項目別查核內容摘要表產出數量為零。");
		//System.out.println("Hash Map has count: " + hMap.size());
		Iterator<String> it2 = hMap.keySet().iterator();
		while(it2.hasNext()){
			String key = it2.next();
			//
			//String staffName = key.split("_")[0];
			//String bisName = key.split("_")[1];
			//
			ByteArrayOutputStream bo = hMap.get(key);
			InputStream is = new ByteArrayInputStream(bo.toByteArray());
			//寫檔
			String fileName = key;
			if(!fileName.contains("*")){
				MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
				ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
				//
				String updateMemo = "重新產出";
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put("cm:author", ErpUtil.getCurrUserFullName(themeDisplay));
				
				AlfrescoDocument alfDoc2 = null;
				boolean isNewNode = false;
				if(!htNames.containsKey(fileName)){
					updateMemo = "";
					props.put(PropertyIds.NAME, fileName);
					alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					//
					isNewNode = true;
				}else{
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));	
				}
				//加入狀態
				props.put("it:updateMemo", updateMemo);
				//props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				//
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				if(isNewNode){
					alfDoc2 = (AlfrescoDocument)alfDoc2.updateProperties(props);
				}else{
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}
				//---------------------------- 加入主評待辦事項 -----------------------------------------------
				//Task t = new Task();
				//t.taskType = ErpUtil.TASK_FILL_REPORT;
				//t.reportNo = ei.reportNo;
				//t.examNo = ei.examNo;
				//t.bankName = ei.bankName;
				//t.stageFolderId = fdStage.getId();
				//t.stageName = fdStage.getName();
				//t.fileId = alfDoc2.getId();
				//t.fileName = alfDoc2.getName();
				//t.versionNo = alfDoc2.getVersionLabel();
				//t.assignee = userFullName;
				//t.deadLine = ei.deadLineStaff;
				//t.issuer = userFullName;
				//
				//ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
			}
		}
	}
	
	//產出主評版評鑑細項評量表
	public void genEvalMeasureFormMainStaff(ActionRequest actionRequest, ActionResponse actionResponse, Connection conn, Session alfSessionAdmin, AlfrescoFolder fdStage) throws Exception {
		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//找到工作分配表
		AlfrescoDocument alfDoc = null;
		//
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
							co2.getName().toLowerCase().endsWith("json") &&
							!co2.getName().startsWith("_del#")){
						alfDoc = (AlfrescoDocument)co2;
						break;
					}
				}
			}
		}
		//
		if(alfDoc == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
		//
		//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.evalItemEvalForm == null) throw new Exception("評鑑細項評量表範本檔找不到。");
		//準備主評版查核內容摘要表
		HashMap<String, InputStream> hMain = new HashMap<String, InputStream>();
		it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("doc") && 
					!co.getName().startsWith("列印用") &&
					!co.getName().startsWith("_del#") &&
					co.getName().contains("評等項目別查核內容摘要表")){
				String pureName = co.getName().substring(0, co.getName().lastIndexOf(".")); 
				hMain.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(hMain.size() == 0) throw new Exception("尚未產出檢查項目別查核內容摘要表。");
		//
		ParseDoc pd = new ParseDoc();
		//產出: 主評版評鑑細項評量表
		FileNameData fnd = new FileNameData();
		fnd.certNumber = ei.examNo;
		fnd.CheckBankName = ei.bankNameShort;
		fnd.CheckNumber = ei.reportNo;
		
		HashMap<String, ByteArrayOutputStream> hMap = pd.Assessment(tempCollect.evalItemEvalForm,
																hMain,
																ei.bankName,
																ei.dateBase,
																ei.leaderName,
																alfDoc.getContentStream().getStream(),
																fnd);			//目前改由領隊產出主評版評鑑細項評量表
		if(hMap.size() == 0) throw new Exception("主評版評鑑細項評量表產出數量為零。");
		//System.out.println("Hash Map has count: " + hMap.size());
		Iterator<String> it2 = hMap.keySet().iterator();
		while(it2.hasNext()){
			String key = it2.next();
			//
			//String staffName = key.split("_")[0];
			//String bisName = key.split("_")[1];
			//
			ByteArrayOutputStream bo = hMap.get(key);
			InputStream is = new ByteArrayInputStream(bo.toByteArray());
			//寫檔
			String fileName = key;
			if(!fileName.contains("*")){
				MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
				ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
				//
				String updateMemo = "重新產出";
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put("cm:author", userFullName);
				
				AlfrescoDocument alfDoc2 = null;
				if(htNames.containsKey(fileName)){
					props.put("it:updateMemo", updateMemo);
					//
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}else{
					props.put(PropertyIds.NAME, fileName);
					alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
				}
				/*
				//---------------------------- 加入助檢待辦事項 -----------------------------------------------
				Task t = new Task();
				t.taskType = ErpUtil.TASK_FILL_REPORT;
				t.reportNo = ei.reportNo;
				t.examNo = ei.examNo;
				t.bankName = ei.bankName;
				t.stageFolderId = fdStage.getId();
				t.stageName = fdStage.getName();
				t.fileId = alfDoc2.getId();
				t.fileName = alfDoc2.getName();
				t.versionNo = alfDoc2.getVersionLabel();
				t.assignee = staffName;
				t.deadLine = ei.deadLineStaff;
				t.issuer = userFullName;
				//
				ErpUtil.addTask(t, conn);
				*/
			}
		}
	}

	//產出領隊版評鑑細項評量表
	public void genEvalMeasureFormLeader(ActionRequest actionRequest, ActionResponse actionResponse, Connection conn, Session alfSessionAdmin, AlfrescoFolder fdStage) throws Exception {
		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//找到工作分配表
		AlfrescoDocument alfDoc = null;
		//
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
											co2.getName().toLowerCase().endsWith("json") &&
											!co2.getName().startsWith("_del#")){
						alfDoc = (AlfrescoDocument)co2;
						break;
					}
				}
			}
		}
		//
		if(alfDoc == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
		//
		//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.evalItemEvalForm == null) throw new Exception("評鑑細項評量表範本檔找不到。");
		//準備主評版評鑑細項評量表
		HashMap<String, InputStream> mapMainStaff = new HashMap<String, InputStream>();
		it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("doc") && 
					!co.getName().startsWith("列印用") &&
					!co.getName().startsWith("_del#") &&
					co.getName().contains("評鑑細項評量表") &&
					!co.getName().contains("領隊版")){						//助檢版檔名含名字
				String pureName = co.getName().substring(0, co.getName().lastIndexOf(".")); 
				mapMainStaff.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(mapMainStaff.size() == 0) throw new Exception("尚未產出助檢版評鑑細項評量表。");
		//
		ParseDoc pd = new ParseDoc();
		//產出: 助檢版評鑑細項評量表
		FileNameData fnd = new FileNameData();
		fnd.certNumber = ei.examNo;
		fnd.CheckBankName = ei.bankNameShort;
		fnd.CheckNumber = ei.reportNo;
		
		HashMap<String, ByteArrayOutputStream> hMap = pd.AssessmentLeader(tempCollect.evalItemEvalForm,
																	mapMainStaff,
																	ei.bankName,
																	ei.dateBase,
																	ei.leaderName,
																	alfDoc.getContentStream().getStream(),
																	fnd);
		//System.out.println("Hash Map has count: " + hMap.size());
		Iterator<String> it2 = hMap.keySet().iterator();
		while(it2.hasNext()){
			String key = it2.next();
			//
			//String staffName = key.split("_")[0];
			//String bisName = key.split("_")[1];
			//
			ByteArrayOutputStream bo = hMap.get(key);
			InputStream is = new ByteArrayInputStream(bo.toByteArray());
			//寫檔
			String fileName = key;
			if(!fileName.contains("*")){
				MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
				ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
				//
				String updateMemo = "重產領隊版評鑑細項評量表";
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put("cm:author", userFullName);
				
				AlfrescoDocument alfDoc2 = null;
				if(htNames.containsKey(fileName)){
					props.put("it:updateMemo", updateMemo);
					//
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}else{
					props.put(PropertyIds.NAME, fileName);
					alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
				}
				/*
				//---------------------------- 加入助檢待辦事項 -----------------------------------------------
				Task t = new Task();
				t.taskType = ErpUtil.TASK_FILL_REPORT;
				t.reportNo = ei.reportNo;
				t.examNo = ei.examNo;
				t.bankName = ei.bankName;
				t.stageFolderId = fdStage.getId();
				t.stageName = fdStage.getName();
				t.fileId = alfDoc2.getId();
				t.fileName = alfDoc2.getName();
				t.versionNo = alfDoc2.getVersionLabel();
				t.assignee = staffName;
				t.deadLine = ei.deadLineStaff;
				t.issuer = userFullName;
				//
				ErpUtil.addTask(t, conn);
				*/
			}
		}
		//發送電子郵件通知
			
	}
	
	//產出評鑑細項得分一覽表
	public void genEvalItemScoreList(ActionRequest actionRequest, ActionResponse actionResponse, Connection conn, Session alfSessionAdmin, AlfrescoFolder fdStage) throws Exception {
		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//找到工作分配表
		AlfrescoDocument alfDoc = null;
		//
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
											co2.getName().toLowerCase().endsWith("json") &&
											!co2.getName().startsWith("_del#")){
						alfDoc = (AlfrescoDocument)co2;
						break;
					}
				}
			}
		}
		//
		if(alfDoc == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
		//
		//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.evalItemScoreList == null) throw new Exception("評鑑細項得分一覽表範本檔找不到。");
		//準備領隊版評鑑細項評量表
		InputStream isMainLeader = null;
		it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("doc") && 
					!co.getName().startsWith("列印用") &&
					!co.getName().startsWith("_del#") &&
					co.getName().contains("評鑑細項評量表") &&
					co.getName().contains("領隊版")){						//助檢版檔名含名字
				isMainLeader = ((AlfrescoDocument)co).getContentStream().getStream();
				break;
			}
		}
		if(isMainLeader == null) throw new Exception("尚未產出領隊版評鑑細項評量表。");
		//
		ParseDoc pd = new ParseDoc();
		//產出: 評鑑細項得分一覽表
		HSSFWorkbook wb = pd.StartAssignExcel(tempCollect.evalItemScoreList, 
										isMainLeader,
										ei.bankName,
										ei.dateBase,
										alfDoc.getContentStream().getStream());
		//
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		wb.write(bos);
		InputStream is = new ByteArrayInputStream(bos.toByteArray());
		//寫檔
		String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "評鑑細項得分一覽表.xls";
		ContentStream cs = new ContentStreamImpl(fileName, null, alfDoc.getContentStreamMimeType(), is);
		//
		String updateMemo = "重產評鑑細項得分一覽表";
		Map<String, String> props = new HashMap<String, String>();
		props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
		props.put("cm:author", userFullName);
		
		AlfrescoDocument alfDoc2 = null;
		if(htNames.containsKey(fileName)){
			props.put("it:updateMemo", updateMemo);
			//
			alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
			if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
			//
			ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
			alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
		}else{
			props.put(PropertyIds.NAME, fileName);
			alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
		}
		//發送電子郵件通知
			
	}

	//產出本會內部參考事項
	public void genEvalInternalReference(ActionRequest actionRequest, ActionResponse actionResponse, Connection conn, Session alfSessionAdmin, AlfrescoFolder fdStage) throws Exception {
		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//找到工作分配表
		AlfrescoDocument alfDocAllocate = null;
		//
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
											co2.getName().toLowerCase().endsWith("json") &&
											!co2.getName().startsWith("_del#")){
						alfDocAllocate = (AlfrescoDocument)co2;
						break;
					}
				}
			}
		}
		//
		if(alfDocAllocate == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
		//
		//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.evalInterRefItem == null) throw new Exception("本會內部參考事項範本檔找不到。");
		//if(tempCollect.evalGridForm == null) throw new Exception("編表連動底稿範本檔找不到。");
		//準備助檢版檢查項目別查核內容摘要表
		ArrayList<InputStream> arrDocStaff = new ArrayList<InputStream>();
		it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("評等")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co2 = it2.next();
					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
							co2.getName().toLowerCase().endsWith("doc") && 
							co2.getName().contains("評等項目別查核內容摘要表") && 
							!co2.getName().toLowerCase().startsWith("_del") && 
							!co2.getName().contains("領隊版")){
						arrDocStaff.add(((AlfrescoDocument)co2).getContentStream().getStream());
					}
				}
				break;
			}
		}
		if(arrDocStaff.size() == 0) throw new Exception("尚未產出評等項目別查核內容摘要表。");
		//
		//準備編表連動底稿
		HashMap<String, InputStream> hmRefExcel = new HashMap<String, InputStream>();
		it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					co.getName().toLowerCase().endsWith("xls") &&
					!co.getName().startsWith("_del")){
				String pureName = co.getName().toLowerCase().replace(".xls", "");
				hmRefExcel.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(hmRefExcel.size() == 0) throw new Exception("尚未載入編表連動底稿之Excel檔案。");
		//
		ParseDoc pd = new ParseDoc();
		//產出: 本會內部參考事項
		ByteArrayOutputStream bos = pd.InternalReference(tempCollect.evalInterRefItem,
												hmRefExcel,
												arrDocStaff,
												alfDocAllocate.getContentStream().getStream());
		if(bos == null) throw new Exception("呼叫本會內部參考事項函式之回傳為空值。");
		//
		InputStream is = new ByteArrayInputStream(bos.toByteArray());
		//寫檔
		String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "本會內部參考事項.810.doc";
		MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
		ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
		//
		String updateMemo = "重產本會內部參考事項.810";
		Map<String, String> props = new HashMap<String, String>();
		props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
		props.put("cm:author", userFullName);
		
		AlfrescoDocument alfDoc2 = null;
		if(htNames.containsKey(fileName)){
			props.put("it:updateMemo", updateMemo);
			//
			alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
			if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
			//
			ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
			alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
		}else{
			props.put(PropertyIds.NAME, fileName);
			alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
		}
	}
	
	//產出本銀總行檢查意見.210N表
	public void genLocalBankExamOpinion(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出檢查意見.210N表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找到工作分配表
			AlfrescoDocument alfDocAllocate = null;
			//
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
					Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
					while(it2.hasNext()){
						CmisObject co2 = it2.next();
						if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
												co2.getName().toLowerCase().endsWith("json") &&
												!co2.getName().startsWith("_del#")){
							alfDocAllocate = (AlfrescoDocument)co2;
							break;
						}
					}
				}
			}
			//
			if(alfDocAllocate == null) throw new Exception("找不到本案之工作分配表(含評鑑細項)");
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//彙整表 InputStream 集合
			HashMap<String, InputStream> hmISDoc = new HashMap<String, InputStream>(); 
			//找出檢討會後缺失情形表
			 it = fdExam.getChildren().iterator();
			 while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("會後")){
					Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
					while(it2.hasNext()){
						CmisObject co2 = it2.next();
						if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
								co2.getName().contains("缺失情形表") &&
								co2.getName().contains("內部") && 
								!co2.getName().contains("面請") &&
								!co2.getName().startsWith("列印用") &&
								!co2.getName().startsWith("_del#") &&
								co2.getName().toLowerCase().endsWith("doc")){
							String pureName = co2.getName().substring(0, co2.getName().lastIndexOf("."));
							hmISDoc.put(pureName,  ((AlfrescoDocument)co2).getContentStream().getStream());
						}
					}
					break;
				}
			 }
			if(hmISDoc.size() == 0) throw new Exception("目前目錄內查無檢查缺失情形表(內部)。");
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.examOpinion210 == null) throw new Exception("查無「檢查意見.210N」之空白範本表格。");
			//
			ParseDoc pd = new ParseDoc();
			//
			ByteArrayOutputStream bos = pd.LocalBankExamReport(hmISDoc,
														tempCollect.examOpinion210, 
														false,
														alfDocAllocate.getContentStream().getStream()); 
			if(bos == null) throw new Exception("呼叫評等審議階段檢查意見函式之回傳為空值。");
			//
			InputStream is = new ByteArrayInputStream(bos.toByteArray());
			//寫檔
			String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查意見.210N.doc";
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
			//
			String updateMemo = "重產檢查意見";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			//
			AlfrescoDocument alfDoc2 = null;
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			conn.close();
	   }catch(Exception ex){
	       	//ex.printStackTrace();
		   	String errMsg = ErpUtil.getItezErrorCode(ex);
		   	if(errMsg.equals("")){
		   		errMsg = ex.getMessage();
		   	}
		   	actionRequest.setAttribute("errMsg", errMsg);
		   	SessionErrors.add(actionRequest, "error");
	   }finally{
	       try{
	    	   if(conn != null) conn.close();
	       }catch(Exception _ex){
	       }
	   }
	}
	
	//產出資訊檢查重要查核事項報告表(助檢版)
	public void genEmptyInfoExamStateStaff(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			String fileId = actionRequest.getParameter("fileId");
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出電腦稽核重要查核事項報告表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//取得最新版
			AlfrescoDocument alfDocAllocate = this.getLateastVersion(alfSessionAdmin, fdStage, fileId);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.fInfoExamImpChkStaffForm == null) throw new Exception("資訊作業重要查核項目事項表(助檢版)範本檔找不到。");
			//
			InformationColumnValue icv = new InformationColumnValue();
			icv.reportId = ei.reportNo;
			icv.checkType = ei.examType;
			icv.organization = ei.bankName;
			icv.leader = ei.leaderName;
			icv.base_date = ei.dateBase;
			icv.startDate = ei.dateStart;
			icv.endDate = ei.dateEnd;
			icv.reviewDate = ei.dateMeeting;
			icv.durationDays = String.valueOf(ei.examDays);
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			HashMap<String,ByteArrayOutputStream> hMap = pd.InformationExam(tempCollect.fInfoExamImpChkStaffForm.getContentStream().getStream(),
																	icv,
																	alfDocAllocate.getContentStream().getStream(),
																	fnd);
			if(hMap.size() == 0) throw new Exception("資訊作業重要查核項目報告表函式之回傳為空值。");
			//檢查所要產出檔案是否已在待辦事項中
			Iterator<String> it = hMap.keySet().iterator();
			while(it.hasNext()){
				String fileName = it.next();
				//檢查是否在流程中
			    if(underToDoListIgnoreStage(collec.examNo, fileName, conn)) throw new Exception(fileName + " 已在流程處理中，無法變更。");
			}
			//email 附件
			Vector<InputStream> vecIS = new Vector<InputStream>();
			//開始寫檔
			it = hMap.keySet().iterator();
			while(it.hasNext()){
				String key = it.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				vecIS.add(is);
				//寫檔
				String fileName = key;
				//
				if(!fileName.contains("*")){
					String pureName = fileName.substring(0, fileName.lastIndexOf("."));
					String[] ss = pureName.split("_");
					String staffName = ss[ss.length - 1];
					//
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
					
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fInfoExamImpChkStaffForm.getName()), is);
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
					
					//---------------------------- 加入助檢待辦事項 -----------------------------------------------
					Task t = new Task();
					t.taskType = ErpUtil.TASK_FILL_REPORT;
					t.reportNo = ei.reportNo;
					t.examNo = ei.examNo;
					t.bankName = ei.bankName;
					t.stageFolderId = fdStage.getId();
					t.stageName = fdStage.getName();
					t.fileId = alfDoc2.getId();
					t.fileName = alfDoc2.getName();
					t.versionNo = alfDoc2.getVersionLabel();
					t.assignee = staffName;
					t.deadLine = ei.deadLineStaff;
					t.issuer = userFullName;
					//
					ErpUtil.addTask(t, ei.formGenedMail2Assistant, conn, themeDisplay);
				}
			}
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//產出資訊檢查重要查核事項彙整表(領隊版)
	public void genInfoExamStateLeader(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出資訊作業重要查核事項彙整表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	if(tempCollect.fInfoExamImpChkLeaderForm == null) throw new Exception("資訊作業重要查核項目項目表(領隊版)範本檔找不到。");
	        	//找出檢查行前、檢查結束
	        	AlfrescoFolder fdPrepare = null, fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				fdPrepare = (AlfrescoFolder)co;
	        			}else if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        			}
	        		}
	        	}
	        	if(fdPrepare == null) throw new Exception("找不到檢查行前階段。");
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
			//找出工作分配表
	        	AlfrescoDocument alfDocAllocate = null;
	        	//
			it = fdPrepare.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        					co.getName().toLowerCase().endsWith("json") &&
	        					!co.getName().startsWith("_del#")){
        				alfDocAllocate = (AlfrescoDocument)co;
        				break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
	        	
	        	//找出要合併的檔(助檢版重要查核項目報告表)
			int countUnApproved = 0;
			String unApprovedNames = "";
	        	ArrayList<InputStream> listIS = new ArrayList<InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("資訊作業")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				co.getName().contains("重要查核") &&
	        				co.getName().contains("助檢") &&
	        				!co.getName().startsWith("_del#") &&
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
	        			if(doc.getPropertyValue("it:docStatus") != null){
	        				if(doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_DRAFT) || doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_APPROVING)){
	        					countUnApproved++;
	        					if(!unApprovedNames.equals("")) unApprovedNames += ",";
	        					unApprovedNames += doc.getName();
	        				}else{
	        					listIS.add(doc.getContentStream().getStream());
	        				}
	        			}else{
	        				listIS.add(doc.getContentStream().getStream());
	        			}
	        		}
	        	}
	        	if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + unApprovedNames + " 等 " + countUnApproved + " 個助檢版報告檔案待審核。");
	        	
	        	if(listIS.size() == 0) throw new Exception("檢查結束階段找不到資訊作業重要查核項目報告表的 MS Word 檔案。");
	        	
	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
			InformationColumnValue icv = new InformationColumnValue();
			icv.reportId = ei.reportNo;
			icv.checkType = ei.examType;
			icv.organization = ei.bankName;
			icv.leader = ei.leaderName;
			icv.base_date = ei.dateBase;
			icv.startDate = ei.dateStart;
			icv.endDate = ei.dateEnd;
			icv.reviewDate = ei.dateMeeting;
			icv.durationDays = String.valueOf(ei.examDays);
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			HashMap<String, ByteArrayOutputStream> hMap = pd.InformationExamMerge(listIS, 
																			tempCollect.fInfoExamImpChkLeaderForm.getContentStream().getStream(), 
																			icv, 
																			alfDocAllocate.getContentStream().getStream(),
																			tempCollect.fInfoExamItemTableHeader.getContentStream().getStream(),
																			fnd); 
			if(hMap.size() == 0) throw new Exception("資訊作業重要查核項目彙整表函式之回傳為空值。");
			//
			Iterator<String> it2 = hMap.keySet().iterator();
			while(it2.hasNext()){
				String key = it2.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				//寫檔
				String fileName = key;
				if(!fileName.contains("*")){
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fInfoExamImpChkLeaderForm.getName()), is);
					//
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						pwc.checkIn(false, props, cs, updateMemo);
						//alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//產出資訊檢查重要查核事項彙整表(領隊版)草稿
	public void genInfoExamStateLeaderDraft(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	String folderIdDraft = actionRequest.getParameter("folderId");
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	AlfrescoFolder fdDraft = (AlfrescoFolder)alfSessionAdmin.getObject(folderIdDraft);
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出資訊作業重要查核事項彙整表草稿", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	if(tempCollect.fInfoExamImpChkLeaderForm == null) throw new Exception("資訊作業重要查核項目項目表(領隊版)範本檔找不到。");
	        	//找出檢查行前、檢查結束
	        	AlfrescoFolder fdPrepare = null, fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				fdPrepare = (AlfrescoFolder)co;
	        			}else if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        			}
	        		}
	        	}
	        	if(fdPrepare == null) throw new Exception("找不到檢查行前階段。");
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
			//找出工作分配表
	        	AlfrescoDocument alfDocAllocate = null;
	        	//
			it = fdPrepare.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        					co.getName().toLowerCase().endsWith("json") &&
	        					!co.getName().startsWith("_del#")){
        				alfDocAllocate = (AlfrescoDocument)co;
        				break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
	        	
	        	//找出要合併的檔(助檢版重要查核項目報告表)
			//int countUnApproved = 0;
	        	ArrayList<InputStream> listIS = new ArrayList<InputStream>();
	        	it = fdEndExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("資訊作業")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				co.getName().contains("重要查核") &&
	        				co.getName().contains("助檢") &&
	        				!co.getName().startsWith("_del#") &&
	        				!co.getName().startsWith("列印用")){
	        			Document doc = (Document)co;
        				listIS.add(doc.getContentStream().getStream());
	        		}
	        	}
	        	if(listIS.size() == 0) throw new Exception("檢查結束階段找不到資訊作業重要查核項目報告表的 MS Word 檔案。");
	        	
	        	//草稿區內所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdDraft);
	        	//
			InformationColumnValue icv = new InformationColumnValue();
			icv.reportId = ei.reportNo;
			icv.checkType = ei.examType;
			icv.organization = ei.bankName;
			icv.leader = ei.leaderName;
			icv.base_date = ei.dateBase;
			icv.startDate = ei.dateStart;
			icv.endDate = ei.dateEnd;
			icv.reviewDate = ei.dateMeeting;
			icv.durationDays = String.valueOf(ei.examDays);
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			HashMap<String, ByteArrayOutputStream> hMap = pd.InformationExamMerge(listIS, 
																			tempCollect.fInfoExamImpChkLeaderForm.getContentStream().getStream(), 
																			icv, 
																			alfDocAllocate.getContentStream().getStream(),
																			tempCollect.fInfoExamItemTableHeader.getContentStream().getStream(),
																			fnd); 
			if(hMap.size() == 0) throw new Exception("執行資訊作業重要查核項目彙整表草稿之回傳為空值。");
			//
			Iterator<String> it2 = hMap.keySet().iterator();
			while(it2.hasNext()){
				String key = it2.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				//寫檔
				String fileName = key;
				if(!fileName.contains("草稿")){
					int idx = fileName.lastIndexOf(".");
					fileName = fileName.substring(0, idx) + "草稿" + fileName.substring(idx);
				}
				if(!fileName.contains("*")){
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fInfoExamImpChkLeaderForm.getName()), is);
					//
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						pwc.checkIn(false, props, cs, updateMemo);
						//alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						fdDraft.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
			//
			//草稿區檔案存入Session
			Vector<StageFile> vecDraft = new Vector<StageFile>();
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					AlfrescoDocument doc = (AlfrescoDocument)co;
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}else if(doc.getPropertyValue("cm:description") != null) {
						if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
							sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
						}
					}
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					//if(!ef.leaderName.contains(sf.author) && ef.staffs.contains(sf.author)) sf.author = ef.leaderName;
					
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdDraft.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
									
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					//
					vecDraft.add(sf);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecDraft", vecDraft);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/draftArea.jsp");
			actionResponse.setRenderParameter("folderId", fdDraft.getId());	
			//
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

	
	//產出資訊作業專案檢查評量表
	public void genInfoExamAssessForm(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出資訊作業專案檢查評量表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	if(tempCollect.fInfoExamAssessForm == null) throw new Exception("資訊作業專案檢查評量表範本檔找不到。");
	        	//找出檢查行前、檢討會前
	        	AlfrescoFolder fdPrepare = null, fdPreMeet = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				fdPrepare = (AlfrescoFolder)co;
	        			}else if(co.getName().contains("會前")){
	        				fdPreMeet = (AlfrescoFolder)co;
	        			}
	        		}
	        	}
	        	if(fdPrepare == null) throw new Exception("找不到檢查行前階段。");
			if(fdPreMeet == null) throw new Exception("找不到檢討會前階段。");
			//找出工作分配表
	        	AlfrescoDocument alfDocAllocate = null;
	        	//
			it = fdPrepare.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        					co.getName().toLowerCase().endsWith("json") &&
	        					!co.getName().startsWith("_del#")){
        				alfDocAllocate = (AlfrescoDocument)co;
        				break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
	        	
	        	//找出檢查行前領隊版資訊作業重要查核事項報告表
	        	InputStream isInfoExamImpChkLeader = null;
	        	
	        	it = fdPreMeet.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getName().contains("資訊作業")  && 
	        				co.getName().toLowerCase().endsWith("doc") &&
	        				co.getName().contains("重要查核") &&
	        				co.getName().contains("領隊") &&
	        				!co.getName().startsWith("_del#") &&
	        				!co.getName().startsWith("列印用")){
	        			isInfoExamImpChkLeader = ((Document)co).getContentStream().getStream();
	        			break;
	        		}
	        	}
	        	if(isInfoExamImpChkLeader == null) throw new Exception("檢討會前找不到資訊作業重要查核項目報告表(領隊版)。");
	        	
	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			HashMap<String, ByteArrayOutputStream> hMap = pd.InformationAssessment(isInfoExamImpChkLeader, 
																			tempCollect.fInfoExamAssessForm.getContentStream().getStream(),
																			ei.dateBase,
																			ei.bankName,
																			alfDocAllocate.getContentStream().getStream(),
																			fnd);
			if(hMap.size() == 0) throw new Exception("資訊作業專案檢查評量表產出函式之回傳為空值。");
			//
			Iterator<String> it2 = hMap.keySet().iterator();
			while(it2.hasNext()){
				String key = it2.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				//寫檔
				String fileName = key;
				if(!fileName.contains("*")){
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fInfoExamImpChkLeaderForm.getName()), is);
					//
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						pwc.checkIn(false, props, cs, updateMemo);
						//alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//產出資訊作業專案檢查評級一覽表
	public void genInfoExamAssessScore(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
	        try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	conn = ds.getConnection();
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	//Log
	        	String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("產出資訊作業專案檢查評級一覽表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//找出範本檔
	        	ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
	        	TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
	        	if(tempCollect.fInfoExamAssessScore == null) throw new Exception("資訊作業專案檢查評鑑細項評級一覽表範本檔找不到。");
	        	//找出評量表
	        	AlfrescoDocument alfDocAssess = null;
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
	        			if(co.getName().toLowerCase().endsWith("doc") &&
	        					!co.getName().startsWith("列印用") &&
							!co.getName().startsWith("_del#") &&	        					
	        					co.getName().contains("資訊作業") &&
	        					co.getName().contains("評量表")){
	        				alfDocAssess = (AlfrescoDocument)co;
	        				break;
	        			}
	        		}
	        	}
	        	if(alfDocAssess == null) throw new Exception("本階段內找不到資訊作業專案檢查評量表。");
	        	//找出工作分配表
	        	AlfrescoDocument alfDocAllocate = null;
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
	        			Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
	        			while(it2.hasNext()){
	        				CmisObject co2 = it2.next();
	        				if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        						co2.getName().toLowerCase().endsWith("json") &&
	        						!co2.getName().startsWith("_del#")){
	        					alfDocAllocate = (AlfrescoDocument)co2;
	        					break;
	        				}
	        			}
	        			break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");	
	        	
	        	//本階段所有檔名
	        	Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
	        	//
			ParseDoc pd = new ParseDoc();
			HSSFWorkbook wb = pd.InformationExamScore(alfDocAssess.getContentStream().getStream(),
														tempCollect.fInfoExamAssessScore.getContentStream().getStream(),
														ei.bankName,
														ei.dateBase,
														alfDocAllocate.getContentStream().getStream());
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			wb.write(bos);
			InputStream is = new ByteArrayInputStream(bos.toByteArray());
			//寫檔
			MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
			String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "資訊作業專案檢查評鑑細項評級一覽表.xls";
			ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fInfoExamAssessScore.getName()), is);
			//
			String updateMemo = "重新產出";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put("cm:author", userFullName);
			
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}


	//交付至檢討會前
	public void moveToPreMeet(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String fileId = actionRequest.getParameter("fileId");
	        	//
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	//
	        	conn = ds.getConnection();
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
	        	AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
	        	//Log
	        	String _path = fdExam.getName() + "/" + fdStage.getName();
	        	ErpUtil.logUserAction("交付檔案至檢討會前", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
       			//找出檢討會前目錄
	        	AlfrescoFolder fdPreMeet = null;
        		Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("會前")){
	        			fdPreMeet = (AlfrescoFolder)co;
	        			break;
	        		}
	        	}
	        	//
			if(fdPreMeet == null) throw new Exception("找不到檢討會前所在目錄。");
			
			//移至檢討會前階段
			alfDoc.move(fdStage, fdPreMeet);
			
			//檢討會前的流程
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			if(ei.preMeetEnableWorkFlow == 1){			//文件需經流程
				Map<String, String> props = new HashMap<String, String>();
				props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
				alfDoc.updateProperties(props);
			}
	        	conn.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/jsp/todo/complete.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
		 
	}
	
	//產出內部版實地檢查缺失情形表(一般與資訊作業共用)
	public void genSpotDefectReportInter(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			boolean isInfoExam = false;				//是否為資訊檢查作業
			boolean isLocalBankHQNormal = false;		//是否為本銀總行	一般檢查
			if(actionRequest.getParameter("infoExam") != null && actionRequest.getParameter("infoExam").equalsIgnoreCase("true")){
				isInfoExam = true;
			}else if(actionRequest.getParameter("localBankHQNormal") != null && actionRequest.getParameter("localBankHQNormal").equalsIgnoreCase("true")){
				isLocalBankHQNormal = true;
			}
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//AlfrescoFolder fdDraft = null;
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出內部版實地檢查缺失情形表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = new Hashtable<String, String>();
			htNames = this.getStageFileNames(fdStage);
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
	        			Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
	        			while(it2.hasNext()){
	        				CmisObject co2 = it2.next();
	        				if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        						co2.getName().toLowerCase().endsWith("json") &&
	        						!co2.getName().startsWith("_del#")){
	        					alfDocAllocate = (AlfrescoDocument)co2;
	        					break;
	        				}
	        			}
	        			break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");			
			//彙整表 InputStream 集合
			ArrayList<InputStream> arrISDoc = new ArrayList<InputStream>(); 
			it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						(co.getName().contains("彙整表") || (co.getName().contains("報告表") && co.getName().contains("領隊"))) &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
					arrISDoc.add(((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(arrISDoc.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業於目前目錄內查無重要查核項目彙整表。");
				}else{
					throw new Exception("目前目錄內查無重要查核項目彙整表。");
				}
			}
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.htSpotDefectInter.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業查無內部版實地檢查缺失情形表之空白範本表格。");
				}else{
					throw new Exception("查無內部版實地檢查缺失情形表之空白範本表格。");
				}
			}else{
				/*
				String sss = "";
				Iterator<String> __it = tempCollect.htSpotDefectInter.keySet().iterator();
				while(__it.hasNext()){
					String s = __it.next();
					if(!sss.equals("")) sss+= "#";
					sss += s;
				}
				if(!sss.equals("")){
					throw new Exception(sss);
				}
				*/
			}
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			fnd.fullBankName = ei.bankName;
			
			//HashMap Key: 報表名稱, value: Word 報表
			String datePeriod = ei.dateStart + " 至 " + ei.dateEnd;
			HashMap<String, ByteArrayOutputStream> hMap = null;
			if(isInfoExam){		//如果是資訊作業
				hMap = pd.CheckDefectReport(arrISDoc.get(0),	//應該只有一個檔
										 tempCollect.htSpotDefectInter,
										 ei.dateBase,
										 datePeriod,
										 alfDocAllocate.getContentStream().getStream(),
										 fnd);
			}else if(isLocalBankHQNormal){		//本銀總行一般
				hMap = pd.LBInternalBefore(arrISDoc,	
						 				tempCollect.htSpotDefectInter,
						 				ei.dateBase,
						 				datePeriod,
						 				ei.bankName,
						 				ei.reportNo,
						 				alfDocAllocate.getContentStream().getStream(),
						 				fnd);
			}else{
				hMap = pd.InternalBefore(arrISDoc,
									tempCollect.htSpotDefectInter,
									ei.dateBase,
									datePeriod,
									ei.bankName,
									ei.reportNo,
									alfDocAllocate.getContentStream().getStream(),
									fnd);
			}
			if(hMap.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業內部版實地檢查缺失情形表產出數量為零。");
				}else{
					throw new Exception("內部版實地檢查缺失情形表產出數量為零。");
				}
			}
			//
			Iterator<String> it3 = hMap.keySet().iterator();
			while(it3.hasNext()){
				String fileName = it3.next();
				if(!fileName.contains("*")){
					ByteArrayOutputStream bo = hMap.get(fileName);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					//寫檔
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
					//文件是否需經流程
					if(ei.preMeetEnableWorkFlow == 1){		
						props = new HashMap<String, String>();
						props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
						alfDoc2.updateProperties(props);
					}
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//產出內部版實地檢查缺失情形表(一般與資訊作業共用)草稿
	public void genSpotDefectReportInterDraft(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			boolean isInfoExam = false;				//是否為資訊檢查作業
			boolean isLocalBankHQNormal = false;		//是否為本銀總行	一般檢查
			if(actionRequest.getParameter("infoExam") != null && actionRequest.getParameter("infoExam").equalsIgnoreCase("true")){
				isInfoExam = true;
			}else if(actionRequest.getParameter("localBankHQNormal") != null && actionRequest.getParameter("localBankHQNormal").equalsIgnoreCase("true")){
				isLocalBankHQNormal = true;
			}
			String folderIdDraft = actionRequest.getParameter("folderId");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			AlfrescoFolder fdDraft = (AlfrescoFolder)alfSessionAdmin.getObject(folderIdDraft);
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出內部版實地檢查缺失情形表草稿", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = new Hashtable<String, String>();
			htNames = this.getStageFileNames(fdDraft);
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
	        			Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
	        			while(it2.hasNext()){
	        				CmisObject co2 = it2.next();
	        				if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        						co2.getName().toLowerCase().endsWith("json") &&
	        						!co2.getName().startsWith("_del#")){
	        					alfDocAllocate = (AlfrescoDocument)co2;
	        					break;
	        				}
	        			}
	        			break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");			
			//彙整表 InputStream 集合
			ArrayList<InputStream> arrISDoc = new ArrayList<InputStream>(); 
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						(co.getName().contains("彙整表") || (co.getName().contains("報告表") && co.getName().contains("領隊"))) &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
					arrISDoc.add(((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(arrISDoc.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業於目前目錄內查無重要查核項目彙整表。");
				}else{
					throw new Exception("目前目錄內查無重要查核項目彙整表。");
				}
			}
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.htSpotDefectInter.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業查無內部版實地檢查缺失情形表之空白範本表格。");
				}else{
					throw new Exception("查無內部版實地檢查缺失情形表之空白範本表格。");
				}
			}else{
				/*
				String sss = "";
				Iterator<String> __it = tempCollect.htSpotDefectInter.keySet().iterator();
				while(__it.hasNext()){
					String s = __it.next();
					if(!sss.equals("")) sss+= "#";
					sss += s;
				}
				if(!sss.equals("")){
					throw new Exception(sss);
				}
				*/
			}
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			fnd.fullBankName = ei.bankName;
			
			//HashMap Key: 報表名稱, value: Word 報表
			String datePeriod = ei.dateStart + " 至 " + ei.dateEnd;
			HashMap<String, ByteArrayOutputStream> hMap = null;
			if(isInfoExam){		//如果是資訊作業
				hMap = pd.CheckDefectReport(arrISDoc.get(0),	//應該只有一個檔
										 tempCollect.htSpotDefectInter,
										 ei.dateBase,
										 datePeriod,
										 alfDocAllocate.getContentStream().getStream(),
										 fnd);
			}else if(isLocalBankHQNormal){		//本銀總行一般
				hMap = pd.LBInternalBefore(arrISDoc,	
						 				tempCollect.htSpotDefectInter,
						 				ei.dateBase,
						 				datePeriod,
						 				ei.bankName,
						 				ei.reportNo,
						 				alfDocAllocate.getContentStream().getStream(),
						 				fnd);
			}else{
				hMap = pd.InternalBefore(arrISDoc,
									tempCollect.htSpotDefectInter,
									ei.dateBase,
									datePeriod,
									ei.bankName,
									ei.reportNo,
									alfDocAllocate.getContentStream().getStream(),
									fnd);
			}
			if(hMap.size() == 0){
				if(isInfoExam){
					throw new Exception("資訊作業內部版實地檢查缺失情形表產出數量為零。");
				}else{
					throw new Exception("內部版實地檢查缺失情形表產出數量為零。");
				}
			}
			//
			Iterator<String> it3 = hMap.keySet().iterator();
			while(it3.hasNext()){
				String fileName = it3.next();
				if(!fileName.contains("*")){
					ByteArrayOutputStream bo = hMap.get(fileName);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					//如果是草稿的話
					int idx = fileName.lastIndexOf(".");
					fileName = fileName.substring(0, idx) + "草稿" + fileName.substring(idx);
					//寫檔
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdDraft.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
			//草稿區檔案存入Session
			Vector<StageFile> vecDraft = new Vector<StageFile>();
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					AlfrescoDocument doc = (AlfrescoDocument)co;
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}else if(doc.getPropertyValue("cm:description") != null) {
						if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
							sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
						}
					}
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					//if(!ef.leaderName.contains(sf.author) && ef.staffs.contains(sf.author)) sf.author = ef.leaderName;
					
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdDraft.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
									
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					//
					vecDraft.add(sf);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecDraft", vecDraft);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/draftArea.jsp");
			actionResponse.setRenderParameter("folderId", fdDraft.getId());			
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	
	//產出外部版實地檢查缺失情形表
	public void genSpotDefectReportOuter(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出外部版實地檢查缺失情形表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//內部版缺失情形表 InputStream 集合
			HashMap<String, InputStream> htISDoc = new HashMap<String, InputStream>(); 
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("內部") &&
						co.getName().contains("缺失情形") &&
						co.getName().contains("檢討會前") &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
					String pureName = co.getName().substring(0, co.getName().lastIndexOf("."));
					htISDoc.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(htISDoc.size() == 0) throw new Exception("目前目錄內查無內部版實地檢查缺失情形表。");
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//報告陳核階段
			AlfrescoFolder fdPost = null;
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null;
			it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
	        				while(it2.hasNext()){
	        					CmisObject co2 = it2.next();
	        					if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        						co2.getName().toLowerCase().endsWith("json") &&
	        						!co2.getName().startsWith("_del#")){
	        						alfDocAllocate = (AlfrescoDocument)co2;
	        						break;
	        					}
	        				}
	        			}else if(co.getName().contains("陳核")){
	        				fdPost = (AlfrescoFolder)co;
	        			}
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");			
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.htSpotDefectOuter.size() == 0) throw new Exception("查無外部版實地檢查缺失情形表之空白範本表格。");
			//
			boolean isLocalBankHQNormal = false;		//是否為本銀總行	一般檢查
			if(actionRequest.getParameter("localBankHQNormal") != null && actionRequest.getParameter("localBankHQNormal").equalsIgnoreCase("true")){
				isLocalBankHQNormal = true;
			}
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			//HashMap Key: 報表名稱, value: Word 報表
			String datePeriod = ei.dateStart + " 至 " + ei.dateEnd;
			boolean showDefectBisTitle = true;
			if(ei.defectWithBisTitle == 0) showDefectBisTitle = false;
			HashMap<String, ByteArrayOutputStream> hMap = null;
			if(isLocalBankHQNormal){
				hMap = pd.LBExternalReport(htISDoc,
								tempCollect.htSpotDefectOuter,
								showDefectBisTitle,
								ei.dateBase,
								datePeriod,
								alfDocAllocate.getContentStream().getStream(),
								ei.reportNo, fnd);
			}else{
				hMap = pd.ExternalReport(htISDoc,
								tempCollect.htSpotDefectOuter,
								showDefectBisTitle,
								ei.dateBase,
								datePeriod,
								ei.bankName,
								ei.reportNo,
								alfDocAllocate.getContentStream().getStream(),
								fnd);
			}
			if(hMap.size() == 0) throw new Exception("無法產出外部版實地檢查缺失情形表。");
			//
			Iterator<String> it3 = hMap.keySet().iterator();
			while(it3.hasNext()){
				String fileName = it3.next();
				if(!fileName.contains("*")){
					ByteArrayOutputStream bo = hMap.get(fileName);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					//寫檔
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
					//
					if(!fileName.toLowerCase().endsWith("doc")) fileName +=".doc";
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
					//如果含有 “面請_評等” 者, 將檔案移至 報告陳核 階段
					if(fdPost != null && fileName.contains("面請_評等")){
						it = fdPost.getChildren().iterator();
					 	while(it.hasNext()){
					 		CmisObject co = it.next();
					 		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
					 				co.getName().equalsIgnoreCase(alfDoc2.getName())){
					 			co.delete();
					 			break;
					 		}
					 	}
					 	alfDoc2.move(fdStage, fdPost);
					}
					//文件是否需經流程
					if(ei.preMeetEnableWorkFlow == 1){
						props = new HashMap<String, String>();
						props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
						alfDoc2.updateProperties(props);
					}
				}
			}
			conn.close();
			//是否發送電子郵件通知
			
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//產出會後版實地檢查缺失情形表
	public void genSpotDefectReportAfter(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出會後版實地檢查缺失情形表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//找出檢查行前階段
			//找出檢討會後目錄
			AlfrescoFolder fdPreMeet = null;
        		Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && 
	        				co.getName().contains("會前")){
	        			fdPreMeet = (AlfrescoFolder)co;
	        			break;
	        		}
	        	}
	        	//
			if(fdPreMeet == null) throw new Exception("找不到檢討前後所在目錄。");

			//檢討會後內部版缺失情形表 InputStream 集合
			HashMap<String, InputStream> htISDoc = new HashMap<String, InputStream>(); 
			it = fdPreMeet.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("內部") &&
						co.getName().contains("缺失情形") &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
					String pureName = co.getName().substring(0, co.getName().lastIndexOf("."));
					htISDoc.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(htISDoc.size() == 0) throw new Exception("檢討會前查無內部版實地檢查缺失情形表。");
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.htSpotDefectAfter.size() == 0) throw new Exception("查無檢討會後內部版實地檢查缺失情形表之空白範本表格。");
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			//HashMap Key: 報表名稱, value: Word 報表
			HashMap<String, ByteArrayOutputStream> hMap = pd.InternalAfter(htISDoc,
																	tempCollect.htSpotDefectAfter,
																	ei.bankName,
																	ei.reportNo, fnd);
			//
			Iterator<String> it3 = hMap.keySet().iterator();
			while(it3.hasNext()){
				String fileName = it3.next();
				if(!fileName.contains("*")){
					ByteArrayOutputStream bo = hMap.get(fileName);
					InputStream is = new ByteArrayInputStream(bo.toByteArray());
					//寫檔
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
					//
					if(!fileName.toLowerCase().endsWith("doc")) fileName += ".doc";
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
					//文件是否需經流程
					if(ei.preMeetEnableWorkFlow == 1){
						props = new HashMap<String, String>();
						props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
						alfDoc2.updateProperties(props);
					}
				}
			}
			conn.close();
			//是否發送電子郵件通知
			
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//產出會後版實地檢查缺失情形表(呈核用)
	public void genSpotDefectReportAfterTrimCol(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出呈核用會後版實地檢查缺失情形表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);

			//檢討會後內部版缺失情形表 InputStream 集合
			HashMap<String, InputStream> hmIS = new HashMap<String, InputStream>(); 
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("缺失情形") &&
						co.getName().toLowerCase().endsWith("doc") && 
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") ){
					String pureName = co.getName().substring(0, co.getName().lastIndexOf("."));
					hmIS.put(co.getName(), ((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(hmIS.size() == 0) throw new Exception("尚未產出實地檢查缺失情形表。");
			//
			ParseDoc pd = new ParseDoc();
			//HashMap Key: 報表名稱, value: Word 報表
			HashMap<String, ByteArrayOutputStream> hMap = pd.TransformWordToPrint(hmIS, "2");
			//
			Iterator<String> it2 = hMap.keySet().iterator();
			while(it2.hasNext()){
				String fileName = it2.next();
				ByteArrayOutputStream bo = hMap.get(fileName);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				//寫檔
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
				props.put("cm:author", userFullName);

				ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
				//
				if(!fileName.toLowerCase().endsWith("doc")) fileName += ".doc";
				//
				AlfrescoDocument alfDoc2 = null;
				if(htNames.containsKey(fileName)){
					String updateMemo = "重新產出";
					props.put("it:updateMemo", updateMemo);
						
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}else{
					props.put(PropertyIds.NAME, fileName);
					alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
				}
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	
	//複製檢討會前之實地檢查缺失情形表至檢討會後
	public void copyDefectReport2EndMeet(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("複製檢討會前之實地檢查缺失情形表至檢討會後", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出開會用檢查缺失情形表
			Vector<AlfrescoDocument> vecDoc = new Vector<AlfrescoDocument>();
			//
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("缺失情形表") && 
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")) {
					vecDoc.add((AlfrescoDocument)co);
				}
			}
			if(vecDoc.size() == 0) throw new Exception("目前目錄內查無檢討會用實地檢查缺失情形表。");
			
			//找出檢討會後目錄
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			
			AlfrescoFolder fdEndMeet = null;
        		it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("會後")){
	        			fdEndMeet = (AlfrescoFolder)co;
	        			break;
	        		}
	        	}
	        	//
			if(fdEndMeet == null) throw new Exception("找不到檢討會後所在目錄。");
	        	//階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdEndMeet);
			//複製檔案至檢討會後
			for(AlfrescoDocument f: vecDoc){
				String fileName = f.getName();
				if(!fileName.contains("*")){
					String updateMemo = "重新複製";
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
					props.put(PropertyIds.NAME, f.getName());
					props.put("cm:author", userFullName);
						
					InputStream is = f.getContentStream().getStream();
					ContentStream cs = new ContentStreamImpl(fileName, null, f.getContentStreamMimeType(), is);
					//
					AlfrescoDocument alfDoc = null;
					if(htNames.containsKey(fileName)){
						props.put("it:updateMemo", updateMemo);
						//
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						alfDoc = (AlfrescoDocument)fdEndMeet.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//將實地檢查缺失情形表移至評等審議階段(本銀總行適用)
	public void moveDefectReport2Eval(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("將實地檢查缺失情形表移至評等審議階段", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出開會用檢查缺失情形表
			Vector<AlfrescoDocument> vecDoc = new Vector<AlfrescoDocument>();
			//
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("缺失情形表") && 
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")) {
					vecDoc.add((AlfrescoDocument)co);
				}
			}
			if(vecDoc.size() == 0) throw new Exception("目前目錄內查無實地檢查缺失情形表。");
			
			//找出評等審議階段目錄
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			
			AlfrescoFolder fdEval = null;
        		it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("評等")){
	        			fdEval = (AlfrescoFolder)co;
	        			break;
	        		}
	        	}
	        	//
			if(fdEval == null) throw new Exception("找不到評等審議階段所在目錄。");
	        	//階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdEval);
			//複製檔案至檢討會後
			for(AlfrescoDocument f: vecDoc){
				String fileName = f.getName();
				if(!fileName.contains("*")){
					AlfrescoDocument alfDoc = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新移入";
						Map<String, String> props = new HashMap<String, String>();
						props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
						//props.put(PropertyIds.NAME, fileName);
						props.put("cm:author", userFullName);
						props.put("it:updateMemo", updateMemo);
						//
						InputStream is = f.getContentStream().getStream();
						ContentStream cs = new ContentStreamImpl(fileName, null, f.getContentStreamMimeType(), is);
						//
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						f.move(fdStage, fdEval);
					}
				}
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//將保外組內參事項表移至報告陳核階段
	public void mergeInterState2Post(ActionRequest actionRequest, ActionResponse actionResponse) throws Throwable {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("將保外組內參事項表移至報告陳核階段", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出檢查結束階段
			AlfrescoFolder fdEndExam = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("結束")){
	        				fdEndExam = (AlfrescoFolder)co;
	        				break;
	        			}
	        		}
	        	}
			if(fdEndExam == null) throw new Exception("找不到檢查結束階段。");
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//找出助檢版內部參考事項表
			int countUnApproved = 0;
			String unApprovedNames = "";
			HashMap<String, InputStream> htDocIS = new HashMap<String, InputStream>();
			//
			it = fdEndExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					if(co.getName().contains("內部參考事項") && 
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")) {
						String pureName = co.getName().substring(0, co.getName().lastIndexOf("."));
						Document doc = (Document)co;
						if(doc.getPropertyValue("it:docStatus") != null){
							if(doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_DRAFT) || 
								doc.getPropertyValue("it:docStatus").equals(ErpUtil.STATUS_APPROVING)){
								countUnApproved++;
								if(!unApprovedNames.equals("")) unApprovedNames += ",";
								unApprovedNames += doc.getName();
							}else if(!doc.getName().startsWith("_del#")){
								htDocIS.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
							}
						}else if(!co.getName().startsWith("_del#")){
							htDocIS.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
						}
					}
				}
			}
			if(countUnApproved > 0) throw new Exception("檢查結束階段仍有 " + unApprovedNames + " 等 " + countUnApproved + " 個檔案待審核。");
			//if(htDocIS.size() == 0 && !ei.isSpecialExam) throw new Exception("檢查結束階段無本會內部參考事項表。");

			//找出檢查行前, 報告陳核階段目錄
			AlfrescoFolder fdPrepare = null;
			AlfrescoFolder fdPreMeet = null;
			//
        		it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				fdPrepare = (AlfrescoFolder)co;
	        			}else if(co.getName().contains("會前")){
	        				fdPreMeet = (AlfrescoFolder)co;
	        			}
	        		}
	        	}
	        	//
			if(fdPreMeet == null) throw new Exception("找不到檢討會前階段。");
			if(fdPrepare == null) throw new Exception("找不到檢查行前階段。");
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null; 
			it = fdPrepare.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        					co.getName().toLowerCase().endsWith("json") &&
	        					!co.getName().startsWith("_del#")){
        				alfDocAllocate = (AlfrescoDocument)co;
        				break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
	        	
			//找出領隊版檢查重點彙整表
			AlfrescoDocument alfDocPoint = null;
			
			it = fdPreMeet.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					if(co.getName().contains("檢查重點") && 
							co.getName().contains("彙整表") &&
							!co.getName().startsWith("列印用") &&
							!co.getName().startsWith("_del#") &&
							co.getName().toLowerCase().endsWith("doc")){
						alfDocPoint = (AlfrescoDocument)co;
					}
				}
			}
			if(alfDocPoint == null) throw new Exception("檢討會前階段查無檢查重點彙整表");
			
	        	//找出業別報告表格(在報告陳核, 可為空值)
	        	AlfrescoDocument alfDocReport = null;
	        	it = fdStage.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        					co.getName().toLowerCase().endsWith("xls")){
	        			alfDocReport = (AlfrescoDocument)co;
        				break;
	        		}
	        	}
	        	
	        	//報告陳核階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//
			ColumnValue cv = new ColumnValue();
			cv.base_date = ei.dateBase;
			cv.checkType = ei.examType;
			cv.durationDays = String.valueOf(ei.examDays);		//檢查天數
			cv.reviewDate = ei.dateMeeting;					//檢討會議日期
			cv.reportId = ei.reportNo;
			cv.startDate = ei.dateStart;
			cv.endDate = ei.dateEnd;
			cv.leader = ei.leaderName;
			cv.organization = ei.bankName;
			//
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.htInsForeState.size() == 0) throw new Exception("查無「內部參考事項」之範本檔案。");
			//
			boolean isNormalProject = false;
			if(!ei.isSpecialExam){
				isNormalProject = true;
			}
			//
			InputStream streamReport = null;
			if(alfDocReport != null) streamReport = alfDocReport.getContentStream().getStream();
			//
			ParseDoc pd = new ParseDoc();
			
			FileNameData fnd = new FileNameData();
			fnd.certNumber = ei.examNo;
			fnd.CheckBankName = ei.bankNameShort;
			fnd.CheckNumber = ei.reportNo;
			
			//HashMap Key: 報表名稱, value: Word 報表
			HashMap<String, ByteArrayOutputStream> hMap = pd.MergeExamComment(alfDocPoint.getContentStream().getStream(),
																		cv,
																		tempCollect.htInsForeState,
																		htDocIS,
																		isNormalProject,
																		streamReport,
																		alfDocAllocate.getContentStream().getStream(),
																		ei.bankName,
																		ei.reportNo,
																		fnd); 
			if(hMap.size() == 0) throw new Exception("產出的內部參考事項表格數為零。");
			//寫至報告陳核階段
			Iterator<String> it2 = hMap.keySet().iterator();
			while(it2.hasNext()){
				String key = it2.next();
				//
				ByteArrayOutputStream bo = hMap.get(key);
				InputStream is = new ByteArrayInputStream(bo.toByteArray());
				//寫檔
				String fileName = key;
				if(!fileName.contains("*")){
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					props.put("cm:author", userFullName);
	
					MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
					ContentStream cs = new ContentStreamImpl(fileName, null, fileTypeMap.getContentType(tempCollect.fPointDocAssistant.getName()), is);
					//
					AlfrescoDocument alfDoc2 = null;
					if(htNames.containsKey(fileName)){
						String updateMemo = "重新產出";
						props.put("it:updateMemo", updateMemo);
						
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
						if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
						//
						ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
						Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
						ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
						alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					}else{
						props.put(PropertyIds.NAME, fileName);
						alfDoc2 = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
					}
				}
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	
	//產出檢查意見210N (非本銀總行)
	public void genExamOpinion210N(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出檢查意見.210N檔", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			//boolean isLocalBankHQNormal = false;
			//if(actionRequest.getPortletSession(true).getAttribute("isLocalBankHQNormal") != null){
			//	isLocalBankHQNormal = Boolean.valueOf(actionRequest.getPortletSession(true).getAttribute("isLocalBankHQNormal").toString());
			//}
			//if(isLocalBankHQNormal){		//本銀總行
			//	this.genExamOpinion210N4LocalBank(alfSessionAdmin, stageFolderId, conn, themeDisplay, userFullName);
			//}else{
				this.genExamOpinion210N4Normal(alfSessionAdmin, collec.stageFolderId, conn, themeDisplay, userFullName);
			//}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//非本銀總行的檢查意見
	private void genExamOpinion210N4Normal(Session alfSessionAdmin, 
										String stageFolderId, 
										Connection conn, 
										ThemeDisplay themeDisplay, 
										String userFullName) throws Exception{
		AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
		//找出檢討會後階段
		AlfrescoFolder fdEndMeet = null;
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
        	while(it.hasNext()){
        		CmisObject co = it.next();
        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
        			if(co.getName().contains("會後")){
        				fdEndMeet = (AlfrescoFolder)co;
        				break;
        			}
        		}
        	}
		if(fdEndMeet == null) throw new Exception("找不到檢討會後階段。");
		
		//彙整表 InputStream 集合
		HashMap<String, InputStream> hmISDoc = new HashMap<String, InputStream>(); 
		it = fdEndMeet.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("缺失情形表") &&
						co.getName().contains("內部") && 
						!co.getName().contains("面請") &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
				String pureName = co.getName().substring(0, co.getName().lastIndexOf("."));
				hmISDoc.put(pureName, ((AlfrescoDocument)co).getContentStream().getStream());
			}
		}
		if(hmISDoc.size() == 0) throw new Exception("檢討會後階段查無內部版實地檢查缺失情形表。");
		
		//本階段已存在的檔案, (檔名,Node ID)
		Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
		
		//取得檢查案結構資訊
		ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		//查詢範本
		TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
		if(tempCollect.examOpinion210 == null) throw new Exception("查無「檢查意見.210N」之空白範本表格。");
		//
		FileNameData fnd = new FileNameData();
		fnd.CheckNumber = ei.reportNo;
		//
		ParseDoc pd = new ParseDoc();
		ByteArrayOutputStream bos = pd.ExamReport210(hmISDoc, tempCollect.examOpinion210, false, fnd); 
		//
		InputStream is = new ByteArrayInputStream(bos.toByteArray());
		//寫檔
		String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查意見.210N.doc";
		
		ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
		//
		String updateMemo = "重產檢查意見";
		Map<String, String> props = new HashMap<String, String>();
		props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
		props.put("cm:author", userFullName);
		//
		AlfrescoDocument alfDoc = null;
		if(htNames.containsKey(fileName)){
			props.put("it:updateMemo", updateMemo);
			//
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
			if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
			alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
		}else{
			props.put(PropertyIds.NAME, fileName);
			alfDoc = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
		}
		//是否發送電子郵件通知
		
	}
	
	//產出面請意見910N
	public void genExamFace910N(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出面請意見.910N檔", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出檢討會後階段
			AlfrescoFolder fdEndMeet = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("會後")){
	        				fdEndMeet = (AlfrescoFolder)co;
	        				break;
	        			}
	        		}
	        	}
			if(fdEndMeet == null) throw new Exception("找不到檢討會後階段。");
			
			//彙整表 InputStream 集合
			ArrayList<InputStream> arrISDoc = new ArrayList<InputStream>(); 
			it = fdEndMeet.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("缺失情形表") &&
						co.getName().contains("內部") && 
						co.getName().contains("面請") && 
						!co.getName().startsWith("列印用") &&
						!co.getName().contains("_del#") && 
						co.getName().toLowerCase().endsWith("doc")){
					arrISDoc.add(((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(arrISDoc.size() == 0) throw new Exception("檢討會後階段內查無內部版面請改善缺失表。");
			if(arrISDoc.size() > 1) throw new Exception("檢討會後階段內包含 " + arrISDoc.size() + " 個面請改善缺失表，請合併後再執行。");
			
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);

			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.examFace910 == null) throw new Exception("查無本「面請改善缺失事項.910N」之空白範本表格。");
			//
			ParseDoc pd = new ParseDoc();
			//
			ByteArrayOutputStream bos = pd.ExamReportFace(arrISDoc.get(0), tempCollect.examFace910); 
			//
			InputStream is = new ByteArrayInputStream(bos.toByteArray());
			//寫檔
			String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "面請改善缺失事項.910N.doc";
			//
			ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
			//
			String updateMemo = "重產面請改善缺失事項";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			AlfrescoDocument alfDoc = null;
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			//
			if(tempCollect.examFace910N2 != null){		//評等面請意見轉銀行局央行與存保
				bos = pd.ExamReportFace(arrISDoc.get(0), tempCollect.examFace910N2); 
				//
				is = new ByteArrayInputStream(bos.toByteArray());
				//寫檔
				fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "面請改善缺失事項.910N2.doc";
				//
				cs = new ContentStreamImpl(fileName, null, "application/msword", is);
				//
				updateMemo = "重產面請改善缺失事項910N2";
				props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
				props.put("cm:author", userFullName);
				
				alfDoc = null;
				if(htNames.containsKey(fileName)){
					props.put("it:updateMemo", updateMemo);
					//
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
				}else{
					props.put(PropertyIds.NAME, fileName);
					alfDoc = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
				}	
			}
			//
			conn.close();
			//是否發送電子郵件通知

		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//將意見檔案移至報告陳核階段
	@SuppressWarnings("resource")
	public void moveOpinionFiles2Post(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//找出報告陳核階段
			AlfrescoFolder fdPost = null;
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ei.folderIdExam);
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("將意見檔案移至報告陳核階段", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("陳核")){
					fdPost = (AlfrescoFolder)co;
				}
			}
			if(fdPost == null) throw new Exception("找不到報告陳核階段。");
			//	
			//陳核階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdPost);
			//意見表陣列
			Vector<AlfrescoDocument> vecDoc = new Vector<AlfrescoDocument>(); 
			it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						(co.getName().contains("檢查意見.210N") || co.getName().contains("面請改善缺失事項.910N")) && 
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")){
					vecDoc.add((AlfrescoDocument)co);
				}
			}
			if(vecDoc.size() == 0) throw new Exception("目前目錄內查無檢查意見.210N或面請改善缺失事項.910N等檔案。");
			//開始搬移
			for(AlfrescoDocument doc: vecDoc){
				String fileName = doc.getName();
				if(htNames.containsKey(fileName)){
					String updateMemo = fdStage.getName() + "重新移入";
					//
					Map<String, String> props = new HashMap<String, String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
					//props.put(PropertyIds.NAME, fileName);
					props.put("cm:author", userFullName);
					props.put("it:updateMemo", updateMemo);
					AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					ObjectId objectId = pwc.checkIn(false, props, doc.getContentStream(), updateMemo);
					alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
					//
					doc.delete();
				}else{
					doc.move(fdStage, fdPost);
				}
			}
			conn.close();
			//是否發送電子郵件通知
			
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	
	//產出意見分級統計表
	public void genExamOpinionStatistic(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出意見分級統計表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//彙整表 InputStream 集合
			ArrayList<InputStream> arrISDoc = new ArrayList<InputStream>(); 
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						co.getName().contains("檢查意見.210N") &&
						co.getName().toLowerCase().endsWith("doc") &&
						!co.getName().startsWith("_del#") &&
						!co.getName().startsWith("列印用")){
					arrISDoc.add(((AlfrescoDocument)co).getContentStream().getStream());
				}
			}
			if(arrISDoc.size() == 0) throw new Exception("目前目錄內查無檢查意見表。");
			if(arrISDoc.size() > 1) throw new Exception("目前目錄內包含 " + arrISDoc.size() + " 個檢查意見表，請合併後再執行。");
			//
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.examOpinionStatistic == null) throw new Exception("查無本類型檢查案件「檢查意見分級統計表」之空白範本表格。");
			//
			ParseDoc pd = new ParseDoc();
			//
			ByteArrayOutputStream bos = pd.ExamReportClassification(arrISDoc.get(0),
															tempCollect.examOpinionStatistic,
															ei.bankName,
															ei.reportNo);
			//
			InputStream is = new ByteArrayInputStream(bos.toByteArray());
			//寫檔
			String fileName = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查意見分級統計表.210N2.doc";
			
			ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
			//
			String updateMemo = "重產檢查意見分級統計表";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			AlfrescoDocument alfDoc = null;
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			//將檔案移至報告陳核階段
			AlfrescoFolder fdPost = null;
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ei.folderIdExam);
			it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("陳核")){
					fdPost = (AlfrescoFolder)co;
				}
			}
			if(fdPost == null) throw new Exception("找不到報告陳核階段。");
			alfDoc.move(fdStage, fdPost);
			//文件是否需經流程
			//if(ei.postEnableWorkFlow == 1){
			//	Map<String, String> props = new HashMap<String, String>();
			//	props.put("it:docStatus", ErpUtil.STATUS_DRAFT);
			//	alfDoc.updateProperties(props);
			//}
			conn.close();
			//是否發送電子郵件通知
			
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//產出檢查報告目錄與封面
	public void genExamReportIndexAndCover(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出檢查報告目錄與封面", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.examReportIndex == null) throw new Exception("查無「檢查報告目錄」之範本檔案。");
			if(tempCollect.fExamCover == null) throw new Exception("查無「檢查報告封面」之範本檔案。");
			//
			ColumnValue cv = new ColumnValue();
			cv.base_date = ei.dateBase;
			cv.checkType = ei.examType;
			cv.durationDays = String.valueOf(ei.examDays);		//檢查天數
			cv.reviewDate = ei.dateMeeting;					//檢討會議日期
			cv.reportId = ei.reportNo;
			cv.startDate = ei.dateStart;
			cv.endDate = ei.dateEnd;
			cv.leader = ei.leaderName;
			cv.organization = ei.bankName;
			//
			ParseDoc pd = new ParseDoc();
			//
			ByteArrayOutputStream bosIndex = pd.Catalog(cv, tempCollect.examReportIndex);
			ByteArrayOutputStream bosCover = pd.MakeCover(tempCollect.fExamCover.getContentStream().getStream(), 
														ei.bankName, 
														ei.reportNo);
			//
			InputStream isIndex = new ByteArrayInputStream(bosIndex.toByteArray());
			InputStream isCover = new ByteArrayInputStream(bosCover.toByteArray());
			//寫檔
			String fileNameIndex = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查報告目錄.000.doc";
			String fileNameCover = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查報告封面.doc";
			
			ContentStream csIndex = new ContentStreamImpl(fileNameIndex, null, "application/msword", isIndex);
			ContentStream csCover = new ContentStreamImpl(fileNameCover, null, "application/msword", isCover);
			//產出目錄
			String updateMemo = "重新產出";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			AlfrescoDocument alfDoc = null;
			if(htNames.containsKey(fileNameIndex)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileNameIndex));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, csIndex, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileNameIndex);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, csIndex, VersioningState.MAJOR);
			}

			//產出封面
			updateMemo = "重新產出";
			props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			alfDoc = null;
			if(htNames.containsKey(fileNameCover)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileNameCover));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, csCover, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileNameCover);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, csCover, VersioningState.MAJOR);
			}
			
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//產出處理流程表
	public void genProcessFlowAndBisItem(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//Log
			String _path = fdExam.getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出處理流程表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
	        			Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
	        			while(it2.hasNext()){
	        				CmisObject co2 = it2.next();
	        				if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        						co2.getName().toLowerCase().endsWith("json") &&
	        						!co2.getName().startsWith("_del#")){
	        					alfDocAllocate = (AlfrescoDocument)co2;
	        					break;
	        				}
	        			}
	        			break;
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
			
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.fProcessFlow == null) throw new Exception("查無「檢查報告處理流程」之範本檔案。");
			if(tempCollect.fAllocateExamItemPerBis == null) throw new Exception("查無「派差作業各業別檢查項目表」之範本檔案。");
			//
			AgendaValue av = new AgendaValue();
			av.BaseDate = ei.dateBase;
			av.EndDate = ei.dateEnd;
			av.ExamCertificate = ei.dateApproval;
			av.ExamNumber = ei.examNo;
			av.General = !ei.isSpecialExam;
			av.LeaderName = ei.leaderName;
			av.organization = ei.bankName;
			av.project = ei.caseName;
			av.ReportId = ei.reportNo;
			av.reviewDate = ei.dateMeeting;
			av.StartDate = ei.dateStart;
			//
			ParseDoc pd = new ParseDoc();
			//
			ByteArrayOutputStream bosFlow = pd.MakeExamReportAgendaForm(tempCollect.fProcessFlow.getContentStream().getStream(),
																		av,
																		alfDocAllocate.getContentStream().getStream());
			ByteArrayOutputStream bosBisItem = pd.BusinessItemForm(tempCollect.fAllocateExamItemPerBis.getContentStream().getStream(),
																	ei.bankName,
																	ei.leaderName,
																	ei.reportNo,
																	alfDocAllocate.getContentStream().getStream()); 
			//
			InputStream isFlow = new ByteArrayInputStream(bosFlow.toByteArray());
			InputStream isBisItem = new ByteArrayInputStream(bosBisItem.toByteArray());
			//寫檔
			String fileNameFlow = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "檢查報告處理流程表.doc";
			String fileNameBisItem = ei.reportNo + "_" + ei.bankNameShort + "_" + ei.examNo + "_" + "派差作業各業別檢查項目表.doc";
			
			ContentStream csFlow = new ContentStreamImpl(fileNameFlow, null, "application/msword", isFlow);
			ContentStream csBisItem = new ContentStreamImpl(fileNameBisItem, null, "application/msword", isBisItem);
			
			//產出檢查報告處理流程表
			String updateMemo = "重新產出";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			AlfrescoDocument alfDoc = null;
			if(htNames.containsKey(fileNameFlow)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileNameFlow));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, csFlow, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileNameFlow);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, csFlow, VersioningState.MAJOR);
			}

			//檢查報告處理流程表
			updateMemo = "重新產出";
			props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			alfDoc = null;
			if(htNames.containsKey(fileNameBisItem)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileNameBisItem));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, csBisItem, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileNameBisItem);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, csBisItem, VersioningState.MAJOR);
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//產出檢查提要
	public void genExamBriefSummary(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//
			//檢查提要初稿檔
			String fileId = actionRequest.getParameter("fileId");
			AlfrescoDocument alfDocBriefDraft = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("產出檢查提要", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			//取得工作分配表
			AlfrescoDocument alfDocAllocate = null;
			
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("行前")){
					Iterator<CmisObject> it2 = ((AlfrescoFolder)co).getChildren().iterator();
					while(it2.hasNext()){
						CmisObject co2 = it2.next();
						if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
												co2.getName().toLowerCase().endsWith("json") &&
												!co2.getName().startsWith("_del#")){
							alfDocAllocate = (AlfrescoDocument)co2;
							break;
						}
					}
				}
			}
			if(alfDocAllocate == null) throw new Exception("找不到工作分配表。");
			
			//Search 目錄內是否有"報告表格"的excel檔
			AlfrescoDocument alfExcelReport = null;
			it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getName().contains("報告表格") && co.getName().toLowerCase().endsWith("xls")){
					alfExcelReport = (AlfrescoDocument)co;
					break;
				}
			}
			//
			InputStream isExcelReport = null;
			if(alfExcelReport != null) isExcelReport = alfExcelReport.getContentStream().getStream();
			//
			ParseDoc pd = new ParseDoc();
			ByteArrayOutputStream bos = pd.ExamSummary(isExcelReport,
													alfDocBriefDraft.getContentStream().getStream(),
													alfDocAllocate.getContentStream().getStream());
			if(bos == null) throw new Exception("檢查提要檔產出函式回傳為空值。");
			//
			InputStream is = new ByteArrayInputStream(bos.toByteArray());
			//寫檔
			String fileName = collec.reportNo + "_" + collec.bankNameShort + "_" + collec.examNo + "_" + "檢查提要.110.doc";
			
			ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", is);
			//
			String updateMemo = "重產檢查提要";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			AlfrescoDocument alfDoc = null;
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
			}else{
				props.put(PropertyIds.NAME, fileName);
				alfDoc = (AlfrescoDocument)fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//檢查重要查核項目報告表是否填妥
	public void checkFormCompleted(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//判斷檢查哪一種報告表(檢查重點或必查項目)
			String formType = actionRequest.getParameter("formType");
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//Log
			String _path = fdStage.getParents().get(0).getName() + "/" + fdStage.getName();
			ErpUtil.logUserAction("驗證重要查核項目報告表", _path, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//找出重要查核項目報告表
			ArrayList<InputStream> arrIS = new ArrayList<InputStream>();
			//
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(formType.equals("檢查重點")){
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
						(co.getName().contains("檢查重點") || co.getName().contains("重要查核")) &&
						!co.getName().startsWith("列印用") &&
						!co.getName().startsWith("_del#") &&
						co.getName().toLowerCase().endsWith("doc")) {
						arrIS.add(((AlfrescoDocument)co).getContentStream().getStream());
					}
				}else if(formType.equals("必查項目")){
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
							co.getName().contains("必查項目") && 
							!co.getName().startsWith("列印用") &&
							!co.getName().startsWith("_del#") &&
							co.getName().toLowerCase().endsWith("doc")) {
							arrIS.add(((AlfrescoDocument)co).getContentStream().getStream());
					}
				}
			}
			if(arrIS.size() == 0) throw new Exception("目前目錄內查無助檢版報告表。");
			//找出工作分配表
			AlfrescoDocument alfDocAllocate = null;
	        	AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			it = fdExam.getChildren().iterator();
	        	while(it.hasNext()){
	        		CmisObject co = it.next();
	        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
	        			if(co.getName().contains("行前")){
	        				it = ((AlfrescoFolder)co).getChildren().iterator();
	        		        	while(it.hasNext()){
	        		        		co = it.next();
	        		        		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && 
	        		        					co.getName().toLowerCase().endsWith("json") &&
	        		        					!co.getName().startsWith("_del#")){
	        	        				alfDocAllocate = (AlfrescoDocument)co;
	        	        				break;
	        		        		}
	        		        	}
	        				break;
	        			}
	        		}
	        	}
	        	if(alfDocAllocate == null) throw new Exception("檢查行前階段找不到工作分配表。");
	        	//
			ParseDoc pd = new ParseDoc();
			HashMap<String, String> hMap = pd.CheckStaffWord(arrIS, 
														alfDocAllocate.getContentStream().getStream(),
														formType);
			//準備訊息儲存物件
			Vector<LossItem> vecLoss = new Vector<LossItem>();
			//
			if(hMap.size() > 0){
				int j = 0;
				Iterator<String> it2 = hMap.keySet().iterator();
				while(it2.hasNext()){
					String key = it2.next();
					j++;
					//
					LossItem loss = new LossItem();
					//
					loss.staffName = key.split("_")[0];
					loss.bisCat = key.split("_")[1];
					loss.nodeId = j;
					loss.itemName = hMap.get(key);
					//
					vecLoss.add(loss);
				}
			}
			if(vecLoss.size() == 0){
				actionRequest.setAttribute("successMsg", "報告表內查核項目皆已填妥。");
				SessionMessages.add(actionRequest, "success");
			}else{
				actionRequest.setAttribute("successMsg", "");
				SessionMessages.add(actionRequest, "success");
				//
				actionRequest.getPortletSession(true).setAttribute("vecLoss", vecLoss);
				actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/lostItem.jsp");
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	
	//取得本階段所有檔名
	private Hashtable<String, String> getStageFileNames(AlfrescoFolder fdStage) throws Exception{
		Hashtable<String, String> ht = new Hashtable<String, String>();
		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
				ht.put(co.getName(),  co.getId());
			}
		}
		//
		return ht;
	}
	
	//取得本檢查案參考的範本檔案
	private TempCollect getTempCollect(Session alfSessionAdmin, ExamFact ei, ThemeDisplay themeDisplay) throws Exception{
		if(ei.docTempPath.equals("")) throw new Exception("本檢查案的文件樣板路徑未指定！");
		//
		TempCollect tempCollect = new TempCollect();
		//文件樣板根目錄
		//Folder fdDocRootTemp = this.getDocTempRootFolder(themeDisplay);
		AlfrescoFolder alfTemplRoot = null;
		for(CmisObject co: alfSessionAdmin.getRootFolder().getChildren()) {
			if(!(co instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
			//
			if(co.getName().contains("文件樣板")) {
				alfTemplRoot = (AlfrescoFolder)co;
				break;
			}
		}
		if(alfTemplRoot == null) throw new Exception("後端儲存區內找不到文件樣板區。");
		//共通樣板根目錄
		AlfrescoFolder alfFolderBlank = null;
		for(CmisObject co: alfTemplRoot.getChildren()){
			if(!(co instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
			if(!co.getName().contains("跨組")) continue;
			//
			for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
				if(!(co2 instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
				if(!co2.getName().contains("跨業")) continue;
				//
				for(CmisObject co3: ((AlfrescoFolder)co2).getChildren()){
					if(!(co3 instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
					if(!co3.getName().contains("跨檢查")) continue;
					//
					for(CmisObject co4: ((AlfrescoFolder)co3).getChildren()){
						if(!(co4 instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
						if(!co4.getName().contains("空白表格")) continue;
						//
						alfFolderBlank = (AlfrescoFolder)co4;
						break;
					}
					break;
				}
				break;
			}
			break;
		}
		if(alfFolderBlank == null) throw new Exception("後端儲存區內找不到文件樣板「跨組/跨業/跨檢查/空白表格」的儲存區。");
		//收集所有可能的表格檔案夾
		for(CmisObject co: alfFolderBlank.getChildren()){
			if(!(co instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
			//
			if(co.getName().contains("重要查核")){
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
					String fileName = co2.getName();
					if(fileName.contains("檢查重點") && fileName.contains("助檢")){
						tempCollect.fPointDocAssistant = (AlfrescoDocument)co2;
					}else if(fileName.contains("檢查重點") && fileName.contains("領隊")){
						tempCollect.fPointDocLeader = (AlfrescoDocument)co2;
					}else if(fileName.contains("必查項目") && fileName.contains("助檢")){
						tempCollect.fEssenDocAssistant = (AlfrescoDocument)co2;
					}else if(fileName.contains("必查項目") && fileName.contains("領隊")){
						tempCollect.fEssenDocLeader = (AlfrescoDocument)co2;
					}else if(fileName.contains("資訊作業") && fileName.contains("重要查核") && fileName.contains("助檢")){
						tempCollect.fInfoExamImpChkStaffForm = (AlfrescoDocument)co2;
					}else if(fileName.contains("資訊作業") && fileName.contains("重要查核") && fileName.contains("領隊")){
						tempCollect.fInfoExamImpChkLeaderForm = (AlfrescoDocument)co2;
					}else if(fileName.contains("ExamItemTableHeader") && fileName.toLowerCase().endsWith("doc")){
						tempCollect.fInfoExamItemTableHeader = (AlfrescoDocument)co2;
					}
				}
			}else if(co.getName().contains("實地檢查缺失")){
				HashMap<String, InputStream> htDefectInter = new HashMap<String, InputStream>();
				HashMap<String, InputStream> htDefectOuter = new HashMap<String, InputStream>();
				HashMap<String, InputStream> htDefectAfter = new HashMap<String, InputStream>();
				//
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
					String fileName = co2.getName();
					String pureName = fileName.substring(0, fileName.lastIndexOf("."));
					if(fileName.contains("實地檢查缺失情形表") &&
							fileName.contains("內部") &&
							fileName.contains("檢討會前")){
						htDefectInter.put(fileName, ((AlfrescoDocument)co2).getContentStream().getStream());
					}else if(fileName.contains("實地檢查缺失情形表") &&
								!fileName.contains("內部") &&
								(fileName.contains("主要") || fileName.contains("其他") || fileName.contains("面請"))){
						htDefectOuter.put(pureName, ((AlfrescoDocument)co2).getContentStream().getStream());
					}else if(fileName.contains("實地檢查缺失情形表") &&
								fileName.contains("內部") &&
									!fileName.contains("檢討會前") &&
									!fileName.contains("無系統欄位")){
						htDefectAfter.put(pureName, ((AlfrescoDocument)co2).getContentStream().getStream());
					}
				}
				tempCollect.htSpotDefectInter = htDefectInter;
				tempCollect.htSpotDefectOuter = htDefectOuter;
				tempCollect.htSpotDefectAfter = htDefectAfter;
			}else if(co.getName().contains("檢查報告")){
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
					String fileName = co2.getName();
					if(fileName.contains("檢查意見") && fileName.contains("210") && !fileName.contains("分級統計")){
						tempCollect.examOpinion210 = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("面請改善") && fileName.contains("910") && !fileName.contains("910N2")){
						tempCollect.examFace910 = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("面請改善") && fileName.contains("910N2") ){
						tempCollect.examFace910N2 = ((AlfrescoDocument)co2).getContentStream().getStream();	
					}else if(fileName.contains("檢查意見") && fileName.contains("分級統計")){
						tempCollect.examOpinionStatistic = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("檢查報告目錄")){
						tempCollect.examReportIndex = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("檢查報告") && fileName.contains("封面")){
						tempCollect.fExamCover = (AlfrescoDocument)co2;
					}else if(fileName.contains("檢查報告") && fileName.contains("處理流程")){
						tempCollect.fProcessFlow = (AlfrescoDocument)co2;
					}else if(fileName.contains("派差作業") && fileName.contains("業務項目")){
						tempCollect.fAllocateExamItemPerBis = (AlfrescoDocument)co2;
					}
				}
			}else if(co.getName().contains("評等審議")){
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
					String fileName = co2.getName();
					if(fileName.contains("本會內部參考")){
						tempCollect.evalInterRefItem = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("評等項目") && fileName.contains("內容摘要表")){
						tempCollect.evalSummFormByEvaltem = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("評鑑細項得分")){
						tempCollect.evalItemScoreList = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("評鑑細項評量")){
						tempCollect.evalItemEvalForm = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("檢查項目") && fileName.contains("內容摘要表")){
						tempCollect.evalSummFormByExamItem = ((AlfrescoDocument)co2).getContentStream().getStream();
					}else if(fileName.contains("資訊作業") && fileName.contains("檢查評量表")){
						tempCollect.fInfoExamAssessForm = (AlfrescoDocument)co2;
					}else if(fileName.contains("資訊作業") && fileName.contains("評級一覽表") && fileName.toLowerCase().endsWith("xls")){
						tempCollect.fInfoExamAssessScore = (AlfrescoDocument)co2;
					}
				}
			}else if(co.getName().contains("檢查評述")){
				HashMap<String, InputStream> hm = new HashMap<String, InputStream>();
				for(CmisObject co2: ((AlfrescoFolder)co).getChildren()){
					String fileName = co2.getName();
					//f(fileName.contains("內部參考") && fileName.toLowerCase().endsWith("doc")){
					//	String pureName = fileName.substring(0, fileName.lastIndexOf("."));
						//
					//	hm.put(pureName, htDocAll.get(fileName).getContentStream());
					if(fileName.contains("檢查評述")){	//證票組專用
						if(fileName.contains("評等")){
							tempCollect.fExamComment2 = (AlfrescoDocument)co2;
						}else{
							tempCollect.fExamComment = (AlfrescoDocument)co2;
						}
					}else if(fileName.toLowerCase().endsWith("doc")){
						String pureName = fileName.substring(0, fileName.lastIndexOf("."));
						hm.put(pureName, ((AlfrescoDocument)co2).getContentStream().getStream());						
					}
				}
				tempCollect.htInsForeState = hm;
			}
		}
		//確認檢查評述檔案存在
		if(tempCollect.fExamComment == null) {
			tempCollect.fExamComment = findExamComment(alfFolderBlank);
		}
		//找出檢查行前工作分配表
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ei.folderIdExam);
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject fdStage = it.next();
			if(fdStage.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && fdStage.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)fdStage).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co = it2.next();
					if(co.getName().toLowerCase().endsWith("json") &&
						!co.getName().startsWith("_del#")){
						tempCollect.alfAllocateJson = (AlfrescoDocument)co;
						break;
					}
				}
				//
				break;
			}
		}
		//
		return tempCollect;
	}
	
	/*
	private TempCollect getTempCollectFromLiferay(Session alfSessionAdmin, ExamFact ei, ThemeDisplay themeDisplay) throws Exception{
		if(ei.docTempPath.equals("")) throw new Exception("本檢查案的文件樣板路徑未指定！");
		//
		TempCollect tempCollect = new TempCollect();
		//文件樣板根目錄
		Folder fdDocRootTemp = this.getDocTempRootFolder(themeDisplay);
		//共通樣板根目錄
		Folder fdBlankUniv =  DLAppLocalServiceUtil.getFolder(fdDocRootTemp.getRepositoryId(), fdDocRootTemp.getFolderId(), "跨組");
		fdBlankUniv = DLAppLocalServiceUtil.getFolder(fdDocRootTemp.getRepositoryId(), fdBlankUniv.getFolderId(), "跨業");
		fdBlankUniv = DLAppLocalServiceUtil.getFolder(fdDocRootTemp.getRepositoryId(), fdBlankUniv.getFolderId(), "跨檢查");
		fdBlankUniv = DLAppLocalServiceUtil.getFolder(fdDocRootTemp.getRepositoryId(), fdBlankUniv.getFolderId(), "空白表格");
		//
		Folder fdBlank = fdDocRootTemp;		//本案文件樣板路徑(直接切到空白表格目錄)
		String[] ss = ei.docTempPath.replaceAll("\\\\", "/").split("/");
		for(String s: ss){			//切到空白表格的父階目錄
			fdBlank = DLAppLocalServiceUtil.getFolder(fdBlank.getRepositoryId(), fdBlank.getFolderId(), s);
		}
		fdBlank = DLAppLocalServiceUtil.getFolder(fdBlank.getRepositoryId(), fdBlank.getFolderId(), "空白表格");
		//收集所有可能的表格檔案夾
		Vector<String> vecFolder = new Vector<String>();
		List<Folder> listSub = DLAppLocalServiceUtil.getFolders(fdBlankUniv.getRepositoryId(), fdBlankUniv.getFolderId());
		for(Folder fdSub: listSub){
			vecFolder.add(fdSub.getName());
		}
		listSub = DLAppLocalServiceUtil.getFolders(fdBlank.getRepositoryId(), fdBlank.getFolderId());
		for(Folder fdSub: listSub){
			if(!vecFolder.contains(fdSub.getName())){
				vecFolder.add(fdSub.getName());
			}
		}
		//	
		for(String fdName: vecFolder){	
			//收集所有可能的檔案
			HashMap<String, FileEntry> htDocAll = new HashMap<String, FileEntry>();
			//先找共通目錄檔案
			Folder fdSub = null;
			try{
				fdSub = DLAppLocalServiceUtil.getFolder(fdBlankUniv.getRepositoryId(), fdBlankUniv.getFolderId(), fdName);
			}catch(Exception _ex){
			}
			if(fdSub != null){
				List<FileEntry> _listFile = DLAppLocalServiceUtil.getFileEntries(fdBlank.getRepositoryId(), fdSub.getFolderId());
				for(FileEntry _f: _listFile){
					htDocAll.put(_f.getTitle(), _f);
				}
			}
			//再找本目錄檔案
			fdSub = null;
			try{
				fdSub = DLAppLocalServiceUtil.getFolder(fdBlank.getRepositoryId(), fdBlank.getFolderId(), fdName);
			}catch(Exception _ex){
			}
			if(fdSub != null){
				List<FileEntry> _listFile = DLAppLocalServiceUtil.getFileEntries(fdSub.getRepositoryId(), fdSub.getFolderId());
				for(FileEntry _f: _listFile){
					htDocAll.put(_f.getTitle(), _f);
				}
			}
			//
			if(fdName.contains("重要查核")){
				Iterator<String> itName = htDocAll.keySet().iterator();
				while(itName.hasNext()){
					String fileName = itName.next();
					if(fileName.contains("檢查重點") && fileName.contains("助檢")){
						tempCollect.fPointDocAssistant = htDocAll.get(fileName);
					}else if(fileName.contains("檢查重點") && fileName.contains("領隊")){
						tempCollect.fPointDocLeader = htDocAll.get(fileName);
					}else if(fileName.contains("必查項目") && fileName.contains("助檢")){
						tempCollect.fEssenDocAssistant = htDocAll.get(fileName);
					}else if(fileName.contains("必查項目") && fileName.contains("領隊")){
						tempCollect.fEssenDocLeader = htDocAll.get(fileName);
					}else if(fileName.contains("資訊作業") && fileName.contains("重要查核") && fileName.contains("助檢")){
						tempCollect.fInfoExamImpChkStaffForm = htDocAll.get(fileName);
					}else if(fileName.contains("資訊作業") && fileName.contains("重要查核") && fileName.contains("領隊")){
						tempCollect.fInfoExamImpChkLeaderForm = htDocAll.get(fileName);
					}else if(fileName.contains("ExamItemTableHeader") && fileName.toLowerCase().endsWith("doc")){
						tempCollect.fInfoExamItemTableHeader = htDocAll.get(fileName);
					}
				}
			}else if(fdName.contains("實地檢查缺失")){
				HashMap<String, InputStream> htDefectInter = new HashMap<String, InputStream>();
				HashMap<String, InputStream> htDefectOuter = new HashMap<String, InputStream>();
				HashMap<String, InputStream> htDefectAfter = new HashMap<String, InputStream>();
				//
				Iterator<String> itName = htDocAll.keySet().iterator();
				while(itName.hasNext()){
					String fileName = itName.next();
					String pureName = fileName.substring(0, fileName.lastIndexOf("."));
					if(fileName.contains("實地檢查缺失情形表") &&
							fileName.contains("內部") &&
							fileName.contains("檢討會前")){
						htDefectInter.put(fileName, htDocAll.get(fileName).getContentStream());
					}else if(fileName.contains("實地檢查缺失情形表") &&
								!fileName.contains("內部") &&
								(fileName.contains("主要") || fileName.contains("其他") || fileName.contains("面請"))){
						htDefectOuter.put(pureName, htDocAll.get(fileName).getContentStream());
					}else if(fileName.contains("實地檢查缺失情形表") &&
								fileName.contains("內部") &&
									!fileName.contains("檢討會前") &&
									!fileName.contains("無系統欄位")){
						htDefectAfter.put(pureName, htDocAll.get(fileName).getContentStream());
					}
				}
				tempCollect.htSpotDefectInter = htDefectInter;
				tempCollect.htSpotDefectOuter = htDefectOuter;
				tempCollect.htSpotDefectAfter = htDefectAfter;
			}else if(fdName.contains("檢查報告")){
				Iterator<String> itName = htDocAll.keySet().iterator();
				while(itName.hasNext()){
					String fileName = itName.next();
					if(fileName.contains("檢查意見") && fileName.contains("210") && !fileName.contains("分級統計")){
						tempCollect.examOpinion210 = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("面請改善") && fileName.contains("910") && !fileName.contains("910N2")){
						tempCollect.examFace910 = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("面請改善") && fileName.contains("910N2") ){
						tempCollect.examFace910N2 = htDocAll.get(fileName).getContentStream();	
					}else if(fileName.contains("檢查意見") && fileName.contains("分級統計")){
						tempCollect.examOpinionStatistic = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("檢查報告目錄")){
						tempCollect.examReportIndex = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("檢查報告") && fileName.contains("封面")){
						tempCollect.fExamCover = htDocAll.get(fileName);
					}else if(fileName.contains("檢查報告") && fileName.contains("處理流程")){
						tempCollect.fProcessFlow = htDocAll.get(fileName);
					}else if(fileName.contains("派差作業") && fileName.contains("業務項目")){
						tempCollect.fAllocateExamItemPerBis = htDocAll.get(fileName);
					}
				}
			}else if(fdName.contains("評等審議")){
				Iterator<String> itName = htDocAll.keySet().iterator();
				while(itName.hasNext()){
					String fileName = itName.next();
					if(fileName.contains("本會內部參考")){
						tempCollect.evalInterRefItem = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("評等項目") && fileName.contains("內容摘要表")){
						tempCollect.evalSummFormByEvaltem = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("評鑑細項得分")){
						tempCollect.evalItemScoreList = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("評鑑細項評量")){
						tempCollect.evalItemEvalForm = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("檢查項目") && fileName.contains("內容摘要表")){
						tempCollect.evalSummFormByExamItem = htDocAll.get(fileName).getContentStream();
					}else if(fileName.contains("資訊作業") && fileName.contains("檢查評量表")){
						tempCollect.fInfoExamAssessForm = htDocAll.get(fileName);
					}else if(fileName.contains("資訊作業") && fileName.contains("評級一覽表") && fileName.toLowerCase().endsWith("xls")){
						tempCollect.fInfoExamAssessScore = htDocAll.get(fileName);
					}
				}
			}else if(fdName.contains("檢查評述")){
				HashMap<String, InputStream> hm = new HashMap<String, InputStream>();
				Iterator<String> itName = htDocAll.keySet().iterator();
				while(itName.hasNext()){
					String fileName = itName.next();
					//f(fileName.contains("內部參考") && fileName.toLowerCase().endsWith("doc")){
					//	String pureName = fileName.substring(0, fileName.lastIndexOf("."));
						//
					//	hm.put(pureName, htDocAll.get(fileName).getContentStream());
					if(fileName.contains("檢查評述")){	//證票組專用
						if(fileName.contains("評等")){
							tempCollect.fExamComment2 = htDocAll.get(fileName);
						}else{
							tempCollect.fExamComment = htDocAll.get(fileName);
						}
					}else if(fileName.toLowerCase().endsWith("doc")){
						String pureName = fileName.substring(0, fileName.lastIndexOf("."));
						hm.put(pureName, htDocAll.get(fileName).getContentStream());						
					}
				}
				tempCollect.htInsForeState = hm;
			}
		}
		//確認檢查評述檔案存在
		if(tempCollect.fExamComment == null) {
			tempCollect.fExamComment = findExamComment(fdBlank);
		}
		//找出檢查行前工作分配表
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ei.folderIdExam);
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject fdStage = it.next();
			if(fdStage.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && fdStage.getName().contains("行前")){
				Iterator<CmisObject> it2 = ((AlfrescoFolder)fdStage).getChildren().iterator();
				while(it2.hasNext()){
					CmisObject co = it2.next();
					if(co.getName().toLowerCase().endsWith("json") &&
						!co.getName().startsWith("_del#")){
						tempCollect.alfAllocateJson = (AlfrescoDocument)co;
						break;
					}
				}
				//
				break;
			}
		}
		//
		return tempCollect;
	}
	*/
	
	private AlfrescoDocument findExamComment(AlfrescoFolder fdCurr) throws Exception{
		AlfrescoDocument fe = null;
		for(CmisObject co: fdCurr.getChildren()){
			if(co instanceof org.apache.chemistry.opencmis.client.api.Folder){
				findExamComment((AlfrescoFolder)co);
			}else if(co instanceof org.apache.chemistry.opencmis.client.api.Document){
				if(co.getName().contains("檢查評述") && (co.getName().toLowerCase().endsWith("doc") || co.getName().toLowerCase().endsWith("docx"))) {
					fe = (AlfrescoDocument)co;
					break;
				}	
			}
		}
		return fe;
	}	
	
	private FileEntry findExamCommentFromLiferay(Folder folder) throws Exception{
		FileEntry fe = null;
		List<FileEntry> listFe = DLAppLocalServiceUtil.getFileEntries(folder.getRepositoryId(), folder.getFolderId());
		for(FileEntry _fe: listFe) {
			if(_fe.getTitle().contains("檢查評述") && (_fe.getTitle().toLowerCase().endsWith("doc") || _fe.getTitle().toLowerCase().endsWith("docx"))) {
				fe = _fe;
				break;
			}
		}
		if(fe == null) {
			List<Folder> listSub = DLAppLocalServiceUtil.getFolders(folder.getRepositoryId(), folder.getFolderId());
			for(Folder _folder: listSub) {
				fe = findExamCommentFromLiferay(_folder);
				if(fe != null) break;
			}
		}
		return fe;
	}
	
	
	public Folder getDocTempRootFolder(ThemeDisplay themeDisplay) throws Exception{
		Folder ret = null;
		//
		try{
			ret = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootDocTemp);
		}catch(Exception ex){
		}
		return ret;
	}

	//查詢檔案版本
	public void viewVersion(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String fileId = actionRequest.getParameter("fileId");
			String fileName = actionRequest.getParameter("fileName");
			//
			Vector<FileVersion> vecVersion = new Vector<FileVersion>();
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			Iterator<Document> it = alfDoc.getAllVersions().iterator();
			while(it.hasNext()){
				AlfrescoDocument doc = (AlfrescoDocument)it.next();
				//
				FileVersion fv = new FileVersion();
				fv.fileId = doc.getId();
				fv.fileName = doc.getName();
				fv.versionNo = doc.getVersionLabel();
				fv.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
				if(doc.getPropertyValue("cm:author") != null){
					fv.author = doc.getPropertyValue("cm:author");
				}
				if(doc.getPropertyValue("it:modifier") != null){
					fv.modifier = doc.getPropertyValue("it:modifier");
				}else if(doc.getPropertyValue("cm:description") != null) {
					if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
						fv.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
					}
				}
				if(fv.author.isEmpty() && !fv.modifier.isEmpty()) fv.author = fv.modifier;
				if(!fv.author.isEmpty() && fv.modifier.isEmpty()) fv.modifier = fv.author;
				//
				if(doc.getPropertyValue("it:docStatus") != null){
					fv.docStatus = doc.getPropertyValue("it:docStatus");
				}
				if(doc.getPropertyValue("it:updateMemo") != null){
					fv.description = doc.getPropertyValue("it:updateMemo");
				}
				int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
				fv.fileSize = String.valueOf(dd);
				//
				vecVersion.add(fv);
			}
			actionRequest.getPortletSession(true).setAttribute("vecVersion", vecVersion);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/fileVersion.jsp");
			actionResponse.setRenderParameter("fileId", fileId);
			actionResponse.setRenderParameter("fileName", fileName);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	

	//進入檢查結束或檢討會前階段草稿區
	public void goDraftArea(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);			
			if(fdStage == null) return;
			//
			AlfrescoFolder fdDraft = null;
			Iterator<CmisObject> it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().equals("草稿區")){
					fdDraft = (AlfrescoFolder)co;
					break;
				}
			}
			if(fdDraft == null){
				Map<String, String>  props = new HashMap<String,String>();
				props.put(PropertyIds.NAME, "草稿區");
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
				ObjectId oid = alfSessionAdmin.createFolder(props, fdStage);
				//
				fdDraft = (AlfrescoFolder)alfSessionAdmin.getObject(oid.getId());
			}
			//
			Vector<StageFile> vecDraft = new Vector<StageFile>();
			//
			it = fdDraft.getChildren().iterator();
			while(it.hasNext()){
				AlfrescoDocument doc = (AlfrescoDocument)it.next();
				if(doc.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					StageFile sf = new StageFile();
					//
					sf.fileName = doc.getName();
					if(doc.getPropertyValue("cm:author") != null){
						sf.author = doc.getPropertyValue("cm:author");
					}
					if(doc.getPropertyValue("it:modifier") != null){
						sf.modifier = doc.getPropertyValue("it:modifier");
					}else if(doc.getPropertyValue("cm:description") != null) {
						if(doc.getPropertyValue("cm:description").toString().startsWith("by ")) {
							sf.modifier = doc.getPropertyValue("cm:description").toString().replaceAll("by ", "");
						}
					}
					if(sf.author.isEmpty() && !sf.modifier.isEmpty()) sf.author = sf.modifier;
					if(!sf.author.isEmpty() && sf.modifier.isEmpty()) sf.modifier = sf.author;
					//if(!ef.leaderName.contains(sf.author) && ef.staffs.contains(sf.author)) sf.author = ef.leaderName;
					
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdDraft.getId();
					//
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
					sf.fileSize = String.valueOf(dd);
									
					String iconName = "file.png";
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}else if(sf.fileName.toLowerCase().endsWith("json")){
						iconName = "json24.png";
					}
					sf.iconUrl = iconName;
					//
					vecDraft.add(sf);
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecDraft", vecDraft);
			actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/draftArea.jsp");
			actionResponse.setRenderParameter("folderId", fdDraft.getId());
			//
			 actionRequest.setAttribute("successMsg", "檢查重點與必查項目彙整表的草稿區。");
			 SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	//領出
	/*
	public void doCheckOut(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//簽出
			String fileId = actionRequest.getParameter("fileId");
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			String stageFolderId = actionRequest.getPortletSession(true).getAttribute("stageFolderId").toString();
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			AlfrescoFolder fdExam =  (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + " 已被領出。");
			if(alfDoc.getName().toLowerCase().contains("working copy")) throw new Exception("本檔案已是領出檔，無法再領出。");
			//
			conn = ds.getConnection();
			if(ErpUtil.underToDoList(fdExam.getName(), fdStage.getName(), alfDoc.getName(), conn)) throw new Exception("檔案：" + alfDoc.getName() + "已在流程之中，請於任務清單中領出。");
			//
			Folder fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			//
			if(alfDoc.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc.getName() + "已被鎖定，無法更新");
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			AlfrescoDocument pwc = (AlfrescoDocument)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			//
			String destName = fdExam.getName() + "#" + fdStage.getName() + "#" + pwc.getName();
			String description = "領出人：" + userFullName;
			FileEntry fe = null;
			try{
				fe = DLAppLocalServiceUtil.getFileEntry(themeDisplay.getScopeGroupId(), 
												fdOut.getFolderId(), 
												destName);
			}catch(Exception _ex){
			}
			if(fe == null){
				fe = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), 
												themeDisplay.getScopeGroupId(), 
												fdOut.getFolderId(), 
												destName, 
												pwc.getContentStreamMimeType(), 
												destName, 
												description, 
												null, 
												IOUtils.toByteArray(pwc.getContentStream().getStream()), 
												new ServiceContext());
			}else{
				fe = DLAppLocalServiceUtil.updateFileEntry(themeDisplay.getUserId(),
												fe.getFileEntryId(),
												destName,
												fe.getMimeType(),
												destName,
												description,
												null,
												false,
												IOUtils.toByteArray(pwc.getContentStream().getStream()),
												new ServiceContext());
			}
			conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
				
			}
		}
	}	
	*/
	
	//取消簽出
	public void doCancelCheckOut(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = actionRequest.getParameter("fileId");
			//
			//Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			//AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//AlfrescoFolder fdExam =  (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//Liferay中的領出檔名
			//String destName = fdExam.getName() + "#" + fdStage.getName() + "#" + alfDoc.getName();
			//執行取銷領出
			alfDoc.cancelCheckOut();
			//找出Liferay是否有領出的檔案
			/*
			Folder fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			List<FileEntry> listFile = DLAppLocalServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), fdOut.getFolderId());
			for(FileEntry f: listFile){
				if(f.getTitle().equals(destName)){
					DLAppLocalServiceUtil.deleteFileEntry(f.getFileEntryId());
					break;
				}
			}
			*/
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//繳回
	/*
	public void doCheckIn(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		//Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//簽出
			String fileId = actionRequest.getParameter("fileId");
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			String stageFolderId = actionRequest.getPortletSession(true).getAttribute("stageFolderId").toString();
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			//領出暫存區
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			//
			ErpUtil.doCheckIn(alfSessionAdmin, fdStage, alfDoc, fdOut, themeDisplay);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	*/	

	//轉存PDF
	public void genPdfFile(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = actionRequest.getParameter("fileId");
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//AlfrescoFolder fdExam =  (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			OutputStream os = new ByteArrayOutputStream();
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			if(!alfDoc.getName().toLowerCase().endsWith("doc")) throw new Exception("目前系統只提供 MS Word 轉存 PDF 功能。");
			//
			com.aspose.words.Document aDoc = new com.aspose.words.Document(alfDoc.getContentStream().getStream());
			
			aDoc.save(os, SaveFormat.PDF);
			if(os == null) throw new Exception("無法產出 Pdf 的輸出串流。");
			
			InputStream is=new ByteArrayInputStream(((ByteArrayOutputStream)os).toByteArray());
			String fileName = alfDoc.getName().substring(0, alfDoc.getName().lastIndexOf(".")) + ".pdf";
			ContentStream cs = new ContentStreamImpl(fileName, null, "application/pdf", is);
			//
			//本階段已存在的檔案, (檔名,Node ID)
			Hashtable<String, String> htNames = this.getStageFileNames(fdStage);
			String updateMemo = "重新產出";
			Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled, P:cm:author");
			props.put("cm:author", userFullName);
			
			if(htNames.containsKey(fileName)){
				props.put("it:updateMemo", updateMemo);
				//
				AlfrescoDocument alfPdf = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
				if(alfPdf.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfPdf.getName() + "已被鎖定，無法更新");
				//
				ObjectId idOfCheckedOutDocument = alfPdf.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, updateMemo);
			}else{
				props.put(PropertyIds.NAME, fileName);
				fdStage.createDocument(props, cs, VersioningState.MAJOR);
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}

	//執行 Authentica 加密
	public void doAuthenticaRun(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection connMS = null;
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdStage =  (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//選取 MS Office 檔案
			Vector<StageFile> vecSelected = new Vector<StageFile>();
			//
			Vector<StageFile> vecFiles = collec.vecFiles;
			for(StageFile sf: vecFiles){
				if((sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("xls")) && !sf.author.contains("加密")){
					vecSelected.add(sf);
				}
			}
			if(vecSelected.size() == 0) throw new Exception("找不到未加密的MS Word或MS Excel檔案。");
			//準備 SQL Server 連線
	        connMS = this.getMsConnection();
			//
			AlfrescoFolder fdExam =  (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			AlfrescoFolder fdReport =  (AlfrescoFolder)alfSessionAdmin.getObject(fdExam.getParentId());
			//準備檔案夾
			File folderAuth = new File(PropsUtil.get("authentica.src.path"));
			File folderReport = new File(folderAuth.getCanonicalPath() + "\\" + fdReport.getName());
			if(!folderReport.exists()) folderReport.mkdir();
			File folderExam = new File(folderReport.getCanonicalPath() + "\\" + fdExam.getName());
			if(!folderExam.exists()) folderExam.mkdir();
			//Authentica 伺服器的相對位置
			String destFolderPath = PropsUtil.get("authentica.dest.path") + "\\" + fdReport.getName() + "\\" + fdExam.getName() + "\\";
			//開始搬檔
			for(StageFile sf: vecSelected){
				AlfrescoDocument doc = (AlfrescoDocument)alfSessionAdmin.getObject(sf.fileId);
				InputStream is = doc.getContentStream().getStream();
				FileOutputStream fos = new FileOutputStream(folderExam.getCanonicalPath() + "\\" + doc.getName());
				byte[] buffer = new byte[4096];  
				int bytesRead;  
				while ((bytesRead = is.read(buffer)) != -1) {  
					fos.write(buffer, 0, bytesRead);  
				}  
				is.close();  
				fos.close();
				//更新 SQL Server
				//java.sql.Date date = new java.sql.Date(0000-00-00);
				long timeNow = Calendar.getInstance().getTimeInMillis();
				java.sql.Timestamp ts = new java.sql.Timestamp(timeNow);
				//
				String sql = "INSERT INTO exd05a (reportno, filefullpath, filename, fileuploaddate) VALUES (?,?,?,?)";
				PreparedStatement ps = connMS.prepareStatement(sql);
				ps.setString(1, fdReport.getName());
				ps.setString(2, destFolderPath);
				ps.setString(3, doc.getName());
				ps.setTimestamp(4, ts);
				ps.execute();
				//刪除 Alfresco node
				doc.delete();
			}
			connMS.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(connMS != null) connMS.close();
			}catch(Exception _ex){
			}
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public String getDocumentURL(Document document, Session session) {
		    String link = null;
		    try {
		        Method loadLink = AbstractAtomPubService.class.getDeclaredMethod("loadLink", 
		            new Class[] { String.class, String.class, String.class, String.class });
		        //loadLink.setAccessible(true);
		        link = (String) loadLink.invoke(session.getBinding().getObjectService(), session.getRepositoryInfo().getId(),
		            document.getId(), AtomPubParser.LINK_REL_CONTENT, null);
		    } catch (Exception e) {
		       e.printStackTrace();
		    }
		    return link;
	  }

	//取得 MS SQL 連線
	private Connection getMsConnection() throws Exception{
		String msDriver = PropsUtil.get("sqlserver.driver"); 		//"net.sourceforge.jtds.jdbc.Driver";
		String msURL = PropsUtil.get("sqlserver.url");			//"jdbc:jtds:sqlserver://localhost:1433/traffic";
		String msUser = PropsUtil.get("sqlserver.user");			//"sa";
        String msPasswd= PropsUtil.get("sqlserver.password");
        //
		Class.forName(msDriver);
        return DriverManager.getConnection(msURL, msUser, msPasswd);
	}
	
	
	//將 InputStream轉成Byte Array
	public byte[] getBytes(InputStream is) throws IOException {
		    int len;
		    int size = 1024;
		    byte[] buf;

		    if (is instanceof ByteArrayInputStream) {
		      size = is.available();
		      buf = new byte[size];
		      len = is.read(buf, 0, size);
		    } else {
		      ByteArrayOutputStream bos = new ByteArrayOutputStream();
		      buf = new byte[size];
		      while ((len = is.read(buf, 0, size)) != -1)
		        bos.write(buf, 0, len);
		      buf = bos.toByteArray();
		    }
		    return buf;
	}
	
	//批次下載
	public void prepareBatchAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
		 try{		
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Enumeration<String> en = actionRequest.getParameterNames();
			if(en == null) return;
			//
			Vector<String> vecBatchId = new Vector<String>();
			//
			conn = ds.getConnection();
			while(en.hasMoreElements()){
				String p = en.nextElement();
				if(p.startsWith("cb_")){
					try{
						String[] ss = p.split("_");
						String fileId = trimAlfDocId(ss[1]);
						vecBatchId.add(fileId);
					}catch(Exception _ex){
					}
				}
			}
			//
			if(vecBatchId.size() == 0)	throw new Exception("請勾選所要處理的檔案。");
			//
			String batchActionType = actionRequest.getParameter("batchActionType");
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			HashMap<String, String> htBatchDoc = new HashMap<String, String>();
			for(String fileId: vecBatchId){
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				htBatchDoc.put(fileId, alfDoc.getName());
			}
			actionRequest.getPortletSession(true).setAttribute("htBatchDoc", htBatchDoc);
			if(batchActionType.equalsIgnoreCase("download")){
				actionRequest.setAttribute("successMsg", "以下 " + htBatchDoc.size() + " 個檔案將以壓縮檔型式下載。");
				SessionMessages.add(actionRequest, "success");
				actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/batchDownload.jsp");
			}else if(batchActionType.equalsIgnoreCase("archive")){
				actionRequest.setAttribute("successMsg", "以下 " + htBatchDoc.size() + " 個檔案將移至歸檔區。");
				SessionMessages.add(actionRequest, "success");
				actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/batchArchive.jsp");
			}else if(batchActionType.equalsIgnoreCase("post")){
				actionRequest.setAttribute("successMsg", "以下 " + htBatchDoc.size() + " 個檔案將上傳至檢查行政系統，並自上傳日起算滿14天後自動關閉本報告案。");
				SessionMessages.add(actionRequest, "success");
				actionResponse.setRenderParameter("jspPage", "/jsp/stagefiles/batchPost.jsp");
			}
		}catch(Exception ex){
		       	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
		}finally{
		    try{
		    	if(conn != null) conn.close();
		    }catch(Exception _ex){
		    }
		}
	}
	
	private String trimAlfDocId(String alfDocId){
		if(alfDocId.contains(";")){
			return alfDocId.substring(0, alfDocId.lastIndexOf(";"));
		}else{
			return alfDocId;
		}
	}
	
	//批次歸檔
	public void doBatchArchive(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
		 try{		
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			@SuppressWarnings("unchecked")
			HashMap<String, String> htBatchDoc = (HashMap<String, String>)actionRequest.getPortletSession(true).getAttribute("htBatchDoc");
			if(htBatchDoc.size() == 0) throw new Exception("未選取歸檔檔案。");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			AlfrescoFolder fdStage = (AlfrescoFolder)(alfSessionAdmin.getObject(collec.stageFolderId));
			AlfrescoFolder fdExam = (AlfrescoFolder)(alfSessionAdmin.getObject(fdStage.getParentId()));
			//取得歸檔區
			AlfrescoFolder fdArchive = null;
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("歸檔")){
					fdArchive = (AlfrescoFolder)co;
					break;
				}
			}
			if(fdArchive == null) throw new Exception("檢查證 " + fdExam.getName() + " 未建立歸檔區。");
			//歸檔區已存在的檔案
			Hashtable<String, String> htNames = this.getStageFileNames(fdArchive);
			//
			Iterator<String> it2 = htBatchDoc.keySet().iterator();
			while(it2.hasNext()){
				String fileId = it2.next();
				String fileName = htBatchDoc.get(fileId);
				//
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				if(htNames.containsKey(fileName)){
					String updateMemo = "重新歸檔";
					Map<String, String> props = new HashMap<String, String>();
					props.put("it:updateMemo", updateMemo);
					props.put("it:modifier", userFullName);
					AlfrescoDocument alfDoc2 = (AlfrescoDocument)alfSessionAdmin.getObject(htNames.get(fileName));
					if(alfDoc2.isVersionSeriesCheckedOut()) throw new Exception("檔案：" + alfDoc2.getName() + "已被鎖定，無法更新");
					//
					ObjectId idOfCheckedOutDocument = alfDoc2.checkOut();
					Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
					pwc.checkIn(false, props, alfDoc.getContentStream(), updateMemo);
					//
					alfDoc.delete();
				}else{
					alfDoc.move(fdStage, fdArchive);
				}
			}
		}catch(Exception ex){
		    ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
		}finally{
	       	try{
	       		if(conn != null) conn.close();
	       	}catch(Exception _ex){
	       	}
		}
	}	

	//批次傳至檢查行政系統
	public void doBatchPost(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		 Connection conn = null;
		 Connection  db2Conn = null;
		 try{		
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			@SuppressWarnings("unchecked")
			HashMap<String, String> htBatchDoc = (HashMap<String, String>)actionRequest.getPortletSession(true).getAttribute("htBatchDoc");
			if(htBatchDoc.size() == 0) throw new Exception("未選取上傳檔案。");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			AlfrescoFolder fdStage = (AlfrescoFolder)(alfSessionAdmin.getObject(collec.stageFolderId));
			AlfrescoFolder fdExam = (AlfrescoFolder)(alfSessionAdmin.getObject(fdStage.getParentId()));
			//取得檢查行政上傳區
			String strDirPost = PropsUtil.get("exam.admin.sys.archive.path");
			if(strDirPost == null || strDirPost.equals("")) throw new Exception("檢查行政上傳目錄未設定。");
			File dirPost = new File(strDirPost);
			if(dirPost == null || !dirPost.isDirectory()) throw new Exception("檢查行政上傳目錄" + strDirPost + " 不存在。");
			//建立年度/報告編號目錄
			AlfrescoFolder fdReport = (AlfrescoFolder)(alfSessionAdmin.getObject(fdExam.getParentId()));
			//Log
			//ErpUtil.logUserAction("上傳至檢查行政", "報告編號:" + collec.reportNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
			String fromIp = "";
			if(httpRequest != null){
				fromIp = httpRequest.getRemoteAddr();
			}
			//
			String sql = "INSERT INTO action_log (screen_name, action_time, action_name, from_ip, target_file_name) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userFullName);
			ps.setString(2, getTimeStamp());
			ps.setString(3, "上傳至檢查行政");
			ps.setString(4, fromIp);
			ps.setString(5, "報告編號:" + collec.reportNo);
			ps.execute();
			//
			String strYear = fdReport.getName().substring(0, 3);
			File dirYear = new File(dirPost.getCanonicalPath() + "\\" + strYear);
			if(dirYear == null || !dirYear.isDirectory()) dirYear.mkdir();
			File dirReport = new File(dirYear.getCanonicalPath() + "\\" + fdReport.getName());
			if(dirReport == null || !dirReport.isDirectory()) dirReport.mkdir();
			//
			String strDocName = "";
			Iterator<String> it2 = htBatchDoc.keySet().iterator();
			while(it2.hasNext()){
				String fileId = it2.next();
				//String fileName = htBatchDoc.get(fileId);
				AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
				
				//檢查行政系統需重新命名
				String alfName = alfDoc.getName().trim().replaceAll(" ", "");
				String newFileName = alfName;
				if(alfName.contains(".") && alfName.contains("_")){ 
					String[] ss1 = alfName.split("_");
					String[] ss2 = alfName.split("\\.");
					//2018-06-07 修改
					if(alfName.toLowerCase().endsWith(".d.doc")) {
						if(ss2.length > 3) {
							newFileName = ss1[0].trim() + "." + ss2[ss2.length - 3].trim() + "." + ss2[ss2.length - 2].trim() + "." + ss2[ss2.length - 1].trim();
						}else {
							newFileName = alfName;
						}
					}else if(ss2.length == 2) {
						newFileName = alfName;
					}else if(ss2.length > 2){
						newFileName = ss1[0].trim() + "." + ss2[ss2.length - 2].trim() + "." + ss2[ss2.length - 1].trim();
					}	
				}
				// write the inputStream to a FileOutputStream
				InputStream is = alfDoc.getContentStream().getStream();
				FileOutputStream os =  new FileOutputStream(new File(dirReport.getCanonicalPath() + "\\" + newFileName));
				int read = 0;
				byte[] bytes = new byte[1024];
				while ((read = is.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
				os.close();
				//
				if(!strDocName.equals("")) strDocName += "^";
				strDocName += newFileName;
			}
			//呼叫檢查行政Stored Procedure
			Class.forName(PropsUtil.get("db2.driver"));
			// establish a connection to DB2
			db2Conn = DriverManager.getConnection(PropsUtil.get("db2.url"),
	        											PropsUtil.get("db2.user"),
	        											PropsUtil.get("db2.password"));
			//
			CallableStatement cstmt = db2Conn.prepareCall("EXEC [sp_Exb01t] ?,?,?,?");
			cstmt.setString(1, fdReport.getName());
			cstmt.setString(2, fdExam.getName());
			cstmt.setString(3, themeDisplay.getUser().getScreenName());
			cstmt.setString(4, strDocName);
			cstmt.execute();
			db2Conn.close();
			//計算關閉報告案日期
			Calendar cal = Calendar.getInstance();
			String datePost = cal.get(Calendar.YEAR) + "-" + String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)); 
			cal.add(Calendar.DATE, 14);
			String dateToClose = cal.get(Calendar.YEAR) + "-" + String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
			//
			conn = ds.getConnection();
			sql = "UPDATE reports SET post_date=?, date_to_close=? WHERE report_no=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, datePost);
			ps.setString(2, dateToClose);
			ps.setString(3, fdReport.getName());
			ps.execute();
		}catch(Exception ex){
		       	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
		}finally{
		       	try{
		       		if(conn != null) conn.close();
		       	}catch(Exception _ex){
		       	}
		       	try{
		       		if(db2Conn != null) db2Conn.close();
		       	}catch(Exception _ex){
		       	}
		}
	}	
	
	//取得時間戳記
	public String getTimeStamp() throws Exception{
		String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS" ;
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(new java.util.Date());
	}
	
	//檢查特定檔名是否在流程之中
	public boolean underToDoListIgnoreStage(String examNo, String fileName, Connection conn) throws Exception{
		boolean ret = false;
		//
		String sql = "SELECT 1 FROM todo_list WHERE exam_no=? AND file_name=? AND (done IS NULL OR done = 0)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, examNo);
        ps.setString(2, fileName);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
        	ret = true;
        }
        rs.close();
        //
        return ret;
	}

	/*
	public void genEmptyExamComment(ActionRequest actionRequest, ActionResponse actionResponse) {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			//取得檢查案結構資訊
			ExamFact ei = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
			//查詢範本
			TempCollect tempCollect = this.getTempCollect(alfSessionAdmin, ei, themeDisplay);
			if(tempCollect.fExamComment == null) throw new Exception("文件樣板區找不到檢查評述樣板檔案。");
			//
			String fileNamePure = collec.reportNo + "_檢查評述_" + collec.userFullName;
			//
			AlfrescoDocument docToUpdate = null;
			AlfrescoFolder fdDest = (AlfrescoFolder)alfSessionAdmin.getObject(collec.stageFolderId);
			for(CmisObject co: fdDest.getChildren()) {
				if(co.getName().startsWith(fileNamePure)) {
					docToUpdate = (AlfrescoDocument)co;
					break;
				}
			}
		    //新檔
			if(docToUpdate == null){
				String fileName = fileNamePure + ".doc";
				//
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put(PropertyIds.NAME, fileName);
				props.put("cm:author", ErpUtil.getCurrUserFullName(themeDisplay));

				ContentStream cs = new ContentStreamImpl(fileName, null, "application/msword", tempCollect.fExamComment.getContentStream());
				// create a major version
				fdDest.createDocument(props, cs, VersioningState.MAJOR);
			}else {
				Map<String, String> props = new HashMap<String, String>();
				props.put("it:updateMemo", "重新產出");
				props.put("it:modifier", ErpUtil.getCurrUserFullName(themeDisplay));
				//
				ContentStream cs = new ContentStreamImpl(docToUpdate.getName(), null, docToUpdate.getContentStreamMimeType(), tempCollect.fExamComment.getContentStream());

				ObjectId idOfCheckedOutDocument = docToUpdate.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, "上傳更新");
			}
		}catch(Exception ex) {
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally {
			try {
				if(conn != null) conn.close();
			}catch(Exception _ex) {
			}
		}
	}
	*/
}

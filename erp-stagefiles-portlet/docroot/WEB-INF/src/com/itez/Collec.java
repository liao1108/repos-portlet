package com.itez;

import java.util.Vector;

public class Collec {
	public String dateWebLife = "2016-05-06";		//Web版與桌面板檢查報告處理系統分水嶺
	
	public String stageFolderId = "";
	public String userFullName = "";
	
	public Vector<StageFile> vecFiles = new Vector<StageFile>();
	public Vector<StageFile> vecDeletedFiles = new Vector<StageFile>();
	
	public String reportNo = "";
	public String examNo = "";
	public String dateStart = "";		//檢查起始日期
	public String bankName = "";
	public String bankNameShort = "";
	public String stageName = "";		//目前階段的名稱
	
	public boolean isLeader = false;					//登入者是否為本檢查證的領隊
	public boolean isStaff = false;						//是否為助檢成員
	public boolean isMainAssesser = false;				//登入者是否為主評人員
	
	public boolean isRatingAssess = false;				//目前檢查案是否執行評等審議
	public boolean isComputerAudit = false;			//是否為電腦稽核
	public boolean isSepecialExam = false;				//是否為專案檢查
	public boolean isEssenNeed = false;				//是否執行必查項目報告
	public int examCommentProduceType = 0;			//檢查評述產出方式(內定不產出)
	
	public boolean isInsuForeBank = false;				//是否為保險外銀組
	public boolean isInsuForeBankNormal = false;		//是否為保險外銀組的一般檢查
	public boolean isSecuritiesNotes = false;			//是否為證票組
	
	public boolean isEvalSumNeededWoSpotDefect = false;		//需產出項目別評等審議但無須實地檢查缺失
	public boolean isEvalSumNeededWSpotDefect = false;			//需產出項目別評等審議但須實地檢查缺失
	
	public boolean reportClosed = false;
	
	public Vector<String> vecMainAssesser = new Vector<String>();	//主評人員姓名集合
	public Vector<String> vecStaff = new Vector<String>();			//助檢人員
	
	public Vector<StageFile> vecComment = null;			//檢查評述檔案
	public String examNature = "";			//檢查性質
}

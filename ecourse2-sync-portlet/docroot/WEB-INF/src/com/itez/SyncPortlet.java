package com.itez;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class SyncPortlet extends MVCPortlet {
	String syncFolders = "";

	Log _log = LogFactoryUtil.getLog(SyncPortlet.class);
	
	@Override
	public void init() throws PortletException {
		super.init();
		syncFolders = getInitParameter("sync-folders");
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Vector<String> vecFolder = new Vector<String>();
			String[] ss = syncFolders.split(",");
			for(String s: ss){
				vecFolder.add(s);
			}
			//
			renderRequest.getPortletSession(true).setAttribute("vecFolder", vecFolder);
		}catch(Exception ex){
		}
		//
		super.doView(renderRequest, renderResponse);
	}

	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String actionType = resRequest.getParameter("actionType");
			//匯出時
			//if(actionType.equalsIgnoreCase("exportFolder")){
				String folderNameIndex = resRequest.getParameter("folderNameIndex").trim();
				String[] ss = syncFolders.split(",");
				String folderName = ss[Integer.parseInt(folderNameIndex)]; 
				//
				Folder fdExport = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, folderName);
				//
				String fileName = folderName + ".zip";
				resResponse.setContentType("application/zip");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode(fileName, "UTF-8") + "\"");
				
				OutputStream out = resResponse.getPortletOutputStream();
				//
			        ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));
			        this.getZipOutStream("", themeDisplay.getScopeGroupId(), fdExport.getFolderId(), zos);
			        zos.flush();
			        out.flush();
			        //
			        zos.close();
				out.close();
				//
			        System.out.println("Done.");
			//}
		}catch(Exception ex){
			//ex.printStackTrace();
			//
   			String errMsg =  getItezErrorCode(ex);
   			resRequest.setAttribute("errMsg", errMsg);
   			//System.out.println(errMsg);
   			_log.error(errMsg);
		}
	}


	//遞迴加入
	private void getZipOutStream(String parentFolder, long respositoryId, long folderId, ZipOutputStream zos) throws Exception{
		//Folder folder = DLAppLocalServiceUtil.getFolder(folderId);
		//寫入檔案	
		List<FileEntry> listFile = DLAppLocalServiceUtil.getFileEntries(respositoryId, folderId);
		for(FileEntry fe: listFile){
			//System.out.println("zipping file: " + fe.getTitle());
			InputStream is = fe.getContentStream();
			//
			String filePath = fe.getTitle();
			if(!parentFolder.equals("")){
				filePath = parentFolder + "\\" + filePath;
			}
			ZipEntry entry = new ZipEntry(filePath);
	                zos.putNextEntry(entry);
	                //
			//ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				zos.write(data);
			}
	                zos.closeEntry();
	                is.close();
	                //
	                //System.out.println(filePath);
		}
		//
		List<Folder> listFolder = DLAppLocalServiceUtil.getFolders(respositoryId, folderId);
		for(Folder fd: listFolder){
			//System.out.print("Exploring foler :" + fd.getName());
			String paPath = fd.getName();
			if(!parentFolder.equals("")){
				paPath = parentFolder + "/" + paPath;
			}
			getZipOutStream(paPath, respositoryId, fd.getFolderId(), zos);
		}
	}
	
	//檔案更新
	public void importFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	//
	        	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要匯入的檔案。");
	        	//
		        String fileName = uploadRequest.getFileName("fileName");
		        if(!fileName.toLowerCase().endsWith(".zip")) throw new Exception("請以 zip 格式包裝匯入檔案。");
	        	//
		        int idx = fileName.lastIndexOf(".");
		        String folderName = fileName.substring(0, idx);
		        //找相對檔案夾
		        Folder fdDest = null;
		        List<Folder> listFolder = DLAppLocalServiceUtil.getFolders(themeDisplay.getScopeGroupId(), 0);
		        for(Folder fd: listFolder){
		        	if(fd.getName().equalsIgnoreCase(folderName)){
		        		fdDest = fd;
		        		break;
		        	}
		        }
			if(fdDest == null) throw new Exception("找不到名稱為「" + folderName + "」之檔案夾。");
		        //
			MimetypesFileTypeMap mtMap = new MimetypesFileTypeMap();
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), actionRequest);
			//
		        InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
		        ZipInputStream zis = new ZipInputStream(is);
		        //get the zipped file list entry
		        ZipEntry ze = zis.getNextEntry();
	            	while(ze != null){
	            		if(!ze.isDirectory()){
		            		String fn = ze.getName();
		            		//System.out.println("Unzip File Name: " + _fileName);
		            		Folder fdCurr = fdDest;
		            		if(fn.contains("/")){
		            			String[] ss = fn.split("/");
		            			for(int i=0; i < ss.length - 1; i++){
		            				boolean found = false;
		            				List<Folder> _list = DLAppLocalServiceUtil.getFolders(fdCurr.getRepositoryId(), fdCurr.getFolderId());
		            				for(Folder _fd: _list){
		            					if(_fd.getName().equalsIgnoreCase(ss[i])){
		            						fdCurr = _fd;
		            						found = true;
		            						break;
		            					}
		            				}
		            				if(!found){
		            					fdCurr = DLAppLocalServiceUtil.addFolder(themeDisplay.getUserId(),
		            														fdCurr.getRepositoryId(),
		            														fdCurr.getFolderId(),
		            														ss[i],
		            														null,
		            														serviceContext);
		            				}
		            			}
		            			//
		            			fn = ss[ss.length - 1];
		            		}
		            		FileEntry fe = null;
		            		try{
		            			fe = DLAppLocalServiceUtil.getFileEntry(fdCurr.getGroupId(), fdCurr.getFolderId(), fn);
		            		}catch(Exception _ex){
		            		}
		            		if(fe == null){
			            		DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(), 
			            									fdCurr.getRepositoryId(), 
			            									fdCurr.getFolderId(), 
			            									fn, 
			            									mtMap.getContentType(fn), 
			            									fn, 
			            									"", 
			            									null, 
			            									ze.getExtra(), 
			            									serviceContext);
		            		}else{
		            			DLAppLocalServiceUtil.updateFileEntry(themeDisplay.getUserId(),
		            											fe.getFileEntryId(),
		            											fn, 
		            											mtMap.getContentType(fn),
		            											fn,
		            											"",
		            											null,
		            											false,
		            											ze.getExtra(),
		            											serviceContext);
		            		}
	            		}		            		
	            		ze = zis.getNextEntry();
	            	} 
	                zis.closeEntry();
	            	zis.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//
	        	//ex.printStackTrace();
	        	//
	        	SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }
	}
	
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}	
}

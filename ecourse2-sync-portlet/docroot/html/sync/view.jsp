<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<portlet:defineObjects />

<%
	Vector<String> vecFolder = new Vector<String>();
	if(renderRequest.getPortletSession(true).getAttribute("vecFolder") != null){
		vecFolder = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecFolder");
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:resourceURL var="exportFolderURL" >
</portlet:resourceURL>	

<portlet:actionURL var="importFileURL" name="importFile" >
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/files.png">&nbsp;檔案匯出與匯入：
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset label="匯出">
	<aui:form action="<%=exportFolderURL.toString() %>"   method="post"  >
	<aui:layout>
		<aui:column cssClass="td">
			<aui:select name="folderNameIndex" label="檔案夾名稱">
				<aui:option>&nbsp;</aui:option>
				<%for(String fn: vecFolder){ %>
					<aui:option value="<%=vecFolder.indexOf(fn) %>"><%=fn%></aui:option>
				<%} %>	
			</aui:select>
		</aui:column>
	</aui:layout>	
	
	<aui:layout>			
		<aui:column cssClass="td">
			<aui:button type="submit"   value="匯出指定之檔案夾"  cssClass="button"/>
		</aui:column>
	</aui:layout>
	</aui:form>
</aui:fieldset>

<br>

<aui:fieldset label="匯入">
	<aui:form action="<%=importFileURL%>"   enctype="multipart/form-data" method="post"  >
		<aui:layout>
			<aui:column>
				<aui:input type="file" name="fileName" label="請指定所要上傳的zip檔案(檔名須與接收之檔案夾相同)" size="100"/>
			</aui:column>
		</aui:layout>
		
		<aui:layout>	
			<aui:column cssClass="td">
				<aui:button type="submit" value="匯入指定之檔案"  cssClass="button"/>
			</aui:column>
		</aui:layout>
	</aui:form>	
</aui:fieldset>

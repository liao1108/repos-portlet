package com.itez.test;

import java.io.File;

import javax.portlet.PortletException;

import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


public class TestPortlet extends MVCPortlet {
 
	@Override
	public void init() throws PortletException {
		super.init();
		//
		try{
			File fd = FileUtil.createTempFolder();
			System.out.println(fd.getCanonicalPath());
			File fd2 = new File(fd.getCanonicalPath() + "\\109F001");
			if(!fd2.exists()) fd2.mkdir();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

}

package com.itez;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.PortletException;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.util.bridges.mvc.MVCPortlet;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

public class SchedulerPortlet extends MVCPortlet implements MessageListener {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
	//Utils utils = new Utils();
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    		requestPath = getInitParameter("requestPath");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void receive(Message arg0) throws MessageListenerException {
		//檢查未回覆留言是否超過三日
		System.out.println("Checking unanswer message ....");
		Connection conn = null;
		try{
			Calendar calCurr = Calendar.getInstance();
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditor_message WHERE responsed IS NULL OR responsed=0";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String logDate = rs.getString("log_time").split(" ")[0];
				String[] dd = logDate.split("-");
				Calendar calLog = Calendar.getInstance();
				calLog.set(Calendar.YEAR, Integer.parseInt(dd[0]));
				calLog.set(Calendar.MONTH, Integer.parseInt(dd[1]) - 1);
				calLog.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dd[2]));
				//calLog.add(Calendar.DATE, 3);
				int nn = 0;
				while(calLog.before(calCurr)){
					calLog.add(Calendar.DATE, 1);
					nn++;
				}
				if(nn == 4 || (nn > 0 && nn%3==0)){	//超過3天,每隔3天發一次mail
					String userNo = rs.getString("user_no");
					String userEMail = rs.getString("user_email");
					String logMsg = rs.getString("log_msg");
					//
					String strMail = userNo + " 您好，\n\n";
					
					strMail += "您於稽核人員留言板的留言：\n\n";
					strMail += logMsg + "\n\n";
					strMail += "本局目前正在處理中，將會儘速回覆給您。";
					strMail += "\n";
					strMail += "金管會檢查局";
					//
					DefaultHttpClient httpClient = new DefaultHttpClient();
					URIBuilder builder = new URIBuilder();
					builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
									.setParameter("type", "SendMail")
									.setParameter("mailNo", userEMail)
									.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
					URI uri = builder.build();
					HttpPost httpPost = new HttpPost(uri);
					HttpResponse response = httpClient.execute(httpPost);
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String line = br.readLine();
					//
					if(response.getEntity() != null ) {
						httpPost.abort();
					}
				}
			}
			conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
}

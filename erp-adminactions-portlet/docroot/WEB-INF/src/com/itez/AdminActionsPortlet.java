package com.itez;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class AdminActionsPortlet extends MVCPortlet {

	public void doAdminAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			String actionType = "";
			if(actionRequest.getParameter("actionType") != null){
				actionType = actionRequest.getParameter("actionType");
			}
			//
			if(actionType.equalsIgnoreCase("deleteAllUsers")){
				deleteAllUsers();
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg =  ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	

	private void deleteAllUsers() throws Exception{
		for (User user : UserLocalServiceUtil.getUsers(0, 99999)) {
			if (user.isDefaultUser() || PortalUtil.isOmniadmin(user.getUserId())) {
				System.out.println("Skipping user " + user.getScreenName());
			}else{
				final long userToDelete = user.getUserId();
				UserLocalServiceUtil.deleteUser(userToDelete);
			}
		}
	}

}

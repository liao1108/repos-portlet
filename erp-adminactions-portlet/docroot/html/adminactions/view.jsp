<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<portlet:defineObjects />
 
  <%
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doAdminActionURL" name="doAdminAction" >
</portlet:actionURL>

<aui:form action="<%=doAdminActionURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
			<img src="<%=renderRequest.getContextPath()%>/images/adminaction.png"/>
		</aui:column>
		
		<aui:column>
			<aui:select name="actionType" label="作業種類" >
				<aui:option value="" >&nbsp;</aui:option>
				<aui:option value="deleteAllUsers">清除所有使用者</aui:option>
			</aui:select>
		</aui:column>
		
		<aui:column>
			<aui:input type="submit"  name="submit" label=""  value="開始執行" cssClass="button"/>
		</aui:column>	
	</aui:layout>
</aui:form>	


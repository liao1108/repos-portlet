<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="industryTree" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="currMonth" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Navigator</title>
	
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery.treeview.css"/>
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/screen.css" />
	
	<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.4.4.js" type="text/javascript"></script>
	<script src="<%=renderRequest.getContextPath()%>/js/jquery.cookie.js" type="text/javascript"></script>
	<script src="<%=renderRequest.getContextPath()%>/js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/demo.js"></script>
</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<table width="100%" >
		<tr valign="top">
			<td height="30"><img src="<%=renderRequest.getContextPath()%>/images/red_box.png"/></td>
			<td nowrap>通過檢測&nbsp;</td>
			<td><img src="<%=renderRequest.getContextPath()%>/images/orange_box.png"/></td>
			<td nowrap>檢測中&nbsp;</td>
			<td><img src="<%=renderRequest.getContextPath()%>/images/green_box.png"/></td>
			<td width="100%" align="left"" nowrap>未檢測</td>
		</tr>
	</table>
	<table width="100%" border = "0">
	  <tr>
	   <td>
		<form id="myNaviForm" method="POST" action="<portlet:actionURL/>">
			<ul id="browser" class="filetree">
				<%=industryTree%>
			</ul>
			<input type="hidden" name="currTopic" value="<%=currMonth%>"/>
		</form>
	   </td>
	  </tr>	
	</table>
</body>
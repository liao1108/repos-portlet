package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

public class LawQuizNaviPortlet extends GenericPortlet {

	String lawQuizRootFoler = "";			//金融法規測驗根目錄
	//ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	
	public void init() {
		try{
			viewJSP = getInitParameter("view-template");
			//
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		lawQuizRootFoler = getInitParameter("lawquiz-root-folder");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
    
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
   		String errMsg = "";
   		Connection conn = null;
   		//
   		String industryTree = "";
   		try{
   			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		//long companyId = themeDisplay.getCompanyId();
	   		long groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//long repositoryId = CompanyConstants.SYSTEM;
	   		//
	   		conn = ds.getConnection();
	   		String sql = "";
	   		PreparedStatement ps = null;
	   		/*
	   		String sql = "CREATE TABLE IF NOT EXISTS " +
	   				" trace_law_quiz  (u_id bigint(45) NOT NULL DEFAULT 0," +
	   				" quiz_start_time varchar(20) COLLATE utf8_bin NOT NULL," +
	   				" tot_score int(10) unsigned DEFAULT NULL," +
	   				" got_score int(10) unsigned DEFAULT NULL," +
	   				" org  varchar(45) COLLATE utf8_bin DEFAULT ''," +
	   				" quiz_end_time varchar(20) COLLATE utf8_bin DEFAULT NULL," +
	   				" quiz_data varchar(100) COLLATE utf8_bin DEFAULT NULL," +
	   				" quiz_minutes int(10) unsigned DEFAULT NULL," +
	   				" again_data varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '答錯的題目'," +
	   				" month_uuid varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '月份 Folder ID'," +
	   				" completed int(11) DEFAULT '0' COMMENT '是否完成答對'," +
	   				" PRIMARY KEY (u_id, quiz_start_time, month_uuid) " +
	   				" ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
   			PreparedStatement ps = conn.prepareStatement(sql);
   			ps.execute();
   			*/
	   		//轄管業別
			Hashtable<Industry, Vector<Month>> htIndustry = new Hashtable<Industry, Vector<Month>>();
	   		//
			//System.out.println(eCourseRootFoler);
	   		List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			//System.out.println("Folder: " + fd.getName().trim());
	   			if(fd.getName().startsWith(lawQuizRootFoler)){
	   				//System.out.println(lawQuizRootFoler);
	   				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
	   				for(Folder fd2: list2){			//轄管業別
	   					//System.out.println("Industry: " + fd2.getName());
	   					//各月目錄
						Vector<Month> vecMonth = new Vector<Month>();
						//
	   					List<Folder> list3 = DLAppServiceUtil.getFolders(groupId, fd2.getFolderId());
	   					for(Folder fd3: list3){		//各月
	   						//System.out.println("Month: " + fd3.getName());
		   					if(!fd3.getName().startsWith("_") && !fd3.getName().contains("104年")){	//前置短底線表示不顯示
								String status = "UnTest";
								Month month = new Month();
								month.name = fd3.getName();
								month.id = fd3.getFolderId();
								//
			   					sql = "SELECT * FROM trace_law_quiz WHERE u_id=? " +
			   									" AND month_uuid=? " +
			   									" AND (quiz_end_time IS NOT NULL AND quiz_end_time <> '') " +
			   									" AND tot_score > 0 " +
			   									//" AND got_score = tot_score";
			   									" ORDER BY quiz_end_time DESC";
			   					ps = conn.prepareStatement(sql);
								ps.setLong(1, userId);
								ps.setLong(2, month.id);
								ResultSet rs = ps.executeQuery();
								if(rs.next()){
									if(rs.getInt("completed") == 1){
										status = "Tested";
									}else if(rs.getInt("got_score") > 0){
										status = "Testing";
									}
								}
								rs.close();
								month.status = status;
								//
								vecMonth.add(month);
		   					}
	   					}
						if(vecMonth.size() > 0){
							int _TestedCount= 0;
							int _TestingCount = 0;
							int _UnTestCount = 0;
							for(Month m: vecMonth){
								if(m.status.equalsIgnoreCase("Tested")){
									_TestedCount++;
								}else if(m.status.equalsIgnoreCase("Testing")){
									_TestingCount++;
								}else if(m.status.equalsIgnoreCase("UnTest")){
									_UnTestCount++;
								}
							}
							//
							Industry industry = new Industry();
							industry.name = fd2.getName();
							if(_TestedCount == vecMonth.size()){
								industry.status = "Tested";
							}else if(_UnTestCount == vecMonth.size()){
								industry.status = "UnTest";
							}else{
								industry.status = "Testing";
							}
							//
							htIndustry.put(industry, vecMonth);
						}
	   				}
	   			}
	   		}
	   		//排序
 			Vector<Industry> vecIndustry = new Vector<Industry>();
 			Vector<String> vecSort = new Vector<String>();
 			Enumeration<Industry> en = htIndustry.keys();
 			while(en.hasMoreElements()){
 				vecSort.add(en.nextElement().name);
 			}
 			Collections.sort(vecSort);
 			for(String s: vecSort){
 				en = htIndustry.keys();
 				while(en.hasMoreElements()){
 					Industry ind = en.nextElement();
 					if(ind.name.equals(s)){
 						vecIndustry.add(ind);
 					}
 				}
 			}
 			//行業別Tree
 			for(Industry ind: vecIndustry){
 				String iconTag = "<img src='" + renderRequest.getContextPath() + "/images/green_box.png'>";
 				if(ind.status.equalsIgnoreCase("Testing")){
 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/orange_box.png'>";
 				}else if(ind.status.equalsIgnoreCase("Tested")){
 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/red_box.png'>";
 				}
 				//
				industryTree += "<li class=\"closed\">" + "<span class=\"subject\">" + iconTag + "&nbsp;" + ind.name + "</span>";
				industryTree += "<ul>";
 				//
 				vecSort = new Vector<String>();
 				for(Month mon: htIndustry.get(ind)){
 					vecSort.add(mon.name);
 				}
				Collections.sort(vecSort);
				//
				Vector<Month> vecMonth = new Vector<Month>();
				for(String s: vecSort){
					for(Month mon: htIndustry.get(ind)){
						if(mon.name.equals(s)){
							vecMonth.add(mon);
							break;
						}
					}
				}
				//
 				for(Month mon: vecMonth){
 	 				iconTag = "<img src='" + renderRequest.getContextPath() + "/images/green_box.png'>";
 	 				if(mon.status.equalsIgnoreCase("Testing")){
 	 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/orange_box.png'>";
 	 				}else if(mon.status.equalsIgnoreCase("Tested")){
 	 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/red_box.png'>";
 	 				}
 					//
 					String strLink = "/lawquiz?monthID=" + mon.id;
 					String monthRef = "<a href=" + strLink  + ">" + iconTag + "&nbsp;" + mon.name + "</a>";
 					
 					industryTree += "<li>" + "<span class=\"topic\">" + monthRef + "</span></li>";
 				}
 				industryTree += "</ul>";
 				industryTree += "</li>";
 			}
 			//System.out.println(topicTree);
   		}catch (Exception ex){
   			if(errMsg.equals("")) errMsg += "\n";
   			errMsg +=  getItezErrorCode(ex);
   			//System.out.println(errMsg);
   		}finally{
   			if (conn !=null){
   				try{
   					conn.close();
   				}catch (Exception ignore){
   				}
   			}
   		}
   		//System.out.println(topicTree);
		renderRequest.setAttribute("industryTree", industryTree);
		//
   		renderRequest.setAttribute("errMsg", errMsg);
   		//
   		include(viewJSP, renderRequest, renderResponse);
	}

	protected void include(String path, RenderRequest renderRequest,RenderResponse renderResponse) throws IOException, PortletException {
		PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(path);
		if (portletRequestDispatcher == null) {
			_log.error(path + " is not a valid include");
		}else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}
 
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(LawQuizNaviPortlet.class);

	
	private String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
}

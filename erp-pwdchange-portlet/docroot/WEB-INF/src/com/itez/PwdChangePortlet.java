package com.itez;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.sql.DataSource;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PwdChangePortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void changePassword(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			String password1 = actionRequest.getParameter("password1").trim();
			String password2 = actionRequest.getParameter("password2").trim();
			
			if(password1.isEmpty() || password2.isEmpty()) throw new Exception("密碼不可空白。");
			if(!password1.equals(password2)) throw new Exception("輸入的密碼不一致。");
			//
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		    User user = themeDisplay.getUser();
		    //
		    conn = ds.getConnection();
		    ErpUtil.logUserAction("密碼逾期修改", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
		    //
		    UserLocalServiceUtil.updatePassword(user.getUserId(), password1, password2, false, false);
			//
			actionResponse.sendRedirect("/myreports");
		}catch(Exception ex){
			//String errMsg = ErpUtil.getItezErrorCode(ex);
			//if(errMsg.equals("")){
			String errMsg = ex.getMessage();
			if(errMsg == null || errMsg.trim().isEmpty()) errMsg = "新的密碼不符合密碼政策規則。";
			//}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
}

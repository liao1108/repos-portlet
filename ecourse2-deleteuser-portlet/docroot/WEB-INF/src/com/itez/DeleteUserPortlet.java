package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Portlet implementation class DeleteUserPortlet
 */
public class DeleteUserPortlet extends GenericPortlet {
	ECourseUtils utils = new ECourseUtils();
	Vector<Organization> vecOrg = new Vector<Organization>();
	
    public void init() {
        viewJSP = getInitParameter("view-jsp");
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
    	String strHTML = "";
    	boolean isOrgAdmin = false;
    	try{
	    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		long companyId = themeDisplay.getCompanyId();
	   		long groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//
	   		//
	   		vecOrg = new Vector<Organization>();
	   		
	   		List<Organization> orgs = OrganizationServiceUtil.getUserOrganizations(userId);
	   		for(Organization org: orgs){
	   			if(org.getSuborganizationsSize() > 0){
	   				getSubOrgs(org);
	   			}
	   			if(!vecOrg.contains(org)){
	   				vecOrg.add(org);
	   			}
	   		}
	   		//
	   		Vector<Long> vecUid = new Vector<Long>();
	   		//
	   		for(Organization org: vecOrg){
	   			long[] userIds = UserServiceUtil.getOrganizationUserIds(org.getOrganizationId());
	   			for(long uid: userIds){
	   				if(!vecUid.contains(uid)){
	   					vecUid.add(uid);
	   				}
	   			}
	   		}
			//long[] userIds = UserServiceUtil.getOrganizationUserIds(myOrg.getOrganizationId());
			//
			strHTML = "<table width='100%' border='1'>";
 			strHTML += "<tr>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "撣唾��" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "憪��" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "�����" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "����" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "�銵�" + "</td>";
 			strHTML += "</tr>";
 			for(long uid: vecUid){
				User _user = UserServiceUtil.getUserById(uid);
				if(_user.getUserId() != userId){
					isOrgAdmin = true;
					//
					strHTML += "<form method='POST' action='" + renderResponse.createActionURL() + "'>";
					strHTML += "<input type='HIDDEN' name='userId' value='" + _user.getUserId() + "'>";
	 				//
	 				strHTML += "<tr>";
					strHTML += "<td align='center'>" + _user.getScreenName() + "</td>";
					strHTML += "<td align='center'>" + _user.getFullName() + "</td>";
					if(_user.isActive()){
						strHTML += "<td align='center'>" + "雿銝�" + "</td>";
						strHTML += "<td align='center'>";
						strHTML += "<select name='todo'>"; 
						strHTML += "<option value ='Activate'>銝��</option>";
						strHTML += "<option value ='DeActivate'>��</option>";
						strHTML += "</select>";
						strHTML += "</td>";
					}else{
						strHTML += "<td align='center'>" + "撌脣�" + "</td>";
						strHTML += "<td align='center'>";
						strHTML += "<select name='todo'>"; 
						strHTML += "<option value ='Activate'>��儔</option>";
						strHTML += "<option value ='Delete'>��</option>";
						strHTML += "</select>";
						strHTML += "</td>";
					}
					strHTML += "<td align='center'>";
					strHTML += "<input type='SUBMIT' value='蝣箄��'>";
					strHTML += "</td>";
					strHTML += "</tr>";
					//
					strHTML += "</form>";
				}
 			}
 			strHTML += "</table>";
			//UserServiceUtil.deleteUser(uid);
			//UserLocalServiceUtil.updateStatus(uid, 0);	//撠����儔�Active
    	}catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        	//
        	System.out.println(errMsg);
        }
    	//
    	if(isOrgAdmin){
    		renderRequest.setAttribute("strHTML", strHTML);
    	}else{
    		renderRequest.setAttribute("strHTML", "");
    	}
    	//
        include(viewJSP, renderRequest, renderResponse);
    }
    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
		String errMsg = "";
		try{
	    	long userId = -1;
	    	if(actionRequest.getParameter("userId") != null){
	    		userId = Long.parseLong(actionRequest.getParameter("userId"));
	    	}
			String todo = "";
			if(actionRequest.getParameter("todo") != null){
				todo = actionRequest.getParameter("todo");
	    	}
			if(userId > 0){
				if(todo.equalsIgnoreCase("activate")){
					UserLocalServiceUtil.updateStatus(userId, 0);	//撠����儔�Active
				}else if(todo.equalsIgnoreCase("deactivate")){
					UserLocalServiceUtil.updateStatus(userId, 5);	//撠�����Deactive
				}else if(todo.equalsIgnoreCase("delete")){
					UserServiceUtil.deleteUser(userId);
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
		}
        actionRequest.setAttribute("errMsg", errMsg);
    	//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}
    
    private Organization getDefaultOrg(long userId) throws Exception{
    	Organization org = null;
    	List<Organization> orgs = OrganizationServiceUtil.getUserOrganizations(userId);
    	if(orgs.size() > 0){
    		org = orgs.get(0);
    	}
    	return org;
    }

    private void getSubOrgs(Organization org) throws Exception{
    	List<Organization> list = org.getSuborganizations();
    	for(Organization org2: list){
    		if(org2.getSuborganizationsSize() > 0){
    			getSubOrgs(org2);
    		}
    		if(!vecOrg.contains(org2)){
    			vecOrg.add(org2);
    		}
    	}
    }

    protected void include(String path, RenderRequest renderRequest,
            				RenderResponse renderResponse) throws IOException, PortletException {
        PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(path);
        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }else{
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(DeleteUserPortlet.class);

}

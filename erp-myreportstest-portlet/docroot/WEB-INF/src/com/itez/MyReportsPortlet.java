package com.itez;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
//import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
//import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.http.HttpHeaders;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.itez.alloc.Alloc;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
//import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyReportsPortlet extends MVCPortlet {
	DataSource ds = null;
	
	String rootReports = "";
	String rootJobTemp = "";		//工作範本區
	String rootDocTemp = "";		//文件樣板區
	
	//User user = null;
	
	//Vector<ReportProfile> vecReport = new Vector<ReportProfile>();
	//Vector<ReportProfile> vecReportClosed = new Vector<ReportProfile>();
	//
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootReports = rs.getString("folder_reports");
	   			rootJobTemp = rs.getString("folder_job_temp");
	   			rootDocTemp = rs.getString("folder_doc_temp");
	   		}
	   		rs.close();
	   		//
	   		//sql = "SELECT * FROM exams";
	   		//ps = conn.prepareStatement(sql);
	   		//rs = ps.executeQuery();
	   		//if(!hasColumn(rs, "report_free")) {
	   		//	sql = "ALTER TABLE exams ADD COLUMN report_free VARCHAR(100);";
	   		//	ps = conn.prepareStatement(sql);
	   		//	ps.execute();
	   		//	//
	   		//	sql = "ALTER TABLE exams ADD COLUMN free_reason VARCHAR(200);";
	   		//	ps = conn.prepareStatement(sql);
	   		//	ps.execute();
	   		//}
	   		//rs.close();
	   		//
	   		//sql = "CREATE TABLE IF NOT EXISTS report_free_log ("
			//			    				+ " time_stamp VARCHAR(50),"
			//			    				+ " exam_id INTEGER, "
			//			    				+ " modify_by VARCHAR(50),"
			//			    				+ " report_free_1 VARCHAR(100),"
			//			    				+ " free_reason_1 VARCHAR(200),"
			//			    				+ " report_free_2 VARCHAR(100),"
			//			    				+ " free_reason_2 VARCHAR(200),"
			//			    				+ " PRIMARY KEY (time_stamp) )";
    		//ps = conn.prepareStatement(sql);
    		//ps.execute();
	   		//
	   		//conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally {
			try {
				if(conn != null) conn.close();
			}catch(Exception _ex) {
			}
		}
	}
	
	private boolean hasColumn(ResultSet rs, String columnName) throws Exception {
	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columns = rsmd.getColumnCount();
	    for (int x = 1; x <= columns; x++) {
	        if (columnName.equalsIgnoreCase(rsmd.getColumnName(x))) {
	            return true;
	        }
	    }
	    return false;
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		//int reportId = 0;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			User user = themeDisplay.getUser();
			//
			conn = ds.getConnection();
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//選擇組別與行業別
			AlfrescoFolder alfJobTemp = this.getJobTempRootFolder(alfSessionAdmin);
			if(alfJobTemp == null) throw new Exception("檢查報告工作範本區找不到！");
			//
			Vector<String> vecSecAll = new Vector<String>();	//組別
			Vector<String> vecSecIndus = new Vector<String>();	//組別+業別
			Hashtable<String, Vector<String>> htJson = new Hashtable<String, Vector<String>>();
			//Hashtable<String, Vector<String>> htIndusType = new Hashtable<String, Vector<String>>();
			Iterator<CmisObject> it = alfJobTemp.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
					AlfrescoFolder alfSec = (AlfrescoFolder)co;
					vecSecAll.add(alfSec.getName());
					//
					Iterator<CmisObject> it2 = alfSec.getChildren().iterator();
					while(it2.hasNext()){
						CmisObject co2 = it2.next();
						if(co2.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
							AlfrescoFolder alfIndus = (AlfrescoFolder)co2;
							//
							String key = alfSec.getName() + "/" + alfIndus.getName();
							vecSecIndus.add(key);
							//取得工作分配的 Json
							Vector<String> vecJson = new Vector<String>();
							Iterator<CmisObject> it3 = alfIndus.getChildren().iterator();
							while(it3.hasNext()){
								CmisObject co3 = it3.next();
								if(co3.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && co3.getName().toLowerCase().endsWith("json")){
									AlfrescoDocument alfDoc = (AlfrescoDocument)co3;
									if(alfDoc.getPropertyValue("it:docStatus") == null || !alfDoc.getPropertyValue("it:docStatus").equals("停用")){
										vecJson.add(co3.getName());
									}
								}
							}
							//
							htJson.put(key, vecJson);
						}
					}
				}
			}
			renderRequest.getPortletSession(true).setAttribute("vecSecIndus", vecSecIndus);
			renderRequest.getPortletSession(true).setAttribute("htJson", htJson);
			//
			Vector<String> vecSecAvail = new Vector<String>();
			if(user.getJobTitle().contains("組長")){
				vecSecAvail = this.getAvailSection(user);
			}
			renderRequest.getPortletSession(true).setAttribute("vecSecAvail", vecSecAvail);
			//是否為 檔案上傳 管理人
			boolean isUploadManager = false;
			try {
				for(Role _r: user.getRoles()){
					if(_r.getName().toLowerCase().contains("uploadmanager")){
						isUploadManager = true;
						break;
					}
				}
			}catch(Exception _ex) {
			}
			
			if(isUploadManager) {
				renderRequest.getPortletSession(true).setAttribute("isUploadManager", "true");
			}else {
				renderRequest.getPortletSession(true).setAttribute("isUploadManager", "false");
			}
			//
			Collec collec = this.getReports(themeDisplay, conn, userFullName, "", "", "", alfSessionAdmin, vecSecAvail);
			//
			//renderRequest.getPortletSession(true).setAttribute("vecReport", collec.vecReport);
			//renderRequest.getPortletSession(true).setAttribute("vecReportClosed", collec.vecReportClosed);
			renderRequest.getPortletSession(true).setAttribute("collec", collec);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
			//
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			//判斷報告案清單列表權限
			Vector<String> vecSecPriv = new Vector<String>(); 
			//檢查是否系統管理者
			PermissionChecker pc =  PermissionCheckerFactoryUtil.create(themeDisplay.getUser());
			if(pc.isOmniadmin()){
				vecSecPriv.add("*");
				for(String s: vecSecAll){
					vecSecPriv.add(s);
				}
				if(!vecSecPriv.contains("檢查制度組")) vecSecPriv.add("檢查制度組");
				//
				renderRequest.getPortletSession(true).setAttribute("isAdmin", "true");
			}else{
				boolean secReporter = false;
				List<Role> listRole = user.getRoles();
				for(Role _r: listRole){
					if(_r.getName().contains("報告列表")){
						secReporter = true;
						break;
					}
				}
				if(secReporter || isUploadManager){
					List<UserGroup> listUserGroup = user.getUserGroups();
					for(UserGroup ug: listUserGroup){
						if(ug.getName().equals("金控公司組") || 
								ug.getName().equals("本國銀行組") || 
								ug.getName().equals("保險外銀組") || 
								ug.getName().equals("證券票券組") || 
								ug.getName().equals("地方金融組")){
							for(String s: vecSecAll){
								if(s.contains(ug.getName())){
									vecSecPriv.add(s);
									break;
								}
							}
						}else if(ug.getName().equals("檢查制度組")) {
							if(!vecSecPriv.contains("檢查制度組")) vecSecPriv.add("檢查制度組");
						}
					}
				}
				if(secReporter) {
					renderRequest.getPortletSession(true).setAttribute("isSecReporter", "true");
				}else {
					renderRequest.getPortletSession(true).setAttribute("isSecReporter", "false");
				}
				renderRequest.getPortletSession(true).setAttribute("isAdmin", "false");
			}
			renderRequest.getPortletSession(true).setAttribute("vecSecPriv", vecSecPriv);
			//
			//目前登入使用者隸屬於哪一個組別
			String secBelongTo = "";
			List<UserGroup> listUserGroup = user.getUserGroups();
			for(UserGroup ug: listUserGroup){
				if(ug.getName().equals("金控公司組") || 
						ug.getName().equals("本國銀行組") || 
						ug.getName().equals("保險外銀組") || 
						ug.getName().equals("證券票券組") || 
						ug.getName().equals("地方金融組")){
					for(String s: vecSecAll){
						if(s.contains(ug.getName())){
							secBelongTo = s;
							break;
						}
					}
				}else if(ug.getName().equals("檢查制度組")) {
					secBelongTo = "檢查制度組";
				}
				//
				if(!secBelongTo.equals("")) break;
			}
			renderRequest.getPortletSession(true).setAttribute("secBelongTo", secBelongTo);
			renderRequest.getPortletSession(true).setAttribute("vecSecAll", vecSecAll);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	
	//副組長以上人員可用的組
	private Vector<String> getAvailSection(User user) throws Exception{
		Vector<String> vecSecAvail = new Vector<String>();
		if(user.getJobTitle().contains("組長")){
			try {
				List<UserGroup> listUserGroup = user.getUserGroups();
				for(UserGroup ug: listUserGroup){
					if(ug.getName().equals("金控公司組")){
						vecSecAvail.add("H"); 
					}else if(ug.getName().equals("本國銀行組")){
						vecSecAvail.add("B");
					}else if(ug.getName().equals("保險外銀組")){
						vecSecAvail.add("F");
					}else if(ug.getName().equals("證券票券組")){
						vecSecAvail.add("S"); 
					}else if(ug.getName().equals("地方金融組")){
						vecSecAvail.add("L");
					//}else if(ug.getName().equals("受託檢查組")){
					//	vecSecAvail.add();
					}
				}
			}catch(Exception ex) {
			}
		}
		return vecSecAvail;
	}
	
	//取得進行中或已歸檔的專案
	/*
	public Vector<ExamInfo> getExams(ThemeDisplay themeDisplay, Connection conn, String userFullName) throws Exception{
		Vector<ExamInfo> vecExam = new Vector<ExamInfo>();
		//
		String sql = "SELECT * FROM reports WHERE (date_closed IS NULL OR date_closed = '') ORDER BY date_start DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			int reportId = rs.getInt("report_id");
			int folderIdReport = rs.getInt("folder_id");
			String caseName = rs.getString("case_name");
			String reportNo = rs.getString("report_no");
			String pmName = "";
			if(rs.getString("pm_name") != null){
				pmName = rs.getString("pm_name");
			}
			String createdBy = rs.getString("created_by");
			//
			boolean examFound = false;
			sql = "SELECT * FROM exams WHERE report_id = ? " +
										" AND (leader_name =? OR staffs LIKE '%" + userFullName +"%') ";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, reportId);
			ps.setString(2, userFullName);
			ResultSet rs2 = ps.executeQuery();
			while(rs2.next()){
				ExamInfo ei = new ExamInfo();
				ei.caseName = caseName;
				ei.reportId = reportId;
				ei.reportNo = reportNo;
				ei.examId = rs2.getInt("exam_id");
				ei.examNo = rs2.getString("exam_no");
				ei.bankName = rs2.getString("bank_name");
				//
				vecExam.add(ei);
				//
				examFound = true;
			}
			if(!examFound){
				if(pmName.contains(userFullName) || createdBy.contains(userFullName)){
					ExamInfo ei = new ExamInfo();
					ei.caseName = caseName;
					ei.reportId = reportId;
					ei.reportNo = reportNo;
					//
					vecExam.add(ei);
				}
			}
		}
		//
		return vecExam;
	}
	*/
	
	//取得進行中或已歸檔的專案
	public Collec getReports(ThemeDisplay themeDisplay,
							Connection conn,
							String userFullName,
							String secName,
							String dateStart,
							String dateEnd,
							Session alfSessionAdmin,
							Vector<String> vecSecAvail) throws Exception{
		Vector<ReportProfile> vecReport = new Vector<ReportProfile>();
		Vector<ReportProfile> vecReportClosed = new Vector<ReportProfile>();
		//
		boolean isSupervisor = false;			//是否為組別副組長以上人員
		if(vecSecAvail.size() > 0) isSupervisor = true;
		//
		String sql = "SELECT * FROM reports WHERE 1= 1 ";
		if(isSupervisor){
			sql += " AND (";
			int k=0;
			for(String s: vecSecAvail){
				sql += " (sec_name LIKE '" + s.trim() + "%') ";
				if(k >= 0 && k < vecSecAvail.size() -1){
					sql += " OR ";
				}
				k++;
			}
			sql += " OR pm_name LIKE '%" + userFullName + "%' "
				 + " OR created_by LIKE '%" + userFullName + "%' "
				 + " OR leader_name LIKE '%" + userFullName + "%' "
				 + " OR staffs LIKE '%" + userFullName + "%' ";
			sql += ")";
		}else if(!userFullName.equals("")){
			sql += " AND (pm_name LIKE '%" + userFullName + "%'  OR created_by LIKE '%" + userFullName + "%' OR leader_name LIKE '%" + userFullName + "%' OR staffs LIKE '%" + userFullName + "%')  ";
		}else{
			if(!secName.equals("*")){
				sql += " AND sec_name = '" + secName + "' "; 
			}
			if(!dateStart.equals("") && !dateEnd.equals("")){
				dateStart += " 00:00:00";
				dateEnd += " 23:59:59";
				sql += " AND date_created >= '" + dateStart + "' AND date_created <='" + dateEnd + "' ";
			}else if(!dateStart.equals("")){
				dateStart += " 00:00:00";
				sql += " AND date_created >= '" + dateStart + "' ";
			}else if(!dateEnd.equals("")){
				dateEnd += " 23:59:59";
				sql += " AND date_created <='" + dateEnd + "' ";
			}
		}
		sql += " ORDER BY date_created DESC";
		
		//System.out.println("SQL: " + sql);
		
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			ReportProfile ri = new ReportProfile();
			//
			ri.reportId = rs.getInt("report_id");
			ri.dateCreated = rs.getString("date_created");
			ri.reportNo = rs.getString("report_no");
			ri.caseName = rs.getString("case_name");
			ri.secName = rs.getString("sec_name");
			ri.folderId = rs.getString("folder_id");
			//ri.dateStart = rs.getString("date_start");
			//ri.dateEnd = rs.getString("date_end");
			ri.dateBase = rs.getString("date_base");
			ri.dateReporting = rs.getString("date_reporting");
			ri.indusType = rs.getString("indus_type");
			if(rs.getString("sec_belong_to") != null) ri.secBelongTo = rs.getString("sec_belong_to");
			
			ri.reportSecIndus = ri.secName + "/" + ri.indusType;
			if(rs.getString("job_sec_indus") != null){
				ri.jobSecIndus = rs.getString("job_sec_indus");
			}
			ri.reportYear = rs.getInt("report_year");
			if(rs.getString("pm_name") != null){
				ri.pmName = rs.getString("pm_name");
			}
			ri.createdBy = rs.getString("created_by");
			
			ri.dropped = rs.getInt("dropped");				//是否作廢
			
			if(rs.getString("date_closed") != null){			//關閉日
				ri.dateClosed = rs.getString("date_closed");
			}
			if(rs.getString("closed_by") != null){			//關閉人
				ri.closedBy = rs.getString("closed_by");
				if("auto".equalsIgnoreCase(ri.closedBy)){
					ri.closedBy="系統關閉";
				}
			}
			ri.reportFact = rs.getString("sec_name") + "/" + rs.getString("indus_type");
			if(rs.getString("leader_name") != null){			//領隊
				ri.leaderName = rs.getString("leader_name");
			}
			if(rs.getString("staffs") != null){			//領隊
				ri.staffs = rs.getString("staffs");
			}
			if(ri.createdBy.contains(userFullName) || ri.pmName.contains(userFullName) || ri.leaderName.contains(userFullName)){
				ri.isLeader = true;
			}
			//讀取檢查證號
			ri.vecExam = this.getExams(ri.reportId, conn, alfSessionAdmin);
			
			//載入異動紀錄
			Vector<TxLog> vecTxLog = new Vector<TxLog>(); 
			sql = "SELECT * FROM report_log WHERE report_id = ? ORDER BY tx_date";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, ri.reportId);
			ResultSet rs3 = ps.executeQuery();
			while(rs3.next()){
				TxLog tx = new TxLog();
				tx.txDate = rs3.getString("tx_date");
				tx.txType = rs3.getString("tx_type");
				tx.txBy = rs3.getString("tx_by");
				vecTxLog.add(tx);
			}
			rs3.close();
			ri.vecTxLog = vecTxLog;	
			//
			if(rs.getString("date_closed") != null && !rs.getString("date_closed").trim().equals("")){
				vecReportClosed.add(ri);	
			}else{
				vecReport.add(ri);
			}
		}
		Collec collec = new Collec();
		collec.vecReport = vecReport;
		collec.vecReportClosed = vecReportClosed;
		collec.isSupervisor = isSupervisor;
		//
		return collec;
	}

	/*
	public void createReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			//System.out.println("Project Id: " + projectId);
			//
			QName qName = new QName("http://itez.com", "sendReportId", "x");
			 actionResponse.setEvent(qName, String.valueOf(reportId) + "#true");
			 //
			 actionRequest.setAttribute("successMsg", "請由右方視窗建立新的檢查報告。");
			 SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	 */
	
	//載入並編輯
	/*
	public void editReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			//System.out.println("Project Id: " + projectId);
			//
			QName qName = new QName("http://itez.com", "sendReportId", "x");
			 actionResponse.setEvent(qName, String.valueOf(reportId) + "#true");
			 //
			 actionRequest.setAttribute("successMsg", "案件資料請由右方視窗編輯。");
			 SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	*/
	
	//更新報告案基本資料
	public void updateReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		int reportId = 0;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//取得登入人密碼
			//String password = (String)PortalUtil.getHttpServletRequest(actionRequest).getSession().getAttribute(WebKeys.USER_PASSWORD);
			//
			if(!ErpUtil.isExamUser(themeDisplay.getUser(), themeDisplay)) throw new Exception("您目前不在檢查人員名單內，無法建立檢查案。");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			if(alfSessionAdmin == null) throw new Exception("無法取得 Alfresco 管理者連線！");
			//
			conn = ds.getConnection();
			//
			AlfrescoFolder fdDocRootAlf = this.getReportsRootFolder(alfSessionAdmin);
			if(fdDocRootAlf == null) throw new Exception("檢查報告文件儲存區不存在！");
			//
			reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			//
			ReportProfile r = new ReportProfile();
			r.reportId = reportId;
			r.folderId = actionRequest.getParameter("folderId").trim();
			r.caseName = actionRequest.getParameter("caseName").trim();
			r.reportSecIndus = actionRequest.getParameter("reportSecIndus").trim();
			if(r.reportSecIndus.equals("")) throw new Exception("請指定本案的組別與行業別。");
			
			r.secBelongTo = actionRequest.getParameter("secBelongTo").trim();
			if(r.secBelongTo.equals("")) throw new Exception("請指定本案歸屬的組別。");
			
			r.secName = r.reportSecIndus.split("/")[0].trim();
			r.indusType = r.reportSecIndus.split("/")[1].trim();
			//
			r.jobSecIndus = actionRequest.getParameter("jobSecIndus").trim();
			if(r.jobSecIndus.equals("")) throw new Exception("請指定本案的工作範本別。");
			//
			r.reportYear = 0;
			try{
				r.reportYear = Integer.parseInt(actionRequest.getParameter("reportYear").toString());
			}catch(Exception _ex){
			}
			r.pmName = actionRequest.getParameter("pmName").trim();
			if(!r.pmName.equals("")){
				String[] arrName = r.pmName.split(";");
				for(String name: arrName){
					if(!ErpUtil.isExamUser(themeDisplay.getUser(), themeDisplay)) throw new Exception(name + "不在檢查人員名單內。");
				}
			}
			if(r.reportYear == 0) throw new Exception("報告年度有誤，請重新輸入。");
			if(actionRequest.getParameter("reportNo") != null) r.reportNo = actionRequest.getParameter("reportNo");
			//r.dateStart = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearStart"), actionRequest.getParameter("monStart"), actionRequest.getParameter("dayStart"));
			//r.dateEnd = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearEnd"), actionRequest.getParameter("monEnd"), actionRequest.getParameter("dayEnd"));
			r.dateBase = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearBase"), actionRequest.getParameter("monBase"), actionRequest.getParameter("dayBase"));
			r.dateReporting = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearReporting"), actionRequest.getParameter("monReporting"), actionRequest.getParameter("dayReporting"));
			//String userFullName = actionRequest.getParameter("userFullName").trim();
			//if(r.dateBase.compareTo(r.dateStart) > 0) throw new Exception("檢查基準日不可能晚於開始檢查日。");
			//if(r.dateStart.compareTo(r.dateEnd) > 0) throw new Exception("開始檢查日不可能晚於完成檢查日。");
			//if(r.dateEnd.compareTo(r.dateReporting) > 0) throw new Exception("完成提出日不可能晚於報告提出日。");
			//
			if(r.reportId == 0){
				ErpUtil.logUserAction("建立報告案", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//
				r.dateCreated = ErpUtil.getCurrDateTime();
				//產出報告編號
				String secNo = String.valueOf(r.secName.charAt(0));
				if(secNo.equals("F")){
					if(r.indusType.contains("險")){
						secNo = "F1"; 			//保險
					}else{
						secNo = "F0";			//外銀
					}
				}
				//搜尋是否已經啟用
				String sql = "SELECT * FROM reports WHERE report_no LIKE '%" + secNo + "%'";
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					int seq = 0;
					//		
					String startNo = r.reportYear + secNo;
					sql = "SELECT MAX(report_no) FROM reports WHERE report_no LIKE '" + startNo + "%'";
					ps = conn.prepareStatement(sql);
					rs = ps.executeQuery();
					if(rs.next() && rs.getString(1) != null && !rs.getString(1).equals("")){
						if(secNo.startsWith("F")){
							seq = Integer.parseInt(rs.getString(1).substring(5));
							seq++;
							//
							r.reportNo = startNo + String.format("%02d", seq);
						}else{
							seq = Integer.parseInt(rs.getString(1).substring(4));
							seq++;
							//
							r.reportNo = startNo + String.format("%03d", seq);
						}
					}else{
						if(secNo.startsWith("F")){
							r.reportNo = startNo + String.format("%02d", 1);
						}else{
							r.reportNo = startNo + String.format("%03d", 1);
						}
					}
				}else{		//找 report_no_start 表格
					sql = "SELECT report_no FROM report_no_start WHERE sec_no=?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, secNo);
					rs = ps.executeQuery();
					if(rs.next()){
						r.reportNo = rs.getString(1);
					}
				}
				rs.close();
				//
				if(r.reportNo.equals("")) throw new Exception("報告編號無法產出，請先確認 report_no_start 表格是否設定各組啟用時的編號。");
				//建立實體目錄
				boolean existed = false;
				Iterator<CmisObject> it = fdDocRootAlf.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getName().equals(r.reportNo)){
						r.folderId = co.getId();
						existed = true;
						//throw new Exception("報告編號：" + r.reportNo + " 儲存區已存在，請聯絡系統管理人員處理。");
					}
				}
				//
				if(!existed){
					Map<String, String>  props = new HashMap<String,String>();
					props.put(PropertyIds.NAME, r.reportNo);
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
					ObjectId oid = alfSessionAdmin.createFolder(props, fdDocRootAlf);
					//指定權限
					r.folderId = oid.getId();
				}
				//
				sql = "INSERT INTO reports (case_name, " +			//1
											" sec_name," +			//2
											" indus_type," +		//3
											" job_sec_indus," +		//4
											" report_year, " +		//5
											" date_created," +		//6
											" date_base," +			//7
											" date_reporting," +	//8
											" pm_name," +			//9
											" created_by," +		//10
											" report_no," +			//11
											" folder_id,  " +		//12
											" sec_belong_to) " +	//13
										" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setString(1, r.caseName);
				ps.setString(2, r.secName);
				ps.setString(3, r.indusType);
				ps.setString(4, r.jobSecIndus);
				ps.setInt(5, r.reportYear);
				ps.setString(6, r.dateCreated);
				ps.setString(7, r.dateBase);
				ps.setString(8, r.dateReporting);
				ps.setString(9, r.pmName);
				ps.setString(10, userFullName);
				ps.setString(11, r.reportNo);
				ps.setString(12, r.folderId);
				ps.setString(13, r.secBelongTo);
				ps.execute();
				//回查 report_id
				sql = "SELECT report_id FROM reports WHERE report_no=? " +			//1
													"AND sec_name=? " +		//2
													"AND indus_type=? " +		//3
													" AND report_year=? " +		//4
													"AND date_created=? " +		//5
													"AND folder_id=?";			//6
				ps = conn.prepareStatement(sql);
				ps.setString(1, r.reportNo);
				ps.setString(2, r.secName);
				ps.setString(3, r.indusType);
				ps.setInt(4, r.reportYear);
				ps.setString(5, r.dateCreated);
				ps.setString(6, r.folderId);
				rs = ps.executeQuery();
				if(rs.next()){
					r.reportId = rs.getInt(1);
				}
				rs.close();
			}else{
				//找出原 ReportProfile
				ReportProfile origRP = null;
				try {
					Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
					for(ReportProfile _rp: collec.vecReport) {
						if(_rp.reportId == r.reportId) {
							origRP = _rp;
							break;
						}
					}
				}catch(Exception _ex) {
					_ex.printStackTrace();
				}
				//找到報告目錄
				AlfrescoFolder fd = null;
				try{
					if(!r.folderId.equals("")){
						fd = (AlfrescoFolder)alfSessionAdmin.getObject(r.folderId);
					}
				}catch(Exception _ex){
				}
				if(fd == null){
					Map<String, String>  props = new HashMap<String,String>();
					props.put(PropertyIds.NAME, r.reportNo);
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
					ObjectId oid = alfSessionAdmin.createFolder(props, fdDocRootAlf);
					//指定權限
					r.folderId = oid.getId();
					fd = (AlfrescoFolder)alfSessionAdmin.getObject(r.folderId);
					//throw new Exception("本案「" + r.reportNo + "」之報告儲存區不存在，請聯絡系統人員處理。"); 
				}
				if(fd == null) throw new Exception("檢查報告目錄不存在。");
				//檢查報告年度與編號
				if(!r.reportNo.startsWith(String.valueOf(r.reportYear))) throw new Exception("報告年度與報告編號不一致，請調整。");
				if(!r.secName.startsWith(String.valueOf(r.reportNo.charAt(3)))) throw new Exception("組別與報告編號不一致，請調整。");
				//Log
				ErpUtil.logUserAction("更新報告案", fd.getName(), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//更名
				if(fd != null && !r.reportNo.equals(fd.getName())){
					Map<String, String>  props = new HashMap<String,String>();
					props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
					props.put(PropertyIds.NAME, r.reportNo);
					fd.updateProperties(props);
				}
				//工作範本別有異動
				if(origRP != null && !origRP.jobSecIndus.equals(r.jobSecIndus) ) {
					for(CmisObject co: fd.getChildren()) {
						if(co instanceof org.apache.chemistry.opencmis.client.api.Document) continue;
						//
						for(CmisObject co2: ((AlfrescoFolder)co).getChildren()) {
							if(co2 instanceof org.apache.chemistry.opencmis.client.api.Document) continue;
							if(!co2.getName().contains("行前")) continue;
							//
							for(CmisObject co3: ((AlfrescoFolder)co2).getChildren()) {
								if(co3 instanceof org.apache.chemistry.opencmis.client.api.Folder) continue;
								if(!co3.getName().endsWith("json")) continue;
								//
								Map<String, String>  props = new HashMap<String,String>();
								props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
								props.put(PropertyIds.NAME, co3.getName() + ".bak");
								co3.updateProperties(props);
							}
						}
					}
				}
				//
				String sql = "UPDATE reports SET case_name=?, " +	//1
										" sec_name=?," +			//2
										" indus_type=?," +			//3
										" job_sec_indus=?," +		//4
										" report_year=?, " +		//5
										" date_base=?," +			//6
										" date_reporting=?, " +		//7
										" pm_name=?, " +			//8
										" sec_belong_to=? " +		//9
								" WHERE report_id = ?  ";			//10
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, r.caseName);
				ps.setString(2, r.secName);
				ps.setString(3, r.indusType);
				ps.setString(4, r.jobSecIndus);
				ps.setInt(5, r.reportYear);
				ps.setString(6, r.dateBase);
				ps.setString(7, r.dateReporting);
				ps.setString(8, r.pmName);
				ps.setString(9, r.secBelongTo);
				ps.setInt(10, r.reportId);
				
				ps.execute();
				//加入異動紀錄
				this.addReportLog(r.reportId, "更新報告案資料", userFullName, conn);
			}
			//r = this.loadReport(r.reportId, conn);
			//actionRequest.getPortletSession(true).setAttribute("reportProfile", r);
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/jsp/myreports/editReport.jsp");
			actionResponse.setRenderParameter("reportId", String.valueOf(reportId));
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//關閉報告案
	public void doCloseReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			String reportNo = actionRequest.getParameter("reportNo");
			//
			String dateClosed = ErpUtil.getCurrDateTime();
			conn = ds.getConnection();
			//Log
			ErpUtil.logUserAction("關閉報告案", reportNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "SELECT 1 FROM todo_list WHERE (done IS NULL OR done=0) AND report_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, reportNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("報告編號 " + reportNo + " 尚有檔案在待辦事項中，無法關閉。");
			}
			rs.close();
			//
			sql = "UPDATE reports SET date_closed=?, closed_by=? WHERE report_id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dateClosed);
			ps.setString(2, userFullName);
			ps.setInt(3, reportId);
			ps.execute();
			//新增異動紀錄
			this.addReportLog(reportId, "關閉", userFullName, conn);
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//新增異動紀錄
	private void addReportLog(int reportId, String txType, String txBy, Connection conn) throws Exception{
		String sql = "INSERT INTO report_log (report_id," +		//1
											"tx_type," +		//2
											"tx_date," +		//3
											"tx_by) " +		//4
										" VALUES (?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, reportId);
		ps.setString(2, txType);
		ps.setString(3, ErpUtil.getCurrDateTimeSSS());
		ps.setString(4, txBy);
		ps.execute();
	}
	
	//作廢報告案
	public void doDropReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			String reportNo = actionRequest.getParameter("reportNo");
			//
			String dateClosed = ErpUtil.getCurrDateTime();
			conn = ds.getConnection();
			//Log
			ErpUtil.logUserAction("作廢報告案", reportNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "SELECT 1 FROM todo_list WHERE (done IS NULL OR done=0) AND report_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, reportNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("報告編號 " + reportNo + " 尚有檔案在待辦事項中，無法作廢。");
			}
			rs.close();
			//
			sql = "SELECT 1 FROM exams WHERE report_id =? AND dropped = 0";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, reportId);
			rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("報告編號 " + reportNo + " 尚有未作廢的檢查證，無法作廢。");
			}
					
			sql = "UPDATE reports SET dropped = 1, date_closed=?, closed_by=? WHERE report_id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dateClosed);
			ps.setString(2, userFullName);
			ps.setInt(3, reportId);
			ps.execute();
			//新增異動紀錄
			this.addReportLog(reportId, "作廢", userFullName, conn);
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//作廢報告案
	public void doDropExam(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			int examId = Integer.parseInt(actionRequest.getParameter("examId"));
			String examNo = actionRequest.getParameter("examNo");
			//
			String dateClosed = ErpUtil.getCurrDateTime();
			conn = ds.getConnection();
			//Log
			ErpUtil.logUserAction("作廢檢查證", examNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "SELECT 1 FROM todo_list WHERE (done IS NULL OR done=0) AND exam_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, examNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				throw new Exception("檢查證號 " + examNo + " 尚有檔案在待辦事項中，無法作廢。");
			}
			rs.close();
			//
			sql = "UPDATE exams SET dropped = 1 WHERE exam_id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ps.execute();
			//新增異動紀錄
			this.addReportLog(reportId, "作廢檢查證號", userFullName, conn);
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
		
	//載入檢查證號
	private Vector<ExamProfile> getExams(int reportId, Connection conn, Session alfSessionAdmin) throws Exception{
		Vector<ExamProfile> vecExam = new Vector<ExamProfile>(); 
		String sql = "SELECT A.*, B.pm_name, B.date_closed FROM exams A, reports B WHERE A.report_id = B.report_id AND A.report_id = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, reportId);
		ResultSet rs2 = ps.executeQuery();
		while(rs2.next()){
			ExamProfile ep = new ExamProfile();
			ep.reportId = reportId;
			ep.examId = rs2.getInt("exam_id");
			if(rs2.getString("exam_no") != null && !rs2.getString("exam_no").equals("")){
				ep.examNo = rs2.getString("exam_no");
				if(!ep.examNo.startsWith("E")) ep.examNoType = "持函檢查";
			}
			ep.folderId = rs2.getString("folder_id");
			ep.leaderName = rs2.getString("leader_name");
			ep.staffs = rs2.getString("staffs");
			//2018-07-06 修改
			if(rs2.getString("report_free") != null) ep.reportFree = rs2.getString("report_free"); 
			if(rs2.getString("free_reason") != null) ep.freeReason = rs2.getString("free_reason");
			
			ep.dateCreated = rs2.getString("date_created");
			ep.dateStart = rs2.getString("date_start");
			ep.dateEnd = rs2.getString("date_end");
			ep.bankName = rs2.getString("bank_name");
			ep.bankNameShort = rs2.getString("bank_name_short");
			ep.createdBy = rs2.getString("created_by");
			ep.examDays = rs2.getFloat("exam_days");
			if(rs2.getString("json_name") != null) ep.jsonName = rs2.getString("json_name");
			if(rs2.getString("doc_temp_path") != null) ep.docTempPath = rs2.getString("doc_temp_path");
			ep.noMeeting = false;
			if(rs2.getInt("no_meeting") == 1) ep.noMeeting = true;
			
			if(!ep.noMeeting && rs2.getString("date_meeting") != null) {
				ep.dateMeeting = rs2.getString("date_meeting");
			}
			
			if(rs2.getString("date_approval") != null) ep.dateApproval = rs2.getString("date_approval");
			if(rs2.getString("verifiers") != null) ep.verifiers = rs2.getString("verifiers");
			if(rs2.getString("deadline_staff") != null) ep.deadlineStaff = rs2.getString("deadline_staff");
			if(rs2.getString("pm_name") != null) ep.pmName = rs2.getString("pm_name");
			//ep.evalAllocateTiming = rs2.getInt("eval_allocate_timing");
			ep.defectWithBisTitle = rs2.getInt("defect_with_bis_title");
			
			ep.dropped = rs2.getInt("dropped");	//是否作廢
			if(rs2.getString("exam_nature") != null) ep.examNature = rs2.getString("exam_nature"); 
			
			if(rs2.getString("date_closed") != null && !rs2.getString("date_closed").equals("")){
				ep.closed = 1;
			}
			/*
			//判斷已經到達的階段
			Vector<String> vecStage = new Vector<String>();
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ep.folderId);
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
					if(!co.getName().contains("行前") && !co.getName().contains("期間")){
						vecStage.add(co.getName() + "#" + co.getId());
					}
				}
			}
			Collections.sort(vecStage);
			//
			String prevStage = "尚未檢查結束";
			for(String s: vecStage){
				String stageName = s.split("#")[0];
				String fdId = s.split("#")[1];
				//
				boolean fileExist = false;
				AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(fdId);
				it = fdStage.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
						fileExist = true;
						break;
					}
				}
				if(fileExist){
					prevStage = stageName;
				}else{
					break;
				}
			}
			ep.stageReached = prevStage;
			*/
			//
			vecExam.add(ep);
		}
		//
		return vecExam;
	}
	
	//判斷已經到達的階段
	private String getExamProgress(Session alfSessionAdmin, String examFolderId) throws Exception{
		//判斷已經到達的階段
		Vector<String> vecStage = new Vector<String>();
		AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(examFolderId);
		Iterator<CmisObject> it = fdExam.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
				if(!co.getName().contains("行前") && !co.getName().contains("期間")){
					vecStage.add(co.getName() + "#" + co.getId());
				}
			}
		}
		Collections.sort(vecStage);
		//
		String prevStage = "尚未檢查結束";
		for(String s: vecStage){
			String stageName = s.split("#")[0];
			String fdId = s.split("#")[1];
			//
			boolean fileExist = false;
			AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(fdId);
			it = fdStage.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
					fileExist = true;
					break;
				}
			}
			if(fileExist){
				prevStage = stageName;
			}else{
				break;
			}
		}
		return prevStage;		
	}

	//重啟報告案
	public void doReOpenReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			conn = ds.getConnection();
			String sql = "SELECT report_no FROM reports WHERE report_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reportId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				//Log
				ErpUtil.logUserAction("重啟報告案", rs.getString(1), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			}
			rs.close();
			//
			sql = "UPDATE reports SET date_closed=?, closed_by=? WHERE report_id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, "");
			ps.setString(2, "");
			ps.setInt(3, reportId);
			ps.execute();
			//新增異動紀錄
			this.addReportLog(reportId, "重新啟用", userFullName, conn);
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	//更新檢查證基本資料
	public void updateExam(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		int reportId = 0;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			int examId = Integer.parseInt(actionRequest.getParameter("examId"));
			//
			ExamProfile ep = new ExamProfile();
			ep.reportId = reportId;
			ep.examId = examId;
			//
			ReportProfile r = new ReportProfile();
			Collec collec =(Collec)actionRequest.getPortletSession(true).getAttribute("collec");
			//Vector<ReportProfile> vecReport = (Vector<ReportProfile>)actionRequest.getPortletSession(true).getAttribute("vecReport");
			for(ReportProfile _rp: collec.vecReport){
				if(_rp.reportId == ep.reportId){
					r = _rp;
					break;
				}
			}
			if(r.reportId == 0) throw new Exception("報告代碼 " + ep.reportId + " 找不到。");
			//
			ep.folderId = actionRequest.getParameter("folderId").trim();
			ep.examNo = actionRequest.getParameter("examNo").trim().toUpperCase();
			ep.examNoType = actionRequest.getParameter("examNoType").trim();
			if(ep.examNoType.equals("持證檢查")){
				if(ep.examNo.length() != 7 || !ep.examNo.startsWith("E")) throw new Exception("檢查證須以E開頭加上六位數字編號。");
			}else{
				if(ep.examNo.length() != 12) throw new Exception("持函檢查請輸入檢查行政資訊系統_檢查函作業(EXA04B)檢查函編號(共12碼數字)。");
				//
				try {
					Integer.parseInt(ep.examNo);
				}catch(Exception __ex) {
					throw new Exception("持函檢查請輸入檢查行政資訊系統_檢查函作業(EXA04B)檢查函編號(共12碼數字)。");
				}
			}
			ep.bankName = actionRequest.getParameter("bankName").trim();
			ep.jsonName = actionRequest.getParameter("jsonName").trim();
			if(ep.jsonName.equals("")) throw new Exception("請指定工作範本檔。");
			if(ep.bankName.equals("")) throw new Exception("請指定受檢機構全名。");
			ep.bankNameShort = actionRequest.getParameter("bankNameShort").trim();
			if(ep.bankNameShort.equals("")) throw new Exception("請指定受檢機構簡稱。");
			ep.leaderName = actionRequest.getParameter("leaderName").trim();
			if(ep.leaderName.equals("")) throw new Exception("請指定領隊名字。");
			ep.staffs = actionRequest.getParameter("staffs").trim();
			if(ep.staffs.equals("")) throw new Exception("請指定助檢成員(以分號區隔人員)。");
			//2018-07-06 修改
			if(actionRequest.getParameter("reportFree") != null) {
				ep.reportFree = actionRequest.getParameter("reportFree").trim();
			}
			if(actionRequest.getParameter("freeReason") != null) {
				ep.freeReason = actionRequest.getParameter("freeReason").trim();
			}
			if(actionRequest.getParameter("noMeeting") != null && actionRequest.getParameter("noMeeting").equalsIgnoreCase("true")) {
				ep.noMeeting = true;
			}
			//
			ep.dateStart = this.verifyDate(actionRequest.getParameter("dateStart").trim(), "開始檢查日");
			ep.dateEnd = this.verifyDate(actionRequest.getParameter("dateEnd").trim(), "完成檢查日");
			ep.deadlineStaff = this.verifyDate(actionRequest.getParameter("deadlineStaff").trim(), "助檢報告期限");
			
			if(!ep.noMeeting) {
				ep.dateMeeting = this.verifyDate(actionRequest.getParameter("dateMeeting").trim(), "溝通會議日期");
			}else {
				if(actionRequest.getParameter("dateMeeting") != null 
						&& !actionRequest.getParameter("dateMeeting").trim().isEmpty()) throw new Exception("無溝通會時請勿輸入溝通會日期。");
				//
				ep.dateMeeting = "";
			}
			ep.dateApproval = this.verifyDate(actionRequest.getParameter("dateApproval").trim(), "核派日期");
			try{
				ep.examDays = Float.parseFloat(actionRequest.getParameter("examDays").trim());
			}catch(Exception _e){
			}
			ep.defectWithBisTitle = Integer.parseInt(actionRequest.getParameter("defectWithBisTitle").trim());
			//
			if(ep.dateStart.compareTo(ep.dateEnd) > 0) throw new Exception("開始檢查日不可能晚於完成檢查日");
			if(!ep.noMeeting && ep.dateEnd.compareTo(ep.dateMeeting) > 0) throw new Exception("完成檢查日不可能晚於溝通會日期");
			//if(actionRequest.getParameter("evalAllocateTiming") != null){
			//	ep.evalAllocateTiming = Integer.parseInt(actionRequest.getParameter("evalAllocateTiming"));
			//}
			String userFullName = actionRequest.getPortletSession(true).getAttribute("userFullName").toString();
			//檢查是否為檢查人員
			if(ep.leaderName.contains(";")){
				String[] ss = ep.leaderName.split(";");
				for(String s: ss){
					if(!ErpUtil.isExamUser(s, themeDisplay)) throw new Exception("領隊：" + s + " 不存在使用者資料庫內。" );
				}
			}else{
				if(!ErpUtil.isExamUser(ep.leaderName, themeDisplay)) throw new Exception("領隊：" + ep.leaderName + " 不存在使用者資料庫內。" );
			}
			//
			ep.staffs = ep.staffs.replaceAll(",", ";").replaceAll("；",  ";").replaceAll("、", ";");
			String[] arrStaff = ep.staffs.split(";");
			for(String staffName: arrStaff){
				if(!ErpUtil.isExamUser(staffName, themeDisplay)) throw new Exception("助檢成員：" + staffName + " 不存在使用者資料庫內。" );
			}
			
			if(actionRequest.getParameter("examNature") == null || actionRequest.getParameter("examNature").equals("")) 
				throw new Exception("請指定本檢查證的性質(一般檢查或專案檢查)。");
			ep.examNature = actionRequest.getParameter("examNature");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			if(alfSessionAdmin == null) throw new Exception("無法取得 Alfresco 管理者連線！");
			//
			AlfrescoFolder fdParent = (AlfrescoFolder)alfSessionAdmin.getObject(r.folderId);
			if(fdParent == null) throw new Exception("編號 " + r.reportNo  + " 之報告目錄不存在！");
			//
			Map<String, String>  props = new HashMap<String,String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
			props.put(PropertyIds.NAME, ep.examNo);
			//是否為修改檢查證
			boolean isModifyingExam = false;
			//
			conn = ds.getConnection();
			if(ep.examId == 0){		//新增檢查案
				String sql = "SELECT 1 FROM exams WHERE exam_no=? and (dropped IS NULL OR dropped = 0)";	//未作廢者才入列
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, ep.examNo);
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					rs.close();
					throw new Exception("檢查證號 " + ep.examNo + " 已存在且尚未作廢，無法重新建立。");
				}
				rs.close();
				//
				boolean existed = false;
				Iterator<CmisObject> it = fdParent.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getName().equals(ep.examNo)){
						ep.folderId = co.getId();
						existed = true;
						break;
					}
				}
				if(!existed){
					ObjectId o = fdParent.createFolder(props);
					ep.folderId = o.getId();
				}
				//
				sql = "INSERT INTO exams (report_id, " +			//1
											" exam_no," +			//2
											" bank_name," +		//3
											" bank_name_short," +	//4
											" leader_name," +		//5
											" staffs," +			//6
											
											" report_free," +		//7
											" free_reason," +		//8
											
											" date_start," +		//9
											" date_end," +			//10
											" deadline_staff," +	//11
											" date_created," +		//12
											" created_by," +		//13
											" exam_days," +			//14
											" date_meeting," +		//15
											" date_approval," +		//16
											" defect_with_bis_title," +	//17
											" json_name," + 		//18
											" dropped," +			//19
											" exam_nature," +		//20
											" no_meeting," +		//21
											" folder_id) " +		//22
										" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, ep.reportId);
				ps.setString(2, ep.examNo);
				ps.setString(3, ep.bankName);
				ps.setString(4, ep.bankNameShort);
				ps.setString(5, ep.leaderName);
				ps.setString(6, ep.staffs);
				
				ps.setString(7, ep.reportFree);
				ps.setString(8, ep.freeReason);
				
				ps.setString(9, ep.dateStart);
				ps.setString(10, ep.dateEnd);
				ps.setString(11, ep.deadlineStaff);
				ps.setString(12, ep.dateCreated);
				ps.setString(13, userFullName);
				ps.setFloat(14, ep.examDays);
				ps.setString(15, ep.dateMeeting);
				ps.setString(16, ep.dateApproval);
				ps.setInt(17, ep.defectWithBisTitle);
				ps.setString(18, ep.jsonName);
				ps.setInt(19, 0);
				ps.setString(20, ep.examNature);
				if(ep.noMeeting) {
					ps.setInt(21, 1);
				}else {
					ps.setInt(21, 0);
				}
				ps.setString(22, ep.folderId);
				ps.execute();
				//回查 exam_id
				sql = "SELECT exam_id FROM exams WHERE exam_no=? " +				//1
													"AND bank_name=? " +		//2
													"AND folder_id=?" +			//3
													" AND dropped = 0";
				ps = conn.prepareStatement(sql);
				ps.setString(1, ep.examNo);
				ps.setString(2, ep.bankName);
				ps.setString(3, ep.folderId);
				rs = ps.executeQuery();
				if(rs.next()){
					ep.examId = rs.getInt(1);
				}
				rs.close();
				//新增異動紀錄
				this.addReportLog(r.reportId, "建立檢查證", userFullName, conn);
				//建立本檢查證各階段工作流程規則
				this.defineExamStageWorkflow(ep.examNo, ep.folderId, conn);
				//Log
				ErpUtil.logUserAction("建立檢查案", ep.examNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			}else{		//修改檢查證
				isModifyingExam = true;
				String examNoOrig = null;
				//
				String sql = "SELECT exam_no FROM exams WHERE exam_id=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, ep.examId);
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					if(rs.getString(1) != null 
							&& !rs.getString(1).trim().isEmpty()
							&& !rs.getString(1).trim().equalsIgnoreCase(ep.examNo)) examNoOrig = rs.getString(1).trim();
				}
				rs.close();
				ps.close();
				//
				if(examNoOrig != null) {	//檢查證號有修改
					sql = "SELECT 1 FROM exams WHERE exam_no=? and (dropped IS NULL OR dropped = 0) and exam_id <> ?";	//未作廢者才入列
					ps = conn.prepareStatement(sql);
					ps.setString(1, ep.examNo);
					ps.setInt(2, ep.examId);
					rs = ps.executeQuery();
					if(rs.next()) {
						rs.close();
						throw new Exception("檢查證號 " + ep.examNo + " 已存在且尚未作廢，修改無法存檔。");
					}
					rs.close();
					ps.close();
					//
					boolean existed = false;
					Iterator<CmisObject> it = fdParent.getChildren().iterator();
					while(it.hasNext()){
						CmisObject co = it.next();
						if(co.getName().equals(examNoOrig)){
							//修改檢查證檔案夾名稱
							Map<String, String> _props = new HashMap<String,String>();
							_props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
							_props.put(PropertyIds.NAME, ep.examNo);
							co.updateProperties(_props);
							//
							ep.folderId = co.getId();
							existed = true;
							//
							break;
						}
					}
					if(!existed){
						ObjectId o = fdParent.createFolder(props);
						ep.folderId = o.getId();
					}
				}else {
					//確認檢查證檔案夾是否存在
					boolean existed = false;
					Iterator<CmisObject> it = fdParent.getChildren().iterator();
					while(it.hasNext()){
						CmisObject co = it.next();
						if(co.getName().equals(ep.examNo)){
							ep.folderId = co.getId();
							existed = true;
							break;
						}
					}
					if(!existed){
						ObjectId o = fdParent.createFolder(props);
						ep.folderId = o.getId();
					}
				}
				//檢查是否變更工作範本
				ExamProfile origEP = null;
				for(ExamProfile _ep: r.vecExam) {
					if(_ep.examId == ep.examId) {
						origEP = _ep;
						break;
					}
				}
				if(origEP != null && !origEP.jsonName.equals(ep.jsonName)) {
					AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ep.folderId);
					for(CmisObject co: fdExam.getChildren()) {
						if(co instanceof org.apache.chemistry.opencmis.client.api.Document) continue;
						if(!co.getName().contains("行前")) continue;
						//
						for(CmisObject co2: ((AlfrescoFolder)co).getChildren()) {
							if(co2 instanceof org.apache.chemistry.opencmis.client.api.Folder) continue;
							if(!co2.getName().endsWith("json")) continue;
							//
							//this.addReportLog(r.reportId,
							//				"舊工作分配表" + co2.getName() + "更名為" + co2.getName() + ".bak",
							//				userFullName,
							//				conn);
							//
							Map<String, String> _props = new HashMap<String,String>();
							_props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
							_props.put(PropertyIds.NAME, co2.getName() + ".bak");
							co2.updateProperties(_props);
						}
					}
				}
				//
				sql = "UPDATE exams SET exam_no=?, " +			//1
										" bank_name=?," +				//2
										" bank_name_short=?," +			//3
										" leader_name=?," +				//4
										" staffs=?," +					//5
										
										" report_free=?," +				//6
										" free_reason=?," +				//7

										" date_start=?," +				//8
										" date_end=?, " +				//9
										" deadline_staff=?, " +			//10
										" exam_days=?," +				//11
										" date_meeting=?," +			//12
										" date_approval=?," +			//13
										" defect_with_bis_title=?," +	//14
										" json_name=?," +				//15
										" exam_nature=?," +				//16
										" no_meeting=?," +				//17
										" folder_id=? " +				//18
								" WHERE exam_id = ?  ";					//19
				ps = conn.prepareStatement(sql);
				ps.setString(1, ep.examNo);
				ps.setString(2, ep.bankName);
				ps.setString(3, ep.bankNameShort);
				ps.setString(4, ep.leaderName);
				ps.setString(5, ep.staffs);
				
				ps.setString(6, ep.reportFree);
				ps.setString(7, ep.freeReason);
				
				ps.setString(8, ep.dateStart);
				ps.setString(9, ep.dateEnd);
				ps.setString(10, ep.deadlineStaff);
				ps.setFloat(11, ep.examDays);
				ps.setString(12, ep.dateMeeting);
				ps.setString(13, ep.dateApproval);
				ps.setInt(14, ep.defectWithBisTitle);
				ps.setString(15, ep.jsonName);
				ps.setString(16, ep.examNature);
				if(ep.noMeeting) {
					ps.setInt(17, 1);
				}else {
					ps.setInt(17, 0);
				}
				ps.setString(18, ep.folderId);
				ps.setInt(19, ep.examId);
				ps.execute();
				//加入異動紀錄
				String msg = "更新檢查證資料";
				if(examNoOrig != null) msg = "檢查證由" + examNoOrig + "改為" + ep.examNo;
				this.addReportLog(r.reportId, msg, userFullName, conn);
				//是否更新 免繳報告人員
				if(origEP != null && !origEP.reportFree.equalsIgnoreCase(ep.reportFree)) {
					SimpleDateFormat sdfLog = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					sdfLog.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
					User user = themeDisplay.getUser();
					//
					sql = "INSERT INTO report_free_log (time_stamp,"			//1
														+ "exam_id,"			//2
														+ "modify_by,"			//3
														+ "report_free_1,"		//4
														+ "free_reason_1,"		//5
														+ "report_free_2,"		//6
														+ "free_reason_2) "		//7
													+ " VALUES (?,?,?,?,?,?,?)";
					ps = conn.prepareStatement(sql);
					ps.setString(1, sdfLog.format(new java.util.Date()));
					ps.setInt(2, ep.examId);
					ps.setString(3, user.getLastName() + user.getFirstName());
					ps.setString(4, origEP.reportFree);
					ps.setString(5, origEP.freeReason);
					ps.setString(6, ep.reportFree);
					ps.setString(7, ep.freeReason);
					ps.execute();
				}
				//Log
				ErpUtil.logUserAction("更新檢查案", ep.examNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			}
			
			//-----------------檢查作業階段----------------------------------
			//取得工作範本檔
			AlfrescoDocument alfJson = this.getJobJson(r.jobSecIndus, ep.jsonName, alfSessionAdmin);
			if(alfJson == null) throw new Exception("無法取得工作範本JSON檔案！");
			Alloc alloc = this.getAllocFromFile(alfJson);
			//加入助檢
			String[] ss = ep.staffs.split(";");
			for(String s: ss){
				alloc.vecStaff.add(s);
			}
			//if(alloc.vecStaff.contains(ep.leaderName)){		//將領隊加入助檢行列
			//	alloc.vecStaff.add(ep.leaderName);
			//}
			//alloc.toJson();
			//切換到檢查性質目錄(一般或專案)
			Folder fdSrc = this.getDocTempRootFolder(themeDisplay);
			String fullPath = alloc.AllocParams_getTemplateRelativePathFolder();
			ss = fullPath.replaceAll("\\\\", "/").split("/");
			for(String s: ss){
				fdSrc = DLAppLocalServiceUtil.getFolder(fdSrc.getRepositoryId(), fdSrc.getFolderId(), s);
			}
			//記住檢查案的屬性
			String sql = "UPDATE exams SET exam_comment_produce_type = ?, " +		//1
								" is_computer_audit = ?, " +					//2
								" is_essen_need = ?, " +						//3
								" is_rating_assess = ?, " +						//4
								" is_special_exam = ?, " +						//5
								" doc_temp_path=? " +						//6
							" WHERE exam_id = ?";							//7
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, alloc.AllocParams_getExamCommentProduceType().ordinal());
			ps.setBoolean(2, alloc.AllocParams_isComputerAudit());
			ps.setBoolean(3, alloc.AllocParams_isEssentialCheckItemShouldBeCared());
			ps.setBoolean(4, alloc.AllocParams_isRatingAssess());
			ps.setBoolean(5, alloc.AllocParams_isSpecialExam());
			ps.setString(6, alloc.AllocParams_getTemplateRelativePathFolder());
			ps.setInt(7, ep.examId);
			ps.execute();
			//
			//本檢查案所在目錄
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(ep.folderId);
			//
			String alfAllocId = "";
			//Folder lfdAllocate = null;		//工作分配表所在目錄
			boolean folderStageNameFound = false;
			List<Folder> listFolder = DLAppLocalServiceUtil.getFolders(fdSrc.getRepositoryId(), fdSrc.getFolderId());
			for(Folder fd: listFolder){
				if(fd.getName().contains("階段")){
					List<Folder> listFolder2 = DLAppLocalServiceUtil.getFolders(fdSrc.getRepositoryId(), fd.getFolderId());
					for(Folder fd2: listFolder2){
						AlfrescoFolder fdStage = null;
						//
						Iterator<CmisObject> it = fdExam.getChildren().iterator();
						while(it.hasNext()){
							CmisObject co = it.next();
							if(co.getName().equals(fd2.getName())){
								fdStage = (AlfrescoFolder)co;
								break;
							}
						}
						if(fdStage == null){
							props = new HashMap<String,String>();
							props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
							props.put(PropertyIds.NAME, fd2.getName());
							//
							fdStage = (AlfrescoFolder)fdExam.createFolder(props);
						}
						if(fdStage != null){
							//看看裡面是否預存檔案
							List<FileEntry> listFile = DLAppLocalServiceUtil.getFileEntries(fdSrc.getRepositoryId(), fd2.getFolderId());
							for(FileEntry f: listFile){
								AlfrescoDocument alfDoc = null;
								//
								it = fdStage.getChildren().iterator();
								while(it.hasNext()){
									CmisObject co = it.next();
									if(co.getName().equals(f.getTitle())){
										alfDoc = (AlfrescoDocument)co;
										break;
									}
								}
								//
								if(alfDoc == null){
									ContentStream cs = new ContentStreamImpl(f.getTitle(), null, f.getMimeType(), f.getContentStream());
									//
									props = new HashMap<String,String>();
									props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
									props.put(PropertyIds.NAME, f.getTitle());
									//
									fdStage.createDocument(props, cs, VersioningState.MAJOR);
								}
							}
							//
							if(fdStage.getName().contains("檢查行前")){
								AlfrescoDocument alfAllocate = null;
								//
								it = fdStage.getChildren().iterator();
								while(it.hasNext()){
									CmisObject co = it.next();
									if(co.getName().equalsIgnoreCase(alfJson.getName())){	//工作範本同名者
										alfAllocate = (AlfrescoDocument)co;
										break;
									}
								}
								//
								props = new HashMap<String,String>();
								if(alfAllocate == null){	//不存在時則建立
									ContentStream cs = new ContentStreamImpl(alfJson.getName(), null, alfJson.getContentStreamMimeType(), new ByteArrayInputStream(alloc.toJson().getBytes()));
									//
									props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
									props.put(PropertyIds.NAME, alfJson.getName());
									//
									alfAllocId = fdStage.createDocument(props, cs, VersioningState.MAJOR).getId();
								}else{	//已存在時確認助檢是否變更
									alfAllocId = alfAllocate.getId();
									//
									alloc = this.getAllocFromFile(alfAllocate);
									ss = ep.staffs.split(";");
									//檢查助檢是否異動
									boolean staffUpdated = false;
									if(alloc.vecStaff.size() != ss.length){
										staffUpdated = true;
									}
									if(!staffUpdated){
										for(String s: ss){
											if(!alloc.vecStaff.contains(s)){
												staffUpdated = true;
												break;
											}
										}
									}
									if(staffUpdated){		//最終判斷有更新者
										alloc.vecStaff.removeAllElements();
										for(String s: ss){
											alloc.vecStaff.add(s);
										}
										//
										props.put("it:updateMemo", "更新助檢");
										props.put("it:modifier", userFullName);
										//
								        if(!alfAllocate.isVersionSeriesCheckedOut()){		//內容更新
								        	ContentStream cs = new ContentStreamImpl(alfAllocate.getName(), null, alfAllocate.getContentStreamMimeType(), new ByteArrayInputStream(alloc.toJson().getBytes()));
								        	//
								        	ObjectId idOfCheckedOutDocument = alfAllocate.checkOut();
								        	Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
								        	pwc.checkIn(false, props, cs, "更新助檢");
								        }
									}
								}
							}
						}
					}
					folderStageNameFound = true;
				//}else if(fd.getName().contains("工作分配")){
				//	lfdAllocate = fd;
				}
			}
			if(!folderStageNameFound) throw new Exception("文件樣板區路徑 " + fullPath + " 找不到「階段」檔案夾。");
			//
			if(!alfAllocId.isEmpty()) {
				this.checkSyncCols(conn);		//檢查與行政系統同步之欄位
				//
				String screenNameLeader = ErpUtil.getUserScreenName(themeDisplay, ep.leaderName);
				if(screenNameLeader == null || screenNameLeader.isEmpty()) throw new Exception("領隊「" + ep.leaderName + "」之登入帳號找不到。");
				//永遠往前新增
				sql = "INSERT INTO alloc_sync (sync_ok,"			//1
												+ "report_no,"			//2
												+ "exam_no,"			//3
												+ "bank_name_short,"	//4	
												+ "screen_name_leader,"	//5
												+ "node_id, "			//6
												+ "exam_update_time "	//7
											+ ") VALUES (?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, 0);
				ps.setString(2, r.reportNo);
				ps.setString(3, ep.examNo);
				ps.setString(4, ep.bankNameShort);
				ps.setString(5, screenNameLeader);
				ps.setString(6, alfAllocId);
				ps.setString(7, ErpUtil.getCurrDateTime());
				ps.execute();
				//
				ps.close();
			}
			//更新領隊與助檢成員
			String _leaders = "", _staffs = ""; 
			sql = "SELECT leader_name, staffs, verifiers FROM exams WHERE report_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, r.reportId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				if(rs.getString("leader_name") != null){
					String _s = rs.getString("leader_name").trim();
					String[] _ss = _s.split(";");
					for(String s: _ss){
						if(!_leaders.contains(s)){
							if(!_leaders.equals("")) _leaders += ";";
							_leaders += rs.getString("leader_name");
						}
					}
				}
				if(rs.getString("staffs") != null && !rs.getString("staffs").equals("")){
					String _s = rs.getString("staffs").trim();
					String[] _ss = _s.split(";");
					for(String s: _ss){
						if(!_staffs.contains(s)){
							if(!_staffs.equals("")) _staffs += ";";
							_staffs += s;
						}
					}
				}
				//審核人員併入助檢成員
				if(rs.getString("verifiers") != null && !rs.getString("verifiers").equals("")){
					if(!_staffs.equals("")) _staffs += ";";
					_staffs += rs.getString("verifiers");
				}
			}
			sql = "UPDATE reports SET leader_name=?, staffs=? WHERE report_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, _leaders);
			ps.setString(2, _staffs);
			ps.setInt(3, r.reportId);
			ps.execute();
			//
			//回存 Session
			r.vecExam = this.getExams(reportId, conn, alfSessionAdmin);
			for(ReportProfile _rp: collec.vecReport){
				if(_rp.reportId == reportId){
					int idx = collec.vecReport.indexOf(_rp);
					collec.vecReport.remove(idx);
					collec.vecReport.add(idx, r);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("collec", collec);
			//
			actionResponse.setRenderParameter("reportId", String.valueOf(reportId));
			actionResponse.setRenderParameter("jspPage", "/jsp/myreports/viewExam.jsp");
			//當重存檢查證時
			if(isModifyingExam){
				actionRequest.setAttribute("successMsg", "檢查案存檔完成，如果您要重新產出「工作分配表」或各工作階段的「樣板文件」，請先刪除已產出的檔案後再重新存檔。");
				SessionMessages.add(actionRequest, "success");
			}
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/jsp/myreports/editExam.jsp");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private void checkSyncCols(Connection conn) throws Exception{
		boolean existReportNo = false;			//report_no
		boolean existExamNo = false;			//exam_no
		boolean existBankShort = false;			//bank_name_short
		boolean existScreenNameLeader = false;	//screen_name_leader
		boolean existSyncTime = false;
		boolean existExamUpdateTime = false;
		//
		String sql = "SELECT * FROM alloc_sync";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			ResultSetMetaData rsMeta = rs.getMetaData();
			int cc = rsMeta.getColumnCount();
			for (int i = 1; i <= cc; i++) {
				String fldName = rsMeta.getColumnName(i);
				if(fldName.equalsIgnoreCase("report_no")) {
					existReportNo = true;
				}else if(fldName.equalsIgnoreCase("exam_no")) {
					existExamNo = true;
				}else if(fldName.equalsIgnoreCase("bank_name_short")) {
					existBankShort = true;
				}else if(fldName.equalsIgnoreCase("screen_name_leader")) {
					existScreenNameLeader = true;
				}else if(fldName.equalsIgnoreCase("sync_time")) {
					existSyncTime = true;					
				}else if(fldName.equalsIgnoreCase("exam_update_time")) {
					existExamUpdateTime = true;
				}
			}
		}
		rs.close();
		ps.close();
		//
		if(!existReportNo) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN report_no VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existExamNo) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN exam_no VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existBankShort) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN bank_name_short VARCHAR(100)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existScreenNameLeader) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN screen_name_leader VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existSyncTime) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN sync_time VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
		if(!existExamUpdateTime) {
			sql = "ALTER TABLE alloc_sync ADD COLUMN exam_update_time VARCHAR(50)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}	
		ps.close();		
	}
	
	public void listMySecReportNo(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//String secBelongTo = actionRequest.getParameter("secBelongTo").toString();
			//if(secBelongTo.equals("")) return;
			//
			String secBelongTo = "";
		    if(actionRequest.getParameter("secBelongTo") != null){
		    	secBelongTo = actionRequest.getParameter("secBelongTo").toString();
		    }
		    //if(!secBelongTo.equals("")) {
			    String dateFrom = "";
			    if(actionRequest.getParameter("dateStart") != null) {
			    	dateFrom = actionRequest.getParameter("dateStart").toString();
			    }
			    if(!dateFrom.equals("")) dateFrom += " 00:00:00";
			    
			    String dateEnd = "";
			    if(actionRequest.getParameter("dateEnd") != null) {
			    	dateEnd = actionRequest.getParameter("dateEnd").toString();
			    }
			    if(!dateEnd.equals("")) dateEnd += " 23:59:59";
			    
				Vector<ReportObj> vecRO = new Vector<ReportObj>();
				
				conn = ds.getConnection();
				String sql = "SELECT report_no, sec_name, date_created, created_by, dropped FROM reports WHERE 1=1 ";
				if(!dateFrom.equals("")) {
					sql += " AND date_created >= '" + dateFrom + "' ";
				}
				if(!dateEnd.equals("")) {
					sql += " AND date_created <= '" + dateEnd + "' ";
				}
				if(secBelongTo != null && !secBelongTo.equals("*") && !secBelongTo.equals("")) {
					String secNo = "";
					try {
						secNo = String.valueOf(secBelongTo.charAt(0));
					}catch(Exception _ex) {
					}
					sql += " AND report_no LIKE '%" + secNo + "%' ";
				}
				sql += " ORDER BY report_no DESC";
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					ReportObj ro = new ReportObj();
					ro.reportNo = rs.getString("report_no");
					ro.secName = rs.getString("sec_name");
					ro.createdDate = rs.getString("date_created");
					ro.createdBy = rs.getString("created_by");
					if(rs.getInt("dropped") == 1) ro.dropped = "是";
					
					vecRO.add(ro);
				}
				rs.close();
				actionRequest.setAttribute("vecRO", vecRO);
		    //}
			actionResponse.setRenderParameter("jspPage", "/jsp/myreports/listReportNo.jsp");
			if(vecRO.size() == 0) return;
			//產出 Excel
			java.io.InputStream is = actionRequest.getPortletSession().getPortletContext().getResourceAsStream("報告編號列表.xls");
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0);
			//查詢區間
			String strDate = "";
			if(!dateFrom.equals("")) strDate += dateFrom.split(" ")[0];
			if(!dateEnd.equals("")) {
				strDate += " ~ " + dateEnd.split(" ")[0];
			}else {
				strDate += " ~ Now";
			}
			
			HSSFRow row = sheet.getRow(1);
			row.getCell(2).setCellValue(strDate);
			//
			int rowInsertAt = 3;
			for(ReportObj ro: vecRO) {
				sheet.shiftRows(rowInsertAt, rowInsertAt, 1, true, false);
				sheet.createRow(rowInsertAt);
				row = sheet.getRow(rowInsertAt);
				row.setHeight((short)-1);			//自動放高
				//
				HSSFRow origRow = sheet.getRow(rowInsertAt+1);
				for(int i=0; i < origRow.getLastCellNum(); i++){
					row.createCell(i);
					row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
				}
				//
				row.getCell(0).setCellValue(vecRO.indexOf(ro) + 1);
				row.getCell(1).setCellValue(ro.reportNo);
				row.getCell(2).setCellValue(ro.secName);
				row.getCell(3).setCellValue(ro.createdDate);
				row.getCell(4).setCellValue(ro.createdBy);
				row.getCell(5).setCellValue(ro.dropped);
				//
				rowInsertAt++;
			}
			sheet.removeRow(sheet.getRow(rowInsertAt));		//刪樣板列
			//
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			wb.write(baos);
			//
			actionRequest.getPortletSession(true).setAttribute("baosMySecReportNo", baos);
			actionRequest.setAttribute("mySecReportNoGened", "true");
		}catch(Exception ex) {
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	//讀取工作分配表並轉成 Alloc 物件
	private Alloc getAllocFromFile(AlfrescoDocument alfDoc) throws Exception{
		InputStreamReader isr = new InputStreamReader(alfDoc.getContentStream().getStream(), "UTF-8");
		BufferedReader br = new BufferedReader(isr);
	        StringBuilder sb = new StringBuilder();
		String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        String json = sb.toString();
	        br.close();
		//
		return Alloc.fromJson(json);		
	}
	
	
	//檢查日期格式
	private String verifyDate(String date, String colName) throws Exception{
		if(date.trim().equals("") || date.split("-").length != 3) throw new Exception(colName + " 日期請以西元年-月-日格式輸入。");
		String ret = "";
		//
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient(false);
 		try {
 			if(date.split("-")[0].trim().length() < 4) throw new Exception("請輸入西元年");
 			//
 			sdf.parse(date);
 			ret = date;
		} catch (Exception ex) {
 		}
 		if(ret.equals("")) throw new Exception(colName + " 日期輸入錯誤。");
 		//
 		return ret;
	}
	
	//取得工作範本檔案
	private AlfrescoDocument getJobJson(String jobSecIndus, String jsonName, Session alfSession) throws Exception{
		AlfrescoDocument alfJson = null;
		//
		String secName = jobSecIndus.split("/")[0];
		String indusName = jobSecIndus.split("/")[1];
		//
		Iterator<CmisObject>  it = alfSession.getRootFolder().getChildren().iterator();
		 while(it.hasNext()){
			 CmisObject co = it.next();
			 if(co.getName().equals(rootJobTemp)){
				 it = ((AlfrescoFolder)co).getChildren().iterator();
				 while(it.hasNext()){
					 co = it.next();			//組別
					 if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().equals(secName)){
						 it = ((AlfrescoFolder)co).getChildren().iterator();
						 while(it.hasNext()){
							 co = it.next();		//業別
							 if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().equals(indusName)){
								 it = ((AlfrescoFolder)co).getChildren().iterator();
								 while(it.hasNext()){
									 co = it.next();		//Json
									 if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && co.getName().equals(jsonName)){
										 alfJson = (AlfrescoDocument)co;
										 //
										 break;
									 }
								 }
								 break;
							 }
						 }
						 break;
					 }
				 }
				 break;
			 }
		 }
		return alfJson;
	}
	
	//進入本檢查案
	public void enterExam(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			int examId = Integer.parseInt(actionRequest.getParameter("examId"));
			//
			//設定 Global Share Session
			LiferaySessionUtil.setGlobalSessionAttribute("currExamId", examId, actionRequest);
			//
			//System.out.println("Project Id: " + projectId);
			//String urlPath = "/stage?examId=" + examId;
			String urlPath = "/stage";
			actionResponse.sendRedirect(urlPath);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	//新增檢查證各階段作業流程規則
	@SuppressWarnings("resource")
	private void defineExamStageWorkflow(String examNo, String folderId, Connection conn) throws Exception{
		String sql = "SELECT * FROM stage_rules WHERE exam_no = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, examNo);
		ResultSet rs = ps.executeQuery();
		if(!rs.next()){
			sql = "INSERT INTO stage_rules (exam_no," +					//1
									 " folder_id," + 					//2
									 " form_gened_mail2_assistant," +	//3
									 " in_exam_mail2_leader," +		//4
									 " exam_end_mail2_leader," +		//5
									 " exam_end_enable_workflow," +	//6
									 " exam_end_workflow_type," +	//7
									 " pre_meet_enable_workflow," +	//8
									 " end_meet_enable_workflow," +	//9
									 " eval_mail2_leader," +			//10
									 " eval_keyuser_mail2_leader," +		//11
									 " eval_enable_workflow," +		//12
									 " eval_workflow_type," +			//13
									 " post_enable_workflow) " +		//14
						 " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, examNo);
			ps.setString(2, folderId);
			ps.setInt(3, 1);
			ps.setInt(4, 1);
			ps.setInt(5, 1);
			ps.setInt(6, 1);
			ps.setInt(7, 1);
			ps.setInt(8, 0);
			ps.setInt(9, 0);
			ps.setInt(10, 0);
			ps.setInt(11, 0);
			ps.setInt(12, 1);
			ps.setInt(13, 1);
			ps.setInt(14, 0);
			ps.execute();
		}else{		//更新 Exam Folder id 
			sql = "UPDATE stage_rules SET folder_id = ? WHERE exam_no = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, folderId);
			ps.setString(2, examNo);
			ps.execute();
		}
		ps.close();
	}
	
	//產出檔案上傳之excel報告
	public void genUploadStatisReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			boolean isAdminRequest = false;
			//
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PermissionChecker pc =  PermissionCheckerFactoryUtil.create(themeDisplay.getUser());
			if(pc.isOmniadmin()) isAdminRequest = true;
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			String paDateStart = "";
			if(actionRequest.getParameter("dateStart") != null) paDateStart = actionRequest.getParameter("dateStart").trim();
			if(paDateStart.equals("")) throw new Exception("報告建立啟始日期未輸入。");
			if(!this.isValidDate(paDateStart)) throw new Exception("啟始日期格式錯誤或無效的日期。");
			
			String paDateEnd = "";
			if(actionRequest.getParameter("dateEnd") != null) paDateEnd = actionRequest.getParameter("dateEnd").trim();
			if(paDateEnd.equals("")) throw new Exception("報告建立截止日期未輸入。");
			if(!this.isValidDate(paDateEnd)) throw new Exception("截止日期格式錯誤或無效的日期。");
			
			String paSecNo = "";
			if(actionRequest.getParameter("secNo") != null) paSecNo = actionRequest.getParameter("secNo").trim();
			if(paSecNo.equals("")) throw new Exception("查詢組別未指定。");
			//報表分類
			String excelType = "";		//一般 或 評述 檔案
			if(actionRequest.getParameter("excelType") != null) excelType = actionRequest.getParameter("excelType").trim();
			if(excelType.equals("")) throw new Exception("報表分類未指定。");
			//
			Vector<String> vecStage = new Vector<String>();
            //vecStage.add("行前");
            //vecStage.add("期間");
            vecStage.add("結束");
            //vecStage.add("會前");
            //vecStage.add("會後");
            //vecStage.add("評等");
            //vecStage.add("陳核");
            //vecStage.add("歸檔");
            //組別統計
            HashMap<String, StageCount> mapSecCount = new HashMap<String, StageCount>();
            //
			java.io.InputStream is = null;
			if(excelType.equalsIgnoreCase("A")) {
				is = actionRequest.getPortletSession().getPortletContext().getResourceAsStream("檔案上傳數統計.xls");
			}else {
				//if(isAdminRequest) {
					is = actionRequest.getPortletSession().getPortletContext().getResourceAsStream("評述上傳管制表(Admin).xls");
				//}else {
				//	is = actionRequest.getPortletSession().getPortletContext().getResourceAsStream("評述上傳管制表.xls");
				//}
			}
			HSSFWorkbook wb = new HSSFWorkbook(is);
			HSSFSheet sheet = wb.getSheetAt(0);
			//查詢區間
			HSSFRow row = sheet.getRow(1);
			row.getCell(1).setCellValue(paDateStart + " ~ " + paDateEnd);
			//
			int rowInsertAt = 3;
			//
			Vector<ReportObj> vecRO = new Vector<ReportObj>();
			
			conn = ds.getConnection();
			String sql = "SELECT * FROM reports WHERE (dropped IS NULL OR dropped = 0) "
										+ " AND date_created >= ? "
										+ " AND date_created <= ? "
										+ " AND report_no IS NOT NULL AND report_no <> '' ";
			if(!paSecNo.equals("*")) {
				sql += " AND (sec_belong_to LIKE '%" + paSecNo + "%' "
							+ " OR ((sec_belong_to IS NULL OR sec_belong_to ='') AND sec_name LIKE '%" + paSecNo + "%'" + "))";
			}
			sql += " ORDER BY date_created";
			PreparedStatement ps = conn.prepareCall(sql);
			ps.setString(1, paDateStart + " " + "00:00:00");
			ps.setString(2, paDateEnd + " " + "23:59:59");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				ReportObj ro = new ReportObj();
				ro.reportId = rs.getInt("report_id");
				if(rs.getString("sec_belong_to") != null && !rs.getString("sec_belong_to").equals("")) {
					ro.secName = rs.getString("sec_belong_to");
				}else {
					ro.secName = rs.getString("sec_name");
				}
				ro.reportNo = rs.getString("report_no");
				ro.createdDate = rs.getString("date_created").split(" ")[0];
				ro.createdBy = rs.getString("created_by");
				//
				Vector<ExamObj2> vecEO = new Vector<ExamObj2>();
				sql = "SELECT * FROM exams WHERE (dropped IS NULL OR dropped = 0) "
								//+ "	AND exam_no IS NOT NULL AND exam_no <> '' "
								+ " AND report_id = ?";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, ro.reportId);
				ResultSet __rs = ps.executeQuery();
				while(__rs.next()){
					ExamObj2 eo2 = new ExamObj2();
					if(__rs.getString("exam_no") != null) eo2.examNo = __rs.getString("exam_no");
					eo2.leaderName = __rs.getString("leader_name");
					eo2.staffs = __rs.getString("staffs");
					
					eo2.dateStart = __rs.getString("date_start");
					eo2.dateEnd = __rs.getString("date_end");
					
					if(__rs.getString("report_free") != null) eo2.reportFree = __rs.getString("report_free");
					if(__rs.getString("free_reason") != null) eo2.freeReason = __rs.getString("free_reason");
					eo2.folderId = __rs.getString("folder_id");
					//
					vecEO.add(eo2);
				}
				__rs.close();
				//
				ro.vecExam = vecEO;
				vecRO.add(ro);
			}
			rs.close();
			//
			for(ReportObj ro: vecRO) {
				StageCount sc = new StageCount();
				if(mapSecCount.containsKey(ro.secName)) sc = mapSecCount.get(ro.secName);
				if(!sc.reportNos.equals("")) sc.reportNos += "、";
				sc.reportNos += ro.reportNo;
				sc.ccReport += 1;
				//未建立檢查證者
				if(ro.vecExam.size() == 0) {
					sheet.shiftRows(rowInsertAt, rowInsertAt, 1, true, false);
					sheet.createRow(rowInsertAt);
					row = sheet.getRow(rowInsertAt);
					row.setHeight((short)-1);			//自動放高
					//
					HSSFRow origRow = sheet.getRow(rowInsertAt+1);
					for(int i=0; i < origRow.getLastCellNum(); i++){
						row.createCell(i);
						row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
					}
					//
					row.getCell(0).setCellValue(ro.reportNo);
					row.getCell(1).setCellValue(ro.createdDate);
					row.getCell(2).setCellValue(ro.createdBy);
					row.getCell(5).setCellValue(ro.secName);
					//
					if(excelType.equalsIgnoreCase("A")) {
						row.getCell(11).setCellValue("領隊尚未維護檢查證(函)資料，請領隊再確認");
					}else {
						row.getCell(20).setCellValue("領隊尚未維護檢查證(函)資料，請領隊再確認");
					}
					//
					rowInsertAt++;
					//
					continue;
				}
				//
				for(ExamObj2 eo2: ro.vecExam){
					String[] arrStaff = eo2.staffs.split(";");
					if(eo2.staffs.contains(",")) arrStaff = eo2.staffs.split(",");
					//扣掉無須上傳報告者
					if(!eo2.reportFree.equals("")) {
						Vector<String> vecRealStaff = new Vector<String>();
						for(String s: arrStaff) {
							if(!eo2.reportFree.contains(s)) {
								vecRealStaff.add(s);
							}
						}
						arrStaff = vecRealStaff.toArray(new String[vecRealStaff.size()]);
					}
					//
					//收集已上傳檔案 CO
					String strLacker = "";	//未上傳之助檢
					String fileNames = "";	//評述檔案夾內已上傳之檔名
					Vector<CmisObject> vecCo = new Vector<CmisObject>();
					//
					org.apache.chemistry.opencmis.client.api.Folder fdExam = getExamFolder(alfSessionAdmin, eo2.folderId, eo2.examNo);
					if(fdExam != null) {
						for(CmisObject co: fdExam.getChildren()) {
							if(!(co instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
							//只看檢查結束階段
							if(!co.getName().contains("結束")) continue;
							//階段內檔案
							for(CmisObject co2: ((org.apache.chemistry.opencmis.client.api.Folder)co).getChildren()) {
								if(excelType.equalsIgnoreCase("A")) {
									if(!(co2 instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
									vecCo.add(co2);
								}else{
									if(!(co2 instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
									
									if(!co2.getName().equals("檢查評述")) continue;
									//
									for(CmisObject co3: ((org.apache.chemistry.opencmis.client.api.Folder)co2).getChildren()) {
										if(!(co3 instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
										//
										vecCo.add(co3);
									}
								}
							}
						}
						//
						if(excelType.equalsIgnoreCase("A")) {
							for(String staffName: arrStaff) {
								boolean found = false;
								for(CmisObject co: vecCo) {
									Document doc = (Document)co;
									String nodeDesc = "";
									try {
										if(doc.getPropertyValue("cm:description") != null) {
											nodeDesc = doc.getPropertyValue("cm:description").toString();
										}
										if(nodeDesc.isEmpty()) {
											AlfrescoDocument alfDoc = (AlfrescoDocument)doc; 
											nodeDesc = alfDoc.getLastModifiedBy();
											//
											if(nodeDesc.isEmpty()) {
												nodeDesc = alfDoc.getCreatedBy();
											}
										}
									}catch(Exception _ex) {
									}
									if(nodeDesc.contains(staffName)) {
										found = true;
										break;
									}else {		//檢查所有歷史版本
										for(Document _doc: doc.getAllVersions()) {
											nodeDesc = "";
											if(_doc.getPropertyValue("cm:description") != null) {
												nodeDesc = _doc.getPropertyValue("cm:description").toString();
											}
											if(nodeDesc.isEmpty()) {
												AlfrescoDocument alfDoc = (AlfrescoDocument)_doc; 
												nodeDesc = alfDoc.getLastModifiedBy();
												//
												if(nodeDesc.isEmpty()) {
													nodeDesc = alfDoc.getCreatedBy();
												}
											}
											if(nodeDesc.contains(staffName)) {
												found = true;
												break;
											}
										}
										if(found) break;
									}
								}
								if(!found) {
									if(!strLacker.equals("")) strLacker += "、";
									strLacker += staffName;
								}
							}
							//if(strLacker.trim().equals("")) continue;
						}else{
							for(CmisObject co: vecCo) {
								if(!fileNames.isEmpty()) fileNames += "、";
								fileNames += co.getName();
							}
						}
					}
					//
					sheet.shiftRows(rowInsertAt, rowInsertAt, 1, true, false);
					sheet.createRow(rowInsertAt);
					row = sheet.getRow(rowInsertAt);
					row.setHeight((short)-1);			//自動放高
					//
					HSSFRow origRow = sheet.getRow(rowInsertAt+1);
					for(int i=0; i < origRow.getLastCellNum(); i++){
						row.createCell(i);
						row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
					}
					//
					row.getCell(0).setCellValue(ro.reportNo);
					row.getCell(1).setCellValue(ro.createdDate);
					row.getCell(2).setCellValue(ro.createdBy);
					row.getCell(3).setCellValue(eo2.dateStart);
					row.getCell(4).setCellValue(eo2.dateEnd);
					row.getCell(5).setCellValue(ro.secName);
					row.getCell(6).setCellValue(eo2.examNo);
					row.getCell(7).setCellValue(eo2.leaderName);
					row.getCell(8).setCellValue(eo2.staffs);
					row.getCell(9).setCellValue(eo2.reportFree);
					row.getCell(10).setCellValue(eo2.freeReason);
					if(excelType.equalsIgnoreCase("A")) {
						row.getCell(11).setCellValue(strLacker);
						if(strLacker.trim().isEmpty()) sc.ccWithFile += 1;
					}else{
						row.getCell(11).setCellValue(fileNames);
						//
						//if(isAdminRequest) {
							strLacker = "";
							for(String staffName: arrStaff) {
								if(!fileNames.contains(staffName)) {
									if(!strLacker.isEmpty()) strLacker += "、";
									strLacker += staffName;
								}
							}
							if(strLacker.isEmpty()) {
								sc.ccWithFile += 1;
							}else {
								row.getCell(20).setCellValue(strLacker);
							}
						//}
					}
					//
					mapSecCount.put(ro.secName, sc);
					//
					rowInsertAt++;
				}
			}
			row = sheet.getRow(rowInsertAt);
			if(row != null) {
				if(row.getCell(0) != null && row.getCell(0).getStringCellValue().equals(".")) {
					sheet.removeRow(row);
				}
			}
			//組別統計
			int sumReport = 0;
			int sumWithFile = 0;
			//
			sheet = wb.getSheetAt(1);
			row = sheet.getRow(1);
			row.getCell(1).setCellValue(paDateStart + "~" + paDateEnd + " 立案之報告" );
			rowInsertAt = 3;
			for(String secName: mapSecCount.keySet()) {
				StageCount sc = mapSecCount.get(secName);
				//
				sheet.shiftRows(rowInsertAt, sheet.getLastRowNum(), 1, true, false);
				sheet.createRow(rowInsertAt);
				row = sheet.getRow(rowInsertAt);
				row.setHeight((short)-1);			//自動放高
				//
				HSSFRow origRow = sheet.getRow(rowInsertAt+1);
				for(int i=0; i < origRow.getLastCellNum(); i++){
					row.createCell(i);
					row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
				}
				//
				row.getCell(0).setCellValue(secName);
				row.getCell(1).setCellValue(sc.ccReport);
				row.getCell(2).setCellValue(sc.ccWithFile);
				row.getCell(3).setCellValue(sc.ccReport - sc.ccWithFile);
				if(sc.ccReport == 0) {
					row.getCell(4).setCellValue("0%");
				}else if(sc.ccReport == sc.ccWithFile) {
					row.getCell(4).setCellValue("100%");
				}else{
					row.getCell(4).setCellValue( (Math.round(((double)sc.ccWithFile / (double)sc.ccReport) * 100d) / 100d) + "%");
				}
				row.getCell(5).setCellValue("");
				//
				rowInsertAt += 1;
				//
				sumReport += sc.ccReport;
				sumWithFile += sc.ccWithFile;
			}
			//
			row = sheet.getRow(rowInsertAt);
			row.getCell(0).setCellValue("合計");
			row.getCell(1).setCellValue(sumReport);
			row.getCell(2).setCellValue(sumWithFile);
			row.getCell(3).setCellValue(sumReport - sumWithFile);
			if(sumReport == 0) {
				row.getCell(4).setCellValue("0%");
			}else if(sumReport == sumWithFile) {
				row.getCell(4).setCellValue("100%");
			}else{
				row.getCell(4).setCellValue( (Math.round(((double)sumWithFile / (double)sumReport) * 100d) / 100d) + "%");
			}
			row.getCell(5).setCellValue("");
			//
			row = sheet.getRow(rowInsertAt + 1);
			if(row != null) {
				row.createCell(1).setCellValue(new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date()));
			}
			//
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			wb.write(baos);
			//
			actionRequest.getPortletSession(true).setAttribute("baosUploadStatisReport", baos);
			if(excelType.equalsIgnoreCase("A")) {
				actionRequest.setAttribute("secNoGened1", paSecNo);
			}else {
				actionRequest.setAttribute("secNoGened2", paSecNo);
			}
			actionResponse.setRenderParameter("jspPage", "/jsp/myreports/listUploadStatis.jsp");
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	private boolean isValidDate(String strDate) {
		String[] ss = strDate.split("-");
		if(ss.length != 3) return false;
		//
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sdf.setLenient(false);
            sdf.parse(strDate);
            if(Integer.parseInt(strDate.split("-")[0]) < 1911) throw new Exception("年度錯誤。");
            return true;
		}catch(Exception ex) {
			return false;
		}
	}
	
	//@SuppressWarnings("deprecation")
	//@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			User user = themeDisplay.getUser();
			//
			conn = ds.getConnection();
			//
			if(resRequest.getParameter("examNo") != null){		//呼叫 DB2 時使用
				String examNo = resRequest.getParameter("examNo");
				String leaderName = resRequest.getParameter("leaderName").trim();
				//
				String ret = this.fetchDB2ExamInfo(examNo, leaderName, themeDisplay);
				//
				JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
				resResponse.setContentType("text/javascript");
				jsonObject.put("ret", ret);
				resResponse.getWriter().write(jsonObject.toString());
			}else if(resRequest.getParameter("listReport") != null){		//輸出報表清單
				ErpUtil.logUserAction("產出報告案清單", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//
				Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
				//檢查是否系統管理者
				//PermissionChecker pc =  PermissionCheckerFactoryUtil.create(themeDisplay.getUser());
				//if(pc.isOmniadmin()){
				//	Collec collec = this.getReports(themeDisplay, conn, "");
				//	vecReport = collec.vecReport;
				//}
				Vector<String> vecSecAvail = new Vector<String>();
				if(user.getJobTitle().contains("組長")){
					vecSecAvail = this.getAvailSection(user);
				}
				//
				String dateStart = "";
				if(resRequest.getParameter("dateStart") != null) dateStart = resRequest.getParameter("dateStart").trim();
				String dateEnd = "";
				if(resRequest.getParameter("dateEnd") != null) dateEnd = resRequest.getParameter("dateEnd").trim();
				String secName = "";
				if(resRequest.getParameter("secName") != null) secName = resRequest.getParameter("secName").trim();
				//
				Collec collec = new Collec();
				if(secName.equals("")){
					collec = this.getReports(themeDisplay, conn, userFullName, secName, dateStart, dateEnd, alfSessionAdmin, vecSecAvail);
				}else{
					collec = this.getReports(themeDisplay, conn, "", secName, dateStart, dateEnd, alfSessionAdmin, vecSecAvail);
				}
				Vector<ReportProfile> vec = new Vector<ReportProfile>();
				for(ReportProfile r: collec.vecReport){
					//更新檢查案進度
					Vector<ExamProfile> _vec = new Vector<ExamProfile>(); 
					for(ExamProfile ep: r.vecExam){
						String _status = this.getExamProgress(alfSessionAdmin, ep.folderId);
						ep.stageReached = _status;
						_vec.add(ep);
					}
					r.vecExam = _vec;
					//
					vec.add(r);
				}
				for(ReportProfile r: collec.vecReportClosed){
					vec.add(r);
				}
				//取得使用者對照
				HashMap<String, User> hUser =this.getUserMap(); 
				Vector<String> vecSec = new Vector<String>();
				vecSec.add("金控公司組");
				vecSec.add("本國銀行組");
				vecSec.add("保險外銀組");
				vecSec.add("證券票券組");
				vecSec.add("地方金融組");
				vecSec.add("受託檢查組");
				//
				resResponse.setContentType("application/vnd.ms-excel");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode ("案件清單.xls", "utf-8") + "\"");
				OutputStream out = resResponse.getPortletOutputStream();
				//
				java.io.InputStream is = resRequest.getPortletSession().getPortletContext().getResourceAsStream("案件清單2.xls");
				HSSFWorkbook wb = new HSSFWorkbook(is);
				HSSFSheet sheet = wb.getSheetAt(0);
				//列印人
				HSSFRow row = sheet.getRow(1);
				//row.createCell(2);
				HSSFCell cell = row.getCell(2);
				cell.setCellValue(userFullName);
				//列印時間
				cell = row.getCell(10);
				cell.setCellValue(ErpUtil.getCurrDateTime());
				//
				int idx = 1;
				int rowInsertAt = 3;
				for(ReportProfile r: vec){
					for(ExamProfile e: r.vecExam){
						sheet.shiftRows(rowInsertAt, sheet.getLastRowNum(), 1, true, false);
						sheet.createRow(rowInsertAt);
						row = sheet.getRow(rowInsertAt);
						row.setHeight((short)-1);			//自動放高
						//
						HSSFRow origRow = sheet.getRow(rowInsertAt+1);
						for(int i=0; i < origRow.getLastCellNum(); i++){
							row.createCell(i);
							row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
						}
						//0
						cell = row.getCell(0);
						cell.setCellValue(idx);
						//1
						cell = row.getCell(1);
						cell.setCellValue(r.reportNo + "/" + e.examNo);
						//2
						cell = row.getCell(2);
						cell.setCellValue(r.caseName);
						//3
						cell = row.getCell(3);
						cell.setCellValue(r.secName + "/" + r.indusType);
						//4 受檢機構
						cell = row.getCell(4);
						cell.setCellValue(e.bankNameShort);
						//5 檢查期間
						cell = row.getCell(5);
						cell.setCellValue(e.dateStart + " ~ " + e.dateEnd);
						//6
						cell = row.getCell(6);
						cell.setCellValue(r.createdBy);
						//7
						cell = row.getCell(7);
						cell.setCellValue(r.dateCreated);
						//8
						cell = row.getCell(8);
						cell.setCellValue(r.dateBase);
						//9
						String _leaderName =  e.leaderName;
						if(hUser.containsKey(_leaderName)){
							User _u = hUser.get(_leaderName);
							String gName = "";
							List<UserGroup> _list = _u.getUserGroups();
							for(UserGroup _ug: _list){
								if(vecSec.contains(_ug.getName())){
									gName = _ug.getName();
								}
							}
							if(gName.equals("") && _list.size() > 0){
								gName = _list.get(0).getName();
							}
							if(!gName.equals("")){
								_leaderName += "(" + gName + ")";
							}
						}
						cell = row.getCell(9);
						cell.setCellValue(_leaderName);
						//10
						cell = row.getCell(10);
						cell.setCellValue(e.staffs);
						//11
						cell = row.getCell(11);
						if(r.closedBy.equals("")){
							if(e.closed == 1){
								cell.setCellValue("已關閉");
							}else if(e.dropped == 1){
								cell.setCellValue("已作廢");
							}else{
								if(!e.stageReached.equals("")){
									cell.setCellValue(e.stageReached);
								}else{
									cell.setCellValue("執行中");
								}
							}
						}else if(r.dropped == 1){
							cell.setCellValue("已作廢");
						}else{
							cell.setCellValue("已關閉");
						}
						//12
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
						String dateProposing = "";	//陳核日期
						AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(e.folderId);
						Iterator<CmisObject> it = fdExam.getChildren().iterator();
						while(it.hasNext()){
							CmisObject co = it.next();
							if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER) && co.getName().contains("陳核")){
								AlfrescoFolder fdStage = (AlfrescoFolder)co;
								it = fdStage.getChildren().iterator();
								while(it.hasNext()){
									co = it.next();
									String dateCurr = formatter.format(co.getLastModificationDate().getTime());
									if(dateCurr.compareTo(dateProposing) > 0){
										dateProposing = dateCurr;
									}
								}
								break;
							}
						}
						cell = row.getCell(12);
						cell.setCellValue(dateProposing);
						//13 結案日期
						cell = row.getCell(13);
						cell.setCellValue(r.dateClosed);
						//14 結案方式
						cell = row.getCell(14);
						if(!r.closedBy.equals("")){
							if(r.closedBy.equalsIgnoreCase("auto")){
								cell.setCellValue("系統自動");
							}else{
								cell.setCellValue("領隊手動");
							}
						}
						//
						rowInsertAt++;
						idx++;
					}
				}
				wb.write(out);
				//
				out.flush();
				out.close();
			}else if(resRequest.getParameter("downloadUploadStatisReport1") != null){		//一般上傳統計
				if(resRequest.getPortletSession(true).getAttribute("baosUploadStatisReport") == null) return;
				//
				String reportName = "重要查核項目報告表上傳作業統計報表_";
				String secNo = "";
				if(resRequest.getParameter("secNoGened1") != null) secNo = resRequest.getParameter("secNoGened1");
				if(secNo.equals("*")) {
					reportName += "所有組別";
				}else if(secNo.equalsIgnoreCase("H")) {
					reportName += "金控組";
				}else if(secNo.equalsIgnoreCase("B")) {
					reportName += "本銀組";
				}else if(secNo.equalsIgnoreCase("S")) {
					reportName += "證票組";
				}else if(secNo.equalsIgnoreCase("F")) {
					reportName += "保外組";
				}else if(secNo.equalsIgnoreCase("L")) {
					reportName += "地金組";
				}else {
					reportName += "制度組";
				}
				reportName += ".xls";
				//
				ByteArrayOutputStream baos = (ByteArrayOutputStream)resRequest.getPortletSession(true).getAttribute("baosUploadStatisReport");
				//
				resResponse.setContentType("application/vnd.ms-excel");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (reportName, "utf-8") + "\"");
				
				OutputStream out = resResponse.getPortletOutputStream();
				baos.writeTo(out);
                out.flush();
                baos.close();
                out.close();
			}else if(resRequest.getParameter("downloadUploadStatisReport2") != null){		//評述上傳統計
    			if(resRequest.getPortletSession(true).getAttribute("baosUploadStatisReport") == null) return;
    			//
    			String reportName = "檢查評述上傳作業統計報表_";
    			String secNo = "";
    			if(resRequest.getParameter("secNoGened2") != null) secNo = resRequest.getParameter("secNoGened2");
    			if(secNo.equals("*")) {
    				reportName += "所有組別";
    			}else if(secNo.equalsIgnoreCase("H")) {
    				reportName += "金控組";
    			}else if(secNo.equalsIgnoreCase("B")) {
    				reportName += "本銀組";
    			}else if(secNo.equalsIgnoreCase("S")) {
    				reportName += "證票組";
    			}else if(secNo.equalsIgnoreCase("F")) {
    				reportName += "保外組";
    			}else if(secNo.equalsIgnoreCase("L")) {
    				reportName += "地金組";
    			}else {
    				reportName += "制度組";
    			}
    			reportName += ".xls";
    			//
    			ByteArrayOutputStream baos = (ByteArrayOutputStream)resRequest.getPortletSession(true).getAttribute("baosUploadStatisReport");
    			//
    			resResponse.setContentType("application/vnd.ms-excel");
    			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
    			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (reportName, "utf-8") + "\"");
    				
    			OutputStream out = resResponse.getPortletOutputStream();
    			baos.writeTo(out);
                out.flush();
                baos.close();
                out.close();                
			}else if(resRequest.getParameter("downloadMySecReportNoList") != null){		//組別報告編號列表
				if(resRequest.getPortletSession(true).getAttribute("baosMySecReportNo") == null) return;
				//
				String reportName = "組別報告編號列表_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date());
				reportName += ".xls";
				//
				ByteArrayOutputStream baos = (ByteArrayOutputStream)resRequest.getPortletSession(true).getAttribute("baosMySecReportNo");
				//
				resResponse.setContentType("application/vnd.ms-excel");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (reportName, "utf-8") + "\"");
				
				OutputStream out = resResponse.getPortletOutputStream();
				baos.writeTo(out);
                out.flush();
                baos.close();
                out.close();
			}else if(resRequest.getParameter("listExamUsers") != null){		//人員權限列表
				resResponse.setContentType("application/vnd.ms-excel");
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode ("人員權限列表.xls", "utf-8") + "\"");
				OutputStream out = resResponse.getPortletOutputStream();
				//
				java.io.InputStream is = resRequest.getPortletSession().getPortletContext().getResourceAsStream("人員權限列表.xls");
				HSSFWorkbook wb = new HSSFWorkbook(is);
				HSSFSheet sheet = wb.getSheetAt(0);
				//列印時間
				HSSFRow row = sheet.getRow(1);
				HSSFCell cell = row.getCell(5);
				cell.setCellValue(ErpUtil.getCurrDateTime());
				//
				int idx = 1;
				int rowInsertAt = 3;
				//
				List<String> listSec = new ArrayList<>();
				listSec.add("局長");
				listSec.add("局長室秘書");
				listSec.add("副局長");
				listSec.add("副局長室秘書");
				listSec.add("主任秘書");
				listSec.add("主任秘書室秘書");
				listSec.add("檢查制度組");
				listSec.add("金控公司組");
				listSec.add("本國銀行組");
				listSec.add("地方金融組");
				listSec.add("證券票券組");
				listSec.add("證券票券組(二)");
				listSec.add("保險外銀組");
				listSec.add("受託檢查組");
				listSec.add("檢查局人員");
				listSec.add("政風室");
				listSec.add("秘書室");
				listSec.add("資訊室");
				listSec.add("主計室");
				listSec.add("人事室");
				//
				HashMap<String, List<User>> mapSecUser = new HashMap<>();
				HashMap<String, String> mapUserRole = new HashMap<>();
				
				List<User> listAllUser = UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
				for(User u: listAllUser) {
					//判斷是否為檢查人員
					boolean isExamer = false;
					String strRole = "";
					List<Role> listRole = u.getRoles();
					for(Role _r: listRole){
						if(_r.getName().toLowerCase().contains("user")) continue;
						//
						if(_r.getName().equals("檢查人員") 
								|| _r.getName().equals("檢查報告上傳管理者")
								|| _r.getName().equals("報告列表")
								|| _r.getName().equals("範本維護")){
							isExamer = true;
							//break;
						}
						if(!strRole.isEmpty()) strRole += "、";
						strRole += _r.getName();
					}
					if(!isExamer) continue;
					//
					mapUserRole.put(u.getScreenName(), strRole);
					//組室
					for(UserGroup ug: u.getUserGroups()) {
						if(!listSec.contains(ug.getName())) continue;
						//
						List<User> _list = new ArrayList<>();
						if(mapSecUser.containsKey(ug.getName())) _list = mapSecUser.get(ug.getName());
						_list.add(u);
						mapSecUser.put(ug.getName(), _list);
						//
						break;
					}
				}
				//
				for(String secName: mapSecUser.keySet()) {
					for(User u: mapSecUser.get(secName)) {
						String strRoles = mapUserRole.get(u.getScreenName());
						String[] arrRole = strRoles.split("、");
						for(String roleName: arrRole) {
							sheet.shiftRows(rowInsertAt, sheet.getLastRowNum(), 1, true, false);
							sheet.createRow(rowInsertAt);
							row = sheet.getRow(rowInsertAt);
							row.setHeight((short)-1);			//自動放高
							//
							HSSFRow origRow = sheet.getRow(rowInsertAt+1);
							for(int i=0; i < origRow.getLastCellNum(); i++){
								row.createCell(i);
								row.getCell(i).setCellStyle(origRow.getCell(i).getCellStyle());
							}
							//帳號
							cell = row.getCell(0);
							cell.setCellValue(u.getScreenName());
							//姓名
							cell = row.getCell(1);
							cell.setCellValue(u.getLastName() + u.getFirstName());
							//組室
							String s = "";
							for(UserGroup ug: u.getUserGroups()) {
								if(ug.getName().equals("檢查局人員")) continue;
								//
								if(!listSec.contains(ug.getName())) continue;
								//
								if(!s.isEmpty()) s += "、";
								s += ug.getName();
							}
							cell = row.getCell(2);
							cell.setCellValue(s);
							//權限
							cell = row.getCell(3);
							cell.setCellValue(roleName);
							//清查結果
							cell = row.getCell(4);
							if(u.isActive()) {
								cell.setCellValue("■帳號續用 □刪除帳號");
							}else {
								cell.setCellValue("□帳號續用 ■刪除帳號");
							}
							//備註
							cell = row.getCell(5);
							cell.setCellValue("");
							//
							rowInsertAt++;
						}
					}
				}
				wb.write(out);
				//
				out.flush();
				out.close();
			}
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private org.apache.chemistry.opencmis.client.api.Folder getExamFolder(Session alfSessionAdmin, String folderId, String examNo) {
		org.apache.chemistry.opencmis.client.api.Folder ret = null;
		try{
			ret = (org.apache.chemistry.opencmis.client.api.Folder)alfSessionAdmin.getObject(folderId);
		}catch(Exception _ex){
			try{
				org.apache.chemistry.opencmis.client.api.Folder folderReport = (org.apache.chemistry.opencmis.client.api.Folder)alfSessionAdmin.getObject(folderId);
				//
				Iterator<CmisObject> it = folderReport.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId() == BaseTypeId.CMIS_FOLDER && co.getName().equalsIgnoreCase(examNo)){
						ret = (AlfrescoFolder)co;
						break;
					}
				}
			}catch(Exception __ex){
			}
		}
		return ret;
	}
	
	private HashMap<String, User> getUserMap() throws Exception{
		HashMap<String, User> hm = new HashMap<String, User>();
		//
		List<User> listUser = UserLocalServiceUtil.getUsers(0, UserLocalServiceUtil.getUsersCount());
		for(User _u: listUser){
			String _userFullName = _u.getLastName().trim() + _u.getFirstName().trim();
			hm.put(_userFullName,  _u);
		}
		return hm;
	}
	
	/*
	public void loadReport(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			int reportId = Integer.parseInt(actionRequest.getParameter("reportId"));
			//System.out.println("Project Id: " + projectId);
			//
			QName qName = new QName("http://itez.com", "sendReportId", "x");
			 actionResponse.setEvent(qName, String.valueOf(reportId) + "#false");
			 //
			 actionRequest.setAttribute("successMsg", "案件資料請由右方視窗檢視。");
			 SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	*/
	
	//取得在 Alfresco 的報告儲存區
	public AlfrescoFolder getReportsRootFolder(Session alfSession) throws Exception{
		AlfrescoFolder ret = null;
		//
		Iterator<CmisObject>  it = alfSession.getRootFolder().getChildren().iterator();
		 while(it.hasNext()){
			 CmisObject co = it.next();
			 if(co.getName().equals(rootReports)){
				ret = (AlfrescoFolder)co;
				break;
			 }
		 }
		 return ret;
	}
	
	//取得在 Alfresco 的工作範本區
	public AlfrescoFolder getJobTempRootFolder(Session alfSession) throws Exception{
		AlfrescoFolder ret = null;
		//
		Iterator<CmisObject>  it = alfSession.getRootFolder().getChildren().iterator();
		 while(it.hasNext()){
			 CmisObject co = it.next();
			 if(co.getName().equals(rootJobTemp)){
				ret = (AlfrescoFolder)co;
				break;
			 }
		 }
		 return ret;
	}

	//取得在 Liferay 的檔案樣板區
	public Folder getDocTempRootFolder(ThemeDisplay themeDisplay) throws Exception{
		Folder ret = null;
		//
		try{
			ret = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootDocTemp);
		}catch(Exception ex){
		}
		return ret;
	}

	//取得檢查行政資訊
	private String fetchDB2ExamInfo(String examNo, String leaderName, ThemeDisplay themeDisplay) throws Exception {
		String ret = "";
		Connection  db2Conn = null;
		try{
			try{
				Class.forName(PropsUtil.get("db2.driver"));
				// establish a connection to DB2
				db2Conn = DriverManager.getConnection(PropsUtil.get("db2.url"),
	        											PropsUtil.get("db2.user"),
	        											PropsUtil.get("db2.password"));
			}catch(Exception _ex){
				ret = "Database 連線失敗！";
				return ret;
			}
			if(db2Conn == null){
				ret = "Database 連線失敗！";
				return ret;
			}
			//
			User user = themeDisplay.getUser();
			String screenNameLeader = ErpUtil.getUserScreenName(themeDisplay, leaderName);
			if(screenNameLeader == null || screenNameLeader.equals("")){
				//return  "NotFound#查無領隊「" + leaderName + "」之基本資料！";
				screenNameLeader = user.getScreenName();
			}
			//String leaderName = "";
			String staffs = "";
			String bankName = "";
			String bankNameShort = "";
			//String dateBase = "";			//檢查基準日
			String dateStart = "";
			String dateEnd = "";
			String dateApproval = "";
			double workDays = 0.0;
			String examNature = "";			//0.一般檢查  1.專案檢查
			//
			CallableStatement cstmt = db2Conn.prepareCall("EXEC [spExd02] ?,?");
			cstmt.setString (1, examNo);
			cstmt.setString(2, screenNameLeader.toLowerCase());
			//cstmt.setString(2, "jylin");
			ResultSet rs = cstmt.executeQuery();
			while(rs.next()){
				String userName = "";
				if(rs.getString("username") != null){
					userName = rs.getString("username");
				}
			      	if(!userName.equals("")){
					int isLeader = rs.getInt("leader");
				      	if(isLeader == 0){
				      		leaderName = userName;
				      	}else{
				      		if(!staffs.equals("")) staffs += ";";
				      		staffs += userName;
				      	}
			      	}
			}
			//將領隊自動補入助檢成員中
			if(!leaderName.equals("") && !staffs.equals("") && !staffs.contains(leaderName)){
				staffs += ( ";" + leaderName );
			}
			rs.close();
			//
			cstmt = db2Conn.prepareCall("EXEC [spExd03] ?,?");
			cstmt.setString (1, examNo);
			cstmt.setString(2, screenNameLeader.toLowerCase());
			//cstmt.setString(2, "jylin");
			rs = cstmt.executeQuery();
			while(rs.next()){
				if(rs.getString("bank_name") != null){
					bankName = rs.getString("bank_name");
				}
				if(rs.getString("bank_b_name") != null){
					bankNameShort = rs.getString("bank_b_name");
				}
			     	//dateBase = rs.getString("base_date");
				if(rs.getString("st_date") != null){
					dateStart = rs.getString("st_date");
				}
				if(rs.getString("en_date") != null){
					dateEnd = rs.getString("en_date");
				}
				if(rs.getString("appr_date") != null){
					dateApproval = rs.getString("appr_date");
				}
			    workDays = rs.getDouble("workdays");
			    if(rs.getInt("ch_type") == 0) {
			    	examNature = "一般檢查";	
			    }else if(rs.getInt("ch_type") == 1) {
			    	examNature = "專案檢查";
			    }
			    
			}
			rs.close();
			//
			if(dateStart.contains("/") && dateStart.split("/").length == 3){
				String[] ss = dateStart.split("/");
				dateStart = (Integer.parseInt(ss[0]) + 1911) + "-" + this.toChineseMonth(Integer.parseInt(ss[1])) + "月" + "-";
				String dd = ss[2];
				if(dd.startsWith("0")) dd = dd.substring(1);
				dateStart += dd;
			}
			if(dateEnd.contains("/") && dateEnd.split("/").length == 3){
				String[] ss = dateEnd.split("/");
				dateEnd = (Integer.parseInt(ss[0]) + 1911) + "-" + this.toChineseMonth(Integer.parseInt(ss[1])) + "月" + "-";
				String dd = ss[2];
				if(dd.startsWith("0")) dd = dd.substring(1);
				dateEnd += dd;
			}
			//
			if(!leaderName.equals("") && !staffs.equals("") && !bankName.equals("")){
				ret = "leaderName=" + leaderName + "#";
				ret += "staffs=" + staffs + "#";
				ret += "bankName=" + bankName + "#";
				ret += "bankNameShort=" + bankNameShort + "#";
				//ret += "dateBase=" + dateBase + "#";
				ret += "dateStart=" + dateStart + "#";
				ret += "dateEnd=" + dateEnd + "#";
				ret += "dateApproval=" + dateApproval + "#";
				ret += "workDays=" + workDays + "#";
				ret += "examNature=" + examNature;
			}else{
				ret = "NotFound#查無領隊「" + leaderName + "」與檢查證號「" + examNo + "」之資料！";
			}
		}catch(Exception ex){
			ret = "Database Stored Procedure 執行錯誤！";
		}finally{
			try{
				if(db2Conn != null) db2Conn.close();
			}catch(Exception _ex){
			}
		}
		//
		return ret;
	}	

	
	private String toChineseMonth(int i) throws Exception{
		switch (i){
			case 1:
				return "一";
			case 2:
				return "二";
			case 3:
				return "三";
			case 4:
				return "四";
			case 5:
				return "五";
			case 6:
				return "六";
			case 7:
				return "七";
			case 8:
				return "八";
			case 9:
				return "九";
			case 10:
				return "十";
			case 11:
				return "十一";
			case 12:
				return "十二";
			default:
				return "";
		}
	}
}


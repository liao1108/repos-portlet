<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.ExamProfile" %>
<%@ page import="com.itez.Collec" %>

<portlet:defineObjects />

<%
	Collec collec = new Collec();
	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	}
	//
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	ExamProfile ei = (ExamProfile)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="editExamURL">
		<portlet:param name="jspPage"  value="/jsp/myreports/editExam.jsp"/>
		<portlet:param name="examId" value="<%=String.valueOf(ei.examId) %>"/>
		<portlet:param name="reportId" value="<%=String.valueOf(ei.reportId) %>"/>
	</portlet:renderURL>
	<%if(ei.dropped == 0 && ei.createdBy.equals(userFullName) || ei.pmName.contains(userFullName) || ei.leaderName.contains(userFullName) || collec.isSupervisor){ %>
		<liferay-ui:icon message="編輯" url="<%=editExamURL.toString() %>"/>
	<%}%>
</liferay-ui:icon-menu>


<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.ReportProfile" %>

<portlet:defineObjects />

<%
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
	}
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	ReportProfile ri = (ReportProfile)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="editReportURL" >
		<portlet:param name="jspPage" value="/jsp/myreports/editReport.jsp"/>
		<portlet:param name="reportId" value="<%=String.valueOf(ri.reportId) %>"/>
		<portlet:param name="closed" value= "true"/>
	</portlet:renderURL>
	
	<%if(ri.createdBy.equals(userFullName) || ri.pmName.contains(userFullName) || ri.leaderName.contains(userFullName)){ %>
		<liferay-ui:icon message="編輯" url="<%=editReportURL.toString() %>"/>
	<%} %>	</liferay-ui:icon-menu>
<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>
<%@ page import= "com.itez.TxLog"%>

<portlet:defineObjects />


  <%
  	//ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
       //User user = themeDisplay.getUser();
       //
       int reportId = Integer.parseInt(renderRequest.getParameter("reportId"));
       boolean closed = false;
       if(renderRequest.getParameter("closed") != null && renderRequest.getParameter("closed").equalsIgnoreCase("true")){
	       closed = true;
       }
       //
       Collec collec = new Collec();
       if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
       	collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
       }
       //
       ReportProfile ri  = new ReportProfile();
       if(!closed){
	     	for(ReportProfile _rp: collec.vecReport){
	     		if(_rp.reportId == reportId){
	     			ri = _rp;
	     			break;
	     		}
	     	}
       }else{
	       for(ReportProfile _rp: collec.vecReportClosed){
		     	if(_rp.reportId == reportId){
		     		ri = _rp;
		     		break;
		     	}
		  }
       }
       //
        if(ri.dateBase.equals("")){
        	ri.dateBase = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        }
        if(ri.dateReporting.equals("")){
        	ri.dateReporting = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        }
        if(ri.reportYear == 0){
        	ri.reportYear = Calendar.getInstance().get(Calendar.YEAR) - 1911;
        }
        //
        Vector<TxLog> vecTxLog= ri.vecTxLog;
        //
        boolean isLeader = false;
        if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
        	isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
        }
        boolean isPM = false;
        if(renderRequest.getPortletSession(true).getAttribute("isPM") != null){
        	isPM = (Boolean)renderRequest.getPortletSession(true).getAttribute("isPM");
        }
        //
        Vector<String> vecSecIndus  = new Vector<String>();
        if(renderRequest.getPortletSession(true).getAttribute("vecSecIndus") != null){
        	vecSecIndus = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecIndus");
        }
        //所有組別
        Vector<String> vecSecAll  = new Vector<String>();
        if(renderRequest.getPortletSession(true).getAttribute("vecSecAll") != null){
        	vecSecAll = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecAll");
        }
        
        Vector<String> vecSecAll2 = new Vector<String>();
        for(String sec: vecSecAll){
        	vecSecAll2.add(sec);
        }
        if(!vecSecAll2.contains("檢查制度組")) vecSecAll2.add("檢查制度組");
        
        //登入者隸屬組別
        String secBelongTo = ri.secBelongTo;
        if(secBelongTo.equals("")){
	        if(renderRequest.getPortletSession(true).getAttribute("secBelongTo") != null){
	        	secBelongTo =  renderRequest.getPortletSession(true).getAttribute("secBelongTo").toString();       	
	        }
        }
        //
        String userFullName = "";
        if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
        	userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
        }
     	//
     	String errMsg = "";	
     	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
     	String successMsg = "";	
     	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateReportURL"  name="updateReport" >
</portlet:actionURL>		

<portlet:resourceURL var="doSelectValUrl">
	<portlet:param name="selectedVal" value="PARAM_PLACEHOLDER_SELECTED_VAL" />
</portlet:resourceURL>

<portlet:renderURL var="queryIndustryURL"  windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
  	<portlet:param name="jspPage"  value="/jsp/myreports/queryIndustry.jsp"/>
  	<portlet:param name="secNo"  value="SEC_NO_TO_BE_REPLACE"/>
</portlet:renderURL>

<portlet:renderURL var="goBackToViewURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/view.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="goCloseReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/close.jsp"/>
  	<portlet:param name="reportId" value="<%=String.valueOf(reportId) %>"/>
</portlet:renderURL>

<portlet:renderURL var="goDropReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/reportDrop.jsp"/>
  	<portlet:param name="reportId" value="<%=String.valueOf(reportId) %>"/>
</portlet:renderURL>

<portlet:renderURL var="goReOpenReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/reOpen.jsp"/>
  	<portlet:param name="reportId" value="<%=String.valueOf(reportId) %>"/>
</portlet:renderURL>

<script type="text/javascript">
	Liferay.Portlet.ready(
		function(portletId, node) {
		        document.getElementById('<portlet:namespace />secName').focus();
		}
	);
	
	Liferay.on('receiveIndustry', 
			function(event, p_data){
				document.getElementById("<portlet:namespace/>indusType").value = p_data;
				document.getElementById('<portlet:namespace />caseName').focus();
			}
	);
	
	function showIndusPopup() {
		var val = document.getElementById("<portlet:namespace/>secName").value.substring(0,1);
		if(val == ""){
			alert("請先指定組別。");				
			return;
		}
		//
		var A = AUI();
		A.use("aui-io-request", 
					"aui-dialog",
					"aui-io', 'event",
					"event-custom",
					function(A) {
		    							var dialog = new A.Dialog({
		            														title: "業別選擇",
		            														centered: true,
		            														draggable: true,
		            														modal: true,
		            														width: 500,
		            														height: 400,
		        														}).plug(A.Plugin.IO, {uri:"<%=queryIndustryURL.toString()%>".replace("SEC_NO_TO_BE_REPLACE", val) }).render();
		       
		        						dialog.show();
					});
	};
	
</script>


<aui:layout>
	<aui:column cssClass="header">
	 	<%if(ri.reportId == 0){ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/new_a_folder.png">&nbsp;建立新案
	 	<%}else{ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/labels.png">&nbsp;本案基本資料
	 	<%} %>
	</aui:column>
</aui:layout>
<hr/>

<aui:fieldset >
<aui:layout>
<aui:column columnWidth="50">
<aui:form action="<%=updateReportURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
			<aui:select name="reportSecIndus"  label="檢查報告別"  cssClass="td">
				<aui:option value="">&nbsp;</aui:option>
				<%for(String sn: vecSecIndus){	%>
					<aui:option value="<%=sn%>"  selected="<%=ri.reportSecIndus.equals(sn) %>"><%=sn %></aui:option>
				<%} %>
			</aui:select>	
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:select name="jobSecIndus"  label="工作範本別"  cssClass="td">
				<aui:option value="">&nbsp;</aui:option>
				<%for(String sn: vecSecIndus){	%>
					<aui:option value="<%=sn%>"  selected="<%=ri.jobSecIndus.equals(sn) %>"><%=sn %></aui:option>
				<%} %>
			</aui:select>	
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column>
			<aui:select name="secBelongTo"  label="報告歸屬組別"  cssClass="td">
				<%for(String _sec: vecSecAll2){	%>
					<aui:option value="<%=_sec%>"  selected="<%=secBelongTo.equals(_sec) %>"><%=_sec %></aui:option>
				<%} %>
			</aui:select>	
		</aui:column>
	</aui:layout>
				
	<aui:layout>
		<aui:column>
			<aui:input type="hidden" name="reportId"  value="<%=ri.reportId %>"/>
			<aui:input type="hidden" name="folderId"  value="<%=ri.folderId %>"/>
			<aui:input type="hidden" name="userFullName"  value="<%=userFullName %>"/>
			<aui:input name="caseName" label="案名 (建議專案檢查時再填)" value="<%=ri.caseName %>"  size="40"  cssClass="td"/>
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column>	
			<aui:input name="reportYear" label="報告年度" value="<%=ri.reportYear %>"  cssClass="td"/>
		</aui:column>

		<aui:column>	
			<aui:input name="reportNo" label="報告編號(系統自動產出)" value="<%=ri.reportNo %>"  readonly="readonly" cssClass="td" />
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column>	
			<h4>檢查基準日</h4>
			<liferay-ui:input-date dayParam="dayBase"  monthParam="monBase"  yearParam="yearBase"
										 yearValue="<%=ErpUtil.getYear(ri.dateBase) %>"  monthValue="<%=ErpUtil.getMonth(ri.dateBase) - 1%>"  dayValue="<%=ErpUtil.getDay(ri.dateBase) %>"
											yearRangeStart="<%=ErpUtil.getYear(ri.dateBase)  - 1 %>" yearRangeEnd="<%=ErpUtil.getYear(ri.dateBase) + 1%>"/>	
		</aui:column>
		<aui:column>
			<h4>報告提出日</h4>
			<liferay-ui:input-date dayParam="dayReporting"  monthParam="monReporting"  yearParam="yearReporting"
										 yearValue="<%=ErpUtil.getYear(ri.dateReporting) %>"  monthValue="<%=ErpUtil.getMonth(ri.dateReporting) - 1%>"  dayValue="<%=ErpUtil.getDay(ri.dateReporting) %>"
											yearRangeStart="<%=ErpUtil.getYear(ri.dateReporting)  - 1 %>" yearRangeEnd="<%=ErpUtil.getYear(ri.dateReporting) + 1%>"/>	
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column>
			<aui:input name="pmName" label="專案代理人(可多人)" value="<%=ri.pmName %>"  cssClass="td"  size="30"/>
		</aui:column>
	</aui:layout>
	<%if(!ri.dateCreated.equals("")){ %>
		<aui:layout>			
				<aui:column>	
					<aui:input name="dateCreate" label="本案建立日" value="<%=ri.dateCreated %>" readonly="readonly" cssClass="td"/>
				</aui:column>
				<aui:column>	
					<aui:input name="createdBy" label="建立人" value="<%=ri.createdBy %>" readonly="readonly" cssClass="td"/>
				</aui:column>
		</aui:layout>
	<%} %>
	<%if(!ri.dateClosed.equals("")){ %>
		<aui:layout>	
			<aui:column>	
				<aui:input name="dateClosed" label="關閉日期" value="<%=ri.dateClosed %>" readonly="readonly" cssClass="td"/>
			</aui:column>
			<aui:column>	
				<aui:input name="closedBy" label="關閉人" value="<%=ri.closedBy %>" readonly="readonly" cssClass="td"/>
			</aui:column>
		</aui:layout>
	<%} %>		
		
	<aui:button-row>
		<aui:button onClick="<%=goBackToViewURL.toString() %>"   value="返回上頁"  cssClass="button"/>
		<%if(ri.reportId == 0){ %>
			<aui:button type="submit"   value="確定建立"  cssClass="button"/>
		<%}else if(ri.reportId > 0){ %>
			<%if(ri.dateClosed.equals("")){ %>
				<aui:button type="submit"   value="確定更新"   cssClass="button"/>
				<aui:button onClick="<%=goCloseReportURL.toString() %>"  value="關閉本案"  cssClass="button"/>
				<aui:button onClick="<%=goDropReportURL.toString() %>"  value="作廢本案"  cssClass="button"/>
			<%}else if(ri.dropped == 0){%>
				<aui:button onClick="<%=goReOpenReportURL.toString() %>"  value="重啟本案"  cssClass="button"/>
			<%} %>
		<%} %>
	</aui:button-row>
</aui:form>
</aui:column>

<aui:column columnWidth="50">			
		<aui:layout>
			<aui:column cssClass="td">本案之異動記錄</aui:column>
		</aui:layout>
		<hr>
		
		<liferay-ui:search-container>
			<liferay-ui:search-container-results>
				<%
					results = ListUtil.subList(vecTxLog, searchContainer.getStart(), searchContainer.getEnd());
					total = vecTxLog.size();
					pageContext.setAttribute("results", results);
					pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row className="com.itez.TxLog" keyProperty="txDate" modelVar="txLog">
				<liferay-ui:search-container-column-text align="center">
					<img src="<%=renderRequest.getContextPath()%>/images/record.png"/>
				</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-text name="異動日期" property="txDate"/>
				<liferay-ui:search-container-column-text name="異動種類" property="txType" />
				<liferay-ui:search-container-column-text name="異動人" property="txBy" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator/>
		</liferay-ui:search-container>
</aui:column>	
</aui:layout>
</aui:fieldset>




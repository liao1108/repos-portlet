<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

<%
	String fileId = "";
	if(renderRequest.getParameter("fileId") != null){
		fileId = renderRequest.getParameter("fileId");
	}
	String fileName = "";
	if(renderRequest.getParameter("fileName") != null){
		fileName = renderRequest.getParameter("fileName");
	}
	//可列出的組別
	Vector<String> vecSecPriv = new Vector<String>();	
	if(renderRequest.getPortletSession(true).getAttribute("vecSecPriv") != null) vecSecPriv = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecPriv");
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/myreports/view.jsp"/>
</portlet:actionURL>

<portlet:resourceURL var="listReportURL">
	 <portlet:param name="listReport" value="listReport"/>
</portlet:resourceURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/query.png">&nbsp;請指定列表條件
	</aui:column>
</aui:layout>

<hr/>

<br/>

<aui:fieldset>
<aui:form action="<%=listReportURL.toString()%>"  method="post" >
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="timeStamp"   value="<%=ErpUtil.getCurrDateTime() %>"  cssClass="td" />
			<aui:input name="dateStart"  label="建立日(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>
		
		<aui:column cssClass="td">
			<br>～
		</aui:column>
		
		<aui:column cssClass="td">
			<aui:input name="dateEnd"  label="截止日(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>

		<aui:column cssClass="td">
			<aui:select name="secName"  label="組別"  cssClass="td">
				<%for(String secName: vecSecPriv){ %>
					<aui:option value="<%=secName%>"  ><%=secName%></aui:option>
				<%} %>
			</aui:select>
		</aui:column>	
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消" />
		</aui:column>
		<aui:column>
			<aui:button type="submit" value="確定" />
		</aui:column>
	</aui:layout>
</aui:form>
</aui:fieldset>
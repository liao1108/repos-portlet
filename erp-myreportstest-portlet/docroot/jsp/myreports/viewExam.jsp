<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ExamProfile"%>
<%@ page import= "com.itez.ErpUtil"%>
<%@ page import= "com.itez.TxLog"%>

<portlet:defineObjects />


  <%
  			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
  			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay); 
            	//
            	int reportId = Integer.parseInt(renderRequest.getParameter("reportId"));
            	boolean closed = false;
            	if(renderRequest.getParameter("closed") != null && renderRequest.getParameter("closed").equalsIgnoreCase("true")){
            		closed = true;
            	}
            	//
            	Collec collec = new Collec();
            	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
            		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
            	}
            	//
            	ReportProfile ri  = new ReportProfile();
            	if(!closed){
            		for(ReportProfile _rp: collec.vecReport){
	           			if(_rp.reportId == reportId){
	           				ri = _rp;
	           				break;
	           			}
	           		}
            	}else{
            		for(ReportProfile _rp: collec.vecReportClosed){
	            		if(_rp.reportId == reportId){
	            			ri = _rp;
	            			break;
	            		}
	            	}
            	}
            	Vector<ExamProfile> vecExam = ri.vecExam;
            	//
            	if(ri.dateBase.equals("")){
            		ri.dateBase = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            	}
            	if(ri.dateReporting.equals("")){
            		ri.dateReporting = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            	}
            	//
            	//boolean isLeader = false;
            	//if(ri.leaderName.contains(userFullName) || ri.closedBy.contains(userFullName)){
            	//	isLeader = true;
            	//}
            	//boolean isPM = false;
            	//if(ri.pmName.contains(userFullName)){
            	//	isPM = true;
            	//}
            	//
            	Vector<String> vecSecName  = new Vector<String>();
            	if(renderRequest.getPortletSession(true).getAttribute("vecSecName") != null){
            		vecSecName = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecName");
            	}
            	//
            	//新增時將領隊名字直接壓上
            	//if(isProjectLeader && pi.projectId < 0){
            	//	pi.leaderName =  userFullName;
            	//}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateReportURL"  name="updateReport" >
</portlet:actionURL>		

<portlet:resourceURL var="doSelectValUrl">
	<portlet:param name="selectedVal" value="PARAM_PLACEHOLDER_SELECTED_VAL" />
</portlet:resourceURL>

<portlet:renderURL var="queryIndustryURL"  windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
  	<portlet:param name="jspPage"  value="/html/myreports/queryIndustry.jsp"/>
  	<portlet:param name="secNo"  value="SEC_NO_TO_BE_REPLACE"/>
</portlet:renderURL>

<portlet:renderURL var="goBackViewReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/view.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="newExamURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/editExam.jsp"/>
  	<portlet:param name="reportId"  value="<%=String.valueOf(reportId) %>"/>
  	<portlet:param name="examId"  value="0"/>
</portlet:renderURL>

<portlet:renderURL var="goCloseReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/close.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="goReOpenReportURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/reOpen.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="goArchivingURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/archiving.jsp"/>
</portlet:renderURL>

<script type="text/javascript">
	Liferay.Portlet.ready(
		function(portletId, node) {
		        document.getElementById('<portlet:namespace />secName').focus();
		}
	);
	
	Liferay.on('receiveIndustry', 
			function(event, p_data){
				document.getElementById("<portlet:namespace/>indusType").value = p_data;
				document.getElementById('<portlet:namespace />caseName').focus();
			}
	);
	
	function showIndusPopup() {
		var val = document.getElementById("<portlet:namespace/>secName").value.substring(0,1);
		if(val == ""){
			alert("請先指定組別。");				
			return;
		}
		//
		var A = AUI();
		A.use("aui-io-request", 
					"aui-dialog",
					"aui-io', 'event",
					"event-custom",
					function(A) {
		    							var dialog = new A.Dialog({
		            														title: "業別選擇",
		            														centered: true,
		            														draggable: true,
		            														modal: true,
		            														width: 500,
		            														height: 400,
		        														}).plug(A.Plugin.IO, {uri:"<%=queryIndustryURL.toString()%>".replace("SEC_NO_TO_BE_REPLACE", val) }).render();
		       
		        						dialog.show();
					});
	};
	
</script>

<aui:layout>
	<aui:column cssClass="header"><img src="<%=renderRequest.getContextPath()%>/images/examCert.png" >&nbsp;
本案(報告編號：<%=ri.reportNo %>，案名：<%=ri.caseName %>)之檢查證列表</aui:column>
</aui:layout>
<hr/>
<br>
<aui:layout>	
	<aui:column>&nbsp;</aui:column>
	<aui:column cssClass="td" >
		<aui:button onClick="<%=goBackViewReportURL.toString() %>" value="返回上頁" cssClass="button"/>
	</aui:column>
	<%if(ri.reportId > 0 && (ri.isLeader || ri.createdBy.contains(userFullName)) && ri.dateClosed.equals("")){ %>
		<aui:column cssClass="td" align="right">
			<aui:button onClick="<%=newExamURL.toString() %>" value="新增檢查證" cssClass="button"/>
		</aui:column>
	<%} %>
</aui:layout>
<br>
<liferay-ui:search-container emptyResultsMessage="本案尚未建立檢查證。" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecExam, searchContainer.getStart(), searchContainer.getEnd());
			total = vecExam.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>

	<liferay-ui:search-container-row className="com.itez.ExamProfile" keyProperty="examId" modelVar="examProfile">
		<portlet:actionURL var="enterExamURL" name="enterExam">
			<portlet:param name="examId" value="<%=String.valueOf(examProfile.examId) %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/bank.png"/>
		</liferay-ui:search-container-column-text>
		<%if(examProfile.dropped == 0){ %>
			<liferay-ui:search-container-column-text name="檢查證號" property="examNo"  href="<%=enterExamURL.toString()%>"/>
		<%}else{ %>
			<liferay-ui:search-container-column-text name="檢查證號" property="examNo"/>
		<%} %>
		<liferay-ui:search-container-column-text name="檢查性質" property="examNature" />	
		<liferay-ui:search-container-column-text name="受檢機構" property="bankName" />
		<liferay-ui:search-container-column-text name="領隊" property="leaderName" />
		<liferay-ui:search-container-column-text name="助檢" property="staffs" />
		<liferay-ui:search-container-column-text name="起始日期" property="dateStart" />
		<liferay-ui:search-container-column-text name="結束日期" property="dateEnd" />
		<liferay-ui:search-container-column-text name="助檢報告日期" property="deadlineStaff" />
		<liferay-ui:search-container-column-text name="檢討會日期" property="dateMeeting" />
		<liferay-ui:search-container-column-text name="作廢否" property="dropped" />
		
		<liferay-ui:search-container-column-jsp path="/jsp/admin/adminEditExam.jsp" align="center" />
			
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>




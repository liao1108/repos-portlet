<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import="com.itez.IndusType" %>

<portlet:defineObjects />

<%
	Vector<String> vecIndusType = new Vector<String>();

	String secNo = renderRequest.getParameter("secNo");
	
	Hashtable<String, Vector<String>> htIndusType = new Hashtable<String, Vector<String>>(); 
	if(renderRequest.getPortletSession(true).getAttribute("htIndusType") != null){
		htIndusType = (Hashtable<String, Vector<String>>)renderRequest.getPortletSession(true).getAttribute("htIndusType");
	}
	vecIndusType = htIndusType.get(secNo);
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<script  type="text/javascript">	
	function sendingIndustry(industry) {
		Liferay.fire('receiveIndustry', industry);
		//
		AUI().DialogManager.hideAll();
	}
</script>

<liferay-ui:search-container delta="500"  emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecIndusType, searchContainer.getStart(), searchContainer.getEnd());
			total = vecIndusType.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.IndusType" keyProperty="typeKey" modelVar="IndusType">
		<liferay-ui:search-container-column-text name="業別" property="industryName" />
		<liferay-ui:search-container-column-text name="檢查案屬性" property="projectType"/>
		<liferay-ui:search-container-column-jsp path="/jsp/admin/select_actions.jsp" align="center" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
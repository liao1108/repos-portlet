<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    User user = themeDisplay.getUser();
    //
    String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
    //
    Collec collec = new Collec();
    if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
    	collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
    }
    //
    int rowsPrefer = 20;
    if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
	 	rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
	}
    //
    boolean isAdmin = false;
    if(renderRequest.getPortletSession(true).getAttribute("isAdmin") != null 
    		&& renderRequest.getPortletSession(true).getAttribute("isAdmin").toString().equalsIgnoreCase("true")){
    	isAdmin = true;
    }
    boolean isUploadManager = false;
    if(renderRequest.getPortletSession(true).getAttribute("isUploadManager") != null 
    		&& renderRequest.getPortletSession(true).getAttribute("isUploadManager").toString().equalsIgnoreCase("true")){
    	isUploadManager = true;
    }
    boolean isSecReporter = false;
    if(renderRequest.getPortletSession(true).getAttribute("isSecReporter") != null 
    		&& renderRequest.getPortletSession(true).getAttribute("isSecReporter").toString().equalsIgnoreCase("true")){
    	isSecReporter = true;
    }
    
    //
    String secBelongTo = "";
    if(renderRequest.getPortletSession(true).getAttribute("secBelongTo") != null){
    	secBelongTo = renderRequest.getPortletSession(true).getAttribute("secBelongTo").toString();
    }
    //
    String errMsg = "";	
    if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
    String successMsg = "";	
    if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="createReportURL">
	<portlet:param name="jspPage" value="/jsp/myreports/editReport.jsp"/>
	<portlet:param name="reportId"  value="0"/>
</portlet:renderURL>

<portlet:resourceURL var="listReportURL" >
	<portlet:param name="timeStamp"  value="<%=ErpUtil.getCurrDateTime() %>"/>
</portlet:resourceURL>

<portlet:resourceURL var="listExamUsersURL" >
	<portlet:param name="listExamUsers"  value="true"/>
</portlet:resourceURL>

<portlet:renderURL var="goListReportURL">
	<portlet:param name="jspPage" value="/jsp/myreports/listReport.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="goListUploadStatisURL">
	<portlet:param name="jspPage" value="/jsp/myreports/listUploadStatis.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="goListMySecReportNoURL"  >
	<portlet:param name="jspPage" value="/jsp/myreports/listReportNo.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="add108ExamNoURL"  name="add108ExamNo">
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="td">
	<%
		if(true){			//isLeader || isPM){
	%>
		<a href="<%=createReportURL%>" ><img src="<%=renderRequest.getContextPath()%>/images/new_a_project.png" >&nbsp;建立新案</a>
	 <%
	 	}else{
	 %>
	 	<img src="<%=renderRequest.getContextPath()%>/images/work_in_process.png">&nbsp;您(<%=userFullName%>)可參與的檢查案：
	 <%
	 	}
	 %>
	 
	 <%if(isSecReporter || isAdmin){ %>
	 	&nbsp;&nbsp;&nbsp;<a href="<%=goListReportURL%>" >案件資料列表</a>
	 <%} %>
	 
	 <%if(isAdmin){ %>
	 		&nbsp;&nbsp;&nbsp;<a href="<%=goListMySecReportNoURL%>" >各組報告編號列表</a>
	 <%}else if(!secBelongTo.equals("")){ %>
	 		&nbsp;&nbsp;&nbsp;<a href="<%=goListMySecReportNoURL%>" >「<%=secBelongTo%>」所屬報告編號列表</a>
	 <%} %>
	 
	 <%if(isUploadManager || isAdmin){ %>
	 	&nbsp;&nbsp;&nbsp;<a href="<%=goListUploadStatisURL%>" >報告上傳作業統計報表查詢</a>
	 <%} %>
	</aui:column>
</aui:layout>
<%if(isAdmin){ %>
	<aui:layout>
		<aui:column cssClass="td">
				&nbsp;&nbsp;&nbsp;<a href="<%=listExamUsersURL%>" >人員權限列表</a>
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=add108ExamNoURL.toString() %>"  value="108年檢查證加入alloc_sync表格"/>
	</aui:button-row>	
<%} %>
<hr/>
<br>
<liferay-ui:search-container emptyResultsMessage="您目前沒有未結的檢查報告案。"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecReport, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecReport.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.ReportProfile" keyProperty="reportId" modelVar="reportProfile">
		<portlet:renderURL var="viewExamURL" >
			<portlet:param name="jspPage" value="/jsp/myreports/viewExam.jsp"/>
    			<portlet:param name="reportId" value= "<%=String.valueOf(reportProfile.reportId)%>"/>
		</portlet:renderURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/report.png"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="報告編號" property="reportNo" href="<%=viewExamURL.toString()%>"/>
		<liferay-ui:search-container-column-text name="案別" property="reportFact" />
		<liferay-ui:search-container-column-text name="案名" property="caseName" />
		<liferay-ui:search-container-column-text name="領隊" property="leaderName" />
		<liferay-ui:search-container-column-text name="助檢" property="staffs" />
		<liferay-ui:search-container-column-text name="建立時間" property="dateCreated" />
		<liferay-ui:search-container-column-text name="建立人" property="createdBy" />
		
		<liferay-ui:search-container-column-jsp path="/jsp/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<br>
<aui:layout>
	<aui:column cssClass="td">已關閉的檢查報告</aui:column>
</aui:layout>

<liferay-ui:search-container emptyResultsMessage="您目前沒有關閉的檢查報告案。" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(collec.vecReportClosed, searchContainer.getStart(), searchContainer.getEnd());
			total = collec.vecReportClosed.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.ReportProfile" keyProperty="reportId" modelVar="reportProfileClosed">
		<portlet:renderURL var="viewExam2URL" >
			<portlet:param name="jspPage" value="/jsp/myreports/viewExam.jsp"/>
    			<portlet:param name="reportId" value= "<%=String.valueOf(reportProfileClosed.reportId)%>"/>
    			<portlet:param name="closed" value= "true"/>
		</portlet:renderURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/close24.png"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="報告編號" property="reportNo" href="<%=viewExam2URL.toString()%>"/>
		<liferay-ui:search-container-column-text name="案別" property="reportFact" />
		<liferay-ui:search-container-column-text name="案名" property="caseName" />
		<liferay-ui:search-container-column-text name="領隊" property="leaderName" />
		<liferay-ui:search-container-column-text name="助檢" property="staffs" />
		<liferay-ui:search-container-column-text name="作廢否" property="dropped" align="center"/>
		<liferay-ui:search-container-column-text name="關閉日期" property="dateClosed" />
		<liferay-ui:search-container-column-text name="關閉人" property="closedBy" />
		
		<liferay-ui:search-container-column-jsp path="/jsp/admin/adminClosed.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<br>
<aui:layout>
	<aui:column cssClass="td">
		<a href="<%=listReportURL%>" ><img src="<%=renderRequest.getContextPath()%>/images/listReport.png" >&nbsp;案件資料列表</a>
	</aui:column>
</aui:layout>
<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.ExamProfile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>


<portlet:defineObjects />


  <%
  			int reportId = Integer.parseInt(renderRequest.getParameter("reportId"));
  			//
            	Collec collec = new Collec();
            if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
            		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
            }
            //
            ReportProfile ri  = new ReportProfile();
    	     	for(ReportProfile _rp: collec.vecReport){
    	     		if(_rp.reportId == reportId){
    	     			ri = _rp;
    	     			break;
    	     		}
   	     	}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/myreports/view.jsp"/>
</portlet:renderURL>		

<portlet:actionURL var="doCloseReportURL"  name="doCloseReport">
	<portlet:param name="reportId"  value="<%=String.valueOf(ri.reportId) %>"/>
	<portlet:param name="reportNo"  value="<%=String.valueOf(ri.reportNo) %>"/>
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/close.png">&nbsp;關閉檢查報告案
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:fieldset >
	<aui:layout>
		<aui:column cssClass="th">
			確定關閉編號為 <%=ri.reportNo %>&nbsp;之檢查報告案？
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消"/>
		<aui:button onClick="<%=doCloseReportURL.toString() %>"  value="確定關閉"/>
	</aui:button-row>				
</aui:fieldset>




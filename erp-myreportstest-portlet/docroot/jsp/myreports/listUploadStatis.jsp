<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

<%
	//可列出的組別
	Vector<String> vecSecPriv = new Vector<String>();	
	if(renderRequest.getPortletSession(true).getAttribute("vecSecPriv") != null){
		vecSecPriv = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecPriv");
	}
	String secNoGened1 = "";		//一般檔案上傳
	if(renderRequest.getAttribute("secNoGened1") != null) secNoGened1 = renderRequest.getAttribute("secNoGened1").toString();
	
	String secNoGened2 = "";		//檢查評述上傳
	if(renderRequest.getAttribute("secNoGened2") != null) secNoGened2 = renderRequest.getAttribute("secNoGened2").toString();
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/myreports/view.jsp"/>
</portlet:actionURL>

<portlet:actionURL var="genUploadStatisReportURL" name="genUploadStatisReport" >
</portlet:actionURL>

<portlet:resourceURL var="downloadUploadStatisReport1URL">
	<portlet:param name="downloadUploadStatisReport1" value="true"/>
	<portlet:param name="secNoGened1"  value="<%=secNoGened1%>"/>
</portlet:resourceURL>

<portlet:resourceURL var="downloadUploadStatisReport2URL">
	<portlet:param name="downloadUploadStatisReport2" value="true"/>
	<portlet:param name="secNoGened2"  value="<%=secNoGened2%>"/>
</portlet:resourceURL>

<%if(!secNoGened1.equals("")){ %>
	<aui:layout>
		<aui:column cssClass="header_red">
	 		<img src="<%=renderRequest.getContextPath()%>/images/excel22.png">&nbsp;<a href="<%=downloadUploadStatisReport1URL.toString()%>" >下載已產出的一般檔案上傳統計表</a>
		</aui:column>
	</aui:layout>
<%}else if(!secNoGened2.equals("")){%>
	<aui:layout>
		<aui:column cssClass="header_red">
	 		<img src="<%=renderRequest.getContextPath()%>/images/excel22.png">&nbsp;<a href="<%=downloadUploadStatisReport2URL.toString()%>" >下載已產出的評述檔案上傳管制表</a>
		</aui:column>
	</aui:layout>
<%} %>	

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/query.png">&nbsp;請指定專案建立起迄日期、組別與報表分類
	</aui:column>
</aui:layout>

<hr/>

<br/>

<aui:fieldset>
<aui:form action="<%=genUploadStatisReportURL.toString()%>"  method="post" >
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="timeStamp" value="<%=ErpUtil.getCurrDateTime() %>"  cssClass="td" />
			<aui:input name="dateStart"  label="起始日期(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>
		
		<aui:column cssClass="td">
			<br>～
		</aui:column>
		
		<aui:column cssClass="td">
			<aui:input name="dateEnd"  label="截止日期(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>

		<aui:column cssClass="td">
			<aui:select name="secNo"  label="組別"  cssClass="td">
				<%for(String secName: vecSecPriv){
						String _secNo = secName;
						if(!_secNo.equals("*")){
							if(!_secNo.equals("檢查制度組")){
								_secNo = String.valueOf(secName.charAt(0));
							}
						}
					%>
					<aui:option value="<%=_secNo%>"  ><%=secName%></aui:option>
				<%} %>
			</aui:select>
		</aui:column>
		<aui:column cssClass="td">
			<aui:select name="excelType"  label="報表分類"  cssClass="td">
					<aui:option value="A"  >一般檔案上傳</aui:option>
					<aui:option value="B"  >評述檔案上傳</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消" />
		</aui:column>
		<aui:column>
			<aui:button type="submit"  value="開始查詢"  title="開始查詢"  cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>
</aui:fieldset>
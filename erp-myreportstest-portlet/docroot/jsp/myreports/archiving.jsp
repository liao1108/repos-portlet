<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.ExamProfile"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />


  <%
            	ReportProfile ri  = new ReportProfile();
            	if(renderRequest.getPortletSession(true).getAttribute("reportProfile") != null){
            		ri = (ReportProfile)renderRequest.getPortletSession(true).getAttribute("reportProfile");
            	}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/jsp/reportprofile/view.jsp"/>
</portlet:renderURL>		

<portlet:actionURL var="doArchivingURL"  name="doArchiving">
	<portlet:param name="reportId"  value="<%=String.valueOf(ri.reportId) %>"/>
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/archiving.png">&nbsp;轉入歸檔區
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:fieldset >
	<aui:layout>
		<aui:column cssClass="th">
			轉入歸檔區後即進行加密保護，無法再還原。確定將編號為 <%=ri.reportNo %>之檢查報告案轉入歸檔區？
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>"  value="取消"/>
		<aui:button onClick="<%=doArchivingURL.toString() %>"  value="確定重啟"/>
	</aui:button-row>				
</aui:fieldset>




<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    			height: 40px
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}	
  </style>
  
  <portlet:defineObjects />
  
 <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	String msgText = "";
	if(renderRequest.getAttribute("msgText") != null){
		msgText = renderRequest.getAttribute("msgText").toString();
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />

<portlet:actionURL var="backToViewURL">
	<portlet:param name="jspPage" value="/html/addmessage/view.jsp" />
</portlet:actionURL>

<table width="100%">
  <tr>
    <td width="2%"><img src='<%=renderRequest.getContextPath()%>/images/ThankYou.png'></td>
    <td width="98%">密碼更新已完成，請重新登入本系統。</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
  	<td class="tb"><a href="/c/portal/logout">登出系統</a></td>
  </tr>
</table>


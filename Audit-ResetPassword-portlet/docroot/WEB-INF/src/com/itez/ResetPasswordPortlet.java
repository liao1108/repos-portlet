package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.PasswordTrackerLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


public class ResetPasswordPortlet extends MVCPortlet {
	@Override
	public void init() throws PortletException {
		super.init();
	}
	
	public void resetPasswd(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			String newPasswd = "", newPasswd2 = "";
			if(actionRequest.getParameter("newPasswd") != null){
				newPasswd = actionRequest.getParameter("newPasswd").trim();
			}
			if(actionRequest.getParameter("newPasswd2") != null){
				newPasswd2 = actionRequest.getParameter("newPasswd2").trim();
			}
			if(newPasswd.equals("") || newPasswd2.equals("")){
				throw new Exception("請完整輸入新密碼。");
			}
			if(!newPasswd.equals(newPasswd2)){
				throw new Exception("新密碼輸入不一致。");
			}
			//
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//User user = UserLocalServiceUtil.getUser(themeDisplay.getUserId());
			User user = themeDisplay.getUser();
			if(PasswordTrackerLocalServiceUtil.isSameAsCurrentPassword(user.getUserId(), newPasswd))
						throw new Exception("新舊密碼不可一樣。");
			//變更Liferay
			try{
				user = UserLocalServiceUtil.updatePassword(user.getUserId(), newPasswd, newPasswd, false);
			}catch(Exception _ex){
				throw new Exception("密碼修改失敗(新舊密碼不可一樣；不可有短底線字元。)");
			}
			//變更角色
			long[] arr = new long[1];
			arr[0] = user.getUserId();
			//加入稽核人員角色
			long companyId = themeDisplay.getCompanyId(); 	// default company
    		Role role = RoleLocalServiceUtil.getRole(companyId, "Auditor");
	        UserLocalServiceUtil.addRoleUsers(role.getRoleId(), arr);
	        
			actionResponse.setRenderParameter("jspPage", "/html/resetpasswordportlet/thanks.jsp");
			//刪除 重設密碼 角色
	        role = RoleLocalServiceUtil.getRole(companyId, "ResetPwd");
	        UserLocalServiceUtil.deleteRoleUser(role.getRoleId(), user.getUserId());
	        
	        UserLocalServiceUtil.updateUser(user);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			//if(errMsg.equals("")){
				errMsg = ex.getMessage();
			//}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
}

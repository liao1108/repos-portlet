package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.tomcat.jdbc.pool.DataSource;


public class TopicSettingPortlet extends GenericPortlet {
	String eCourseRootFoler = "";	//課程根目錄
	ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	String strHTML = "";
	
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	    	//
	    	eCourseRootFoler = getInitParameter("ecourse-root-folder");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
   		Connection conn = null;
   		//
   		strHTML = "";
    	try{
    		if(renderRequest.getAttribute("errMsg") != null){
    			errMsg = renderRequest.getAttribute("errMsg").toString();
    		}
    		//
	    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		long groupId = themeDisplay.getScopeGroupId();
	   		//long userId = themeDisplay.getUserId();
	   		//
			conn = ds.getConnection();
	   		//主題暫存
			Hashtable<String, Vector<Topic>> htCourse = new Hashtable<String, Vector<Topic>>();
	   		//
	   		List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			if(fd.getName().equals(eCourseRootFoler)){
	   				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
	   				for(Folder fd2: list2){			//主題
	   					//單元
						Vector<Topic> vecTopic = new Vector<Topic>();
						//
	   					List<Folder> list3 = DLAppServiceUtil.getFolders(groupId, fd2.getFolderId());
	   					for(Folder fd3: list3){		//單元
							Topic topic = new Topic();
							topic.id = fd3.getFolderId();
							topic.name = fd3.getName();
							//
							String sql = "SELECT * FROM topic_setting WHERE topic_id=?";
							PreparedStatement ps = conn.prepareStatement(sql);
							ps.setLong(1, topic.id);
							ResultSet rs = ps.executeQuery();
							if(rs.next()){
								topic.readLeastMinutes = rs.getInt("read_least_minutes");
								topic.quizMaxMinutes = rs.getInt("quiz_max_minutes");
								topic.thanksWord = rs.getString("thanks_word");
								topic.studyHoursGet = rs.getDouble("study_hours_get");
							}
							rs.close();
							//
							vecTopic.add(topic);
	   					}
	   					//
	   					if(vecTopic.size() > 0){
							//排序 vecTopic
	   						Vector<Topic> vecTopicSort = new Vector<Topic>(); 
	   						Vector<String> vec = new Vector<String>();
	   						for(Topic t: vecTopic){
	   							vec.add(t.name);
	   						}
	   						Collections.sort(vec);
	   						for(String s: vec){
	   							for(Topic t: vecTopic){
	   								if(t.name.equals(s)){
	   									vecTopicSort.add(t);
	   									break;
	   								}
	   							}
	   						}
	   						//
							htCourse.put(fd2.getName(), vecTopic);
						}
	   				}
	   			}
	   		}
	   		//排序主題
	   		Vector<String> vecSubject = new Vector<String>();
 			Enumeration<String> en = htCourse.keys();
 			while(en.hasMoreElements()){
 				vecSubject.add(en.nextElement());
 			}
 			Collections.sort(vecSubject);
			//畫HTML
 			strHTML = "<table width='100%' border='1'>";
 			strHTML += "<tr>";
 			strHTML += "<td align='center' style='background-color:lightblue' width='15%'>" + "主題" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue' width='25%'>" + "單元" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "最短閱讀分鐘數" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "最長檢測分鐘數" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue' width='50%'>" + "感謝詞" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "換算學習時數" + "</td>";
 			strHTML += "<td align='center' style='background-color:lightblue'>" + "更新" + "</td>";
 			strHTML += "</tr>";			
 			for(String sbName: vecSubject){
 				Vector<Topic> vecTopic = htCourse.get(sbName);
 				int topicCount = 0;
 				for(Topic topic: vecTopic){
 					strHTML += "<form method='POST' action='" + renderResponse.createActionURL() + "'>";
 					//
 					strHTML += "<tr>";
 					if(topicCount == 0){
 						strHTML += "<td align='left' rowspan='" + vecTopic.size() + "'>" + sbName + "</td>"; 
 					}
 					strHTML += "<td>" + topic.name + "</td>";
 					
 					strHTML += "<td align='center'>";
 					strHTML += "<input type='HIDDEN' name='topicId' value='" + topic.id + "'>";
 					strHTML += "<input type='TEXT' name='readLeastMinutes' value='"	+ topic.readLeastMinutes + "' size='2'>";
 					strHTML += "</td>";
 					
 					strHTML += "<td align='center'>";
 					strHTML += "<input type='TEXT' name='quizMaxMinutes' value='"	+ topic.quizMaxMinutes + "' size='2'>";
 					strHTML += "</td>";

 					strHTML += "<td align='center'>";
 					strHTML += "<input type='TEXT' name='thanksWord' value='"	+ topic.thanksWord + "' width='100%' size='75'>";
 					strHTML += "</td>";
 					
 					strHTML += "<td align='center'>";
 					strHTML += "<input type='TEXT' name='studyHoursGet' value='"	+ topic.studyHoursGet + "' size='2'>";
 					strHTML += "</td>";

 					strHTML += "<td align='center'>";
 					strHTML += "<input type='SUBMIT' value='更新'>";
 					strHTML += "</td>";

 					strHTML += "</tr>";
 					//
 					strHTML += "</form>";
 					//
 					topicCount++;
 				}
 			}
 			strHTML += "</table>";
        }catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
        }
		renderRequest.setAttribute("strHTML", strHTML);
    	//
   		renderRequest.setAttribute("errMsg", errMsg);
    	//
        include(viewJSP, renderRequest, renderResponse);
    }
    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
		String errMsg = "";
		Connection conn = null;
		try{
	    	long topicId = -1;
	    	if(actionRequest.getParameter("topicId") != null){
	    		topicId = Long.parseLong(actionRequest.getParameter("topicId"));
	    	}
			int readLeastMinutes = 0;
			if(actionRequest.getParameter("readLeastMinutes") != null){
				readLeastMinutes = Integer.parseInt(actionRequest.getParameter("readLeastMinutes"));
	    	}
			int quizMaxMinutes = 0;
			if(actionRequest.getParameter("quizMaxMinutes") != null){
				quizMaxMinutes = Integer.parseInt(actionRequest.getParameter("quizMaxMinutes"));
	    	}
			String thanksWord = "";
			if(actionRequest.getParameter("thanksWord") != null){
				thanksWord = actionRequest.getParameter("thanksWord");
	    	}
			double studyHoursGet = 0;
			if(actionRequest.getParameter("studyHoursGet") != null){
				studyHoursGet = Double.parseDouble(actionRequest.getParameter("studyHoursGet"));
	    	}
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM topic_setting WHERE topic_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, topicId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				sql = "UPDATE topic_setting SET read_least_minutes=?, quiz_max_minutes=?, thanks_word=?, study_hours_get=? " +
							" WHERE topic_id = ?";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, readLeastMinutes);
				ps.setInt(2, quizMaxMinutes);
				ps.setString(3, thanksWord);
				ps.setDouble(4, studyHoursGet);
				ps.setLong(5, topicId);
				ps.execute();
			}else{
				sql = "INSERT INTO topic_setting (topic_id, read_least_minutes, quiz_max_minutes, thanks_word, study_hours_get) VALUES (?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setLong(1, topicId);
				ps.setInt(2, readLeastMinutes);
				ps.setInt(3, quizMaxMinutes);
				ps.setString(4, thanksWord);
				ps.setDouble(5, studyHoursGet);
				ps.execute();
			}
			rs.close();
		}catch(Exception ex){
			ex.printStackTrace();
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
        actionRequest.setAttribute("errMsg", errMsg);
    	//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }

    private void mapUser(ActionRequest actionRequest){
    	Connection conn = null;
    	try{
    		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	long companyId = themeDisplay.getCompanyId();
	    	//long userId = themeDisplay.getUserId();
    		//
	    	Hashtable<String, Long> ht605 = new Hashtable<String, Long>();
	    	Workbook wb = Workbook.getWorkbook(new File("D:/Redo/users_605.xls"));
		    Sheet ws = wb.getSheet(0);
		    for(int i = 0; i < ws.getRows(); i++){
		    	if(ws.getCell(0, i).getContents() != null){
		    		long userId = Long.parseLong(ws.getCell(1, i).getContents());
		    		String screenName = ws.getCell(15,i).getContents();
		    		ht605.put(screenName, userId);
		    	}
		    }
		    wb.close();
	    	//
		    conn = ds.getConnection();
		    //
	    	List<User> listUser = UserLocalServiceUtil.getCompanyUsers(companyId, 0, 999999);
	    	for(User u: listUser){
	    		long userId = u.getUserId();
	    		String screenName = u.getScreenName();
	    		if(ht605.containsKey(screenName)){
	    			String sql = "INSERT INTO user_map (user_605, user_610) VALUES (?,?)";
	    			PreparedStatement ps = conn.prepareCall(sql);
	    			ps.setLong(1, ht605.get(screenName));
	    			ps.setLong(2, userId);
	    			ps.execute();
	    		}
	    	}
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
    }
    
    private void importUser(ActionRequest actionRequest){
    	try{
    		ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	long companyId = themeDisplay.getCompanyId();
	    	long userId = themeDisplay.getUserId();
    		
			Workbook wb = Workbook.getWorkbook(new File("D:/Redo/users_605.xls"));
		    Sheet ws = wb.getSheet(0);
		    for(int i = 0; i < ws.getRows(); i++){
		    	if(ws.getCell(0, i).getContents() != null){
		    		//long userId = Long.parseLong(ws.getCell(1, i).getContents());
		    		String screenName = ws.getCell(15,i).getContents();
		    		String emailAddress = ws.getCell(16, i).getContents();
		    		//String locale = "zh_TW";
		    		String timeZone = "Asia/Shanghai";
		    		String firstName = ws.getCell(24, i).getContents();
		    		String lastName = ws.getCell(26, i).getContents();
		    		boolean male = true;
		    		String jobTitle = ws.getCell(27,i).getContents();
		    		long contactId = Long.parseLong(ws.getCell(6, i).getContents());
		    		String passwdEncrypted = ws.getCell(7, i).getContents();
		    		//
		    		UserLocalServiceUtil.addUser(userId,
		    										companyId,
		    										false,
		    										screenName,
		    										screenName,
		    										false,
		    										screenName,
		    										emailAddress,
		    										0L,
		    										"",
		    										Locale.TAIWAN,
		    										firstName,
		    										"",
		    										lastName,
		    										0,
		    										0,
		    										male,
		    										0,
		    										1,
		    										1970,
		    										jobTitle,
		    										null,
		    										null,
		    										null,
		    										null,
		    										false,
		    										new ServiceContext());
		    	}
		    }
		    wb.close();
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(TopicSettingPortlet.class);

}

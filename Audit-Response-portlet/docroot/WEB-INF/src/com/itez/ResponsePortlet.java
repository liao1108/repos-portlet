package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ResponsePortlet extends MVCPortlet {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
	//Utils utils = new Utils();
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    		requestPath = getInitParameter("requestPath");
	    		//補 time stamp
	    		Connection conn = ds.getConnection();
	    		String sql = "SELECT * FROM message_with_type WHERE time_stamp IS NULL OR time_stamp = '' ";
	    		PreparedStatement ps = conn.prepareStatement(sql);
	    		ResultSet rs = ps.executeQuery();
	    		while(rs.next()){
	    			int msgId = rs.getInt("msg_id");
	    			String userNo = rs.getString("user_no");
	    			String msgType = rs.getString("msg_type");
	    			String ourResponse = rs.getString("our_reponse");
	    			//
	    			String uuid = UUID.randomUUID().toString();
	    			//
	    			sql = "UPDATE message_with_type SET time_stamp=? WHERE msg_id=? " +		//1, 2
			    					" AND user_no = ? " +		//3
			    					" AND msg_type=? " +		//4
			    					" AND our_response=?";		//5
	    			ps = conn.prepareStatement(sql);
	    			ps.setString(1, uuid);
	    			ps.setInt(2, msgId);
	    			ps.setString(3, userNo);
	    			ps.setString(4, msgType);
	    			ps.setString(5, ourResponse);
	    			ps.execute();
	    		}
	    		rs.close();
	    		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			this.alterTables();
			//
			/*
			Vector<Message> vecMessage = new Vector<Message>();
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditor_message WHERE responsed IS NULL OR responsed=0";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				int msgId = rs.getInt("msg_id");
				//
				Message msg = new Message();
				msg.msgId = msgId;
				msg.userNo = rs.getString("user_no");
				msg.userEmail = rs.getString("user_email");
				msg.logTime = rs.getString("log_time");
				msg.logMsg = rs.getString("log_msg");
				//
				int i = 0;
				sql = "SELECT * FROM message_with_type WHERE msg_id=? ORDER BY response_time";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, msgId);
				ResultSet rs2 = ps.executeQuery();
				while(rs2.next()){
					 switch(i) { 
				            case 0:
				        	    msg.msgType1 = rs2.getString("msg_type");
				        	    msg.logMsg1 = rs2.getString("log_msg");
				        	    msg.ourResponse1 = rs2.getString("our_response");
				        	    break;
				            case 1: 
				        	    msg.msgType2 = rs2.getString("msg_type");
				        	    msg.logMsg2 = rs2.getString("log_msg");
				        	    msg.ourResponse2 = rs2.getString("our_response"); 
				        	    break; 
				            case 2: 
				        	    msg.msgType3 = rs2.getString("msg_type");
				        	    msg.logMsg3 = rs2.getString("log_msg");
				        	    msg.ourResponse3 = rs2.getString("our_response"); 
				        	    break; 
				            case 3: 
				        	    msg.msgType4 = rs2.getString("msg_type");
				        	    msg.logMsg4 = rs2.getString("log_msg");
				        	    msg.ourResponse4 = rs2.getString("our_response"); 
				        	    break; 
				            case 4: 
				        	    msg.msgType5 = rs2.getString("msg_type");
				        	    msg.logMsg5 = rs2.getString("log_msg");
				        	    msg.ourResponse5 = rs2.getString("our_response"); 
				        	    break; 
				            case 5:
				        	    msg.msgType6 = rs2.getString("msg_type");
				        	    msg.logMsg6 = rs2.getString("log_msg");
				        	    msg.ourResponse6 = rs2.getString("our_response"); 
				        	    break; 
				            default: 
				        	    msg.msgType1 = rs2.getString("msg_type");
				        	    msg.logMsg1 = rs2.getString("log_msg");
				        	    msg.ourResponse1 = rs2.getString("our_response");
				        }
					i++;
				}
				//
				msg.msgTyped = i;
				//
				vecMessage.add(msg);
			}
			renderRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			*/
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	public void doQuery(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			String responsedType = "未回覆";
			if(actionRequest.getParameter("responsedType") != null){
				responsedType = actionRequest.getParameter("responsedType");
			}
			actionRequest.getPortletSession(true).setAttribute("responsedType", responsedType);
			//
			Vector<Message> vecMessage = new Vector<Message>();
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditor_message WHERE (responsed_status IS NULL OR responsed_status = 0) ORDER BY log_time";
			//if(responsedType.equals("已處理未回覆")){
			//	sql = "SELECT * FROM auditor_message WHERE (responsed IS NULL OR responsed = 0) AND auditor_message.msg_id IN (SELECT msg_id FROM message_with_type)";
			if(responsedType.equals("已回覆未完全發布")){
				sql = "SELECT * FROM auditor_message WHERE responsed_status >= 1 AND (published_status IS NULL OR published_status < 2) ORDER BY log_time";
			}else if(responsedType.equals("已完全發布")){
				sql = "SELECT * FROM auditor_message WHERE published_status = 2 ORDER BY log_time DESC";
			}
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				int msgId = rs.getInt("msg_id");
				//
				Message msg = new Message();
				msg.msgId = msgId;
				msg.userNo = rs.getString("user_no");
				msg.userEmail = rs.getString("user_email");
				msg.logTime = rs.getString("log_time");
				msg.logMsg = rs.getString("log_msg");
				msg.logMsgDisplay = msg.logMsg;
				if(msg.logMsgDisplay.length() > 24){
					msg.logMsgDisplay = msg.logMsgDisplay.substring(0, 24) + " ....";
				}
				if(rs.getString("responsed_time") != null){
					msg.responsedTime = rs.getString("responsed_time");
					//msg.responsedTimeDisplay = msg.responsedTime.split(" ")[0];
				}
				//反查金融機構名
				sql = "SELECT bank_name FROM auditors WHERE user_no=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, msg.userNo);
				ResultSet _rs = ps.executeQuery();
				if(_rs.next()){
					if(_rs.getString(1) != null) msg.bankName = _rs.getString(1);
				}else{
					sql = "SELECT fin_name FROM fin_staff WHERE login_id = ?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, msg.userNo);
					_rs = ps.executeQuery();
					if(_rs.next()){
						msg.bankName = _rs.getString(1);
					}
				}
				_rs.close();
				
				//準備收集留言標題
				String logTitleCollect = "";
				//
				int i = 0;
				sql = "SELECT * FROM message_with_type WHERE msg_id=? ORDER BY response_time";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, msgId);
				ResultSet rs2 = ps.executeQuery();
				while(rs2.next()){
					if(rs2.getString("log_title") != null && !rs2.getString("log_title").equals("")){
						if(!logTitleCollect.equals("")) logTitleCollect += "<br>";
						//
						logTitleCollect += rs2.getString("log_title").trim();
					}
					//
					switch(i) { 
				            case 0:
				        	    msg.msgType1 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle1 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg1 = rs2.getString("log_msg");
				        	    msg.ourResponse1 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo1 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail1 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp1 = rs2.getString("time_stamp");
				        	    }
				        	    break;
				            case 1: 
				        	    msg.msgType2 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle2 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg2 = rs2.getString("log_msg");
				        	    msg.ourResponse2 = rs2.getString("our_response"); 
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo2 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail2 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp2 = rs2.getString("time_stamp");
				        	    }
				        	    break; 
				            case 2: 
				        	    msg.msgType3 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle3 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg3 = rs2.getString("log_msg");
				        	    msg.ourResponse3 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo3 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail3 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp3 = rs2.getString("time_stamp");
				        	    }
				        	    break; 
				            case 3: 
				        	    msg.msgType4 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle4 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg4 = rs2.getString("log_msg");
				        	    msg.ourResponse4 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo4 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail4 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp4 = rs2.getString("time_stamp");
				        	    }
				        	    break; 
				            case 4: 
				        	    msg.msgType5 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle5 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg5 = rs2.getString("log_msg");
				        	    msg.ourResponse5 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo5 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail5 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp5 = rs2.getString("time_stamp");
				        	    }
				        	    break; 
				            case 5:
				        	    msg.msgType6 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle6 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg6 = rs2.getString("log_msg");
				        	    msg.ourResponse6 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo6 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail6 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp6 = rs2.getString("time_stamp");
				        	    }
				        	    break; 
				            default: 
				        	    msg.msgType1 = rs2.getString("msg_type");
				        	    if(rs2.getString("log_title") != null){
				        		    msg.logTitle1 = rs2.getString("log_title");
				        	    }
				        	    msg.logMsg1 = rs2.getString("log_msg");
				        	    msg.ourResponse1 = rs2.getString("our_response");
				        	    if(rs2.getString("msg_no") != null){
				        		    msg.msgNo1 = rs2.getString("msg_no");
				        	    }
				        	    msg.withinMail1 = rs2.getInt("within_mail");
				        	    if(rs2.getString("time_stamp") != null){
				        		    msg.timeStamp1 = rs2.getString("time_stamp");
				        	    }
				        }
					i++;
				}
				//
				if(!logTitleCollect.equals("")){
					msg.logMsgDisplay = logTitleCollect;
				}
				//
				msg.msgTyped = i;
				if(i > 0){
					if(!msg.msgNo1.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo1; 
					}
					if(!msg.msgNo2.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo2; 
					}
					if(!msg.msgNo3.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo3; 
					}
					if(!msg.msgNo4.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo4; 
					}
					if(!msg.msgNo5.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo5; 
					}
					if(!msg.msgNo6.equals("")){
						if(!msg.msgNos.equals("")) msg.msgNos += ",";
						msg.msgNos += msg.msgNo6; 
					}
				}
				vecMessage.add(msg);
				//
				/*
				if(responsedType.equals("未回覆")){
					if(i == 0){
						vecMessage.add(msg);
					}
				}else if(responsedType.equals("已回覆未發布")){
					if(i > 0){
						vecMessage.add(msg);
					}
				}else{
					vecMessage.add(msg);
				}
				*/
			}
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public void doProcess(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			String userNo = actionRequest.getParameter("userNo");
			User userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
			//
			int orderObject = 0; 
			Message msg = new Message();
			@SuppressWarnings("unchecked")
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					orderObject = vecMessage.indexOf(m);
					msg = m;
					break;
				}
			}
			//
			msg.msgType1 = actionRequest.getParameter("msgType1");
			msg.logTitle1 = actionRequest.getParameter("logTitle1");
			msg.logMsg1 = actionRequest.getParameter("logMsg1");
			msg.ourResponse1 = actionRequest.getParameter("ourResponse1");
			if(actionRequest.getParameter("withinMail1") != null){
				msg.withinMail1 = Integer.parseInt(actionRequest.getParameter("withinMail1"));
			}
			msg.timeStamp1 = actionRequest.getParameter("timeStamp1");
			
			msg.msgType2 = actionRequest.getParameter("msgType2");
			msg.logTitle2 = actionRequest.getParameter("logTitle2");
			msg.logMsg2 = actionRequest.getParameter("logMsg2");
			msg.ourResponse2 = actionRequest.getParameter("ourResponse2");
			if(actionRequest.getParameter("withinMail2") != null){
				msg.withinMail2 = Integer.parseInt(actionRequest.getParameter("withinMail2"));
			}
			msg.timeStamp2 = actionRequest.getParameter("timeStamp2");
			
			msg.msgType3 = actionRequest.getParameter("msgType3");
			msg.logTitle3 = actionRequest.getParameter("logTitle3");
			msg.logMsg3 = actionRequest.getParameter("logMsg3");
			msg.ourResponse3 = actionRequest.getParameter("ourResponse3");
			if(actionRequest.getParameter("withinMail3") != null){
				msg.withinMail3 = Integer.parseInt(actionRequest.getParameter("withinMail3"));
			}
			msg.timeStamp3 = actionRequest.getParameter("timeStamp3");
			
			msg.msgType4 = actionRequest.getParameter("msgType4");
			msg.logTitle4 = actionRequest.getParameter("logTitle4");
			msg.logMsg4 = actionRequest.getParameter("logMsg4");
			msg.ourResponse4 = actionRequest.getParameter("ourResponse4");
			if(actionRequest.getParameter("withinMail4") != null){
				msg.withinMail4 = Integer.parseInt(actionRequest.getParameter("withinMail4"));
			}
			msg.timeStamp4 = actionRequest.getParameter("timeStamp4");
			
			msg.msgType5 = actionRequest.getParameter("msgType5");
			msg.logTitle5 = actionRequest.getParameter("logTitle5");
			msg.logMsg5 = actionRequest.getParameter("logMsg5");
			msg.ourResponse5 = actionRequest.getParameter("ourResponse5");
			if(actionRequest.getParameter("withinMail5") != null){
				msg.withinMail5 = Integer.parseInt(actionRequest.getParameter("withinMail5"));
			}
			msg.timeStamp5 = actionRequest.getParameter("timeStamp5");
			
			msg.msgType6 = actionRequest.getParameter("msgType6");
			msg.logTitle6 = actionRequest.getParameter("logTitle6");
			msg.logMsg6 = actionRequest.getParameter("logMsg6");
			msg.ourResponse6 = actionRequest.getParameter("ourResponse6");
			if(actionRequest.getParameter("withinMail6") != null){
				msg.withinMail6 = Integer.parseInt(actionRequest.getParameter("withinMail6"));
			}
			msg.timeStamp6 = actionRequest.getParameter("timeStamp6");
			
			 //邏輯驗證與存檔
			if((msg.logTitle1.trim().equals("") && !msg.logMsg1.trim().equals("")) || (!msg.logTitle1.trim().equals("") && msg.logMsg1.trim().equals(""))){
		    		throw new Exception("留言分類一請輸入留言標題與摘要。");
			}else if((msg.logTitle2.trim().equals("") && !msg.logMsg2.trim().equals("")) || (!msg.logTitle2.trim().equals("") && msg.logMsg2.trim().equals(""))){
				throw new Exception("留言分類二請輸入留言標題與摘要。");
			}else if((msg.logTitle3.trim().equals("") && !msg.logMsg3.trim().equals("")) || (!msg.logTitle3.trim().equals("") && msg.logMsg3.trim().equals(""))){
				throw new Exception("留言分類三請輸入留言標題與摘要。");
			}else if((msg.logTitle4.trim().equals("") && !msg.logMsg4.trim().equals("")) || (!msg.logTitle4.trim().equals("") && msg.logMsg4.trim().equals(""))){
				throw new Exception("留言分類四請輸入留言標題與摘要。");
			}else if((msg.logTitle5.trim().equals("") && !msg.logMsg5.trim().equals("")) || (!msg.logTitle5.trim().equals("") && msg.logMsg5.trim().equals(""))){
				throw new Exception("留言分類五請輸入留言標題與摘要。");
			}else if((msg.logTitle6.trim().equals("") && !msg.logMsg6.trim().equals("")) || (!msg.logTitle6.trim().equals("") && msg.logMsg6.trim().equals(""))){
				throw new Exception("留言分類六請輸入留言標題與摘要。");
			}
			
		    	if((msg.logMsg1.trim().equals("") && !msg.ourResponse1.trim().equals("")) || (!msg.logMsg1.trim().equals("") && msg.ourResponse1.trim().equals(""))){
		    		throw new Exception("留言分類一請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg2.trim().equals("") && !msg.ourResponse2.trim().equals("")) || (!msg.logMsg2.trim().equals("") && msg.ourResponse2.trim().equals(""))){
				throw new Exception("留言分類二請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg3.trim().equals("") && !msg.ourResponse3.trim().equals("")) || (!msg.logMsg3.trim().equals("") && msg.ourResponse3.trim().equals(""))){
				throw new Exception("留言分類三請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg4.trim().equals("") && !msg.ourResponse4.trim().equals("")) || (!msg.logMsg4.trim().equals("") && msg.ourResponse4.trim().equals(""))){
				throw new Exception("留言分類四請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg5.trim().equals("") && !msg.ourResponse5.trim().equals("")) || (!msg.logMsg5.trim().equals("") && msg.ourResponse5.trim().equals(""))){
				throw new Exception("留言分類五請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg6.trim().equals("") && !msg.ourResponse6.trim().equals("")) || (!msg.logMsg6.trim().equals("") && msg.ourResponse6.trim().equals(""))){
				throw new Exception("留言分類六請輸入留言內容以及本局答覆。");
			}
		    	//
		    	conn = ds.getConnection();
		    	//記取之前之回應 TimeStamp
			Vector<String> vecExist = new Vector<String>();
		    	//String sql = "DELETE FROM message_with_type WHERE msg_id = ?";
			String sql = "SELECT time_stamp FROM message_with_type WHERE msg_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				vecExist.add(rs.getString(1));
			}
			rs.close();
			//
			//Vector<String> vecMsgNo = new Vector<String>();
			//int logYear = Integer.parseInt(msg.logTime.trim().split("-")[0]);
			for(int i = 0; i < 6; i++){
				String _msgType = "", _logTitle="", _logMsg = "", _ourResponse= "", _timeStamp = "";
				int _withinMail = 0;
				if(i== 0){
					_msgType = msg.msgType1;
					_logTitle = msg.logTitle1;
					_logMsg = msg.logMsg1.trim();
					_ourResponse = msg.ourResponse1.trim();
					_withinMail = msg.withinMail1;
					_timeStamp = msg.timeStamp1;
				}else if(i == 1){
					_msgType = msg.msgType2;
					_logTitle = msg.logTitle2;
					_logMsg = msg.logMsg2.trim();
					_ourResponse = msg.ourResponse2.trim();
					_withinMail = msg.withinMail2;
					_timeStamp = msg.timeStamp2;
				}else if(i == 2){
					_msgType = msg.msgType3;
					_logTitle = msg.logTitle3;
					_logMsg = msg.logMsg3.trim();
					_ourResponse = msg.ourResponse3.trim();
					_withinMail = msg.withinMail3;
					_timeStamp = msg.timeStamp3;
				}else if(i == 3){
					_msgType = msg.msgType4;
					_logTitle = msg.logTitle4;
					_logMsg = msg.logMsg4.trim();
					_ourResponse = msg.ourResponse4.trim();
					_withinMail = msg.withinMail4;
					_timeStamp = msg.timeStamp4;
				}else if(i == 4){
					_msgType = msg.msgType5;
					_logTitle = msg.logTitle5;
					_logMsg = msg.logMsg5.trim();
					_ourResponse = msg.ourResponse5.trim();
					_withinMail = msg.withinMail5;
					_timeStamp = msg.timeStamp5;
				}else if(i == 5){
					_msgType = msg.msgType6;
					_logTitle = msg.logTitle6;
					_logMsg = msg.logMsg6.trim();
					_ourResponse = msg.ourResponse6.trim();
					_withinMail = msg.withinMail6;
					_timeStamp = msg.timeStamp6;
				}
				//
				if(!_msgType.equals("") && !_logTitle.equals("") && !_logMsg.equals("") && !_ourResponse.equals("")){
					//int msgIndex = 1000;
					//sql = "SELECT next_no FROM message_no_gen WHERE year=? AND msg_type=? FOR UPDATE";
					//ps = conn.prepareStatement(sql);
					//ps.setInt(1, logYear);
					//ps.setString(1, _msgType);
					//ResultSet rs = ps.executeQuery();
					//if(rs.next()){
					//	if(rs.getString(1) != null && !rs.getString(1).equals("")){
					//		msgIndex = Integer.parseInt(rs.getString(1).substring(4)) + 1;
					//		if(msgIndex < 1000){
					//			msgIndex += 1000;
					//		}
					//	}
					//}
					//rs.close();
					//
					//String dateTimeString = String.valueOf(System.currentTimeMillis());
					String DATE_FORMAT = "yyyyMMdd HHmmssSSS" ;
					final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
					String dateTimeString =  sdf.format(new Date());
					//
					//String msgNo = String.valueOf(_msgType.charAt(0)) + String.format("%03d", logYear-1911) + String.format("%05d", msgIndex);
					if(_timeStamp.equals("")){
						String uuid = UUID.randomUUID().toString();
						sql = "INSERT INTO message_with_type (msg_id, user_no, email, log_time, log_title, log_msg, msg_type, our_response, response_time, within_mail, responsed, time_stamp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, msgId);
						ps.setString(2, userNo);
						ps.setString(3, userAuditor.getEmailAddress());
						ps.setString(4, msg.logTime);
						ps.setString(5, _logTitle);
						ps.setString(6, _logMsg);
						ps.setString(7, _msgType);
						ps.setString(8, _ourResponse);
						ps.setString(9, dateTimeString);
						ps.setInt(10, _withinMail);
						ps.setInt(11, 1);
						ps.setString(12, uuid);
						ps.execute();
					}else{
						if(vecExist.contains(_timeStamp)){
							vecExist.remove(_timeStamp);
						}else{
							throw new Exception("目前檢視的資料已被其他人修改，請重新載入。");
						}
						//
						sql = "UPDATE message_with_type SET log_title=?, " +		//1	
													" log_msg=?, " +		//2
													" msg_type=?, " +		//3
													" our_response=?, " +	//4
													" within_mail=?, " +		//5
													" responsed=? " +		//6
									" WHERE msg_id=? AND time_stamp=?";		//7, 8
						ps = conn.prepareStatement(sql);
						ps.setString(1, _logTitle);
						ps.setString(2, _logMsg);
						ps.setString(3, _msgType);
						ps.setString(4, _ourResponse);
						ps.setInt(5, _withinMail);
						ps.setInt(6, 1);
						ps.setInt(7, msgId);
						ps.setString(8, _timeStamp);
						ps.execute();
					}
					//
					//vecMsgNo.add(msgNo);
					//
					/* 回覆時再更新
					if(msgIndex > 1){
						sql = "UPDATE message_no_gen SET next_no = ? WHERE year=? AND msg_type=?";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, msgIndex + 1);
						ps.setInt(2, logYear);
						ps.setString(3, _msgType);
						ps.execute();
					}else{
						sql = "INSERT INTO message_no_gen (year, msg_type, next_no) VALUES (?,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, logYear);
						ps.setString(2, _msgType);
						ps.setInt(3, msgIndex + 1);
						ps.execute();
					}
					*/
				}
			 }
			//移除
			if(vecExist.size() > 0){
				for(String _ts: vecExist){
					sql = "DELETE FROM message_with_type WHERE time_stamp = ?";
					ps = conn.prepareStatement(sql);
					ps.setString(1, _ts);
					ps.execute();
				}
			}
			//是否回覆
			String responseAction = actionRequest.getParameter("doResponse");
			if(!responseAction.equalsIgnoreCase("0")){
				this.doResponse(msgId, userNo, themeDisplay, conn, responseAction, msg);
			}
			//
			vecMessage.remove(orderObject);
			vecMessage.add(orderObject, msg);
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		//actionResponse.setRenderParameter("jspPage", "/html/response/process.jsp");
	}

	public void doRemove(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
		    	//
		    	conn = ds.getConnection();
		    	//刪除之前之回應
			String sql = "DELETE FROM message_with_type WHERE msg_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.execute();
			//
			sql = "DELETE FROM auditor_message WHERE msg_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.execute();
			//
			@SuppressWarnings("unchecked")
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					vecMessage.remove(m);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
			conn.close();
			//
			actionResponse.setRenderParameter("jspPage", "/html/response/view.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		
	}

	public void reMail2User(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
		    	//
		    	conn = ds.getConnection();
		    	//
			String sql = "SELECT * FROM auditor_message WHERE msg_id = ? ";
			//"(user_no, user_email, log_time, log_msg) "
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				User user = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), rs.getString("user_no"));
				String[] dd = rs.getString("log_time").split(" ")[0].split("-");
			
				String strMail = user.getScreenName() + " 您好，\n\n";
				strMail += "您於 " + dd[0] + " 年 " + dd[1] + " 月 " + dd[2] + " 日已成功留言至本局網站留言板，訊息如下：" + "\n\n";
				strMail += rs.getString("log_msg") + "\n\n";
				strMail += "本局將於處理完成後，以e-mail通知您，再請您上網瀏覽相關資訊，謝謝您！" + "\n\n";
				strMail += "金管會檢查局";
			
				DefaultHttpClient httpClient = new DefaultHttpClient();
				URIBuilder builder = new URIBuilder();
				builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
							.setParameter("type", "SendMail")
							.setParameter("mailNo", user.getEmailAddress())
							.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
				URI uri = builder.build();
				HttpPost httpPost = new HttpPost(uri);
				HttpResponse response = httpClient.execute(httpPost);
				BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String line = br.readLine();
				//
				if(response.getEntity() != null ) {
					httpPost.abort();
				}
			}
			rs.close();
			//
			actionResponse.setRenderParameter("jspPage", "/html/response/process.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		
	}

	public void reMail2Feb(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
		    	//
		    	conn = ds.getConnection();
		    	//
			String sql = "SELECT * FROM auditor_message WHERE msg_id = ? ";
			//"(user_no, user_email, log_time, log_msg) "
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				String strAccounts = "";
				Role roleFeb = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), "FEB_S");
				if(roleFeb != null){
					List<User> listUser = UserLocalServiceUtil.getRoleUsers(roleFeb.getRoleId());
					for(User u: listUser){
						if(!strAccounts.equals("")) strAccounts += ";";
						strAccounts += u.getEmailAddress();
					}
				}
				//
				User user = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), rs.getString("user_no"));
				String[] dd = rs.getString("log_time").split(" ")[0].split("-");
				//
				String strMail = " 您好，\n\n";
				strMail += "代名 " + user.getScreenName() + " 之稽核人員已留言，內容為：\n\n";
				strMail += rs.getString("log_msg");
				strMail += "\n\n";
				strMail += "稽核人員留言板系統";
				//
				DefaultHttpClient httpClient = new DefaultHttpClient();
				URIBuilder builder = new URIBuilder();
				//
				String[] ss = strAccounts.split(";");
				for(int i=0; i < ss.length; i++){
					builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
									.setParameter("type", "SendMail")
									.setParameter("mailNo", ss[i])
									.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
					URI uri = builder.build();
					HttpPost httpPost = new HttpPost(uri);
					HttpResponse response = httpClient.execute(httpPost);
					BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
					String line = br.readLine();
					//
					if(response.getEntity() != null ) {
						httpPost.abort();
					}
				}
			}
			rs.close();
			//
			actionResponse.setRenderParameter("jspPage", "/html/response/process.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		
	}

	/*
	public void doResponse(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			String userNo = actionRequest.getParameter("userNo");
			User userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
			//
			int orderObject = 0; 
			Message msg = new Message();
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					orderObject = vecMessage.indexOf(m);
					msg = m;
					break;
				}
			}
			//
			msg.msgType1 = actionRequest.getParameter("msgType1");
			msg.logMsg1 = actionRequest.getParameter("logMsg1");
			msg.ourResponse1 = actionRequest.getParameter("ourResponse1");
			msg.msgType2 = actionRequest.getParameter("msgType2");
			msg.logMsg2 = actionRequest.getParameter("logMsg2");
			msg.ourResponse2 = actionRequest.getParameter("ourResponse2");
			msg.msgType3 = actionRequest.getParameter("msgType3");
			msg.logMsg3 = actionRequest.getParameter("logMsg3");
			msg.ourResponse3 = actionRequest.getParameter("ourResponse3");
			msg.msgType4 = actionRequest.getParameter("msgType4");
			msg.logMsg4 = actionRequest.getParameter("logMsg4");
			msg.ourResponse4 = actionRequest.getParameter("ourResponse4");
			msg.msgType5 = actionRequest.getParameter("msgType5");
			msg.logMsg5 = actionRequest.getParameter("logMsg5");
			msg.ourResponse5 = actionRequest.getParameter("ourResponse5");
			msg.msgType6 = actionRequest.getParameter("msgType6");
			msg.logMsg6 = actionRequest.getParameter("logMsg6");
			msg.ourResponse6 = actionRequest.getParameter("ourResponse6");
			 //邏輯驗證與存檔
		    	if((msg.logMsg1.trim().equals("") && !msg.ourResponse1.trim().equals("")) || (!msg.logMsg1.trim().equals("") && msg.ourResponse1.trim().equals(""))){
		    		throw new Exception("留言分類一請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg2.trim().equals("") && !msg.ourResponse2.trim().equals("")) || (!msg.logMsg2.trim().equals("") && msg.ourResponse2.trim().equals(""))){
				throw new Exception("留言分類二請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg3.trim().equals("") && !msg.ourResponse3.trim().equals("")) || (!msg.logMsg3.trim().equals("") && msg.ourResponse3.trim().equals(""))){
				throw new Exception("留言分類三請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg4.trim().equals("") && !msg.ourResponse4.trim().equals("")) || (!msg.logMsg4.trim().equals("") && msg.ourResponse4.trim().equals(""))){
				throw new Exception("留言分類四請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg5.trim().equals("") && !msg.ourResponse5.trim().equals("")) || (!msg.logMsg5.trim().equals("") && msg.ourResponse5.trim().equals(""))){
				throw new Exception("留言分類五請輸入留言內容以及本局答覆。");
			}else if((msg.logMsg6.trim().equals("") && !msg.ourResponse6.trim().equals("")) || (!msg.logMsg6.trim().equals("") && msg.ourResponse6.trim().equals(""))){
				throw new Exception("留言分類六請輸入留言內容以及本局答覆。");
			}
		    	//
		    	conn = ds.getConnection();
		    	//刪除之前之回應
			String sql = "DELETE FROM message_with_type WHERE msg_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.execute();
			//
			Vector<String> vecMsgNo = new Vector<String>();
			int logYear = Integer.parseInt(msg.logTime.trim().split("-")[0]);
			for(int i = 0; i < 6; i++){
				String _msgType = "", _logMsg = "", _ourResponse= "";
				if(i== 0){
					_msgType = msg.msgType1;
					_logMsg = msg.logMsg1.trim();
					_ourResponse = msg.ourResponse1.trim();
				}else if(i == 1){
					_msgType = msg.msgType2;
					_logMsg = msg.logMsg2.trim();
					_ourResponse = msg.ourResponse2.trim();
				}else if(i == 2){
					_msgType = msg.msgType3;
					_logMsg = msg.logMsg3.trim();
					_ourResponse = msg.ourResponse3.trim();
				}else if(i == 3){
					_msgType = msg.msgType4;
					_logMsg = msg.logMsg4.trim();
					_ourResponse = msg.ourResponse4.trim();
				}else if(i == 4){
					_msgType = msg.msgType5;
					_logMsg = msg.logMsg5.trim();
					_ourResponse = msg.ourResponse5.trim();
				}else if(i == 5){
					_msgType = msg.msgType6;
					_logMsg = msg.logMsg6.trim();
					_ourResponse = msg.ourResponse6.trim();
				}
				//
				if(!_msgType.equals("") && !_logMsg.equals("") && !_ourResponse.equals("")){
					int msgIndex = 1;
					sql = "SELECT next_no FROM message_no_gen WHERE year=? AND msg_type=? FOR UPDATE";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, logYear);
					ps.setString(2, _msgType);
					ResultSet rs = ps.executeQuery();
					if(rs.next()){
						msgIndex = rs.getInt(1);
					}
					rs.close();
					//
					String dateTimeString = String.valueOf(System.currentTimeMillis());
					//String DATE_FORMAT = "yyyyMMdd HHmmssSSS" ;
					//final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					//sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
					//String dateTimeString =  sdf.format(new Date());
					//
					String msgNo = String.valueOf(_msgType.charAt(0)) + String.format("%03d", logYear-1911) + String.format("%05d", msgIndex);
					sql = "INSERT INTO message_with_type (msg_no, msg_id, user_no, email, log_time, log_msg, msg_type, our_response, response_time) VALUES (?,?,?,?,?,?,?,?,?)";
					ps = conn.prepareStatement(sql);
					ps.setString(1, msgNo);
					ps.setInt(2, msgId);
					ps.setString(3, userNo);
					ps.setString(4, userAuditor.getEmailAddress());
					ps.setString(5, msg.logTime);
					ps.setString(6, _logMsg);
					ps.setString(7, _msgType);
					ps.setString(8, _ourResponse);
					ps.setString(9, dateTimeString);
					ps.execute();
					//
					vecMsgNo.add(msgNo);
					//
					if(msgIndex > 1){
						sql = "UPDATE message_no_gen SET next_no = ? WHERE year=? AND msg_type=?";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, msgIndex + 1);
						ps.setInt(2, logYear);
						ps.setString(3, _msgType);
						ps.execute();
					}else{
						sql = "INSERT INTO message_no_gen (year, msg_type, next_no) VALUES (?,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setInt(1, logYear);
						ps.setString(2, _msgType);
						ps.setInt(3, msgIndex + 1);
						ps.execute();
					}
				}
			 }
			//
			vecMessage.remove(orderObject);
			vecMessage.add(orderObject, msg);
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = utils.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	*/
	
	public void doResponse(int msgId, String userNo, ThemeDisplay themeDisplay, Connection conn, String responseType, Message msg) throws Exception {
		User userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
		//
		Calendar cal = Calendar.getInstance();
		int respYear = cal.get(Calendar.YEAR);
		if(msg.responsedTime.trim().length() > 4){
			try{
				respYear = Integer.parseInt(msg.responsedTime.trim().substring(0, 4));
			}catch(Exception _ex){
			}
		}
		//
		int countNormal = 0;			//正常案件
		int countAbnormal = 0;			//特殊案件(暫時回覆)
		Vector<TypedMessage> vecTMsg = new Vector<TypedMessage>();
		
		String sql = "SELECT * FROM message_with_type WHERE msg_id = ?";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, msgId);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			TypedMessage tm = new TypedMessage();
			tm.msgNo = "";
			if(rs.getString("msg_no") != null) tm.msgNo = rs.getString("msg_no");
			tm.msgType = rs.getString("msg_type");
			if(rs.getString("log_title") != null){
				tm.logTitle = rs.getString("log_title");
			}
			tm.logMsg = rs.getString("log_msg");
			tm.withinMail = rs.getInt("within_mail");
			tm.ourResponse = rs.getString("our_response");
			tm.timeStamp = rs.getString("time_stamp");
			//正式取號
			if(tm.msgNo.equals("")){
				int msgIndex = 1;
				sql = "SELECT next_no FROM message_no_gen WHERE year=? AND msg_type=? FOR UPDATE";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, respYear);
				ps.setString(2, tm.msgType);
				ResultSet _rs = ps.executeQuery();
				if(_rs.next()){
					msgIndex = _rs.getInt(1);
				}
				_rs.close();
				//
				String DATE_FORMAT = "yyyyMMdd HHmmssSSS" ;
				final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
				//
				tm.msgNo = String.valueOf(tm.msgType.charAt(0)) + String.format("%03d", respYear-1911) + String.format("%05d", msgIndex);
				//
				sql = "UPDATE message_with_type SET msg_no=? WHERE msg_id=? AND time_stamp=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, tm.msgNo);
				ps.setInt(2, msgId);
				ps.setString(3, tm.timeStamp);
				ps.execute();
				//
				if(msgIndex > 1){
					sql = "UPDATE message_no_gen SET next_no = ? WHERE year=? AND msg_type=?";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, msgIndex + 1);
					ps.setInt(2, respYear);
					ps.setString(3, tm.msgType);
					ps.execute();
				}else{
					sql = "INSERT INTO message_no_gen (year, msg_type, next_no) VALUES (?,?,?)";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, respYear);
					ps.setString(2, tm.msgType);
					ps.setInt(3, msgIndex + 1);
					ps.execute();
				}
			}
			//
			vecTMsg.add(tm);
			//
			if(tm.withinMail == 1){
				countAbnormal++;
			}else{
				countNormal++;
			}
		}
		rs.close();
		//
		if(vecTMsg.size() == 0){
			throw new Exception("本留言尚未處理完成，無法進行回覆作業。");
		}
		//
		String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss" ;
		final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		String dateTimeString =  sdf.format(new Date());
		if(countAbnormal > 0){
			if(responseType.equals("2")){	//直接歸檔
				sql = "UPDATE auditor_message SET responsed_status = 2, published_status = 2, responsed_time=? WHERE msg_id=?";
			}else{
				if(countNormal > 0){
					sql = "UPDATE auditor_message SET responsed_status = 1, published_status = 1, responsed_time=? WHERE msg_id=?";
				}else{
					sql = "UPDATE auditor_message SET responsed_status = 1, responsed_time=? WHERE msg_id=?";
				}
			}
		}else{
			sql = "UPDATE auditor_message SET responsed_status = 2, published_status = 2, responsed_time=? WHERE msg_id=?";
		}
		ps = conn.prepareStatement(sql);
		ps.setString(1, dateTimeString);
		ps.setInt(2, msgId);
		ps.execute();
		//編寫郵件通知
		String strMail = "";
		if(vecTMsg.size() == 1){
			TypedMessage tm = vecTMsg.get(0);
			//
			strMail += userNo + " 您好，\n\n";
			if(tm.withinMail == 1){		//特殊案件
				strMail += "本局已回覆您的留言，摘錄如下：\n\n";
				strMail += "留言編號 — " + tm.msgNo + "\n\n";
				if(!tm.logTitle.equals("")){
					strMail += "留言內容摘要 — " + tm.logTitle + "\n\n";
				}else{
					strMail += "留言內容摘要 — " + tm.logMsg + "\n\n";
				}
				strMail += "本局回覆內容 — " + tm.ourResponse + "\n";
			}else{
				strMail += "本局已回覆您的留言，歡迎上網瀏覽：\n\n";
				strMail += "留言編號 — " + tm.msgNo + "\n\n";
				if(!tm.logTitle.equals("")){
					strMail += "留言內容摘要 — " + tm.logTitle + "\n\n";
				}else{
					strMail += "留言內容摘要 — " + tm.logMsg + "\n\n";
				}
			}
			strMail += "\n";
			//
			strMail += "金管會檢查局";
		}else{
			String strTypes = "";
			String strContent = "";
			for(TypedMessage tm: vecTMsg){
				if(!strTypes.equals("")) strTypes += "、";
				strTypes += tm.msgType.substring(tm.msgType.indexOf(".") + 1);
				//
				if(!strContent.equals("")) strContent += "\n\n";
				strContent += "留言編號 " + " — " +   tm.msgNo + "\n\n";
				if(!tm.logTitle.equals("")){
					strContent += "留言內容摘要 — " + tm.logTitle + "\n\n";
				}else{
					strContent += "留言內容摘要 — " + tm.logMsg + "\n\n";
				}
				//如果勾選傳送回覆內容者
				if(tm.withinMail == 1){
					strContent += "本局回覆內容 — " + tm.ourResponse + "\n\n";
				}
			}
			strMail += userNo + " 您好，\n\n";
			strMail += "本局已回覆您的留言";  //，由於您的留言涉及" + strTypes;
			strMail += "，為方便您的查詢，本局已將您的留言分類及編號如下：\n";
			strMail += strContent;
			strMail += "\n";
			strMail += "您可依「編號」或按「我的留言」查詢，歡迎上網瀏覽。\n\n";
			strMail += "金管會檢查局";
		}
		//
		DefaultHttpClient httpClient = new DefaultHttpClient();
		URIBuilder builder = new URIBuilder();
		builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
						.setParameter("type", "SendMail")
						.setParameter("mailNo", userAuditor.getEmailAddress())
						.setParameter("doBcc", "true")
						.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
		URI uri = builder.build();
		HttpPost httpPost = new HttpPost(uri);
		HttpResponse response = httpClient.execute(httpPost);
		BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = br.readLine();
		//
		if(response.getEntity() != null ) {
			httpPost.abort();
		}
	}
	
	public void doBackToUnPublish(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			
			conn = ds.getConnection();
			String sql = "UPDATE auditor_message SET responsed_status = 1, published_status = 0 WHERE msg_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.execute();
			//
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					vecMessage.remove(m);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	/*
	public void doResponse(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			String userNo = actionRequest.getParameter("userNo");
			User userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
			//
			Vector<TypedMessage> vecTMsg = new Vector<TypedMessage>();
			
			conn = ds.getConnection();
			String sql = "SELECT * FROM message_with_type WHERE msg_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				TypedMessage tm = new TypedMessage();
				tm.msgNo = rs.getString("msg_no");
				tm.msgType = rs.getString("msg_type");
				tm.logMsg = rs.getString("log_msg");
				vecTMsg.add(tm);
			}
			rs.close();
			//
			if(vecTMsg.size() == 0){
				throw new Exception("本留言尚未處理完成，無法進行回覆作業。");
			}
			//
			String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss" ;
			final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			String dateTimeString =  sdf.format(new Date());
			
			sql = "UPDATE auditor_message SET responsed = 1, responsed_time=? WHERE msg_id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dateTimeString);
			ps.setInt(2, msgId);
			ps.execute();
			//編寫郵件通知
			String strMail = "";
			if(vecTMsg.size() == 1){
				TypedMessage tm = vecTMsg.get(0);
				//
				strMail += userNo + " 您好，\n\n";
				strMail += "本局已回覆您的留言，您可依「編號」或「查詢我的留言」查詢：\n";
				strMail += "留言編號 — " + tm.msgNo + "\n";
				strMail += "留言內容摘要 — " + tm.logMsg + "\n";
				strMail += "\n";
				strMail += "金管會檢查局";
			}else{
				String strTypes = "";
				String strContent = "";
				for(TypedMessage tm: vecTMsg){
					if(!strTypes.equals("")) strTypes += "、";
					strTypes += tm.msgType.substring(tm.msgType.indexOf(".") + 1);
					//
					if(!strContent.equals("")) strContent += "\n";
					strContent += "留言編號 " + " — " +   tm.msgNo + "\n";
					strContent += "留言內容摘要 — " + tm.logMsg + "\n";
				}
				strMail += userNo + " 您好，\n\n";
				strMail += "本局已回覆您的留言，由於您的留言涉及" + strTypes;
				strMail += "，為方便您的查詢，本局已將您的留言分類及編號如下：\n";
				strMail += strContent;
				strMail += "\n";
				strMail += "您可依「編號」或按「我的留言」查詢，歡迎上網瀏覽。\n\n";
				strMail += "金管會檢查局";
			}
			//
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URIBuilder builder = new URIBuilder();
			builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
							.setParameter("type", "SendMail")
							.setParameter("mailNo", userAuditor.getEmailAddress())
							.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
			URI uri = builder.build();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse response = httpClient.execute(httpPost);
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = br.readLine();
			//
			if(response.getEntity() != null ) {
				httpPost.abort();
			}
			//
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					vecMessage.remove(m);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = utils.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public void doPublish(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			String userNo = actionRequest.getParameter("userNo");
			User userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
			//
			Vector<TypedMessage> vecTMsg = new Vector<TypedMessage>();
			
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditor_message WHERE msg_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				int responsed = rs.getInt("responsed");
				if(responsed == 0){
					throw new Exception("本留言尚未回覆完成，無法進行發布作業。");
				}else if(responsed == 2){
					throw new Exception("本留言已發布完成，無需重複發布。");
				}
			}
			rs.close();
			//
			sql = "UPDATE auditor_message SET responsed = 2 WHERE msg_id=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.execute();
			//
			Vector<Message> vecMessage = (Vector<Message>)actionRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					vecMessage.remove(m);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecMessage", vecMessage);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = utils.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	*/
	
	
	public void removeUser(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditors WHERE user_no <> 'test5'";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String userNo = rs.getString("user_no");
				User userAuditor = null;
				try{
					userAuditor = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
				}catch(Exception _ex){
				}
				if(userAuditor != null){
					UserLocalServiceUtil.deleteUser(userAuditor);
				}
				//
				sql = "DELETE FROM auditors WHERE user_no= ?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, userNo);
				ps.execute();
			}
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}

	public void removeMessage(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			String sql = "DELETE FROM auditor_message";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.execute();
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public void removeResponse(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			String sql = "DELETE FROM message_with_type";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.execute();
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private void alterTables() throws Exception{
		
		Connection conn = ds.getConnection();
		//
		boolean existed = false;
		String sql = "SELECT * FROM message_with_type";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		int numCol = meta.getColumnCount();
		for (int i = 1; i < numCol+1; i++) {
			if(meta.getColumnName(i).equals("log_title")){
				existed = true;
				break;
			}
		}
		if(!existed){
			sql = "ALTER TABLE message_with_type ADD log_title VARCHAR(30)";
			ps = conn.prepareStatement(sql);
			ps.execute();
			
		}
    		//
    		conn.close();
    		
	}

	/*
	private void publishResponse(){
		//回寫 auditor_message
		sql = "UPDATE auditor_message SET responsed = 1 WHERE msg_id=?";
		ps = conn.prepareStatement(sql);
		ps.setInt(1, msgId);
		ps.execute();
	}
	//編寫郵件通知
	String strMail = "";
	if(vecMsgNo.size() == 1){
		sql = "SELECT * FROM message_with_type WHERE msg_no = ?";
		ps = conn.prepareStatement(sql);
		ps.setString(1, vecMsgNo.get(0));
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			strMail += rs.getString("user_no") + " 您好，\n\n";
			strMail += "本局已回覆您的留言，歡迎上網瀏覽：\n";
			strMail += "留言編號 — " + vecMsgNo.get(0) + "\n";
			strMail += "留言內容摘要 — " + rs.getString("log_msg") + "\n";
			strMail += "\n";
			strMail += "金管會檢查局";
		}
		rs.close();
    }else if (vecMsgNo.size() > 1){
	    String msgTypeAll = "";
	    if(!msgType1.equals("")){
		    msgTypeAll += msgType1.substring(2);
	    }
	    if(!msgType2.equals("")){
		    if(!msgTypeAll.equals("")) msgTypeAll += "、";
		    msgTypeAll += msgType2.substring(2);
	    }
	    if(!msgType3.equals("")){
		    if(!msgTypeAll.equals("")) msgTypeAll += "、";
		    msgTypeAll += msgType3.substring(2);
	    }
	    if(!msgType4.equals("")){
		    if(!msgTypeAll.equals("")) msgTypeAll += "、";
		    msgTypeAll += msgType4.substring(2);
	    }
	    if(!msgType5.equals("")){
		    if(!msgTypeAll.equals("")) msgTypeAll += "、";
		    msgTypeAll += msgType5.substring(2);
	    }
	    if(!msgType6.equals("")){
		    if(!msgTypeAll.equals("")) msgTypeAll += "、";
		    msgTypeAll += msgType6.substring(2);
	    }
	    //
	    for(String _msgNo: vecMsgNo){
		    sql = "SELECT * FROM message_with_type WHERE msg_no = ?";
		    ps = conn.prepareStatement(sql);
		    ps.setString(1, _msgNo);
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()){
			    if(vecMsgNo.indexOf(_msgNo) == 0){
				    strMail += rs.getString("user_no") + " 您好，\n\n";
				    strMail += "本局已回覆您的留言，由於您的留言涉及" + msgTypeAll;
				    strMail += "，為方便您的查詢，本局已將您的留言分類及編號如下：\n";
			    }
			    strMail += "留言編號 " +  (vecMsgNo.indexOf(_msgNo) + 1) + " — " + _msgNo + "\n";
			    strMail += "留言內容摘要 — " + rs.getString("log_msg") + "\n";
			    strMail += "\n";
		    }
	    }
	    strMail += "您可依「編號」或按「我的留言」查詢，歡迎上網瀏覽。\n\n";
	    strMail += "金管會檢查局";
    }
		    //
		    DefaultHttpClient httpClient = new DefaultHttpClient();
		    URIBuilder builder = new URIBuilder();
		    builder.setScheme("http").setHost(httpHost).setPath(requestPath)
									.setParameter("type", "SendMail")
									.setParameter("mailNo", eMail)
									.setParameter("mailText", strMail);
		    URI uri = builder.build();
		    HttpPost httpPost = new HttpPost(uri);
		    HttpResponse response = httpClient.execute(httpPost);
		    BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		    String line = br.readLine();
		    //
		    if(response.getEntity() != null ) {
			   httpPost.abort();
		    }
		    //
		    if(line.equalsIgnoreCase("T")){
			    actionResponse.sendRedirect("/responseok");
		    }
	    }
	}
	*/
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		//if(showErrorCode == 1){
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			ex.printStackTrace(printWriter);
			//
			BufferedReader reader = new BufferedReader(
					   						new StringReader(stringWriter.toString()));
			try{
				String str = "";
				while((str = reader.readLine()) != null){
					if(str.contains("com.itez")){
						ret += ("\n" + str);
						break;
					}
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		//}
		return ret;
	}
	
}

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>


<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:12pt;
			color:darkred;
    		}
    	tr  {
    			height: 35px
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
    		resize:true;
	}
.button{
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}		
  </style>
<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

	int msgId = Integer.parseInt(renderRequest.getParameter("msgId"));

	Message message = new Message();
	Vector<Message> vecMessage = new Vector<Message>();
	if(renderRequest.getPortletSession(true).getAttribute("vecMessage") != null){
			vecMessage = (Vector<Message>)renderRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					message = m;
					break;
				}
			}
	}
	
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/response/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="doProcessURL" name="doProcess">
</portlet:actionURL>

<portlet:actionURL var="reMail2UserURL"  name="reMail2User">
	<portlet:param name="msgId" value="<%=String.valueOf(msgId)%>"/>
</portlet:actionURL>

<portlet:actionURL var="reMail2FebURL"  name="reMail2Feb">
	<portlet:param name="msgId" value="<%=String.valueOf(msgId)%>"/>
</portlet:actionURL>


<script type="text/javascript">
	function doResponse() {
		if (confirm("確定將輸入之內容回覆(以電子郵件)給留言者?")==true){
			document.getElementById("<portlet:namespace/>doResponse").value = "true";
			document.<portlet:namespace />fm.submit();
		}
	}
	
	function addFormalText(fldName) {
		//alert("特殊案件: " + fldName);
		var val = document.getElementById("<portlet:namespace/>" + fldName).value;
		if(val == ""){
			document.getElementById("<portlet:namespace/>" + fldName).value ="基於您的問題○○○(請權責組簡要表達無法及時處理之原因如：涉他局職掌或較為複雜等)，本局目前正在處理中，將會儘速回覆給您。";				
		}
	}
</script>

<table width="100%">
<tr><td>
<aui:form action="<%=doProcessURL%>" method="post" name="<portlet:namespace />fm">
	<aui:fieldset label="稽核人員留言">
		<aui:layout>
			<aui:column>
				<aui:input type="text" name="userNo" label="留言者" value="<%=message.userNo %>" readonly="readonly"/>
			</aui:column>
			<aui:column>
				<aui:input type="text"  name="logTime" label="留言時間" value="<%=message.logTime %>" readonly="readonly"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="5">
				<img src="<%=renderRequest.getContextPath()%>/images/message.png"/>
			</aui:column>
			<aui:column columnWidth="95">	
				<aui:input type="textarea" name="logMsg" label="留言內容" value="<%=message.logMsg %>"  rows="6" style="width: 100%;"  readonly="readonly" />
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
	<aui:fieldset label="本局回覆">
		<aui:input type="hidden" name="msgId" value="<%=msgId %>"/>
	<%
	for(int i=0; i < 6; i++){ 
		String msgType="", logTitle="", logMsg="", ourResponse="", msgNo="", timeStamp="";
		String strLogTitle="", strLogMsg = "";
		String nameLogTitle = "", nameLogMsg = "", nameMsgType = "", nameOurResponse ="", nameWithinMail = "", nameTimeStamp="";
		boolean selectedA = false, selectedB=false, selectedC=false, selectedD=false, selectedE=false, selectedF=false;
		int withinMail=0;
		switch(i){
			case 0:
				msgType = message.msgType1;
				logTitle = message.logTitle1;
				logMsg = message.logMsg1;
				ourResponse = message.ourResponse1;
				msgNo = message.msgNo1;
				timeStamp = message.timeStamp1;
				//if(ourResponse.equals("")){
				//	ourResponse = "「基於您的問題○○○(請權責組簡要表達無法及時處理之原因如：涉他局職掌或較為複雜等)，本局目前正在處理中，將會儘速回覆給您。」";
				//}
				strLogTitle = "留言標題一";
				strLogMsg = "留言摘要一";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle1";
				nameLogMsg = "logMsg1";
				nameMsgType = "msgType1";
				nameOurResponse = "ourResponse1";
				nameWithinMail = "withinMail1";
				if(message.withinMail1==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp1";
				break;
			case 1:
				msgType = message.msgType2;
				logTitle = message.logTitle2;
				logMsg = message.logMsg2;
				ourResponse = message.ourResponse2;
				msgNo = message.msgNo2;
				timeStamp = message.timeStamp2;
				strLogTitle = "留言標題二";
				strLogMsg = "留言摘要二";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle2";
				nameLogMsg = "logMsg2";
				nameMsgType = "msgType2";
				nameOurResponse = "ourResponse2";
				nameWithinMail = "withinMail2";
				if(message.withinMail2==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp2";
				break;
			case 2:
				msgType = message.msgType3;
				logTitle = message.logTitle3;
				logMsg = message.logMsg3;
				ourResponse = message.ourResponse3;
				msgNo = message.msgNo3;
				timeStamp = message.timeStamp3;
				strLogTitle = "留言標題三";
				strLogMsg = "留言摘要三";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle3";
				nameLogMsg = "logMsg3";
				nameMsgType = "msgType3";
				nameOurResponse = "ourResponse3";
				nameWithinMail = "withinMail3";
				if(message.withinMail3==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp3";
				break;
			case 3:
				msgType = message.msgType4;
				logTitle = message.logTitle4;
				logMsg = message.logMsg4;
				ourResponse = message.ourResponse4;
				msgNo = message.msgNo4;
				timeStamp = message.timeStamp4;
				strLogTitle = "留言標題四";
				strLogMsg = "留言摘要四";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle4";
				nameLogMsg = "logMsg4";
				nameMsgType = "msgType4";
				nameOurResponse = "ourResponse4";
				nameWithinMail = "withinMail4";
				if(message.withinMail4==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp4";
				break;
			case 4:
				msgType = message.msgType5;
				logTitle = message.logTitle5;
				logMsg = message.logMsg5;
				ourResponse = message.ourResponse5;
				msgNo = message.msgNo5;
				timeStamp = message.timeStamp5;
				strLogTitle = "留言標題五";
				strLogMsg = "留言摘要五";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle5";
				nameLogMsg = "logMsg5";
				nameMsgType = "msgType5";
				nameOurResponse = "ourResponse5";
				nameWithinMail = "withinMail5";
				if(message.withinMail5==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp5";
				break;	
			case 5:
				msgType = message.msgType6;
				logTitle = message.logTitle6;
				logMsg = message.logMsg6;
				ourResponse = message.ourResponse6;
				msgNo = message.msgNo6;
				timeStamp = message.timeStamp6;
				strLogTitle = "留言標題六";
				strLogMsg = "留言摘要六";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo + ")";
				}
				nameLogTitle = "logTitle6";
				nameLogMsg = "logMsg6";
				nameMsgType = "msgType6";
				nameOurResponse = "ourResponse6";
				nameWithinMail = "withinMail6";
				if(message.withinMail6==1){
					withinMail = 1;
				}
				nameTimeStamp = "timeStamp6";
				break;
			default:
		}
		if(msgType.startsWith("A")) selectedA = true;
		if(msgType.startsWith("B")) selectedB = true;
		if(msgType.startsWith("C")) selectedC = true;
		if(msgType.startsWith("D")) selectedD = true;
		if(msgType.startsWith("E")) selectedE = true;
		if(msgType.startsWith("F")) selectedF = true;
	%>
	
	<aui:layout>
		<aui:column columnWidth="50">
			<aui:input type="text"  name="<%=nameLogTitle %>"  label="<%=strLogTitle %>"  value="<%=logTitle %>"  style="width: 100%;" />
			<aui:input type="hidden"  name="<%=nameTimeStamp %>"   value="<%=timeStamp %>" />
		</aui:column>	
		<aui:column columnWidth="50">
			<aui:select name="<%=nameMsgType %>"  label="留言分類" >
				<aui:option value="A.稽核人員"  selected="<%=selectedA %>">A.稽核人員</aui:option>
				<aui:option value="B.自行查核"  selected="<%=selectedB %>">B.自行查核</aui:option>
				<aui:option value="C.內部控制"  selected="<%=selectedC %>">C.內部控制</aui:option>
				<aui:option value="D.稽核作業"  selected="<%=selectedD %>">D.稽核作業</aui:option>
				<aui:option value="E.檢查報告及缺改" selected="<%=selectedE %>">E.檢查報告及缺改</aui:option>
				<aui:option value="F.其他"  selected="<%=selectedF %>">F.其他</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column columnWidth="100">
			<aui:input type="textarea"  name="<%=nameLogMsg %>"  label="<%=strLogMsg %>"  value="<%=logMsg %>"  rows="3" style="width: 100%;" />
		</aui:column>	
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="5">
			<img src="<%=renderRequest.getContextPath()%>/images/reply.png"/>
		</aui:column>
		<aui:column>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="一般案件"  inlineLabel="right" value="0"  checked="<%= (withinMail == 0) %>"></aui:input>
		</aui:column>
		<aui:column>
		<%if(i==0){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse1');"></aui:input>
     		<%}else if(i==1){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse2');"></aui:input>
     		<%}else if(i==2){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse3');"></aui:input>
     		<%}else if(i==3){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse4');"></aui:input>
     		<%}else if(i==4){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse5');"></aui:input>
     		<%}else if(i==5){ %>
     			<aui:input name="<%=nameWithinMail %>"  type="radio"  label="特殊案件"  inlineLabel="right" value="1"  checked="<%= (withinMail == 1) %>"  onchange="addFormalText('ourResponse6');"></aui:input>
     		<%} %>
		</aui:column>
		<aui:column>
     			&nbsp;
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="5">
			&nbsp;
		</aui:column>
		<aui:column columnWidth="95">	
			<aui:input type="textarea"  name="<%=nameOurResponse %>" label="回覆內容"  value="<%=ourResponse %>" style="width: 100%;"  rows="6" />
		</aui:column>	
	</aui:layout>
	<%} %>
	
	<aui:layout>
		<aui:column>
			<aui:select name="doResponse"  label='"確定"後系統的動作：'  inlineLabel="left">
				<aui:option value="0">暫存</aui:option>
				<aui:option value="1">立即回覆並發布(特殊案件另案處理中)</aui:option>
				<aui:option value="2">立即回覆並發布(特殊案件存查歸檔)</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:button-row>
		<aui:button type="submit"  value="確定" cssClass="button"/>
		<aui:button onClick="<%=backToViewURL%>"  value="返回" cssClass="button"/>
		
		<br/>
     			註：一般案件回覆後系統自動發布；特殊案件(處理中或機敏案件)只通知留言人但不會發布。
	</aui:button-row>
	</aui:fieldset>
</aui:form>
</td></tr>
<%if(themeDisplay.getUser().getScreenName().equalsIgnoreCase("febmis")){ %>
<tr><td>
		<aui:button onClick="<%=reMail2UserURL %>"  value="重新發送留言者通知郵件" cssClass="button"/>
		<aui:button onClick="<%=reMail2FebURL %>"  value="重新發送內部通知郵件" cssClass="button"/>
</td></tr>			
<%} %>

</table>



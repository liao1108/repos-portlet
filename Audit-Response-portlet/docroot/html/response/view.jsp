<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>


<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	_tr  {
    			height: 35x
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}
	select {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	 .button{
	  	font-family:標楷體;
		font-size: 12px ;
		text-align: center ;
		line-height: 24px;
		text-decoration: none ; 
	}			
	
	.portlet-section-header results-header th {	
		background-color: #FF6633;
		color:white;
	}
		
	.portlet-section-alternate.results-row.alt.cwfi-row-index-1  td{
     		background-color: #FFFFCC;
 	}
  </style>
  
<%
  	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
  	//
  	Vector<Message> vecMessage = new Vector<Message>();
  	if(renderRequest.getPortletSession(true).getAttribute("vecMessage") != null){
  		vecMessage = (Vector<Message>)renderRequest.getPortletSession(true).getAttribute("vecMessage");
  	}
  	//
  	String responsedType= "未回覆";
  	if(renderRequest.getPortletSession(true).getAttribute("responsedType") != null){
  		responsedType = renderRequest.getPortletSession(true).getAttribute("responsedType").toString();
  	}
  	//
  	String errMsg = "";	
  	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
  	
  	String successMsg = "";	
  	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  	//
  	int rowIndex = 0;
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="removeUserURL" name="removeUser">
</portlet:actionURL>

<portlet:actionURL var="removeMessageURL" name="removeMessage">
</portlet:actionURL>

<portlet:actionURL var="removeResponseURL" name="removeResponse">
</portlet:actionURL>


<portlet:actionURL var="doQueryURL" name="doQuery">
</portlet:actionURL>

<table width="100%">
<tr>
	<td>
	
<aui:form action="<%=doQueryURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
			<img src="<%=renderRequest.getContextPath()%>/images/inbox.png"/>
		</aui:column>
		
		<aui:column>
			<aui:select name="responsedType" label="留言處理狀態" inlineLabel="left"  class="select">
				<aui:option value="未回覆" selected='<%=responsedType.equals("未回覆")%>' >未回覆之留言</aui:option>
				<aui:option value="已回覆未完全發布" selected='<%=responsedType.equals("已回覆未完全發布")%>'>已回覆未完全發布之留言</aui:option>
				<aui:option value="已完全發布" selected='<%=responsedType.equals("已完全發布")%>'>已完全發布(歸檔)之留言</aui:option>
			</aui:select>
		</aui:column>
		
		<aui:column>
			<aui:input type="submit"  name="submit" label=""  value="開始查詢" cssClass="button"/>
		</aui:column>	
	</aui:layout>
</aui:form>	

<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMessage, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMessage.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.Message" keyProperty="msgId" modelVar="Message">
		<%
			if(rowIndex % 2 == 1){
				row.setClassName("cwfi-row-index-1");
			}
			//
			rowIndex++;
		%>
		<liferay-ui:search-container-column-text name="留言時間" property="logTime"/>
		<liferay-ui:search-container-column-text name="留言者" property="userNo"/>
		<liferay-ui:search-container-column-text name="金融機構" property="bankName"/>
		<liferay-ui:search-container-column-text name="留言內容" property="logMsgDisplay"/>
		<liferay-ui:search-container-column-text name="留言分類號" property="msgNos"/>
		<liferay-ui:search-container-column-text name="回覆時間" property="responsedTime"/>
		
		<%if(responsedType.equals("未回覆")){ %>
			<liferay-ui:search-container-column-jsp path="/html/admin/admin_process.jsp" align="center" />
		<%}else if(responsedType.equals("已回覆未完全發布")){ %>
			<liferay-ui:search-container-column-jsp path="/html/admin/admin_process.jsp" align="center" />
		<%}else{ %>
			<liferay-ui:search-container-column-jsp path="/html/admin/admin_view.jsp" align="center" />
		<%} %>	
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

</td>
</tr>

<%if(themeDisplay.getUser().getScreenName().equalsIgnoreCase("febmis")){ %>
<tr>
	<td>
		<aui:layout>
			<aui:column>
				測試資料刪除：
			</aui:column>
			<aui:column>
				<aui:button type="submit" onClick="<%=removeUserURL %>" value="清除使用帳號" cssClass="button"/>
			</aui:column>
			<aui:column>
				<aui:button type="submit" onClick="<%=removeMessageURL %>"  value="清除留言紀錄" cssClass="button"/>
			</aui:column>
			<aui:column>
				<aui:button type="submit"  onClick="<%=removeResponseURL %>"  value="清除已發布之留言" cssClass="button"/>
			</aui:column>
			
		</aui:layout>
	</td>
</tr>
</table>
<%}%>
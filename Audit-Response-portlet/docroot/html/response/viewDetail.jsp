<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>


<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:12pt;
			color:darkred;
    		}
    	tr  {
    			height: 35px
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
    		resize:true;
	}	
	.button{
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}		
  </style>
<%
	int msgId = Integer.parseInt(renderRequest.getParameter("msgId"));

	Message message = new Message();
	Vector<Message> vecMessage = new Vector<Message>();
	if(renderRequest.getPortletSession(true).getAttribute("vecMessage") != null){
			vecMessage = (Vector<Message>)renderRequest.getPortletSession(true).getAttribute("vecMessage");
			for(Message m: vecMessage){
				if(m.msgId == msgId){
					message = m;
					break;
				}
			}
	}
	
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/response/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="doBackToUnPublishURL"  name="doBackToUnPublish" >
</portlet:actionURL>

<table width="100%">
</tr>
<tr><td>
<aui:form action="<%=doBackToUnPublishURL%>"    method="post" name="<portlet:namespace />fm">
	<aui:fieldset label="稽核人員留言">
	<aui:layout>
		<aui:column>
			<aui:input type="text" name="userNo" label="留言者" value="<%=message.userNo %>" readonly="readonly"/>
		</aui:column>
		<aui:column>
			<aui:input type="text"  name="logTime" label="留言時間" value="<%=message.logTime %>" readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="5">
			<img src="<%=renderRequest.getContextPath()%>/images/message.png""/>
		</aui:column>
		<aui:column columnWidth="95">	
			<aui:input type="textarea" name="logMsg" label="留言內容" value="<%=message.logMsg %>"  rows="6" style="width: 100%;"  readonly="readonly" />
		</aui:column>
	</aui:layout>
	</aui:fieldset>
	
	
	<aui:fieldset label="本局回覆">
		<aui:input type="hidden" name="msgId" value="<%=msgId %>"/>
	<%
	for(int i=0; i < 6; i++){ 
		String msgType="", logTitle="", logMsg="", ourResponse="", msgNo="";
		String strLogTitle="", strLogMsg = "";
		String nameLogTitle = "", nameLogMsg = "", nameMsgType = "", nameOurResponse ="";
		boolean selectedA = false, selectedB=false, selectedC=false, selectedD=false, selectedE=false, selectedF=false;
		int withinMail = 0;		//1.特殊案件
		switch(i){
			case 0:
				msgType = message.msgType1;
				logTitle = message.logTitle1;
				logMsg = message.logMsg1;
				ourResponse = message.ourResponse1;
				msgNo = message.msgNo1;
				withinMail = message.withinMail1;
				strLogTitle = "留言標題一";
				strLogMsg = "留言摘要一";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg1";
				nameMsgType = "msgType1";
				nameOurResponse = "ourResponse1";
				break;
			case 1:
				msgType = message.msgType2;
				logTitle = message.logTitle2;
				logMsg = message.logMsg2;
				ourResponse = message.ourResponse2;
				msgNo = message.msgNo2;
				withinMail = message.withinMail2;
				strLogTitle = "留言標題二";
				strLogMsg = "留言摘要二";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg2";
				nameMsgType = "msgType2";
				nameOurResponse = "ourResponse2";
				break;
			case 2:
				msgType = message.msgType3;
				logTitle = message.logTitle3;
				logMsg = message.logMsg3;
				ourResponse = message.ourResponse3;
				msgNo = message.msgNo3;
				withinMail = message.withinMail3;
				strLogTitle = "留言標題三";
				strLogMsg = "留言摘要三";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg3";
				nameMsgType = "msgType3";
				nameOurResponse = "ourResponse3";
				break;
			case 3:
				msgType = message.msgType4;
				logTitle = message.logTitle4;
				logMsg = message.logMsg4;
				ourResponse = message.ourResponse4;
				msgNo = message.msgNo4;
				withinMail = message.withinMail4;
				strLogTitle = "留言標題四";
				strLogMsg = "留言摘要四";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg4";
				nameMsgType = "msgType4";
				nameOurResponse = "ourResponse4";
				break;
			case 4:
				msgType = message.msgType5;
				logTitle = message.logTitle5;
				logMsg = message.logMsg5;
				ourResponse = message.ourResponse5;
				msgNo = message.msgNo5;
				withinMail = message.withinMail5;
				strLogTitle = "留言標題五";
				strLogMsg = "留言摘要五";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg5";
				nameMsgType = "msgType5";
				nameOurResponse = "ourResponse5";
				break;	
			case 5:
				msgType = message.msgType6;
				logTitle = message.logTitle6;
				logMsg = message.logMsg6;
				ourResponse = message.ourResponse6;
				msgNo = message.msgNo6;
				withinMail = message.withinMail6;
				strLogTitle = "留言標題六";
				strLogMsg = "留言摘要六";
				if(!msgNo.equals("")){
					strLogMsg += "(編號 " + msgNo;
					if(withinMail == 1){
						strLogMsg += " " + "特殊案件";
					}
					strLogMsg += ")";
				}
				nameLogMsg = "logMsg6";
				nameMsgType = "msgType6";
				nameOurResponse = "ourResponse6";
				break;
			default:
		}
		if(msgType.startsWith("A")) selectedA = true;
		if(msgType.startsWith("B")) selectedB = true;
		if(msgType.startsWith("C")) selectedC = true;
		if(msgType.startsWith("D")) selectedD = true;
		if(msgType.startsWith("E")) selectedE = true;
		if(msgType.startsWith("F")) selectedF = true;
		//
		if(!logMsg.equals("")){
	%>
	<aui:layout>
		<aui:column columnWidth="50">
			<aui:input type="text"  name="<%=nameLogTitle %>"  label="<%=strLogTitle %>"  value="<%=logTitle %>"  style="width: 100%;" />
		</aui:column>
			
		<aui:column columnWidth="50">
			<aui:select name="<%=nameMsgType %>"  label="留言分類" >
				<aui:option value="A.稽核人員"  selected="<%=selectedA %>">A.稽核人員</aui:option>
				<aui:option value="B.自行查核"  selected="<%=selectedB %>">B.自行查核</aui:option>
				<aui:option value="C.內部控制"  selected="<%=selectedC %>">C.內部控制</aui:option>
				<aui:option value="D.稽核作業"  selected="<%=selectedD %>">D.稽核作業</aui:option>
				<aui:option value="E.檢查報告及缺改" selected="<%=selectedE %>">E.檢查報告及缺改</aui:option>
				<aui:option value="F.其他"  selected="<%=selectedF %>">F.其他</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column columnWidth="100">
			<aui:input type="textarea"  name="<%=nameLogMsg %>"  label="<%=strLogMsg %>"  value="<%=logMsg %>"  rows="3" style="width: 100%;"  readonly="readonly"/>
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column columnWidth="5">
			<img src="<%=renderRequest.getContextPath()%>/images/reply.png"/>
		</aui:column>
		<aui:column columnWidth="95">	
			<aui:input type="textarea"  name="<%=nameOurResponse %>" label="回覆訊息"  value="<%=ourResponse %>" style="width: 100%;"  rows="6" readonly="readonly"/>
		</aui:column>	
	</aui:layout>
	<%}
			} %>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL%>"  value="返回上頁"/>
		<aui:button type="submit"  value="退回未發布(歸檔)狀態"/>
	</aui:button-row>
	</aui:fieldset>
</aui:form>
</td></tr>
</table>





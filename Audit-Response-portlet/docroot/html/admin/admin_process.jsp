<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Message" %>

<portlet:defineObjects />

<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	//
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Message message = (Message)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="doProcessURL" >
		<portlet:param name="msgId" value="<%=String.valueOf(message.msgId) %>"/>
		<portlet:param name="jspPage" value="/html/response/process.jsp"/>
	</portlet:renderURL>
	
	<portlet:actionURL var="doRemoveURL"  name="doRemove">
		<portlet:param name="msgId" value="<%=String.valueOf(message.msgId) %>"/>
	</portlet:actionURL>
	

	<liferay-ui:icon image="edit" message="回覆" url="<%=doProcessURL.toString() %>"/>
	
	<%if(themeDisplay.getUser().getScreenName().equalsIgnoreCase("febmis")){ %>
		<liferay-ui:icon image="delete" message="刪除" url="<%=doRemoveURL.toString() %>"/>
	<%} %>
</liferay-ui:icon-menu>

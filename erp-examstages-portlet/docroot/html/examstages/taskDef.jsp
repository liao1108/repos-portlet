<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.ExamFlow"%>
<%@ page import= "com.itez.Stage"%>
 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	boolean isLeader = false;
	if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
		isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	}
	//
	String bankName = "";
	if(renderRequest.getPortletSession(true).getAttribute("bankName") != null){
		bankName = renderRequest.getPortletSession(true).getAttribute("bankName").toString();
	}
	//
	ExamFlow ef = new ExamFlow();
	if(renderRequest.getPortletSession(true).getAttribute("examFlow") != null){
		ef = (ExamFlow)renderRequest.getPortletSession(true).getAttribute("examFlow");
	}
	
	Stage currStage = new Stage();
	if(renderRequest.getPortletSession(true).getAttribute("currStage") != null){
		currStage = (Stage)renderRequest.getPortletSession(true).getAttribute("currStage");
	}
	//
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/html/examstages/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="updateExamFlowURL"  name="updateExamFlow" >
</portlet:actionURL>		

		
<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/job.png">&nbsp;本案(<%=bankName %>)工作階段(<%=currStage.stageName %>)之工作定義
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:form action="<%=updateExamFlowURL%>" method="post" name="<portlet:namespace />fm">
<%if(currStage.stageName.contains("行前")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 產出助檢版報告表時是否以電子郵件通知負責的助檢人員：
			<%if(ef.formGenedMail2Assistant==1){ %>
				<aui:input name="formGenedMail2Assistant" type="radio"  value="1" label="是" checked="true"/>
	                  <aui:input name="formGenedMail2Assistant" type="radio"  value="0" label="否"/>
	            <%}else{ %>
				<aui:input name="formGenedMail2Assistant" type="radio"  value="1" label="是"/>
	                  <aui:input name="formGenedMail2Assistant" type="radio"  value="0" label="否"  checked="true"/>
			<%} %>	                  
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("期間")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 助檢上傳/更新檔案時是否以電子郵件通知領隊：
			<%if(ef.inExamMail2Leader==1){ %>
				<aui:input name="inExamMail2Leader" type="radio"  value="1" label="是" checked="true"/>
	                  <aui:input name="inExamMail2Leader" type="radio"  value="0" label="否" />
	            <%}else{ %> 
				<aui:input name="inExamMail2Leader" type="radio"  value="1" label="是" />
	                  <aui:input name="inExamMail2Leader" type="radio"  value="0" label="否"  checked="true"/>
			<%} %>	                 
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("結束")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 助檢上傳/更新檔案時是否以電子郵件通知領隊：
			<%if(ef.examEndMail2Leader==1){ %>
				<aui:input name="examEndMail2Leader" type="radio"  value="1" label="是"  checked="true" />
	                  <aui:input name="examEndMail2Leader" type="radio"  value="0" label="否"/>
	             <%}else{ %>
				<aui:input name="examEndMail2Leader" type="radio"  value="1" label="是" />
	                  <aui:input name="examEndMail2Leader" type="radio"  value="0" label="否" checked="true"/>
			<%} %>	                  
		</aui:column>
	</aui:layout>
	<br/>
	<aui:layout>
		<aui:column cssClass="td">
			※ 助檢上傳/更新檔案時是否啟動審核流程：
			<%if(ef.examEndEnableWorkFlow==1){ %>
				<aui:input name="examEndEnableWorkFlow" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="examEndEnableWorkFlow" type="radio"  value="0" label="否" />
	             <%}else{ %>
				<aui:input name="examEndEnableWorkFlow" type="radio"  value="1" label="是" />
	                  <aui:input name="examEndEnableWorkFlow" type="radio"  value="0" label="否"  checked="true"/>
	             <%} %>    
		</aui:column>
	</aui:layout>
	<br/>
	<aui:layout>
		<aui:column cssClass="td">
			※ 啟動審核流程時之簽審程序：
			<%if(ef.examEndWorkFlowType==1){ %>
				<aui:input name="examEndWorkFlowType" type="radio"  value="1"  label="助檢→領隊"  checked="true"/>
	                  <aui:input name="examEndWorkFlowType" type="radio"  value="2"  label="助檢→領隊→主管" />
	            <%}else{%>
				<aui:input name="examEndWorkFlowType" type="radio"  value="1"  label="助檢→領隊"  />
	                  <aui:input name="examEndWorkFlowType" type="radio"  value="2"  label="助檢→領隊→主管" checked="true" />
	            <%} %>      
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("會前")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 檔案異動是否啟動審核流程：
			<%if(ef.preMeetEnableWorkFlow==1){ %>
				<aui:input name="preMeetEnableWorkFlow" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="preMeetEnableWorkFlow" type="radio"  value="0" label="否" />
	            <%}else{ %>
				<aui:input name="preMeetEnableWorkFlow" type="radio"  value="1" label="是" />
	                  <aui:input name="preMeetEnableWorkFlow" type="radio"  value="0" label="否"  checked="true"/>
	            <%} %>
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("會後")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 檔案異動是否啟動審核流程：
			<%if(ef.endMeetEnableWorkFlow==1){ %>
				<aui:input name="endMeetEnableWorkFlow" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="endMeetEnableWorkFlow" type="radio"  value="0" label="否" />
	             <%}else{ %>
				<aui:input name="endMeetEnableWorkFlow" type="radio"  value="1" label="是"  />
	                  <aui:input name="endMeetEnableWorkFlow" type="radio"  value="0" label="否"  checked="true"/>
	             <%} %>     
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("評等")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 助檢上傳/更新檔案時是否以電子郵件通知領隊：
			<%if(ef.evalMail2Leader==1){ %>
				<aui:input name="evalMail2Leader" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="evalMail2Leader" type="radio"  value="0" label="否" />
	             <%}else{ %>
				<aui:input name="evalMail2Leader" type="radio"  value="1" label="是" />
	                  <aui:input name="evalMail2Leader" type="radio"  value="0" label="否" checked="true"/>
	             <%} %>     
		</aui:column>
	</aui:layout>
	<br/>
	<aui:layout>
		<aui:column cssClass="td">
			※ 主評人員上傳/更新檔案時是否以電子郵件通知領隊：
			<%if(ef.evalKeyUserMail2Leader==1){ %>
				<aui:input name="evalKeyUserMail2Leader" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="evalKeyUserMail2Leader" type="radio"  value="0" label="否" />
	            <%}else{ %>
				<aui:input name="evalKeyUserMail2Leader" type="radio"  value="1" label="是" />
	                  <aui:input name="evalKeyUserMail2Leader" type="radio"  value="0" label="否" checked="true"/>
			<%} %>	                  
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column cssClass="td">
			※ 檔案上傳/更新檔案時是否啟動審核流程：
			<%if(ef.evalEnableWorkFlow==1){ %>
				<aui:input name="evalEnableWorkFlow" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="evalEnableWorkFlow" type="radio"  value="0" label="否" />
	             <%}else{ %>
				<aui:input name="evalEnableWorkFlow" type="radio"  value="1" label="是"  />
	                  <aui:input name="evalEnableWorkFlow" type="radio"  value="0" label="否" checked="true"/>
	             <%} %>     
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column cssClass="td">
			※ 啟動審核流程時之簽審程序：
			<%if(ef.evalEnableWorkFlow==1){ %>
				<aui:input name="evalWorkFlowType" type="radio"  value="1"  label="助檢→領隊"  checked="true"/>
	                  <aui:input name="evalWorkFlowType" type="radio"  value="2"  label="助檢→領隊→主管" />
	            <%}else{ %>
				<aui:input name="evalWorkFlowType" type="radio"  value="1"  label="助檢→領隊" />
	                  <aui:input name="evalWorkFlowType" type="radio"  value="2"  label="助檢→領隊→主管"  checked="true" />
	            <%} %>      
		</aui:column>
	</aui:layout>
<%}else if(currStage.stageName.contains("陳核")){ %>
	<aui:layout>
		<aui:column cssClass="td">
			※ 檔案是否啟動審核流程：
			<%if(ef.postEnableWorkFlow==1){ %>
				<aui:input name="postEnableWorkFlow" type="radio"  value="1" label="是"  checked="true"/>
	                  <aui:input name="postEnableWorkFlow" type="radio"  value="0" label="否" />
	             <%}else{ %>
				<aui:input name="postEnableWorkFlow" type="radio"  value="1" label="是" />
	                  <aui:input name="postEnableWorkFlow" type="radio"  value="0" label="否"   checked="true"/>
	             <%} %>     
		</aui:column>
	</aui:layout>
<%}%>

<aui:button-row>
	<aui:button onClick="<%=backToViewURL.toString() %>" value="返回"/>
	<aui:button  type="submit"   value="更新" />
</aui:button-row>

</aui:form>
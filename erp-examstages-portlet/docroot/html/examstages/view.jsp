<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Stage"%>
 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	boolean isLeader = false;
	if(renderRequest.getPortletSession(true).getAttribute("isLeader") != null){
		isLeader = (Boolean)renderRequest.getPortletSession(true).getAttribute("isLeader");
	}
	//
	String reportNo = "";
	if(renderRequest.getPortletSession(true).getAttribute("reportNo") != null){
		reportNo = renderRequest.getPortletSession(true).getAttribute("reportNo").toString();
	}
	//
	String bankName = "";
	if(renderRequest.getPortletSession(true).getAttribute("bankName") != null){
		bankName = renderRequest.getPortletSession(true).getAttribute("bankName").toString();
	}
	//
	Vector<Stage> vecStage = new Vector<Stage>();
	if(renderRequest.getPortletSession(true).getAttribute("vecStage") != null){
		vecStage = (Vector<Stage>)renderRequest.getPortletSession(true).getAttribute("vecStage");
	}
	//
	String userFullName = "";
	if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
		userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
	}
	
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/step.png">&nbsp;本案(<%=reportNo + " " + bankName %>)之工作階段
	</aui:column>
	<aui:column>
		&nbsp;<a href="javascript:history.go(-1);">回上頁</a>
	</aui:column>
</aui:layout>
<hr/>
<br>

<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecStage, searchContainer.getStart(), searchContainer.getEnd());
			total = vecStage.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Stage" keyProperty="folderId" modelVar="stage">
		<portlet:actionURL var="enterStageURL"  name="enterStage">
			<portlet:param name="folderId" value="<%=stage.folderId %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text align="center">
			<%if(stage.currStage){ %>
				<img src="<%=renderRequest.getContextPath()%>/images/here.png"/>
			<%} %>	
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="階段" property="stageName"   href="<%=enterStageURL.toString() %>" align="left"/>

		<%if(isLeader && !stage.isClosed){ %>
			<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center"  />
		<%} %>	
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>


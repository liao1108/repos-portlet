package com.itez;

public class ExamFlow {
	public String examNo = "";				//階段所在目錄 id
	public int formGenedMail2Assistant = 1;		//檢查行前階段 產出工作分配表時是否email給助檢人員
	public int inExamMail2Leader = 1;			//檢查期間階段 有助檢文件匯入時是否通知領隊
	public int examEndMail2Leader = 1;		//檢查結束階段 助檢文件匯入時是否email給領隊
	public int examEndEnableWorkFlow = 1;		//檢查結束階段 助檢文件匯入時是否啟動流程
	public int examEndWorkFlowType = 1;		//檢查結束階段 助檢文件匯入時之流程種類
	public int preMeetEnableWorkFlow = 0;		//檢討會前階段 文件匯入時是否啟動流程
	public int endMeetEnableWorkFlow = 0;		//檢討會後階段 文件匯入時是否啟動流程
	public int evalMail2Leader = 0;			//評等審議階段 助檢文件匯入時是否email給領隊
	public int evalKeyUserMail2Leader = 0;		//評等審議階段 主評文件匯入時是否email給領隊
	public int evalEnableWorkFlow = 1;			//評等審議階段 文件匯入時是否啟動流程
	public int evalWorkFlowType = 1;			//評等審議階段 助檢文件匯入時之流程種類
	public int postEnableWorkFlow = 0; 		//報告陳核階段 是否啟動流程
}

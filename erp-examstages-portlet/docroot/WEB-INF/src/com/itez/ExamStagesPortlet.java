package com.itez;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;
import javax.xml.namespace.QName;

import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ExamStagesPortlet extends MVCPortlet {
	DataSource ds = null;
	//
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userNo = themeDisplay.getUser().getScreenName();
			conn = ds.getConnection();
			//
			int examId = 0;
			if(LiferaySessionUtil.getGlobalSessionAttribute("currExamId", renderRequest) != null){
				examId = (int)LiferaySessionUtil.getGlobalSessionAttribute("currExamId", renderRequest);
			}else{
				String sql = "SELECT exam_id FROM curr_stage WHERE row_timestamp IS NOT NULL AND user_no=? ORDER BY row_timestamp DESC";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, themeDisplay.getUser().getScreenName());
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					examId = rs.getInt(1);
				}
				rs.close();
			}
			//
			if(examId == 0){
				conn.close();
				return;
			}
			//
			//User user = themeDisplay.getUser();
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String sql = "SELECT A.*, B.report_no FROM exams A, reports B WHERE A.report_id = B.report_id AND A.exam_id=? AND (A.leader_name LIKE '%" + userFullName + "%' OR B.pm_name LIKE '%" + userFullName + "%' OR A.staffs LIKE '%" + userFullName + "%' OR A.verifiers LIKE '%" + userFullName + "%') ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ResultSet rs = ps.executeQuery();
			if(!rs.next()) throw new Exception("您沒有權限進入本檢查案。");
			//
			String reportNo = rs.getString("report_no");
			String examNo = rs.getString("exam_no");
			String bankName = rs.getString("bank_name");
			String examFolderId = rs.getString("folder_id");
			//檢查本報告案是否已關閉
			boolean reportClosed = ErpUtil.isReportClosed(examId, conn);
			//
			boolean isLeader = false;
			if(rs.getString("leader_name").equals(userFullName)){
				isLeader = true;
			}
			//
			rs.close();
			//查詢流程定義
			ExamFlow ef = new ExamFlow();
			ef.examNo = examNo;
			
			sql = "SELECT * FROM stage_rules WHERE exam_no = ?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, examNo);
			rs = ps.executeQuery();
			if(rs.next()){
				ef.formGenedMail2Assistant = rs.getInt("form_gened_mail2_assistant");		//檢查行前產出工作分配表時是否email給助檢人員
				ef.inExamMail2Leader = rs.getInt("in_exam_mail2_leader");			//檢查期間有助檢文件匯入時是否通知領隊
				ef.examEndMail2Leader = rs.getInt("exam_end_mail2_leader");		//檢查結束助檢文件匯入時是否email給領隊
				ef.examEndEnableWorkFlow = rs.getInt("exam_end_enable_workflow");		//檢查結束助檢文件匯入時是否啟動流程
				ef.examEndWorkFlowType = rs.getInt("exam_end_workflow_type");		//檢查結束助檢文件匯入時之流程種類
				ef.preMeetEnableWorkFlow = rs.getInt("pre_meet_enable_workflow");		//檢討會前文件匯入時是否啟動流程
				ef.endMeetEnableWorkFlow = rs.getInt("end_meet_enable_workflow");		//檢討會後文件匯入時是否啟動流程
				ef.evalMail2Leader = rs.getInt("eval_mail2_leader");			//評等審議助檢文件匯入時是否email給領隊
				ef.evalKeyUserMail2Leader = rs.getInt("eval_keyuser_mail2_leader");		//評等審議主評文件匯入時是否email給領隊
				ef.evalEnableWorkFlow = rs.getInt("eval_enable_workflow");			//評等審議助檢文件匯入時是否啟動流程
				ef.evalWorkFlowType = rs.getInt("eval_workflow_type");			//評等審議助檢文件匯入時之流程種類
				ef.postEnableWorkFlow = rs.getInt("post_enable_workflow");
			}
			rs.close();
			
			//內定的作業階段
			String stageFolderIdCurr = "";
			sql = "SELECT stage_folder_id FROM curr_stage WHERE exam_id=? AND user_no=?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ps.setString(2, userNo);
			rs = ps.executeQuery();
			if(rs.next()){
				stageFolderIdCurr = rs.getString(1);
			}
			rs.close();
			//
			TreeMap<String, Stage> tmStage = new TreeMap<String, Stage>();
			//
			Session alfSessionAdmin =  ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(examFolderId);
			Iterator<CmisObject> it = fdExam.getChildren().iterator();
			while(it.hasNext()){
				CmisObject co = it.next();
				//
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){
					Stage st = new Stage();
					st.folderId = co.getId();
					st.stageName = co.getName();
					st.isClosed = reportClosed;
					if(st.folderId.equals(stageFolderIdCurr)){
						st.currStage = true;
					}else if(stageFolderIdCurr.equals("") && st.stageName.contains("行前")){
						st.currStage = true;
					}
					//
					tmStage.put(st.stageName, st);
				}
			}
			//
			Vector<Stage> vecStage = new Vector<Stage>();
			Iterator<String> it2 = tmStage.keySet().iterator();
			while(it2.hasNext()){
				vecStage.add(tmStage.get(it2.next()));
			}
			//
			renderRequest.getPortletSession(true).setAttribute("isLeader", isLeader);
			renderRequest.getPortletSession(true).setAttribute("examId", examId);
			renderRequest.getPortletSession(true).setAttribute("reportNo", reportNo);
			renderRequest.getPortletSession(true).setAttribute("examNo", examNo);
			renderRequest.getPortletSession(true).setAttribute("bankName", bankName);
			renderRequest.getPortletSession(true).setAttribute("vecStage", vecStage);
			renderRequest.getPortletSession(true).setAttribute("examFlow", ef);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//取得進行中或已歸檔的專案
	/*
	public Vector<ExamInfo> getProjects(ThemeDisplay themeDisplay, Folder fdRoot, Connection conn, String userName) throws Exception{
		Vector<ExamInfo> vec = new Vector<ExamInfo>();
		//
		String sql = "SELECT * FROM projects WHERE 1 = 1 ";
		if(!userName.equals("")){
			sql += " AND (leader_name = '" + userName + "' OR staffs LIKE '%" + userName + "%') ";
		}
		sql += " ORDER BY archived DESC, create_date DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			ExamInfo p = new ExamInfo();
			//
			p.projectId = rs.getInt("project_id");
			p.createDate = rs.getString("create_date");
			if(rs.getString("exam_no") != null){
				p.examNo = rs.getString("exam_no");
			}
			p.projectName = rs.getString("project_name");
			if(rs.getBoolean("archived")){
				p.archived = true;
			}
			if(rs.getString("bank_name") != null){
				p.bankName = rs.getString("bank_name");
			}
			//
			vec.add(p);
		}
		//
		return vec;
	}

	
	//載入
	public void loadProject(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			int projectId = Integer.parseInt(actionRequest.getParameter("projectId"));
			//System.out.println("Project Id: " + projectId);
			//
			QName qName = new QName("http://itez.com", "sendProjectId", "x");
			 actionResponse.setEvent(qName, String.valueOf(projectId));
			 //
			 actionRequest.setAttribute("successMsg", "檢查案基本資料請由右方視窗編輯。");
			 SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = utils.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	*/
	
	//編輯階段流程設定
	public void editStageDefinition(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		try{
			String folderId = actionRequest.getParameter("stageFolderId");
			//
			Stage currStage = null;
			Vector<Stage> vecStage = (Vector<Stage>)actionRequest.getPortletSession(true).getAttribute("vecStage");
			for(Stage st: vecStage){
				if(st.folderId.equals(folderId)){
					currStage = st;
				}
			}
			//
			if(currStage == null) throw new Exception("找不到指定的階段。");
			//
			actionRequest.getPortletSession(true).setAttribute("currStage", currStage);
			actionResponse.setRenderParameter("jspPage", "/html/examstages/taskDef.jsp");
			//
			actionRequest.setAttribute("successMsg", "本階段流程規則請由下方視窗設定。");
			SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	
	//進入
	@SuppressWarnings("resource")
	public void enterStage(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userNo = themeDisplay.getUser().getScreenName();
			//
			String folderId = actionRequest.getParameter("folderId");
			//System.out.println("Sending stage folder id: " + folderId);
			
			int examId = (int)actionRequest.getPortletSession(true).getAttribute("examId");
			conn = ds.getConnection();
			String sql = "SELECT 1 FROM curr_stage WHERE exam_id = ? AND user_no=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, examId);
			ps.setString(2, userNo);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				sql = "UPDATE curr_stage SET stage_folder_id=?, row_timestamp=? WHERE exam_id=? AND user_no=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, folderId);
				ps.setString(2, ErpUtil.getCurrDateTime());
				ps.setInt(3, examId);
				ps.setString(4, userNo);
				ps.execute();
			}else{
				sql = "INSERT INTO curr_stage (exam_id, stage_folder_id, user_no, row_timestamp) VALUES (?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setInt(1, examId);
				ps.setString(2, folderId);
				ps.setString(3, userNo);
				ps.setString(4, ErpUtil.getCurrDateTime());
				ps.execute();
			}
			rs.close();
			//
			QName qName = new QName("http://itez.com", "sendStageFolderId", "x");
			actionResponse.setEvent(qName, folderId);
			//
			actionRequest.setAttribute("successMsg", "本階段作業請由右方視窗執行。");
			SessionMessages.add(actionRequest, "success");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//更新流程設定
	public void updateExamFlow(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			ExamFlow ef = (ExamFlow)actionRequest.getPortletSession(true).getAttribute("examFlow");
			//
			conn = ds.getConnection();
			//Log
			ErpUtil.logUserAction("更新階段設定", ef.examNo, themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			if(actionRequest.getParameter("formGenedMail2Assistant") != null){
				int val = Integer.parseInt(actionRequest.getParameter("formGenedMail2Assistant"));
				String sql = "UPDATE stage_rules SET form_gened_mail2_assistant = ? WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, val);
				ps.setString(2, ef.examNo);
				ps.execute();
				//
				ef.formGenedMail2Assistant = val;
			}else if(actionRequest.getParameter("inExamMail2Leader") != null){
				int val = Integer.parseInt(actionRequest.getParameter("inExamMail2Leader"));
				String sql = "UPDATE stage_rules SET in_exam_mail2_leader = ? WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, val);
				ps.setString(2, ef.examNo);
				ps.execute();
				//
				ef.inExamMail2Leader = val;
			}else if(actionRequest.getParameter("examEndMail2Leader") != null){
				int examEndMail2Leader = Integer.parseInt(actionRequest.getParameter("examEndMail2Leader"));
				int examEndEnableWorkFlow = Integer.parseInt(actionRequest.getParameter("examEndEnableWorkFlow"));
				int examEndWorkFlowType = Integer.parseInt(actionRequest.getParameter("examEndWorkFlowType"));
				String sql = "UPDATE stage_rules SET exam_end_mail2_leader = ?, " +
											"exam_end_enable_workflow=?, " +
											"exam_end_workflow_type=? " +
										" WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, examEndMail2Leader);
				ps.setInt(2, examEndEnableWorkFlow);
				ps.setInt(3, examEndWorkFlowType);
				ps.setString(4, ef.examNo);
				ps.execute();
				//
				ef.examEndMail2Leader = examEndMail2Leader;
				ef.examEndEnableWorkFlow = examEndEnableWorkFlow;
				ef.examEndWorkFlowType = examEndWorkFlowType;
			}else if(actionRequest.getParameter("preMeetEnableWorkFlow") != null){
				int val = Integer.parseInt(actionRequest.getParameter("preMeetEnableWorkFlow"));
				String sql = "UPDATE stage_rules SET pre_meet_enable_workflow = ? WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, val);
				ps.setString(2, ef.examNo);
				ps.execute();
				//
				ef.preMeetEnableWorkFlow = val;
			}else if(actionRequest.getParameter("endMeetEnableWorkFlow") != null){
				int val = Integer.parseInt(actionRequest.getParameter("endMeetEnableWorkFlow"));
				String sql = "UPDATE stage_rules SET end_meet_enable_workflow = ? WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, val);
				ps.setString(2, ef.examNo);
				ps.execute();
				//
				ef.endMeetEnableWorkFlow = val;
			}else if(actionRequest.getParameter("evalMail2Leader") != null){
				int evalMail2Leader = Integer.parseInt(actionRequest.getParameter("evalMail2Leader"));
				int evalKeyUserMail2Leader = Integer.parseInt(actionRequest.getParameter("evalKeyUserMail2Leader"));
				int evalEnableWorkFlow = Integer.parseInt(actionRequest.getParameter("evalEnableWorkFlow"));
				int evalWorkFlowType = Integer.parseInt(actionRequest.getParameter("evalWorkFlowType"));
				String sql = "UPDATE stage_rules SET eval_workflow_type = ?, " +
											"eval_keyuser_mail2_leader=?, " +
											"eval_enable_workflow=?, " +
											"eval_workflow_type=? " +
										" WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, evalMail2Leader);
				ps.setInt(2, evalKeyUserMail2Leader);
				ps.setInt(3, evalEnableWorkFlow);
				ps.setInt(4, evalWorkFlowType);
				ps.setString(5, ef.examNo);
				ps.execute();
				//
				ef.evalMail2Leader = evalMail2Leader;
				ef.evalKeyUserMail2Leader = evalKeyUserMail2Leader;
				ef.evalEnableWorkFlow = evalEnableWorkFlow;
				ef.evalWorkFlowType = evalWorkFlowType;
			}else if(actionRequest.getParameter("postEnableWorkFlow") != null){
				int val = Integer.parseInt(actionRequest.getParameter("postEnableWorkFlow"));
				String sql = "UPDATE stage_rules SET post_enable_workflow = ? WHERE exam_no = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, val);
				ps.setString(2, ef.examNo);
				ps.execute();
				//
				ef.postEnableWorkFlow = val;
			}
			actionRequest.getPortletSession(true).setAttribute("examFlow", ef);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	

}

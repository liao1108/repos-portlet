<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    			height: 40px
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}
.button{
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}		
  </style>
  
  <portlet:defineObjects />
 
 <%
	String rootPaht = renderRequest.getContextPath();
%>
  
 <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	String msgText = "";
	if(renderRequest.getAttribute("msgText") != null){
		msgText = renderRequest.getAttribute("msgText").toString();
	}
	String strCap = renderRequest.getPortletSession(true).getAttribute("strCap").toString();
	String cap1 = String.valueOf(strCap.charAt(0));
	String cap2 = String.valueOf(strCap.charAt(1));
	String cap3 = String.valueOf(strCap.charAt(2));
	String cap4 = String.valueOf(strCap.charAt(3));
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="addMessageURL" name="addMessage">
</portlet:actionURL>

<portlet:actionURL var="doLogoutURL" name="doLogout">
</portlet:actionURL>




<portlet:resourceURL var="captchaURL"/>

<script src="<%=rootPaht%>/js/jquery-1.9.1.js" type="text/javascript"></script>

<table width="100%">
<tr><td>			  
<aui:form action="<%=addMessageURL%>" method="post" name="<portlet:namespace />fm">
<aui:layout>
	<img src="<%=renderRequest.getContextPath()%>/images/horse.gif"/>
</aui:layout>
<aui:layout>
	<aui:input name="userNo" label="使用者名稱" value="<%=user.getScreenName() %>" readonly="readonly"></aui:input>
</aui:layout>
<aui:layout>
	<aui:input type="textarea" name="msgText" label="留言內容"  value="<%=msgText %>" style="width: 100%;"  rows="8" />
</aui:layout>
<aui:layout>
	<aui:column>
		<aui:input type="text" name="capText" label="驗證碼"  />
    	</aui:column>
	<aui:column>
		<br/>
		<img src="<%=renderRequest.getContextPath()%>/images/<%=cap1 %>.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/<%=cap2 %>.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/<%=cap3 %>.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/<%=cap4 %>.png"/>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column>
		<aui:input type="submit"   name="submit"  label="" value="送出" cssClass="button" />
	</aui:column>
	<aui:column>
		<aui:button onClick="<%=doLogoutURL %>"  value="取消並登出" cssClass="button"/>
	</aui:column>
</aui:layout>
</aui:form>
</td></tr>
</table>
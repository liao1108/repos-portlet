package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class AddMessagePortlet
 */
public class AddMessagePortlet extends MVCPortlet {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
    
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    		requestPath = getInitParameter("requestPath");
	    		//
	    		/*
	    		Connection conn = ds.getConnection();
	    		String sql = "CREATE TABLE IF NOT EXISTS auditor_message (" +
										    		"msg_id INT NOT NULL AUTO_INCREMENT, " +
										    		"entry_time VARCHAR(30)," +		//避免多次Submit
												"user_no VARCHAR(50) NOT NULL, " +
												"user_email VARCHAR(30) NOT NULL," +
												"log_time  VARCHAR(20) NOT NULL," +
												"log_msg VARCHAR(2000) NOT NULL, " +
												"responsed INT(1)," +
												"PRIMARY KEY (msg_id))";
	    		PreparedStatement ps = conn.prepareStatement(sql);
	    		ps.execute();
	    		//
	    		conn.close();
	    		*/
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			//this.createTables();
			//取得驗證碼數字
			String strCap = "";
			Random dice = new Random();
			while(strCap.length() < 4){
				strCap += dice.nextInt(10);
			}
			if(strCap.length() > 4){
				strCap.substring(0, 4);
			}
			renderRequest.getPortletSession(true).setAttribute("strCap",strCap);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	public void addMessage(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			User user = themeDisplay.getUser();
			//
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			String msgText = ParamUtil.getString(actionRequest, "msgText");
			if(msgText == null || msgText.trim().equals("")){
				throw new Exception("請輸入留言內容。");
			}
			//
			String strCap = actionRequest.getPortletSession(true).getAttribute("strCap").toString();
			String capText = ParamUtil.getString(actionRequest, "capText");
			if(capText == null || !capText.equals(strCap)){
				throw new Exception("請輸入正確的驗證碼。");
			}
			//
			String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss" ;
			final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
			String dateTimeString =  sdf.format(new Date());
			
			Connection conn = ds.getConnection();
			//this.doPoke(conn, "auditor_message");
			
			String sql = "INSERT INTO auditor_message (user_no, user_email, log_time, log_msg) " +
									" VALUES (?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getScreenName());
			ps.setString(2, user.getEmailAddress());
			ps.setString(3, dateTimeString);
			ps.setString(4, msgText);
			ps.execute();
			//
			String[] dd = dateTimeString.split(" ")[0].split("-");
			
			String strMail = user.getScreenName() + " 您好，\n\n";
			strMail += "您於 " + dd[0] + " 年 " + dd[1] + " 月 " + dd[2] + " 日已成功留言至本局網站留言板，訊息如下：" + "\n\n";
			strMail += msgText + "\n\n";
			strMail += "本局將於處理完成後，以e-mail通知您，再請您上網瀏覽相關資訊，謝謝您！" + "\n\n";
			strMail += "金管會檢查局";
			
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URIBuilder builder = new URIBuilder();
			builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
							.setParameter("type", "SendMail")
							.setParameter("mailNo", user.getEmailAddress())
							.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
			URI uri = builder.build();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse response = httpClient.execute(httpPost);
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = br.readLine();
			//
			if(response.getEntity() != null ) {
				httpPost.abort();
			}
			
			//再發給檢查局人員
			//找出檢查局管理人員
			String strAccounts = "";
			Role roleFeb = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(), "FEB_S");
			if(roleFeb != null){
				List<User> listUser = UserLocalServiceUtil.getRoleUsers(roleFeb.getRoleId());
				for(User u: listUser){
					if(!strAccounts.equals("")) strAccounts += ";";
					strAccounts += u.getEmailAddress();
				}
			}
			if(strAccounts.equals("")) throw new Exception("檢查局管理人員未設定。");
			//
			String msg = msgText;
			if(msgText.length() > 100){
				msg = msgText.substring(0, 100);
				msg += "\n";
				msg += "....詳細內容請登入稽核人員留言版參考。";
			}
			//
			strMail = " 您好，\n\n";
			strMail += "代名 " + user.getScreenName() + " 之稽核人員已留言，內容為：\n\n";
			strMail += msg;
			strMail += "\n\n";
			strMail += "稽核人員留言板系統";
			//
			httpClient = new DefaultHttpClient();
			builder = new URIBuilder();
			//
			String[] ss = strAccounts.split(";");
			for(int i=0; i < ss.length; i++){
				builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
								.setParameter("type", "SendMail")
								.setParameter("mailNo", ss[i])
								.setParameter("mailText", URLEncoder.encode(strMail, "UTF-8"));
				uri = builder.build();
				httpPost = new HttpPost(uri);
				response = httpClient.execute(httpPost);
				br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				line = br.readLine();
				//
				if(response.getEntity() != null ) {
					httpPost.abort();
				}
			}
			//
			actionResponse.setRenderParameter("jspPage", "/html/addmessage/thanks.jsp");
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/addmessage/view.jsp");
		}
	}
	
	public void doLogout(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		actionResponse.sendRedirect("/c/portal/logout");
	}
	
	
	//
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
	        try {
	        	com.liferay.portal.kernel.captcha.CaptchaUtil.serveImage(resourceRequest, resourceResponse);
	        } catch (Exception ex) {
	        	ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			resourceRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resourceRequest, "error");
	        }
	}
	
	private void checkCaptcha(PortletRequest request, String enteredCaptchaText) throws Exception {
	        PortletSession session = request.getPortletSession();
	        String captchaText = getCaptchaValueFromSession(session);
	        if (Validator.isNull(captchaText)) {
	        	throw new Exception("Internal Error! Captcha text not found in session");
	        }
	        if (!captchaText.equals(enteredCaptchaText)) {
	        	throw new Exception("驗證碼輸入有誤。");
	        }
	}

	private String getCaptchaValueFromSession(PortletSession session) {
	        Enumeration<String> atNames = session.getAttributeNames();
	        while (atNames.hasMoreElements()) {
	        	String name = atNames.nextElement();
	        	if (name.contains("CAPTCHA_TEXT")) {
	        		return (String) session.getAttribute(name);
	        	}
	        }
	        return null;
	}
	
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
				if(str.contains("com.itez")){
					ret += ("\n" + str);
				}
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
	
	private void createTables() throws Exception{
		Connection conn = ds.getConnection();
		//
		boolean existed = false;
		String sql = "SELECT * FROM auditor_message";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		int numCol = meta.getColumnCount();
		for (int i = 1; i < numCol+1; i++) {
			if(meta.getColumnName(i).equals("responsed_status")){
				existed = true;
				break;
			}
		}
		if(!existed){
			sql = "ALTER TABLE auditor_message ADD responsed_status INT(1)";
			ps = conn.prepareStatement(sql);
			ps.execute();
			
		}
		//
		existed = false;
		for (int i = 1; i < numCol+1; i++) {
			if(meta.getColumnName(i).equals("published_status")){
				existed = true;
				break;
			}
		}
		if(!existed){
			sql = "ALTER TABLE auditor_message ADD published_status INT(1)";
			ps = conn.prepareStatement(sql);
			ps.execute();
		}
    		//
    		conn.close();
	}

	/*
	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}
	*/
}

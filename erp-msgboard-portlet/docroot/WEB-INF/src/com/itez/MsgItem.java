package com.itez;

import java.util.Hashtable;
import java.util.Vector;

public class MsgItem {
	public int msgId = 0;
	public String subject = "";
	public String publishUnit = "";
	public String publishBy = "";
	public String publishDate = "";
	public String toWhom = "";
	public String dueDate = "";
	public String publishContent = "";
	public String attachFileId = "";
	public String attachFileName = "";
	public String createdDate = "";
	public String attachIcon = "";
	//附檔
	public Vector<AttachItem> vecAttach = new Vector<AttachItem>();
	public int countAttach = 0;
	//可否發布
	public int canRelease = 0;
	public String strCanRelease = "否";
}

package com.itez;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;

import javax.activation.MimetypesFileTypeMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.http.HttpHeaders;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MsgBoardPortlet extends MVCPortlet {
	DataSource ds = null;
	
	String rootDocTemp = "";
	String rootAttach = "";				//附檔區
	//
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		Connection conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootDocTemp = rs.getString("folder_doc_temp");
	   			rootAttach = rs.getString("folder_attach");
	   		}
	   		rs.close();
	   		//
	   		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			boolean isAdmin = false;
			User user = themeDisplay.getUser();
			List<Role> userRoles = user.getRoles();
            for(Role r : userRoles){
                if("Administrator".equalsIgnoreCase(r.getName())){
                	isAdmin = true;
                	break;
                }
            }
            //
			conn = ds.getConnection();
			//
			String sortingBy = "publishDate";
			if(renderRequest.getPortletSession(true).getAttribute("sortingBy") != null){
				sortingBy = renderRequest.getPortletSession(true).getAttribute("sortingBy").toString();
			}else{
				String sql = "SELECT msg_sorting FROM user_prefer WHERE screen_name=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, themeDisplay.getUser().getScreenName());
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					String s = rs.getString(1);
					if(s.equals("到期日")){
						sortingBy = "dueDate";
					}else if(s.equals("主旨")){
						sortingBy = "subject";
					}else if(s.equals("發布單位")){
						sortingBy = "publishUnit";
					}
				}
				rs.close();
			}
			//Vector<MsgItem> vecMsg = this.getMsg(themeDisplay, conn, sortingBy);
			CaObj ca = this.getMsg(themeDisplay, conn, sortingBy);
			//載入組別
			//選擇組別與行業別
			//Folder fdTemplate = this.getDocTempRootFolder(themeDisplay);
			//if(fdTemplate == null) throw new Exception("檢查報告文件樣板區找不到！");
			//
			Vector<String> vecSecName = new Vector<String>();	//組別
			//
			vecSecName.add("金控公司組"); 
			vecSecName.add("本國銀行組"); 
			vecSecName.add("保險外銀組"); 
			vecSecName.add("證券票券組"); 
			vecSecName.add("地方金融組");
			vecSecName.add("檢查制度組");
			//
			renderRequest.getPortletSession(true).setAttribute("vecSecName", vecSecName);
			renderRequest.getPortletSession(true).setAttribute("vecMsg", ca.vecMsg);
			renderRequest.getPortletSession(true).setAttribute("vecMsgHand", ca.vecMsgHand);
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
			renderRequest.getPortletSession(true).setAttribute("sortingBy", sortingBy);
			//
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			if(isAdmin) {
				renderRequest.getPortletSession(true).setAttribute("isAdmin", 1);
			}
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	private CaObj getMsg(ThemeDisplay themeDisplay, Connection conn, String sortingBy) throws Exception{
		Vector<MsgItem> vecMsg = new Vector<MsgItem>();
		Vector<MsgItem> vecMsgHand = new Vector<MsgItem>();
		//
		String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
		//
		User user = themeDisplay.getUser();
		String strUserGroup = "";
		List<UserGroup> listUserGroup = user.getUserGroups();
		for(UserGroup g: listUserGroup){
			if(!strUserGroup.equals("")) strUserGroup += ",";
			strUserGroup += "'" + g.getName() + "'";
		}
		//
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Taipei"));
		String currDate = cal.get(Calendar.YEAR) + "-" + 
						String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
						String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
		//System.out.println("currDate: " + currDate);
		//
		String sql = "SELECT * FROM msg_board WHERE publish_by='" + userFullName + "'" +
								" OR ( " +
									" can_release = 1 "+ 
									" AND publish_date <= ? AND due_date >= ? " +
									" AND (to_whom = '全部' OR SUBSTR(to_whom, 3) IN (" + strUserGroup + ") ) " +
								" ) "; 
		if(strUserGroup.equals("")){
			sql = "SELECT * FROM msg_board WHERE publish_by='" + userFullName + "' " +
								" OR (" +
									" can_release = 1 "+ 
									" AND publish_date <= ? AND due_date >= ? " +
									" AND to_whom = '全部' " +
								") ";
		}
		if(sortingBy.equalsIgnoreCase("publishDate")){
			sql += " ORDER BY publish_date DESC";
		}else if(sortingBy.equalsIgnoreCase("dueDate")){
			sql += " ORDER BY due_date";
		}else if(sortingBy.equalsIgnoreCase("subject")){
			sql += " ORDER BY subject";
		}else if(sortingBy.equalsIgnoreCase("publishUnit")){
			sql += " ORDER BY publish_unit";
		}
		//System.out.println(sortingBy);
		//
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, currDate);
		ps.setString(2, currDate);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			MsgItem mi = new MsgItem();
			mi.msgId = rs.getInt("msg_id");
			mi.subject = rs.getString("subject");
			mi.publishUnit = rs.getString("publish_unit");
			mi.publishBy = rs.getString("publish_by");
			mi.publishDate = rs.getString("publish_date");
			mi.toWhom = rs.getString("to_whom");
			mi.dueDate = rs.getString("due_date");
			mi.publishContent = rs.getString("publish_content");
			if(rs.getString("attach_file_id") != null) mi.attachFileId = rs.getString("attach_file_id");
			if(rs.getString("attach_file_name") != null) mi.attachFileName = rs.getString("attach_file_name");
			mi.createdDate = rs.getString("created_date");
			mi.canRelease = rs.getInt("can_release");
			if(mi.canRelease == 1){
				mi.strCanRelease = "是";
			}
			//查看附檔
			Vector<AttachItem> vecAttach = this.getAttach(mi.msgId, conn);
			mi.vecAttach = vecAttach;
			mi.countAttach = vecAttach.size();
			//
			if(mi.canRelease == 1){
				vecMsg.add(mi);
			}else{
				vecMsgHand.add(mi);
			}
		}
		CaObj ca = new CaObj();
		ca.vecMsg = vecMsg;
		ca.vecMsgHand = vecMsgHand;
		//
		return ca;
	}
	
	private Vector<AttachItem> getAttach(int msgId, Connection conn) throws Exception{
		Vector<AttachItem> vecAttach = new Vector<AttachItem>(); 
		String sql = "SELECT * FROM msg_attach WHERE msg_id = ? ORDER BY file_name";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setInt(1, msgId);
		ResultSet rs  = ps.executeQuery();
		while(rs.next()){
			AttachItem ai = new AttachItem();
			ai.fileName = rs.getString("file_name");
			ai.msgId = msgId;
			//
			String iconName = "file.png";
			if(ai.fileName.toLowerCase().endsWith("xls")){
				iconName = "xls.png";
			}else if(ai.fileName.toLowerCase().endsWith("doc") || ai.fileName.toLowerCase().endsWith("docx")){
				iconName = "doc.png";
			}else if(ai.fileName.toLowerCase().endsWith("ppt")){
				iconName = "ppt.png";
			}else if(ai.fileName.toLowerCase().endsWith("pdf")){
				iconName = "pdf.png";
			}
			ai.iconName = iconName;
			//
			vecAttach.add(ai);
		}
		rs.close();
		return vecAttach;
	}
	
	//更新
	public void updateMsg(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		MsgItem mi = new MsgItem();
		boolean onHand = false;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			if(actionRequest.getParameter("onHand") != null && actionRequest.getParameter("onHand").equalsIgnoreCase("true")){
				onHand = true;
			}
			//
			mi.msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			mi.subject = actionRequest.getParameter("subject").trim();
			mi.publishContent = actionRequest.getParameter("publishContent").trim();
			mi.publishUnit = actionRequest.getParameter("publishUnit").trim();
			mi.publishDate = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearPublish"), actionRequest.getParameter("monPublish"), actionRequest.getParameter("dayPublish"));
			mi.dueDate = ErpUtil.genDateFromAUI(actionRequest.getParameter("yearDue"), actionRequest.getParameter("monDue"), actionRequest.getParameter("dayDue"));
			mi.toWhom = actionRequest.getParameter("toWhom").trim();
			mi.canRelease = Integer.parseInt(actionRequest.getParameter("canRelease"));
			mi.publishBy = userFullName;
			//
			if(mi.subject.equals("")) throw new Exception("請輸入主旨摘要。");
			if(mi.publishContent.equals("")) throw new Exception("請輸入公布內容。");
			if(mi.publishUnit.equals("")) throw new Exception("請輸入發布單位。");
			if(mi.toWhom.equals("")) throw new Exception("請輸入公布對象。");
			//
			conn = ds.getConnection();
			if(mi.msgId == 0){		//新增
				ErpUtil.logUserAction("新增公佈欄訊息", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//
				mi.createdDate = ErpUtil.getCurrDateTime();
				String sql = "INSERT INTO msg_board (subject, " +			//1
											" publish_content," +	//2
											" publish_unit," +		//3
											" to_whom," +			//4
											" publish_date," +		//5
											" due_date," +			//6
											" publish_by," +		//7
											" created_date," +		//8
											" can_release) " +		//9
										" VALUES (?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, mi.subject);
				ps.setString(2, mi.publishContent);
				ps.setString(3, mi.publishUnit);
				ps.setString(4, mi.toWhom);
				ps.setString(5, mi.publishDate);
				ps.setString(6, mi.dueDate);
				ps.setString(7, userFullName);
				ps.setString(8, mi.createdDate);
				ps.setInt(9, mi.canRelease);
				ps.execute();
				//找回 msgId
				sql = "SELECT msg_id FROM msg_board WHERE created_date=? AND subject=? AND publish_content=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, mi.createdDate);
				ps.setString(2, mi.subject);
				ps.setString(3, mi.publishContent);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					mi.msgId = rs.getInt(1);
				}
			}else{
				ErpUtil.logUserAction("更新公佈欄訊息", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
				//
				String sql = "UPDATE msg_board SET subject=?, " +			//1
										" publish_content=?," +		//2
										" publish_unit=?," +			//3
										" to_whom=?," +			//4
										" publish_date=?, " +		//5
										" due_date=?," +			//6
										" publish_by=?, " +			//7
										" can_release=? " +			//8
								" WHERE msg_id = ?  ";				//9
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, mi.subject);
				ps.setString(2, mi.publishContent);
				ps.setString(3, mi.publishUnit);
				ps.setString(4, mi.toWhom);
				ps.setString(5, mi.publishDate);
				ps.setString(6, mi.dueDate);
				ps.setString(7, userFullName);
				ps.setInt(8, mi.canRelease);
				ps.setInt(9, mi.msgId);
				ps.execute();
			}
			//附加檔案
			//查看附檔
			Vector<AttachItem> vecAttach = this.getAttach(mi.msgId, conn);
			mi.vecAttach = vecAttach;
			mi.countAttach = vecAttach.size();
			//
			actionRequest.getPortletSession(true).setAttribute("currMsg", mi);
			//
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		if(onHand){
			actionResponse.setRenderParameter("onHand", "true");
		}else{
			actionResponse.setRenderParameter("onHand", "false");
		}
		actionResponse.setRenderParameter("jspPage", "/html/msgboard/editMsg.jsp");
	}
	
	//返回動作(須更新畫面)
	public void goBackAndRefresh(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
	}

	
	//附加檔案上傳
	public void uploadAttachFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
	       	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	       	//
	       	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	       	//
	       	int msgId = Integer.parseInt(ParamUtil.getString(uploadRequest,"msgId"));
	       	//
	       	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要上傳或更新的檔案。");
	       	//
		    String sourceFileName = uploadRequest.getFileName("fileName");
		    //File file = uploadRequest.getFile("fileName");
		    InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
		    MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	        //
		    String destName = String.valueOf(msgId) + "_" + sourceFileName;
		    //		
		    conn = ds.getConnection();
		    Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			if(alfSessionAdmin == null) throw new Exception("無法取得 Alfresco 管理者連線！");
			//
	        AlfrescoFolder fdAttach = this.getAttachFolder(alfSessionAdmin);
			if(fdAttach == null) throw new Exception("後台附檔區尚未設定。");
			//
			AlfrescoDocument alfDoc = null;
			for(CmisObject co: fdAttach.getChildren()){
				if(!(co instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
				//
				if(co.getName().equals(destName)){
					alfDoc = (AlfrescoDocument)co;
					break;
				}
			}
			//
			if(alfDoc != null) alfDoc.delete();
			//
			//新增
			ContentStream cs = new ContentStreamImpl(destName, null, mimeTypesMap.getContentType(sourceFileName), is);
			//
			Map<String, String> props = new HashMap<String,String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
			props.put(PropertyIds.NAME, destName);
			//
			fdAttach.createDocument(props, cs, VersioningState.MAJOR);
			//
			is.close();
	        //
	        
	        ErpUtil.logUserAction("上傳公佈欄訊息附檔", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        //
	        String sql = "SELECT * FROM msg_attach WHERE msg_id=? AND file_name=?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setInt(1, msgId);
	        ps.setString(2, sourceFileName);
	        ResultSet rs = ps.executeQuery();
	        if(!rs.next()){
	        	sql = "INSERT INTO msg_attach (msg_id, file_name) VALUES (?,?)";
	        	ps = conn.prepareStatement(sql);
	        	ps.setInt(1, msgId);
	        	ps.setString(2, sourceFileName);
	        	ps.execute();
	        }
	        Vector<AttachItem> vecAttach = this.getAttach(msgId, conn);
	        MsgItem cm = (MsgItem)actionRequest.getPortletSession(true).getAttribute("currMsg");
	        cm.vecAttach = vecAttach;
	        actionRequest.getPortletSession(true).setAttribute("currMsg", cm);
	        //
	        conn.close();
	    }catch(Exception ex){
	       	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	       	//ex.printStackTrace();
	    	String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	    }finally{
	       	try{
	       		if(conn != null) conn.close();
	       	}catch(Exception _ex){
	       	}
	    }
		actionResponse.setRenderParameter("jspPage", "/html/msgboard/editMsg.jsp");
	}
	
	/*
	public void uploadAttachFile_Liferay(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	    Connection conn = null;
		try{
	       	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	       	//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	       	//
	       	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	       	//
	       	int msgId = Integer.parseInt(ParamUtil.getString(uploadRequest,"msgId"));
	       	//
	       	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要更新的檔案。");
	       	//
		    String sourceFileName = uploadRequest.getFileName("fileName");
		    File file = uploadRequest.getFile("fileName");
		    InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
		    MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
	        //
		    String destName = String.valueOf(msgId) + "#" + sourceFileName;
		    //		
	        Folder fdAttach = null;
			try{
				fdAttach = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootAttach);
			}catch(Exception _ex){
			}
			if(fdAttach == null) throw new Exception("附檔區尚未設定。");
			//
			FileEntry fe = null;
			try{
				fe = DLAppLocalServiceUtil.getFileEntry(themeDisplay.getScopeGroupId(), 
														fdAttach.getFolderId(),
														destName);
			}catch(Exception _ex){
			}
			//
			if(fe != null) DLAppLocalServiceUtil.deleteFileEntry(fe.getFileEntryId());
			//
			//新增
			fe = DLAppLocalServiceUtil.addFileEntry(themeDisplay.getUserId(),
												themeDisplay.getScopeGroupId(),
												fdAttach.getFolderId(),
												destName,
												mimeTypesMap.getContentType(file),
												destName,
												"", 
												null,
												IOUtils.toByteArray(is), 
												new ServiceContext());
			//}else{
			//	fe = DLAppLocalServiceUtil.updateFileEntry(themeDisplay.getUserId(),
			//									fe.getFileEntryId(),
			//									destName,
			//									fe.getMimeType(),
			//									destName,
			//									"",
			//									null,
			//									false,
			//									IOUtils.toByteArray(is),
			//									new ServiceContext());
			//}
	        is.close();
	        //
	        conn = ds.getConnection();
	        ErpUtil.logUserAction("上傳公佈欄訊息附檔", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        //
	        String sql = "SELECT * FROM msg_attach WHERE msg_id=? AND file_name=?";
	        PreparedStatement ps = conn.prepareStatement(sql);
	        ps.setInt(1, msgId);
	        ps.setString(2, sourceFileName);
	        ResultSet rs = ps.executeQuery();
	        if(!rs.next()){
	        	sql = "INSERT INTO msg_attach (msg_id, file_name) VALUES (?,?)";
	        	ps = conn.prepareStatement(sql);
	        	ps.setInt(1, msgId);
	        	ps.setString(2, sourceFileName);
	        	ps.execute();
	        }
	        Vector<AttachItem> vecAttach = this.getAttach(msgId, conn);
	        MsgItem cm = (MsgItem)actionRequest.getPortletSession(true).getAttribute("currMsg");
	        cm.vecAttach = vecAttach;
	        actionRequest.getPortletSession(true).setAttribute("currMsg", cm);
	        //
	        conn.close();
	    }catch(Exception ex){
	       	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	       	//ex.printStackTrace();
	    	String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	    }finally{
	       	try{
	       		if(conn != null) conn.close();
	       	}catch(Exception _ex){
	       	}
	    }
		actionResponse.setRenderParameter("jspPage", "/html/msgboard/editMsg.jsp");
	}
	*/
	
	public void doRemoveAttach(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileName = actionRequest.getParameter("fileName");
			int msgId = Integer.parseInt(actionRequest.getParameter("msgId"));
			//
			String destName = msgId + "_" + fileName;
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			if(alfSessionAdmin == null) throw new Exception("無法取得 Alfresco 管理者連線！");
			//
	        AlfrescoFolder fdAttach = this.getAttachFolder(alfSessionAdmin);
			if(fdAttach == null) throw new Exception("後台附檔區尚未設定。");
			//
			AlfrescoDocument alfDoc = null;
			for(CmisObject co: fdAttach.getChildren()){
				if(!(co instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
				//
				if(co.getName().equals(destName)){
					alfDoc = (AlfrescoDocument)co;
					break;
				}
			}
			//
			if(alfDoc != null) alfDoc.delete();
			//
			ErpUtil.logUserAction("刪除公佈欄訊息附檔", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "DELETE FROM msg_attach WHERE msg_id=? AND file_name=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, msgId);
			ps.setString(2, fileName);
			ps.execute();
			//
			Vector<AttachItem> vecAttach = this.getAttach(msgId, conn);
		    MsgItem cm = (MsgItem)actionRequest.getPortletSession(true).getAttribute("currMsg");
		    cm.vecAttach = vecAttach;
		    actionRequest.getPortletSession(true).setAttribute("currMsg", cm);
	        //
			conn.close();
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		actionResponse.setRenderParameter("jspPage", "/html/msgboard/editMsg.jsp");
	}
	
	public AlfrescoFolder getAttachFolder(Session alfSession) throws Exception{
		AlfrescoFolder ret = null;
		//
		for(CmisObject co: alfSession.getRootFolder().getChildren()){
			if(!(co instanceof org.apache.chemistry.opencmis.client.api.Folder)) continue;
			//
			if(co.getName().contains("附檔區")){
				ret = (AlfrescoFolder)co;
				break;
			 }
		 }
		 return ret;
	}	
	
	//執行排序
	public void doSorting(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			String sortingBy = actionRequest.getParameter("sortingBy");
			if(sortingBy.equals("")) throw new Exception("請輸入排序種類。");
			
			actionRequest.getPortletSession(true).setAttribute("sortingBy", sortingBy);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			ErpUtil.logUserAction("下載公佈欄訊息附檔", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
			//
			String msgId = resRequest.getParameter("msgId");
			String fileName = java.net.URLDecoder.decode(resRequest.getParameter("fileName"), "UTF-8");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			if(alfSessionAdmin == null) throw new Exception("無法取得 Alfresco 管理者連線！");
			//
	        AlfrescoFolder fdAttach = this.getAttachFolder(alfSessionAdmin);
			if(fdAttach == null) throw new Exception("後台附檔區尚未設定。");
			//
			String destName = msgId + "_" + fileName;
			AlfrescoDocument alfDoc = null;
			for(CmisObject co: fdAttach.getChildren()){
				if(!(co instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
				//
				if(co.getName().equals(destName)){
					alfDoc = (AlfrescoDocument)co;
					break;
				}
			}
			if(alfDoc == null) return;
			//
			MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
			//
			resResponse.setContentType(mimeTypesMap.getContentType(fileName));
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			//
			java.io.InputStream is = alfDoc.getContentStream().getStream();
			byte[] buffer = new byte[4096];
			int n;
			while ((n = is.read(buffer)) > 0) {
			    out.write(buffer, 0, n);
			}
			out.flush();
			out.close();
			//
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	//取得在 Liferay 的報表範本區
	//public Folder getDocTempRootFolder(ThemeDisplay themeDisplay) throws Exception{
	//	Folder ret = null;
	//	//
	//	try{
	//		ret = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootDocTemp);
	//	}catch(Exception ex){
	//	}
	//	return ret;
	//}
}

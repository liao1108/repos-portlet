<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>
<%@ page import= "com.itez.MsgItem"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   		Vector<MsgItem> vecMsg = new Vector<MsgItem>();
            if(renderRequest.getPortletSession(true).getAttribute("vecMsg") != null){
            	vecMsg = (Vector<MsgItem>)renderRequest.getPortletSession(true).getAttribute("vecMsg");
            }
		Vector<MsgItem> vecMsgHand = new Vector<MsgItem>();
            if(renderRequest.getPortletSession(true).getAttribute("vecMsgHand") != null){
            	vecMsgHand = (Vector<MsgItem>)renderRequest.getPortletSession(true).getAttribute("vecMsgHand");
            }
            //
            String sortingBy = "";
            if(renderRequest.getPortletSession(true).getAttribute("sortingBy") != null){
            	sortingBy = renderRequest.getPortletSession(true).getAttribute("sortingBy").toString();
            }
		//
		int rowsPrefer = 20;
		if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
			 rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
		}
            //	
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
              	
             String successMsg = "";	
             if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="goEditMsgURL" >
	<portlet:param name="jspPage" value="/html/msgboard/editMsg.jsp"/>
	<portlet:param name="msgId" value="0"/>
</portlet:renderURL>

<portlet:actionURL var="doSortingURL" name="doSorting" >
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
		<img src="<%=renderRequest.getContextPath()%>/images/msg32.png" >&nbsp;檢查人員專用布告欄
	</aui:column>
</aui:layout>
<hr/>

<aui:layout>
	<aui:column>
		<aui:button onClick="<%=goEditMsgURL.toString()%>" value="新增公布訊息"/>
	</aui:column>
	<aui:column>
		&nbsp;&nbsp;
	</aui:column>
	
	<%if(vecMsg.size() > 0){ %>
		<aui:form action="<%=doSortingURL%>" method="post" name="<portlet:namespace />fm">
			<aui:column>排序方式：</aui:column>
			<aui:column><aui:input name="sortingBy" type="radio"  label="公布時間"  value="publishDate"  checked='<%=sortingBy.equalsIgnoreCase("publishDate") %>'/></aui:column>
			<aui:column><aui:input name="sortingBy" type="radio"  label="到期日"  value="dueDate"  checked='<%=sortingBy.equalsIgnoreCase("dueDate") %>'/></aui:column>
			<aui:column><aui:input name="sortingBy" type="radio"  label="主旨"  value="subject"  checked='<%=sortingBy.equalsIgnoreCase("subject") %>'/></aui:column>
			<aui:column><aui:input name="sortingBy" type="radio"  label="發布單位"  value="publishUnit" checked='<%=sortingBy.equalsIgnoreCase("publishUnit") %>'/></aui:column>
			<aui:column><aui:button type="submit" value="重排" /></aui:column>
		</aui:form>
	<%} %>		
</aui:layout>

<br/>

<liferay-ui:search-container emptyResultsMessage="目前沒有布告欄訊息。"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMsg, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMsg.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.MsgItem" keyProperty="msgId" modelVar="mi">
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/msg24.png"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="公布日期" property="publishDate" />
		<liferay-ui:search-container-column-text name="主旨" property="subject" />
		<liferay-ui:search-container-column-text name="發布單位" property="publishUnit"/>
		<liferay-ui:search-container-column-text name="發布人" property="publishBy"/>
		<liferay-ui:search-container-column-text name="發布對象" property="toWhom"/>
		<liferay-ui:search-container-column-text name="到期日期" property="dueDate" />
		<liferay-ui:search-container-column-text name="附檔數"  property="countAttach"  align="center"/>
		<liferay-ui:search-container-column-text name="建立日期" property="createdDate" />
				
		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<%if(vecMsgHand.size() > 0){%>
<br>
<aui:layout>
	<aui:column cssClass="header">
		您已建檔尚未發布的訊息
	</aui:column>
</aui:layout>

<liferay-ui:search-container>
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecMsgHand, searchContainer.getStart(), searchContainer.getEnd());
			total = vecMsgHand.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.MsgItem" keyProperty="msgId" modelVar="mih">
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/msg24.png"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="公布日期" property="publishDate" />
		<liferay-ui:search-container-column-text name="主旨" property="subject" />
		<liferay-ui:search-container-column-text name="發布單位" property="publishUnit"/>
		<liferay-ui:search-container-column-text name="發布人" property="publishBy"/>
		<liferay-ui:search-container-column-text name="發布對象" property="toWhom"/>
		<liferay-ui:search-container-column-text name="到期日期" property="dueDate" />
		<liferay-ui:search-container-column-text name="附檔數"  property="countAttach"  align="center"/>
		<liferay-ui:search-container-column-text name="建立日期" property="createdDate" />
		<liferay-ui:search-container-column-text name="可發布" property="strCanRelease"  align="center"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminHand.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<%}%>
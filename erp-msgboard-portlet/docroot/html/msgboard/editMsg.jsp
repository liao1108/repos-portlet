<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>

<%@ page import= "com.itez.MsgItem"%>
<%@ page import= "com.itez.AttachItem"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />


  <%
  	boolean onHand = false;
  	if(renderRequest.getParameter("onHand") != null){
  		if(renderRequest.getParameter("onHand").equalsIgnoreCase("true")){
  			onHand = true;
  		}
  	}
  	//
    MsgItem mi  = new MsgItem();
    if(renderRequest.getParameter("msgId") != null){
    	int msgId = Integer.parseInt(renderRequest.getParameter("msgId"));
             		
	   	Vector<MsgItem> vecMsg = new Vector<MsgItem>();
	   	if(onHand){
	   		if(renderRequest.getPortletSession(true).getAttribute("vecMsgHand") != null){
	   			vecMsg = (Vector<MsgItem>)renderRequest.getPortletSession(true).getAttribute("vecMsgHand");
	   		}
	   	}else{
    		if(renderRequest.getPortletSession(true).getAttribute("vecMsg") != null){
     			vecMsg= (Vector<MsgItem>)renderRequest.getPortletSession(true).getAttribute("vecMsg");
    		}
	   	}
    	//
    	for(MsgItem _m: vecMsg){
    		if(_m.msgId == msgId){
    			mi = _m;
    			break;
    		}
     	}
    }else if(renderRequest.getPortletSession(true).getAttribute("currMsg") != null){
    	mi = (MsgItem)renderRequest.getPortletSession(true).getAttribute("currMsg");
    }
    renderRequest.getPortletSession(true).setAttribute("currMsg", mi);
    //
    Vector<AttachItem> vecAttach = mi.vecAttach;
    //組別
    Vector<String> vecSecName = new Vector<String>();
    if(renderRequest.getPortletSession(true).getAttribute("vecSecName") != null){
    	vecSecName= (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecName");
    }
    //
    if(mi.publishDate.equals("")){
    	mi.publishDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    }
             	
    if(mi.dueDate.equals("")){
    	mi.dueDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
    }
             	
    String userFullName = "";
    if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
    	userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
    }
    boolean isAdmin = false;
    if(renderRequest.getPortletSession(true).getAttribute("isAdmin") != null){
    	isAdmin = true;
    }
    boolean isPublisher = false;
    if(mi.publishBy.equalsIgnoreCase(userFullName) || mi.publishBy.contains("admin") || isAdmin){
    	isPublisher = true;
    }
    //
    String errMsg = "";	
    if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
             	
    String successMsg = "";	
    if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateMsgURL"  name="updateMsg" >
</portlet:actionURL>		

<portlet:actionURL var="doReleaseMsgURL"  name="doReleaseMsg" >
	<portlet:param name="msgId" value="<%=String.valueOf(mi.msgId) %>"/>
</portlet:actionURL>		

<portlet:actionURL var="goBackAndRefreshURL" name="goBackAndRefresh" >
</portlet:actionURL>		

<portlet:actionURL var="uploadAttachFileURL"  name="uploadAttachFile" >
</portlet:actionURL>	

<aui:layout>
	<aui:column cssClass="header">
	 	<%if(mi.msgId == 0){ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/msg32.png">&nbsp;建立新公布訊息
	 	<%}else{ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/msg32.png">&nbsp;修改公布訊息
	 	<%} %>
	</aui:column>
</aui:layout>
<hr/>

<aui:fieldset >
<aui:form action="<%=updateMsgURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column cssClass="td">
			<%if(onHand){ %>
				<aui:input type="hidden" name="onHand"  value="true"/>
			<%}else{ %>
				<aui:input type="hidden" name="onHand"  value="false"/>
			<%} %>
			<aui:input type="hidden" name="msgId"  value="<%=mi.msgId %>"/>
			
			<%if(isPublisher || mi.msgId == 0){ %>
				<aui:input name="subject" label="主旨" value="<%=mi.subject %>"  size="100"  />
			<%}else{ %>	
				<span style="color:dimgray">主旨：</span>
				<br>
				<span style="color:black"><%=mi.subject %></span>
				<br>
				<br>
			<%} %>	
		</aui:column>
	</aui:layout>
			
	<aui:layout>
		<aui:column  cssClass="td"  columnWidth="90">
			<%if(isPublisher || mi.msgId == 0){ %>
				<aui:input type="textarea" name="publishContent" label="發布內容"  value="<%=mi.publishContent %>" style="width: 100%;"  rows="6" />
			<%}else{ %>
				<span style="color:dimgray">發布內容：</span>
				<br>
				<span style="color:black"><%=mi.publishContent %></span>
				<br>
				<br>
			<%} %>		
		</aui:column>
	</aui:layout>	
	
	<aui:layout>
		<aui:column cssClass="td">
			<%if(isPublisher || mi.msgId == 0){ %>
				<aui:input name="publishUnit" label="發布單位" value="<%=mi.publishUnit%>" />
			<%}else{ %>
				<span style="color:dimgray">發布單位：</span>
				<br>
				<span style="color:black"><%=mi.publishUnit%></span>
				<br>
				<br>
			<%} %>	
		</aui:column>
		<aui:column cssClass="td">
			<%if(isPublisher || mi.msgId == 0){ %>
				<aui:select name="toWhom"  label="發布對象"  cssClass="td">
					<aui:option value="全部" selected='<%=mi.toWhom.equals("全部") %>'>全部</aui:option>
					<%for(String sn: vecSecName){	%>
						<aui:option value="<%=sn%>"  selected="<%=mi.toWhom.equals(sn) %>"><%=sn %></aui:option>
					<%} %>
				</aui:select>
			<%}else{ %>
				<span style="color:dimgray">發布對象：</span>
				<br>
				<span style="color:black"><%=mi.toWhom %></span>	
				<br>
				<br>		
			<%} %>		
		</aui:column>
	</aui:layout>

<%if(isPublisher || mi.msgId == 0){ %>
	<aui:layout>
		<aui:column cssClass="td">
			可否發布：
		</aui:column>
		<aui:column cssClass="td">
			<%if(mi.canRelease == 1){ %>
				<aui:input name="canRelease" type="radio"  value="1" label="是" checked="true"/>
                  	<aui:input name="canRelease" type="radio"  value="0" label="否" />
	            <%}else{ %>
				<aui:input name="canRelease" type="radio"  value="1" label="是" />
	                  <aui:input name="canRelease" type="radio"  value="0" label="否"  checked="true"/>
	            <%} %>
		</aui:column>
	</aui:layout>
<%} %>
	
	<aui:layout>
	
		<aui:column cssClass="td">
			<%if(isPublisher || mi.msgId == 0){ %>
				<h4>發布日期</h4>
				<liferay-ui:input-date dayParam="dayPublish"  monthParam="monPublish"  yearParam="yearPublish"
										 yearValue="<%=ErpUtil.getYear(mi.publishDate) %>"  monthValue="<%=ErpUtil.getMonth(mi.publishDate) - 1%>"  dayValue="<%=ErpUtil.getDay(mi.publishDate) %>"
										yearRangeStart="<%=ErpUtil.getYear(mi.publishDate)  %>" yearRangeEnd="<%=ErpUtil.getYear(mi.publishDate) + 1%>"/>
			<%}else{ %>
				<span style="color:dimgray">發布日期：</span>
				<br>
				<span style="color:black"><%=mi.publishDate %></span>	
				<br>
			<%} %>								
		</aui:column>
		
		<aui:column cssClass="td">
			<%if(isPublisher || mi.msgId == 0){ %>
				<h4>到期日期</h4>
				<liferay-ui:input-date dayParam="dayDue"  monthParam="monDue"  yearParam="yearDue"
										 yearValue="<%=ErpUtil.getYear(mi.dueDate) %>"  monthValue="<%=ErpUtil.getMonth(mi.dueDate) - 1%>"  dayValue="<%=ErpUtil.getDay(mi.dueDate) %>"
										yearRangeStart="<%=ErpUtil.getYear(mi.dueDate)  %>" yearRangeEnd="<%=ErpUtil.getYear(mi.dueDate) + 10%>"/>
			<%}else{ %>
				<span style="color:dimgray">到期日期：</span>
				<br>
				<span style="color:black"><%=mi.dueDate %></span>	
				<br>
			<%} %>
		</aui:column>
	</aui:layout>
	
	<%if(mi.msgId > 0){ %>
		<aui:layout>
			<aui:column cssClass="td">
				<%if(isPublisher || mi.msgId == 0){ %>
					<aui:input name="publishBy" label="發布人" value="<%=mi.publishBy%>" />
				<%}else{ %>
					<br>	
					<span style="color:dimgray">發布人：</span>
					<br>
					<span style="color:black"><%=mi.publishBy%></span>
					<br>
				<br>						
				<%} %>	
			</aui:column>
			<aui:column cssClass="td">
				<%if(isPublisher || mi.msgId == 0){ %>
					<aui:input name="createdDate" label="建立時間" value="<%=mi.createdDate %>" />
				<%}else{ %>
					<br>	
					<span style="color:dimgray">建立時間：</span>
					<br>
					<span style="color:black"><%=mi.createdDate %></span>
					<br>
				<%} %>	
			</aui:column>
		</aui:layout>
	<%} %>

	<aui:button-row>
		<aui:button onClick="<%=goBackAndRefreshURL.toString() %>"    value="返回上頁" />
		<%if(mi.msgId == 0){ %>
			<aui:button type="submit"   value="確定建立" />
		<%}else if(isPublisher){ %>
			<aui:button type="submit"   value="確定更新" />
		<%} %>
	</aui:button-row>
</aui:form>
</aui:fieldset>

<%if(mi.msgId > 0){ %>
	<br>
	<aui:layout>
		<aui:column cssClass="header">
	 		<img src="<%=renderRequest.getContextPath()%>/images/attach32.png">&nbsp;附加檔案
		</aui:column>
	</aui:layout>
	<hr/>
	
	<%if(isPublisher){ %>
	<aui:form action="<%=uploadAttachFileURL%>"  enctype="multipart/form-data" method="post" >
		<aui:layout>
			<aui:column>
				<aui:input type="hidden" name="msgId"  value="<%=mi.msgId %>"/>
				<aui:input type="file" name="fileName" label="請指定所要附加的檔案" size="100"/>
			</aui:column>
			<aui:column>
				<br/>
				<aui:button type="submit" value="開始上傳" />
			</aui:column>
		</aui:layout>
	</aui:form>
	<%} %>
		
	<liferay-ui:search-container emptyResultsMessage="目前沒有附加檔案。" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecAttach, searchContainer.getStart(), searchContainer.getEnd());
			total = vecAttach.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.AttachItem" keyProperty="fileName" modelVar="ai">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="msgId"  value="<%=String.valueOf(ai.msgId) %>"/>
  			<portlet:param name="fileName"  value="<%= java.net.URLEncoder.encode(ai.fileName, \"UTF-8\") %>"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=ai.iconName %>"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="附檔名" property="fileName" href="<%=downloadFileURL.toString() %>"/>
		
		<%if(isPublisher){ %>
			<liferay-ui:search-container-column-jsp path="/html/admin/adminAttach.jsp" align="center" />
		<%} %>	
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
	
<%} %>	
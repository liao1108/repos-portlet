<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.AttachItem" %>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	AttachItem ai = (AttachItem)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:actionURL var="doRemoveAttachURL" name="doRemoveAttach" >
		<portlet:param name="msgId" value="<%=String.valueOf(ai.msgId) %>"/>
		<portlet:param name="fileName" value="<%=java.net.URLEncoder.encode(ai.fileName, \"UTF-8\") %>"/>
	</portlet:actionURL>
	
	<liferay-ui:icon image="delete" message="刪除" url="<%=doRemoveAttachURL.toString() %>"/>
</liferay-ui:icon-menu>
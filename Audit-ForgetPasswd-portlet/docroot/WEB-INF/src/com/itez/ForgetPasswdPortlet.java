package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.sql.DataSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ForgetPasswdPortlet extends MVCPortlet {
	DataSource ds = null;
	String httpHost = "";
	int httpPort = 80;
	String requestPath = "";
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		httpHost = getInitParameter("httpHost");
			httpPort = Integer.parseInt(getInitParameter("httpPort"));
	    		requestPath = getInitParameter("requestPath");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	//
	public void doForgetPasswd(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//
			ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String userNo = "", userName="", eMail = "";
			if(actionRequest.getParameter("userNo") != null){
				userNo = this.cleanXSS(actionRequest.getParameter("userNo")).trim();
			}
			if(actionRequest.getParameter("userName") != null){
				userName = this.cleanXSS(actionRequest.getParameter("userName")).trim();
			}
			if(actionRequest.getParameter("eMail") != null){
				eMail = this.cleanXSS(actionRequest.getParameter("eMail")).trim();
			}
			if(userNo.equals("") || userName.equals("") || eMail.equals("")){
				throw new Exception("請完整輸入使用帳號、姓名以及電子郵件帳號。");
			}
			//
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//User user = UserLocalServiceUtil.getUser(themeDisplay.getUserId());
			//
			User user = null;
			try{
				user = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
			}catch(Exception _ex){
			}
			if(user == null){
				throw new Exception("使用者資料庫找不到帳號為'" + userNo + "'的用戶，請洽檢查局資訊室管理人員。");
			}
			//
			boolean goOn = false;
			if( (user.getFirstName() + user.getLastName()).contains(userName)
					|| (user.getLastName() + user.getFirstName()).contains(userName)) goOn = true;
			if(!goOn) throw new Exception("您的使用帳號與姓名不相符。");
			//
			if(!user.getEmailAddress().equalsIgnoreCase(eMail)) throw new Exception("您的使用帳號與電子郵件不相符。");
			//
			String uuid = UUID.randomUUID().toString();
			if(uuid.contains("-")) uuid = uuid.split("-")[0];
			//
			uuid = "A" + uuid;
			//
			UserLocalServiceUtil.updatePassword(user.getUserId(), uuid, uuid, false);
			//
			String strBody = "您好：\n" +
   								"您於本局稽核人員留言板的新密碼為 " + uuid + " 。\n" +
   								"\n金管會檢查局";
			//
			DefaultHttpClient httpClient = new DefaultHttpClient();
			URIBuilder builder = new URIBuilder();
			builder.setScheme("http").setHost(httpHost).setPort(httpPort).setPath(requestPath)
							.setParameter("type", "SendMail")
							.setParameter("mailNo", eMail)
							.setParameter("mailText", URLEncoder.encode(strBody, "UTF-8"));
			URI uri = builder.build();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse response = httpClient.execute(httpPost);
			BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = br.readLine();
			//
			if(response.getEntity() != null ) {
				httpPost.abort();
			}
			//
			SessionMessages.add(actionRequest, "success");
			//actionRequest.setAttribute("successMsg", "您已成功註冊稽核人員留言板，請點選登入按鈕登入本系統。");
			actionRequest.setAttribute("successMsg", "");
			//
			actionResponse.setRenderParameter("jspPage", "/html/forgetpasswd/sentOk.jsp");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	 
	private String cleanXSS(String value){
		String ret = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		ret = ret.replaceAll("\\(", "& #40;").replaceAll("\\)", "&#41;");
		ret = ret.replaceAll("'", "& #39;");
		ret = ret.replaceAll("eval\\((.*)\\)", "");
		ret = ret.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		ret = ret.replaceAll("script", "");
        return ret;
	}
	 
	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}

	private void doPoke(Connection conn, String table){
		try{
			if(conn == null) conn = ds.getConnection();
			//
			String sql = "SELECT  1 FROM " + table + " LIMIT 1";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.executeQuery();
		}catch(Exception ex){
		}
	}

}

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:微軟正黑體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    		height: 40px
    		}
  	td {
    			font-family:微軟正黑體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:red;
    		}		
  </style>

<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	long groupId = themeDisplay.getScopeGroupId();
	long userId = themeDisplay.getUserId();
			
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>


<portlet:actionURL var="doForgetPasswdURL" name="doForgetPasswd">
</portlet:actionURL>

<aui:fieldset>
<aui:form action="<%=doForgetPasswdURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
			<img src='<%=renderRequest.getContextPath()%>/images/forget.png'>
		</aui:column>
		<aui:column>
			<aui:layout>
      			<aui:column>
      				<aui:input type="text" name="userNo" label="使用者名稱" />
      			</aui:column>
      		</aui:layout>
			<aui:layout>
      			<aui:column>
      				<aui:input type="text" name="userName" label="姓名" />
      			</aui:column>
      		</aui:layout>
      		<aui:layout>	
      			<aui:column>
      				<aui:input type="text" name="eMail" label="eMail"/>
      			</aui:column>
      		</aui:layout>
      		<aui:layout>		
      			<aui:column>
      				<aui:input type="submit" name="submit" label="" value="送出密碼"/>
      			</aui:column>
      		</aui:layout>	
		</aui:column>
	</aui:layout>
	
</aui:form>
</aui:fieldset>

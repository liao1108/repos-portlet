<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.HtmlUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.TimeZone" %>

<%@ page import="com.itez.feb.LogRec" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay" %>

<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	_tr  {
    			height: 35x
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}
	select {
    			font-family:標楷體;
			font-size:12pt;
    		}	
  </style>
  
<%
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

  	Vector<LogRec> vecLog = new Vector<LogRec>();
  	if(renderRequest.getPortletSession(true).getAttribute("vecLog") != null){
  		vecLog = (Vector<LogRec>)renderRequest.getPortletSession(true).getAttribute("vecLog");
  	}
  	//
  	sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
	String logDate = sdf.format(new java.util.Date());
	if(renderRequest.getPortletSession(true).getAttribute("logDate") != null){
  		logDate = renderRequest.getPortletSession(true).getAttribute("logDate").toString();
	}
  	//
  	String errMsg = "";	
  	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
  	
  	String successMsg = "";	
  	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
	
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="queryLogByDateURL" name="queryLogByDate">
</portlet:actionURL>

<aui:form action="<%=queryLogByDateURL%>" method="post" name="<portlet:namespace />fm" >
	<aui:layout>
		<aui:column>
			<aui:input type="text" name="logDate" value="<%=logDate%>" label="請輸入查詢日期(yyyy-MM-dd)：" inlineLabel="left"/>
		</aui:column>
		<aui:column>
			<aui:button type="submit" name="submit" value="查詢登入記錄" cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>

<liferay-ui:search-container emptyResultsMessage="本日尚未有任何非銀行之金融機構稽核人員登錄記錄" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecLog, searchContainer.getStart(), searchContainer.getEnd());
			total = vecLog.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.feb.LogRec" keyProperty="idx" modelVar="LogRec">
		<liferay-ui:search-container-column-text name="帳號" property="loginId"/>
		<liferay-ui:search-container-column-text name="銀行名稱" property="finName"/>
		<liferay-ui:search-container-column-text name="姓名" property="staffName"/>
		<liferay-ui:search-container-column-text name="登入IP" property="fromIp"/>
		<liferay-ui:search-container-column-text name="填報IP" property="staticIp"/>
		<liferay-ui:search-container-column-text name="登入時間" property="loginTime"/>
		<liferay-ui:search-container-column-text name="登入狀態" property="status"/>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

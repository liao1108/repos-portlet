package com.itez.feb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class FinLoginTracePortlet extends MVCPortlet {
	DataSource ds = null;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			conn = ds.getConnection();
			//
			sdf.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
			String logDate = sdf.format(new java.util.Date());
			if(renderRequest.getPortletSession(true).getAttribute("logDate") != null){
				logDate = renderRequest.getPortletSession(true).getAttribute("logDate").toString();
			}
			//
			Vector<LogRec> vecLog = new Vector<LogRec>();
			if(renderRequest.getPortletSession(true).getAttribute("vecLog") != null){
				vecLog = (Vector<LogRec>)renderRequest.getPortletSession(true).getAttribute("vecLog");
			}
			//
			if(vecLog.size() == 0){
				vecLog = this.getLogRecByDate(logDate, conn);
			}
			renderRequest.getPortletSession(true).setAttribute("vecLog", vecLog);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}	
	
	
	private Vector<LogRec> getLogRecByDate(String date, Connection conn) throws Exception{
		Vector<LogRec> vecLog = new Vector<LogRec>();
		//
		String sql = "SELECT A.login_id," +
							" B.fin_name," +
							" B.staff_name," +
							" A.from_ip," +
							" B.static_ip," +
							" A.login_time," +
							" A.success" +
						" FROM login_log A, fin_staff B " +
						" WHERE A.login_id = B.login_id " +
							" AND A.login_time LIKE '" + date + "%' " +
						" ORDER BY A.login_time DESC";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		int idx = 0;
		while(rs.next()){
			LogRec rec = new LogRec();
			//
			idx += 1;
			rec.idx = idx;
			rec.loginId = rs.getString("login_id");
			rec.finName = rs.getString("fin_name");
			rec.staffName = rs.getString("staff_name");
			rec.fromIp = rs.getString("from_ip");
			rec.staticIp = rs.getString("static_ip");
			rec.loginTime = rs.getString("login_time");
			if(rs.getInt("success") == 1){
				rec.status = "成功";
			}else{
				rec.status = "失敗";
			}
			//
			vecLog.add(rec);
		}
		rs.close();
		//
		return vecLog;
	}

	public void queryLogByDate(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			conn = ds.getConnection();
			//
			String logDate = actionRequest.getParameter("logDate");
			if(logDate == null || logDate.trim().isEmpty()) throw new Exception("請以西元年-月-日格式輸入查詢日期。");
			//
			Vector<LogRec> vecLog = this.getLogRecByDate(logDate, conn);
			actionRequest.getPortletSession(true).setAttribute("vecLog", vecLog);
			actionRequest.getPortletSession(true).setAttribute("logDate", logDate);
		}catch(Exception ex){
			ex.printStackTrace();
			//String errMsg = getItezErrorCode(ex);
			//if(errMsg.equals("")){
				String errMsg = ex.getMessage();
			//}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
	
}

package com.itez;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.http.HttpHeaders;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class FileSearchPortlet extends MVCPortlet {
	DataSource ds = null;
	
	String rootReports = "";
	
	Vector<GotFile> vecOut = new Vector<GotFile>();
	//
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootReports = rs.getString("folder_reports");
	   		}
	   		rs.close();
	   		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
				
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	//執行搜尋
	public void doSearching(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String keyWord = actionRequest.getParameter("keyWord");
			if(keyWord.equals("")) throw new Exception("請輸入搜尋關鍵字。");
			//
			//TreeMap<String, GotFile> tmGot = new TreeMap<String, GotFile>(); 
			Vector<GotFile> vecGot = new Vector<GotFile>();
			//我參與的報告案
			conn = ds.getConnection();
			ErpUtil.logUserAction("搜尋檔案", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoFolder fdDocRoot = null;
			Iterator<CmisObject>  _it = alfSessionAdmin.getRootFolder().getChildren().iterator();
			 while(_it.hasNext()){
				 CmisObject co = _it.next();
				 if(co.getName().equals(rootReports)){
					 fdDocRoot = (AlfrescoFolder)co;
					break;
				 }
			 }
			 if(fdDocRoot == null) throw new Exception("檢查報告文件根目錄找不到。");
			//取得 Object Id 的屬性名稱
			//ObjectType type = alfSessionAdmin.getTypeDefinition("cmis:document");
			//PropertyDefinition<?> objectIdPropDef = type.getPropertyDefinitions().get(PropertyIds.OBJECT_ID);
			//String objectIdQueryName = objectIdPropDef.getQueryName();
			//
			HashMap<String, String> htExamLeader = new HashMap<String, String>();
			String sql = "SELECT exam_no, leader_name FROM exams";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				htExamLeader.put(rs.getString(1), rs.getString(2));
			}
			rs.close();
			//
			sql = "SELECT d.* FROM cmis:document d WHERE IN_TREE(d, '" + fdDocRoot.getId() + "') AND ( CONTAINS(d, '" + keyWord + "') OR d.cmis:name LIKE '%" + keyWord + "%')";
			Iterator<QueryResult>  it = alfSessionAdmin.query(sql, false).iterator();
			while(it.hasNext()){
				QueryResult qr = it.next();
				//
				if(qr == null){
					System.out.println("Query Result is null !!!!!");
				}else{
					GotFile g = new GotFile();
					//
					boolean selected = false;
					//
					//System.out.println(qr.getPropertyValueById(PropertyIds.OBJECT_ID));
					
					//if(qr.getProperties().contains(PropertyIds.OBJECT_ID) && qr.getPropertyValueById(PropertyIds.OBJECT_ID) != null){
						g.fileId = qr.getPropertyValueById(PropertyIds.OBJECT_ID);
						//g.fileId = qr.getPropertyValueByQueryName(objectId);
						//
						AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(g.fileId);
						AlfrescoFolder fdStage = (AlfrescoFolder)alfDoc.getParents().get(0);
						AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
						//
						if(alfDoc.getName().startsWith("_del#")){
							selected = false;
						}else if(alfDoc.getName().contains(userFullName)){
							selected = true;
						}else if(alfDoc.getPropertyValue("it:author") != null && alfDoc.getPropertyValue("it:author").equals(userFullName)){
							selected = true;
						}else if(fdExam != null && htExamLeader.get(fdExam.getName()) != null && htExamLeader.get(fdExam.getName()).equalsIgnoreCase(userFullName)){
							selected = true;
							//sql = "SELECT 1 FROM exams WHERE leader_name=? AND exam_no=?";
							//PreparedStatement ps = conn.prepareStatement(sql);
							//ps.setString(1, userFullName);
							//ps.setString(2, fdExam.getName());
							//ResultSet rs = ps.executeQuery();
							//if(rs.next()){
							//	selected = true;
							//}
							//rs.close();
						}
						if(selected){
							AlfrescoFolder fdReport = (AlfrescoFolder)alfSessionAdmin.getObject(fdExam.getParentId());
							//
							g.fileName = alfDoc.getName();
							if(alfDoc.getDescription() != null) g.description = alfDoc.getDescription();
							if(alfDoc.getPropertyValue("cm:author") != null) g.author = alfDoc.getPropertyValue("cm:author");
							g.docPath = fdReport.getName() + "/" + fdExam.getName() + "/" + fdStage.getName() + "/" + g.fileName;
							g.stageFolderId = fdStage.getId();
							g.examNo = fdExam.getName();
							g.reportNo = fdReport.getName();
							g.stageName = fdStage.getName();
							
							g.dateCreated = ErpUtil.getDateTimeStr(alfDoc.getCreationDate().getTime());
							g.dateUpdated = ErpUtil.getDateTimeStr(alfDoc.getLastModificationDate().getTime());
							//
							String iconName = "file.png";
							if(g.fileName.toLowerCase().endsWith("xls")){
								iconName = "xls.png";
							}else if(g.fileName.toLowerCase().endsWith("doc") || g.fileName.toLowerCase().endsWith("docx")){
								iconName = "doc.png";
							}else if(g.fileName.toLowerCase().endsWith("ppt")){
								iconName = "ppt.png";
							}else if(g.fileName.toLowerCase().endsWith("pdf")){
								iconName = "pdf.png";
							}
							g.iconName = iconName;
							//
							//StringBuffer output = new StringBuffer();
						        //POITextExtractor textExtractor = ExtractorFactory.createExtractor(alfDoc.getContentStream().getStream());
							//String text = textExtractor.getText();
							String text = "";
							if(g.fileName.toLowerCase().endsWith("doc")){
								com.aspose.words.Document _doc = new com.aspose.words.Document(alfDoc.getContentStream().getStream());
								text = _doc.getText();
							}
							g.times = text.split(keyWord).length - 1;
							//
							vecGot.add(g);
						}
					//}
				}
			}
			//排序
			String sortingBy = "byTimes";
			if(actionRequest.getPortletSession(true).getAttribute("sortingBy") != null){
				sortingBy = actionRequest.getPortletSession(true).getAttribute("sortingBy").toString();
			}else{
				sql = "SELECT search_sorting FROM user_prefer WHERE screen_name=?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, themeDisplay.getUser().getScreenName());
				rs = ps.executeQuery();
				if(rs.next()){
					String s = rs.getString(1);
					if(s.equals("標題")){
						sortingBy = "byTitle";
					}else if(s.equals("更新時間")){
						sortingBy = "byUpdateTime";
					}else if(s.equals("建檔時間")){
						sortingBy = "byCreateTime";
					}
				}
				rs.close();
			}
			//
			vecGot = this.getSortedFile(vecGot, sortingBy);
			//
			actionRequest.getPortletSession(true).setAttribute("keyWord", keyWord);
			actionRequest.getPortletSession(true).setAttribute("vecGot", vecGot);
			actionRequest.getPortletSession(true).setAttribute("sortingBy", sortingBy);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//執行排序
	public void doSorting(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String sortingBy = actionRequest.getParameter("sortingBy");
			if(sortingBy.equals("")) throw new Exception("請輸入排序種類。");
			//
			Vector<GotFile> vecGot = (Vector<GotFile>)actionRequest.getPortletSession(true).getAttribute("vecGot");
			vecGot = this.getSortedFile(vecGot, sortingBy);
			//
			actionRequest.getPortletSession(true).setAttribute("vecGot", vecGot);
			actionRequest.getPortletSession(true).setAttribute("sortingBy", sortingBy);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private Vector<GotFile> getSortedFile(Vector<GotFile> vecGot, String sortingBy) throws Exception{
		TreeMap<String, GotFile> tmGot = new TreeMap<String, GotFile>();
		for(GotFile g: vecGot){
			String key = "";
			if(sortingBy.equalsIgnoreCase("byTimes")){
				key = String.format("%05d", 10000 - g.times) + "#" + g.fileId;
			}else if(sortingBy.equalsIgnoreCase("byTitle")){
				key = g.docPath;
			}else if(sortingBy.equalsIgnoreCase("byUpdateTime")){
				key = g.dateUpdated + "#" + g.fileId;
			}else if(sortingBy.equalsIgnoreCase("byCreateTime")){
				key = g.dateCreated + "#" + g.fileId;
			}
			tmGot.put(key, g);
		}
		//
		Vector<GotFile> _vecGot = new Vector<GotFile>();  
		Iterator<String> it = tmGot.keySet().iterator();
		while(it.hasNext()){
			GotFile g = tmGot.get(it.next());
			_vecGot.add(g);
		}
		return _vecGot;
	}

	
	//執行搜尋
	public void doSearching_BAK(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String keyWord = actionRequest.getParameter("keyWord");
			if(keyWord.equals("")) throw new Exception("請輸入搜尋關鍵字。");
			//
			Vector<GotFile> vecGot = new Vector<GotFile>();
			//我參與的報告案
			Vector<String> vecFid = new Vector<String>();
			
			conn = ds.getConnection();
			String sql = "SELECT folder_id FROM reports WHERE leader_name = ? OR staffs LIKE '%" + userFullName + "%' ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userFullName);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				vecFid.add(rs.getString(1));
			}
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//取得 Object Id 的屬性名稱
			//ObjectType type = alfSessionAdmin.getTypeDefinition("cmis:document");
			//PropertyDefinition<?> objectIdPropDef = type.getPropertyDefinitions().get(PropertyIds.OBJECT_ID);
			//String objectIdQueryName = objectIdPropDef.getQueryName();
			//
			for(String fid: vecFid){
				//System.out.println(fid);
				
				sql = "SELECT d.* FROM cmis:document d WHERE IN_TREE(d, '" + fid + "') AND CONTAINS(d, '" + keyWord + "') ";
				//sql = "SELECT d.cmis:objectId FROM cmis:document d WHERE CONTAINS(d, '" + keyWord + "') ";
				Iterator<QueryResult>  it = alfSessionAdmin.query(sql, false).iterator();
				while(it.hasNext()){
					QueryResult qr = it.next();
					//
					GotFile g = new GotFile();
					//
					//System.out.println(qr.getPropertyValueById(PropertyIds.OBJECT_ID));
					g.fileId = qr.getPropertyValueById(PropertyIds.OBJECT_ID);
					
					AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(g.fileId);
					AlfrescoFolder fdStage = (AlfrescoFolder)alfDoc.getParents().get(0);
					AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
					AlfrescoFolder fdReport = (AlfrescoFolder)alfSessionAdmin.getObject(fdExam.getParentId());
					//
					
					g.fileName = alfDoc.getName();
					if(alfDoc.getDescription() != null) g.description = alfDoc.getDescription();
					if(alfDoc.getPropertyValue("it:author") != null) g.author = alfDoc.getPropertyValue("it:author");
					g.docPath = fdReport.getName() + "/" + fdExam.getName() + "/" + fdStage.getName();
					g.stageFolderId = fdStage.getId();
					g.examNo = fdExam.getName();
					g.reportNo = fdReport.getName();
					g.stageName = fdStage.getName();
					
					g.dateCreated = ErpUtil.getDateTimeStr(alfDoc.getCreationDate().getTime());
					g.dateUpdated = ErpUtil.getDateTimeStr(alfDoc.getLastModificationDate().getTime());

					
					String iconName = "file.png";
					/*
					if(sf.fileName.toLowerCase().endsWith("xls")){
						iconName = "xls.png";
					}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
						iconName = "doc.png";
					}else if(sf.fileName.toLowerCase().endsWith("ppt")){
						iconName = "ppt.png";
					}else if(sf.fileName.toLowerCase().endsWith("pdf")){
						iconName = "pdf.png";
					}
					*/
					g.iconName = iconName;
					//
					vecGot.add(g);
				}
			}
			//
			actionRequest.getPortletSession(true).setAttribute("keyWord", keyWord);
			actionRequest.getPortletSession(true).setAttribute("vecGot", vecGot);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
 
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = resRequest.getParameter("fileId");
			//String stageFolderId = resRequest.getParameter("stageFolderId");
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			CmisObject co = alfSessionAdmin.getObject(fileId);
			Document doc = (Document)co;
			String fileName = doc.getName();

			resResponse.setContentType(doc.getContentStreamMimeType());
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			//
			java.io.InputStream is = doc.getContentStream().getStream();
			if(is != null){
				byte[] buffer = new byte[4096];
				int n;
				while ((n = is.read(buffer)) > 0) {
				    out.write(buffer, 0, n);
				}
				//
				out.flush();
				out.close();
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	/*
	private String getContentAsString(ContentStream stream) throws IOException {
		StringBuilder sb = new StringBuilder();
		Reader reader = new InputStreamReader(stream.getStream(), "UTF-8");
		try{	
			final char[] buffer = new char[4 * 1024];
			int b;
			while (true){
				b = reader.read(buffer, 0, buffer.length);
				if(b > 0){
				      sb.append(buffer, 0, b);
				}else if (b == -1){
				      break;
				}
			}
		}finally{
		       reader.close();
		}
		return sb.toString();
	}
	*/
}

<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.GotFile"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            	User user = themeDisplay.getUser();
            	//
            	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
            	//
            	Vector<GotFile> vecGot = new Vector<GotFile>();
            	if(renderRequest.getPortletSession(true).getAttribute("vecGot") != null){
            		vecGot = (Vector<GotFile>)renderRequest.getPortletSession(true).getAttribute("vecGot");
            	}
            	
            	String keyWord = "";
            	if(renderRequest.getPortletSession(true).getAttribute("keyWord") != null){
            		keyWord = renderRequest.getPortletSession(true).getAttribute("keyWord").toString();
            	}
            	
            	String sortingBy = "";
            	if(renderRequest.getPortletSession(true).getAttribute("sortingBy") != null){
            		sortingBy = renderRequest.getPortletSession(true).getAttribute("sortingBy").toString();
            	}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doSearchingURL" name="doSearching" >
</portlet:actionURL>

<portlet:actionURL var="doSortingURL" name="doSorting" >
</portlet:actionURL>

<script type="text/javascript">
	function doSorting() {
			document.getElementById("<portlet:namespace/>" + fldName).value ="基於您的問題○○○(請權責組簡要表達無法及時處理之原因如：涉他局職掌或較為複雜等)，本局目前正在處理中，將會儘速回覆給您。";				
	}
</script>


<aui:form action="<%=doSearchingURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>
		 	<img src="<%=renderRequest.getContextPath()%>/images/search.png">
		</aui:column>
	
		<aui:column cssClass="td" >
		 	<aui:input name="keyWord" label="請輸入搜尋關鍵字 (範圍僅限您所參與的檢查報告案)" value="<%=keyWord %>"   cssClass="td" size="100"/>
		</aui:column>
		
		<aui:column>
			<br/>
			<aui:button type="submit" value="開始搜尋"/>
		</aui:column>
	</aui:layout>
</aui:form>

<hr/>
<%if(vecGot.size() > 0){ %>
<aui:form action="<%=doSortingURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</aui:column>
		<aui:column><aui:input name="sortingBy" type="radio"  label="以出現次數排序"  value="byTimes"  checked='<%=sortingBy.equalsIgnoreCase("byTimes") %>'/></aui:column>
		<aui:column><aui:input name="sortingBy" type="radio"  label="以標題排序"  value="byTitle"  checked='<%=sortingBy.equalsIgnoreCase("byTitle") %>'/></aui:column>
		<aui:column><aui:input name="sortingBy" type="radio"  label="以更新時間排序"  value="byUpdateTime"  checked='<%=sortingBy.equalsIgnoreCase("byUpdateTime") %>'/></aui:column>
		<aui:column><aui:input name="sortingBy" type="radio"  label="以建檔時間排序"  value="byCreateTime" checked='<%=sortingBy.equalsIgnoreCase("byCreateTime") %>'/></aui:column>
		<aui:column><aui:button type="submit" value="重排" /></aui:column>
	</aui:layout>
</aui:form>

<br>
<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecGot, searchContainer.getStart(), searchContainer.getEnd());
			total = vecGot.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.GotFile" keyProperty="fileId" modelVar="gotFile">
		<portlet:resourceURL var="downloadFileURL" >
    			<portlet:param name="fileId" value= "<%=String.valueOf(gotFile.fileId)%>"/>
    			<portlet:param name="stageFolderId" value= "<%=String.valueOf(gotFile.stageFolderId)%>"/>
		</portlet:resourceURL>
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=gotFile.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="標題" property="docPath" href="<%=downloadFileURL.toString()%>"/>
		<liferay-ui:search-container-column-text name="摘要" property="description" />
		<liferay-ui:search-container-column-text name="作者" property="author" align="center" />
		<liferay-ui:search-container-column-text name="內文出現次數" property="times"  align="center"/>
		<liferay-ui:search-container-column-text name="建立時間" property="dateCreated" align="center"/>
		<liferay-ui:search-container-column-text name="更新時間" property="dateUpdated" align="center"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>
<%}%>
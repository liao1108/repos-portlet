package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jdbc.pool.DataSource;

/**
 * Portlet implementation class ListItemPortlet
 */
public class ListItemPortlet extends GenericPortlet {
	DataSource ds = null;
	ECourseUtils utils = new ECourseUtils();
	String quizForwardPage = "/quizone";
	int topicLeastMinutes = 5;
	
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	//
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		topicLeastMinutes = Integer.parseInt(getInitParameter("topic-least-minutes"));
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
    	Connection conn = null;
    	try{
	    	String itemTree = "";
			long topicID = 0;
			long itemID = 0;
			//int leastMinutes = 10;
			int readMinutes = 0;
			String thankWording = "";
			
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
	    	HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
			//
			if(httpRequest.getParameter("topicID") == null){
				throw new Exception("Topic ID not exist in request parameter !");
			}else{
				topicID = Long.parseLong(httpRequest.getParameter("topicID"));
			}
			//
			if(httpRequest.getParameter("itemID") != null){
				itemID = Long.parseLong(httpRequest.getParameter("itemID"));
			}else if(renderRequest.getAttribute("itemID") != null){
				itemID = Long.parseLong(renderRequest.getAttribute("itemID").toString());
			}
			//
			conn = ds.getConnection();
			//		
			if(httpRequest.getParameter("topicLeastMinutes") != null){
				topicLeastMinutes = Integer.parseInt(httpRequest.getParameter("topicLeastMinutes"));
			}else{
				String sql = "SELECT read_least_minutes, thanks_word FROM topic_setting WHERE topic_id = ?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setLong(1, topicID);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					topicLeastMinutes = rs.getInt(1);
					thankWording = rs.getString(2);
				}
				rs.close();
			}
			//
			long userId = -1;
			try{
				userId = PortalUtil.getUser(renderRequest).getUserId();
			}catch(Exception _ex){
				
			}
			//
			List<Organization> orgs = OrganizationLocalServiceUtil.getUserOrganizations(userId);
			String orgName = "";
			if(orgs.size() > 0){
				orgName = orgs.get(0).getName();
			}
			//
			Vector<Item> vecItem = new Vector<Item>();
			//
			//Folder fdTopic = DLAppServiceUtil.getFolder(topicID);
			//thankWording = fdTopic.getDescription();
			//leastMinutes = Integer.parseInt(fdTopic..getPropertyValue("cm:title").toString().trim());
			//已閱讀秒數
			int readSeconds = 0;
			String sql = "SELECT SUM(read_seconds) FROM trace_reading WHERE u_id=? AND topic_uuid=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, topicID);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				readSeconds = rs.getInt(1);
			}
			rs.close();
			//
			List<Folder> list = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), topicID);
			for(Folder fdItem: list){
				//System.out.println("Course Item: " + fdItem.getName());
				//
				Item item = new Item();
				item.name = fdItem.getName();
				item.id = fdItem.getFolderId();
				item.description = fdItem.getDescription();
				//
				sql = "SELECT * FROM trace_reading WHERE u_id=? " +
															" AND topic_uuid=? " +
															" AND item_uuid=? " +
															" AND start_time IS NOT NULL AND start_time <> '' ORDER BY completed DESC";
				ps = conn.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, topicID);
				ps.setLong(3, fdItem.getFolderId());
				rs = ps.executeQuery();
				if(rs.next()){
					if(rs.getInt("completed") == 1){
						item.status = "Read";
					}else{
						item.status = "Reading";
					}
				}else{
					item.status  ="UnRead";
				}
				rs.close();
				//
				vecItem.add(item);
			}
			//
			Vector<Item> vecItemSort = new Vector<Item>();
			//
			Vector<String> vecSort = new Vector<String>();
			for(Item it: vecItem){
				vecSort.add(it.name);
			}
			Collections.sort(vecSort);
			for(String s: vecSort){
				for(Item it: vecItem){
					if(it.name.equals(s)){
						vecItemSort.add(it);
						break;
					}
				}
			}
			if(itemID == 0 && vecItemSort.size() > 0){
				itemID = vecItemSort.get(0).id;
			}
		 	//
		 	itemTree = "";
		 	for(Item it: vecItemSort){
		 		itemTree += "<form id= \"myItemForm\" method=\"POST\" action=\"" + renderResponse.createActionURL() + "\">";
		 		itemTree += "<table width='100%' cellpadding='5' >";
		 		//itemTree += "<tr><td colspan='3' height='3px' background='/images/item_table_bg.png'>&nbsp</td></tr>";
		 		itemTree += "<tr>";
		 		//
		 		itemTree += "<td height='20'>";
		 		if(it.id == itemID){
		 			itemTree += "<img src='" + renderRequest.getContextPath() + "/images/selected.png'/>";
		 		}else{
		 			itemTree += "&nbsp;";
		 		}
		 		itemTree += "</td>";
		 		//System.out.println(itemUUID + ":" + it.uuid);
		 		//
		 		String statusImage = "/images/green_box.png";
		 		if(it.status.equals("Reading")){
		 			statusImage = "/images/orange_box.png";
		 		}else if(it.status.equals("Read")){
		 			statusImage = "/images/red_box.png";
		 		}
		 		//
		 		itemTree += "<td height='20'><img src='" + statusImage + "'/></td>";
		 		itemTree += "<td height='20'>";
				itemTree += "<input type='submit' style='background:none;border:0;' value='" + it.name +"'/>";
				itemTree += "<input type=\"hidden\" name=\"topicID\" id=\"topicID\" value=\"" + topicID + "\"/>";
				itemTree += "<input type=\"hidden\" name=\"itemID\" id=\"itemID\" value=\"" + it.id + "\"/>";
				itemTree += "</td>";
				//
				itemTree += "</tr>";
				//
				if(it.description != null && !it.description.equals("")){
					itemTree += "<tr>";
					itemTree += "<td>&nbsp;</td>";
					itemTree += "<td>&nbsp;</td>"; 
					itemTree += "<td>";
					
					itemTree += "<table width = '100%'>";
					String[] ss = it.description.split("\n");
					for(String s: ss){
						itemTree += "<tr valign='top'><td>" + s + "</td></tr>";
					}
					itemTree += "</table>";
					
					itemTree += "</td>";
					itemTree += "</tr>";
				}
				//
				itemTree += "</table>";
				itemTree += "</form>";
		 	}
			//
			renderRequest.setAttribute("itemTree", itemTree);
			renderRequest.setAttribute("topicID", topicID);
			renderRequest.setAttribute("itemID", itemID);
			renderRequest.setAttribute("topicLeastMinutes", String.valueOf(topicLeastMinutes));
			renderRequest.setAttribute("topicDesc", thankWording);
			//
			readMinutes = (int)(readSeconds / 60);
			renderRequest.setAttribute("readMinutes", String.valueOf(readMinutes));
		 	//
			int readCount = 0;
			for(Item it: vecItemSort){
				if(it.status.equalsIgnoreCase("Read")) readCount++;
			}
			//
			int quizStatus = 0;
			if(userId == -1 || orgName.equals("大眾")){
				quizStatus = 1;	
			}else{
				if(readCount >= vecItem.size() && readSeconds >= topicLeastMinutes * 60){
					//查詢最後閱讀時間
					sql = "SELECT MAX(end_time) FROM trace_reading WHERE u_id=? AND topic_uuid = ?";
					ps = conn.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setLong(2, topicID);
					rs = ps.executeQuery();
					if(rs.next()){
						String sqlEndDate = rs.getString(1);
						//
						//查詢是否已將本單元閱讀完畢之狀態寫入
						sql = "UPDATE trace_topic SET end_time = ? " +
								 " WHERE u_id=? AND topic_uuid=? AND start_time IS NOT NULL AND (end_time IS NULL OR end_time ='')";
						ps = conn.prepareStatement(sql);
						ps.setString(1, sqlEndDate);
						ps.setLong(2, userId);
						ps.setLong(3, topicID);
						ps.execute();
					}
					rs.close();
					//allowQuiz = true;
					sql = "SELECT * FROM trace_quiz WHERE u_id = ? AND topic_uuid = ? ORDER BY quiz_start_time DESC";
					ps = conn.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setLong(2, topicID);
					rs = ps.executeQuery();
					if(rs.next()){
						if(rs.getInt("completed") != 1){
							quizStatus = 2;		//繼續檢測
						}else{
							quizStatus = 3;		//已檢測完成
						}
					}else{
						quizStatus = 1;			//首次進行自我檢測
					}
					rs.close();
				}
			}
			//測試用
			//if(quizStatus == 0){
			//	quizStatus = 1;			//首次進行自我檢測
			//}
	 		//
			String htmlQuiz = "<table border = '0' width='100%'>";
			htmlQuiz +=	"<tr><td>&nbsp;</td></tr>";
			htmlQuiz +=	"<tr>";
			htmlQuiz += "<td align='center'>";
	    	if(quizStatus == 0){
	    		String srcImage = renderRequest.getContextPath() + "/images/reading.png";
	    		htmlQuiz += "<img src='" + srcImage + "'/>";
	    	}else if(quizStatus > 0 && quizStatus < 3){
	    		String srcImage = renderRequest.getContextPath() + "/images/self_test_go.png";
	    		if(quizStatus == 2){
	    			srcImage = renderRequest.getContextPath() + "/images/redo.png";
	    		}
	    		htmlQuiz += "<a href='" + quizForwardPage + "?topicID=" + topicID + "'>";
	    		htmlQuiz += "<img src='" + srcImage + "'/>";
	    		htmlQuiz += "</a>";
	    	}else{
	    		String srcImage = renderRequest.getContextPath() + "/images/pass.png";
	    		htmlQuiz += "<a href='" + quizForwardPage + "?topicID=" + topicID + "'>";
	    		htmlQuiz += "<img src='" + srcImage + "'/>";
	    		htmlQuiz += "</a>";
	    	}
	    	htmlQuiz += "</td>";
	    	htmlQuiz += "</tr></table>";
	    	//
	    	renderRequest.setAttribute("htmlQuiz", htmlQuiz);
	    } catch (Exception ex){
	    	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
	    }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
		renderRequest.setAttribute("errMsg", errMsg);
		//
		this.include(viewJSP, renderRequest, renderResponse);  
    }
    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
    	String topicID = "";
		if(actionRequest.getParameter("topicID") != null){
			topicID = actionRequest.getParameter("topicID");
    		actionRequest.setAttribute("topicID", actionRequest.getParameter("topicID"));
    	}
		String itemID = "";
    	if(actionRequest.getParameter("itemID") != null){
    		itemID = actionRequest.getParameter("itemID");
    		actionRequest.setAttribute("itemID", actionRequest.getParameter("itemID"));
    	}
    	actionResponse.setEvent("load-item-event", topicID + "#" + itemID);
    	//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(ListItemPortlet.class);

}

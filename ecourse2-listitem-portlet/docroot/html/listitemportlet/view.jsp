<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="itemTree" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="topicUUID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="topicLeastMinutes" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="readMinutes" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="itemUUID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="topicDesc" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="htmlQuiz" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<table width="100%">
		<tr valign="middle">
			<td align="left" width="5"><img src="<%=renderRequest.getContextPath()%>/images/course_list.png"/></td>
			<td align="left" colspan="6"><a href="/courselist">&nbsp;返回課程目錄</a></td>
		</tr>
		<tr valign="middle">
		 	<td>&nbsp;</td>
			<td height="26"><img src="/images/red_box.png"/></td>
			<td nowrap align="center">已閱讀&nbsp;</td>
			<td>&nbsp;&nbsp;<img src="/images/orange_box.png"/></td>
			<td nowrap align="center">閱讀中&nbsp;</td>
			<td align="center">&nbsp;&nbsp;<img src="/images/green_box.png"/></td>
			<td width="100%" align="left"" nowrap>未閱讀</td>
		</tr>
	</table>
	
	<%=itemTree%>
	<p/>

	<table width="100%">
		<tr valign="middle">
			<td><img src="<%=renderRequest.getContextPath()%>/images/timer.png"></td>
		</tr>
		<tr>	
			<td width="100%">
				<span style="font-family: 微軟正黑體;font-size: 10pt;">
					&nbsp;&nbsp;&nbsp;本單元最短閱讀時間:&nbsp;<%=topicLeastMinutes%>&nbsp;分鐘
				</span>
			</td>
		</tr>
		<tr>
			<td width="100%">
				<span style="font-family: 微軟正黑體;font-size: 10pt;">
					&nbsp;&nbsp;&nbsp;本單元累計閱讀時間:&nbsp;<%=readMinutes%>&nbsp;分鐘
				</span>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<%if(!topicDesc.equals("")){ %>
		<table width="100%">
			<tr valign="middle">
				<td><img src="<%=renderRequest.getContextPath()%>/images/thankyou.png"></td>
				<td width="100%"><span style="font-family: 微軟正黑體;font-size: 10pt;"><%=topicDesc%></span></td>
			</tr>	
		</table>
	<%} %>
	
	<%=htmlQuiz%>
</body>
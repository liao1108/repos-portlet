<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.RowChecker"%>

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.Task"%>
 
<portlet:defineObjects />
 
  <%
   		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            User user = themeDisplay.getUser();
            //
            if(renderRequest.getAttribute("javax.servlet.forward.query_string") != null){
        	    	String currParam = renderRequest.getAttribute("javax.servlet.forward.query_string").toString();
        	    	renderRequest.getPortletSession(true).setAttribute("currParam", currParam);
            }else{
        	    	renderRequest.getPortletSession(true).removeAttribute("currParam");
            }
		//未簽收
            Vector<Task> vecTaskQue = new Vector<Task>();
            if(renderRequest.getPortletSession(true).getAttribute("vecTaskQue") != null){
            	vecTaskQue = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecTaskQue");
            }
            //我底下的助檢未簽收
            Vector<Task> vecTaskQueStaff = new Vector<Task>();
            if(renderRequest.getPortletSession(true).getAttribute("vecTaskQueStaff") != null){
        	    vecTaskQueStaff = (Vector<Task>)renderRequest.getPortletSession(true).getAttribute("vecTaskQueStaff");
            }
		//
            String userFullName = "";
            if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
            	userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
            }
            //
            int rowsPrefer = 20;
		if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
			rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
		}
		//
            String errMsg = "";	
            if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            String successMsg = "";	
            if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="batchSignURL"  name="doBatchSign" >
</portlet:actionURL>	

<script type="text/javascript">
	function doCheckAll(){
		var withCkecked = false;
		var chk = document.getElementsByTagName('input');
	    	var len = chk.length;
	      for (var i = 0; i < len; i++) {
	      	if (chk[i].type == 'checkbox') {
	            	chk[i].checked = true;
	        	}
	    	}      
	}

	function doUnCheckAll(){
		var withCkecked = false;
		var chk = document.getElementsByTagName('input');
	    	var len = chk.length;
	      for (var i = 0; i < len; i++) {
	      	if (chk[i].type == 'checkbox') {
	      		chk[i].checked = false;
	        	}
	    	}      
	}
	
</script>

<aui:form action="<%=batchSignURL%>" method="post" name="<portlet:namespace />fm">

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/que.png">&nbsp;您(<%=userFullName%>)尚未簽收的待辦事項
	</aui:column>
	
	<aui:column>
		<aui:button onClick="doCheckAll();"   value="全選" cssClass="button"/>
	</aui:column>
	<aui:column>
		<aui:button onClick="doUnCheckAll();"  value="全不選" cssClass="button"/>
	</aui:column>
	<aui:column>
		<aui:button type="submit"  value="批次簽收" cssClass="button"/>
	</aui:column>
</aui:layout>
<hr/>
<br>
<liferay-ui:search-container curParam="curQue" emptyResultsMessage="您目前沒有待簽收事項。"  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecTaskQue, searchContainer.getStart(), searchContainer.getEnd());
			total = vecTaskQue.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="taskQue">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="stageFolderId"  value="<%=taskQue.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=taskQue.fileId %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=taskQue.taskId%>"/>   
            </liferay-ui:search-container-column-text>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=taskQue.iconName %>"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName"  href="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminQue.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" paginate="<%= true %>"/>
</liferay-ui:search-container>
<br>

<%if(vecTaskQueStaff.size() > 0){ %>

<aui:layout>
	<aui:column cssClass="td">
	 	<img src="<%=renderRequest.getContextPath()%>/images/proxy.png">&nbsp;您可代理簽收的待辦事項
	</aui:column>
</aui:layout>
<hr/>
<br>
<liferay-ui:search-container curParam="curQueStaff"  emptyResultsMessage=""  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecTaskQueStaff, searchContainer.getStart(), searchContainer.getEnd());
			total = vecTaskQueStaff.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.Task" keyProperty="taskId" modelVar="taskQueStaff">
		<portlet:resourceURL var="downloadFileURL" >
			<portlet:param name="stageFolderId"  value="<%=taskQueStaff.stageFolderId %>"/>
  			<portlet:param name="fileId"  value="<%=taskQueStaff.fileId %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
                  <input type="checkbox"  name="cb_<%=taskQueStaff.taskId%>"/>   
            </liferay-ui:search-container-column-text>
            
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=taskQueStaff.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="受任人" property="assignee" />
		<liferay-ui:search-container-column-text name="到期日" property="deadLine" />
		<liferay-ui:search-container-column-text name="任務類型" property="taskType" />
		<liferay-ui:search-container-column-text name="任務摘要" property="taskComment" />
		<liferay-ui:search-container-column-text name="受檢單位" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔案名稱" property="fileName"  href="<%=downloadFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="傳送人" property="issuer"/>
		<liferay-ui:search-container-column-text name="傳送時間" property="createdTime"/>
		
		<liferay-ui:search-container-column-jsp path="/html/admin/adminQueStaff.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" paginate="<%= true %>"/>
</liferay-ui:search-container>

<%} %>

</aui:form>

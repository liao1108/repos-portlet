package com.itez;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.http.HttpHeaders;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ToSignPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			conn = ds.getConnection();
			//
			Collec collec = this.getTaskQue(userFullName, conn);
			//
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			//
			conn.close();
			//
			renderRequest.getPortletSession(true).setAttribute("vecTaskQue", collec.vecTaskQue);
			renderRequest.getPortletSession(true).setAttribute("vecTaskQueStaff", collec.vecTaskQueStaff);
			
			renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
		}catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	private Collec getTaskQue(String userFullName, Connection conn) throws Exception{
		Vector<Task> vecTaskQue = new Vector<Task>();
		Vector<Task> vecTaskQueStaff = new Vector<Task>();
		//
		String sql = "SELECT * FROM todo_list WHERE assignee = ? AND (done IS NULL OR done = 0) ORDER BY dead_line, file_name";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("issuer") != null) t.issuer = rs.getString("issuer");
			if(rs.getString("reissuer") != null) t.reIssuer = rs.getString("reissuer");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			t.received = rs.getInt("received");
			//
			if(t.received != 1){
				vecTaskQue.add(t);
			}
		}
		//是否有可代簽的任務
		sql = "SELECT A.* FROM todo_list A WHERE A.assignee <> ? " +
				" AND (A.done IS NULL OR A.done = 0) " +
				" AND A.exam_no IN (SELECT B.exam_no FROM exams B WHERE B.leader_name = ? OR B.created_by = ?) " +
				" ORDER BY A.dead_line, A.file_name";
		ps = conn.prepareStatement(sql);
		ps.setString(1, userFullName);
		ps.setString(2, userFullName);
		ps.setString(3, userFullName);
		rs = ps.executeQuery();
		while(rs.next()){
			Task t = new Task();
			t.taskId = rs.getInt("task_id");
			t.assignee = rs.getString("assignee");
			t.reportNo = rs.getString("report_no");
			t.examNo = rs.getString("exam_no");
			t.bankName = rs.getString("bank_name");
			t.stageFolderId = rs.getString("stage_folder_id");
			t.stageName = rs.getString("stage_name");
			t.fileId = rs.getString("file_id");
			t.fileName = rs.getString("file_name");
			t.versionNo = rs.getString("version_no");
			t.taskType = rs.getString("task_type");
			t.createdTime = rs.getString("created_time");
			t.deadLine = rs.getString("dead_line");
			t.done = rs.getInt("done");
			if(rs.getString("issuer") != null) t.issuer = rs.getString("issuer");
			if(rs.getString("reissuer") != null) t.reIssuer = rs.getString("reissuer");
			if(rs.getString("task_comment") != null) t.taskComment = rs.getString("task_comment");
			if(rs.getString("web_dav_url") != null) t.webDavURL = rs.getString("web_dav_url");
			//
			if(t.taskType.equals(ErpUtil.TASK_FILL_REPORT)){
				t.iconName = "task_fill.png";
			}else if(t.taskType.equals(ErpUtil.TASK_MODIFY_REPORT)){
				t.iconName = "task_modify.png";
			}else if(t.taskType.equals(ErpUtil.TASK_REVIEW)){
				t.iconName = "task_verify.png";
			}
			t.received = rs.getInt("received");
			//
			if(t.received != 1){
				vecTaskQueStaff.add(t);
			}			
		}
		//
		Collec collec = new Collec();
		collec.vecTaskQue = vecTaskQue;
		collec.vecTaskQueStaff = vecTaskQueStaff;
		//
		return collec;
	}
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//String stageFolderId = resRequest.getParameter("stageFolderId");
			String fileId = resRequest.getParameter("fileId");
			//String stageFolderId = resRequest.getParameter("stageFolderId");
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			//
			conn = ds.getConnection();
			//AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
			//AlfrescoFolder fdExam = (AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId());
			//
			CmisObject co = alfSessionAdmin.getObject(fileId);
			Document doc = (Document)co;
			//
			ErpUtil.logUserAction("下載待簽檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
			//String fileName = doc.getName();
			//AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			//
			//System.out.println("Mine type: " + doc.getContentStreamMimeType());
			resResponse.setContentType(doc.getContentStreamMimeType());
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (doc.getName(), "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			java.io.InputStream is = doc.getContentStream().getStream();
			//
			if(is != null){
				byte[] buffer = new byte[4096];
				int n;
				while ((n = is.read(buffer)) > 0) {
				    out.write(buffer, 0, n);
				}
				//
				out.flush();
				out.close();
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//簽收 doReceiveTask
	public void doReceiveTask(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			int taskId = Integer.parseInt(actionRequest.getParameter("taskId"));
			//
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			//
			String fileId = "";
			String sql = "SELECT file_id FROM todo_list WHERE task_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, taskId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				fileId = rs.getString(1);
			}
			rs.close();
			//
			if(fileId.equals("")) return;
			//
			sql =  "UPDATE todo_list SET received=1, received_time=?, received_by=? WHERE task_id=?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, ErpUtil.getCurrDateTime());
			ps.setString(2, userFullName);
			ps.setInt(3, taskId);
			ps.execute();
			//
			ErpUtil.logUserAction("簽收檔案", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			conn.close();
			//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//簽收 doReceiveTask
	public void doBatchSign(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Enumeration<String> en = actionRequest.getParameterNames();
			if(en == null) return;
			//
			int cc = 0;
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			conn = ds.getConnection();
			while(en.hasMoreElements()){
				String p = en.nextElement();
				if(p.startsWith("cb_")){
					try{
						int taskId = Integer.parseInt(p.split("_")[1]);
						//
						String fileId = "";
						String sql = "SELECT file_id FROM todo_list WHERE task_id=?";
						PreparedStatement ps = conn.prepareStatement(sql);
						ps.setInt(1, taskId);
						ResultSet rs = ps.executeQuery();
						if(rs.next()){
							fileId = rs.getString(1);
						}
						rs.close();
						if(!fileId.equals("")){
							sql =  "UPDATE todo_list SET received=1, received_time=?, received_by=? WHERE task_id=?";
							ps = conn.prepareStatement(sql);
							ps.setString(1, ErpUtil.getCurrDateTime());
							ps.setString(2, userFullName);
							ps.setInt(3, taskId);
							ps.execute();
							//
							cc++;
							//
							ErpUtil.logUserAction("批次簽收", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
						}
					}catch(Exception _ex){
					}
				}
			}
			conn.close();
			//
			if(cc == 0) throw new Exception("請先勾選批次簽收的項目。");
			//強迫回到進入時的URL
			if(actionRequest.getPortletSession(true).getAttribute("currParam") != null && !actionRequest.getPortletSession(true).getAttribute("currParam").toString().equals("")){
				String pageURL = themeDisplay.getURLPortal() + themeDisplay.getURLCurrent().substring(0, themeDisplay.getURLCurrent().indexOf("?"));
				String params = actionRequest.getPortletSession(true).getAttribute("currParam").toString();
				String[] pp = params.split("&");
				params = "";
				for(String p: pp){
					if(!p.contains("action")){ 
						if(!params.equals("")) params += "&";
						params += p;
					}
				}
				String fullURL= pageURL + "?" + params;
				//
				try{
					actionResponse.sendRedirect(fullURL);
				}catch(Exception _ex){
					actionResponse.sendRedirect(pageURL);
				}
			}
	        }catch(Exception ex){
	        	ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			//actionResponse.setRenderParameter("jspPage", "/html/todo/refresh.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

}

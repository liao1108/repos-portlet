<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.util.HashMap"%>
<%@ page import= "java.util.Iterator"%>
<%@ page import= "java.util.Collections"%>

<%@ page import= "com.itez.ErpUtil"%>
<%@ page import= "com.itez.UserPrefer"%>

<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	UserPrefer p = new UserPrefer();
	if(renderRequest.getPortletSession(true).getAttribute("userPrefer") != null){
		p = (UserPrefer)renderRequest.getPortletSession(true).getAttribute("userPrefer");
	}
	
	HashMap<String, String> htPage = new HashMap<String, String>();
	if(renderRequest.getPortletSession(true).getAttribute("htPage") != null){
		htPage = (HashMap<String, String>)renderRequest.getPortletSession(true).getAttribute("htPage");
	}
	
	
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updatePreferURL"  name="updatePrefer" >
</portlet:actionURL>		

		
<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/prefer.png">&nbsp;您(<%=ErpUtil.getCurrUserFullName(themeDisplay) %>)的個人化設定
	</aui:column>
</aui:layout>
<hr/>
<br>

<aui:layout>
	<aui:column columnWidth="50">
		<aui:fieldset>
			<aui:form action="<%=updatePreferURL%>" method="post" name="<portlet:namespace />fm">
				<aui:layout>
					<aui:column cssClass="td">
						<aui:select name="landingPage"  label="登入後首頁">
							<aui:option value="" selected='<%=p.landingPage.equals("") %>'>&nbsp;</aui:option>
							<%
								Vector<String> vecPage = new Vector<String>();
								Iterator<String> it = htPage.keySet().iterator();
								while(it.hasNext()){
									String s = it.next();	
									vecPage.add(s);
								}
								Collections.sort(vecPage);
								//
								for(String pageName: vecPage){
							%>
							<aui:option value="<%=pageName %>"  selected='<%=p.landingPage.equals(pageName) %>'><%=pageName %></aui:option>
							<%
								}
							%>
						</aui:select>
					</aui:column>
				</aui:layout>
			
				<br/>
			
				<aui:layout>
					<aui:column cssClass="td">
						顯示已完成的任務
					</aui:column>
					<aui:column cssClass="td">
						<%if(p.showTaskDone==1){ %>
							<aui:input name="showTaskDone" type="radio"  value="1" label="是"  checked="true"/>
							<aui:input name="showTaskDone" type="radio"  value="0" label="否" />
						<%}else{ %>
							<aui:input name="showTaskDone" type="radio"  value="1" label="是" />
							<aui:input name="showTaskDone" type="radio"  value="0" label="否"  checked="true"/>
						<%} %>	
					</aui:column>
				</aui:layout>
				
				<br/>
				
				<aui:layout>
					<aui:column cssClass="td">
						<aui:select name="searchSorting"  label="搜尋排序方式">
							<aui:option value="" selected='<%=p.searchSorting.equals("") %>'>&nbsp;</aui:option>
							<aui:option value="出現次數" selected='<%=p.searchSorting.equals("出現次數") %>'>出現次數</aui:option>
							<aui:option value="標題" selected='<%=p.searchSorting.equals("標題") %>'>標題</aui:option>
							<aui:option value="更新時間" selected='<%=p.searchSorting.equals("更新時間") %>'>更新時間</aui:option>
							<aui:option value="建檔時間" selected='<%=p.searchSorting.equals("建檔時間") %>'>建檔時間</aui:option>
						</aui:select>
					</aui:column>
				</aui:layout>
			
				<br/>
				
				<aui:layout>
					<aui:column cssClass="td">
						<aui:select name="msgSorting"  label="公布欄排序方式">
							<aui:option value="" selected='<%=p.msgSorting.equals("") %>'>&nbsp;</aui:option>
							<aui:option value="公布時間" selected='<%=p.msgSorting.equals("公布時間") %>'>公布時間</aui:option>
							<aui:option value="到期日" selected='<%=p.msgSorting.equals("到期日") %>'>到期日</aui:option>
							<aui:option value="主旨" selected='<%=p.msgSorting.equals("主旨") %>'>主旨</aui:option>
							<aui:option value="發布單位" selected='<%=p.msgSorting.equals("發布單位") %>'>發布單位</aui:option>
						</aui:select>
					</aui:column>
				</aui:layout>
			
				<br/>
				
				<aui:layout>
					<aui:column cssClass="td">
						<aui:select name="rowsTable"  label="資料表格每頁列數">
							<aui:option value="20" selected='<%=(p.rowsTable == 20) %>'>20列</aui:option>
							<aui:option value="30" selected='<%=(p.rowsTable == 30) %>'>30列</aui:option>
							<aui:option value="50" selected='<%=(p.rowsTable == 50) %>'>50列</aui:option>
							<aui:option value="75" selected='<%=(p.rowsTable == 75) %>'>75列</aui:option>
						</aui:select>
					</aui:column>
				</aui:layout>
				
				<br/>
				
				<aui:layout>
					<aui:column cssClass="td">
						<aui:input name="authorizeCode" label="代理人授權碼"  value="<%=p.authorizeCode %>" />
					</aui:column>
				</aui:layout>
				
				<br/>
				
				<aui:button-row>
					<aui:button  type="submit"   value="確定更新" />
				</aui:button-row>
			</aui:form>
		</aui:fieldset>
	</aui:column>	
	
	<aui:column columnWidth="50">
		<aui:fieldset>
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input name="screenName"  label="登入帳號"  value="<%=user.getScreenName() %>"  readonly="readonly"/>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input name="fullName"  label="全名"  value="<%=ErpUtil.getCurrUserFullName(themeDisplay) %>"  readonly="readonly"/>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input name="jobTitle"  label="職稱"  value="<%=user.getJobTitle() %>"  readonly="readonly"/>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input name="eMail"  label="電子郵件信箱"  value="<%=user.getEmailAddress() %>"  readonly="readonly"/>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column cssClass="td">
					<aui:input name="lastLogin"  label="最後登入時間"  value="<%=ErpUtil.getDateTimeStr(user.getLastLoginDate()) %>" readonly="readonly" />
				</aui:column>
			</aui:layout>
		</aui:fieldset>
	</aui:column>
</aui:layout>
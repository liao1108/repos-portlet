package com.itez;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import com.aspose.words.Cell;
import com.aspose.words.CellMerge;
import com.aspose.words.Document;
import com.aspose.words.NodeType;
import com.aspose.words.Row;
import com.aspose.words.Table;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PreferPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM user_prefer WHERE screen_name=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, themeDisplay.getUser().getScreenName());
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				UserPrefer p = new UserPrefer();
				p.landingPage = rs.getString("landing_page");
				p.showTaskDone = rs.getInt("show_task_done");
				p.searchSorting = rs.getString("search_sorting");
				p.msgSorting = rs.getString("msg_sorting");
				p.rowsTable = rs.getInt("rows_table");
				if(rs.getString("authorize_code") != null){
					p.authorizeCode = rs.getString("authorize_code");
				}
				//p.showErrorCode = rs.getInt("show_error_code");
				//
				renderRequest.getPortletSession(true).setAttribute("userPrefer", p);
			}
			//
			HashMap<String, String> htPage = new HashMap<String, String>();
			sql = "SELECT * FROM page_url_map ORDER BY page_name";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				htPage.put(rs.getString("page_name"), rs.getString("page_url"));
			}
			rs.close();
			//
			renderRequest.getPortletSession(true).setAttribute("htPage", htPage);
			//
			conn.close();	
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}

	//更新個人化資料
	public void updatePrefer(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			//if(!ErpUtil.isExamUser(userFullName, themeDisplay)) throw new Exception("您目前不在檢查人員名單內，無法更新個人化資料。");
			//
			UserPrefer p = new UserPrefer();
			p.landingPage = actionRequest.getParameter("landingPage");
			p.showTaskDone = Integer.parseInt(actionRequest.getParameter("showTaskDone"));
			p.searchSorting = actionRequest.getParameter("searchSorting");
			p.msgSorting = actionRequest.getParameter("msgSorting");
			p.rowsTable = Integer.parseInt(actionRequest.getParameter("rowsTable"));
			if(actionRequest.getParameter("authorizeCode") != null){
				p.authorizeCode = actionRequest.getParameter("authorizeCode");
			}
			//p.showErrorCode = Integer.parseInt(actionRequest.getParameter("showErrorCode"));
			//
			conn = ds.getConnection();
			ErpUtil.logUserAction("更新偏好設定", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
			//
			String sql = "SELECT 1 FROM user_prefer WHERE screen_name = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, themeDisplay.getUser().getScreenName());
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				sql = "UPDATE user_prefer SET landing_page=?," +			//1
										" show_task_done=?," +		//2
										" search_sorting=?," +		//3
										" msg_sorting=?, " +			//4
										" rows_table=?, " +			//5
										"authorize_code=? " +		//6
									" WHERE screen_name=?";		//7
				ps = conn.prepareStatement(sql);
				ps.setString(1, p.landingPage);
				ps.setInt(2, p.showTaskDone);
				ps.setString(3, p.searchSorting);
				ps.setString(4, p.msgSorting);
				ps.setInt(5, p.rowsTable);
				ps.setString(6, p.authorizeCode);
				ps.setString(7, themeDisplay.getUser().getScreenName());
				ps.execute();
			}else{
				sql = "INSERT INTO user_prefer (landing_page," +		//1
										" show_task_done," +	//2
										" search_sorting," +		//3
										" msg_sorting, " +		//4
										" rows_table, " +		//5
										"screen_name," +		//6
										"authorize_code) " +		//7
									" VALUES (?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setString(1, p.landingPage);
				ps.setInt(2, p.showTaskDone);
				ps.setString(3, p.searchSorting);
				ps.setString(4, p.msgSorting);
				ps.setInt(5, p.rowsTable);
				ps.setString(6, themeDisplay.getUser().getScreenName());
				ps.setString(7, p.authorizeCode);
				ps.execute();
			}
			actionRequest.getPortletSession(true).setAttribute("userPrefer", p);
			conn.close();
			
			//測試Aspose.words
			Document doc = new Document("C:/temp/lock.docx");
			// searching for a first table in document
			Table table = (Table)doc.getChild(NodeType.TABLE, 0, true);
			if(table==null) return; //exit if table not found
			for(int i=0; i < 5; i++){
				Row row = table.getLastRow(); //taking last row as a template
				//
				String level = "";
				if(i==0){
					level = "一級";
				}else if(i == 1){
					level = "二級";
				}else if(i == 2){
					level = "三級";
				}else if(i == 3){
					level = "面請";
				}else if(i == 4){
					level = "無缺失";
				}
				row.getCells().get(1).getParagraphs().get(0).getRuns().get(0).setText(level);
				
				row = (Row)row.deepClone(true); // deep cloning
				//
				table.getRows().add(row); // adding
			}
			
			
			
			//for(com.aspose.words.Cell cell: row.getCells()) {
				// changing text
				//cell.getParagraphs().get(0).getRuns().get(0).setText("New");
			//}
			
			//Merge cell vertical
			Cell cellStartRange = table.getRows().get(1).getCells().get(0);
			Cell cellEndRange = table.getRows().get(5).getCells().get(0);

			// Merge all the cells between the two specified cells into one.
			mergeCells(cellStartRange, cellEndRange);
			
			table.getRows().get(1).getCells().get(0).getFirstParagraph().getRuns().get(0).setText("業務項目一");
			//
			doc.save("C:/temp/lock2.docx");
		}catch(Exception ex){
			PortalUtil.copyRequestParameters(actionRequest, actionResponse);
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public static void mergeCells(Cell startCell, Cell endCell)
	{
	    Table parentTable = startCell.getParentRow().getParentTable();

	    // Find the row and cell indices for the start and end cell.
	    Point startCellPos = new Point(startCell.getParentRow().indexOf(startCell), parentTable.indexOf(startCell.getParentRow()));
	    Point endCellPos = new Point(endCell.getParentRow().indexOf(endCell), parentTable.indexOf(endCell.getParentRow()));
	    // Create the range of cells to be merged based off these indices. Inverse each index if the end cell if before the start cell.
	    Rectangle mergeRange = new Rectangle(Math.min(startCellPos.x, endCellPos.x), Math.min(startCellPos.y, endCellPos.y),
	            Math.abs(endCellPos.x - startCellPos.x) + 1, Math.abs(endCellPos.y - startCellPos.y) + 1);

	    for (Row row : parentTable.getRows())
	    {
	        for(Cell cell : row.getCells())
	        {
	            Point currentPos = new Point(row.indexOf(cell), parentTable.indexOf(row));

	            // Check if the current cell is inside our merge range then merge it.
	            if (mergeRange.contains(currentPos))
	            {
	                if (currentPos.x == mergeRange.x)
	                    cell.getCellFormat().setHorizontalMerge(CellMerge.FIRST);
	                else
	                    cell.getCellFormat().setHorizontalMerge(CellMerge.PREVIOUS);

	                if (currentPos.y == mergeRange.y)
	                    cell.getCellFormat().setVerticalMerge(CellMerge.FIRST);
	                else
	                    cell.getCellFormat().setVerticalMerge(CellMerge.PREVIOUS);
	            }
	        }
	    }
	}
}

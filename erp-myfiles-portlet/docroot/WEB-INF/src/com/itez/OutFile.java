package com.itez;

public class OutFile {
	public long fileId = 0;
	public long folderId = 0;
	
	public String fileNameOut = "";
	public String fileName = "";
	public String examNo = "";
	public String stageName = "";
	public String bankName = "";
	public String dateCreated = "";
	public String dateUpdated = "";
	public String fileSize = "";
	public String webDavURL = "";
	
	public String iconName = "";
}

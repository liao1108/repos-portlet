package com.itez;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.apache.http.HttpHeaders;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyFilesPortlet extends MVCPortlet {
	DataSource ds = null;
	
	String rootReports = "";
	String rootTemplates = "";
	String rootCheckOut = "";		//領出檔案編輯區
	
	Vector<OutFile> vecOut = new Vector<OutFile>();
	//
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/postgres");
	   		
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootReports = rs.getString("folder_reports");
	   			rootTemplates = rs.getString("folder_templates");
	   			rootCheckOut = rs.getString("folder_checkout");
	   		}
	   		rs.close();
	   		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
				
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			if(fdOut == null) throw new Exception("尚未設定檔案領出暫存區。");
			//
			vecOut = new Vector<OutFile>();
			//
			conn = ds.getConnection();
			//
			List<FileEntry> listFile = DLAppLocalServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), fdOut.getFolderId());
			for(FileEntry fe: listFile){
				if(fe.getDescription().contains(userFullName)){		//領出人記在 Description 欄位內
					if(fe.getTitle().contains("#")){
						String[] ss = fe.getTitle().split("#");
						//
						if(ss.length == 3){
							OutFile of = new OutFile();
							of.fileId = fe.getFileEntryId();
							of.folderId = fdOut.getFolderId();
							of.examNo = ss[0];
							of.stageName = ss[1];
							of.bankName = ErpUtil.getBankName(of.examNo, conn);
							of.fileName = ss[2];
							of.dateUpdated = ErpUtil.getDateTimeStr(fe.getModifiedDate());
							of.fileSize = String.valueOf((int)Math.round( (double)fe.getSize()/1024 ));
							of.webDavURL = URLEncoder.encode(DLUtil.getWebDavURL(themeDisplay, fdOut, fe), "utf-8");
							//
							String iconName = "file.png";
							if(of.fileName.toLowerCase().endsWith("xls")){
								iconName = "xls.png";
							}else if(of.fileName.toLowerCase().endsWith("doc") || of.fileName.toLowerCase().endsWith("docx")){
								iconName = "doc.png";
							}else if(of.fileName.toLowerCase().endsWith("ppt")){
								iconName = "ppt.png";
							}else if(of.fileName.toLowerCase().endsWith("pdf")){
								iconName = "pdf.png";
							}
							of.iconName = iconName;
							//
							vecOut.add(of);
						}
					}
				}
			}
			
			//String password = (String)PortalUtil.getHttpServletRequest(renderRequest).getSession().getAttribute(WebKeys.USER_PASSWORD);
			//
			conn = ds.getConnection();
			renderRequest.getPortletSession(true).setAttribute("vecOut", vecOut);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			long fileId = Long.parseLong(resRequest.getParameter("fileId"));
			FileEntry fe = DLAppLocalServiceUtil.getFileEntry(fileId);
			String[] ss = fe.getTitle().split("#");
			String examNo = ss[0];
			String stageName = ss[1];
			String fileName = ss[2];
			//
			resResponse.setContentType(fe.getMimeType());
			resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
			resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
			OutputStream out = resResponse.getPortletOutputStream();
			//
			InputStream is = fe.getContentStream();
			byte[] buffer = new byte[4096];
			int n;
			while ((n = is.read(buffer)) > 0) {
			    out.write(buffer, 0, n);
			}
			//
			out.flush();
			out.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	//繳回
	public void doCheckIn(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			conn = ds.getConnection();
			//
			long fileId = Long.parseLong(actionRequest.getParameter("fileId"));
			FileEntry fe = DLAppLocalServiceUtil.getFileEntry(fileId);
			//
			Folder fdOut = null;
			try{
				fdOut = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0, rootCheckOut);
			}catch(Exception _ex){
			}
			//
			ErpUtil.doCheckIn(fdOut, fe, conn, themeDisplay);
	        	//
	        	conn.close();
	        }catch(Exception ex){
	        	PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	

}

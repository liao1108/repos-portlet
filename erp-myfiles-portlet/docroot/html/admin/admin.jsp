<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.OutFile" %>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	OutFile o = (OutFile)row.getObject();
	//
	String editOnlineURL = "javascript: openWebDAVFile('" + o.webDavURL + "');";
	String copyToClipBoardURL = "javascript: copyToClipBoard('" + o.webDavURL + "');";
	String showWebDavURL = "javascript: showWebDavURL('" + o.webDavURL + "');";
%>

<script type="text/javascript">
	function openWebDAVFile(url) {
		//alert(url);
		new ActiveXObject("SharePoint.OpenDocuments.1").EditDocument(url);
		//url.execCommand("Copy")

	};
	
	function copyToClipBoard(url){
			try{
				var result = window.clipboardData.setData('Text', url);
				if(result){
					alert("已將檔案編輯網址複製到剪貼簿。");
				}else{
					alert("檔案編輯網址無法複製到剪貼簿！");
				}
			}catch(ex){
				alert("檔案編輯網址無法複製到剪貼簿！");
			}
	}
	
	function showWebDavURL(url){
		prompt("檔案編輯網址： ", url);
	}
	
</script>

<portlet:actionURL var="doCheckInURL" name="doCheckIn" >
	<portlet:param name="fileId" value="<%=String.valueOf(o.fileId) %>"/>
</portlet:actionURL>


<liferay-ui:icon-menu>
	<liferay-ui:icon image="edit" message="線上編輯" url="<%=editOnlineURL.toString() %>"/>
	<liferay-ui:icon image="edit" message="複製網址" url="<%=copyToClipBoardURL.toString() %>"/>
	<liferay-ui:icon image="edit" message="顯示網址" url="<%=showWebDavURL.toString() %>"/>
	<liferay-ui:icon image="edit" message="繳回" url="<%=doCheckInURL.toString() %>"/>
</liferay-ui:icon-menu>
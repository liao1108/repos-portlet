<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.OutFile"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
   			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
            	User user = themeDisplay.getUser();
            	//
            	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
            	//
            	Vector<OutFile> vecOut = new Vector<OutFile>();
            	if(renderRequest.getPortletSession(true).getAttribute("vecOut") != null){
            		vecOut = (Vector<OutFile>)renderRequest.getPortletSession(true).getAttribute("vecOut");
            	}
            	//
            	String errMsg = "";	
            	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
            	String successMsg = "";	
            	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
   %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doSyncDataURL" name="doSyncData" >
</portlet:actionURL>


<aui:layout>
	<aui:column cssClass="header">
		<img src="<%=renderRequest.getContextPath()%>/images/checkedOut.png" >&nbsp;您(<%=userFullName %>)所領出可直接線上編輯的檔案(包含所有進行中的檢查案)
	</aui:column>
</aui:layout>
<hr/>
<br>
<liferay-ui:search-container emptyResultsMessage="您目前沒有領出檔案。" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecOut, searchContainer.getStart(), searchContainer.getEnd());
			total = vecOut.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.OutFile" keyProperty="fileId" modelVar="outFile">
		<portlet:resourceURL var="downloadFileURL" >
  			<portlet:param name="fileId"  value="<%=String.valueOf(outFile.fileId) %>"/>
		</portlet:resourceURL>

		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/<%=outFile.iconName %>"/>
		</liferay-ui:search-container-column-text>
		<liferay-ui:search-container-column-text name="檢查證號" property="examNo" />
		<liferay-ui:search-container-column-text name="受檢機構" property="bankName"/>
		<liferay-ui:search-container-column-text name="作業階段" property="stageName"/>
		<liferay-ui:search-container-column-text name="檔名" property="fileName"  href="<%=downloadFileURL.toString()%>"/>
		<liferay-ui:search-container-column-text name="KB" property="fileSize" />
		<liferay-ui:search-container-column-text name="最近更新時間" property="dateUpdated" />
		
		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center" />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

package com.itez;

import com.liferay.portal.kernel.log.Log;


import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.tomcat.jdbc.pool.DataSource;

public class StudyHisPortlet extends GenericPortlet {
	String eCourseRootFoler = "";	//課程根目錄
	ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	String tree = "";

	public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	//
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	    	//
	    	eCourseRootFoler = getInitParameter("ecourse-root-folder");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
   		Connection conn = null;
   		//
   		tree = "";
    	try{
	    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		//long companyId = themeDisplay.getCompanyId();
	   		long groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//
	   		//取得登入者的機構名稱
	   		Organization myOrg = this.getDefaultOrg(userId);
			if(myOrg == null){
				throw new Exception("您尚未歸屬於任何組織 !");
			}
			//
			conn = ds.getConnection(); 
	   		//主題暫存
			Hashtable<Subject, Vector<Topic>> htSubject = new Hashtable<Subject, Vector<Topic>>();
	   		//
	   		List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			if(fd.getName().equals(eCourseRootFoler)){
	   				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
	   				for(Folder fd2: list2){			//主題
	   					//單元
						Vector<Topic> vecTopic = new Vector<Topic>();
						//
	   					List<Folder> list3 = DLAppServiceUtil.getFolders(groupId, fd2.getFolderId());
	   					for(Folder fd3: list3){		//單元
							Topic topic = new Topic();
							topic.name = fd3.getName();
							topic.id = fd3.getFolderId();
							topic.status = "";
							//
							vecTopic.add(topic);
	   					}
	   					//
	   					if(vecTopic.size() > 0){
							Subject subject = new Subject();
							subject.name = fd2.getName();
							subject.status = "";
							//
							htSubject.put(subject, vecTopic);
						}
	   				}
	   			}
	   		}
	   		Vector<Subject> vecSubject = new Vector<Subject>();
 			Vector<String> vecSort = new Vector<String>();
 			Enumeration<Subject> en = htSubject.keys();
 			while(en.hasMoreElements()){
 				vecSort.add(en.nextElement().name);
 			}
 			Collections.sort(vecSort);
 			for(String s: vecSort){
 				en = htSubject.keys();
 				while(en.hasMoreElements()){
 					Subject sb = en.nextElement();
 					if(sb.name.equals(s)){
 						vecSubject.add(sb);
 					}
 				}
 			}
			//
			
			makeUpOrgTree(renderRequest, myOrg, htSubject, vecSubject, 0, conn);
        }catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
        }
        //System.out.println(topicTree);
		renderRequest.setAttribute("tree", tree);
    	//
   		renderRequest.setAttribute("errMsg", errMsg);
    	//
        include(viewJSP, renderRequest, renderResponse);
    }
    
    
    private void makeUpOrgTree(RenderRequest renderRequest,
    							Organization org, 
    							Hashtable<Subject, Vector<Topic>> htSubject,
    							Vector<Subject> vecSubject,
    							int loopLevel,
    							Connection conn) throws Exception{
		String strOpen = "closed";
    	if(loopLevel < 3){
    		strOpen = "open";
		}
    	String iconOrg = "<img src='" + renderRequest.getContextPath() + "/images/org.png'>";
		//String iconMembers = "<img src='" + renderRequest.getContextPath() + "/images/members.jpg'>";
		//
		boolean withSubOrg = false;
		//boolean withDirectMember = false;
		//
		List<Organization> subOrgs = org.getSuborganizations();
		if(subOrgs.size() > 0){	//底下尚有次組織
			withSubOrg = true;
		}
		//
		String strLI = iconOrg + "&nbsp;" + org.getName();
		//
		List<User> listUser = UserLocalServiceUtil.getOrganizationUsers(org.getOrganizationId());
		//if(listUser.size() > 0){
		//	withDirectMember = true;
		//}
		//
		if(!withSubOrg){	//最底層組織
			strLI += "&nbsp;(成員 " + listUser.size() + " 人)";
		}
		
    	tree += "<li class='" + strOpen + "'><span class='subject'>" + strLI + "</span>";
		tree += "<ul>";
		//if(withDirectMember && withSubOrg){
		//	tree += "<li class='" + strOpen + "'><span class='subject'>" + iconMembers + "&nbsp;" + "直屬成員 " + listUser.size() + " 人" + "</span>";
		//	tree += "<ul>";
		//	createCourseTree(renderRequest, listUser, htSubject, vecSubject, conn);
		//	tree += "</ul>";
		//	tree += "</li>";
		//}else	if(withDirectMember){
		//	createCourseTree(renderRequest, listUser, htSubject, vecSubject, conn);
		//}
		if(withSubOrg){
			for(Organization o: subOrgs){
				this.makeUpOrgTree(renderRequest, o, htSubject, vecSubject, loopLevel+1, conn);
			}
		}else{
			createCourseTree(renderRequest, listUser, htSubject, vecSubject, conn);
		}
		//
		tree += "</ul>";
		tree += "</li>";
    }

    private void createCourseTree(RenderRequest renderRequest,
    							  List<User> listUser,
    							  Hashtable<Subject,
    							  Vector<Topic>> htSubject,
    							  Vector<Subject> vecSubject,
    							  Connection conn) throws Exception{
    	String strOpen = "closed";
    	//
		String iconSubject = "<img src='" + renderRequest.getContextPath() + "/images/subject.png'>";
		String iconTopic = "<img src='" + renderRequest.getContextPath() + "/images/topic.png'>";
		String iconMember = "<img src='" + renderRequest.getContextPath() + "/images/member.jpg'>";
		
		String iconPass = "<img src='" + renderRequest.getContextPath() + "/images/red_box.png'>";
		String iconQuizing = "<img src='" + renderRequest.getContextPath() + "/images/purple_box.png'>";
		String iconRead = "<img src='" + renderRequest.getContextPath() + "/images/blue_box.png'>";
		String iconReading = "<img src='" + renderRequest.getContextPath() + "/images/orange_box.png'>";
		String iconUnRead = "<img src='" + renderRequest.getContextPath() + "/images/green_box.png'>";
		//
    	for(Subject subject: vecSubject){
    		tree += "<li class='" + strOpen + "'><span class='subject'>" + iconSubject + "&nbsp;" + subject.name + "</span>";
    		tree += "<ul>";
    		Vector<Topic> vecTopic = htSubject.get(subject);
    		for(Topic topic: vecTopic){
    			Vector<User> vecPass = new Vector<User>();
    			Hashtable<User, Vector<QuizRec>> htQuizing = new Hashtable<User, Vector<QuizRec>>();
    			Vector<User> vecRead = new Vector<User>();
    			Hashtable<User, Vector<ReadRec>> htReading = new Hashtable<User, Vector<ReadRec>>();
    			Vector<User> vecUnRead = new Vector<User>();
    			for(User u: listUser){
    				Vector<QuizRec> vecQuizing = new Vector<QuizRec>();
    				Vector<ReadRec> vecReading = new Vector<ReadRec>();
    				//
    				String sql = "SELECT * FROM trace_quiz WHERE u_id=? AND topic_uuid=? AND completed = 1";
    				PreparedStatement ps = conn.prepareStatement(sql);
    				ps.setLong(1, u.getUserId());
    				ps.setLong(2, topic.id);
    				ResultSet rs = ps.executeQuery();
    				if(rs.next()){	//加入已通過檢測
    					vecPass.add(u);
    				}else{			//未通過考試
    					sql = "SELECT * FROM trace_quiz WHERE u_id=? AND topic_uuid=? ORDER BY quiz_start_time";
    					ps = conn.prepareStatement(sql);
    					ps.setLong(1, u.getUserId());
        				ps.setLong(2, topic.id);
        				rs = ps.executeQuery();
        				while(rs.next()){
        					QuizRec qr = new QuizRec();
        					qr.quizStartTime = rs.getString("quiz_start_time");
        					qr.quizEndTime = rs.getString("quiz_end_time");
        					qr.totScore = rs.getInt("tot_score");
        					qr.gotScore = rs.getInt("got_score");
        					vecQuizing.add(qr);
        				}
        				if(vecQuizing.size() > 0){
        					htQuizing.put(u, vecQuizing);
        				}
    				}
    				rs.close();
    				//
    				if(vecPass.size()==0 && vecQuizing.size() == 0){	//尚未進入檢測
    					sql = "SELECT * FROM trace_topic WHERE u_id=? AND topic_uuid=? AND start_time IS NOT NULL AND start_time <> '' AND end_time IS NOT NULL AND end_time <> ''";
    					ps = conn.prepareStatement(sql);
    					ps.setLong(1, u.getUserId());
        				ps.setLong(2, topic.id);
        				rs = ps.executeQuery();
        				if(rs.next()){
        					vecRead.add(u);
        				}else{
        					sql = "SELECT * FROM trace_reading WHERE u_id=? AND topic_uuid=? ORDER BY start_time";
        					ps = conn.prepareStatement(sql);
        					ps.setLong(1, u.getUserId());
            				ps.setLong(2, topic.id);
            				rs = ps.executeQuery();
            				while(rs.next()){
            					ReadRec rr = new ReadRec();
            					rr.itemId = rs.getLong("item_uuid");
            					rr.readStartTime = rs.getString("start_time");
            					rr.readEndTime = rs.getString("end_time");
            					rr.readSeconds = rs.getInt("read_seconds");
            					vecReading.add(rr);
            				}
            				if(vecReading.size() > 0){
            					htReading.put(u, vecReading);
            				}
        				}
        				rs.close();
    				}
    				//
    				if(vecPass.size()==0 && vecQuizing.size() == 0 && vecRead.size()==0 && vecReading.size()==0){
    					vecUnRead.add(u);
    				}
    			}
    			//
    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconTopic + "&nbsp;" + topic.name + "</span>";
    			tree += "<ul>";
    			{
	    			//1已通過檢測
	    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconPass + "&nbsp;" +  "5.已通過檢測 (" + vecPass.size() + "/" + listUser.size() + ")" + "</span>";
	    			if(vecPass.size() > 0){
		    			tree += "<ul>";
	    				for(User u: vecPass){
		    				tree += "<li>" + iconMember + "&nbsp;" + u.getFullName() + "</li>";
		    			}
	    				tree += "</ul>";
	    			}
	    			tree += "</li>";
	    			//2檢測中
	    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconQuizing + "&nbsp;" +  "4.檢測中 (" + htQuizing.size() + "/" + listUser.size() + ")" + "</span>";
	    			if(htQuizing.size() > 0){
		    			tree += "<ul>";
		    			Enumeration<User> en = htQuizing.keys(); 
	    				while(en.hasMoreElements()){
	    					User _u = en.nextElement();
		    				tree += "<li class='closed'>";
		    				tree += "<span class='subject'>" + iconMember + "&nbsp;" + _u.getFullName() + "</span>";
		    				//
		    				tree += "<ul><li>";
		    				tree += "<table border = '1' width='100%'>";
		    				tree += "<tr>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "起始時間" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "結束時間" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "累計通過比例" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "本次題數與通過比例" + "</td>";
		    				tree += "</tr>";
		    				int accuScore = 0;
		    				int totScore = 0;
		    				Vector<QuizRec> vecQuizing = htQuizing.get(_u);
		    				for(QuizRec qr: vecQuizing){
		    					if(qr.totScore > totScore) totScore = qr.totScore;
		    					accuScore += qr.gotScore;
		    					if(accuScore > totScore) accuScore = totScore;
		    					//
		    					tree += "<tr>";
			    				tree += "<td align='center'>" + qr.quizStartTime + "</td>";
			    				tree += "<td align='center'>" + qr.quizEndTime + "</td>";
			    				tree += "<td align='center'>" + (accuScore/10) + "/" + (totScore/10) + "</td>";
			    				tree += "<td align='center'>" + (qr.gotScore/10) + "/" + (qr.totScore/10) + "</td>";
			    				tree += "</tr>";
		    				}
		    				tree += "</table>";
		    				tree += "</li></ul>";
		    				//
		    				tree += "</li>";
		    			}
	    				tree += "</ul>";
	    			}
	    			tree += "</li>";	    			
	    			//3閱讀完成
	    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconRead + "&nbsp;" + "3.已閱讀完成 (" +  vecRead.size() + "/" + listUser.size() + ")" + "</span>";
	    			if(vecRead.size() > 0){
	    				tree += "<ul>";
	    				for(User u: vecRead){
		    				tree += "<li>" + iconMember + "&nbsp;" + u.getFullName() + "</li>";
		    			}
	    				tree += "</ul>";
	    			}
	    			tree += "</li>";
	    			//4閱讀中
	    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconReading + "&nbsp;" + "2.閱讀中 (" + htReading.size() + "/" + listUser.size() + ")" + "</span>";
	    			if(htReading.size() > 0){
		    			tree += "<ul>";
		    			Enumeration<User> en = htReading.keys(); 
	    				while(en.hasMoreElements()){
	    					User _u = en.nextElement();
		    				tree += "<li class='closed'>";
		    				tree += "<span class='subject'>" + iconMember + "&nbsp;" + _u.getFullName() + "</span>";
		    				//
		    				tree += "<ul><li>";
		    				tree += "<table border = '1' width='100%'>";
		    				tree += "<tr>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "起始時間" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "結束時間" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "累計閱讀時間" + "</td>";
		    				tree += "<td style='background-color:lightblue' align='center'>" + "本次閱讀時間" + "</td>";
		    				tree += "</tr>";
		    				int accuSeconds = 0;
		    				Vector<ReadRec> vecReading = htReading.get(_u);
		    				for(ReadRec rr: vecReading){
		    					accuSeconds += rr.readSeconds;
		    					//
		    					tree += "<tr>";
			    				tree += "<td align='center'>" + rr.readStartTime + "</td>";
			    				tree += "<td align='center'>" + rr.readEndTime + "</td>";
			    				tree += "<td align='center'>" + this.getReadTimeString(accuSeconds) + "</td>";
			    				tree += "<td align='center'>" + this.getReadTimeString(rr.readSeconds) + "</td>";
			    				tree += "</tr>";
		    				}
		    				tree += "</table>";
		    				tree += "</li></ul>";
		    				//
		    				tree += "</li>";
		    			}
	    				tree += "</ul>";
	    			}	    			
	    			tree += "</li>";
	    			//5
	    			tree += "<li class= '" + strOpen + "'><span class='subject'>" + iconUnRead + "&nbsp;" + "1.未閱讀 (" + vecUnRead.size() + "/" + listUser.size() + ")" + "</span>";
	    			if(vecUnRead.size() > 0){
	    				tree += "<ul>";
	    				for(User u: vecUnRead){
		    				tree += "<li>" + iconMember + "&nbsp;" + u.getFullName() + "</li>";
		    			}
	    				tree += "</ul>";
	    			}
	    			tree += "</li>";
    			}
    			tree += "</ul>";
    			tree += "</li>";
    		}
    		tree += "</ul>";
    		tree += "</li>";
    	}
    }
    
    
    
    private Organization getDefaultOrg(long userId) throws Exception{
    	Organization org = null;
    	List<Organization> orgs = OrganizationServiceUtil.getUserOrganizations(userId);
    	if(orgs.size() > 0){
    		org = orgs.get(0);
    	}
    	return org;
    }
    
    private String getReadTimeString(int readSeconds){
    	String ret = "0 秒";
    	if(readSeconds > 60){
    		ret = (readSeconds % 60) + " 秒";
    		int mm = readSeconds / 60;
    		if(mm > 60){
    			ret = (mm / 60) + " 時 " + (mm % 60) + " 分 " + ret;
    		}else{
    			ret = mm + " 分 " + ret;
    		}
    	}else{
    		ret = readSeconds + " 秒";
    	}
    	//
    	return ret;
    }
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(StudyHisPortlet.class);

}

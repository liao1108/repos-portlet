package com.itez;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jdbc.pool.DataSource;

import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.sql.*;

/**
 * Servlet implementation class UpdateReadSecondServlet
 */
public class UpdateReadSecondServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds = null;
	
    public UpdateReadSecondServlet() {
        super();
    }

    public void init() throws ServletException {
    		try{
    			Context initContext = new InitialContext();
    			Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
    }    

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = null;
		try{
			long userId = Long.parseLong(request.getParameter("userId"));
			//
			String screenName = "";
			String email = "";
			//
			try{
				User user = UserLocalServiceUtil.getUser(userId);
				screenName = user.getScreenName();
				email = user.getEmailAddress();
			}catch(Exception _ex){
			}
			//
			long topicID = 0;
			if(request.getParameter("topicID") != null){
				topicID = Long.parseLong(request.getParameter("topicID").trim());
			}
			String topicTitle = "";
			try{
				topicTitle = DLAppLocalServiceUtil.getFolder(topicID).getName();
			}catch(Exception _ex){
			}
			
			long itemID = 0;
			if(request.getParameter("itemID2") != null){
				itemID = Long.parseLong(request.getParameter("itemID2").trim());
			}
			String itemTitle = "";
			try{
				itemTitle = DLAppLocalServiceUtil.getFolder(itemID).getName();
			}catch(Exception _ex){
			}
			
			System.out.println("Servlet get item id: " + itemID);
			//書籤頁
			int lastPage = 0;
			if(request.getParameter("currentFrame") != null){
				try{
					lastPage = Integer.parseInt(request.getParameter("currentFrame").trim());
				}catch(Exception ex){
					
				}
			}
			//
			int totalPages = 0;
			if(request.getParameter("totalFrame") != null){
				try{
					totalPages = Integer.parseInt(request.getParameter("totalFrame").trim());
				}catch(Exception ex){
					
				}
			}
			//目前時間 
			String sqlCurrDate = (new java.sql.Timestamp(new java.util.Date().getTime())).toString().substring(0, 19);
			//
			String sqlStartDate = "";	//本次開始閱讀時間
			if(request.getParameter("sqlStartDate") != null){
				sqlStartDate = request.getParameter("sqlStartDate");
			}
			
			//累積閱讀秒數
			//int readSeconds = Integer.parseInt(request.getParameter("readSeconds"));
			
			//進入閱讀時的秒數
			long startTime = Long.parseLong(request.getParameter("startTime"));
			long currTime = System.currentTimeMillis();
			long tDelta = currTime - startTime;
			int elapsedSeconds = (int)(tDelta / 1000.0);
			
			//if(elapsedSeconds < readSeconds){
			//	elapsedSeconds = readSeconds;
			//}
			//計算之前已累積閱讀秒數
			conn = ds.getConnection();
			
			//int otherReadSeconds = 0;
			//String sql = "SELECT SUM(read_seconds) FROM trace_reading WHERE u_id=? AND topic_uuid=?";
			//PreparedStatement ps = conn.prepareStatement(sql);
			//ps.setLong(1, userId);
			//ps.setLong(2, topicID);
			//ResultSet rs = ps.executeQuery();
			//if(rs.next()){
			//	otherReadSeconds = rs.getInt(1);
			//}
			//rs.close();
			//本次累計閱讀秒數
			//int thisReadSeconds = readSeconds - otherReadSeconds;
			//
			if(userId > -1){
				//更新課程項目最後一頁
				String sql = "SELECT * FROM trace_reading WHERE u_id=? AND topic_uuid=? AND item_uuid=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, topicID);
				ps.setLong(3, itemID);
				//ps.setString(4, sqlStartDate);
				ResultSet rs = ps.executeQuery();
				if(rs.next()){
					int origLastPage = rs.getInt("last_page");
					if(lastPage < origLastPage){
						lastPage = origLastPage;
					}
					//
					int completed = rs.getInt("completed");
					if(totalPages == lastPage){
						completed = 1;
					}
					//
					int origReadSeconds = rs.getInt("read_seconds");
					//
					sql = "UPDATE trace_reading SET read_seconds=?,"			//1
													+ " last_page=?,"		//2
													+ " end_time=?,"			//3
													+ " completed=?,"		//4
													+" topic_title=?,"		//5
													+ " item_title=?,"		//6
													+ " u_name=?,"			//7
													+ " u_email=? " 			//8
													+ " WHERE u_id=? AND topic_uuid=? AND item_uuid=?";
					ps = conn.prepareStatement(sql);
					ps.setInt(1, origReadSeconds + elapsedSeconds);		//增秒數
					ps.setInt(2, lastPage);
					ps.setString(3, sqlCurrDate);
					ps.setInt(4, completed);
					ps.setString(5, topicTitle);
					ps.setString(6, itemTitle);
					ps.setString(7, screenName);
					ps.setString(8, email);
					
					ps.setLong(9, userId);
					ps.setLong(10, topicID);
					ps.setLong(11, itemID);
					//ps.setString(12, sqlStartDate);
					ps.execute();
				}else{
					int completed = 0;
					if(totalPages == lastPage){
						completed = 1;
					}
					
					sql = "INSERT INTO trace_reading (u_id, start_time, end_time, read_seconds, last_page, completed, topic_uuid, item_uuid, topic_title, item_title, u_name, u_email) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
					ps = conn.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setString(2, sqlStartDate);
					ps.setString(3, sqlCurrDate);
					ps.setInt(4, elapsedSeconds);			//遞增秒
					ps.setInt(5, lastPage);
					ps.setInt(6, completed);
					ps.setLong(7, topicID);
					ps.setLong(8, itemID);
					ps.setString(9, topicTitle);
					ps.setString(10, itemTitle);
					ps.setString(11, screenName);
					ps.setString(12, email);
					ps.execute();
					
					//
					sql = "SELECT * FROM trace_topic WHERE u_id=? AND topic_uuid=?";
					ps = conn.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setLong(2, topicID);
					rs = ps.executeQuery();
					if(!rs.next()){
						sql = "INSERT INTO trace_topic (u_id, topic_uuid, start_time) VALUES (?,?,?)";
						ps = conn.prepareStatement(sql);
						ps.setLong(1, userId);
						ps.setLong(2, topicID);
						ps.setString(3, sqlStartDate);
						ps.execute();
					}
					rs.close();
				}
				rs.close();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
	}

	/*
	public Connection getConnection(String dbDriver,
											String dbURL,
											String dbUser,
											String dbPassword) throws Exception {
        Class.forName(dbDriver);
        //
        java.util.Properties connProperties = new java.util.Properties();
        connProperties.put("user", dbUser);
        connProperties.put("password", dbPassword);
        connProperties.put("autoReconnect", "true");
        connProperties.put("maxReconnects", "4");
        //
        return DriverManager.getConnection(dbURL, connProperties);
   }
   */
}

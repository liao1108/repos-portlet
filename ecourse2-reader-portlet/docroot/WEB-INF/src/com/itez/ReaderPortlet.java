package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletContext;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.jdbc.pool.DataSource;

public class ReaderPortlet extends GenericPortlet {
	DataSource ds = null;
	String topicPath = "";
	String topicFullViewPath = "";
		
	protected String viewJSP;
    //protected String viewJSP2;
    protected String viewJSP3;
    
    private static Log _log = LogFactoryUtil.getLog(ReaderPortlet.class);
    
    PortletContext portletContext = null;
    
	//ECourseUtils utils = new ECourseUtils();
	
    public void init() {
    	Connection conn = null;
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	//viewJSP2 = getInitParameter("view-jsp2");
	    	viewJSP3 = getInitParameter("view-jsp3");
	    	//
	    	Context initContext = new InitialContext();
    		Context envContext  = (Context)initContext.lookup("java:/comp/env");
		   	ds = (DataSource)envContext.lookup("jdbc/efeb");
		   	//
		   	topicPath = getInitParameter("topic-path");
		    topicFullViewPath = getInitParameter("topic-fullview-path");
		    //
		    //conn = ds.getConnection();
	        //String sql = "";
	        //PreparedStatement ps = null;
		    //try{
		    //	sql = "ALTER TABLE trace_reading ADD u_name VARCHAR(50)";
		    //	ps = conn.prepareStatement(sql);
		    //	ps.execute();
		    //}catch(Exception _ex){
		    //}
		    
	        //try{
	    	//	sql = "ALTER TABLE trace_reading ADD u_email VARCHAR(50)";
	    	//	ps = conn.prepareStatement(sql);
	    	//	ps.execute();
	        //}catch(Exception _ex){
	        //}
		    
		    //try{
		    //	sql = "ALTER TABLE trace_reading ADD topic_title VARCHAR(100)";
		    //	ps = conn.prepareStatement(sql);
		    //	ps.execute();
		    //}catch(Exception _ex){
		    //}
		    
		    //try{
		    //	sql = "ALTER TABLE trace_reading ADD item_title VARCHAR(100)";
		    //	ps = conn.prepareStatement(sql);
		    //	ps.execute();
		    //}catch(Exception _ex){
		    //}
		    
		    //try{
	    	//	sql = "ALTER TABLE trace_reading ADD read_times INT DEFAULT 0";
	    	//	ps = conn.prepareStatement(sql);
	    	//	ps.execute();
		    //}catch(Exception _ex){
		    //}
		    //補填User資料
		    //ResultSet rs = null;
		    /*
		    try{
			    sql = "SELECT u_id FROM trace_reading WHERE u_name IS NULL OR u_name = '' GROUP BY u_id";
			    ps = conn.prepareStatement(sql);
			    rs = ps.executeQuery();
		        while(rs.next()){
		       		int uid = rs.getInt(1);
		       		//
		       		User u = UserLocalServiceUtil.getUser(uid);
		       		String screenName = u.getScreenName();
		       		String email = "";
		       		if(u.getEmailAddress() != null) email = u.getEmailAddress();
		       		//
		       		System.out.print("Updating user name, email of uid: " + uid + " to " + screenName + "," + email + " ...");
		        		
		       		sql = "UPDATE trace_reading SET u_name = ?, u_email=? WHERE u_id = ?";
		       		ps = conn.prepareStatement(sql);
		       		ps.setString(1, screenName);
		       		ps.setString(2, email);
		       		ps.setInt(3, uid);
		       		ps.execute();
		        		
		       		System.out.println("Ok");
		        }
		    }catch(Exception _ex){
		    		
		    }
		    */
		    //補填Topic資料
		    /*
		    try{
		        sql = "SELECT topic_uuid FROM trace_reading WHERE topic_title IS NULL OR topic_title = '' GROUP BY topic_uuid";
		        ps = conn.prepareStatement(sql);
		        rs = ps.executeQuery();
		        while(rs.next()){
		        		int topicId = rs.getInt(1);
		        		//
		        		try{
		        			if(DLAppLocalServiceUtil.getFolder(topicId) != null){
		        				System.out.print("Updating topic title of id: " + topicId + " to " + DLAppLocalServiceUtil.getFolder(topicId).getName() + " ...");
				        		
		        				sql = "UPDATE trace_reading SET topic_title=? WHERE topic_uuid = ?";
		        				ps = conn.prepareStatement(sql);
		        				ps.setString(1, DLAppLocalServiceUtil.getFolder(topicId).getName());
		        				ps.setInt(2, topicId);
		        				ps.execute();
		        				
		        				System.out.println("Ok");
		        			}
		        		}catch(Exception _ex){
		        			
		        		}
		        }
		    }catch(Exception _ex){
		    	
		    }
	        */
		    
		    //補填Item資料
	        /*
		    try{
			    sql = "SELECT item_uuid FROM trace_reading WHERE item_title IS NULL OR item_title = '' GROUP BY item_uuid";
		        ps = conn.prepareStatement(sql);
		        rs = ps.executeQuery();
		        while(rs.next()){
		        		int itemId = rs.getInt(1);
		        		//
		        		try{
		        			if(DLAppLocalServiceUtil.getFolder(itemId) != null){
		        				System.out.print("Updating item title of id: " + itemId + " to " + DLAppLocalServiceUtil.getFolder(itemId).getName() + " ...");
				        		
		        				sql = "UPDATE trace_reading SET item_title=? WHERE item_uuid = ?";
		        				ps = conn.prepareStatement(sql);
		        				ps.setString(1, DLAppLocalServiceUtil.getFolder(itemId).getName());
		        				ps.setInt(2, itemId);
		        				ps.execute();
		        				
		        				System.out.println("Ok");
		        			}
		        		}catch(Exception _ex){
		        			
		        		}
		        }
	        }catch(Exception _ex){
	        	
	        }
	        */
	        //
	        //rs.close();
	        //conn.close();
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}finally{
    		try{
    			if(conn != null) conn.close();
    		}catch(Exception _ex){
    			
    		}
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	renderResponse.setContentType("text/html");
    	//
    	String errMsg = "";
    	Connection conn = null;
    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	//
    	portletContext = renderRequest.getPortletSession().getPortletContext();
    	//XSS 過濾
    	//String fullURL = PortalUtil.getCurrentURL(renderRequest);
    	//if(fullURL.contains("<") || fullURL.contains(">")){
    	//	throw new PortletException("網址列含有跨腳本攻擊標籤!");
    	//}else if(!fullURL.equals(this.cleanXSS(fullURL))){
	    //	throw new PortletException("網址列含有跨腳本攻擊標籤!");
	    //}
    	//
    	boolean isHTML5 = false;
    	//
    	long topicID = 0;
    	try{
    		//topicID = Long.parseLong(renderRequest.getPortletSession(true).getAttribute("topicID").toString());
    		topicID = Long.parseLong(LiferaySessionUtil.getGlobalSessionAttribute("currTopicID", renderRequest).toString());
    	}catch(Exception _ex){
    	}
    	//
    	long itemID = 0;
    	try{
    		itemID = Long.parseLong(LiferaySessionUtil.getGlobalSessionAttribute("currItemID", renderRequest).toString());
    	}catch(Exception _ex){
    	}
    	//
    	String fullView = "false";
    	try{
    		fullView = LiferaySessionUtil.getGlobalSessionAttribute("fullview", renderRequest).toString();
    	}catch(Exception _ex){
    	}
    	//
    	Calendar cal = Calendar.getInstance();
    	String sqlStartDate = cal.get(Calendar.YEAR) + "-" +
    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
    							String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
    							String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
    							String.format("%02d", cal.get(Calendar.SECOND));
    	//String sqlEndDate = "";
    	int readSeconds = 0;	
    	int itemCompleted = -1;	//本項目是否已閱讀完成
    	int swfBookmarkPage = -1;
    	try{
			long userId = -1;
			//
			//String screenName = "";
			//String email = "";
			try{
				User u = PortalUtil.getUser(renderRequest);
				userId = u.getUserId();
				//screenName = u.getScreenName();
				//if(u.getEmailAddress() != null) email = u.getEmailAddress();
			}catch(Exception _ex){
			}
			//
			List<Organization> orgs = OrganizationLocalServiceUtil.getUserOrganizations(userId);
			String orgName = "";
			if(orgs.size() > 0){
				orgName = orgs.get(0).getName();
			}
			//
			//if(renderRequest.getParameter("itemID") != null){
			//	itemID = Long.parseLong(renderRequest.getParameter("itemID"));
			//}
			if(renderRequest.getParameter("sqlStartDate") != null){
	    		sqlStartDate = this.getParameterXSS(renderRequest, "sqlStartDate");
	    	}
	    	if(renderRequest.getParameter("itemCompleted") != null){
	    		try{
	    			itemCompleted = Integer.parseInt(renderRequest.getParameter("itemCompleted"));
	    		}catch(Exception ex){
	    		
	    		}
	    	}
	    	if(renderRequest.getParameter("swfBookmarkPage") != null){
	    		try{
	    			swfBookmarkPage = Integer.parseInt(renderRequest.getParameter("swfBookmarkPage"));
	    		}catch(Exception ex){
	    		}
	    	}
	    	//
	    	Item currItem = new Item();
	    	//
	    	Folder fdTopic = DLAppLocalServiceUtil.getFolder(topicID);
	    	Folder fdItem = null;
	    	//
	    	if(itemID == 0){
	    		Hashtable<String, Long> ht = new Hashtable<String, Long>();
	    		Vector<String> vec = new Vector<String>();
	    		//
	    		List<Folder> list = DLAppLocalServiceUtil.getFolders(themeDisplay.getScopeGroupId(), fdTopic.getFolderId());
	    		for(Folder fd: list){
					vec.add(fd.getName());
					ht.put(fd.getName(), fd.getFolderId());
				}
				Collections.sort(vec);
				if(vec.size() > 0){
					itemID = ht.get(vec.get(0));
				}
	    	}
	    	//
	    	if(itemID != 0){
	    		fdItem = DLAppLocalServiceUtil.getFolder(itemID);
	    		currItem.name = fdItem.getName();
	    		currItem.id = fdItem.getFolderId();
	    	}else{
	    		throw new Exception("Item not exist !");
	    	}
	    	//
	    	Folder folderHtml5 = null;
	    	try{
	    		for(Folder folder: DLAppLocalServiceUtil.getFolders(themeDisplay.getScopeGroupId(), fdItem.getFolderId())){
	    			if(!folder.getName().equalsIgnoreCase("html5")) continue;
	    			//
	    			folderHtml5 = folder;
	    			isHTML5 = true;
	    			break;
	    		}
	    	}catch(Exception ex){
	    	}
	    	//
	    	if(folderHtml5 == null){
	    		List<FileEntry> list = DLAppLocalServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), fdItem.getFolderId());
				//再找 SWF
				for(FileEntry f: list){
					if(!f.getExtension().toLowerCase().endsWith("swf")) continue;
					//
					currItem.swfURI = "/documents/" + themeDisplay.getScopeGroupId() + "/" + f.getUuid();
					currItem.id = f.getFileEntryId();
					break;
				}				
			}
			//
			currItem.name = fdItem.getName();
			//
			if(folderHtml5 == null && currItem.swfURI.isEmpty()){
				throw new Exception("Course file does not exist !");
			}
    		//
			String itemPath = fdTopic.getParentFolder().getName() + "/" + fdTopic.getName() + "/" + fdItem.getName();
			//
	    	conn = ds.getConnection();
	    	//累加閱讀次數
	    	int clickTimes = 0;
	    	String sql = "CREATE TABLE IF NOT EXISTS topic_click (" +
	    				  		"topic_id int(11) NOT NULL, " +
	    				  		"click_times int(11), PRIMARY KEY (topic_id))";
	    	PreparedStatement ps = conn.prepareStatement(sql);
	    	ps.execute();
	    	//
	    	sql = "SELECT click_times FROM topic_click WHERE topic_id = ?";
	    	ps = conn.prepareStatement(sql);
	    	ps.setLong(1, topicID);
	    	ResultSet rs = ps.executeQuery();
	    	if(rs.next()){
	    		clickTimes = rs.getInt("click_times") + 1;
	    		//
	    		sql = "UPDATE topic_click SET click_times = ? WHERE topic_id=?";
	    		ps = conn.prepareStatement(sql);
	    		ps.setInt(1, clickTimes);
	    		ps.setLong(2, topicID);
	    		ps.execute();
	    	}else{
	    		sql = "INSERT INTO topic_click (topic_id, click_times) VALUES (?,?)";
	    		ps = conn.prepareStatement(sql);
	    		ps.setLong(1, topicID);
	    		ps.setInt(2, 1);
	    		ps.execute();
	    	}
	    	rs.close();
	    		
	    	//統計本課程累計閱讀秒數
	    	//String sql = "SELECT SUM(read_seconds) FROM trace_reading WHERE u_id=? AND topic_uuid=? AND item_uuid=?";
	    	//統計本單元累計閱讀秒數
	    	sql = "SELECT SUM(read_seconds) FROM trace_reading WHERE u_id=? AND topic_uuid=?";
	    	ps = conn.prepareStatement(sql);
	    	ps.setLong(1, userId);
	    	ps.setLong(2, topicID);
	    	//ps.setLong(3, itemID);
	    	rs = ps.executeQuery();
	    	if(rs.next()){
	    		readSeconds = rs.getInt(1);
	    	}
	    	rs.close();
	    	//
	    	if(itemCompleted == -1){	//剛進到閱讀畫面
	    		sql = "SELECT * FROM trace_reading WHERE u_id=? AND topic_uuid=? AND item_uuid=? AND completed = 1";
	    		ps = conn.prepareStatement(sql);
	    		ps.setLong(1, userId);
	    		ps.setLong(2, topicID);
	    		ps.setLong(3, itemID);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			itemCompleted = 1;
	    		}else{
	    			itemCompleted = 0;
	    		}
	    		rs.close();
	    	}
	    	//如果是剛剛進到閱讀畫面, 找出最後一次閱讀到的頁次
	    	if(swfBookmarkPage == -1){
	    		sql = "SELECT last_page FROM trace_reading WHERE u_id=? AND topic_uuid=? AND item_uuid=? ORDER BY start_time DESC";
	    		ps = conn.prepareStatement(sql);
	    		ps.setLong(1, userId);
	    		ps.setLong(2, topicID);
	    		ps.setLong(3, itemID);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			swfBookmarkPage = rs.getInt(1);
	    		}
	    		rs.close();
	    	}
	    	//找出本單元最早進入閱讀時間
	    	String sqlStartDateTopic = sqlStartDate;
	    	//
	    	sql = "SELECT start_time FROM trace_reading WHERE u_id=? AND topic_uuid=? ORDER BY start_time";
	    	ps = conn.prepareStatement(sql);
	    	ps.setLong(1, userId);
	    	ps.setLong(2, topicID);
	    	rs = ps.executeQuery();
	    	if(rs.next()){
	    		sqlStartDateTopic = rs.getString("start_time");
	    	}
	    	rs.close();
			//
	    	java.sql.Timestamp sqlQuizEndDate = null;
	    	if(sqlStartDateTopic != null){
	    		sql = "SELECT quiz_end_time FROM trace_quiz WHERE u_id=? AND topic_uuid=?";
	    		ps = conn.prepareStatement(sql);
	    		ps.setLong(1, userId);
	    		ps.setLong(2, topicID);
	    		rs = ps.executeQuery();
	    		if(rs.next()){
	    			sqlQuizEndDate = rs.getTimestamp("quiz_end_time");
	    		}
	    		rs.close();
	    	}
	    	conn.close();
	    	//
			/*
			String viewURI = "http://" + renderRequest.getServerName() + ":" +
											renderRequest.getServerPort() + 
											topicPath +
											"?topicID=" + topicID +
											"&itemID=" + itemID;
			String fullViewURI = "http://" + renderRequest.getServerName() + ":" +
											renderRequest.getServerPort() + 
											topicFullViewPath +
											"?topicID=" + topicID +
											"&itemID=" + itemID + 
											"&fullview=true";
			*/
	    	renderRequest.setAttribute("fullview", fullView);
			//
			renderRequest.setAttribute("sqlStartDate", sqlStartDate);
			
			if(sqlStartDateTopic != null){
				renderRequest.setAttribute("sqlStartDateTopic", sqlStartDateTopic.toString().substring(0, 19));
			}
			
			if(sqlQuizEndDate != null){
				renderRequest.setAttribute("sqlQuizEndDate", sqlQuizEndDate.toString().substring(0, 19));
			}
			renderRequest.setAttribute("readSeconds", String.valueOf(readSeconds));
			renderRequest.setAttribute("itemCompleted", String.valueOf(itemCompleted));
			renderRequest.setAttribute("topicID", String.valueOf(topicID));
			renderRequest.setAttribute("itemID", String.valueOf(itemID));
			renderRequest.setAttribute("swfURI", currItem.swfURI);
			renderRequest.setAttribute("swfBookmarkPage", String.valueOf(swfBookmarkPage));
			renderRequest.setAttribute("userId", String.valueOf(userId));
			renderRequest.setAttribute("itemPath", itemPath);
			renderRequest.setAttribute("startTime", String.valueOf(System.currentTimeMillis()));
			//
	    	if(isHTML5){
	    		//確定檔案在 Servlet 檔案夾內
	    		File fd = new File(portletContext.getRealPath("/"));
	    		while(!fd.getName().toLowerCase().contains("tomcat")){
	    			fd = fd.getParentFile();
	    			if(fd == null) break;
	    		}
	    		if(fd == null) throw new Exception("無法找到tomcat目錄！");
	    		//
	    		File fdRootHtml5 = new File(fd.getCanonicalPath() + "/webapps/Html5Servlet");
	    		if(!fdRootHtml5.exists()) throw new Exception("HTML5 Servlet 未安裝 !");
	    		//
	    		//FileEntry feIndex = null;
	    		//Folder folderData = null;
	    		//try{	
	    		//	feIndex = DLAppLocalServiceUtil.getFileEntry(themeDisplay.getScopeGroupId(), folderHtml5.getFolderId(), "index.html");
	    		//	folderData = DLAppLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), folderHtml5.getFolderId(), "data");
	    		//}catch(Exception ex){
	    		//}
	    		//if(feIndex == null || folderData == null) throw new Exception("本項目HTML5內查無index.html檔案或data檔案夾!");
	    		//
	    		File fdHtml5 = new File(fdRootHtml5.getCanonicalPath() + "/" + currItem.id);
	    		//if(!fdHtml5.exists()) fdHtml5.mkdir();
	    		//
	    		//SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	    		//
	    		//File fdData = new File(fdHtml5.getCanonicalPath() + "/" + "data");
	    		//if(!fdData.exists()) fdData.mkdir();
	    		//
	    		//File fileDest = new File(fdHtml5.getCanonicalPath() + "\\index.html");
	    		//if(!fileDest.exists() 
	    		//		|| fileDest.length() == 0
	    		//		|| sdf.format(feIndex.getModifiedDate()).compareTo(sdf.format(new java.util.Date(fileDest.lastModified()))) > 0){
	    		//	FileUtils.copyInputStreamToFile(feIndex.getContentStream(), fileDest);
	    		//}
	    		//
	    		//for(FileEntry fe: DLAppLocalServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), folderData.getFolderId())){
	    		//	fileDest = new File(fdData.getCanonicalPath() + "\\" + fe.getTitle());
		    	//	if(!fileDest.exists()
		    	//			|| fileDest.length() == 0
		    	//			|| sdf.format(fe.getModifiedDate()).compareTo(sdf.format(new java.util.Date(fileDest.lastModified()))) > 0){
		    	//		FileUtils.copyInputStreamToFile(feIndex.getContentStream(), fileDest);
		    	//	}
	    		//}
	    		String html5Path = "/Html5Servlet/" + fdHtml5.getName() + "/index.html";
	    		renderRequest.setAttribute("html5Path", html5Path);
	    		//
	    		this.include(viewJSP3, renderRequest, renderResponse);
			}else{
				this.include(viewJSP, renderRequest, renderResponse);
			}    	
    	}catch (Exception ex){
    		//if(!errMsg.equals("")) errMsg += "\n";
    		//errMsg +=  utils.getItezErrorCode(ex);
    		errMsg = ex.getMessage();
    		//
    		ex.printStackTrace();
    	}finally{
    		if (conn !=null){
    			try{
    				conn.close();
    			}catch (Exception ignore){
    			}
    		}
        }
    	//XSS 過濾
    	/*
		String fullURL = PortalUtil.getCurrentURL(renderRequest);
		if(fullURL.contains("<") || fullURL.contains(">") || fullURL.toLowerCase().contains("alert")){
			//throw new PortletException("網址列含有跨腳本攻擊標籤!");
			renderRequest.setAttribute("XSS", "true");
		}else if(!fullURL.equals(this.cleanXSS(fullURL))){
			//throw new PortletException("網址列含有跨腳本攻擊標籤!");
			renderRequest.setAttribute("XSS", "true");
		}else{
			renderRequest.setAttribute("XSS", "false");
		}
		*/
    	renderRequest.setAttribute("errMsg", errMsg);
    }
    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException{
		if(actionRequest.getParameter("fullview") != null){
			if(actionRequest.getParameter("fullview").equalsIgnoreCase("false")){
				LiferaySessionUtil.setGlobalSessionAttribute("fullview", "true", actionRequest);
				actionResponse.sendRedirect("/fullview");
			}else{
				LiferaySessionUtil.setGlobalSessionAttribute("fullview", "false", actionRequest);
				actionResponse.sendRedirect("/topic");
			}
		}
    }
    
    private String getParameterXSS(ActionRequest actionRequest, String paramName){
    		if(actionRequest.getParameter(paramName) == null){
    			return "";
    		}else{
    			return actionRequest.getParameter(paramName).trim().replaceAll("<", "").replaceAll(">", "").replace("alert", "").replace("script", "");
    		}
    }

    private String getParameterXSS(RenderRequest renderRequest, String paramName){
		if(renderRequest.getParameter(paramName) == null){
			return "";
		}else{
			return renderRequest.getParameter(paramName).trim().replaceAll("<", "").replaceAll(">", "").replace("alert", "").replace("script", "");
		}
    }
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    private String cleanXSS(String value) {
        //You'll need to remove the spaces from the html entities below
		value = value.replaceAll("<", "& lt;").replaceAll(">", "& gt;");
		value = value.replaceAll("\\(", "& #40;").replaceAll("\\)", "& #41;");
		value = value.replaceAll("'", "& #39;");
		value = value.replaceAll("eval\\((.*)\\)", "");
		value = value.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
		value = value.replaceAll("script", "");
		value = value.replaceAll("alert", "");
		return value;
	}
}

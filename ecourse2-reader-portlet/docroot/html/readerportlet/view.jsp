<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="topicID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="itemID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="itemCompleted" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="swfURI" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="sqlStartDate" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="sqlStartDateTopic" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="sqlQuizEndDate" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="readSeconds" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="sqlEndDateTopic" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="fullview" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="userId" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="itemPath" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="startTime" scope="request"></jsp:useBean>

<portlet:defineObjects />

<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.4.4.min.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">
	var adjustSized = false;
	
	//var savedHeight = 0;
	//var savedWidth = 0;
	
	$(window).resize(function() {
		adjustSize();
	});

	$(document).ready(function(){
		var hasFlash = true;
		try {
		  	var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
		  	if (fo) {
		  		hasFlash = true;
		  	}else{
		    		hasFlash = false;
		  	}
		} catch (e) {
		  	if (navigator.mimeTypes
		        		&& navigator.mimeTypes['application/x-shockwave-flash'] != undefined
		        		&& navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
		  		hasFlash = true;
		  	}else{
		  		hasFlash = false;
		  	}
		}
		if(hasFlash == false){
			alert("您的瀏覽器尚未安裝Flash播放器，請前往https://get.adobe.com/flashplayer下載安裝。");	
			return;
		}
		//
		if(!adjustSized){
			adjustSize();
		}
	});
	
	function adjustSize(){
		var ww = $(window).width();
		var hh = $(window).height();
		
		//var h = hh - 225;
		var h = hh - 70;
		//
		var fullview = document.getElementById("fullview").value;
		if(fullview == "true"){
			 h = hh - 60;
		}
		adjustSized = true;
		//
		if(h > 400){
			//document.getElementById("mainSwfTable").height = h;
			//$(".one").height(h);
			
			//$("object").attr({
            //     "height": h
            // });
		}
	}
	
	function updateReadSeconds(){
		var readSeconds = 0;
		try{
			var ss = DisplayStr.split("</b>");
			//alert(ss[0]);
			for(var i=0; i < ss.length; i++){
				var s = ss[i];
				if(i == 0){
					var kk = s.split("<b>");
					readSeconds += parseInt(kk[1]) * 60 * 60;
				}else if(i == 1){
					var kk = s.split("<b>");
					readSeconds += parseInt(kk[1]) * 60;
				}else if(i == 2){
					var kk = s.split("<b>");
					readSeconds += parseInt(kk[1]);
				}
			}
		}catch(ex){
			
		}
		
		//寫回欄位
		document.getElementById("readSeconds").value = readSeconds;
	}
	
	function forwardToFullView(){
		//window.location = document.getElementById("fullViewURI").value + "&swfBookmarkPage=" + document.getElementById("swfBookmarkPage").value;
		window.location = document.getElementById("fullViewURI").value + "&swfBookmarkPage=1";
	}
	
	function backToStdView(){
		//window.location = document.getElementById("viewURI").value + "&swfBookmarkPage=" + document.getElementById("swfBookmarkPage").value;
		window.location = document.getElementById("viewURI").value + "&swfBookmarkPage=1";
	}
</script>

<%
if(!swfURI.equals("")){
%>

<div class="one">
<table id = "mainSwfTable" width="100%" height="100%">
	<tr><td>
	
		<table border="0" cellpadding="20" id="table1">
			<tr height="20" valign="middle">
				<td><img src="<%=renderRequest.getContextPath()%>/images/item_path.png"/></td>
				<td nowrap><%=itemPath%></td>
				<td>&nbsp;</td>
                
                <td nowrap>&nbsp;</td>
               	
				<form id= "myViewForm" method="POST" action="<%=renderResponse.createActionURL()%>" >
					<input type="hidden" name="fullview" id="fullview" value="<%=fullview%>"/>		
				<%
					if(!fullview.equalsIgnoreCase("true")){
				%>
					<td width="90%" align="right">
						<input type="image" src="<%=renderRequest.getContextPath()%>/images/full_screen.png" border="0" alt="全螢幕閱讀" />
 					</td>					
				<%
					}else{
				%>	
					<td width="90%" align="right">
						<input type="image" src="<%=renderRequest.getContextPath()%>/images/orig_screen.png" border="0" alt="選單式閱讀" />
 					</td>					
				<%} %>
				</form>
			</tr>
		</table>
		
		<input type="hidden" name="topicID" id="topicID" value="<%=topicID%>"/>
   	    <input type="hidden" name="itemID" id="itemID" value="<%=itemID%>"/>
   	    <input type="hidden" name="itemID2" id="itemID2" value="<%=itemID%>"/>
	    <input type="hidden" name="swfURI" id="swfURI" value="<%=swfURI%>"/>
	    <input type="hidden" name="sqlStartDate" id="sqlStartDate" value="<%=sqlStartDate%>"/>
	    <input type="hidden" name="itemCompleted" id="itemCompleted" value="<%=itemCompleted%>"/>
	    <input type="hidden" name="sqlEndDateTopic" id="sqlEndDateTopic" value="<%=sqlEndDateTopic%>"/>
	    <input type="hidden" name="readSeconds" id="readSeconds" value="<%=readSeconds%>"/>
	    <input type="hidden" name="fullview" id="fullview" value="<%=fullview%>"/>
	    <input type="hidden" name="userId" id="userId" value="<%=userId%>"/>
	    <input type="hidden" name="startTime" id="startTime" value="<%=startTime%>"/>
	</td></tr>
	
	<tr><td>
		<object id="myFlashMovie" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="100%" height="700">
			<param name="movie" value="<%=swfURI%>" />
			<param name="quality" value="high" />
			<param name="autoplay" value="true"/>
			<param name="wmode" value="transparent"/>
			<param name="bgcolor" value="#FFFFFF"/>
			<PARAM NAME="SCALE" VALUE="default">
			<embed id="fla" src="<%=swfURI%>" 
							quality="high" 
							type="application/x-shockwave-flash" 
							width="100%" 
							height="700" 
							SCALE="default" 
							pluginspage="http://www.macromedia.com/go/getflashplayer"
							wmode="transparent"
							bgcolor="#FFFFFF" />
		</object>
	</td></tr>
</table>	
</div>
<%
}
%>
package com.itez.erp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Random;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.sql.DataSource;

import com.itez.ErpUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class Auth4VdaPortlet extends MVCPortlet {
	DataSource ds = null;
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void checkVdaPassword(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			String strDate = actionRequest.getParameter("dateAccess").trim();
			if(strDate.isEmpty()) throw new Exception("密碼日期空白，無法驗證。");
			
			String screenName = actionRequest.getParameter("screenName").trim();
			if(screenName.isEmpty()) throw new Exception("登入帳號空白，無法驗證。");
			
			String password = actionRequest.getParameter("vdaPassword").trim();
			if(password.isEmpty()) throw new Exception("密碼空白，無法驗證。");
			//
			boolean pass = false;
			//
			conn = ds.getConnection();
			String sql = "SELECT * FROM vda_access WHERE screen_name = ?" +
													" AND date_access = ? " +
													" AND otp_code = ? ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, screenName);
			ps.setString(2, strDate);
			ps.setString(3, password);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				pass = true;
			}
			rs.close();
			//
			if(!pass) throw new Exception("密碼錯誤，請重新輸入。");
			//
			sql = "UPDATE vda_access SET auth_done = 1 WHERE screen_name = ?" +
													" AND date_access = ? " +
													" AND otp_code = ? ";
			ps = conn.prepareStatement(sql);
			ps.setString(1, screenName);
			ps.setString(2, strDate);
			ps.setString(3, password);
			ps.execute();
			//
			actionResponse.sendRedirect("/myreports");
		}catch(Exception ex){
			//String errMsg = ErpUtil.getItezErrorCode(ex);
			//if(errMsg.equals("")){
			String errMsg = ex.getMessage();
			//}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public void sendPasswordAgain(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			User user = themeDisplay.getUser();
			//
			conn = ds.getConnection();
			//
			boolean doUpdate = false;
			//
			String strDate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
			String sql = "SELECT * FROM vda_access WHERE screen_name = ? AND date_access = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, user.getScreenName());
			ps.setString(2, strDate);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				doUpdate = true;
			}
			rs.close();
			//再取一次密碼
			String otp = this.generatorOTP(6);
			//
			if(doUpdate){
				sql = "UPDATE vda_access SET otp_code = ? WHERE screen_name = ? AND date_access = ?";
				ps = conn.prepareStatement(sql);
				ps.setString(1, otp);
				ps.setString(2, user.getScreenName());
				ps.setString(3, strDate);
				ps.execute();
			}else{
				sql = "INSERT INTO vda_access (screen_name, date_access, otp_code) VALUES (?,?,?)";
				ps = conn.prepareStatement(sql);
				ps.setString(1, user.getScreenName());
				ps.setString(2, strDate);
				ps.setString(3, otp);
				ps.execute();
			}
			//
			String newLine = System.getProperty("line.separator");
			String msg = "認證碼：" + otp;
			msg = msg.concat(newLine).concat("認證碼今日" + strDate + "內有效。");
			//
			ErpUtil.sendMail(user.getEmailAddress(), "檢查報告處理系統認證碼(日期:" + strDate + ")", msg);
		}catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private String generatorOTP(int length){ 
	    Random obj = new Random(); 
	    char[] otp = new char[length]; 
	    for (int i=0; i<length; i++){ 
	      otp[i]= (char)(obj.nextInt(10)+48); 
	    } 
	    return new String(otp); 
	}
}

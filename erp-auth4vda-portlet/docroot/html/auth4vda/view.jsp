<%@ page contentType="text/html; charset=UTF-8"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%>

<portlet:defineObjects />
<%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    User user = themeDisplay.getUser();
    //
    String strDate = new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
    //
    String errMsg = "";	
    if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
    String successMsg = "";	
    if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>
  
<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="checkVdaPasswordURL" name="checkVdaPassword" >
</portlet:actionURL>		

<portlet:actionURL var="sendPasswordAgainURL" name="sendPasswordAgain" >
</portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/keylock.png">&nbsp;請輸入系統(<%=strDate%>)傳至您信箱(<%=user.getEmailAddress()%>)之使用密碼:
	</aui:column>
</aui:layout>
<hr/>

<aui:fieldset >
	<aui:form action="<%=checkVdaPasswordURL%>" method="post" name="<portlet:namespace />fm">
		<aui:layout>
			<aui:column>
				<aui:input name="vdaPassword" label="密碼" cssClass="td"/>
				<aui:input type="hidden" name="dateAccess" label="日期" value="<%=strDate%>" cssClass="td"/>
				<aui:input type="hidden" name="screenName" label="登入帳號" value="<%=user.getScreenName()%>" cssClass="td"/>
			</aui:column>
		</aui:layout>
		
		<aui:layout>	
			<aui:column>
				<aui:button type="submit" value="繼續登入" cssClass="button"/>
			</aui:column>
			
			<aui:column cssClass="td">
				&nbsp;&nbsp;&nbsp;<a href="<%=sendPasswordAgainURL%>">重新發送密碼</a>
			</aui:column>			
				
		</aui:layout>
	</aui:form>
</aui:fieldset>



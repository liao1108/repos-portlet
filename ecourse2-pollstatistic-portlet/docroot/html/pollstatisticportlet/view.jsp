<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="strHTML" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="strPollOption" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<form id="myNaviForm" method="POST" action="<portlet:actionURL/>">
	<fieldset width = '100%'>
		<legend align='left'>篩選條件</legend>
		<table width="100%" cellpadding = "20 cellspacing = "20">
			<tr valign="middle">
				<td nowrap>請選擇問卷調查單：&nbsp;&nbsp;</td>
				<td>
					<select name="pollId">
						<%=strPollOption%>
					</select>
				</td>
				<td width='100%' align='left'>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="開始查詢">
				</td>
			</tr>
		</table>
	</fieldset>
	<p/>
	<%=strHTML%>		
	</form>
</body>
package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;

import org.apache.tomcat.jdbc.pool.DataSource;

/**
 * Portlet implementation class PollStatisticPortlet
 */
public class PollStatisticPortlet extends GenericPortlet {
	String eCoursePollFolder = "";	//問卷根目錄
	ECourseUtils utils = new ECourseUtils();
	DataSource ds = null;
	long groupId = -1;
	
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	    	//
	    	eCoursePollFolder = getInitParameter("ecourse-poll-folder");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	String errMsg = "";
   		//
   		String strPollOption = "";
   		String strHTML = "";
   		long selectedPollId = -1;
    	try{
	    	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		//long companyId = themeDisplay.getCompanyId();
	   		groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//
			if(renderRequest.getAttribute("pollId") != null){
				try{
					selectedPollId = Long.parseLong(renderRequest.getAttribute("pollId").toString());
				}catch(Exception ignore){
					
				}
			}
			//問卷選項
			List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			if(fd.getName().equals(eCoursePollFolder)){
	   				List<FileEntry> listFile = DLAppServiceUtil.getFileEntries(groupId, fd.getFolderId());
	   				for(FileEntry f: listFile){			//主題
	   					String strSelected = "";
	   					if(f.getFileEntryId() == selectedPollId){
	   						strSelected = "SELECTED";
	   					}
	   					strPollOption += "<OPTION value='" + f.getFileEntryId() + "'" + " " + strSelected + ">" + f.getTitle() + "</OPTION>";
	   				}
	   			}
	   		}
	   		
	   		if(renderRequest.getAttribute("strHTML") != null){
				strHTML = renderRequest.getAttribute("strHTML").toString();
			}

	   		if(renderRequest.getAttribute("errMsg") != null){
	   			errMsg = renderRequest.getAttribute("errMsg").toString();
			}
    	}catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }
        renderRequest.setAttribute("strPollOption", strPollOption);
        renderRequest.setAttribute("strHTML", strHTML);
   		renderRequest.setAttribute("errMsg", errMsg);
   		renderRequest.setAttribute("selectedPollId", String.valueOf(selectedPollId));
    	//
        include(viewJSP, renderRequest, renderResponse);
    }

    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException{
		String errMsg="";
		long pollId = 0;
		String strHTML = "";
		//
		try{
			if(actionRequest.getParameter("pollId") != null){
				pollId = Long.parseLong(actionRequest.getParameter("pollId"));
	    	}
			if(pollId > 0){
				strHTML = this.genReport(pollId); 
			}else{
				throw new Exception("未指定問卷調查單 !");
			}
    	}catch (Exception ex){
    		if(errMsg.equals("")) errMsg += "\n";
    		errMsg +=  utils.getItezErrorCode(ex);
    	}
    	//
    	actionRequest.setAttribute("errMsg", errMsg);
		actionRequest.setAttribute("pollId", String.valueOf(pollId));
		actionRequest.setAttribute("strHTML", strHTML);
		//
    	actionResponse.setPortletMode(PortletMode.VIEW);
	}
    
    private String genReport(long pollId) throws Exception{
    	String strHTML = "";
    	//
    	try{
	    	Hashtable<String, Poll> htPoll = new Hashtable<String, Poll>(); 
	    	FileEntry f = DLAppServiceUtil.getFileEntry(pollId);
			Workbook wb = Workbook.getWorkbook(f.getContentStream());
		    Sheet ws = wb.getSheet(0);
		    for(int i = 0; i < ws.getRows(); i++){
		    	if(ws.getCell(0, i).getContents() != null 
		    			&& !ws.getCell(0, i).getContents().equals("")
		    			&& !ws.getCell(0, i).getContents().startsWith("題")
		    			&& !ws.getCell(1, i).getContents().equals("1")
		    			&& !ws.getCell(1, i).getContents().equalsIgnoreCase("Y")){
		    		String seqNo = ws.getCell(0, i).getContents().trim();
		    		//
		    		Poll p = new Poll();
		    		p.pollText = ws.getCell(2, i).getContents();
					p.ansOption = ws.getCell(3, i).getContents();
					//選擇題才入列
					if(!p.ansOption.trim().equals("")){
						htPoll.put(seqNo, p);
					}
		    	}
		    }
		    wb.close();
		    //
		    int sheetCount = 0;		//總交卷數
	    	Hashtable<String, Integer> htAns = new Hashtable<String, Integer>(); 
	    	Connection conn = ds.getConnection();
	    	String sql = "SELECT * FROM trace_poll WHERE poll_id = ?";
	    	PreparedStatement ps = conn.prepareStatement(sql);
	    	ps.setLong(1, pollId);
	    	ResultSet rs = ps.executeQuery();
	    	while(rs.next()){
	    		String ansData = "";
	    		if(rs.getString("ans_data") != null){
	    			ansData = rs.getString("ans_data");
	    		}
	    		String[] aa = ansData.split("#");
	    		for(String s: aa){
	    			if(s.contains("CBox_")){		//選擇題才統計
	    				String seqNo = s.split(":")[0];
	    				String[] cbs = s.split(":")[1].split(",");	//多選時以,區隔 
	    				for(String s2: cbs){
	    					String k = seqNo.trim() + ":" + s2.split("_")[1].trim();
	    					int c = 0;
	    					if(htAns.containsKey(k)){
	    						c = htAns.get(k);
	    					}
	    					c++;
	    					htAns.put(k, c);
	    				}
	    			}
	    		}
	    		sheetCount++;
	    	}
	    	rs.close();
	    	//測試
	    	/*
	    	Enumeration<String> enn = htAns.keys();
	    	while(enn.hasMoreElements()){
	    		String key = enn.nextElement();
	    		int c = htAns.get(key);
	    		System.out.println(key + "---" + c);
	    	}
	    	*/
			//計算最大答案選項數
	    	int maxAnsOption = 0;
	    	Vector<Integer> vecSeq = new Vector<Integer>();
	    	Enumeration<String> en = htPoll.keys();
	    	while(en.hasMoreElements()){
	    		String seqNo = en.nextElement();
	    		Poll p = htPoll.get(seqNo);
	    		String[] ss = p.ansOption.split("#");
	    		if(ss.length > maxAnsOption){
	    			maxAnsOption = ss.length;
	    		}
	    		//
	    		vecSeq.add(Integer.parseInt(seqNo));
	    	}
	    	Collections.sort(vecSeq);
		    //開始畫 HTML
	    	strHTML = "<p align='right'>問卷回收數: " + sheetCount + "</p>";
	    	strHTML += "<table width='100%' border='1'>";
	    	strHTML += "<tr>";
			strHTML += "<td align='center' style='background-color:lightblue'>" + "題號" + "</td>"; 	
			strHTML += "<td align='center' style='background-color:lightblue'>" + "題目" + "</td>";
			strHTML += "<td align='center' style='background-color:lightblue' colspan='" + maxAnsOption + "'>" + "答案選項" + "</td>";
			strHTML += "</tr>";
	    	for(int seqNo: vecSeq){
	    		Poll poll = htPoll.get(String.valueOf(seqNo));
	    		//
	    		strHTML += "<tr>";
	    		strHTML += "<td align='center' rowspan='2'>" + seqNo + "</td>";
	    		strHTML += "<td align='left' rowspan='2'>" + poll.pollText + "</td>";
	    		//
	    		String[] anss = poll.ansOption.split("#");
	    		for(String ans: anss){
	    			strHTML += "<td align='center'>" + ans + "</td>";
	    		}
	    		if(anss.length < maxAnsOption){
	    			for(int i=0; i < maxAnsOption - anss.length; i++){
	    				strHTML += "<td align='center'>" + "&nbsp;" + "</td>";
	    			}
	    		}
	    		//
	    		strHTML += "<tr>";
	    		for(int i = 1; i <= anss.length; i++){
	    			String key = (seqNo + ":" + i);
	    			int cc =0;
	    			if(htAns.containsKey(key)){
	    				cc = htAns.get(key);
	    			}
	    			strHTML += "<td align='center' style='background-color:lightyellow'>" + cc + "</td>";
	    		}
	    		if(anss.length < maxAnsOption){
	    			for(int i=0; i < maxAnsOption - anss.length; i++){
	    				strHTML += "<td align='center' style='background-color:lightyellow'>" + "&nbsp;" + "</td>";
	    			}
	    		}
	    		strHTML += "</tr>";
	    	}
			//
			strHTML += "</table>";
    	}catch(Exception ex){
    		ex.printStackTrace();
    		throw new Exception(ex);
    	}
		//
		return strHTML;
    }

    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(PollStatisticPortlet.class);

}

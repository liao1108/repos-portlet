package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

public class LawQuizDbPortlet extends GenericPortlet {
	ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	int quizMinutes = 60;

	public void init() {
		try{
			viewJSP = getInitParameter("view-jsp");
			editJSP = getInitParameter("edit-jsp");
			//
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		quizMinutes = Integer.parseInt(getInitParameter("quiz-minutes"));
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
    
	@SuppressWarnings("unchecked")
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		renderResponse.setContentType("text/html");
		//
		String errMsg = "";
   		Connection conn = null;
   		
   		long monthID = 0;
   		//
   		String ansData = "";
   		String htmlStr = "";
		//
   		Calendar cal = Calendar.getInstance();
   		String quizStartTime = cal.get(Calendar.YEAR) + "-" +
    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
    							String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
    							String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
    							String.format("%02d", cal.get(Calendar.SECOND));
		int currQuizIndex = 0;	//目前考到第幾題
		//int submitCount = 0;	//按第幾次確認按鈕 2017-06-20 改成全部考完再出示答案
		//
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
			
			long userId = themeDisplay.getUserId();
			
			User user = UserLocalServiceUtil.getUserById(userId);
   			String userNo = user.getScreenName();
			
			if(httpRequest.getParameter("monthID") != null){
				monthID = Long.parseLong(httpRequest.getParameter("monthID").trim());
			}else if(renderRequest.getAttribute("monthID") != null){
				monthID = Long.parseLong(renderRequest.getAttribute("monthID").toString());
			}else{
				throw new Exception("Month UUID not exist !");
				//monthID = 81400;
			}
			
			//
			userId = -1;
			try{
				userId = PortalUtil.getUser(renderRequest).getUserId();
			}catch(Exception _ex){
				
			}
			//取得登入者的機構名稱
			String orgName = "";
			if(userId > -1){
				List<Organization> orgs = OrganizationLocalServiceUtil.getUserOrganizations(userId);
				if(orgs.size() > 0){
					orgName = orgs.get(0).getName();
				}
			}
			//
			//讀取所有考題
			String quizSessionKey = "VecLawQuestions_" + monthID;
			//1.從 Session 讀取題目
			Vector<Question> vecQuestions = new Vector<Question>();
			if(renderRequest.getPortletSession().getAttribute(quizSessionKey) != null){
				vecQuestions = (Vector<Question>)renderRequest.getPortletSession().getAttribute(quizSessionKey);
			}
			//
			conn = ds.getConnection();
			//取得檢測最長分鐘數
			if(httpRequest.getParameter("quizMinutes") != null){
				quizMinutes = Integer.parseInt(httpRequest.getParameter("quizMinutes"));
			}
			//2.從檢測紀錄讀取
			if(vecQuestions.size() == 0){
				//Log 進入考試紀錄
				String ipClient = httpRequest.getHeader("x-forwarded-for");
				if(ipClient == null || ipClient.length() == 0 || "unknown".equalsIgnoreCase(ipClient)) {
					ipClient = httpRequest.getHeader("Proxy-Client-IP");
				}
				if(ipClient == null || ipClient.length() == 0 || "unknown".equalsIgnoreCase(ipClient)) {
					ipClient = httpRequest.getHeader("WL-Proxy-Client-IP");
				}
				if(ipClient == null || ipClient.length() == 0 || "unknown".equalsIgnoreCase(ipClient)) {
					ipClient = httpRequest.getRemoteAddr();
				}
				//
				String userFullName = user.getFirstName() + user.getLastName();
				String sql = "INSERT INTO trace_login (org, u_name, login_time, remote_addr, login_url, month_uuid) VALUES (?,?,?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, orgName);
				ps.setString(2, userFullName);
				ps.setString(3, quizStartTime);
				ps.setString(4, ipClient);
				ps.setString(5, PortalUtil.getHttpServletRequest(renderRequest).getServerName());
				ps.setLong(6, monthID);
				ps.execute();
				//將時間記在 Session 裏
				renderRequest.getPortletSession(true).setAttribute("quizStartTime", quizStartTime);
				renderRequest.getPortletSession(true).setAttribute("userFullName", userFullName);
				//先讀取所有的試題
				Vector<Question> vecAll = new Vector<Question>();	//所有試題
				sql = "SELECT * FROM law_quiz_ques WHERE month_uuid=? ORDER BY seq_no";
				ps = conn.prepareStatement(sql);
				ps.setString(1, String.valueOf(monthID));
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					Question q = new Question();
					q.seqNo = rs.getString("seq_no");
					q.quesText = rs.getString("ques_text");
					q.ansOption = rs.getString("ans_option");
					q.stdAns = rs.getString("std_ans");
					q.ansTip = rs.getString("ans_tip");
					vecAll.add(q);
				}
				//查詢之前檢測紀錄
				Vector<String> vecDBPrev = new Vector<String>();
				if(userId > -1){
					sql = "SELECT * FROM trace_law_quiz WHERE u_id=? AND month_uuid=? ORDER BY quiz_start_time DESC";
					ps = conn.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setLong(2, monthID);
					rs = ps.executeQuery();
					if(rs.next()){
						if(rs.getInt("completed") != 1){	//未檢測完成
							if(rs.getString("again_data") != null && !rs.getString("again_data").equals("")){
								//取答錯的題目
								String s = rs.getString("again_data");
								String[] zz = s.split(",");
								for(int i=0; i < zz.length; i++){
									vecDBPrev.add(zz[i].trim());
								}
							}else{	//全部檢測題目重考
								String s = rs.getString("quiz_data");
								String[] zz = s.split(",");
								for(int i=0; i < zz.length; i++){
									vecDBPrev.add(zz[i].trim());
								}
							}
						}
					}
					rs.close();
				}
				//
				if(vecDBPrev.size() > 0){		//繼續考上一次出的考題
					for(String s: vecDBPrev){
						for(Question qs: vecAll){
							if(qs.seqNo.trim().equalsIgnoreCase(s)){
								vecQuestions.add(qs);
								break;
							}
						}
					}
				}else{	//隨機挑20題
					int qty = 0;
					if(vecAll.size() <= 10){
						qty = vecAll.size();
					}else{
						qty = (int)(vecAll.size() * 2 / 3);
						//
						if(qty < 10) qty = 10;
					}
					//
					Vector<Integer> vecIn = new Vector<Integer>();
					while(vecQuestions.size() < qty && vecQuestions.size() < vecAll.size()){
						double d = Math.random();
						int i = (int)((long)(d*10000) % (long)vecAll.size());
						if(!vecIn.contains(i)){
							vecIn.add(i);
							vecQuestions.add(vecAll.get(i));
						}
					}
				}
			}
			//
			if(vecQuestions.size() == 0){
				throw new Exception("Can not get quiz question !");
			}
			//將試題放進 Session 內
			renderRequest.getPortletSession().setAttribute(quizSessionKey, vecQuestions);
			//
			//開始畫 HTML
			if(renderRequest.getAttribute("quizStartTime") != null){
				quizStartTime = renderRequest.getAttribute("quizStartTime").toString();
			}
		    
			if(renderRequest.getAttribute("currQuizIndex") != null){
				currQuizIndex = Integer.parseInt(renderRequest.getAttribute("currQuizIndex").toString());
			}
			if(renderRequest.getAttribute("ansData") != null){
				ansData = renderRequest.getAttribute("ansData").toString();
			}
			//if(renderRequest.getAttribute("submitCount") != null){
			//	submitCount = Integer.parseInt(renderRequest.getAttribute("submitCount").toString());
			//}
			if(currQuizIndex < vecQuestions.size()){
				htmlStr += "<table class='quiztable' border='0' width='100%'>";
	 			htmlStr += "<tr valign='top'>";
	 			htmlStr += "<td align='right'><img src='" + renderRequest.getContextPath() + "/images/timer.png'/></td>";
	 			htmlStr += "<td align='left'><span style='color: DarkGreen; font-familt:微軟正黑體; font-size: 18pt; padding:10px'>本次檢測共 " + vecQuestions.size() + " 題，  起始時間為: " + quizStartTime.toString().substring(0, 19) + "。" + "</span></td>";
	 			htmlStr += "</tr>";
				htmlStr += "</table>";
				//
	 			htmlStr += "<table class='quiztable' border='0' width='100%'>";
 				//
	 			Question q = vecQuestions.get(currQuizIndex);
	 			//
 				String quesText = q.quesText;
 				String optText = q.ansOption;
 				String stdAns = q.stdAns;
 				String ansTip = q.ansTip;
 				//int score = 10;
 				//if(submitCount%2 == 0){
 					//totScore += score;
 				//}
 				String imgPath = renderRequest.getContextPath() + "/images/question.png";
 				//if(submitCount%2 == 1){
 				//	if(qPassed){
 				//		imgPath = renderRequest.getContextPath() + "/images/quiz_right.png";
 				//	}else{
 				//		imgPath = renderRequest.getContextPath() + "/images/quiz_wrong.png";
 				//	}
 				//}
 				//
 				htmlStr += "<tr valign='top'>";
 				htmlStr += "<td><img src='" + imgPath + "'/></td>";
 				htmlStr += "<td>" + (currQuizIndex+1) + ". " + "</td>";
 				htmlStr += "<td  width='100%'><span style='font-family:微軟正黑體'>"+  quesText + "</span></td>";
 				htmlStr += "</tr>";
 				//
 				htmlStr += "<tr valign='top'>";
 				htmlStr += "<td colspan='2'>&nbsp;</td>";
 				htmlStr += "<td>";
 				
 				if(!optText.equals("")){
	 				String[] opt = optText.split("#");
	 				for(int j=0; j < opt.length; j++){
	 					String text = opt[j].trim();
	 					if(text.startsWith("(")){
	 						int idx = text.indexOf(")");
	 						text = "<table width='100%'><tr valign='top'>" +
	 									"<td>" + text.substring(0, idx + 1) + "</td>" +
	 									"<td width='100%'>" +
	 									"<span style='font-family:微軟正黑體'>" + text.substring(idx+1) +"</span>" +
	 									"</td>" +
	 								"</tr></table>";
	 					}
	 					
	 					String optName = "CBox_" + (currQuizIndex + 1) + "_" + (j + 1);
	 					//
	 					String checked = "";
	 					//if(!q.ansData.equals("") && q.ansData.contains(optName)){
	 					//	checked = "checked";
	 					//}
	 					//
	 					String readOnly = "";
	 					//if(submitCount%2 == 1) readOnly = "disabled='disabled'";
	 					htmlStr += "<table width='100%'><tr background='" + renderRequest.getContextPath() + "/images/quiz_bg.png'>";
	 					htmlStr += "<td background='" + renderRequest.getContextPath() + "/images/quiz_bg.png' valign='top'><input type='CHECKBOX' name='" + optName + "' " + checked + " " + readOnly + "></td>";
	 					htmlStr += "<td width='100%'  background='" + renderRequest.getContextPath() + "/images/quiz_bg.png' valign='top'><span style='font-family:微軟正黑體'>" + text +"</span></td>";
	 					htmlStr += "</tr></table>";
	 				}
 				}
 				htmlStr += "</td>";
 				htmlStr += "</tr>";
 				
 				//if(submitCount%2 == 1 && !ansTip.equals("")){
	 			//	htmlStr += "<tr valign='top'>";
	 			//	htmlStr += "<td>&nbsp;</td>";
	 			//	htmlStr += "<td colspan='2'>";
	 			//		htmlStr += "<table width='100%'>";
	 			//		htmlStr += "<tr valign='top'>";
	 			//		htmlStr += "<td><img src='" + renderRequest.getContextPath() + "/images/quiz_tip.png'/></td>";
	 			//		htmlStr += "<td width='100%'><span style='font-family:微軟正黑體'><font color='blue'>" + ansTip + "</font></span></td>";
	 			//		htmlStr += "</tr>";
	 			//		htmlStr += "</table>";
	 			//	htmlStr += "</td>";
	 			//	htmlStr += "</tr>";
 				//}
 			}else{ //答題完畢
 				cal = Calendar.getInstance();
 				String quizEndTime = cal.get(Calendar.YEAR) + "-" +
    	    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
    	    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
    	    							String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
    	    							String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
    	    							String.format("%02d", cal.get(Calendar.SECOND));
 				//int origGetScore = 0;
 				String scoreComment = "";
 				boolean doIt = false;
 				String sql = "SELECT * FROM trace_law_quiz WHERE u_id=? AND month_uuid = ? ORDER BY quiz_start_time DESC";
 				PreparedStatement ps = conn.prepareStatement(sql);
 				ps.setLong(1, userId);
 				ps.setLong(2, monthID);
 				ResultSet rs = ps.executeQuery();
 				if(rs.next()){
 					if(rs.getInt("completed") == 1){
 						scoreComment += "但您之前已通過檢測，本次資料將不列入紀錄。";
 					}else{
 						//origGetScore = rs.getInt("got_score");
 						//
 						long SECOND_MILLIS = 1000;
 						long seconds = (Timestamp.valueOf(quizEndTime).getTime()/SECOND_MILLIS)-(Timestamp.valueOf(quizStartTime).getTime()/SECOND_MILLIS);
 						if(seconds > 1){
 							if(seconds > quizMinutes * 60){
 								scoreComment += "但您的檢測時間已逾時，本次資料將不列入檢測紀錄。";
 							}else{
 								doIt = true;
 							}
 						}else{
 							System.out.println("It seems that submit was clicked twice !!");
 							return;
 						}
 					}
 				}else{
 					doIt = true;
 				}
 				//統計分數
 				//totScore = 0;
 				//getScore = 0;
 				//for(Question q: vecQuestions){
	 			//	int score = 10;
	 			//	totScore += score;
	 			//	if(q.gotScore) getScore += score;
 				//}
 				int ccPassed = 0;
 				for(Question q: vecQuestions){
 					if(q.gotScore) ccPassed++;
 				}
 				boolean completed = false;
 				if(ccPassed == vecQuestions.size()) completed = true;
 				//返回閱讀頁面URL
	 			String courseURI = "http://" + renderRequest.getServerName() + ":" +
	 												renderRequest.getServerPort() + 
	 												"/lawnavi" +
	 												"?monthID=" + monthID;
	 			//
 				htmlStr += "<table class='quiztable' border='0' width='100%'>";
	 			htmlStr += "<tr valign='top'>";
	 			htmlStr += "<td align='right'><img src='" + renderRequest.getContextPath() + "/images/timer.png'/></td>";
	 			htmlStr += "<td align='left'><span style='color: DarkGreen; font-familt:微軟正黑體; font-size: 18pt; padding:10px'>本次檢測共 " + vecQuestions.size() + " 題，  您答對 " + ccPassed +" 題。" + scoreComment + "</span></td>";
	 			htmlStr += "<td align='right'>" +
	 							"<a href='" + courseURI + "'>" +
	 								"<img src='" + renderRequest.getContextPath() + "/images/orig_screen.png' title='返回課程目錄'/>" +
	 							"</a>" +	
	 						"</a>";
	 			htmlStr += "</tr>";
				htmlStr += "</table>";
				//
	 			htmlStr += "<table class='quiztable' border='0' width='100%'>"; // background='/images/quiz_bg.png'>";
	 			//
 				//ansData = ansDataAll;
 				//準備儲存答錯的題號
	 			Vector<String> vecAgain = new Vector<String>();
	 			//將所有題目與答題狀況再顯示 一次
 				for(Question q: vecQuestions){
	 				String quesText = q.quesText;
	 				String optText = q.ansOption;
	 				//String stdAns = q.stdAns;
	 				String ansTip = q.ansTip;
	 				//Detect if quiz was answered
	 				String imgPath = renderRequest.getContextPath() + "/images/quiz_wrong.png";
	 				if(q.gotScore){
	 					imgPath = renderRequest.getContextPath() + "/images/quiz_right.png";
	 				}else{
 						//堆疊答錯題目的文字
	 					vecAgain.add(q.seqNo);
	 				}
	 				//
	 				htmlStr += "<tr valign='top'>";
	 				htmlStr += "<td><img src='" + imgPath + "'/></td>";
	 				htmlStr += "<td>" + (vecQuestions.indexOf(q)+ 1) + ". " + "</td>";
	 				htmlStr += "<td width='100%'><span style='font-family:微軟正黑體'>" + quesText + "</span></td>";
	 				htmlStr += "</tr>";
	 				//
	 				if(!optText.equals("")){
		 				String[] opt = optText.split("#");
		 				for(int j=0; j < opt.length; j++){
		 					htmlStr += "<tr valign='top'>";
			 				htmlStr += "<td>&nbsp;</td>";
			 				//
		 					String optName = "CBox_" + (vecQuestions.indexOf(q) + 1) + "_" + (j + 1);
		 					//
		 					String checked = "";
		 					if(!q.ansData.equals("") && q.ansData.contains(optName)){
		 						checked = "checked";
		 					}
		 					//
		 					String text = opt[j].trim();
		 					if(text.startsWith("(")){
		 						int idx = text.indexOf(")");
		 						text = "<table width='100%'><tr valign='top'>" +
		 									"<td>" + text.substring(0, idx + 1) + "</td>" +
		 									"<td width='100%'>" +
		 									"<span style='font-family:微軟正黑體'>" + text.substring(idx+1) +"</span>" +
		 									"</td>" +
		 								"</tr></table>";
		 					}
		 					htmlStr += "<td>";
		 						htmlStr += "<input type='CHECKBOX' name='" + optName + "' " + checked + " disabled>";
		 					htmlStr += "</td>";
		 					htmlStr += "<td width='100%'>" + text + "</td>";
		 					//
			 				htmlStr += "</tr>";
		 				}
	 				}
	 				
	 				if(!ansTip.equals("")){
		 				htmlStr += "<tr valign='top'>";
		 				htmlStr += "<td>&nbsp;</td>";
		 				htmlStr += "<td colspan='2'>";
		 					htmlStr += "<table width='100%'>";
		 					htmlStr += "<tr valign='top'>";
		 					htmlStr += "<td><img src='" + renderRequest.getContextPath() + "/images/quiz_tip.png'/></td>";
		 					htmlStr += "<td width='100%'><span style='font-family:微軟正黑體'><font color='blue'>" + ansTip + "</font></span></td>";
		 					htmlStr += "</tr>";
		 					htmlStr += "</table>";
		 				htmlStr += "</td>";
		 				htmlStr += "</tr>";
	 				}
	 			}
		 		//
		 		if(doIt && userId > -1){	//Guest 不紀錄
		 			String quizData = "";
		 			for(Question q: vecQuestions){
		 				if(!quizData.equals("")) quizData += ",";
		 				quizData += q.seqNo;
		 			}
		 			String againData = "";
		 			for(String s: vecAgain){
		 				if(!againData.equals("")) againData += ",";
		 				againData += s;
		 			}
		 			//
		 			//int completed = 0;
		 			//if(vecAgain.size() == 0){	//已經完全答對
		 			//	completed = 1;
		 			//}
		 			//
		 			int getScore = 0;
		 			int totScore = 0;
		 			if(renderRequest.getAttribute("getScore") != null) getScore = Integer.parseInt(renderRequest.getAttribute("getScore").toString());
		 			if(renderRequest.getAttribute("totScore") != null) totScore = Integer.parseInt(renderRequest.getAttribute("totScore").toString());
		 			//寫入資料庫
		 			sql = "INSERT INTO trace_law_quiz (u_id, quiz_start_time, quiz_end_time, tot_score, got_score, quiz_data, again_data, month_uuid, completed) VALUES (?,?,?,?,?,?,?,?,?)";
		 			ps = conn.prepareStatement(sql);
		 			ps.setLong(1, userId);
		 			ps.setString(2, quizStartTime);
		 			ps.setString(3, quizEndTime);
		 			ps.setInt(4, totScore);
		 			ps.setInt(5, getScore);
		 			ps.setString(6, quizData);
		 			ps.setString(7, againData);
		 			ps.setLong(8, monthID);
		 			if(completed){
		 				ps.setInt(9, 1);
		 			}else{
		 				ps.setInt(9, 0);
		 			}
		 			ps.execute();
	   				//清除 Session
	   				renderRequest.getPortletSession().removeAttribute(quizSessionKey);
		 		}
 			}
 			htmlStr += "</table>";
 			//
			renderRequest.setAttribute("monthID", String.valueOf(monthID));
			renderRequest.setAttribute("quizMinutes", String.valueOf(quizMinutes));
			renderRequest.setAttribute("quizStartTime", quizStartTime.toString());
			renderRequest.setAttribute("htmlStr", htmlStr);
			renderRequest.setAttribute("currQuizIndex", String.valueOf(currQuizIndex));
			renderRequest.setAttribute("totQuizNum", String.valueOf(vecQuestions.size()));
			//renderRequest.setAttribute("submitCount", String.valueOf(submitCount));
			//renderRequest.setAttribute("totScore", String.valueOf(totScore));
			//renderRequest.setAttribute("getScore", String.valueOf(getScore));
			//
			//if(submitCount%2 == 0){
			//	renderRequest.setAttribute("submitTitle", "作答");
			if(currQuizIndex >= vecQuestions.size() - 1){
				renderRequest.setAttribute("submitTitle", "交卷");
			}else{
				renderRequest.setAttribute("submitTitle", "下一題");
			}
			
			//
			if(currQuizIndex >= vecQuestions.size()){
				//標注交卷時間
				String _quizStopTime = cal.get(Calendar.YEAR) + "-" +
						String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
						String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
						String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
						String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
						String.format("%02d", cal.get(Calendar.SECOND));
				//
				String _quizStartTime = renderRequest.getPortletSession(true).getAttribute("quizStartTime").toString();
				String _userFullName = renderRequest.getPortletSession(true).getAttribute("userFullName").toString();
				
				String sql = "UPDATE trace_login SET logout_time=? WHERE login_time=? AND u_name=?";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, _quizStopTime);
				ps.setString(2, _quizStartTime);
				ps.setString(3, _userFullName);
				ps.execute();		
				//
				this.include(viewJSP, renderRequest, renderResponse);
			}else{
				this.include(editJSP, renderRequest, renderResponse);
			}		
        } catch (Exception ex){
        	//ex.printStackTrace();
        	//
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        	System.out.println(errMsg);
        }finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
        }
        //
        renderRequest.setAttribute("errMsg", errMsg);
    }

    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException{
    	//int submitCount = 0;
    	//if(actionRequest.getParameter("submitCount") != null){
    	//	submitCount = Integer.valueOf(actionRequest.getParameter("submitCount")) + 1;
    	//	actionRequest.setAttribute("submitCount", String.valueOf(submitCount));
    	//}
    	//
    	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(actionRequest));
		//
    	String ansData = "";
    	//if(submitCount%2 == 1){
	    	Enumeration<String> en = actionRequest.getParameterNames();
	    	while(en.hasMoreElements()){
	    		String param = en.nextElement();
	    		if(param.startsWith("CBox_")){
	    			if(!ansData.equals("")) ansData += ",";
	    			//
	    			ansData += param;
	    		}
	    	}
    	//}
	    //System.out.println("Your answer: " + ansData);
    	//actionRequest.setAttribute("ansData", ansData);
	    	
    	long monthID = 0;
    	if(httpRequest.getParameter("monthID") != null){
			monthID = Long.parseLong(httpRequest.getParameter("monthID").trim());
		}else if(actionRequest.getAttribute("monthID") != null){
			monthID = Long.parseLong(actionRequest.getAttribute("monthID").toString());
		}else{
			//throw new Exception("Month UUID not exist !");
			//monthID = 81400;
		}
    	String quizSessionKey = "VecLawQuestions_" + monthID;
	    //
	    //解題
	    int currQuizIndex = Integer.valueOf(actionRequest.getParameter("currQuizIndex"));
	    Vector<Question> vecQuestions = (Vector<Question>)actionRequest.getPortletSession().getAttribute(quizSessionKey);
	    boolean qPassed = true;
		Question q = vecQuestions.get(currQuizIndex);
		//
		String[] aa = q.stdAns.split("#");
		for(int j=0; j < aa.length; j++){
			String keyAns = "CBox_" + (currQuizIndex + 1) + "_" + aa[j];
			if(!ansData.contains(keyAns)){
				qPassed = false;
				break;
			}
		}
		if(qPassed){	//check if over answered
			int countChecked = 0;
			String key = "CBox_" + (currQuizIndex + 1) + "_";
			String[] tt = ansData.split(",");
			for(int j=0; j < tt.length; j++){
				if(tt[j].startsWith(key)){
					countChecked++;
				}
			}
			if(aa.length != countChecked) qPassed = false; 
		}
		//
		int score = 10;
		int getScore = 0;
		int totScore = 0;
		if(actionRequest.getAttribute("getScore") != null){
			getScore = Integer.parseInt(actionRequest.getAttribute("getScore").toString());
		}
		if(actionRequest.getAttribute("totScore") != null){
			totScore = Integer.parseInt(actionRequest.getAttribute("totScore").toString());
		}
		totScore += score;
		if(qPassed){
			getScore += score;
			//
			q.gotScore = true;
			//System.out.println("You got the score !");
		}else{
			q.gotScore = false;
			//System.out.println("You miss the score !");
		}
		q.ansData = ansData;
					
		vecQuestions.set(currQuizIndex, q);
		//放回 Session 內
		actionRequest.getPortletSession().setAttribute(quizSessionKey, vecQuestions);
    	//
    	//
    	if(actionRequest.getParameter("monthID") != null){
    		actionRequest.setAttribute("monthID", actionRequest.getParameter("monthID"));
    	}
    	//
    	if(actionRequest.getParameter("quizStartTime") != null){
    		actionRequest.setAttribute("quizStartTime", actionRequest.getParameter("quizStartTime"));
    	}
    	//
    	if(actionRequest.getParameter("quizMinutes") != null){
    		actionRequest.setAttribute("quizMinutes", actionRequest.getParameter("quizMinutes"));
    	}
    	//
    	//if(actionRequest.getParameter("currQuizIndex") != null){
    	//	int currQuizIndex = Integer.valueOf(actionRequest.getParameter("currQuizIndex"));
    		//if(submitCount%2 == 1){
    		//	actionRequest.setAttribute("currQuizIndex", String.valueOf(currQuizIndex));
    		//}else{
    			actionRequest.setAttribute("currQuizIndex", String.valueOf(currQuizIndex + 1));
    		//}
    	//}
    	//
    	if(actionRequest.getParameter("totQuizNum") != null){
    		actionRequest.setAttribute("totQuizNum",  actionRequest.getParameter("totQuizNum"));
    	}
    	//
    	actionRequest.setAttribute("totScore",  String.valueOf(totScore));
    	actionRequest.setAttribute("getScore",  String.valueOf(getScore));
    }
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;
    protected String editJSP;

    private static Log _log = LogFactoryUtil.getLog(LawQuizDbPortlet.class);

}
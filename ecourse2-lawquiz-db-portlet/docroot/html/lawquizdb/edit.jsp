<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="htmlStr" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="quizMinutes" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="quizStartTime" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="submitTitle" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="currQuizIndex" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="totQuizNum" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="submitCount" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="totScore" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="getScore" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="monthID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

	<STYLE TYPE="text/css">
	<!--
		TD{font-family:微軟正黑體; font-size: 14pt; padding:5px;}
	-->
	</STYLE>
</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<form id="myformQuiz" method="POST" action="<portlet:actionURL/>">
		<%=htmlStr%>
		<div>
			<input type="hidden" name="monthID" value="<%=monthID%>"/>
			<input type="hidden" name="quizMinutes" value="<%=quizMinutes%>"/>
			<input type="hidden" name="quizStartTime" value="<%=quizStartTime%>"/>
			
			<input type="hidden" name="currQuizIndex" value="<%=currQuizIndex%>"/>
			<input type="hidden" name="totQuizNum" value="<%=totQuizNum%>"/>
			<input type="hidden" name="submitCount" value="<%=submitCount%>"/>
			<input type="hidden" name="totScore" value="<%=totScore%>"/>
			<input type="hidden" name="getScore" value="<%=getScore%>"/>
			
			<input type="submit" name="Submit" value="<%=submitTitle%>"/>
		</div>
	</form>
</body>
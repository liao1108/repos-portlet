package com.itez;

import java.util.ArrayList;
import java.util.List;

public class FileAttrHeader {
	public String paramSec = "";
	public String paramDateStart = "";
	public String paramDateEnd = "";
	//
	public String reportNo = "";
	public String secName = "";
	public String createdDate = "";
	
	public String createdBy = "";
	public String dropped = "否";
	
	public String examNo = "";
	public String leaderName = "";
	public String staffs = "";
	
	public String dateStart = "";
	public String dateEnd = "";
	public String examNature = "";
	
	public String examFolderId = "";
	
	public List<FileAttr> listFileAttr = new ArrayList<>();
}

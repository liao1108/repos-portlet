package com.itez;

import java.util.Vector;

public class ReportProfile {
	public int reportId = 0;
	public String reportNo = "";
	public String caseName = "";
	public String dateBase = "";		//檢查基準日
	public String dateClosed = "";		//報告關閉日
	public String pmName = "";
	public String leaderName = "";
	public String staffs = "";
	public String createdBy = "";
	public String dateCreated = "";
	
	public String reportFact = "";			//組別/業別/檢查別
	
	public String folderId = "";			//Alfresco Folder Id
	//public String dateStart = "";			//案件起始日期
	//public String dateEnd = "";			//案件結束日期
	public String closedBy = "";			//關閉人
	
	public int dropped = 0;				//是否作廢
	
	public String dateReporting = "";		//報告提出日期
	public String secName = "";				//組別
	public String indusType = "";			//行業別
	
	public String reportSecIndus = "";		//報告用組別加業別
	public String jobSecIndus = "";			//工作範本用組別加業別
	
	public String secBelongTo = "";			//報告歸屬組別
	
	public int reportYear = 0;			//報告年度
	public boolean editable = false;
	
	public boolean isLeader = false;		//目前用戶是否為領隊
	
	public Vector<ExamProfile> vecExam = new Vector<ExamProfile>();
	
	public Vector<TxLog> vecTxLog = new Vector<TxLog>();
}

package com.itez;

public class ExamProfile {
	public int reportId = 0;
	public int examId = 0;
	public String reportNo = "";
	public String reporFoldertId = "";
	public String examNo = "";			//檢查證號
	public String examNoType = "持證檢查";
	public String examFolderId = "";
	public String jsonName = "";			//工作範本檔名
	public String folderId = "";			//Alfresco Folder node Id
	public String bankName = "";
	public String bankNameShort = "";
	public String leaderName = "";
	public String staffs = "";			//助檢成員
	
	public String reportFree = "";		//無需繳交報告人員(助理人員)
	public String freeReason = "";		//無需繳交報告原因
	
	public String dateStart = "";
	public String dateEnd = "";
	public String dateCreated = "";
	public String createdBy = "";		//建立人
	public String verifiers = "";		//審核人員
	public float examDays = 0f;			//檢查日數
	public boolean noMeeting = false;	//無溝通會
	public String dateMeeting = "";		//檢討會日期
	public String dateApproval = "";		//核派日期
	public String docTempPath = "";		//文件樣板目錄
	public String pmName = "";			//專案代理人員
	
	public boolean editable = false;
	public String deadlineStaff = "";	//助檢繳回報告期限
	//public int evalAllocateTiming = 1;		//評鑑細項工作分配時機( 1.檢查行前階段 2.評等審議階段)
	
	public int defectWithBisTitle = 1;		//外部版實地檢查缺失情形表是否標上業務項目
	//目前執行的階段
	public String stageReached = "";
	
	public int dropped = 0;				//是否作廢
	
	public int closed = 0;				//是否已關閉（報告編號關閉者, 旗下之檢查證號視同關閉）
	
	public String examNature = "";		//一般檢查 、 專案檢查 或 一般檢查(評等)
	
	//2017-03-22 加入
	public String rootDocTempl = "";		//文件樣板跟目錄名稱 (Liferay 文件區)
	public String caseName = "";			//案名
	public String dateBase = "";			//檢查基準日
	public String dateReport = "";			//報告日
}

<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>

<%@ page import= "com.itez.FileAttrHeader"%>
<%@ page import= "com.itez.FileAttr"%>
<%@ page import= "com.itez.ErpUtil"%>
 
<portlet:defineObjects />
 
  <%
	int rowsPrefer = 75;
    //if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
	// 	rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
	//}
    //
    boolean isAdmin = false;
    if(renderRequest.getPortletSession(true).getAttribute("isAdmin") != null 
    		&& renderRequest.getPortletSession(true).getAttribute("isAdmin").toString().equalsIgnoreCase("true")){
    	isAdmin = true;
    }
    //
    String secBelongTo = "";
    if(isAdmin){
    	secBelongTo = "*";
    }else if(renderRequest.getPortletSession(true).getAttribute("secBelongTo") != null){
    	secBelongTo = renderRequest.getPortletSession(true).getAttribute("secBelongTo").toString();
    }
    
    Vector<String> vecSecPriv = new Vector<String>();	
	if(renderRequest.getPortletSession(true).getAttribute("vecSecPriv") != null){
		vecSecPriv = (Vector<String>)renderRequest.getPortletSession(true).getAttribute("vecSecPriv");
	}
	//
	String myFileEndGened = "";
	if(renderRequest.getAttribute("myFileEndGened") != null) myFileEndGened = renderRequest.getAttribute("myFileEndGened").toString();
	//
	Vector<FileAttrHeader> vecFileEnd = null;
    if(renderRequest.getAttribute("vecFileEnd") != null){
    	vecFileEnd = (Vector<FileAttrHeader>)renderRequest.getAttribute("vecFileEnd");
    }
    
	//
    String errMsg = "";	
    if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
    String successMsg = "";	
    if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="goBackViewURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/view.jsp"/>
</portlet:renderURL>

<portlet:actionURL var="genListStageEndFilesURL" name="listStageEndFiles" >
</portlet:actionURL>

<portlet:resourceURL var="downloadStageEndFilesURL">
	<portlet:param name="downloadStageEndFiles" value="true"/>
</portlet:resourceURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/query.png">&nbsp;請指定報告案建立起迄日期
	</aui:column>
</aui:layout>

<hr/>
<br/>

<aui:fieldset>
<aui:form action="<%=genListStageEndFilesURL.toString()%>"  method="post" >
	<aui:layout>
		<aui:column cssClass="td">
			<aui:select name="secBelongTo"  label="組別"  cssClass="td">
				<%for(String secName: vecSecPriv){
						String _secNo = secName;
						if(!_secNo.equals("*")){
							if(!_secNo.equals("檢查制度組")){
								_secNo = String.valueOf(secName.charAt(0));
							}
						}
					%>
					<aui:option value="<%=_secNo%>"  ><%=secName%></aui:option>
				<%} %>
			</aui:select>
		</aui:column>	
		
		<aui:column cssClass="td">
			<aui:input name="dateStart"  label="立案起始日(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>
		
		<aui:column cssClass="td">
			<br>～
		</aui:column>
		
		<aui:column cssClass="td">
			<aui:input name="dateEnd"  label="立案截止日(yyyy-mm-dd)"  value=""  cssClass="td" />
		</aui:column>

	</aui:layout>
	
	<aui:layout>
		<aui:column>
			<aui:button onClick="<%=goBackViewURL.toString() %>"  value="取消" />
		</aui:column>
		<aui:column>
			<aui:button type="submit"  value="開始查詢"  title="開始查詢"  cssClass="button"/>
		</aui:column>
	</aui:layout>
</aui:form>
</aui:fieldset>

<hr/>
<br/>

<aui:layout>
	<aui:column cssClass="td">查詢結果:</aui:column>
	<%if(!myFileEndGened.equals("")){ %>
		<aui:column cssClass="header_red">
	 		<img src="<%=renderRequest.getContextPath()%>/images/excel22.png">&nbsp;<a href="<%=downloadStageEndFilesURL.toString()%>" >下載已產出的報告編號列表</a>
		</aui:column>
<%} else if(vecFileEnd != null && vecFileEnd.size() == 0){ %>
	<aui:column cssClass="header_red">查無任何檔案。</aui:column>
<%} %>	
</aui:layout>

<aui:layout>
<aui:column>&nbsp;</aui:column>
	<aui:column cssClass="td" >
		<aui:button onClick="<%=goBackViewURL.toString() %>" value="返回上頁" cssClass="button"/>
	</aui:column>
</aui:layout>
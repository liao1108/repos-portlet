<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ page import= "java.util.Vector"%>
<%@ page import= "java.text.SimpleDateFormat"%>
<%@ page import= "java.util.Calendar"%>
<%@ page import= "java.util.Hashtable"%>

<%@ page import= "com.itez.ReportProfile"%>
<%@ page import= "com.itez.ExamProfile"%>
<%@ page import= "com.itez.Collec"%>
<%@ page import= "com.itez.ErpUtil"%>

<portlet:defineObjects />

  <%
  		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	User user = themeDisplay.getUser();
    	//
    	int	examId = Integer.parseInt(renderRequest.getParameter("examId"));
    	int	reportId = Integer.parseInt(renderRequest.getParameter("reportId"));
    	//
    	ReportProfile rp = new ReportProfile();
            	
   		Collec collec = new Collec();
    	if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
    		collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
    	}
            	
        for(ReportProfile _rp: collec.vecReport){
        	if(_rp.reportId == reportId){
        		rp = _rp;
            	break;
            }
       	}
        //
        Hashtable<String, Vector<String>> htJson = (Hashtable<String, Vector<String>>)renderRequest.getPortletSession(true).getAttribute("htJson");
        Vector<String> vecJson = new Vector<String>();
        if(htJson.containsKey(rp.jobSecIndus)){
        	vecJson = htJson.get(rp.jobSecIndus);
        }
		//
		ExamProfile ep = new ExamProfile();
			
		Vector<ExamProfile> vecExam = rp.vecExam;
		for(ExamProfile _ep: vecExam){
			if(_ep.examId == examId){
				ep = _ep;
				break;
			}
		}
		//
        if(ep.dateStart.equals("")) ep.dateStart = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        if(ep.dateEnd.equals("")) ep.dateEnd = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        //if(ep.dateMeeting.equals("")) ep.dateMeeting = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        if(ep.deadlineStaff.equals("")) ep.deadlineStaff = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        //
        String userFullName = "";
        if(renderRequest.getPortletSession(true).getAttribute("userFullName") != null){
        	userFullName = (String)renderRequest.getPortletSession(true).getAttribute("userFullName");
        }
        //
        String errMsg = "";	
        if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
            	
        String successMsg = "";	
        if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="updateExamURL"  name="updateExam" >
</portlet:actionURL>		

<portlet:resourceURL var="doSelectValUrl">
	<portlet:param name="selectedVal" value="PARAM_PLACEHOLDER_SELECTED_VAL" />
</portlet:resourceURL>

<portlet:renderURL var="queryIndustryURL"  windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
  	<portlet:param name="jspPage"  value="/jsp/myreports/queryIndustry.jsp"/>
  	<portlet:param name="secName"  value="SEC_NAME_TO_BE_REPLACE"/>
</portlet:renderURL>

<portlet:renderURL var="goBackURL">
  	<portlet:param name="jspPage"  value="/jsp/myreports/viewExam.jsp"/>
  	<portlet:param name="reportId"  value="<%=String.valueOf(reportId) %>"/>
</portlet:renderURL>

<portlet:resourceURL var="fetchDB2ExamInfoURL" >
	<portlet:param name="fetchDB2" value="true"/>
	<portlet:param name="examNo" value="EXAM_NO_TO_BE_REPLACE"/>
	<portlet:param name="leaderName" value="LEADER_NAME_TO_BE_REPLACE"/>
	<portlet:param name="authorizeCode" value="AUTHORIZE_CODE_TO_BE_REPLACE"/>
</portlet:resourceURL>		

<portlet:renderURL var="goDropExamURL" >
  	<portlet:param name="jspPage"  value="/jsp/myreports/examDrop.jsp"/>
  	<portlet:param name="reportId" value="<%=String.valueOf(reportId) %>"/>
  	<portlet:param name="examId" value="<%=String.valueOf(examId) %>"/>
</portlet:renderURL>

<script type="text/javascript">
	Liferay.Portlet.ready(
		function(portletId, node) {
		        document.getElementById('<portlet:namespace />examNo').focus();
		}
	);
	
	Liferay.on('receiveIndustry', 
		function(event, p_data){
			document.getElementById("<portlet:namespace/>indusType").value = p_data;
			document.getElementById('<portlet:namespace />caseName').focus();
		}
	);
	
	function showIndusPopup() {
		var val = document.getElementById("<portlet:namespace/>secName").value;
		if(val == ""){
			alert("請先指定組別。");				
			return;
		}
		//
		var A = AUI();
		A.use('aui-io-request', 
					'aui-dialog',
					'aui-io', 'event',
					'event-custom',
					function(A) {
						var dialog = new A.Dialog({
												title: '業別選擇',
												centered: true,
												draggable: true,
												modal: true,
												width: 500,
												height: 400,
								}).plug(A.Plugin.IO, {uri: '<%=queryIndustryURL%>'.replace('SEC_NAME_TO_BE_REPLACE', val) }).render();
   						dialog.show();
					});
	};
	
	
	//由檢查行政取得資料
	function doAJAX(){
		var val = document.getElementById("<portlet:namespace/>examNo").value;
		if(val ==""){
			alert("請輸入檢查證號！");
			return;
		}else{
			//alert(val);
		}
		//判斷是否為代理人
		//var _leaderName = document.getElementById("<portlet:namespace/>leaderName").value;
		//var leaderName = "";
		//if(_leaderName == ""){
		//	_leaderName = document.getElementById("<portlet:namespace/>userFullName").value;
		//	leaderName = encodeURIComponent(document.getElementById("<portlet:namespace/>userFullName").value);
		//}else{
		//	leaderName = encodeURIComponent(document.getElementById("<portlet:namespace/>leaderName").value);
		//}
		var leaderName = encodeURIComponent(document.getElementById("<portlet:namespace/>leaderName").value);
		var userFullName = encodeURIComponent(document.getElementById("<portlet:namespace/>userFullName").value);
		var pmName = encodeURIComponent(document.getElementById("<portlet:namespace/>pmName").value);
		
		var authorizeCode = "";
		if(userFullName != pmName && userFullName != leaderName){
			alert("非領隊本人或代理人，無法建立檢查證！");
			return;
		}
		//
		var queryString = "<%=fetchDB2ExamInfoURL.toString()%>".replace("EXAM_NO_TO_BE_REPLACE", val).replace("LEADER_NAME_TO_BE_REPLACE", leaderName).replace("AUTHORIZE_CODE_TO_BE_REPLACE", authorizeCode);
		
		//alert(queryString);
		
		var A = AUI();
		A.io.request(queryString, {			
									dataType : 'JSON',
											on: {
												success: function(event, id, obj) {
															 var responseData = this.get('responseData');
															 if(responseData.ret == ""){
																 alert("檢查行政系統沒有回應(回應值為空字串)。");
															 }else if(responseData.ret.indexOf('Database') >= 0){
																 alert(responseData.ret);
															 }else if(responseData.ret.indexOf('NotFound') >= 0){
																 var ss = responseData.ret.split("#");
																 alert(ss[1]);
															 }else if(responseData.ret.indexOf('AUTHORIZE') >= 0){
																 var ss = responseData.ret.split("#");
																 alert(ss[1]);																								 
															 }else{
															 	 //alert(responseData.ret);
																 document.getElementById("<portlet:namespace/>debug").value = responseData.ret;
																 //
															 	 var ss = responseData.ret.split("#");
																 for(var i=0; i < ss.length; i++){
																	//alert(ss[i]);
																	var idx = ss[i].indexOf("=");
																	if(idx > 0){
																		var vv = ss[i].split("=");
																		if(vv[0] == "leaderName"){
																			document.getElementById("<portlet:namespace/>leaderName").value = vv[1];
																		}else if(vv[0] == "staffs"){
																			document.getElementById("<portlet:namespace/>staffs").value = vv[1];
																		}else if(vv[0] == "bankName"){
																			document.getElementById("<portlet:namespace/>bankName").value = vv[1];
																		}else if(vv[0] == "bankNameShort"){
																			document.getElementById("<portlet:namespace/>bankNameShort").value = vv[1];
																		}else if(vv[0] == "dateStart"){
																			document.getElementById("<portlet:namespace/>dateStart").value = vv[1];
																		}else if(vv[0] == "dateEnd"){
																			document.getElementById("<portlet:namespace/>dateEnd").value = vv[1];
																		}else if(vv[0] == "dateApproval"){
																			document.getElementById("<portlet:namespace/>dateApproval").value = vv[1];
																		}else if(vv[0] == "workDays"){
																			document.getElementById("<portlet:namespace/>examDays").value = vv[1];
																		}else if(vv[0] == "examNature"){
																			document.getElementById("<portlet:namespace/>examNature").value = vv[1];
																		}
																	 }
																 }
															  }
															},
															failure: function() {
																		alert('呼叫失敗。');
																	  }
													}
									});
	};
</script>


<aui:layout>
	<aui:column cssClass="header">
	 	<%if(ep.examId == 0){ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/exam.png">&nbsp;建立新檢查證
	 	<%}else{ %>
	 		<img src="<%=renderRequest.getContextPath()%>/images/exam.png">&nbsp;檢查證基本資料
	 	<%} %>
	</aui:column>
</aui:layout>
<hr/>

<aui:fieldset >
<aui:form action="<%=updateExamURL%>" method="post" name="<portlet:namespace />fm">
		<aui:layout>
			<aui:column>
				<aui:input type="hidden" name="reportId"  value="<%=rp.reportId %>"/>
				<aui:input type="hidden" name="examId"  value="<%=ep.examId %>"/>
				<aui:input type="hidden" name="folderId"  value="<%=ep.folderId %>"/>
				<aui:input type="hidden" name="userFullName"  value="<%=userFullName %>"/>
				<aui:input type="hidden" name="pmName"  value="<%=rp.pmName %>"/>
				<aui:input name="secName" label="組別" value="<%=rp.secName %>"   readonly="readonly" cssClass="td"/>
			</aui:column>
			
			<aui:column >
				<aui:input name="indusType" label="行業別" value="<%=rp.indusType %>"   size="30"  readonly="readonly" cssClass="td"/>
			</aui:column>
		</aui:layout>
			
		<aui:layout>
			<aui:column>	
				<aui:input name="reportYear" label="報告年度" value="<%=rp.reportYear %>"  readonly="readonly" cssClass="td" />
			</aui:column>

			<aui:column>	
				<aui:input name="reportNo" label="報告編號" value="<%=rp.reportNo %>"  readonly="readonly" cssClass="td"/>
			</aui:column>
		</aui:layout>
		
		<hr/>

		<aui:layout>
			<aui:column>
				<aui:select name="jsonName"  label="工作範本"  cssClass="td">
					<aui:option value="">&nbsp;</aui:option>
					<%for(String j: vecJson){	%>
						<aui:option value="<%=j%>"  selected="<%=ep.jsonName.equals(j) %>"><%=j%></aui:option>
					<%} %>
				</aui:select>
			</aui:column>	
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:select name="examNoType"  label="持證或持函"  cssClass="td">
					<aui:option value="持證檢查" selected="<%=ep.examNoType.equals(\"持證檢查\") %>">持證檢查</aui:option>
					<aui:option value="持函檢查" selected="<%=ep.examNoType.equals(\"持函檢查\") %>">持函檢查</aui:option>
				</aui:select>
			</aui:column>
			
			<aui:column>
				<aui:input name="examNo" label="檢查證號" value="<%=ep.examNo %>"  cssClass="td">
					<aui:validator name="required" />
				</aui:input>
			</aui:column>
			<aui:column>	
				<br/><br/>
				<aui:button onClick= "doAJAX();"  value="由檢查行政系統匯入資料"  title="由檢查行政系統匯入資料"  cssClass="button"/>
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:input name="bankName" label="受檢機構全名" value="<%=ep.bankName %>"  size = "60" cssClass="td">
					<aui:validator name="required" />
				</aui:input>	
			</aui:column>
			<aui:column>
				<aui:input name="bankNameShort" label="受檢機構簡稱" value="<%=ep.bankNameShort %>"  cssClass="td">
					<aui:validator name="required" />
				</aui:input>	
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:select name="examNature"  label="檢查性質"  cssClass="td">
				<aui:option value="" ></aui:option>
				<aui:option value="一般檢查" selected="<%=ep.examNature.equals(\"一般檢查\") %>" >一般檢查</aui:option>
				<aui:option value="專案檢查" selected="<%=ep.examNature.equals(\"專案檢查\") %>" >專案檢查</aui:option>
				<aui:option value="一般檢查(評等)" selected="<%=ep.examNature.equals(\"一般檢查(評等)\") %>" >一般檢查(評等)</aui:option>
			</aui:select>	
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:input name="leaderName" label="領隊" value="<%=ep.leaderName %>"  cssClass="td">
					<aui:validator name="required" />
				</aui:input>
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:input name="staffs" label="助檢成員" value="<%=ep.staffs %>"  cssClass="td"  size="60">
					<aui:validator name="required" />
				</aui:input>	
			</aui:column>
			<aui:column>
				<br/>
				(助檢姓名以分號 ; 區隔。如為他局人員，請先於本系統建檔。)
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column>
				<aui:input name="reportFree" label="上傳人員排除名單" value="<%=ep.reportFree %>"  cssClass="td"  size="60"/>
			</aui:column>
			<aui:column>
				<aui:input name="freeReason" label="排除原因說明" value="<%=ep.freeReason %>"  cssClass="td"  size="60"/>
			</aui:column>
		</aui:layout>

		<aui:layout>
			<aui:column cssClass="td">
				<aui:input name="dateApproval"  label="核派日期(yyyy-mm-dd)" value="<%=ep.dateApproval %>"  cssClass="td" />
			</aui:column>
			<aui:column>
				<aui:input name="examDays" label="工作天數" value="<%=ep.examDays %>"  cssClass="td" />
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:column cssClass="td">
				<aui:input name="dateStart"  label="開始檢查日(yyyy-mm-dd)" value="<%=ep.dateStart %>"  cssClass="td" />
			</aui:column>
			<aui:column cssClass="td">
				<aui:input name="dateEnd"  label="完成檢查日(yyyy-mm-dd)" value="<%=ep.dateEnd %>"  cssClass="td" />
			</aui:column>
		</aui:layout>
		
		<aui:layout>	
			<aui:column cssClass="td">
				<aui:input name="deadlineStaff"  label="助檢報告期限(yyyy-mm-dd)" value="<%=ep.deadlineStaff %>"  cssClass="td" />
			</aui:column>
			
			<aui:column cssClass="td">
				<aui:input name="noMeeting"  type="checkbox" label="無溝通會" value="<%=ep.noMeeting %>"  cssClass="td" />
			</aui:column>
			
			<aui:column cssClass="td">
				<aui:input name="dateMeeting"  label="溝通會議日期(yyyy-mm-dd)" value="<%=ep.dateMeeting %>"  cssClass="td" />
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<br/>
			<aui:column  cssClass="td">
				<h4>產出外部版實地檢查缺失情形表是否標上檢查業務項目(本銀組含評等作業時本項應選「是」)：</h4>
				<%if(ep.defectWithBisTitle==1){ %>
					<aui:input name="defectWithBisTitle" type="radio" value="1"  label="是" checked="true"/>
                        	<aui:input name="defectWithBisTitle" type="radio" value="0" label="否" />
				<%}else{ %>
					<aui:input name="defectWithBisTitle" type="radio" value="1"  label="是" />
                        	<aui:input name="defectWithBisTitle" type="radio" value="0" label="否" checked="true"/>
				<%} %>
			</aui:column>
		</aui:layout>
		
		<%if(!ep.dateCreated.equals("")){ %>
		<aui:layout>			
			<aui:column>	
				<aui:input name="dateCreate" label="建立日期" value="<%=ep.dateCreated %>" readonly="readonly" cssClass="td"/>
			</aui:column>
			<aui:column>	
				<aui:input name="createdBy" label="建立人" value="<%=ep.createdBy %>" readonly="readonly" cssClass="td"/>
			</aui:column>
		</aui:layout>
		<%} %>
		<aui:button-row>
			<aui:button  onClick="<%=goBackURL.toString() %>"   value="返回上頁"  cssClass="button"/>
			<%if(ep.examId == 0){ %>
				<aui:button type="submit"  value="確定建立" cssClass="button" />
			<%}else{ %>
				<aui:button  type="submit"   value="確定更新"  cssClass="button"/>
				<aui:button onClick="<%=goDropExamURL.toString() %>"  value="作廢本檢查證"  cssClass="button"/>
			<%}%>
		</aui:button-row>
		
		<aui:layout>			
			<aui:column>	
				<aui:input name="debug" label="偵錯訊息："  inlineLabel = "left"  value=""  size="120"/>
			</aui:column>
		</aui:layout>	
</aui:form>
</aui:fieldset>

<form action="<%=fetchDB2ExamInfoURL %>"  method="POST" name="db2RequestForm" id="db2RequestForm">
    <input type="hidden" value="" name="examNo4DB2" id="examNo4DB2"/>
</form>




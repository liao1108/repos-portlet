<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<portlet:defineObjects />

<%
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="doAgreementURL" name="doAgreement">
</portlet:actionURL>

<portlet:actionURL var="doDisAgreementURL" name="doDisAgreement">
</portlet:actionURL>

 <style type="text/css">
  	.th {
    			font-family:微軟正黑體;
			font-size:14pt;
			color:darkred;
    		}
    	tr  {
    		height: 40px
    		}
  	td {
    			font-family:微軟正黑體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:red;
    		}
    	r{
    			font-family:微軟正黑體;
			font-size:12pt;
			color:darkred;
    		}
  .button{
  	font-family:標楷體;
	font-size: 12px ;
	text-align: center ;
	line-height: 24px;
	text-decoration: none ; 
	}	
	
.checked{
  	font-family:微軟正黑體;
			font-size:12pt;
			color:blue;
	}	
    				
  </style>
  
 <%
	String rootPaht = renderRequest.getContextPath();
%>
<script src="<%=rootPaht%>/js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript">
		function checkAccount() {
			var signUpId =$("#<portlet:namespace />signUpId" ).val();
			var param = [{ name: "signUpId", value: signUpId}];
			$.ajax({
					type : "POST",
					url : '<portlet:resourceURL/>',
					data : param,
					dataType : "text",
					success : function(data) {
						//$('#result').text(data);
						alert(data);
					},
					error : function() {
					}
			});
		}
</script>
 
 <aui:form action="<%=doAgreementURL%>" method="post"  id="<portlet:namespace />fm"  name="<portlet:namespace />fm">
<table width = '100%'>
	<tr>
		<td><img src='<%=renderRequest.getContextPath()%>/images/SignUp.png'></td>
		<td class='th' >個人資料提供同意書</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>本局稽核人員留言板受理金融機構稽核人員之寶貴見解或問題，
		當您首次申請使用留言板時，需要填列您的個人資料包括姓名、電子郵件地址等，
		依據個人資料保護法與相關法令之規範，本局稽核人員留言板所蒐集、處理及利用您的個人資料，
		僅作為處理相關問答之需，不對外公開或作其他用途，請提供您本人正確及最新之資料。</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>當您選擇「我同意」時，表示您已閱讀、瞭解並接受本同意書之所有內容及其後修改變更之規定，
		且同意本局以您所提供的個人資料確認您的身分或與您連絡，若您所提供之個人資料，經檢舉或本局發現不實等情形，
		本局有權停止提供對您的服務，若有不便之處敬請見諒。</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>您瞭解本同意書符合個人資料保護法及相關法規之要求，具有書面同意本局蒐集、處理及利用您的個人資料之效果。</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td class="checked"><aui:input name="checked" label="我已閱讀並充分瞭解本同意書內容" type="checkbox" /></td>
	</tr>	
	
	<tr>
		<td>&nbsp;</td>	
		<td>
			<aui:button-row>
				<aui:column>
					<aui:button type='submit'  name="submit"    value="我同意"   cssClass="button"/>
				</aui:column>
				<aui:column>
					<aui:button onClick="window.close();"    value="我不同意"   cssClass="button"/>
				</aui:column>
				
			</aui:button-row>
		</td>
		<td>&nbsp;</td>
	</tr>
</table>
</aui:form>


package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class AgreePortlet extends MVCPortlet {
	
	public void doAgreement(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			if(actionRequest.getParameter("checked") != null && actionRequest.getParameter("checked").equalsIgnoreCase("true")){
				//ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				 
				 //PortletURL redirectURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),
						 									//"signUp",
						 									//themeDisplay.getLayout().getPlid(),
						 									//PortletRequest.RENDER_PHASE);
				 //redirectURL.setParameter("jspPage", "/signUp");
				//actionResponse.sendRedirect(redirectURL.toString()); 
				
				actionResponse.sendRedirect("/signup2");
			}else{
				throw new Exception("如果您同意提供個人資料，請勾選「我已閱讀並充分瞭解本同意書內容」。");
			}
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}

	public void doDisAgreement(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			actionResponse.sendRedirect("http://www.feb.gov.tw/ch/home.jsp?id=206&websitelink=artwebsite.jsp&parentpath=0,4,156,181");
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}

	public String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
				if(str.contains("com.itez")){
					ret += ("\n" + str);
				}
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
}

package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.sql.Connection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
//import java.util.Map;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

//import org.apache.chemistry.opencmis.client.api.CmisObject;

//import org.apache.chemistry.opencmis.client.api.Folder;
//import org.apache.chemistry.opencmis.client.api.ItemIterable;
//import org.apache.chemistry.opencmis.client.api.Session;
//import org.apache.chemistry.opencmis.client.api.SessionFactory;
//import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
//import org.apache.chemistry.opencmis.commons.SessionParameter;
//import org.apache.chemistry.opencmis.commons.enums.BindingType;
//import org.apache.tomcat.jdbc.pool.DataSource;

public class RatingResultPortlet extends GenericPortlet {
	//String alfURL = "";
	//String alfAppName = "";
	//String alfUser = "";
	//String alfPassword = "";
	//String alfRootFolder = "";
	String eCourseFolder = "";

	//final String CMIS_FOLDER = "CMIS_FOLDER";
	//final String CMIS_DOCUMENT = "CMIS_DOCUMENT";

	//private Session session;
	String eCourseRootFoler = "";	//課程根目錄
	
	DataSource ds = null;
	//String tableSchema = "";

	
	public void init() {
		try{
			//alfURL = getInitParameter("alf-url");
			//alfUser = getInitParameter("alf-user");
			//alfPassword = getInitParameter("alf-password");
			//alfRootFolder = getInitParameter("alf-root-folder");
			eCourseFolder = getInitParameter("ecourse-folder");
			//
			//tableSchema = getInitParameter("table_schema");
			//
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
			//
	   		viewJSP = getInitParameter("view-jsp");
	   		eCourseRootFoler = getInitParameter("ecourse-root-folder");
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	Connection conn = null;
    	try{
    		ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
   		long groupId = themeDisplay.getScopeGroupId();
   		//
    		conn = ds.getConnection();
		//以 Alfresco 為 Repository
		/*
    		session = this.cmisConnect(alfURL + "/service/cmis", alfUser, alfPassword);
    		//
		Folder rootCourse = null;
		Folder root = (Folder)session.getRootFolder();
		ItemIterable<CmisObject> children = root.getChildren();
		for(CmisObject o : children){
			if(o.getName().equals(eCourseFolder)){
				rootCourse = (Folder)o;
				break;
			}
		}
		if(rootCourse == null){
			throw new Exception("課程根目錄 " + eCourseFolder + " 不存在 !");
		}
		//主題-單元-單元UUID 暫存
		Vector<String> vecTopic = new Vector<String>();
		//
		ItemIterable<CmisObject> subjects = rootCourse.getChildren();
		for(CmisObject coSubject: subjects){
			if(coSubject.getBaseTypeId().toString().equals("CMIS_FOLDER")){
				Folder fdSubject = (Folder)coSubject;
				//主題
				ItemIterable<CmisObject> topics = fdSubject.getChildren();
				for(CmisObject coTopic: topics){
					//單元
					if(coTopic.getBaseTypeId().toString().equals(this.CMIS_FOLDER)){
						vecTopic.add(fdSubject.getName() + "#" + coTopic.getName() + "#" + coTopic.getId());
					}
				}
			}
		}
		*/
    		//讀取舊記錄
    		HashMap<String, OldRating> htRating = this.getOldRating();
    		//
    		Vector<String> vecTopic = new Vector<String>();
    		//
    		List<Folder> list = DLAppLocalServiceUtil.getFolders(groupId, 0);
   		for(Folder fd: list){
   			if(fd.getName().startsWith(eCourseRootFoler)){
   				List<Folder> listSubject = DLAppLocalServiceUtil.getFolders(groupId, fd.getFolderId());
   				for(Folder fdSubject: listSubject){			//主題
   					if(!fdSubject.getName().contains("測試")){
   						List<Folder> listTopic = DLAppLocalServiceUtil.getFolders(groupId, fdSubject.getFolderId());
   						for(Folder fdTopic: listTopic){		//單元
   							if(!fdTopic.getName().contains("測試")){
   								String val = fdSubject.getName() + "#" + fdTopic.getName() + "#" + fdTopic.getFolderId();
   								vecTopic.add(val);
   							}
   						}
   					}
   				}
   			}
   		}
		//
		Collections.sort(vecTopic);
		//
		int count01 = 0;
		int count02 = 0;
		int count03 = 0;
		int count04 = 0;
		int count05 = 0;
		int count06 = 0;
		int count07 = 0;
		int count08 = 0;
		int count09 = 0;
		//
		String strHTML = "<table width='100%' border='0' cellpadding='2' cellapacing='2'";
		strHTML += "<tr>";
		strHTML += "<td align='center' colspan='3'>";
		strHTML += "<img src='" + renderRequest.getContextPath() + "/images/result_header.png'>";
		strHTML += "</td>";
		strHTML += "</tr>";
		strHTML += "<tr>";
		strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
		strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>主題單元</span>";
		strHTML += "</td>";
		strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
		strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>評價</span>";
		strHTML += "</td>";
		strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
		strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>人氣(人次)</span>";
		strHTML += "</td>";
		strHTML += "</tr>";
		//
		String bgImgName = "";
		for(String s: vecTopic){
			String[] ss = s.split("#");
			if(ss[0].contains("01")){
				count01++;
				bgImgName = "bg_01_odd.png";
				if(count01%2 == 0){
					bgImgName = "bg_01_even.png";
				}
			}else if(ss[0].contains("02")){
				count02++;
				bgImgName = "bg_02_odd.png";
				if(count02%2 == 0){
					bgImgName = "bg_02_even.png";
				}
			}else if(ss[0].contains("03")){
				count03++;
				bgImgName = "bg_03_odd.png";
				if(count03%2 == 0){
					bgImgName = "bg_03_even.png";
				}
			}else if(ss[0].contains("04")){
				count04++;
				bgImgName = "bg_04_odd.png";
				if(count04%2 == 0){
					bgImgName = "bg_04_even.png";
				}
			}else if(ss[0].contains("05")){
				count05++;
				bgImgName = "bg_05_odd.png";
				if(count05%2 == 0){
					bgImgName = "bg_05_even.png";
				}
			}else if(ss[0].contains("06")){
				count06++;
				bgImgName = "bg_06_odd.png";
				if(count06%2 == 0){
					bgImgName = "bg_06_even.png";
				}
			}else if(ss[0].contains("07")){
				count07++;
				bgImgName = "bg_07_odd.png";
				if(count07%2 == 0){
					bgImgName = "bg_07_even.png";
				}
			}else if(ss[0].contains("08")){
				count08++;
				bgImgName = "bg_08_odd.png";
				if(count08%2 == 0){
					bgImgName = "bg_08_even.png";
				}
			}else if(ss[0].contains("09")){
				count09++;
				bgImgName = "bg_09_odd.png";
				if(count09%2 == 0){
					bgImgName = "bg_09_even.png";
				}
			}
			//
			long topicID = Long.parseLong(ss[2].trim());
			
			double numAverage = 0;
			int numRating = 0;
			//String topicUUID = ss[2].trim();
			String sql = "SELECT AVG(rating_score), COUNT(rating_score) FROM topic_rating WHERE topic_id LIKE '%" + topicID + "'";
			PreparedStatement ps = conn.prepareStatement(sql);
			//ps.setString(1, topicUUID);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				numAverage = rs.getDouble(1);
				numRating = rs.getInt(2);
			}
			rs.close();
			//
			sql = "CREATE TABLE IF NOT EXISTS topic_click (" +
  				  "topic_id int(11) NOT NULL, " +
  				  "click_times int(11), PRIMARY KEY (topic_id))";
			ps = conn.prepareStatement(sql);
			ps.execute();
			
			int readTimes = 0;
			//sql = "SELECT COUNT(topic_id) FROM topic_read WHERE topic_id LIKE '%" + topicID + "'";
			sql = "SELECT SUM(click_times) FROM topic_click WHERE topic_id = ?";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, topicID);
			rs = ps.executeQuery();
			if(rs.next()){
				readTimes = rs.getInt(1);
			}
			rs.close();
			//
			numAverage = (double)Math.round(numAverage * 10) /10d;
			
			//加上舊的紀錄 (Liferay 6.0.5)
			String topicFullName = ss[0] + " - " + ss[1];
			OldRating o = new OldRating();
			if(htRating.containsKey(topicFullName)){
				o = htRating.get(topicFullName);
			}
			readTimes += o.readTimes;
			if(o.ratingScore > 0 && numAverage > 0){
				numAverage = (double)Math.round((numAverage * numRating + o.ratingScore * o.readTimes)/ (numRating + o.readTimes) * 10) /10d;
			}else if(numAverage > 0){
				numAverage = (double)Math.round(numAverage * 10) /10d;
			}else if(o.ratingScore > 0){
				numAverage = (double)Math.round(o.ratingScore * 10) /10d;
			}
			//
			if(numAverage == 0) numAverage = 4.5;
			//	
			int numStar = (int)numAverage;
			boolean halfStar = false;
			if(numAverage - (int)numAverage > 0){
				halfStar = true;
			}
			//
			strHTML += "<tr>";
			strHTML += "<td background='" + renderRequest.getContextPath() + "/images/" + bgImgName + "'>"; 
			strHTML += "<span style='color: DarkBlue; font-family:微軟正黑體; font-size: 14pt;'>" + "&nbsp;" + topicFullName + "&nbsp;" + "</span>";
			strHTML += "</td>";
			strHTML += "<td align='left' background='" + renderRequest.getContextPath() + "/images/" + bgImgName + "'>";
			for(int i=0; i < numStar; i++){
				strHTML += "<img src='" + renderRequest.getContextPath() + "/images/star_full.png'>";;
			}
			if(halfStar){
				strHTML += "<img src='" + renderRequest.getContextPath() + "/images/star_half.png'>";;
				//
				for(int i=5; i > (numStar + 1); i--){
					//strHTML += "<img src='" + renderRequest.getContextPath() + "/images/star_empty.png'>";;
				}
			}else{
				for(int i=5; i > numStar; i--){
					//strHTML += "<img src='" + renderRequest.getContextPath() + "/images/star_empty.png'>";;
				}
			}
			if(numAverage > 0){
				strHTML += "(" + numAverage + ")";
			}
			//
			strHTML += "</td>";
			strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/" + bgImgName + "'>";
			if(readTimes > 0){
				strHTML += "<span style='color: DarkBlue; font-family:微軟正黑體; font-size: 14pt;'>" + readTimes + "</span>";
			}else{
				strHTML += "&nbsp;";
			}
			strHTML += "</td>";
			strHTML += "</tr>";
		}
		strHTML += "</table>";
		//
		boolean isAdmin = false;
			try{
				List<Role> userRoles = RoleLocalServiceUtil.getUserRoles(themeDisplay.getUserId());
				for (Role role : userRoles) {
					if(role.getName().equalsIgnoreCase("Administrator")){
						isAdmin = true;
						break;
					}
				}
			}catch(Exception _ex){
			}
			if(isAdmin){
				strHTML += "<br>";
				strHTML += "<table width='100%' border='1' cellpadding='2' cellapacing='2'>";
				strHTML += "<tr>";
				strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
				strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>留言時間</span>";
				strHTML += "</td>";
				strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
				strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>版面建議</span>";
				strHTML += "</td>";
				strHTML += "<td align='center' background='" + renderRequest.getContextPath() + "/images/bg_header.png'>";
				strHTML += "<span style='color: White; font-family:微軟正黑體; font-size: 14pt;'>其他建議</span>";
				strHTML += "</td>";
				strHTML += "</tr>";
				//
				String sql = "SELECT rating_time, layout_comment, else_comment FROM overall_rating ORDER BY rating_time DESC";
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					String time = rs.getString(1);
					String layoutComment = "";
					if(rs.getString(2) != null) layoutComment = rs.getString(2).trim();
					String elseComment = "";
					if(rs.getString(3) != null) elseComment = rs.getString(3).trim();
					//
					if(layoutComment.contains("http") || layoutComment.contains("<script>")) continue;
					if(elseComment.contains("http") || elseComment.contains("<script>")) continue;
					if(layoutComment.isEmpty() && elseComment.isEmpty()) continue;
					if(layoutComment.length() <= 2 && elseComment.length() <= 2) continue;
					//
					strHTML += "<tr>";
					//Col1
					strHTML += "<td>";
					strHTML += time;
					strHTML += "</td>";
					//Col2
					strHTML += "<td>";
					strHTML += layoutComment;
					strHTML += "</td>";
					//Col3
					strHTML += "<td>";
					strHTML += elseComment;
					strHTML += "</td>";
					//
					strHTML += "</tr>";
				}
				rs.close();
				//
				strHTML += "</table>";
			}
			//
			renderRequest.setAttribute("strHTML", strHTML);
    	}catch(Exception ex){
    		logErrorWithCode("General Exception: ", ex);
    	}finally{
        	if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
    	}
    	//
    	include(viewJSP, renderRequest, renderResponse);
    }

    public HashMap<String, OldRating> getOldRating() throws Exception{
	    HashMap<String, OldRating> ht = new HashMap<String, OldRating>();
	    //
	    OldRating o = new OldRating();
	    //1
	    o.readTimes = 7176;
	    o.ratingScore = 4.8;
	    ht.put("01.存款業務 - A.存款業務及開戶審查", o);
	    //2
	    o = new OldRating();
	    o.readTimes = 2580;
	    o.ratingScore = 5.0;
	    ht.put("01.存款業務 - B.存款業務管理", o);
	    //3
	    o = new OldRating();
	    o.readTimes = 3438;
	    o.ratingScore = 4.8;
	    ht.put("02.洗錢防制作業 - A.大額通貨交易", o);
	    //4
	    o = new OldRating();
	    o.readTimes = 1740;
	    o.ratingScore = 4.9;
	    ht.put("02.洗錢防制作業 - B.疑似洗錢交易", o);
	    //5
	    o = new OldRating();
	    o.readTimes = 2487;
	    o.ratingScore = 4.6;
	    ht.put("03.內部管理 - A.公司治理", o);
	    //6
	    o = new OldRating();
	    o.readTimes = 1255;
	    o.ratingScore = 5.0;
	    ht.put("03.內部管理 - B.業務操作", o);
	    //7
	    o = new OldRating();
	    o.readTimes = 2011;
	    o.ratingScore = 5.0;
	    ht.put("03.內部管理 - C.內部稽核制度", o);
	    //8
	    o = new OldRating();
	    o.readTimes = 2642;
	    o.ratingScore = 4.8;
	    ht.put("04.授信業務 - A.授信作業程序", o);
	    //9
	    o = new OldRating();
	    o.readTimes = 1591;
	    o.ratingScore = 4.7;
	    ht.put("04.授信業務 - B.不動產授信", o);
	    //10
	    o = new OldRating();
	    o.readTimes = 1256;
	    o.ratingScore = 5.0;
	    ht.put("04.授信業務 - C.逾期放款之管理", o);
	    //11
	    o = new OldRating();
	    o.readTimes = 990;
	    o.ratingScore = 4.2;
	    ht.put("04.授信業務 - D.不良債權出售", o);
	    //12
	    o = new OldRating();
	    o.readTimes = 1906;
	    o.ratingScore = 4.8;
	    ht.put("05.投資業務 - A.投資業務", o);
	    //13
	    o = new OldRating();
	    o.readTimes = 1203;
	    o.ratingScore = 5.0;
	    ht.put("05.投資業務 - B.衍生性金融商品", o);
	    //14
	    o = new OldRating();
	    o.readTimes = 1341;
	    o.ratingScore = 4.8;
	    ht.put("06.信託業務 - A.信託業風險管理", o);
	    //15
	    o = new OldRating();
	    o.readTimes = 1013;
	    o.ratingScore = 5.0;
	    ht.put("06.信託業務 - B.特定金錢信託業務", o);
	    //16
	    o = new OldRating();
	    o.readTimes = 669;
	    o.ratingScore = 5.0;
	    ht.put("06.信託業務 - C.其他信託業務", o);
	    //17
	    o = new OldRating();
	    o.readTimes = 1287;
	    o.ratingScore = 5.0;
	    ht.put("07.金融商品銷售 - A.受託投資業務人員管理", o);
	    //18
	    o = new OldRating();
	    o.readTimes = 1363;
	    o.ratingScore = 5.0;
	    ht.put("07.金融商品銷售 - B.業務推廣及風險管理", o);
	    //19
	    o = new OldRating();
	    o.readTimes = 1168;
	    o.ratingScore = 5.0;
	    ht.put("07.金融商品銷售 - C.銷售管理機制及客訴處理", o);
	    //20
	    o = new OldRating();
	    o.readTimes = 1362;
	    o.ratingScore = 4.8;
	    ht.put("08.資訊作業 - A.資訊組織與管理", o);
	    //21
	    o = new OldRating();
	    o.readTimes = 914;
	    o.ratingScore = 5.0;
	    ht.put("08.資訊作業 - B.實體安全管理", o);
	    //22
	    o = new OldRating();
	    o.readTimes = 740;
	    o.ratingScore = 5.0;
	    ht.put("08.資訊作業 - C.資訊系統開發及維護管理", o);
	    //23
	    o = new OldRating();
	    o.readTimes = 596;
	    o.ratingScore = 5.0;
	    ht.put("08.資訊作業 - D.資訊作業運作管理", o);
	    //24
	    o = new OldRating();
	    o.readTimes = 615;
	    o.ratingScore = 0.0;
	    ht.put("08.資訊作業 - E.故障復原及災害應變管理", o);
	    //25
	    o = new OldRating();
	    o.readTimes = 1049;
	    o.ratingScore = 5.0;
	    ht.put("09.保險招攬業務 - A.業務員及其他通路管理", o);
	    //26
	    o = new OldRating();
	    o.readTimes = 391;
	    o.ratingScore = 5.0;
	    ht.put("09.保險招攬業務 - B.文宣廣告管理", o);
	    //27
	    o = new OldRating();
	    o.readTimes = 325;
	    o.ratingScore = 4.8;
	    ht.put("10.證券承銷業務 - 證券商股票承銷業務", o);
	    return ht;
    }
    
    /*
	private Session cmisConnect(String cmisURL, String cmisUser, String cmisPassword) {
		Map<String, String> parameter = new HashMap<String, String>();
		parameter.put(SessionParameter.USER, cmisUser);
		parameter.put(SessionParameter.PASSWORD, cmisPassword);
		//parameter.put(SessionParameter.ATOMPUB_URL, cmisURL);		//測試Liferay 到 Alfresco 的CMIS
		parameter.put(SessionParameter.ATOMPUB_URL, alfURL + "/service/cmis");
		parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
		// Set the alfresco object factory
		parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");
		// Create a session
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Session session = factory.getRepositories(parameter).get(0).createSession();
		//
		return session;
	}
*/
    
	private void logErrorWithCode(String text, Exception ex){
		String errMsg = text + " Exception: " + ex.getMessage();
		//
    	StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null) {
	           if (str.contains("com.itez")){
	        	   if(!errMsg.equals("")) errMsg += "\n";
	        	   errMsg += str;
	           }
			}
			//
			System.out.println(errMsg);
		}catch(IOException e){
		   e.printStackTrace();
		}
	}

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(RatingResultPortlet.class);

}

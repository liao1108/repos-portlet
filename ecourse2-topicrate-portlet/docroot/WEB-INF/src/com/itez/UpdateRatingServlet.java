package com.itez;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jdbc.pool.DataSource;

import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Servlet implementation class UpdateReadSecondServlet
 */
public class UpdateRatingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds = null;
	
    public UpdateRatingServlet() {
        super();
    }

    public void init() throws ServletException {
    	try{
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }    

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection conn = null;
		try{
			conn = ds.getConnection();
			//
			//補填Topic資料
		    try{
		        String sql = "SELECT topic_id FROM topic_rating WHERE topic_title IS NULL OR topic_title = '' GROUP BY topic_id";
		        PreparedStatement ps = conn.prepareStatement(sql);
		        ResultSet rs = ps.executeQuery();
		        while(rs.next()){
		        		int _topicId = rs.getInt(1);
		        		//
		        		try{
		        			if(DLAppLocalServiceUtil.getFolder(_topicId) != null){
		        				sql = "UPDATE topic_rating SET topic_title=? WHERE topic_id = ?";
		        				ps = conn.prepareStatement(sql);
		        				ps.setString(1, DLAppLocalServiceUtil.getFolder(_topicId).getName());
		        				ps.setInt(2, _topicId);
		        				ps.execute();
		        				
		        			}
		        		}catch(Exception _ex){
		        			
		        		}
		        }
		        rs.close();
		    }catch(Exception _ex){
		    	
		    }
			
			
			//long userId = Long.parseLong(request.getParameter("userId"));
			String topicID = "";
			if(request.getParameter("topicID") != null){
				topicID = request.getParameter("topicID");
			}
			
			String topicTitle = "";
			try{
				topicTitle = DLAppLocalServiceUtil.getFolder(Integer.parseInt(topicID)).getName();
			}catch(Exception _ex){
			}
			
			String rating = "";
			if(request.getParameter("rating") != null){
				rating = request.getParameter("rating").trim();
			}
			System.out.println("Topic: " + topicID + " Rating: " + rating);
			//
			if(!rating.equals("")){
				String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
			    Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
				String currDateTime = sdf.format(cal.getTime());
				//
				String sql = "INSERT INTO topic_rating (topic_id, topic_title, rating_time, rating_score) VALUES (?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, topicID);
				ps.setString(2, topicTitle);
				ps.setString(3, currDateTime);
				ps.setInt(4, Integer.parseInt(rating));
				ps.execute();
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if (conn !=null){
        		try{
        			conn.close();
        		}catch (Exception ignore){
        		}
        	}
		}
	}

	/*
	public Connection getConnection(String dbDriver,
											String dbURL,
											String dbUser,
											String dbPassword) throws Exception {
        Class.forName(dbDriver);
        //
        java.util.Properties connProperties = new java.util.Properties();
        connProperties.put("user", dbUser);
        connProperties.put("password", dbPassword);
        connProperties.put("autoReconnect", "true");
        connProperties.put("maxReconnects", "4");
        //
        return DriverManager.getConnection(dbURL, connProperties);
   }
   */
}

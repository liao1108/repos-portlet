package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.jdbc.pool.DataSource;

public class TopicRatingPortlet extends GenericPortlet {
	ECourseUtils utils = new ECourseUtils();
	DataSource ds = null;
	String tableSchema = "";
    
	public void init() {
    		Connection conn = null;
		try{
    			viewJSP = getInitParameter("view-jsp");
    			tableSchema = getInitParameter("table_schema");
    			//
    			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		conn = ds.getConnection();
	   		//
   			String sql = "CREATE TABLE IF NOT EXISTS topic_rating (topic_id VARCHAR(50) NOT NULL, rating_time VARCHAR(50) NOT NULL, rating_score int, PRIMARY KEY (topic_id, rating_time))";
   			PreparedStatement ps = conn.prepareStatement(sql);
   			ps.execute();
   			//
   			try{
   				sql = "ALTER TABLE topic_rating ADD topic_title VARCHAR(100)";
   				ps = conn.prepareStatement(sql);
   				ps.execute();
   			}catch(Exception _ex){
   			}
   			
   			//補填Topic資料
		    try{
		        sql = "SELECT topic_id FROM topic_rating WHERE topic_title IS NULL OR topic_title = '' GROUP BY topic_id";
		        ps = conn.prepareStatement(sql);
		        ResultSet rs = ps.executeQuery();
		        while(rs.next()){
		        		int topicId = rs.getInt(1);
		        		//
		        		try{
		        			if(DLAppLocalServiceUtil.getFolder(topicId) != null){
		        				sql = "UPDATE topic_rating SET topic_title=? WHERE topic_id = ?";
		        				ps = conn.prepareStatement(sql);
		        				ps.setString(1, DLAppLocalServiceUtil.getFolder(topicId).getName());
		        				ps.setInt(2, topicId);
		        				ps.execute();
		        				
		        			}
		        		}catch(Exception _ex){
		        			
		        		}
		        }
		        rs.close();
		    }catch(Exception _ex){
		    	
		    }
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}finally{
    			try{
    				if(conn != null) conn.close();
    			}catch(Exception _ex){
    				
    			}
    		}
    }
    
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse)throws IOException, PortletException {
    		renderResponse.setContentType("text/html");
    		//
    		String errMsg = "";
    		long topicID = 0;
    		String topicUUID = "";
    		Connection conn = null;
    		try{
			//Check id rating table exist
    			conn = ds.getConnection();
   			//
   			long userId = -1;
			try{
				userId = PortalUtil.getUser(renderRequest).getUserId();
			}catch(Exception _ex){
				
			}
			//
			HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
	    		//
	    		if(renderRequest.getAttribute("topicID") != null){
	    			topicID = Long.parseLong(renderRequest.getAttribute("topicID").toString());
	    		}else if(httpRequest.getParameter("topicID") != null){
	    			topicID = Long.parseLong(httpRequest.getParameter("topicID"));
	    		}else{
	    			//throw new Exception("Topic ID not exist !");
	    		}
	    		if(renderRequest.getAttribute("topicUUID") != null){
	    			topicUUID = renderRequest.getAttribute("topicUUID").toString();
	    		}else if(httpRequest.getParameter("topicUUID") != null){
	    			topicUUID = httpRequest.getParameter("topicUUID");
	    		}else{
	    			//throw new Exception("Topic ID not exist !");
	    		}
	    		if(!topicUUID.equals("")){
				String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
			    Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
				String currDateTime = sdf.format(cal.getTime());
				//
				String sql = "INSERT INTO topic_read (topic_id, read_time) VALUES (?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, topicUUID);
				ps.setString(2, currDateTime);
				ps.execute();
	    		}else if(topicID > 0){
	    		
	    		}else{
	    			throw new Exception("Topic ID not exist !");
	    		}
	    		//
			renderRequest.setAttribute("topicID", String.valueOf(topicID));
			renderRequest.setAttribute("topicUUID", topicUUID);
			renderRequest.setAttribute("userId", String.valueOf(userId));
    		}catch (Exception ex){
    			if(errMsg.equals("")) errMsg += "\n";
    			String itezErr = utils.getItezErrorCode(ex);
    			errMsg +=  itezErr;
    			System.out.println(itezErr);
        }finally{
        		if (conn !=null){
        			try{
        				conn.close();
        			}catch (Exception ignore){
        			}
        		}
        }
    		renderRequest.setAttribute("errMsg", errMsg);
		//
        include(viewJSP, renderRequest, renderResponse);    	
    }

    protected void include(String path,
    						RenderRequest renderRequest,
    						RenderResponse renderResponse) throws IOException, PortletException {
        PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(path);
        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        } else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(TopicRatingPortlet.class);

}

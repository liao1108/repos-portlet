<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="topicID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="topicUUID" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="userId" scope="request"></jsp:useBean>

<portlet:defineObjects />

<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.4.4.min.js" type="text/javascript"></script>

<script type="text/javascript" language="javascript">
	
	function doRating(){
		var ratingDone = document.getElementById("rating_done").value;
		if(ratingDone == "N"){
			var userId = document.getElementById("userId").value;
			var topicId = document.getElementById("topicUUID").value;
			if(topicId == ""){
				topicId = document.getElementById("topicID").value;
			}
			var rating = document.getElementById("rating").value;
			//
			var urlServlet = "/delegate/UpdateRating?userId=" + userId +
													"&topicID=" + topicId +
													"&rating=" + rating;
			//alert(urlServlet);
			$.post(urlServlet);
			alert("感謝您給的評分 " + rating + " 分.");
			//
			document.getElementById("rating_done").value = "Y";
		}
	}
	
	function doRatingAndSuggest(){
		doRating();
		//
		window.location = "/comment";
	}
	
	function viewRatingResult(){
		window.location = "/viewresult";	
	}
	
	function radioChange(rating){
		//alert(rating);
		document.getElementById("rating").value = rating;	
	}
	
	function httpRequest(reqType,url,asynch) {
		//Mozilla-based browsers
		if (window.XMLHttpRequest) {
			request = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			request = new ActiveXObject("Msxml2.XMLHTTP");
			if (!request) {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		// Request could still be null if neither ActiveXObject
		// initialization succeeded
		if (request) {
			// If the reqType param is POST, then the fifth arg is the POSTed data
			if (reqType.toLowerCase() != "post") {
				initReq(reqType, url, asynch, respHandle);
			} else {
				// The POSTed data
				var args = arguments[4];
				if (args != null && args.length > 0) {
					initReq(reqType, url, asynch, respHandle, args);
				}
			}
		} else {
			alert("Your browser does not permit the use of all " +
					"of this application's features!");
		}
	}
	
	function forwardToFullView(){
		window.location = document.getElementById("fullViewURI").value + "&swfBookmarkPage=" + document.getElementById("swfBookmarkPage").value;
	}
	
	// ----------------------------------------
	// Initialize a request object that is already constructed
	// ----------------------------------------
	function initReq(reqType, url, bool, respHandle) {
		try {
			// Specify the function that will handle the HTTP response
			request.onreadystatechange = handleResponse;
			request.open(reqType, url, bool);
			// If the reqType param is POST, then the
			//   fifth argument to the function is the POSTed data
			if (reqType.toLowerCase() == "post") {
				// Set the Content-Type header for a POST request
				request.setRequestHeader("Content-Type", "application/x-ww-form-urlencoded; charset=UTF-8");
				request.send(arguments[4]);
			} else {
				request.send(null);
			}
		} catch (errv) {
			alert("The application cannot contact the server at the moment. " +
					"Please try again in a few seconds.\n" +
					"Error detail: " + errv.message);
		}
	}
</script>

<form id="myRateForm" method="POST" action="<portlet:actionURL/>">
<input type="hidden" name="topicID" Id="topicID" value="<%=topicID%>"/>
<input type="hidden" name="topicUUID" Id="topicUUID" value="<%=topicUUID%>"/>
<input type="hidden" name="userId" Id="userId" value="<%=userId%>"/>
<input type="hidden" name="rating" Id="rating" value="5"/>
<input type="hidden" name="rating_done" Id="rating_done" value="N"/>

<table width="100%" border = "0">
  <tr>
	<td nowrap>
		<span style='color: DarkRed; font-familt:微軟正黑體; font-size: 16pt; padding:1px'>單元評價</span>
	</td>
	<td nowrap>
		<input type="radio" name="_rating" onClick="javascript:radioChange('5');" value="5" checked>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		
		<input type="radio" name="_rating" onClick="javascript:radioChange('4');" value="4">
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		
		<input type="radio" name="_rating" onClick="javascript:radioChange('3');" value="3">
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>

		<input type="radio" name="_rating" onClick="javascript:radioChange('2');" value="2">
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
		
		<input type="radio" name="_rating" onclick="javascript:radioChange('1');" value="1">
		<img src="<%=renderRequest.getContextPath()%>/images/star.png"/>
	</td>	
	
	<td nowrap>&nbsp;&nbsp;
		<a href="javascript:doRating()">
			<img src="<%=renderRequest.getContextPath()%>/images/rating_submit.png"/>
		</a>
		<a href="javascript:doRatingAndSuggest()">
			<img src="<%=renderRequest.getContextPath()%>/images/rating_submit2.png"/>
		</a>
		<a href="javascript:viewRatingResult()">	
			<img src="<%=renderRequest.getContextPath()%>/images/rating_submit3.png"/>
		</a>	
	</td>
	
	<td width="90%">&nbsp;</td>
  </tr>	
</table>
</form>
package com.itez;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ListUsersPortlet
 */
public class ListUsersPortlet extends MVCPortlet {
	DataSource ds = null;
	//Utils utils = new Utils();
	
	@Override
	public void init() throws PortletException {
		super.init();
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			Vector<Auditor> vecAuditor = new Vector<Auditor>();
			conn = ds.getConnection();
			String sql = "SELECT * FROM auditors ORDER BY bank_no, user_no";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Auditor a = new Auditor();
				if(rs.getString("bank_no") != null){
					a.bankNo = rs.getString("bank_no");
				}
				if(rs.getString("bank_name") != null){
					a.bankName = rs.getString("bank_name");
				}
				if(rs.getString("user_no") != null){
					a.userNo = rs.getString("user_no");
				}
				if(rs.getString("user_name") != null){
					a.userName = rs.getString("user_name");
				}
				//if(rs.getString("passwd") != null){
				//	a.passwd = rs.getString("passwd");
				//}
				if(rs.getString("email") != null){
					a.email = rs.getString("email");
				}
				//
				vecAuditor.add(a);
			}
			renderRequest.getPortletSession(true).setAttribute("vecAuditor", vecAuditor);
			
			//其他金融機構人員
			Vector<FinStaff> vecStaff = new Vector<FinStaff>();
			sql = "SELECT * FROM fin_staff ORDER BY fin_id";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				FinStaff staff = new FinStaff();
				if(rs.getString("fin_id") != null){
					staff.finId = rs.getString("fin_id");
				}
				if(rs.getString("fin_type") != null){
					staff.finType = rs.getString("fin_type");
				}
				if(rs.getString("fin_name") != null){
					staff.finName = rs.getString("fin_name");
				}
				if(rs.getString("static_ip") != null){
					staff.staticIp = rs.getString("static_ip");
				}
				if(rs.getString("staff_name") != null){
					staff.staffName = rs.getString("staff_name");
				}
				if(rs.getString("staff_title") != null){
					staff.staffTitle = rs.getString("staff_title");
				}
				if(rs.getString("staff_phone") != null){
					staff.staffPhone = rs.getString("staff_phone");
				}
				if(rs.getString("staff_email") != null){
					staff.staffEmail = rs.getString("staff_email");
				}
				if(rs.getInt("force_ip") == 1){
					staff.forceIp = true;
				}
				if(rs.getString("login_id") != null){
					staff.loginId = rs.getString("login_id");
				}
				if(rs.getString("login_pwd") != null){
					staff.loginPwd = rs.getString("login_pwd");
				}
				//排除測試者
				if(staff.loginId.equalsIgnoreCase("A001")) continue;
				//
				vecStaff.add(staff);
			}
			renderRequest.getPortletSession(true).setAttribute("vecStaff", vecStaff);
			
			
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				//errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}
	
	public void doRemoveUser(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String userNo = actionRequest.getParameter("userNo");
			//
			conn = ds.getConnection();
			String sql = "DELETE FROM auditors WHERE user_no = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userNo);
			ps.execute();
			//
			try{
				User user = UserLocalServiceUtil.getUserByScreenName(themeDisplay.getCompanyId(), userNo);
				UserLocalServiceUtil.deleteUser(user);
			}catch(Exception _ex){
				_ex.printStackTrace();
			}
			//
			Vector<Auditor> vecAuditor = new Vector<Auditor>();
			if(actionRequest.getPortletSession(true).getAttribute("vecAuditor") != null){
				vecAuditor = (Vector<Auditor>)actionRequest.getPortletSession(true).getAttribute("vecAuditor");
			}
			for(Auditor a: vecAuditor){
				if(a.userNo.equals(userNo)){
					int idx = vecAuditor.indexOf(a);
					vecAuditor.remove(idx);
					break;
				}
			}
			actionRequest.getPortletSession(true).setAttribute("vecAuditor", vecAuditor);
		}catch(Exception ex){
			ex.printStackTrace();
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	public void addFinUser(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			Vector<FinStaff> vecStaff = (Vector<FinStaff>)actionRequest.getPortletSession(true).getAttribute("vecStaff");
			for(FinStaff staff: vecStaff){
				if(staff.loginId.isEmpty() || staff.loginPwd.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的帳號或密碼空白，無法匯入Liferay。");
				if(staff.loginId.contains("_")) throw new Exception(staff.finName + staff.staffName + "的帳號不可包含底線(_)符號。");
				if(staff.staffEmail.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的電子郵件空白，無法匯入Liferay。");
				if(staff.staticIp.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的固定ip空白，無法匯入Liferay。");
			}
			//
			int cc = 0;
			for(FinStaff staff: vecStaff){
				long creatorUserId = themeDisplay.getUserId(); 	// default liferay user
	    		long companyId = themeDisplay.getCompanyId(); 	// default company
	    		Role role = RoleLocalServiceUtil.getRole(companyId, "Auditor");
	    		//已存在者略過
	    		User u = null;
	    		try{
	    			u = UserLocalServiceUtil.getUserByEmailAddress(companyId, staff.staffEmail);
	    		}catch(Exception _ex){
	    		}
	    		if(u != null) continue;
	    		//
	    		boolean autoPassword = false;
	    		String password1 = staff.loginPwd;
	    		String password2 = staff.loginPwd;
	    		boolean autoScreenName = false;
	    		String screenName = staff.loginId;
	    		String emailAddress = staff.staffEmail;
	    		long facebookId = 0;
	    		String openId = "";
	    		//Locale locale = themeDisplay.getLocale();
	    		String firstName = staff.staffName;
	    		String middleName = "";
	    		String lastName = ".";
	    		int prefixId = 0;
	    		int suffixId = 0;
	    		boolean male = true;    
	    		int birthdayMonth = 1;
	    		int birthdayDay = 1;
	    		int birthdayYear = 1970;
	    		String jobTitle = "";

	    		long[] groupIds = {};
	    		long[] organizationIds = {};
	    		long[] roleIds = {role.getRoleId()};
	    		long[] userGroupIds = {};

	    		boolean sendEmail = false;

	    		//ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
	    		com.liferay.portal.service.ServiceContext serviceContext = new com.liferay.portal.service.ServiceContext();
	    		UserLocalServiceUtil.addUser(creatorUserId,		//10196,
											 companyId,		//10154,
						                     autoPassword,
						                     password1,
						                     password2,
						                     autoScreenName,
						                     screenName,
						                     emailAddress,
						                     facebookId,
						                     openId,
						                     LocaleUtil.getDefault(),
						                     firstName,
						                     middleName,
						                     lastName,
						                     prefixId,
						                     suffixId,
						                     male,
						                     birthdayMonth,
						                     birthdayDay,
						                     birthdayYear,
						                     jobTitle,
						                     groupIds,
						                     organizationIds,
						                     roleIds,
						                     userGroupIds,
						                     sendEmail,
						                     serviceContext);
	    		cc++;
			}
			SessionMessages.add(actionRequest, "success");
			actionRequest.setAttribute("successMsg", "已成功新增 " + cc + " 人員到Liferay。");
		}catch(Exception ex){
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}
	
	public void updateFinUser(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			HashMap<String, String> mapEmail = new HashMap<String, String>();
			Vector<FinStaff> vecStaff = (Vector<FinStaff>)actionRequest.getPortletSession(true).getAttribute("vecStaff");
			for(FinStaff staff: vecStaff){
				if(staff.loginId.isEmpty() || staff.loginPwd.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的帳號或密碼空白，無法匯入Liferay。");
				if(staff.loginId.contains("_")) throw new Exception(staff.finName + staff.staffName + "的帳號不可包含底線(_)符號。");
				if(staff.staffEmail.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的電子郵件空白，無法匯入Liferay。");
				if(staff.staticIp.isEmpty()) throw new Exception(staff.finName + staff.staffName + "的固定ip空白，無法匯入Liferay。");
				//
				if(!mapEmail.containsKey(staff.staffEmail)){
					mapEmail.put(staff.staffEmail, staff.finName + staff.staffName);
				}else{
					throw new Exception(staff.finName + staff.staffName + "的email帳號與" + mapEmail.get(staff.staffEmail) + "重複。");
				}
			}
			//
			int cc = 0;
			for(FinStaff staff: vecStaff){
				long creatorUserId = themeDisplay.getUserId(); 	// default liferay user
	    		long companyId = themeDisplay.getCompanyId(); 	// default company
	    		Role role1 = RoleLocalServiceUtil.getRole(companyId, "Auditor");
	    		Role role2 = RoleLocalServiceUtil.getRole(companyId, "ResetPwd");
	    		//已存在者略過
	    		User u = null;
	    		try{
	    			u = UserLocalServiceUtil.getUserByEmailAddress(companyId, staff.staffEmail);
	    		}catch(Exception _ex){
	    		}
	    		if(u == null) continue;
	    		//
	    		UserLocalServiceUtil.deleteRoleUser(role1.getRoleId(), u.getUserId());
	    		//
	    		long[] ll = {u.getUserId()};
	    		UserLocalServiceUtil.addRoleUsers(role1.getRoleId(), ll);
	    		//
	    		cc++;
			}
			SessionMessages.add(actionRequest, "success");
			actionRequest.setAttribute("successMsg", "已成功 " + cc + " 人員的角色為「重設密碼」。");
		}catch(Exception ex){
			String errMsg = getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}
	}

	private String getItezErrorCode(Exception ex){
		String ret = ex.getMessage();
		//
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		ex.printStackTrace(printWriter);
		//
		BufferedReader reader = new BufferedReader(
				   						new StringReader(stringWriter.toString()));
		try{
			String str = "";
			while((str = reader.readLine()) != null){
	           if(str.contains("com.itez")){
	        	   ret += ("\n" + str);
	           }
			}
		}catch(IOException e){
		   e.printStackTrace();
		}
		return ret;
	}
	
}

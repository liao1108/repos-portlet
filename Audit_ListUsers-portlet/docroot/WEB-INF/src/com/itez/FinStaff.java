package com.itez;

public class FinStaff {
	public String finId = "";
	public String finType = "";			//業別
	public String finName = "";			//機構名稱
	public String staticIp = "";		//對外ip，以 ; 區隔
	public String staffName = "";		//專責人員姓名
	public String staffTitle = "";		//專責人員職稱
	public String staffPhone = "";		//專責人員電話
	public String staffEmail = "";		//專責人員電子郵件
	public boolean forceIp = false;		//是否檢查來源ip
	public String loginId = "";			//登入帳號
	public String loginPwd = "";		//登入密碼	
}

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.HtmlUtil"%> 

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.Auditor" %>
<%@ page import="com.itez.FinStaff" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay" %>

<portlet:defineObjects />

<style type="text/css">
  	.th {
    			font-family:標楷體;
			font-size:14pt;
			color:darkred;
    		}
    	_tr  {
    			height: 35x
    		}
  	td {
    			font-family:標楷體;
			font-size:12pt;
    		}
    	.tb{
    			font-family:標楷體;
			font-size:12pt;
			color:darkgreen;
    		}
    	.err{
    			font-family:標楷體;
			font-size:12pt;
			color:red;
    		}
    	textarea{
     		width:100%;
    		height:100%;
	}
	select {
    			font-family:標楷體;
			font-size:12pt;
    		}	
  </style>
  
<%
  	Vector<Auditor> vecAuditor = new Vector<Auditor>();
  	if(renderRequest.getPortletSession(true).getAttribute("vecAuditor") != null){
  		vecAuditor = (Vector<Auditor>)renderRequest.getPortletSession(true).getAttribute("vecAuditor");
  	}
  	//
  	Vector<FinStaff> vecStaff = new Vector<FinStaff>();
  	if(renderRequest.getPortletSession(true).getAttribute("vecStaff") != null){
  		vecStaff = (Vector<FinStaff>)renderRequest.getPortletSession(true).getAttribute("vecStaff");
  	}
  	//
  	String errMsg = "";	
  	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
  	
  	String successMsg = "";	
  	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
	
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
  %>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="removeUserURL" name="removeUser">
</portlet:actionURL>

<portlet:actionURL var="addFinUserURL"  name="addFinUser" >
</portlet:actionURL>

<portlet:actionURL var="updateFinUserURL"  name="updateFinUser" >
</portlet:actionURL>


<a href="<%= themeDisplay.getURLMyAccount()%>">修改我的基本資料</a>

<liferay-ui:search-container emptyResultsMessage="目前尚未建立銀行稽核人員" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecAuditor, searchContainer.getStart(), searchContainer.getEnd());
			total = vecAuditor.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.Auditor" keyProperty="userNo" modelVar="Auditor">
		<liferay-ui:search-container-column-text name="銀行代號" property="bankNo"/>
		<liferay-ui:search-container-column-text name="銀行名稱" property="bankName"/>
		<liferay-ui:search-container-column-text name="使用者代名" property="userNo"/>
		<liferay-ui:search-container-column-text name="使用者姓名" property="userName"/>
		<liferay-ui:search-container-column-text name="電子郵件" property="email"/>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<hr/>

<aui:fieldset >
	<aui:form action="<%=updateFinUserURL%>" method="post" name="<portlet:namespace />fm">
		<aui:button-row>
			<aui:button type="submit"  value="更新非銀行之金融機構人員為「重設密碼」角色" cssClass="button" />
		</aui:button-row>
	</aui:form>
</aui:fieldset>

<liferay-ui:search-container emptyResultsMessage="目前尚未建立非銀行之金融機構人員" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecStaff, searchContainer.getStart(), searchContainer.getEnd());
			total = vecStaff.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	<liferay-ui:search-container-row className="com.itez.FinStaff" keyProperty="loginId" modelVar="FinStaff">
		<liferay-ui:search-container-column-text name="機構代碼" property="finId"/>
		<liferay-ui:search-container-column-text name="機構業別" property="finType"/>
		<liferay-ui:search-container-column-text name="機構名稱" property="finName"/>
		<liferay-ui:search-container-column-text name="對外ip" property="staticIp"/>
		<liferay-ui:search-container-column-text name="專責人員" property="staffName"/>
		<liferay-ui:search-container-column-text name="職稱" property="staffTitle"/>
		<liferay-ui:search-container-column-text name="電話" property="staffPhone"/>
		<liferay-ui:search-container-column-text name="電子郵件" property="staffEmail"/>
		<liferay-ui:search-container-column-text name="檢查ip" property="forceIp"/>
		<liferay-ui:search-container-column-text name="登入帳號" property="loginId"/>
		<liferay-ui:search-container-column-text name="密碼" property="loginPwd"/>
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>


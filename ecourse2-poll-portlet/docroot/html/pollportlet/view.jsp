<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="ansData" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />
<head>
</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<table border='0' width='100%'>
 		<tr valign='top'>
 			<td align='right'><img src='<%=renderRequest.getContextPath()%>/images/finish.png'/></td>
 			<td align='left' valign='middle'>
 				<span style='color: DarkRed; font-familt:微軟正黑體; font-size: 18pt; padding:10px'>
 					系統已記錄完成。感謝 您的寶貴意見，歡迎 您再次光臨賜教，謝謝。
 				</span>
 			</td>
 		</tr>
 		<tr>
 			<td colspan='2' align='center'>
 				<span style='color: DarkRed; font-familt:微軟正黑體; font-size: 12pt; padding:10px'>
 					<input type='button' value='回課程選單主畫面' onClick="document.location.href('/courselist');"/>
 				</span>	
 			</td>
 		</tr>
	</table>
</body>

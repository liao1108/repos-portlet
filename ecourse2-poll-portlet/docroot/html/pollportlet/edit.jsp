<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="htmlStr" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="pollStartTime" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="pollId" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

	<STYLE TYPE="text/css">
	<!--
		TD{font-family: 微軟正黑體; font-size: 14pt; padding:5px;}
	-->
	</STYLE>
</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<form id="myformQuiz" method="POST" action="<portlet:actionURL/>">
		<%=htmlStr%>
		<input type="hidden" name = "pollStartTime" value="<%=pollStartTime%>"/>
		<input type="hidden" name = "pollId" value="<%=pollId%>"/>
		<div align = 'center'>
			<input type="submit" name="Submit" value=" 送 出 問 卷 "/>
		</div>
	</form>
</body>

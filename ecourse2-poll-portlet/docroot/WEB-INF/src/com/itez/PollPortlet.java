package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.tomcat.jdbc.pool.DataSource;

public class PollPortlet extends GenericPortlet {
	String eCoursePollFoler = "";	//問卷調查根目錄
	ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	
    public void init() {
    	try{
	    	viewJSP = getInitParameter("view-jsp");
	    	editJSP = getInitParameter("edit-jsp");
	    	//
	    	Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	    	//
	   		eCoursePollFoler = getInitParameter("ecourse-poll-folder");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
    	renderResponse.setContentType("text/html");
		//
    	if(renderRequest.getAttribute("submitted") != null){
    		String ansData = "";
    		if(renderRequest.getAttribute("ansData") != null){
    			ansData = renderRequest.getAttribute("ansData").toString();
    		}
    		renderRequest.setAttribute("ansData", ansData);
    		this.include(viewJSP, renderRequest, renderResponse);
    		return;
    	}
    	
    	String errMsg = "";
   		//
    	String htmlStr = "";
		//
    	Calendar cal = Calendar.getInstance();
    	String pollStartTime = cal.get(Calendar.YEAR) + "-" +
    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
    							String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
    							String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
    							String.format("%02d", cal.get(Calendar.SECOND));
    	String strCurrDate = cal.get(Calendar.YEAR) + 
    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + 
    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH));
    	//System.out.println("Current Date: " + strCurrDate);
		//int currPollIndex = 0;	//
		//
    	FileEntry fPoll = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	//HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
			//
			Vector<Poll> vecPolls = new Vector<Poll>();
			//
			Hashtable<String, FileEntry> ht = new Hashtable<String, FileEntry>();
			Vector<String> vec = new Vector<String>();
			List<Folder> list = DLAppServiceUtil.getFolders(themeDisplay.getScopeGroupId(), 0);
	   		for(Folder fd: list){
	   			if(fd.getName().equals(eCoursePollFoler)){
	   				List<FileEntry> listFile = DLAppServiceUtil.getFileEntries(themeDisplay.getScopeGroupId(), fd.getFolderId());
	   				for(FileEntry f: listFile){
	   					if(f.getTitle().toLowerCase().startsWith("poll") && f.getTitle().toLowerCase().endsWith("xls")){
	   						ht.put(f.getTitle(), f);
	   						vec.add(f.getTitle());
	   					}
	   				}
	   				break;
	   			}
	   		}
	   		if(vec.size() > 0){
	   			Collections.sort(vec);
	   			//
	   			String strTargetName = "";
	   			String strActiveDate = "";
	   			for(String s: vec){
	   				if(s.contains("_")){
	   					int idx = s.lastIndexOf(".");
	   					String[] ss = s.substring(0, idx).split("_");
	   					if(ss[1].compareTo(strCurrDate) <= 0 && ss[1].compareTo(strActiveDate) > 0){
	   						strActiveDate = ss[1];
	   						strTargetName = s;
	   					}
	   				}
	   			}
	   			if(!strTargetName.equals("")){
	   				fPoll = ht.get(strTargetName);
	   			}
	   		}
			if(fPoll == null){
				throw new Exception("找不到問卷調查檔案 !");
			}
			//
			//conn = ds.getConnection();
			//2.由檔案讀取
			if(vecPolls.size() == 0){
				Workbook wb = Workbook.getWorkbook(fPoll.getContentStream());
			    Sheet ws = wb.getSheet(0);
			    for(int i = 0; i < ws.getRows(); i++){
			    	if(ws.getCell(0, i).getContents() != null 
			    			&& !ws.getCell(0, i).getContents().equals("")
			    			&& !ws.getCell(0, i).getContents().startsWith("題")
			    			&& !ws.getCell(1, i).getContents().equals("1")
			    			&& !ws.getCell(1, i).getContents().equalsIgnoreCase("Y")){
			    		String seqNo = ws.getCell(0, i).getContents().trim();
	    				Poll p = new Poll();
			    		p.seqNo = seqNo;
			    		p.pollText = ws.getCell(2, i).getContents();
			    		p.ansOption = ws.getCell(3, i).getContents();
	    				vecPolls.add(p);
			    	}
			    }
			    wb.close();
			}
			//
			if(vecPolls.size() == 0){
				throw new Exception("問卷調查檔案內沒有題目 !");
			}
		    //
		    if(renderRequest.getAttribute("pollStartTime") != null){
		    	pollStartTime = renderRequest.getAttribute("pollStartTime").toString();
		    }
		    //排問卷畫面
		    htmlStr += "<table class='polltable' border='0' width='100%'>";
 			htmlStr += "<tr valign='top'>";
 			htmlStr += "<td align='right'><img src='" + renderRequest.getContextPath() + "/images/poll.png'/></td>";
 			htmlStr += "<td align='left'><span style='color: DarkRed; font-familt:微軟正黑體; font-size: 18pt; padding:10px'>本問卷調查共有 " + vecPolls.size() + " 個題目，敬請 撥冗惠賜寶貴意見，謝謝。" + "</span></td>";
 			htmlStr += "</tr>";
			htmlStr += "</table>";
		    for(Poll p: vecPolls){
	 			htmlStr += "<table class='polltable' border='0' width='100%'>";
 				//
 				String pollText = p.pollText;
 				String optText = p.ansOption;
 				//
 				String imgPath = renderRequest.getContextPath() + "/images/question.png";
 				//
 				htmlStr += "<tr valign='top'>";
 				htmlStr += "<td><img src='" + imgPath + "'/></td>";
 				htmlStr += "<td>" + (vecPolls.indexOf(p) + 1) + ". " + "</td>";
 				htmlStr += "<td  width='100%'><span style='font-family:微軟正黑體'>"+  pollText + "</span></td>";
 				htmlStr += "</tr>";
 				//
 				htmlStr += "<tr valign='top'>";
 				htmlStr += "<td colspan='2'>&nbsp;</td>";
 				htmlStr += "<td>";
 				
 				if(!optText.equals("")){
	 				String[] opt = optText.split("#");
	 				for(int j=0; j < opt.length; j++){
	 					String text = opt[j].trim();
	 					if(text.startsWith("(")){
	 						int idx = text.indexOf(")");
	 						text = "<table width='100%'><tr valign='top'>" +
	 									"<td>" + text.substring(0, idx + 1) + "</td>" +
	 									"<td width='100%'>" +
	 									"<span style='font-family:微軟正黑體'>" + text.substring(idx+1) +"</span>" +
	 									"</td>" +
	 								"</tr></table>";
	 					}
	 					String optName = "CBox_" + (vecPolls.indexOf(p) + 1) + "_" + (j + 1);
	 					//
	 					String checked = "";
	 					if(!p.ansData.equals("") && p.ansData.contains(optName)){
	 						checked = "checked";
	 					}
	 					//
	 					//String readOnly = "";
	 					//if(submitCount%2 == 1) readOnly = "disabled='disabled'";
	 					htmlStr += "<table width='100%'><tr background='" + renderRequest.getContextPath() + "/images/poll_bg.png'>";
	 					htmlStr += "<td background='" + renderRequest.getContextPath() + "/images/poll_bg.png' valign='top'><input type='CHECKBOX' name='" + optName + "' " + checked + "></td>";
	 					htmlStr += "<td width='100%'  background='" + renderRequest.getContextPath() + "/images/poll_bg.png' valign='top'><span style='font-family:微軟正黑體'>" + text +"</span></td>";
	 					htmlStr += "</tr></table>";
	 				}
 				}else{
 					String txtName = "TEXT_" + (vecPolls.indexOf(p) + 1);
 					htmlStr += "<p>";
 					htmlStr += "<TEXTAREA NAME='" + txtName + "' ROWS='5' COLS='100%' WRAP='SOFT'></TEXTAREA>";
 					htmlStr += "</p>";
 				}
 				htmlStr += "</td>";
 				htmlStr += "</tr>";
 				htmlStr += "</table>";
		    }
        } catch (Exception ex){
        	if(errMsg.equals("")) errMsg += "\n";
        	errMsg +=  utils.getItezErrorCode(ex);
        }
        //
		renderRequest.setAttribute("htmlStr", htmlStr);
		renderRequest.setAttribute("pollStartTime",  pollStartTime);
		if(fPoll != null){
			renderRequest.setAttribute("pollId", String.valueOf(fPoll.getFileEntryId()));
		}
        renderRequest.setAttribute("errMsg", errMsg);
        //
        this.include(editJSP, renderRequest, renderResponse);
    }

    
    public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException{
    	String errMsg = "";
    	String ansData = "";
    	Connection conn = null;
    	try{
	    	Calendar cal = Calendar.getInstance();
	    	String pollEndTime = cal.get(Calendar.YEAR) + "-" +
	    							String.format("%02d", cal.get(Calendar.MONTH) + 1) + "-" +
	    							String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " +
	    							String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" +
	    							String.format("%02d", cal.get(Calendar.MINUTE)) + ":" +
	    							String.format("%02d", cal.get(Calendar.SECOND));
	
	    	//
	    	String pollStartTime = "";
	    	if(actionRequest.getParameter("pollStartTime") != null){
	    		pollStartTime = actionRequest.getParameter("pollStartTime");
	    	}
	    	
	    	long pollId = 0;
	    	if(actionRequest.getParameter("pollId") != null && !actionRequest.getParameter("pollId").toString().equals("")){
	    		pollId = Long.parseLong(actionRequest.getParameter("pollId"));
	    	}
	    	//
	    	Hashtable<Integer, String> htAns = new Hashtable<Integer, String>();
	    	//
	    	Enumeration<String> en = actionRequest.getParameterNames();
		    while(en.hasMoreElements()){
		    	String param = en.nextElement();
		    	if(param.startsWith("CBox_")){
		    		int currIndex = Integer.parseInt(param.split("_")[1]);
		    		//
		    		String ans = "";
		    		if(htAns.containsKey(currIndex)){
		    			ans = htAns.get(currIndex);
		    		}
		    		if(!ans.equals("")) ans += ",";
		    		ans += ("CBox_" + param.split("_")[2]);
		    		//
		    		htAns.put(currIndex, ans);
		    	}else if(param.startsWith("TEXT_") 
		    			&& actionRequest.getParameter(param) != null
		    			&& !actionRequest.getParameter(param).trim().equals("")){
		    		int currIndex = Integer.parseInt(param.split("_")[1]);
	    			htAns.put(currIndex, "TEXT_" + actionRequest.getParameter(param).trim());
		    	}
		    }
		    //
		    Enumeration<Integer> en2 = htAns.keys();
		    while(en2.hasMoreElements()){
		    	int index = en2.nextElement();
		    	//
		    	String s = htAns.get(index).trim();
		    	if(!s.split("_")[1].trim().equals("")){
		    		if(!ansData.equals("")) ansData += "#";
		    		ansData += (index + ":" + s);
		    	}
		    }
		    
		    long userId = PortalUtil.getUser(actionRequest).getUserId();
	    
		    //寫入資料庫
		    conn = ds.getConnection();
		    //
		    String sql = "INSERT INTO trace_poll (u_id, poll_id, poll_start_time, poll_end_time, ans_data) VALUES (?,?,?,?,?)";
		    PreparedStatement ps = conn.prepareStatement(sql);
		    ps.setLong(1, userId);
		    ps.setLong(2, pollId);
		    ps.setString(3, pollStartTime);
		    ps.setString(4, pollEndTime);
		    ps.setString(5, ansData);
		    ps.execute();
    	} catch (Exception ex){
	       	if(errMsg.equals("")) errMsg += "\n";
	       	errMsg +=  utils.getItezErrorCode(ex);
	    }finally{
	       	if (conn !=null){
	       		try{
	       			conn.close();
	       		}catch (Exception ignore){
	       		}
	       	}
	    }
	    //
        actionRequest.setAttribute("errMsg", errMsg);
	    actionRequest.setAttribute("ansData", ansData);
	    if(!ansData.equals("")){
	    	actionRequest.setAttribute("submitted", "true");
	    }
    }
    
    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
 
    protected String viewJSP;
    protected String editJSP;

    private static Log _log = LogFactoryUtil.getLog(PollPortlet.class);

}

package com.itez;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.sql.DataSource;

//import org.apache.tomcat.jdbc.pool.DataSource;

public class NaviPortlet extends GenericPortlet {
	String eCourseRootFoler = "";	//課程根目錄
	//ECourseUtils utils = new ECourseUtils();
	
	DataSource ds = null;
	
	public void init() {
		try{
			viewJSP = getInitParameter("view-jsp");
			//
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/efeb");
	   		//
	   		eCourseRootFoler = getInitParameter("ecourse-root-folder");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
    
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
   		String errMsg = "";
   		Connection conn = null;
   		//
   		String topicTree = "";
   		try{
   			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	   		//User currUser = themeDisplay.getRealUser();
	   		//
	   		//long companyId = themeDisplay.getCompanyId();
	   		long groupId = themeDisplay.getScopeGroupId();
	   		long userId = themeDisplay.getUserId();
	   		//long repositoryId = CompanyConstants.SYSTEM;
	   		//
	   		conn = ds.getConnection(); 
	   		//主題暫存
			Hashtable<Subject, Vector<Topic>> htSubject = new Hashtable<Subject, Vector<Topic>>();
	   		//
			//System.out.println(eCourseRootFoler);
	   		List<Folder> list = DLAppServiceUtil.getFolders(groupId, 0);
	   		for(Folder fd: list){
	   			//System.out.println("Folder: " + fd.getName().trim());
	   			if(fd.getName().startsWith(eCourseRootFoler)){
	   				//System.out.println(eCourseRootFoler);
	   				List<Folder> list2 = DLAppServiceUtil.getFolders(groupId, fd.getFolderId());
	   				for(Folder fd2: list2){			//主題
	   					//System.out.println("Subject: " + fd2.getName());
	   					//單元
						Vector<Topic> vecTopic = new Vector<Topic>();
						//
	   					List<Folder> list3 = DLAppServiceUtil.getFolders(groupId, fd2.getFolderId());
	   					for(Folder fd3: list3){		//單元
	   						//System.out.println("Topic: " + fd3.getName());
		   					//檢查單元閱讀狀態
							String status = "UnRead";
							Topic topic = new Topic();
							topic.name = fd3.getName();
							topic.id = fd3.getFolderId();
							//
		   					String sql = "SELECT * FROM trace_quiz WHERE u_id=? " +
		   									" AND topic_uuid=? " +
		   									" AND (quiz_end_time IS NOT NULL AND quiz_end_time <> '') " +
		   									" AND tot_score > 0 " +
		   									" AND got_score = tot_score";
		   					PreparedStatement ps = conn.prepareStatement(sql);
							ps.setLong(1, userId);
							ps.setLong(2, topic.id);
							ResultSet rs = ps.executeQuery();
							if(rs.next()){
								if(rs.getTimestamp("quiz_end_time") != null && !rs.getTimestamp("quiz_end_time").equals("")){
									status = "Read";
								}else{
									status = "Reading";
								}
							}
							rs.close();
							//檢查是否有閱讀紀錄
							if(status.equals("UnRead")){
								sql = "SELECT * FROM trace_reading WHERE u_id=? AND topic_uuid=?";
								ps = conn.prepareStatement(sql);
								ps.setLong(1, userId);
								ps.setLong(2, topic.id);
								rs = ps.executeQuery();
								if(rs.next()){
									status = "Reading";
								}
								rs.close();
							}
							topic.status = status;
							//
							vecTopic.add(topic);
	   					}
						if(vecTopic.size() > 0){
							int _ReadCount= 0;
							int _ReadingCount = 0;
							int _UnReadCount = 0;
							for(Topic t: vecTopic){
								if(t.status.equalsIgnoreCase("Read")){
									_ReadCount++;
								}else if(t.status.equalsIgnoreCase("Reading")){
									_ReadingCount++;
								}else if(t.status.equalsIgnoreCase("UnRead")){
									_UnReadCount++;
								}
							}
							//
							Subject subject = new Subject();
							subject.name = fd2.getName();
							if(_ReadCount == vecTopic.size()){
								subject.status = "Read";
							}else if(_UnReadCount == vecTopic.size()){
								subject.status = "UnRead";
							}else{
								subject.status = "Reading";
							}
							//
							htSubject.put(subject, vecTopic);
						}
	   				}
	   			}
	   		}
	   		//將Hashtable排序
 			Vector<Subject> vecSubject = new Vector<Subject>();
 			Vector<String> vecSort = new Vector<String>();
 			Enumeration<Subject> en = htSubject.keys();
 			while(en.hasMoreElements()){
 				vecSort.add(en.nextElement().name);
 			}
 			Collections.sort(vecSort);
 			for(String s: vecSort){
 				en = htSubject.keys();
 				while(en.hasMoreElements()){
 					Subject sb = en.nextElement();
 					if(sb.name.equals(s)){
 						vecSubject.add(sb);
 					}
 				}
 			}
 			//產出 TopicTree
 			for(Subject sb: vecSubject){
 				String iconTag = "<img src='" + renderRequest.getContextPath() + "/images/green_box.png'>";
 				if(sb.status.equalsIgnoreCase("Reading")){
 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/orange_box.png'>";
 				}else if(sb.status.equalsIgnoreCase("Read")){
 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/red_box.png'>";
 				}
 				//
				topicTree += "<li class=\"closed\">" + "<span class=\"subject\">" + iconTag + "&nbsp;" + sb.name + "</span>";
				topicTree += "<ul>";
 				//
 				vecSort = new Vector<String>();
 				for(Topic tp: htSubject.get(sb)){
 					vecSort.add(tp.name);
 				}
				Collections.sort(vecSort);
				//將單元排序
				Vector<Topic> vecTopic = new Vector<Topic>();
				for(String s: vecSort){
					for(Topic tp: htSubject.get(sb)){
						if(tp.name.equals(s)){
							vecTopic.add(tp);
							break;
						}
					}
				}
				//
 				for(Topic tp: vecTopic){
 	 				iconTag = "<img src='" + renderRequest.getContextPath() + "/images/green_box.png'>";
 	 				if(tp.status.equalsIgnoreCase("Reading")){
 	 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/orange_box.png'>";
 	 				}else if(tp.status.equalsIgnoreCase("Read")){
 	 					iconTag = "<img src='" + renderRequest.getContextPath() + "/images/red_box.png'>";
 	 				}
 					//
 					String strLink = "/web/feb/topic?topicID=" + tp.id;
 					//String topicRef = "<a href=\"javascript:void(0);\" onclick=\"window.open('" + strLink + "'," +
 					//					"'課程項目', 'toolbar=no, menubar=no, scrollbars=yes, resizable=yes,location=no, status=yes'" +
 					//					")\">" + iconTag + "&nbsp;" + tp.name + "</a>";
 					String topicRef = "<a href=" + strLink  + ">" + iconTag + "&nbsp;" + tp.name + "</a>";
 					
 					topicTree += "<li>" + "<span class=\"topic\">" + topicRef + "</span></li>";
 				}
 				topicTree += "</ul>";
				topicTree += "</li>";
 			}
 			//System.out.println(topicTree);
   		}catch (Exception ex){
   			if(errMsg.equals("")) errMsg += "\n";
   			errMsg +=  utils.getItezErrorCode(ex);
   		}finally{
   			if (conn !=null){
   				try{
   					conn.close();
   				}catch (Exception ignore){
   				}
   			}
   		}
   		//System.out.println(topicTree);
		renderRequest.setAttribute("topicTree", topicTree);
		//
   		renderRequest.setAttribute("errMsg", errMsg);
   		//
   		include(viewJSP, renderRequest, renderResponse);
	}

	protected void include(String path, RenderRequest renderRequest,RenderResponse renderResponse) throws IOException, PortletException {

		PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(path);

		if (portletRequestDispatcher == null) {
			_log.error(path + " is not a valid include");
		}
		else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}
 
	protected String viewJSP;

	private static Log _log = LogFactoryUtil.getLog(NaviPortlet.class);

}

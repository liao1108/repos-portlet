<%@ page contentType="text/html; charset=UTF-8"%> 
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<jsp:useBean class="java.lang.String" id="topicTree" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="currTopic" scope="request"></jsp:useBean>
<jsp:useBean class="java.lang.String" id="errMsg" scope="request"></jsp:useBean>

<portlet:defineObjects />

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Navigator</title>
	
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery.treeview.css"/>
	<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/screen.css" />
	
	<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.4.4.js" type="text/javascript"></script>
	<script src="<%=renderRequest.getContextPath()%>/js/jquery.cookie.js" type="text/javascript"></script>
	<script src="<%=renderRequest.getContextPath()%>/js/jquery.treeview.js" type="text/javascript"></script>
	
	<script type="text/javascript" src="<%=renderRequest.getContextPath()%>/js/demo.js"></script>
	
	<%
    	//int per=15000; //ms
    	String sessionExtenderPath = renderRequest.getContextPath() + "/view2.jsp";
    	//This is a portlet which includes servlet that returns a simple string.
	%>

	<script>
    	periyod= 15 * 1000 * 60;	//15分鐘
    	function invokeRequest(){
     		$.get("<%=sessionExtenderPath%>", function(data) {
      			//alert(1);
     		});
    	}
    	window.setInterval("invokeRequest()",periyod);
	</script>
	
</head>

<body>
	<%if(!errMsg.equals("")){ %>
		<table width="100%" cellspacing="3">
			<tr>
				<td><img src="<%=renderRequest.getContextPath()%>/images/warning.png"></td>
				<td width="100%"><%=errMsg %> </td>
			</tr>
		</table>	
	<%}%>
	<table width="100%" background="<%=renderRequest.getContextPath()%>/images/under_line.png">
		<tr valign="top">
			<td height="30"><img src="<%=renderRequest.getContextPath()%>/images/red_box.png"/></td>
			<td nowrap>通過檢測&nbsp;</td>
			<td><img src="<%=renderRequest.getContextPath()%>/images/orange_box.png"/></td>
			<td nowrap>閱讀中&nbsp;</td>
			<td><img src="<%=renderRequest.getContextPath()%>/images/green_box.png"/></td>
			<td width="100%" align="left"" nowrap>未閱讀</td>
		</tr>
	</table>
	<table width="100%" border = "0">
	  <tr>
	   <td>
		<form id="myNaviForm" method="POST" action="<portlet:actionURL/>">
			<ul id="browser" class="filetree">
				<%=topicTree%>
			</ul>
			<input type="hidden" name="currTopic" value="<%=currTopic%>"/>
		</form>
	   </td>
	  </tr>	
	</table>
</body>
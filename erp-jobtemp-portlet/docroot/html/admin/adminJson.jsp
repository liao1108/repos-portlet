<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import= "com.liferay.portal.kernel.dao.search.SearchEntry"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>

<%@ page import="com.itez.JsonFile" %>
<%@ page import="com.itez.ErpUtil" %>

<portlet:defineObjects />

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	JsonFile jsFile = (JsonFile)row.getObject();
%>

<liferay-ui:icon-menu>
	<portlet:renderURL var="copyJsonURL">
  		<portlet:param name="jspPage"  value="/html/jobtemp/copyJson.jsp"/>
  		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="renameJsonURL">
  		<portlet:param name="jspPage"  value="/html/jobtemp/renameJson.jsp"/>
  		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
	</portlet:renderURL>
	
	<portlet:resourceURL var="editAllocateURL32" >
  		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
  		<portlet:param name="actionType"  value="edit32"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="editAllocateURL64" >
  		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
  		<portlet:param name="actionType"  value="edit64"/>
	</portlet:resourceURL>

	<portlet:renderURL var="deleteJsonURL">
		<portlet:param name="jspPage"  value="/html/jobtemp/deleteJson.jsp"/>
		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
	</portlet:renderURL>
	
	<portlet:renderURL var="changeStatusOnURL">
		<portlet:param name="jspPage"  value="/html/jobtemp/changeStatus.jsp"/>
		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
		<portlet:param name="statusType"  value="1"/>
	</portlet:renderURL>

	<portlet:renderURL var="changeStatusOffURL">
		<portlet:param name="jspPage"  value="/html/jobtemp/changeStatus.jsp"/>
		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
		<portlet:param name="statusType"  value="0"/>
	</portlet:renderURL>

	<portlet:resourceURL var="downloadFileURL" >
  		<portlet:param name="fileId"  value="<%=jsFile.fileId %>"/>
  		<portlet:param name="actionType"  value="download"/>
  		<portlet:param name="currentTime"  value="<%=ErpUtil.getCurrDateTime() %>"/>
	</portlet:resourceURL>
	
	<liferay-ui:icon image="edit" message="線上編輯(32位元)" url="<%=editAllocateURL32.toString() %>"/>
	<liferay-ui:icon image="edit" message="線上編輯(64位元)" url="<%=editAllocateURL64.toString() %>"/>
	<liferay-ui:icon image="add" message="複製" url="<%=copyJsonURL.toString() %>"/>
	<liferay-ui:icon image="edit" message="更名" url="<%=renameJsonURL.toString() %>"/>
	<%if(!jsFile.jobStatus.contains("停用")){ %>
		<liferay-ui:icon image="edit" message="停用" url="<%=changeStatusOffURL.toString() %>"/>
	<%}else{ %>
		<liferay-ui:icon image="edit" message="啟用" url="<%=changeStatusOnURL.toString() %>"/>
	<%} %>
	<liferay-ui:icon image="view" message="下載" url="<%=downloadFileURL.toString() %>"/>
	<liferay-ui:icon image="delete" message="刪除" url="<%=deleteJsonURL.toString() %>"/>
</liferay-ui:icon-menu>
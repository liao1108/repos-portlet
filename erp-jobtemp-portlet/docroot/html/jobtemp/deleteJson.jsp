<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>


<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.JsonFile"%>

<portlet:defineObjects />

<%
	Vector<JsonFile> vecFile = new Vector<JsonFile>();
	if(renderRequest.getPortletSession(true).getAttribute("vecFile") != null){
		vecFile = (Vector<JsonFile>)renderRequest.getPortletSession(true).getAttribute("vecFile");
	}
	//
	String fileId = "";
	if(renderRequest.getParameter("fileId") != null){
		fileId = renderRequest.getParameter("fileId");
	}
	
	//
	JsonFile jsFile = new JsonFile();
	for(JsonFile _js: vecFile){
		if(_js.fileId.equals(fileId)){
			jsFile = _js;
			break;
		}
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();

%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:actionURL var="deleteJsonFileURL"  name="deleteJsonFile" >
</portlet:actionURL>	

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/html/jobtemp/viewJson.jsp"/>
</portlet:renderURL>	


<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/delete.png">&nbsp;工作範本刪除
	</aui:column>
</aui:layout>

<hr/>

<aui:fieldset>
<aui:form action="<%=deleteJsonFileURL%>" method="post" name="<portlet:namespace />fm">
	<aui:layout>
		<aui:column cssClass="td">
			<aui:input type="hidden" name="fileId"  value="<%=fileId %>"/>
			範本檔名：<%=jsFile.fileName %>
		</aui:column>
	</aui:layout>
	
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" />	
		<aui:button type="submit" value="確定刪除" />
	</aui:button-row>
</aui:form>
</aui:fieldset>
<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>
<%@ page import= "com.itez.SecInd"%>

 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	Vector<SecInd> vecSecInd = new Vector<SecInd>();
	if(renderRequest.getPortletSession(true).getAttribute("vecSecInd") != null){
		vecSecInd = (Vector<SecInd>)renderRequest.getPortletSession(true).getAttribute("vecSecInd");
	}
	int rowsPrefer = 75;
	if(renderRequest.getPortletSession(true).getAttribute("rowsPrefer") != null){
		rowsPrefer = Integer.parseInt(renderRequest.getPortletSession(true).getAttribute("rowsPrefer").toString());
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:resourceURL var="editAllocateURL" >
  	<portlet:param name="actionType"  value="edit"/>
  	
</portlet:resourceURL>
		
<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/bank.png">&nbsp;您可編輯工作範本的組別與業別
	</aui:column>
	
	<aui:column>
 		<aui:button onClick="<%=editAllocateURL.toString() %>" value="啟動工作分配編輯程式" />	
	</aui:column>
</aui:layout>
<hr/>
<br>

<liferay-ui:search-container emptyResultsMessage=""  delta="<%=rowsPrefer %>">
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecSecInd, searchContainer.getStart(), searchContainer.getEnd());
			total = vecSecInd.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.SecInd" keyProperty="folderId" modelVar="si" >
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/info.png"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="組別" property="secName"  align="center"/>
		<liferay-ui:search-container-column-text name="行業別" property="indusName"  align="center"/>

		<liferay-ui:search-container-column-jsp path="/html/admin/admin.jsp" align="center"  />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

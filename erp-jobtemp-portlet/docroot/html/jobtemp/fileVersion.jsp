<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 
<%@ page import= "com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.Iterator" %>

<%@ page import= "com.itez.JsonFile"%>
<%@ page import= "com.itez.FileVersion"%>
<%@ page import= "com.itez.ErpUtil"%>


<portlet:defineObjects />

<%
	//Collec collec = new Collec();
	//if(renderRequest.getPortletSession(true).getAttribute("collec") != null){
	//	collec = (Collec)renderRequest.getPortletSession(true).getAttribute("collec");
	//}

	//String stageName = "";
	//if(renderRequest.getPortletSession(true).getAttribute("stageName") != null){
	//	stageName = renderRequest.getPortletSession(true).getAttribute("stageName").toString();
	//}

	String fileId = "";
	if(renderRequest.getParameter("fileId") != null){
		fileId = renderRequest.getParameter("fileId");
	}
	//
	Vector<JsonFile> vecFile = new Vector<JsonFile>();
	if(renderRequest.getPortletSession(true).getAttribute("vecFile") != null){
		vecFile = (Vector<JsonFile>)renderRequest.getPortletSession(true).getAttribute("vecFile");
	}
	//
	String fileName = "";
	for(JsonFile jf: vecFile){
		if(jf.fileId.equals(fileId)){
			fileName = jf.fileName;
			break;
		}
	}
	//
	Vector<FileVersion> vecVersion = new Vector<FileVersion>();
	if(renderRequest.getPortletSession(true).getAttribute("vecVersion") != null){
		vecVersion = (Vector<FileVersion>)renderRequest.getPortletSession(true).getAttribute("vecVersion");
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>

<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage" value="/html/jobtemp/viewJson.jsp"/>
</portlet:renderURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/version.png">&nbsp;版本記錄：<%=fileName %>
	</aui:column>
</aui:layout>

<hr/>

<br/>

<liferay-ui:search-container emptyResultsMessage="" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecVersion, searchContainer.getStart(), searchContainer.getEnd());
			total = vecVersion.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.FileVersion" keyProperty="fileId" modelVar="fileVersion">

		<portlet:resourceURL var="viewJsonFileURL" >
  			<portlet:param name="fileId"  value="<%=fileVersion.fileId %>"/>
  			<portlet:param name="actionType"  value="view"/>
  			<portlet:param name="currentTime"  value="<%=ErpUtil.getCurrDateTime() %>"/>
		</portlet:resourceURL>
		
		<portlet:resourceURL var="editAllocateURL" >
  			<portlet:param name="fileId"  value="<%=fileVersion.fileId %>"/>
  			<portlet:param name="actionType"  value="edit"/>
		</portlet:resourceURL>
		
		<liferay-ui:search-container-column-text  align="center">
			<img src="<%=renderRequest.getContextPath() %>/images/version24.png"/>
		</liferay-ui:search-container-column-text>	
		<liferay-ui:search-container-column-text name="版次" property="versionNo"  align="center" href="<%=viewJsonFileURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="摘要" property="description"  align="center"/>
		<liferay-ui:search-container-column-text name="更新時間" property="updateDate"  align="center"/>
		<liferay-ui:search-container-column-text name="修改人" property="modifier"  align="center"/>
		<liferay-ui:search-container-column-text name="KB" property="fileSize"  align="center"/>
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

<aui:layout>
	<aui:button-row>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" />
	</aui:button-row>			
</aui:layout>


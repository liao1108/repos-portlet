<%@ page contentType="text/html; charset=UTF-8"%> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import= "com.liferay.portal.model.Organization"%>
<%@ page import= "com.liferay.portal.model.User"%>
<%@ page import= "com.liferay.portal.service.OrganizationServiceUtil"%>
<%@ page import= "com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import= "com.liferay.portal.kernel.util.ListUtil"%> 

<%@ page import= "java.util.Vector"%>
<%@ page import= "com.itez.SecInd"%>
<%@ page import="com.itez.JsonFile"%>
 
<portlet:defineObjects />
 
  <%
	ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	User user = themeDisplay.getUser();
	//
	Vector<JsonFile> vecFile = new Vector<JsonFile>();
	if(renderRequest.getPortletSession(true).getAttribute("vecFile") != null){
		vecFile = (Vector<JsonFile>)renderRequest.getPortletSession(true).getAttribute("vecFile");
	}
	//
	String pathName = "";
	if(renderRequest.getPortletSession(true).getAttribute("pathName") != null){
		pathName = renderRequest.getPortletSession(true).getAttribute("pathName").toString();
	}
	//
	String errMsg = "";	
	if(renderRequest.getAttribute("errMsg") != null) errMsg = renderRequest.getAttribute("errMsg").toString();
	
	String successMsg = "";	
	if(renderRequest.getAttribute("successMsg") != null) successMsg = renderRequest.getAttribute("successMsg").toString();
%>

<liferay-ui:error key="error" message="<%=errMsg%>" />
<liferay-ui:success key="success" message="<%=successMsg%>"/>


<portlet:renderURL var="backToViewURL" >
	<portlet:param name="jspPage"  value="/html/jobtemp/view.jsp"/>
</portlet:renderURL>	

<portlet:actionURL var="uplaodURL" name="uploadDocument"></portlet:actionURL>

<aui:layout>
	<aui:column cssClass="header">
 		<img src="<%=renderRequest.getContextPath()%>/images/json.png">&nbsp;<%=pathName %> &nbsp;包含的工作範本檔案
	</aui:column>
	
	<aui:column>
		<aui:button onClick="<%=backToViewURL.toString() %>" value="返回上頁" />	
	</aui:column>	
</aui:layout>

<b>上傳新範本</b> 

<form action="<%=uplaodURL%>" method="post" enctype="multipart/form-data">
	<table><tr>
		<td><input type="file" name="uploadedFile"></td>
		<td>檔案命名:</td>
		<td><input type="text" name="targetFileName"></td>
		<td><input type="Submit" name="開始上傳"></td>
	</tr></table>
</form>

<hr/>
<br>

<liferay-ui:search-container emptyResultsMessage="" delta="75" >
	<liferay-ui:search-container-results>
		<%
			results = ListUtil.subList(vecFile, searchContainer.getStart(), searchContainer.getEnd());
			total = vecFile.size();
			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);
		%>
	</liferay-ui:search-container-results>
	
	<liferay-ui:search-container-row className="com.itez.JsonFile" keyProperty="fileId" modelVar="js">
		<portlet:actionURL var="viewVersionURL" name="viewVersion" >
			<portlet:param name="fileId"  value="<%=js.fileId %>"/>
			<portlet:param name="fileName"  value="<%=js.fileName %>"/>
		</portlet:actionURL>
		
		<liferay-ui:search-container-column-text align="center">
			<img src="<%=renderRequest.getContextPath()%>/images/json2.png"/>
		</liferay-ui:search-container-column-text>
		
		<liferay-ui:search-container-column-text name="範本檔名" property="fileName"  align="center"/>
		<liferay-ui:search-container-column-text name="更新時間" property="updateTime"  align="center"/>
		<liferay-ui:search-container-column-text name="建立人" property="author"  align="center"/>
		<liferay-ui:search-container-column-text name="版本" property="versionNo"  align="center" href="<%=viewVersionURL.toString() %>"/>
		<liferay-ui:search-container-column-text name="修改人" property="modifier"  align="center"/>
		<liferay-ui:search-container-column-text name="狀態" property="jobStatus"  align="center"/>

		<liferay-ui:search-container-column-jsp path="/html/admin/adminJson.jsp" align="center"  />
	</liferay-ui:search-container-row>
	
	<liferay-ui:search-iterator/>
</liferay-ui:search-container>

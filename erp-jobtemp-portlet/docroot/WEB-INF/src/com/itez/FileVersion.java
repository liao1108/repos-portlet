package com.itez;

public class FileVersion {
	public String fileId = "";
	public String updateDate = "";
	public String versionNo = "";
	public String docStatus = "";			//狀態
	public String deletedBy = "";
	public String author = "";			//建立者
	public String modifier = "";		//最後修改者
	public String fileSize = "";
	public String description = "";
}

package com.itez;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.activation.MimetypesFileTypeMap;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.sql.DataSource;

import org.alfresco.cmis.client.AlfrescoDocument;
import org.alfresco.cmis.client.AlfrescoFolder;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.bindings.spi.atompub.AbstractAtomPubService;
import org.apache.chemistry.opencmis.client.bindings.spi.atompub.AtomPubParser;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.apache.http.HttpHeaders;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class JobTempPortlet extends MVCPortlet {
	DataSource ds = null;
	String rootJobTemp = "";
	//
	@Override
	public void init() throws PortletException {
		super.init();
		Connection conn = null;
		try{
			Context initContext = new InitialContext();
	   		Context envContext  = (Context)initContext.lookup("java:/comp/env");
	   		ds = (DataSource)envContext.lookup("jdbc/exam");
	   		//
	   		conn = ds.getConnection();
	   		String sql = "SELECT * FROM settings";
	   		PreparedStatement ps = conn.prepareStatement(sql);
	   		ResultSet rs = ps.executeQuery();
	   		if(rs.next()){
	   			rootJobTemp = rs.getString("folder_job_temp");
	   		}
	   		rs.close();
	   		//
	   		conn.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//列出工作範本組別
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoFolder alfJobTemp = this.getJobTempRootFolder(alfSessionAdmin);
			//目前用戶所屬的群組
			User user = themeDisplay.getUser();
			List<UserGroup> listGroup = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
			//找出有權限的組別
			Iterator<CmisObject> it = alfJobTemp.getChildren().iterator();
			Vector<String> vecSec = new Vector<String>();		//String為FolderId
			while(it.hasNext()){
				CmisObject co = it.next();
				if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){		//組別
					String pureName = co.getName();
					if(pureName.contains(".")){
						pureName = pureName.substring(pureName.indexOf(".") + 1);
					}
					for(UserGroup ug: listGroup){
						if(ug.getName().contains(pureName)){
							vecSec.add(co.getId());
							break;
						}
					}
				}
			}
			//
			Vector<SecInd> vecSecInd = new Vector<SecInd>(); 
			for(String fdId: vecSec){
				AlfrescoFolder fdSec = (AlfrescoFolder)alfSessionAdmin.getObject(fdId);
				it = fdSec.getChildren().iterator();
				while(it.hasNext()){
					CmisObject co = it.next();
					if(co.getBaseTypeId().equals(BaseTypeId.CMIS_FOLDER)){	//業別
						AlfrescoFolder fdIndus = (AlfrescoFolder)co;
						//
						SecInd si = new SecInd();
						si.secName = fdSec.getName();
						si.indusName = fdIndus.getName();
						si.folderId = fdIndus.getId();
						//
						vecSecInd.add(si);
					}
				}
			}
			renderRequest.getPortletSession(true).setAttribute("vecSecInd", vecSecInd);
			//查詢偏好筆數
			int rowsPrefer = ErpUtil.getRowsPrefer(themeDisplay.getUser().getScreenName(), conn);
			renderRequest.getPortletSession(true).setAttribute("rowsPrefer", rowsPrefer);
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			renderRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(renderRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	}

	//進入
	public void enterSecIndus(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
        	//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	//String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	String folderId = actionRequest.getParameter("folderId");
        	//
        	conn = ds.getConnection();
        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
        	AlfrescoFolder fdIndus = (AlfrescoFolder)alfSessionAdmin.getObject(folderId);
        	Vector<JsonFile> vecFile = this.getJsonFiles(fdIndus);
        	//
        	String pathName = alfSessionAdmin.getObject(fdIndus.getParentId()).getName() + " / " + fdIndus.getName();
        	//
        	actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
        	actionRequest.getPortletSession(true).setAttribute("pathName", pathName);
        	//準備接收上傳的檔案夾
        	actionRequest.getPortletSession(true).setAttribute("uploadFolderId", folderId);
        	//
        	actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
        	//
        	SessionMessages.add(actionRequest, "success", "");
        }catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
        }finally{
        	try{
        		if(conn != null) conn.close();
        	}catch(Exception _ex){
        	}
        }
	}
	
	
	private Vector<JsonFile> getJsonFiles(AlfrescoFolder fdIndus) throws Exception{
		Vector<JsonFile> vecFile = new Vector<JsonFile>();
		//
		Iterator<CmisObject> it = fdIndus.getChildren().iterator();
    	while(it.hasNext()){
    		CmisObject co = it.next();
    		if(co.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT) && co.getName().toLowerCase().endsWith("json")){
    			AlfrescoDocument alfDoc = (AlfrescoDocument)co;
    			//
    			JsonFile js = new JsonFile();
    			js.fileId = alfDoc.getId();
    			js.fileName = alfDoc.getName();
    			js.updateTime = ErpUtil.getDateTimeStr(alfDoc.getLastModificationDate().getTime());
    			if(alfDoc.getPropertyValue("cm:author") != null){
    				js.author = alfDoc.getPropertyValue("cm:author");
    			}
    			if(alfDoc.getPropertyValue("it:modifier") != null){
    				js.modifier = alfDoc.getPropertyValue("it:modifier");
    			}
    			js.versionNo = alfDoc.getVersionLabel();
    			if(alfDoc.getPropertyValue("it:docStatus") != null){
    				js.jobStatus = alfDoc.getPropertyValue("it:docStatus");
    			}
    			js.versionNo = alfDoc.getVersionLabel();
    			//
    			vecFile.add(js);
    		}
    	}		
    	//
		return vecFile;
	}
	
	
	//複製檔案
	public void copyJsonFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		String fileId = "";
		String toFileName = "";
		Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	Vector<JsonFile> vecFile = (Vector<JsonFile>)actionRequest.getPortletSession(true).getAttribute("vecFile");
        	//
        	fileId = actionRequest.getParameter("fileId");
        	if(fileId.equals("")) throw new Exception("檔案 ID 找不到 ！。");
        	
        	toFileName = actionRequest.getParameter("toFileName");
        	if(toFileName.equals("")) throw new Exception("請指定目的檔案名稱。");
        	//
        	if(!toFileName.toLowerCase().endsWith(".json")) throw new Exception("工作範本請以 .json 為副檔名。");
        	//確定檔名是否重複
        	boolean nameDup = false;
        	for(JsonFile js: vecFile){
        		if(js.fileName.equals(toFileName)){
        			nameDup = true;
        			break;
        		}
        	}
        	if(nameDup) throw new Exception("目的檔名 " + toFileName + " 已存在。");
        	//
        	conn = ds.getConnection();
        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
        	//Log
        	ErpUtil.logUserAction("複製工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
        	//
        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
        	
        	AlfrescoFolder fdDest = (AlfrescoFolder)alfDoc.getParents().get(0);
        	Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put(PropertyIds.NAME, toFileName);
			props.put("cm:author", userFullName);

			ContentStream cs = new ContentStreamImpl(toFileName, null, alfDoc.getContentStreamMimeType(), alfDoc.getContentStream().getStream());
			// create a major version
			fdDest.createDocument(props, cs, VersioningState.MAJOR);
			//
			vecFile = this.getJsonFiles(fdDest);
			actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
	        	//
	        	actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
	        }catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/jobtemp/copyJson.jsp");
			if(!fileId.equals("")){
				actionResponse.setRenderParameter("fileId", fileId);
			}
			if(!toFileName.equals("")){
				actionResponse.setRenderParameter("toFileName", toFileName);
			}
	    }finally{
	       	try{
	       		if(conn != null) conn.close();
	       	}catch(Exception _ex){
	       	}
	    }
	}
	
	//更名
	public void renameJsonFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		String fileId = "";
		String toFileName = "";
		Connection conn = null;
		try{
        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
        	//
        	Vector<JsonFile> vecFile = (Vector<JsonFile>)actionRequest.getPortletSession(true).getAttribute("vecFile");
        	//
        	fileId = actionRequest.getParameter("fileId");
        	if(fileId.equals("")) throw new Exception("檔案 ID 找不到 ！。");
        	
        	toFileName = actionRequest.getParameter("toFileName");
        	if(toFileName.equals("")) throw new Exception("請指定新的檔名。");
        	//
        	if(!toFileName.toLowerCase().endsWith(".json")) throw new Exception("工作範本請以 .json 為副檔名。");
        	//確定檔名是否重複
        	boolean nameDup = false;
        	for(JsonFile js: vecFile){
        		if(js.fileName.equals(toFileName)){
        			nameDup = true;
        			break;
        		}
        	}
        	if(nameDup) throw new Exception("檔名 " + toFileName + " 已存在。");
        	//
        	conn = ds.getConnection();
        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
        	
        	AlfrescoFolder fdDest = (AlfrescoFolder)alfDoc.getParents().get(0);
        	//Log
        	ErpUtil.logUserAction("工作分配表更名", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
        	//
        	Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put(PropertyIds.NAME, toFileName);
			props.put("it:updateMemo", "更名");
			props.put("it:modifier", userFullName);
			//
			ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
			Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
			pwc.checkIn(false, props, alfDoc.getContentStream(), "更名");
			//
			vecFile = this.getJsonFiles(fdDest);
			actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
	        //
	        actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
	    }catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/jobtemp/renameJson.jsp");
			if(!fileId.equals("")){
				actionResponse.setRenderParameter("fileId", fileId);
			}
			if(!toFileName.equals("")){
				actionResponse.setRenderParameter("toFileName", toFileName);
			}
	    }finally{
        	try{
        		if(conn != null) conn.close();
        	}catch(Exception _ex){
        	}
	    }
	}

	//狀態改變
	public void changeStatus(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	Vector<JsonFile> vecFile = (Vector<JsonFile>)actionRequest.getPortletSession(true).getAttribute("vecFile");
	        	//
	        	String fileId = actionRequest.getParameter("fileId");
	        	if(fileId.equals("")) throw new Exception("檔案 ID 找不到 ！。");
	        	
	        	String statusType = actionRequest.getParameter("statusType");
	        	if(statusType.equals("")) throw new Exception("請指定啟用或停用 ！。");
	        	//
	        	conn = ds.getConnection();
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
	        	
	        	AlfrescoFolder fdDest = (AlfrescoFolder)alfDoc.getParents().get(0);
	        	Map<String, String> props = new HashMap<String, String>();
	        	props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
	        	if(statusType.equals("1")){
	        		ErpUtil.logUserAction("啟用工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        		//
	        		props.put("it:updateMemo", "啟用");
	        		props.put("it:docStatus", "啟用");
	        	}else if(statusType.equals("0")){
					ErpUtil.logUserAction("停用工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
					//
					props.put("it:updateMemo", "停用");
					props.put("it:docStatus", "停用");
				}
				props.put("it:modifier", userFullName);
				//
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				if(statusType.equals("1")) {
					pwc.checkIn(false, props, alfDoc.getContentStream(), "啟用");
				}else {
					pwc.checkIn(false, props, alfDoc.getContentStream(), "停用");
				}
				//
				vecFile = this.getJsonFiles(fdDest);
				actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
	        	//
	        	actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
	        }catch(Exception ex){
				String errMsg = ErpUtil.getItezErrorCode(ex);
				if(errMsg.equals("")){
					errMsg = ex.getMessage();
				}
				actionRequest.setAttribute("errMsg", errMsg);
				SessionErrors.add(actionRequest, "error");
				//
				actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

	//刪除檔案
	public void deleteJsonFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
		String fileId = "";
		Connection conn = null;
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	fileId = actionRequest.getParameter("fileId");
	        	if(fileId.equals("")) throw new Exception("檔案 ID 找不到 ！。");
	        	//
	        	conn = ds.getConnection();
	        	Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
	        	//Log
	        	ErpUtil.logUserAction("刪除工作分配表", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(actionRequest), conn);
	        	//
	        	AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
	        	AlfrescoFolder fdDest = (AlfrescoFolder)alfDoc.getParents().get(0);
	        	alfDoc.delete();
			//
			Vector<JsonFile> vecFile = this.getJsonFiles(fdDest);
			actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
	        	//
	        	actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
	        }catch(Exception ex){
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionResponse.setRenderParameter("jspPage", "/html/jobtemp/deleteJson.jsp");
			if(!fileId.equals("")){
				actionResponse.setRenderParameter("fileId", fileId);
			}
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}
	
	//查詢檔案版本
	public void viewVersion(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		Connection conn = null;
		try{
			//ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			//
			String fileId = actionRequest.getParameter("fileId");
			String fileName = actionRequest.getParameter("fileName");
			//
			Vector<FileVersion> vecVersion = new Vector<FileVersion>();
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
			Iterator<Document> it = alfDoc.getAllVersions().iterator();
			while(it.hasNext()){
				AlfrescoDocument doc = (AlfrescoDocument)it.next();
				//
				FileVersion fv = new FileVersion();
				fv.fileId = doc.getId();
				fv.versionNo = doc.getVersionLabel();
				fv.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
				if(doc.getPropertyValue("cm:author") != null){
					fv.author = doc.getPropertyValue("cm:author");
				}
				if(doc.getPropertyValue("it:modifier") != null){
					fv.modifier = doc.getPropertyValue("it:modifier");
				}
				if(doc.getPropertyValue("it:docStatus") != null){
					fv.docStatus = doc.getPropertyValue("it:docStatus");
				}
				if(doc.getPropertyValue("it:updateMemo") != null){
					fv.description = doc.getPropertyValue("it:updateMemo");
				}
				int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
				fv.fileSize = String.valueOf(dd);
				//
				vecVersion.add(fv);
			}
			//actionRequest.getPortletSession().setAttribute("fileName", fileName);
			//
			actionRequest.getPortletSession(true).setAttribute("vecVersion", vecVersion);
			actionResponse.setRenderParameter("jspPage", "/html/jobtemp/fileVersion.jsp");
			actionResponse.setRenderParameter("fileId", fileId);
			
			//actionResponse.setRenderParameter("fileName", fileName);
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	
	
	
	@Override
	public void serveResource(ResourceRequest resRequest, ResourceResponse resResponse) throws PortletException, IOException {
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay= (ThemeDisplay)resRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			//
			String fileId = "";
			if(resRequest.getParameter("fileId") != null){
				fileId = resRequest.getParameter("fileId");
			}
			String actionType = resRequest.getParameter("actionType");
			//
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			Document doc = null;
			String fileName = "";
			if(!fileId.equals("")){
				doc = (Document)alfSessionAdmin.getObject(fileId);
				fileName = doc.getName();
			}
			//下載時
			if(actionType.equalsIgnoreCase("download")){
				//Log
				ErpUtil.logUserAction("下載工作分配表範本", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				//
				resResponse.setContentType(doc.getContentStreamMimeType());
				resResponse.addProperty(HttpHeaders.CACHE_CONTROL, "max-age=3600, must-revalidate");
				resResponse.addProperty("Content-Disposition","attachment; filename=\"" + URLEncoder.encode (fileName, "utf-8") + "\"");
				OutputStream out = resResponse.getPortletOutputStream();
				//
				java.io.InputStream is = doc.getContentStream().getStream();
				if(is != null){
					byte[] buffer = new byte[4096];
					int n;
					while ((n = is.read(buffer)) > 0) {
					    out.write(buffer, 0, n);
					}
					//
					out.flush();
					out.close();
					is.close();
				}
			}else if(actionType.equalsIgnoreCase("edit32") || actionType.equalsIgnoreCase("edit64") || actionType.equalsIgnoreCase("view")){
				if(!fileId.equals("")){
					if(actionType.equalsIgnoreCase("edit32") || actionType.equalsIgnoreCase("edit64")){
						ErpUtil.logUserAction("編輯工作分配表範本", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
					}else{
						ErpUtil.logUserAction("檢視工作分配表範本", ErpUtil.getFileFullName(fileId, alfSessionAdmin), themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
					}
				}else{	//啟動空白的編輯程式
					ErpUtil.logUserAction("啟動空白工作分配表編輯程式", "", themeDisplay.getUser(), PortalUtil.getHttpServletRequest(resRequest), conn);
				}
				 //Cipher cipher = Cipher.getInstance("DES");
				 //cipher.init(Cipher.ENCRYPT_MODE, "#A29429820");
				//
				String alfTicket = ErpUtil.getAlfrecoTicket(PropsUtil.get("alfresco.admin.user"), PropsUtil.get("alfresco.admin.password"));
				String url = "";
				if(!fileId.equals("") && (actionType.equalsIgnoreCase("edit32") || actionType.equalsIgnoreCase("edit64"))){
					//搜尋最新版本
					AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(doc.getParents().get(0).getId());
					AlfrescoDocument docCurr = this.getLateastVersion(alfSessionAdmin, fdStage, fileId);		//取得最新版本
					url = this.getDocumentURL(docCurr, alfSessionAdmin);
					int idx = url.lastIndexOf("?");
					url = PropsUtil.get("alfresco.host.lan") + "/alfresco/api/-default-/public/cmis/versions/1.0/atom/content/" + URLEncoder.encode(docCurr.getName(), "UTF-8") + url.substring(idx);
				}
				//
				String home = resRequest.getScheme() + "://" + resRequest.getServerName();
				String uri = home + "/" + resRequest.getContextPath();
				String jarName = "/webstart/Allocater.jar";
				if(actionType.equalsIgnoreCase("edit64")){
					jarName = "/webstart/Allocater64.jar";
				}
				String className = "com.itez.AllocateMain";
				//
				resResponse.setContentType("application/x-java-jnlp-file");
				OutputStream out = resResponse.getPortletOutputStream();
				//
				String s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
				s +="<jnlp spec=\"1.0+\" codebase=\"" + uri + "\" href=\"\">";
				s += "<information>";
				s += "<title>檢查報告處理系統工作分配編輯程式</title>";
				s += "<vendor>商頁網股份有限公司</vendor>";
				s += "<homepage href=\"" + home + "\" />";
				s += "<description></description>";
				s += "</information>";
				s += "<security>";
				s += "<all-permissions/>";
				s += "</security>";
				s += "<resources>";
				s += "<j2se version=\"1.7+\" />";
				s += "<jar href=\"" + jarName + "\" />";
				s += "</resources>";
				s += "<application-desc main-class=\"" + className + "\" >";
				s += "<argument>" +  PropsUtil.get("alfresco.host.lan") + "</argument>";
				s += "<argument>" +  PropsUtil.get("alfresco.admin.user") + "</argument>";
				s += "<argument>" +  PropsUtil.get("alfresco.admin.password") + "</argument>";
				if(!fileId.equals("")){
					s += "<argument>" +  fileId + "</argument>";
					s += "<argument>" + (url + "&alf_ticket=" + alfTicket) + "</argument>";
					s += "<argument>" + userFullName + "</argument>";
					s += "<argument>" + "T" + "</argument>";		//T:範本區 E:檢查報告處理區
				}
				s += "</application-desc>";
				s += "</jnlp>";
				//
				out.write(s.getBytes());
				out.flush();
				out.close();
			}
			conn.close();
		}catch(Exception ex){
			//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			System.out.println(errMsg);
			
			resRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(resRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}
	
	private String getDocumentURL(Document document, Session session) {
		    String link = null;
		    try {
		        Method loadLink = AbstractAtomPubService.class.getDeclaredMethod("loadLink", 
		            new Class[] { String.class, String.class, String.class, String.class });
		        loadLink.setAccessible(true);
		        link = (String) loadLink.invoke(session.getBinding().getObjectService(), session.getRepositoryInfo().getId(),
		            document.getId(), AtomPubParser.LINK_REL_CONTENT, null);
		    } catch (Exception e) {
		       e.printStackTrace();
		    }
		    return link;
	  }	

	public void uploadDocument(ActionRequest actionRequest,ActionResponse actionResponse) throws IOException,PortletException, PortalException, SystemException{
		Connection conn = null;
		try{
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		      
			String fileName = uploadPortletRequest.getFileName("uploadedFile");
			if(!fileName.toLowerCase().endsWith("json")) throw new Exception("範本檔案請以 json 為副檔名。");
			//
			String targetFileName = "";
			try{
				targetFileName = ParamUtil.getString(uploadPortletRequest, "targetFileName");
			}catch(Exception _ex){
			}
			if(targetFileName.equals("")) throw new Exception("請指定上傳後所要儲存的檔名(檔案命名不能空白)。");
			if(!targetFileName.toLowerCase().endsWith(".json")){
				int idxLastDot = targetFileName.lastIndexOf(".");
				if(idxLastDot < 0){
					targetFileName += ".json";
				}else{
					targetFileName = targetFileName.substring(0, idxLastDot) + ".json";
				}
			}
			//
			File file = uploadPortletRequest.getFile("uploadedFile");
			String mimeType = uploadPortletRequest.getContentType("uploadedFile");

			String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
			
			conn = ds.getConnection();
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin(conn);
			String folderId = actionRequest.getPortletSession(true).getAttribute("uploadFolderId").toString();
			AlfrescoFolder fdDest = (AlfrescoFolder)alfSessionAdmin.getObject(folderId);
        	
			AlfrescoDocument alfDocDest = null;
			for(CmisObject co: fdDest.getChildren()){
				if(!(co instanceof org.apache.chemistry.opencmis.client.api.Document)) continue;
				//
				if(co.getName().equalsIgnoreCase(targetFileName)){
					alfDocDest = (AlfrescoDocument)co;
					break;
				}
			}
			//
			InputStream is = new BufferedInputStream(new FileInputStream(file));
			ContentStream cs = new ContentStreamImpl(targetFileName,
														null,
														mimeType,
														is);
			if(alfDocDest == null){
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put(PropertyIds.NAME, targetFileName);
				props.put("cm:author", userFullName);
				// create a major version
				fdDest.createDocument(props, cs, VersioningState.MAJOR);
				//
				Vector<JsonFile> vecFile = this.getJsonFiles(fdDest);
				actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
			}else{
				Map<String, String> props = new HashMap<String, String>();
				props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
				props.put("it:updateMemo", "更新");
				props.put("it:modifier", userFullName);
				//
				ObjectId idOfCheckedOutDocument = alfDocDest.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				pwc.checkIn(false, props, cs, "更新");
				//
				Vector<JsonFile> vecFile = this.getJsonFiles(fdDest);
				actionRequest.getPortletSession(true).setAttribute("vecFile", vecFile);
			}
	        //
	        actionResponse.setRenderParameter("jspPage", "/html/jobtemp/viewJson.jsp");
		}catch(Exception ex){
			//String errMsg = ErpUtil.getItezErrorCode(ex);
			//if(errMsg.equals("")){
				String errMsg = ex.getMessage();
			//}
			//System.out.println(errMsg);
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
		}finally{
			try{
				if(conn != null) conn.close();
			}catch(Exception _ex){
			}
		}
	}	

	
	/*
	private Collec getStageFiles(String stageFolderId, ThemeDisplay themeDisplay, Connection conn, String userFullName) throws Exception{
		Collec collec = new Collec();
		//
		Vector<StageFile> vecFiles = new Vector<StageFile>();
		Vector<StageFile> vecDeletedFiles = new Vector<StageFile>();
		//
		Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
		AlfrescoFolder fdStage = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId);
		collec.userFullName = userFullName;
		collec.stageFolderId = stageFolderId;
		collec.stageName = fdStage.getName();
		collec.examNo =  ((AlfrescoFolder)alfSessionAdmin.getObject(fdStage.getParentId())).getName();
		//報告案是否已關閉
		boolean reportClosed = ErpUtil.isReportClosed(collec.examNo, conn);
		//
		ExamFact ef = ErpUtil.getExamInfo(fdStage, alfSessionAdmin, conn);
		collec.isComputerAudit = ef.isCompuerAudit;
		collec.isEssenNeed = ef.isEssenNeed;
		collec.isInsuForeBank = ef.isInsuForeBank;
		collec.isInsuForeBankNormal = ef.isInsuForeBankNormal;
		collec.isRatingAssess = ef.isRatingAssess;
		collec.isSecuritiesNotes = ef.isSecuritiesNotes;
		collec.isSepecialExam = ef.isSpecialExam;
		collec.examCommentProduceType = ef.examCommentProduceType;
		//this.evalAllocateTiming = ef.evalAllocateTiming;
		//判斷登入者是否為領隊
		if(ef.leaderName.contains(userFullName)) collec.isLeader = true;
		if(ef.mainAssessers.contains(userFullName)) collec.isMainAssesser = true;
		//
		HashMap<String, StageFile> htFile = new HashMap<String, StageFile>();
		//
		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			AlfrescoDocument doc = (AlfrescoDocument)it.next();
			//
			if(doc.getBaseTypeId().equals(BaseTypeId.CMIS_DOCUMENT)){
				StageFile sf = new StageFile();
				//
				sf.fileName = doc.getName();
				if(doc.getPropertyValue("cm:author") != null){
					sf.author = doc.getPropertyValue("cm:author");
				}
				if(doc.getPropertyValue("it:modifier") != null){
					sf.modifier = doc.getPropertyValue("it:modifier");
				}
				if(collec.isLeader || sf.fileName.contains(userFullName) || 
						sf.author.equals(userFullName) || sf.modifier.equals(userFullName)){
					sf.fileId = doc.getId();
					sf.stageName = collec.stageName;
					sf.stageFolderId = fdStage.getId();
					//
					//Document doc = (Document)co;
					//
					//DateFormat formatter= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					//formatter.setTimeZone(TimeZone.getTimeZone("Asia/Taipei"));
					sf.createDate = ErpUtil.getDateTimeStr(doc.getCreationDate().getTime());
					sf.updateDate = ErpUtil.getDateTimeStr(doc.getLastModificationDate().getTime());
					sf.versionNo = doc.getVersionLabel();
					sf.deletedBy = doc.getPropertyValue("it:deletedBy");		
					if(doc.getPropertyValue("it:docStatus") != null){
						sf.docStatus = doc.getPropertyValue("it:docStatus");
					}
					sf.leaderName = ef.leaderName;
					//
					//if(Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString()) == 0d){
					//	doc.updateProperties(arg0)
					//}else{
						int dd = (int)Math.round((Double.parseDouble(doc.getPropertyValue("cmis:contentStreamLength").toString())/1024));
						sf.fileSize = String.valueOf(dd);
								
						String iconName = "file.png";
						if(sf.fileName.toLowerCase().endsWith("xls")){
							iconName = "xls.png";
						}else if(sf.fileName.toLowerCase().endsWith("doc") || sf.fileName.toLowerCase().endsWith("docx")){
							iconName = "doc.png";
						}else if(sf.fileName.toLowerCase().endsWith("ppt")){
							iconName = "ppt.png";
						}else if(sf.fileName.toLowerCase().endsWith("pdf")){
							iconName = "pdf.png";
						}
						sf.iconUrl = iconName;
						sf.isClosed = reportClosed;
						if(sf.fileName.contains("Working Copy")){
							sf.isWorkingCopy = true;
						}else if(doc.isVersionSeriesCheckedOut()){
							sf.isLocked = true;
							sf.docStatus = "領出";
						}
						//
						if(sf.fileName.startsWith("_del#")){
							sf.fileName = sf.fileName.substring(5);
							//
							vecDeletedFiles.add(sf);
						}else{
							//vecFiles.add(sf);
							htFile.put(sf.fileName, sf);
						}
					//}
				}
			}
		}
		//排序處理
		Vector<String> vecSort = new Vector<String>();
		Iterator<String> it2 = htFile.keySet().iterator();
		while(it2.hasNext()){
			String s = it2.next();
			vecSort.add(s);
		}
		Collections.sort(vecSort);
		for(String fileName: vecSort){
			vecFiles.add(htFile.get(fileName));
		}
		//
		collec.vecFiles = vecFiles;
		collec.vecDeletedFiles = vecDeletedFiles;
		//
		return collec;
	}
	
	private StageFile getStageFile(String fileId, Vector<StageFile> _vecFiles) throws Exception{
		StageFile stageFile = null;
		for(StageFile sf: _vecFiles){
			if(sf.fileId.equals(fileId)){
				stageFile = sf;
				break;
			}
		}
		return stageFile;
	}
	
	//檔案更新
	public void updateFile(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException, IOException {
	        Connection conn = null;
	        StageFile currFile = new StageFile();
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	String userFullName = ErpUtil.getCurrUserFullName(themeDisplay);
	        	//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	//
	        	String fileId = ParamUtil.getString(uploadRequest,"fileId");
	        	
	        	if(fileId == null) throw new Exception("檔案 Id 為空值。");
	        	
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	Vector<StageFile> _vecFiles = collec.vecFiles;
	        	//
	        	currFile = this.getStageFile(fileId, _vecFiles);
	        	if(currFile == null) throw new Exception("檔案 Id: " + fileId + " 找不到。");
	        		
	        	actionRequest.getPortletSession(true).setAttribute("currFile", currFile);
	        	//
	        	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要更新的檔案。");
	        	//
		        String sourceFileName = uploadRequest.getFileName("fileName");
	        	String origFileName = ParamUtil.getString(uploadRequest,"origFileName");
	        	//IE 下載時會將空白以加號取代
	        	sourceFileName= sourceFileName.replace("+(Working+Copy)", " (Working Copy)");
	        	if(sourceFileName.contains("(") && sourceFileName.contains(")")){
	        		boolean isSaveVersion = false;
	        		int idx1 = sourceFileName.lastIndexOf("(");
	        		int idx2 = sourceFileName.lastIndexOf(")");
	        		try{
	        			Integer.parseInt(sourceFileName.substring(idx1 +1, idx2));
	        			isSaveVersion = true;
	        		}catch(Exception _ex){
	        		}
	        		if(isSaveVersion){
	        			String vv = " " + sourceFileName.substring(idx1, idx2 + 1);
	        			sourceFileName = sourceFileName.replace(vv, "");
	        		}
	        	}
	        	//
	        	String origPureName = origFileName.substring(0, origFileName.lastIndexOf("."));
	        	String sourcePureName = sourceFileName.substring(sourceFileName.lastIndexOf("//") + 1).substring(0, sourceFileName.lastIndexOf("."));
	        	String origExtName = origFileName.substring(origFileName.lastIndexOf(".") + 1);
	        	String sourceExtName = sourceFileName.substring(sourceFileName.lastIndexOf(".") + 1);
	        	
		        if(!origExtName.equalsIgnoreCase(sourceExtName) || !sourcePureName.startsWith(origPureName)){ 
		        	throw new Exception("您上傳的檔名(" + sourceFileName + ")與所要更新的檔名不符。");
		        }
		        //檢查是否在流程中
		        conn = ds.getConnection();
		        
		        String examNo = collec.examNo;
		        String stageName = collec.stageName;
		        
		        if(ErpUtil.underToDoList(examNo, stageName, origFileName, conn)) throw new Exception(origFileName + " 已在流程處理中，無法變更。");
		        //
		        String updateMemo = ParamUtil.getString(uploadRequest,"updateMemo");
		        if(updateMemo.equals("")) updateMemo = "更新";
		        //
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId); 
			//
			InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
			ContentStream cs = new ContentStreamImpl(alfDoc.getName(), null, alfDoc.getContentStreamMimeType(), is);
		        //
			Map<String, String> props = new HashMap<String, String>();
			props.put("it:updateMemo", updateMemo);
			props.put("it:modifier", userFullName);
			//
		        if(!alfDoc.isVersionSeriesCheckedOut()){		//內容更新
				ObjectId idOfCheckedOutDocument = alfDoc.checkOut();
				Document pwc = (Document)alfSessionAdmin.getObject(idOfCheckedOutDocument);
				ObjectId objectId = pwc.checkIn(false, props, cs, updateMemo);
				alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(objectId);
		        }
			is.close();
	        }catch(Exception ex){
	        	//PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRequest.getPortletSession(true).setAttribute("currFile", currFile);
			actionResponse.setRenderParameter("jspPage", "/html/stagefiles/update.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}

	
	//檔案上傳
	public void uploadFile(ActionRequest actionRequest, ActionResponse actionRresponse) throws PortletException, IOException {
	        Connection conn = null;
		try{
	        	ThemeDisplay themeDisplay= (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        	//
	        	Collec collec = (Collec)actionRequest.getPortletSession(true).getAttribute("collec");
	        	String stageFolderId = collec.stageFolderId;
	        	//
	        	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
	        	if (uploadRequest.getSize("fileName")==0)  throw new Exception("請指定所要上傳的新檔案。");
	        	//
		        String sourceFileName = uploadRequest.getFileName("fileName");
		        //
			Session alfSessionAdmin = ErpUtil.getAlfrescoSessionAdmin();
			AlfrescoFolder fdDest = (AlfrescoFolder)alfSessionAdmin.getObject(stageFolderId); 
			//
			CmisObject co = null;
			try{
				co = alfSessionAdmin.getObjectByPath(fdDest.getPath() + "/" + sourceFileName );
			}catch(Exception _ex){
			}
			if(co != null) throw new Exception("檔案：" + sourceFileName + " 已存在，請以「更新」動作異動文件。");
		        //
		        File file = uploadRequest.getFile("fileName");
		        InputStream is = new BufferedInputStream(uploadRequest.getFileAsStream("fileName"));
		        //FileInputStream fis = new FileInputStream(file);
		        MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		        
		        Map<String, String> props = new HashMap<String, String>();
			props.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:author");
			props.put(PropertyIds.NAME, sourceFileName);
			props.put("cm:author", ErpUtil.getCurrUserFullName(themeDisplay));

			ContentStream cs = new ContentStreamImpl(sourceFileName, null, mimeTypesMap.getContentType(file), is);
			// create a major version
			fdDest.createDocument(props, cs, VersioningState.MAJOR);
			//
			is.close();
	        }catch(Exception ex){
	        	//ex.printStackTrace();
			String errMsg = ErpUtil.getItezErrorCode(ex);
			if(errMsg.equals("")){
				errMsg = ex.getMessage();
			}
			actionRequest.setAttribute("errMsg", errMsg);
			SessionErrors.add(actionRequest, "error");
			//
			actionRresponse.setRenderParameter("jspPage", "/html/stagefiles/uploadFile.jsp");
	        }finally{
	        	try{
	        		if(conn != null) conn.close();
	        	}catch(Exception _ex){
	        	}
	        }
	}	
	*/
	
	//取得在 Alfresco 的工作範本區
	public AlfrescoFolder getJobTempRootFolder(Session alfSession) throws Exception{
		AlfrescoFolder ret = null;
		//
		Iterator<CmisObject>  it = alfSession.getRootFolder().getChildren().iterator();
		 while(it.hasNext()){
			 CmisObject co = it.next();
			 if(co.getName().equals(rootJobTemp)){
				ret = (AlfrescoFolder)co;
				break;
			 }
		 }
		 return ret;
	}
	
	//取的檔案最新版本
	private AlfrescoDocument getLateastVersion(Session alfSessionAdmin, AlfrescoFolder fdStage, String fileId) throws Exception{
		AlfrescoDocument alfDoc = (AlfrescoDocument)alfSessionAdmin.getObject(fileId);
		Iterator<CmisObject> it = fdStage.getChildren().iterator();
		while(it.hasNext()){
			CmisObject co = it.next();
			if(co.getName().equals(alfDoc.getName())){
				alfDoc = (AlfrescoDocument)co;
				break;
			}
		}
		return alfDoc;
	}	
	
}
